
	if(ds_list_size(braille_select) > 0){
		var pos = ds_list_find_index(braille_select,braille_display[menupos])
	
		if(pos == -1){
			if(move_cando[|menupos] && moves_mp_cost[|menupos] <= character.mp.current){
				menuBattleTabletRight();
			} else {
				if(moves_mp_cost[|menupos] <= character.mp.current){
					textboxBattleMain("invalid_lowmp")	
				} else {
					textboxBattleMain("invalid_tabletrecover")	
				}
			}
		} else {
			if(move_cando[|menupos]){
				soundfxPlay(sound_select)
				var moves_instance = menuCreateInstance(menuBattleTabletConfirm);
				moves_instance.character = character;
				moves_instance.add_string = selected_string;
				moves_instance.mp_total_cost = total_mpcost;
				var obj = moves[|main_index];
				with(moves_instance){
					move_id = obj;
					moveData(obj);	
					move_type_sprite = menuBattleMovesGetElementSprite(element)
				}
			}
		}
	} else {
		menuBattleTabletRight();	
	}

