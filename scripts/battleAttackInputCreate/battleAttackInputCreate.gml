var char = argument[0];
var tar = argument[1];
var att = argument[2];

var ha = instance_create(0,0,battleAttackInput);

moveData(att);

ha.character = char;
ha.target = tar
ha.attack = att;
ha.priority = speed_priority;

with(BattleHandler){
	ds_list_add(attack_input,ha)
}

return ha;