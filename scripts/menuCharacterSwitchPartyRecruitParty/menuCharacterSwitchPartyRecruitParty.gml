//draw party stuff

var chara = -1;

var xoff = 20;
var yoff = 10;
var spritexoff = -10
var spriteyoff = 0;
var namexoff = 125;
var nameyoff = 20;
var namesize = 2;
var levelxoff = 140;
var levelyoff = 30+nameyoff;
var levelsize = 1;

var spaa = 800*.333
draw_set_color($111111)
for(var i = 0; i < menu_size; i++){
	draw_circle(64+xoff+spritexoff+spaa*i,64+yoff+spriteyoff,65,false)
}

draw_set_color(global.textColor)
for(var i = 0; i < 5; i++){
	chara = recruit_list[|(((menupos-2) + i+menu_size) mod menu_size)];
	if(!is_undefined(chara)){
	draw_sprite(chara.face_sprite,0,xoff+spritexoff+spaa*i,yoff+spriteyoff);
	draw_text_transformed_ratio(xoff+namexoff+spaa*i,yoff+nameyoff,chara.name,namesize,namesize,0);
	draw_text_transformed_ratio(xoff+levelxoff+spaa*i,yoff+levelyoff,"Lv." + string(chara.level),levelsize,levelsize,0)
	}
}