if(ds_exists(menu_script,ds_type_list)){

	var menu_select = menu_script[| menupos];

	if(!is_undefined(menu_select) && is_real(menu_select)){
		if(menu_select != -1){
			with(text_box){
				instance_destroy();	
			}
			
			script_execute(menu_select);	
			soundfxPlay(sound_select);
		}
	}

	with(menuControl){
		please_draw_all_visible = true
	
	}
}