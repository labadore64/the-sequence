
if(!is_undefined(argument[0])){
	// validate items
	with(argument[0]){
		
		// if an item doesn't exist, make it empty
		for(var i = 0; i < 5; i++){
			if(item[i] >= objdata_items.data_size){
				item[i] = -1;	
			}
		}
		
		// if a tablet doesn't exist, make it empty
		for(var i = 0; i < 3; i++){
			if(item[i] >= objdata_tablet.data_size){
				item[i] = -1;	
			}
		}
		
		// if experience is not in range, correct it
		if(experience < 0){
			experience = 0;
		} else if (experience > objdata_character.total_exp[character]){
			experience = objdata_character.total_exp[character];
		}
	}
}