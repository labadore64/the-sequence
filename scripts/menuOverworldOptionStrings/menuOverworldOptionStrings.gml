update_image_only_active = false;
menuAddOption(global.langini[0,26],global.langini[1,26],menuOverworldOptionTextScript)
menuAddOption(global.langini[0,27],global.langini[1,27],menuOverworldOptionGraphicScript)
menuAddOption(global.langini[0,28],global.langini[1,28],menuOverworldOptionSoundScript)
menuAddOption(global.langini[0,29],global.langini[1,29],menuOverworldOptionBattleScript)
menuAddOption(global.langini[0,30],global.langini[1,30],menuOverworldOptionControlsScript);
menuAddOption(global.langini[0,6],global.langini[1,10],menuOverworldOptionBraillescript);
surfa = -1
mouseHandler_clear_main()
menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();

menu_draw = menuOverworldOptionDraw;

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menu_up = menuOverworldOptionRealUp;
menu_down = menuOverworldOptionRealDown;

menu_left = -1;
menu_right = -1;

