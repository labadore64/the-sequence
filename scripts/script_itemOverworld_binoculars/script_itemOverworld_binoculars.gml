with(menuParent){
	ignore_clean_script = false;
	instance_destroy();
}

with(Player){
	prop_place_id = -1;
	prop_placing = false;
	
	spray_release_type = SPRAY_NONE
	spray_lock = false; 
	
	binocular_item = !binocular_item;
	overworld_item_active = binocular_item;
	binoculars = !binoculars;
}

with(CameraOverworld){
	binoculars = true;	
}

return true