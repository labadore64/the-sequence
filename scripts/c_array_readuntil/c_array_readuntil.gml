//reads until it finds the sequence in the array.

var checkbytes = argument[0]; //bytes to look out for

var byteArray = argument[1]; //bytes that you're searching thru

//returns the byte array if its smaller than the check array
if(array_length_1d(byteArray) < array_length_1d(checkbytes)){
	return byteArray;	
}

var bytes; //bytes that represent the last trailing bytes, length = checkbytes.length

var sizer = array_length_1d(checkbytes);

for(var i = 0; i < sizer; i++){
	bytes[i] = 0;	
}

var returnbytes; //what to return.
returnbytes[0] = -1;

var counter = -1;

var arraysize = array_length_1d(byteArray);

var neverLocated = false;

for(var j = 0; j < arraysize; j++){

		
			counter++
			returnbytes[counter] = byteArray[j];

			for(var i = 1; i < sizer; i++){
				bytes[i] = bytes[i-1];	
			}
		
			bytes[0] = returnbytes[counter]

		
			if(c_comparebytes(bytes,checkbytes)){
				break;
			}

	if(j == arraysize-1){
		neverLocated = true;	
	}
}
if(neverLocated){
	return byteArray;	
}

var returner;
returner[0] = -1;

for(var i = 0; i < array_length_1d(returnbytes)-1; i++){
	returner[i] = returnbytes[i];
}

return returner;