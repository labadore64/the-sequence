update_image_only_active = false;
var script_for_update = menuOverworldKeyboardGamepadStart

menuAddOption(global.langini[9,14],global.langini[10,14],menuOverworldKeyboardGamepadbindSetDefault)
menuAddOption(global.langini[9,15],global.langini[10,15],-1)
menuAddOption(global.langini[9,16],global.langini[10,16],-1)
menuAddOption(global.langini[9,0],global.langini[10,0],script_for_update) // select
menuAddOption(global.langini[9,2],global.langini[10,2],script_for_update) // cancel 
menuAddOption(global.langini[9,3],global.langini[10,3],script_for_update) // up
menuAddOption(global.langini[9,4],global.langini[10,4],script_for_update) // down
menuAddOption(global.langini[9,5],global.langini[10,5],script_for_update) // left
menuAddOption(global.langini[9,6],global.langini[10,6],script_for_update) // right
menuAddOption(global.langini[9,7],global.langini[10,7],script_for_update) // open menu
menuAddOption(global.langini[9,8],global.langini[10,8],script_for_update) // info
menuAddOption(global.langini[9,9],global.langini[10,9],script_for_update) // run
menuAddOption(global.langini[9,10],global.langini[10,10],script_for_update) // bookmark
menuAddOption(global.langini[9,18],global.langini[10,18],script_for_update) // sight
menuAddOption(global.langini[9,19],global.langini[10,19],script_for_update) // coordinates
menuAddOption(global.langini[9,11],global.langini[10,11],script_for_update) // help
menuAddOption(global.langini[9,12],global.langini[10,12],script_for_update) // hud
menuAddOption("Screenshot","Take a screenshot!",script_for_update) // screenshot
menuAddOption(global.langini[9,13],global.langini[10,13],script_for_update) // exit

axis_values = ds_list_create();
axis_strings = ds_list_create();

vib_values = ds_list_create();
vib_strings = ds_list_create();

ds_list_add(axis_values,false,true);
ds_list_add(axis_strings,LANG_SELECT_LEFT,LANG_SELECT_RIGHT);

ds_list_add(vib_values,false,true);
ds_list_add(vib_strings,LANG_SELECT_ON,LANG_SELECT_OFF);

keybind_text = ds_list_create();

key_ids = ds_list_create();

ds_list_add(key_ids,"","","","select","cancel","up","down","left","right","open_menu","control","run","bookmark","sight","coordinates","help","hud","screenshot","exit")

menuOverworldKeyboardSetKeybindText(keybind_text);

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();

menu_draw = menuOverworldOptionControllerDraw

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

//menu_up = menuOverworldOptionRealUp;
//menu_down = menuOverworldOptionRealDown;

menu_left =  menuOverworldOptionsControllerLeft;
menu_right = menuOverworldOptionsControllerRight;

curposs[0] = global.gamepadRightAxis;
curposs[1] = global.gamepadVibration;

wrapz[0] = true;
wrapz[1] = true;
