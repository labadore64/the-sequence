
	var chara = argument[0];
	var targe = argument[1];

	var battletext = moveGetBattleText(attack);

	var stringer = battletext[|0];

	stringer = string_replace_all(stringer,"@0",chara.name);
	stringer = string_replace_all(stringer,"@1",targe.name);

	battleAttackInputAddString(stringer);
	
	ds_list_destroy(battletext );

	var map = ds_map_create();
	ds_map_add(map,BATTLE_STAT_MAGIC,2);
	ds_map_add(map,BATTLE_STAT_ATTACK,2);

	move_stat_boost_generic(chara,chara,map,2)
