draw_set_color(global.textColor);
draw_set_font(global.normalFont);

var linecounter = 0;
var line_place = 0;

var sizer = 60;
var xspacer = 60;
var yspacer = 60;
var x_base = 120;
var y_base = 200+25;

var line_len = 400;
var line_hei = 5;
var xx = 200;
var yy = 150+25;

draw_set_alpha(.45)


draw_rectangle_ratio(xx,yy+line_hei,xx+line_len,yy+(line_hei*2),false)

draw_set_alpha(1)

draw_rectangle_ratio(xx,yy,xx+line_len,yy+line_hei,false)

for(var i = 0; i < sizer; i++){
	
	draw_text_transformed_ratio(x_base+xspacer*line_place,y_base+yspacer*linecounter,letters[|i],3,3,0)

	if(i == char_menupos){
		draw_sprite_extended_ratio(spr_text_select,counter,x_base+xspacer*(line_place+.25*.5),y_base+yspacer*(linecounter+.25)-3,1,1,0,c_white,1)
	}
	line_place++;
	if(line_place == menu_per_line){
		line_place = 0;
		linecounter++;
	}
}

draw_set_halign(fa_center)
draw_text_transformed_ratio(400,yy-150,"Your name?",4,4,0)
draw_set_halign(fa_left)

draw_set_font(global.largeFont)

draw_text_transformed_ratio(xx+10,yy-80,current_text,4,4,0)

draw_set_font(global.normalFont);