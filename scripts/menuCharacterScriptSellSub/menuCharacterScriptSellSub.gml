itemData(item_id[|menupos])

if(can_sell){
	//do sell
	menuCharacterScriptSellQuantity();
} else {
	soundfxPlay(soundMenuInvalid);	
}

var m_option = menu_option;
var m_pos = menupos

with(menuCharacterShop){
	sell_menu_option = m_option;
	sell_menupos = m_pos
}