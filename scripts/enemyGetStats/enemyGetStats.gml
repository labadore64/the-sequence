	
sprite_idle_front = -1
sprite_idle_back = -1
sprite_hit_front = -1
sprite_hit_back = -1
sprite_jump0_front = -1
sprite_jump0_back = -1
sprite_jump1_front = -1
sprite_jump1_back = -1
sprite_cast0_front = -1
sprite_cast0_back = -1
sprite_cast1_front = -1
sprite_cast1_back = -1


var stat_phy_power = 1;
var stat_phy_guard = 1;
var stat_mag_power = 1;
var stat_mag_guard = 1;
var stat_spe_power = 1;
var stat_spe_guard = 1;
var stat_hp = 1;
var stat_mp = 1;

var jumper = true;

var ccc = argument[0]

//put calculations here

if(!is_undefined(ccc)){
	if(ccc >= 0){
		var itemcount = ccc;
		
		shadow_y = objdata_enemy.shadow_y[itemcount];

		ai_script =  objdata_enemy.ai_script_id[itemcount];
		ai_target_script = objdata_enemy.ai_script_target_id[itemcount];

		phy_power.base = objdata_enemy.stat_phy_power[itemcount];
		phy_guard.base = objdata_enemy.stat_phy_guard[itemcount];
		mag_power.base = objdata_enemy.stat_mag_power[itemcount];
		mag_guard.base = objdata_enemy.stat_mag_guard[itemcount];
		spe_power.base = objdata_enemy.stat_spe_power[itemcount];
		spe_guard.base =  objdata_enemy.stat_spe_guard[itemcount];
		hp.base = objdata_enemy.stat_hp[itemcount];
		mp.base = objdata_enemy.stat_mp[itemcount];
		character_scale = objdata_enemy.sprite_scale_factor[itemcount];

		var elementz = objdata_enemy.type[itemcount] 

		color = objdata_enemy.color[itemcount];
		name = objdata_enemy.name[itemcount]
		ax_desc = objdata_enemy.ax_desc[itemcount]

		for(var i = 0; i < 3; i++){
			flavor_text[i]	= objdata_enemy.flavor_text[itemcount,i];
		}
		experience = objdata_enemy.experience[itemcount]

		sprite_default_speed = objdata_enemy.sprite_default_speed[itemcount]

		jump_hit = objdata_enemy.jumper[itemcount]
		can_move = objdata_enemy.can_move[itemcount]

		//item drops
		drop_normal = objdata_enemy.drop_normal[itemcount]
		drop_bribe = objdata_enemy.drop_bribe[itemcount]
		drop_megabribe = objdata_enemy.drop_megabribe[itemcount]
		drop_special = objdata_enemy.drop_special[itemcount]

		bribe = objdata_enemy.bribe[itemcount]
		ability = objdata_enemy.ability[itemcount]

		var min_money = objdata_enemy.min_money[itemcount]
		var max_money = objdata_enemy.max_money[itemcount]

		if(min_money < 0){
			min_money = 0;	
		}

		if(max_money < 0){
			max_money = 0;	
		}

		talk_x = objdata_enemy.talk_x[itemcount]
		talk_y = objdata_enemy.talk_y[itemcount]

		money = irandom_range(min_money,max_money);

		sprite_idle_front = objdata_enemy.sprite_idle_front[itemcount]
		sprite_idle_back = objdata_enemy.sprite_idle_back[itemcount]
		sprite_hit_front = objdata_enemy.sprite_hit_front[itemcount]
		sprite_hit_back = objdata_enemy.sprite_hit_back[itemcount]
		sprite_jump0_front = objdata_enemy.sprite_jump0_front[itemcount]
		sprite_jump0_back = objdata_enemy.sprite_jump0_back[itemcount]
		sprite_jump1_front = objdata_enemy.sprite_jump1_front[itemcount]
		sprite_jump1_back = objdata_enemy.sprite_jump1_back[itemcount]
		sprite_cast0_front = objdata_enemy.sprite_cast0_front[itemcount]
		sprite_cast0_back = objdata_enemy.sprite_cast0_back[itemcount]
		sprite_cast1_front = objdata_enemy.sprite_cast1_front[itemcount]
		sprite_cast1_back = objdata_enemy.sprite_cast1_back[itemcount]

	}
}

element = stringToType(elementz);
character = ccc;

//end calculations