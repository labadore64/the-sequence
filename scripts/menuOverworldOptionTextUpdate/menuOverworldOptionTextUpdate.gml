//updates the option based on direction.
//-1 = left, 1 = right
var list_to_check = -1;
var curpos = 0;

if(menupos == 0){
	//speed
	list_to_check=text_speed_strings;
	curpos = global.textSpeed+1;
} else if(menupos == 1){
	//color	
	list_to_check=text_color_strings;
	curpos = ds_list_find_index(text_color_values,global.textColor);
	if(curpos == -1){
		curpos = 0;	
	}
} else if (menupos == 2){
	//font	
	list_to_check = text_font_strings;
	curpos = ds_list_find_index(text_font_values,global.textFont);
	if(curpos == -1){
		curpos = 0;	
	}
} else if (menupos == 3){
	//sound	
	list_to_check = text_sound_strings;
	curpos = global.voice;
} else if (menupos == 4){
	//sound	
	list_to_check = text_metric_strings;
	curpos = global.metric;
} else if (menupos == 5){
	//sound	
	list_to_check = text_tts_strings;
	curpos = global.tts;
} else if (menupos == 6){
	list_to_check = text_tts_strings;
	curpos = global.tutorial;
}

var directionz = argument[0];
var list_size = ds_list_size(list_to_check);

if(directionz == -1){
	//if moving left	
	curpos--;
	if(curpos < 0){
		if(wrapz[menupos]){
			curpos = list_size-1;
		} else {
			curpos = 0;	
		}
	}
} else if (directionz == 1){
	//if moving right	
	curpos++;
	if(curpos > list_size-1){
		if(wrapz[menupos]){
			curpos = 0;	
		} else {
			curpos = list_size - 1;	
		}
	}
}

/*
text_speed_strings
text_color_strings
text_font_strings
text_sound_strings
*/

//finally set values accordingly
if(menupos == 0){
	//speed
	global.textSpeed = curpos-1;
	curposs[0] = curpos;
	tts_say(text_speed_strings[|curpos])
} else if(menupos == 1){
	//color	
	global.textColor = text_color_values[|curpos];
	curposs[1] = curpos;
	tts_say(text_color_strings[|curpos])
} else if (menupos == 2){
	//font	
	global.textFont = text_font_values[|curpos]
	curposs[2] = curpos;
	tts_say(text_font_strings[|curpos])
} else if (menupos == 3){
	global.voice = curpos;
	curposs[3] = curpos;
	tts_say(text_sound_strings[|curpos])
} else if (menupos == 4){
	global.metric = curpos;
	curposs[4] = curpos;
	tts_say(text_sound_strings[|curpos])
} else if (menupos == 5){
	global.tts = curpos;
	curposs[5] = curpos;
	if(global.tts || access_mode()){
		tts_say(text_tts_strings[|curpos])
	} else {
		tts_stop();		
	}
}  else if (menupos == 6){
	global.tutorial = curpos;
	curposs[6] = curpos;
	tts_say(text_tts_strings[|curpos])
}

menuParentUpdateLookAndFeel()

menuOverworldOptionInfokeyStrings();

soundfxPlay(sound_select)