// set up array. by default all keys are restricted from each other
for(var i = 0; i < KEYBOARD_KEY_TOTAL; i++){
	for(var j = 0; j < KEYBOARD_KEY_TOTAL; j++){
		global.keybind_restrict[i,j] = true;
	}
}

// set up definitions
// format:
// global.keybind_restrict[KEY,KEY_TO_COMPARE]
// if true, it means the two cannot share the same binding.

// outside of the exception keys, these keys
// are basically bindable to anything.
for(var i = 0; i < KEYBOARD_KEY_TOTAL; i++){

	
	if(i > KEYBOARD_KEY_OPEN_MENU){
		// these keys can be bound to anything that is higher than the Open Menu key
		keybind_restrict_value(i,KEYBOARD_KEY_RUN,false)
		keybind_restrict_value(i,KEYBOARD_KEY_ACCESSMENU,false)
		keybind_restrict_value(i,KEYBOARD_KEY_BOOKMARK,false)
		keybind_restrict_value(i,KEYBOARD_KEY_SIGHT,false)
		keybind_restrict_value(i,KEYBOARD_KEY_COORDINATES,false)
		
		keybind_restrict_value(i,KEYBOARD_KEY_SCOPEFUNC1,false)
		keybind_restrict_value(i,KEYBOARD_KEY_SCOPEFUNC2,false)
		
		// access keys and function keys can't be bound to each other
		if((i >= KEYBOARD_KEY_ACCESS1 && i <= KEYBOARD_KEY_ACCESS0) ||
			i >= KEYBOARD_KEY_EXIT && i <= KEYBOARD_KEY_SCREENSHOT){
			for(var j = KEYBOARD_KEY_ACCESS1; j <= KEYBOARD_KEY_ACCESS0; j++){
				keybind_restrict_value(i,j,true)	
			}
			for(var j = KEYBOARD_KEY_EXIT; j <= KEYBOARD_KEY_SCREENSHOT; j++){
				keybind_restrict_value(i,j,true)	
			}
		}
		
		
	} else {
		keybind_force_value(i,KEYBOARD_KEY_RUN,false)
		keybind_force_value(i,KEYBOARD_KEY_ACCESSMENU,false)
		keybind_force_value(i,KEYBOARD_KEY_BOOKMARK,false)
		keybind_force_value(i,KEYBOARD_KEY_SIGHT,false)
		keybind_force_value(i,KEYBOARD_KEY_COORDINATES,false)
		
		keybind_force_value(i,KEYBOARD_KEY_SCOPEFUNC1,false)
		keybind_force_value(i,KEYBOARD_KEY_SCOPEFUNC2,false)
	}
}