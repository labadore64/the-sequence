//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);
stringer = string_replace_all(stringer,"@1",targe.name);
stringer = string_replace_all(stringer,"@2",bribe);

battleAttackInputAddString(stringer);

//
if(!BattleHandler.battle_sim){
	targe.money += bribe;

	BattleHandler.money -= bribe;
}

if(targe.money >= targe.bribe){
	//successful bribe	
	var stringer = battletext[|1];

	stringer = string_replace_all(stringer,"@0",targe.name);

	battleAttackInputAddString(stringer);
	if(targe.money < (targe.bribe * 3)){
		targe.trigger_drop_bribe = true;
	} else {
		targe.trigger_drop_megabribe = true;	
	}
	
	targe.exp_multiplier = .66;
	targe.bribed = true;
	
	with(targe){
		trigger_disappear_anim = true;
		hp.current = 0;
	}
	//defeat target.
} else if (BattleHandler.battle_sim){
	var stringer = battletext[|3];

	stringer = string_replace_all(stringer,"@0",targe.name);

	battleAttackInputAddString(stringer);	
} else {
	//failed bribe	
	var stringer = battletext[|2];

	stringer = string_replace_all(stringer,"@0",targe.name);

	battleAttackInputAddString(stringer);
}



ds_list_destroy(battletext);