//if(access_mode){
	if(objGenericCanDo()){
		if(!keypress_this_frame){
			if(infokey_script > -1){
				for(var i = 0; i < 10; i++){
					if(global.key_state_array[KEYBOARD_KEY_ACCESS1 + i] == KEY_STATE_PRESS ||
						global.gamepad_state_array[KEYBOARD_KEY_ACCESS1 + i] == KEY_STATE_PRESS){
						script_execute(infokey_script,i);
					}
				}
			} else {
				if(infokeys_loaded){
					with(HUDController){
					//only allow infokeys to appear in blind mode.
						for(var i = 0; i < infokey_size; i++){
							//only check keys that have infokey strings set	
							if(global.key_state_array[KEYBOARD_KEY_ACCESS1 + i] == KEY_STATE_PRESS ||
								global.gamepad_state_array[KEYBOARD_KEY_ACCESS1 + i] == KEY_STATE_PRESS){
								infokey_trigger(i);
					
								break;
							}
						}
					}
				} else {
					var thisobj = id;
					with(HUDController){
					//only allow infokeys to appear in blind mode.
						for(var i = 0; i < 10; i++){
							//only check keys that have infokey strings set	
							if(global.key_state_array[KEYBOARD_KEY_ACCESS1 + i] == KEY_STATE_PRESS ||
								global.gamepad_state_array[KEYBOARD_KEY_ACCESS1 + i] == KEY_STATE_PRESS){
								with(thisobj){
									menupos = i;
							
									if(menupos >= menu_size){
										menupos = menu_size-1;	
									}
									menuParentUpdate();
								}
								break;
							}
						}
					}
				}
			}
		}
	}
//}