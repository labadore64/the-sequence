with(BattleHandler){
	var val = argument[0];

	var index = 0;

	if(val < 10000){
		//set the index.	
		index = val;
	} else {
		if(ds_exists(party_list,ds_type_list)){
			index = ds_list_find_index(party_list,val);
		}
	}

	if(index > -1){
		if(ds_exists(allies,ds_type_list)){
			var saver = index;

			index = ds_list_find_index(allies,ds_list_find_value(party_list,index));
	
			if(index <= -1){
				index = saver;
			}
				character_select[index] = argument[1];
		}
	}
}