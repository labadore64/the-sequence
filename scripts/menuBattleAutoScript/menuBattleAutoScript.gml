var check_talking = false;

with(Battler){
	if(is_enemy){
		if(battlerHasFilter(id,"Talk")){
			check_talking = true;	
		}
	}
}
if(!check_talking){
	with(BattleHandler){
		var sizer = ds_list_size(party_list);
		var getauto = objdataToIndex(objdata_moves,"attack");
		var gettarget = -1;
		var chara = -1;
		for(var i = 0; i < sizer; i++){
			chara = party_list[|i];
			
			getauto = script_execute(chara.ai_script,chara)


			gettarget = script_execute(chara.ai_target_script,getauto,chara);
			moveData(getauto);
			if(move_type == MOVE_TYPE_PHYSICAL){
				if(battlerHasFilter(chara,"lock")){
					getauto = objdataToIndex(objdata_moves,"none");	
				}
			} else {
				if(battlerHasFilter(chara,"mute")){
					getauto = objdataToIndex(objdata_moves,"none");	
				}	
			}
			if(!battlerHasFilter(chara,"repeat")){
				if(target_ally){
					battleAttackInputCreate(chara,party_list[|floor(clamp(gettarget,0,2))],getauto);	
				} else {
					battleAttackInputCreate(chara,enemy_list[|floor(clamp(gettarget,0,2))],getauto);	
				}
			}
		}
	}
}

battleAttackStartScript()