//list of types used in cutscenes.

#macro CUTSCENE_TYPE_NONE 0						//does nothing and will return the signal to push to
												//the next cutscene element. think of it like a nop
												//any value not defined here will be treated like NONE.

#macro CUTSCENE_TYPE_DIALOG 1					//represents dialog that can change happiness values/learn moves/ect,
												//involves interaction with the player.

#macro CUTSCENE_TYPE_TEXT 2						//represents a text box. Text boxes can have characters assigned
												//to them like dialog but involve no interaction.

#macro CUTSCENE_TYPE_MOVE_CHARACTER 3			//represents moving characters and sprites.
												//You should be able to move multiple characters at a time.

#macro CUTSCENE_TYPE_MOVE_CAMERA 4				//represents moving the camera.

#macro CUTSCENE_TYPE_MOVE_CHARACTER_AND_CAMERA 5 //represents moving characters and a camera. 
												//So 3 and 4 combined. 
												
#macro CUTSCENE_TYPE_GRAPHIC 6			//Represents displaying a graphic
#macro CUTSCENE_TYPE_RECRUIT 7					//recruits a character, sets a flag. Since recruitment happens
												//frequently, it's just its own type.
												
#macro CUTSCENE_TYPE_ITEM_GET 8					//displays a textbox that tells the player they received an item.

#macro CUTSCENE_TYPE_BATTLE 9						//triggers a battle. The cutscene object stays active during the battle. 
												//When the battle completes, if you won, it will reload a new event queue
												//based on the args. Otherwise it will continue the current queue.
												//Flee is disabled in cutscene battles.
												
#macro CUTSCENE_TYPE_CHECK_EVENT 10					//Checks a generic event type (can be item/dialog/event). If it's true, it
												//reloads the queue for a new cutscene. If its false, it continues this
												//cutscene.
												
#macro CUTSCENE_TYPE_CHECK_PARTY 11					//Checks if you have a certain character in your party. If it's true, it
												//reloads the queue for a new cutscene. If it's false, it continues this
												//cutscene.
												
#macro CUTSCENE_TYPE_CHECK_ITEM 12					//checks if you have a certain item at all in the current save file
												//This includes items stored in the library or storage facility.
												
#macro CUTSCENE_TYPE_CHECK_INVENTORY 13				//checks if you have an item in your current held inventory.
												
#macro CUTSCENE_TYPE_MUSIC 14					//Just plays music. The music variable is if
												//you want to have music with any other type.

#macro CUTSCENE_TYPE_WAIT 15							//Waits a certain number of steps.

#macro CUTSCENE_TYPE_SOUNDFX 16						//plays a sound effect.

#macro CUTSCENE_TYPE_EVENT_SET 17				//sets an event.
												
#macro CUTSCENE_TYPE_SCRIPT 18						//Executes a defined script. arg script should be the name of the script
												//in gamemaker. The rest of the args are passed as the arguments to the script.
												//technically you can do any of the above items with this but it's more optimized
												//to use the above IDs instead of having everything be defined scripts.
												
#macro CUTSCENE_TYPE_EXIT 19							//Immediately empties the queue, so that the cutscene just ends.
												//I can't imagine this actually being used but it makes it easy to check
												//which functions are well defined
																								
//macros that correlate with certain byte arrays representing certain parts of the file.

//file start
#macro C_START_FILE1 0x69
#macro C_START_FILE2 0x69
#macro C_START_FILE3 0x69
#macro C_START_FILE4 0x69

//start of address header
#macro C_START_ADDRESS_HEADER1 0x69
#macro C_START_ADDRESS_HEADER2 0x69
#macro C_START_ADDRESS_HEADER3 0x42
#macro C_START_ADDRESS_HEADER4 0x00

//EOF
#macro C_EOF1 0x19
#macro C_EOF2 0x79
#macro C_EOF3 0x07
#macro C_EOF4 0xBB

//start anotes
#macro C_ANNOTE1 0xAB
#macro C_ANNOTE2 0xBA
#macro C_ANNOTE3 0xAB
#macro C_ANNOTE4 0xBA

//argument separator
#macro C_ARG1 0x80
#macro C_ARG2 0x81
#macro C_ARG3 0x82
#macro C_ARG4 0x80

//start command
#macro C_START_COMMAND1 0xE7
#macro C_START_COMMAND2 0xE7
#macro C_START_COMMAND3 0xE8
#macro C_START_COMMAND4 0xE8

//end header
#macro C_END_COMMAND1 0xEF
#macro C_END_COMMAND2 0xEF
#macro C_END_COMMAND3 0xE0
#macro C_END_COMMAND4 0xE0

//annotes
#macro C_ANNOTE_COCURRENT 0x11
#macro C_ANNOTE_BACKGROUND 0x14