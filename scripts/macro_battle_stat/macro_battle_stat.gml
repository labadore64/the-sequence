#macro BATTLE_STAT_ATTACK 0
#macro BATTLE_STAT_DEFENSE 1
#macro BATTLE_STAT_MAGIC 2
#macro BATTLE_STAT_RESISTANCE 3
#macro BATTLE_STAT_AGILITY 4
#macro BATTLE_STAT_MP_RECOVER 5

#macro BATTLE_STAT_EXP_TOTAL 5000

//classes
//classes multiply 3 of the stats by a certain amount and each class represents what 3 stats it affects

#macro BATTLE_CLASS_NONE 0
#macro BATTLE_CLASS_PHYSICAL_SPEED 1
#macro BATTLE_CLASS_MAGIC_SPEED 2
#macro BATTLE_CLASS_PHYSICAL_MPR 3
#macro BATTLE_CLASS_MAGIC_MPR 4
#macro BATTLE_CLASS_OFFENSIVE_SPEED 5
#macro BATTLE_CLASS_DEFENSIVE_SPEED 6
#macro BATTLE_CLASS_OFFENSIVE_MPR 7
#macro BATTLE_CLASS_DEFENSIVE_MPR 8
#macro BATTLE_CLASS_PHYS_POWER_SPEED 9
#macro BATTLE_CLASS_PHYS_GUARD_SPEED 10
#macro BATTLE_CLASS_MAG_POWER_SPEED 11
#macro BATTLE_CLASS_MAG_GUARD_SPEED 12

#macro BATTLE_CLASS_BONUS 75

#macro BATTLE_STAT_MULTIPLIER_TABLET 4
#macro BATTLE_STAT_MULTIPLIER_ITEM 2