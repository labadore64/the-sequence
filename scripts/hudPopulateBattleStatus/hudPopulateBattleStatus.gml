// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from

if(!is_undefined(obj)){
	if(instance_exists(obj)){
		if(obj.object_index == battleStatusEffect){
			// name and character
			hudControllerAddData(entryname + "name",string(obj.name));
			hudControllerAddData(entryname + "desc",string(obj.flavor));
			hudControllerAddData(entryname + "turns",string(obj.turns));
		}
	}
}