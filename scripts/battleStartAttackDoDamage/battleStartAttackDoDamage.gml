var attacker = argument[0];
var targ = argument[1];
var stat_power = argument[2];
var stat_guard = argument[3];
var attack_type = argument[4];

attacker.damage_type_multiplier = 1;
targ.damage_type_multiplier = 1;

damage_type_multiplier = 1;
damage_attack_multiplier = 1;

var final_multiplier = 1;

if(stat_guard = 0){
	stat_guard = 1;	
}

var STABbonus = 1;

if(attack_type != -1){
	//if there is an attack type

	//multipliers
	var type_attack_multiplier = moveTypeAttackMultiplier(attack_type,targ.element);

	final_multiplier = type_attack_multiplier

	// if you use a move of the same type as you, you get a bonus
	if(attack_type == attacker.element){
		STABbonus = 1.2
	}

	//set end values
	damage_attack_multiplier = 1;
	damage_type_multiplier = type_attack_multiplier
	targ.damage_type_multiplier = damage_type_multiplier;
}

var effect_multiplier = battleStartAttackAdjustDamage(attacker,targ)

//damage calculation

var attack_strength = (((stat_power)/(stat_guard))*(clamp(damage/50,.05,1)))*100*damage_type_multiplier*STABbonus*effect_multiplier;

var randomizer = random_range(.85,1.1);

var final_dmg = battleStartAttackChangeDamage(attacker,targ,attack_strength*randomizer);

return clamp(ceil(final_dmg),1,9999);