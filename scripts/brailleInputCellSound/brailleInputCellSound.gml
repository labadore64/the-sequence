	if(global.BrailleSound){
	
		if(playing_sound != -1){
			audio_stop_sound(playing_sound);
		}
		playing_sound = audio_play_sound(cells_sound[column_pos,menupos],2,false);
	
		if(cells[column_pos,menupos]){
			audio_sound_pitch(playing_sound,1.25)	
		} else {
			audio_sound_pitch(playing_sound,1)	
		}
	
	} else {
		soundfxPlay(soundMenuDefaultMove)
	}