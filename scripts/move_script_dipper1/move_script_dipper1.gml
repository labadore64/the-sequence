//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);

battleAttackInputAddString(stringer);

var stringer = battletext[|1];

battleAttackInputAddString(stringer);

var color = choose($000000, $FF0000, $00FF00, $0000FF, $FFFF00, $FF00FF, $00FFFF, $FFFFFF);

battleAuraColorAdd(color,3);

ds_list_destroy(battletext);