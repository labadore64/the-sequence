/// @description - returns adjusted MP stat

var minvalue = 10;

var multiplier = getMultiplierFromLevel(objdata_character.mp_grow[character],level);

var returner = ceil((minvalue + multiplier * mp_base));

return returner;