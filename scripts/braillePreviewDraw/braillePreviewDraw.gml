
	brailleDisplayDraw(argument[0]);	

	var width = x_spacing*row_length*.5

	draw_set_color(global.textColor)
	draw_set_halign(fa_center)
	var text_posx = x+width;
	var text_posy = y+100;

	if(directional){
		draw_text_transformed_ratio(text_posx+90,text_posy-135,add_string,3,3,0);
		draw_sprite_extended_ratio(spr_arrow_point_left,sprite_counter,text_posx,text_posy-120,1,.5,0,global.textColor,1);
	} else {
		draw_text_transformed_ratio(text_posx-80,text_posy-135,add_string,3,3,0);
		draw_sprite_extended_ratio(spr_arrow_point_left,sprite_counter,text_posx,text_posy-120,-1,.5,0,global.textColor,1);
	}
