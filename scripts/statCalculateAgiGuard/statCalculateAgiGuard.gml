/// @description - returns adjusted AGIGUARD stat

var minvalue = 1;

var multiplier = getMultiplierFromLevel(objdata_character.spe_guard_grow[character],level);

var returner = ceil((minvalue + multiplier * (spe_mpr_base+boost_spe_mpr)));

returner += statCalculateBrailleMPBoost(multiplier, argument[0]);

return returner;