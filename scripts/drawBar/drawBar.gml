/// @description - draw a box
/// @param x
/// @param y
/// @param width
/// @param height
/// @param percent
/// @param color
	
	var xx = argument[0];
	var yy = argument[1];
	var width = argument[2];
	var height = argument[3];
	var percent = argument[4];
	var colorrr = argument[5]
	
	draw_set_color($111111)
	draw_rectangle_ratio(xx- width-2,
					yy- height-2,
					xx + width+2,
					yy + height+2,
					false)
				
	draw_set_color(c_black)

	draw_rectangle_ratio(xx- width,
					yy - height,
					xx + width,
					yy + height,
					false)
					
	//var getcol = floor(chaa.exp_percent * 255)
	//draw_set_color(make_color_hsv(getcol,255,255))
	if(percent > 0){
	draw_set_color(colorrr)
	draw_rectangle_ratio(xx- width,
					yy - height,
					clamp(xx - width + width*percent*2,xx- width,xx + width),
					yy+ height,
					false)
	}