
if(ds_exists(menu_option_names,ds_type_list)){
	ds_list_destroy(menu_option_names)	
}

if(ds_exists(other_list,ds_type_list)){
	ds_list_destroy(other_list)	
}

if(ds_exists(equip_list,ds_type_list)){
	ds_list_destroy(equip_list)	
}

if(ds_exists(heal_list,ds_type_list)){
	ds_list_destroy(heal_list)	
}

if(ds_exists(battle_list,ds_type_list)){
	ds_list_destroy(battle_list)	
}

if(ds_exists(field_list,ds_type_list)){
	ds_list_destroy(field_list)	
}

if(ds_exists(other_list_quant,ds_type_list)){
	ds_list_destroy(other_list_quant)	
}

if(ds_exists(equip_list_quant,ds_type_list)){
	ds_list_destroy(equip_list_quant)	
}

if(ds_exists(heal_list_quant,ds_type_list)){
	ds_list_destroy(heal_list_quant)	
}

if(ds_exists(battle_list_quant,ds_type_list)){
	ds_list_destroy(battle_list_quant)	
}

if(ds_exists(field_list_quant,ds_type_list)){
	ds_list_destroy(field_list_quant)	
}

if(ds_exists(book_list,ds_type_list)){
	ds_list_destroy(book_list)	
}

if(ds_exists(book_list_quant,ds_type_list)){
	ds_list_destroy(book_list_quant)	
}

if(ds_exists(food_list,ds_type_list)){
	ds_list_destroy(food_list)	
}

if(ds_exists(food_list_quant,ds_type_list)){
	ds_list_destroy(food_list_quant)	
}

if(ds_exists(craft_list,ds_type_list)){
	ds_list_destroy(craft_list)	
}

if(ds_exists(craft_list_quant,ds_type_list)){
	ds_list_destroy(craft_list_quant)	
}

if(ds_exists(decor_list,ds_type_list)){
	ds_list_destroy(decor_list)	
}

if(ds_exists(decor_list_quant,ds_type_list)){
	ds_list_destroy(decor_list_quant)	
}


if(ds_exists(list_of_lists,ds_type_list)){
	ds_list_destroy(list_of_lists)	
}

if(ds_exists(list_of_lists_quant,ds_type_list)){
	ds_list_destroy(list_of_lists_quant)	
}