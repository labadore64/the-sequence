// argument0 brightness
// argument1 contrast

with(CameraOverworld){
	shd_bc_var_brightness_amount = argument[0];
	shd_bc_var_contrast_amount = argument[1]
}