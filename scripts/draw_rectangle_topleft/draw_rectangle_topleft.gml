var posx = argument[0]*global.scale_factor;
var posy = argument[1]*global.scale_factor;
var width = argument[2]*global.scale_factor
var length = argument[3]*global.scale_factor


draw_rectangle(global.display_x 
				+posx,
				global.display_y 
				+posy,
				global.display_x 
				+posx+width,
				global.display_y
				+posy+length,
				false);