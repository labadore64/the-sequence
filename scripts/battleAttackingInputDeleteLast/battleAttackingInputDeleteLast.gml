
	//returns how many attacks a character has.
	//var char = argument[0]

	var lister = -1;

	with(BattleHandler){
		lister = attack_input;	
	}

	var sizer = ds_list_size(lister);

	var obj = -1;

	var update_stuff = false;

	obj = lister[|sizer-1];
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			with(obj){
				instance_destroy();	
			}
		}
	}

	ds_list_delete(lister,sizer - 1);

	with(menuBattleFight){
		if(character > 0){

			ds_list_clear(menu_name); //name of all menu items
			ds_list_clear(menu_description); //descriptions of all menu items
			ds_list_clear(menu_script); //scripts for all menu items
		
			menuBattleFightPopulateItems()
		
			menu_size = menuGetSize();	

			menupos = 0;
		}
	}


