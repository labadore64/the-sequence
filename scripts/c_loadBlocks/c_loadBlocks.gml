var currentheader = noone;
var eof = false;

var arrayOfStuff;

file_bin_seek(file,8)
arrayOfStuff[0] = 1;

var inst;
while(arrayOfStuff != -1){
	inst = c_createBlock();
	arrayOfStuff = inst.commands;
	ds_list_add(block_list,inst);
	file_bin_seek(file,file_bin_position(file)+3)
}

//delete last two headers because they're not actually headers (first is the addresses, last is null)
repeat(1){
	if(ds_list_size(block_list) >= 1){
		var possi = ds_list_size(block_list)-1;
		var inst = ds_list_find_value(block_list,possi);
		with(inst){
			instance_destroy();	
		}
	}
}

//we got everything we need from the file so we can close it now.
file_bin_close(file);

//now that the headers are set up, start the program.
c_block_start();