if(!lock){
	textbox_hold_counter = 0;
	battleStartAttackRemoveAttack();

	if(!BattleHandler.finish){
	if(active){
	if(current_attack != -1){

		if(current_attack.done){
			battleZoomDefault(20)
			battleAnimReturnAll();
		
			newalarm[0] = 16;
			active = false;
		
			var currr = current_attack;
			
			if(instance_exists(brailleDisplayBattle)){
				if(wordTestInString(brailleDisplayBattle.display_string)){
					with(brailleDisplayBattle){
						if(word_move > -1){
							var mov = word_move;
							with(battleStartAttack){
								var ha = instance_create(0,0,battleAttackInput);

								ha.character = currr.character;
								ha.target = currr.target
								ha.attack = mov;
			
								ds_list_insert(priority[3],0,ha)

							}
						}
						var striner = "";
					
						for(var i = 0; i < 5; i++){
							striner += demo[irandom(25)]
						}
					
					
						brailleDisplayReplace(striner,id)
					}
				}
			}
		} else {
			//move along the battle start attack
			if(current_attack.state == -1 && current_attack.state != BATTLE_PHASE_STATE_BRAILLE){
				// checks if they have braille loaded.
				if(string_length(current_attack.braille_string) != 0){
						var braille = instance_create(0,0,brailleInputQueue);
						tts_say("Braille Attack!")
						var waiter = 60;
						braille.braille_attack = true;
						braille.newalarm[11] += waiter;
						braille.newalarm[2] += waiter;
						braille.input_string = current_attack.braille_string;
						braille.display_ready_message = true
						current_attack.braille_string = "";
				
						braille.move_input = id;
				
						current_attack.state = BATTLE_PHASE_STATE_BRAILLE;
				} else {
			
					current_attack.state = BATTLE_PHASE_STATE_ANIMATE;	
					current_attack.newalarm[1] = current_attack.animation_length;
		
					//checks if they have MP.
					with(current_attack){
						
						if(braille_string_base != ""){
							var len = string_length(braille_string_base);
							mp_cost = floor(menuBattleTabletCalculateMPCostMultiplier(len)
									*character.mp.base)
							BattleHandler.braille_load_count = len-1;
							if(braille_left){
								BattleHandler.braille_insert_direction = 0;
							} else {
								BattleHandler.braille_insert_direction = 1
							}
						}
				
						if(!battleStartAttackMPCanDo()){
							attack = objdataToIndex(objdata_moves,"defend")
							moveData(attack);
						} else if (battlerHasFilter(character,"panic") && (random(1) < .5)){
					
							var stringer = "@0 panicked!";
	
							stringer = string_replace_all(stringer,"@0",name);
							battleTextDisplayAddText(stringer);
					
							attack = objdataToIndex(objdata_moves,"defend")
							moveData(attack);
						} else if (battlerHasFilter(character,"baffle") && (random(1) < .333)){
							//	
							target = character;
					
							attack = objdataToIndex(objdata_moves,"hurt self")
							moveData(attack);
						} else if(character.flinch){
							target = character;
					
							attack = objdataToIndex(objdata_moves,"flinch")
							moveData(attack);
						} else {
							if(mp_cost > 0){
						
								if(battlerHasFilter(character,"ether")){
									mp_cost = 0;	
								}
						
								if(battlerHasFilter(character,"cough")){
									mp_cost = mp_cost * 2;	
								}
						

								var change_mp = character.mp.current - mp_cost;

						
								if(change_mp < 0){
									change_mp = 0;	
								}
								battlerMPChange(character,change_mp);

							}
						}

					}
					BattleHandler.is_talking = false;

					battleStartAttackAnimatePhase();
					with(current_attack){
						if(damage <= 0 && animation_complete){
							state = BATTLE_PHASE_STATE_TEXT
							battleStartAttackTextPhase();	
						} else {
						}
					}
				}
			} else if(current_attack.state == BATTLE_PHASE_STATE_ANIMATE){
				if(!global.battleAnimate){
					current_attack.animation_complete = true;
				}
			
				if(battleAnimSpriteFinished() && (current_attack.animation_complete || current_attack.newalarm[1] == -1)){
					with(current_attack){
						with(Battler){
							if(trigger_disappear_anim){//){defeat_animation_trigger){
								trigger_disappear_anim = false;
								defeat_animation_trigger = true;
							}
						}
						with(Battler){
							if(death_zoom != -1){
								battlerDeathZoom(death_zoom);	
								death_zoom = -1;
							}
						}
						death_zoom = -1;
						animation_complete = true;
						state = BATTLE_PHASE_STATE_TEXT
						battleStartAttackTextPhase();	
					
					
					}
				}
		
			}
		}
	
		if(current_attack.state == BATTLE_PHASE_STATE_TEXT){
			battleTextDisplayAXRead();
		}
	
	} else {
		//complete the attacks
		battleAnimReturnAll();
		instance_destroy();
	}
	}
	}
}
	updateBattleMainHUD();

