gml_pragma("forceinline");

if(debug){
	
	//update all transformations
	
	var x1upd = stretch[0].x_delta;
	var x2upd = stretch[1].x_delta;
	var y1upd = stretch[0].y_delta;
	var y2upd = stretch[1].y_delta;

	x1+=x1upd;
	x2+=x2upd;
	y1+=y1upd;
	y2+=y2upd;
	
	var gradupdate = stretch[2].y_delta;
	gradient_height+=gradupdate
	
	//updates internal values for copying
	draw_width = x2-x1; //width of box in pixels
	draw_height = y2-y1; //height of box in pixels
	if(draw_centered){
		x = x1 + draw_width*.5;
		y = y1 + draw_height*.5;
	} else {
		x = x1;
		y = y1;
	}
	
	//gets debug values if in range
	if(mouse_check_button_pressed(mb_right)){
		menuParentGetDebugValues();	
	}
	
}