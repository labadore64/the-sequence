if(audio_sound > 0){
    audio_emitter_falloff(emit, falloff_min, falloff_max , falloff_factor);
    audio_emitter_position(emit, x, y, 0);
    audio_stop_sound(emit_sound)
    emit_sound = audio_play_sound_on(emit, audio_sound, loop, 1);
	audio_sound_set_track_position(emit_sound, random(audio_sound_length(emit_sound)))
}

