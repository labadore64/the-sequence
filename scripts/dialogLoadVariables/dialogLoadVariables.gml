var filename = argument[0]

ini_open(filename)

var dialogsize = dialogGetSize("dialog","dialog");
var optionsize = dialogGetSize("option","dialog");

//dialog
dialogReadIni(dialog_emotionList,"dialog","emotion",dialogsize)
dialogReadIni(dialog_spriteList,"dialog","sprite",dialogsize)
dialogReadIni(dialog_background,"dialog","background",dialogsize)

//options
var counter = 0;
var cando;

for(var i = 0; i < optionsize; i ++){
	cando = true;
	
	if(ini_key_exists("option","eventTrue"+string(i))){
		var eventID = eventFind(ini_read_string("option","eventTrue"+string(i),"none"));
		if(eventID > -1){
			if(!eventNormalCheckIndex(eventID)){
				cando = false;
			}
		}
	}
	if(ini_key_exists("option","eventFalse"+string(i))){
		var eventID = eventFind(ini_read_string("option","eventFalse"+string(i),"none"));
		if(eventID > -1){
			if(eventNormalCheckIndex(eventID)){
				cando = false;
			}
		}
	}
	
	if(cando){
		ds_list_add(option_stringList,ini_read_string("option","dialog"+string(i),""));
		ds_list_add(option_gotoList,ini_read_string("option","goto"+string(i),""));
		ds_list_add(option_eventSetList,ini_read_string("option","eventSet"+string(i),""));
		counter++;	
	}
	
	if(counter >= 4){
		break;	
	}
}

ini_close();

