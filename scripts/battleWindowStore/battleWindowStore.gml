var text = argument[0];
var width = argument[1];
var height = argument[2];

var obj = instance_create(400,300,battleEndVictoryHolder);

obj.text = text;
obj.width = width;
obj.height = height;

return obj;