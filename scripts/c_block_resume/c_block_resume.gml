//get the header
var blockToRun = argument[0]

//gets the actual block to run as an object instead of an index.
with(CutsceneParser){
	blockToRun = ds_list_find_value(block_list,blockToRun);
}

//resumes execution, one command after the last command.
c_block_go(argument[0],blockToRun.return_header,blockToRun.pointer+1);