
do
{
	menupos--;

	if(menupos < 0){
		if(!wrap){
			menupos = 0;	
		} else {
			menupos = menu_size-1;
		}
	}
}
until (character.tablet[menupos]  >= 0); 

if(character.tablet_recover[menupos] > -1){
	tts_say("Recovery" + " ");	
}

if(numbering[menupos] > -1){
	tts_say(string(numbering[menupos]+1) + " ")
}

tts_say("\"" + string(braille_display[menupos].cell_character) + "\""  + " ");

menuParentUpdate();

menuBattleTabletUpdateHUD();