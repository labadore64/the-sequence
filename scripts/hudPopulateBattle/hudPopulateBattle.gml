// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from
var visibleStats = argument[2];

if(instance_exists(obj)){
	// name and character
	hudControllerAddData(entryname + "name",string(obj.name));
	hudControllerAddData(entryname + "ax_desc",string(obj.ax_desc));
	
	hudControllerAddData(entryname + "hp",string(obj.hp.current));
	hudControllerAddData(entryname + "hp_max",string(obj.hp.base));
	hudControllerAddData(entryname + "hp_percent",string(floor(100*obj.hp.current/obj.hp.base)));
	
	if(visibleStats){
		hudControllerAddData(entryname + "mp",string(obj.mp.current));
		hudControllerAddData(entryname + "mp_max",string(obj.mp.base));
		hudControllerAddData(entryname + "mp_percent",string(floor(100*obj.mp.current/obj.mp.base)));
		
		var status = "";
		
		var sizer = ds_list_size(obj.status_effects);
		var aaa;
		for(var i = 0; i < sizer; i++){
			aaa = obj.status_effects[|i];
			if(!is_undefined(aaa)){
				status += aaa.name + ", ";	
			}
		}
		if(sizer > 0){
			hudControllerAddData(entryname + "status_list",status);
		}
	}
}