// damage particles
damage_particle = part_type_create();

part_type_shape(damage_particle,pt_shape_sphere);
part_type_scale(damage_particle,.5,.5)
part_type_alpha2(damage_particle,.85,0)
part_type_speed(damage_particle,7,7,-.05,0)

part_type_blend(damage_particle, true);
part_type_life(damage_particle, 35, 60);  
part_type_gravity(damage_particle,.25,270)

// default size params
part_type[0] = damage_particle; // link to index
part_width[0] = 256; // base width for emitter
part_height[0] = 256; // base height for emitter
part_x[0] = 0; // base x offset for emitter
part_y[0] = -128; // base y offset for emitter
part_quantity[0] = 2; //particles emitted every frame
part_distance[0] = 20; // used to determine how much offset based on rotation there is

// Fire particle
part_type[1] = part_type_create();
var particle1 = part_type[1];

part_type_shape(particle1,pt_shape_sphere);
part_type_size(particle1,0.10,0.10,0.01,0);
part_type_scale(particle1,1,1);
part_type_color3(particle1,65535,33023,255);
part_type_alpha3(particle1,0.25,0.50,0);
part_type_speed(particle1,0,1,0,0);
part_type_direction(particle1,0,359,0,0);
part_type_gravity(particle1,0.10,90);
part_type_orientation(particle1,0,0,0,0,1);
part_type_blend(particle1,1);
part_type_life(particle1,60,60);

part_width[1] = 256; // base width for emitter
part_height[1] = 10; // base height for emitter
part_x[1] = 0; // base x offset for emitter
part_y[1] = -50; // base y offset for emitter
part_quantity[1] = 2; //particles emitted every frame
part_distance[1] = 5; // used to determine how much offset based on rotation there is

// Snow Particle
part_type[2] = part_type_create();
var particle1 = part_type[2];

part_type_shape(particle1,pt_shape_snow);
part_type_size(particle1,0.20,0.20,0.01,0.01);
part_type_scale(particle1,1,1);
part_type_color1(particle1,16777215);
part_type_alpha3(particle1,0,0.50,0);
part_type_speed(particle1,1,1,-0.01,0);
part_type_direction(particle1,270,270,0,0.25);
part_type_gravity(particle1,0.03,90);
part_type_blend(particle1,1);
part_type_life(particle1,15,60);

part_width[2] = 256; // base width for emitter
part_height[2] = 400; // base height for emitter
part_x[2] = 0; // base x offset for emitter
part_y[2] = -200; // base y offset for emitter
part_quantity[2] = 1; //particles emitted every frame
part_distance[2] = 0; // used to determine how much offset based on rotation there is

// Glitch Particle
part_type[3] = part_type_create();
var particle1 = part_type[3];

part_type_shape(particle1,pt_shape_square);
part_type_size(particle1,0.20,0.20,0,0.01);
part_type_scale(particle1,1,1);
part_type_color1(particle1,16744576);
part_type_alpha3(particle1,0,0.50,0);
part_type_speed(particle1,1,1,-0.01,0);
part_type_direction(particle1,90,90,0,0.25);
part_type_gravity(particle1,0.03,270);
part_type_blend(particle1,1);
part_type_life(particle1,60,60);

part_width[3] = 256; // base width for emitter
part_height[3] = 256; // base height for emitter
part_x[3] = 0; // base x offset for emitter
part_y[3] = -128; // base y offset for emitter
part_quantity[3] = 2; //particles emitted every frame
part_distance[3] = 0; // used to determine how much offset based on rotation there is


