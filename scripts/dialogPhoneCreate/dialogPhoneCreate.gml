var chara = argument[0];
var dialog = argument[1];

with(Player){
	draw_action = PLAYER_ACTION_STAND; 	
	playerStandSprite();
	active = false;
}

var newchara = instance_create(0,0,dataCharacter);

newchara.temporary = true;
newchara.character = chara.character;
newchara.newalarm[0] = 1;

var dia = instance_create(0,0,DialogHandlerPhone);

dia.character = newchara;
dia.portrait.character = chara.character;

dia.dialogRef = dialog;