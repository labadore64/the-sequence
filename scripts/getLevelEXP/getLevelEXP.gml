var grow_id = argument[0];
var level = argument[1];
var max_exp = argument[2];

// gets the array of key point exp curves.
// the level is calculated between these.
for(var i = 0; i < 8; i++){
	if(level < objdata_exp.EXPERIENCE[i]){
		break;
	}
}

if(i >=8 || i <= 0){
	return 0;	
}

if(objdata_exp.EXPERIENCE[i]== level){
	return floor(objdata_exp.growth[grow_id,i]*max_exp);	
} else if(objdata_exp.EXPERIENCE[i-1]== level){
	return floor(objdata_exp.growth[grow_id,i-1]*max_exp);	
}  

var expmin = floor(objdata_exp.growth[grow_id,i-1]*max_exp);
var expmax = floor(objdata_exp.growth[grow_id,i]*max_exp);

var leveldiff = level - objdata_exp.EXPERIENCE[i-1]
var diff = (expmax - expmin)/(objdata_exp.EXPERIENCE[i]-objdata_exp.EXPERIENCE[i-1]);

repeat(leveldiff){
	expmin+=diff;
}

return floor(expmin)