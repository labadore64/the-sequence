//get the data for the birds.

var entryname = "bird.";

if(is_undefined(bird_name)){
	bird_name = "";
	bird_science_name = "";
	bird_desc = "";
	bird_height = "";
	bird_weight = "";
	bird_time = "";
} else {

	var birdd = objdataToIndex(objdata_bird,bird_name)

	if(birdd > -1){

		bird_science_name = objdata_bird.bird_science_name[birdd]
		bird_long_name = objdata_bird.bird_long_name[birdd]
		bird_desc = textForceCharsPerLine(objdata_bird.bird_desc[birdd],char_per_line);
		if(global.metric){
			bird_height = objdata_bird.bird_height_metric[birdd]
			bird_weight = objdata_bird.bird_weight_metric[birdd]
		} else {
			bird_height = objdata_bird.bird_height[birdd]
			bird_weight = objdata_bird.bird_weight[birdd]
		}
	
		bird_adjust = objdata_bird.bird_adjust[birdd]
		bird_scale = objdata_bird.bird_scale[birdd]
		bird_sprite = objdata_bird.bird_male_sprite[birdd]
		bird_male = true;
		bird_male_sprite = objdata_bird.bird_male_sprite[birdd]
		bird_female_sprite = objdata_bird.bird_female_sprite[birdd]
		
		
		hudControllerAddData(entryname + "name",bird_long_name);
		hudControllerAddData(entryname + "science_name",bird_science_name);
		hudControllerAddData(entryname + "desc",bird_desc);
		
		hudControllerAddData(entryname + "height",bird_height);
		hudControllerAddData(entryname + "weight",bird_weight);
	}

	var time = ds_map_find_value(obj_data.bird_seen,bird_name)

	if(!is_undefined(time)){
		var day = string(date_get_day(time));
		var month = string(date_get_month(time));
		var year = string(date_get_year(time))

		if(global.metric){
		bird_time = day + "/" + month + "/" + year;
		} else {
			bird_time = month + "/" + day + "/" + year;	
		}
		
		hudControllerAddData(entryname + "time",bird_time);
	} else {
		bird_time ="UNDEF"	
	}
	
	
}