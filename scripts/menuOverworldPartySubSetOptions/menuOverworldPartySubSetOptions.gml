if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	menuAddOption(global.langini[0,17],replaceStringWithVars(global.langini[1,17],character.name),menuOverworldStatsScript)
	menuAddOption(global.langini[0,18],replaceStringWithVars(global.langini[1,18],character.name),menuOverworldSwitchScript)
	SHORTHAND_CANCEL_OPTION
	
	menupos = 0;
	
}

menu_size = menuGetSize();