// replaces a single string in the hud controller
var stringer = argument[0];
var obj = argument[1];

if(instance_exists(obj)){
	var str_size = string_length(stringer);
	var position = string_pos("{{",stringer);
	var initpos = position;
	if(position > 0){
		while(position <= str_size && position > 0){
			while(!(
					string_char_at(stringer,position) == "}" &&
					string_char_at(stringer,position+1) == "}"
				)){
				position++;
			}

			var val = string_replace_all(string_copy(stringer,initpos+2,(position-initpos-2))," ","");

			var divider = string_pos(":",val);
			var as_string = "";		
			
			if(divider > 0){
				as_string = string_copy(val,divider,string_length(val)-divider+1);
				val = string_copy(val,1,divider-1);
			}
			var replacestring = hudControllerGetData(obj.variable_name + "." + val);

			stringer=string_delete(stringer,initpos,((position-initpos+2)));
			stringer=string_insert("[["+replacestring+as_string+"]]",stringer,initpos);
			position = string_pos("{{",stringer);
			initpos = position;
			str_size = string_length(stringer);
		}
	}
}

return stringer;