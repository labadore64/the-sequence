	dir = AXManager.axis_angle
	move_this_turn = true
	
	if(AXManager.axis_amount > AXManager.axis_min){
		is_running = true
	} else {
		is_running = false;
	}
	
	var toler = 23.5;
	if(abs(angle_difference(dir,180)) < toler){
		dir = 180;	
	} else if (abs(angle_difference(dir,270)) < toler){
		dir = 270;	
	} else if (abs(angle_difference(dir,90)) < toler){
		dir = 90;	
	} else if (abs(angle_difference(dir,0)) < toler){
		dir = 0;	
	}
	
	if(dir == 270){
		if(axis_last_dir == 90){
			orientation = 180	
		}
	} else if(dir == 90){
		if(axis_last_dir == 270){
			orientation = 0	
		}
	}
	
	axis_last_dir = dir;
	playerOrientationMoveTowardDirection(dir)