var obj = instance_create(0,0,menuPracticeSub)

obj.braille_cell = display_cell[selected_row,selected_column]
obj.braille_character = obj.braille_cell.cell_character;
obj.braille_char = obj.braille_cell.cell_character;
obj.braille_id = braille_keys[|menupos];
obj.desc = braillePracticeList.braille_data.braille_desc[? obj.braille_character];

if(is_undefined(obj.desc)){
	obj.desc = "";	
} else {
	var display_linesize = 18
	obj.desc = string_replace_all(obj.desc,"\n","~");	
	obj.desc = textForceCharsPerLine(obj.desc,display_linesize);
	obj.desc = string_replace_all(obj.desc,"~","\n");	
}