var teee = instance_create_depth(0,30,0,TextBox);

teee.filename_id = argument[0]
teee.text_section = "dialog";
teee.text_name = "dialog"
teee.text_id = "dialog"
teee.character = argument[1]
teee.draw_portrait = true;
teee.delay_display = 30
teee.end_script = dialogLoadOptions


//teee.triggered_by_player = true;

ini_open(argument[0]);

var sizer = dialogGetSize("dialog","dialog");

ini_close();

//set scripts
for(var i = 0; i < sizer; i++){
	textboxScriptSet(teee,dialogScriptUpdate,i)
}


textboxScriptAddArgument(teee,"sprite_list",dialog_spriteList)
textboxScriptAddArgument(teee,"emotion_list",dialog_emotionList)
textboxScriptAddArgument(teee,"pitch_list",dialog_pitchList)
textboxScriptAddArgument(teee,"background_list",dialog_background)

//set end text argument
var sizer = ds_list_size(option_stringList);

if(sizer > 0){
	
} else {
	teee.end_script = dialogEnd;	
}


with(Player){
	active=false;
}

return teee;