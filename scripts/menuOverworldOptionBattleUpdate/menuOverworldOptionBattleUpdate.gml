//updates the option based on direction.
//-1 = left, 1 = right
var list_to_check = -1;
var curpos = 0;

if(menupos == 0){
	//speed
	list_to_check=rotation_strings;
	curpos = global.battleRotate;
} else if(menupos == 1){
	//color	
	list_to_check=animation_strings;
	curpos = global.battleAnimate;
} else if (menupos == 2){
	//font	
	list_to_check = particle_strings;
	curpos = global.battlePortrait;
} else if (menupos == 3){
	list_to_check = crt_strings;
	curpos = global.vcr_on;
} else if (menupos == 4){
	list_to_check = sfx_strings;
	curpos = global.attackSound;
} else if (menupos == 5){
	list_to_check = sfx_strings;
	curpos = global.battleHPSound;
}


var directionz = argument[0];
var list_size = ds_list_size(list_to_check);

if(directionz == -1){
	//if moving left	
	curpos--;
	if(curpos < 0){
		if(wrapz[menupos]){
			curpos = list_size-1;
		} else {
			curpos = 0;	
		}
	}
} else if (directionz == 1){
	//if moving right	
	curpos++;
	if(curpos > list_size-1){
		if(wrapz[menupos]){
			curpos = 0;	
		} else {
			curpos = list_size - 1;	
		}
	}
}
//finally set values accordingly
if(menupos == 0){
	global.battleRotate = curpos;
	tts_say(rotation_strings[|curpos])
} else if (menupos == 1){
	global.battleAnimate = curpos;
	tts_say(rotation_strings[|curpos])
} else if (menupos == 2){
	global.battlePortrait = curpos
	tts_say(rotation_strings[|curpos])
} else if (menupos == 3){
	global.vcr_on = curpos;
	tts_say(rotation_strings[|curpos])
} else if (menupos == 4){
	global.attackSound = curpos;
	tts_say(rotation_strings[|curpos])
} else if (menupos == 5){
	global.battleHPSound = curpos;
	tts_say(rotation_strings[|curpos])
}

curposs[0] = global.battleRotate
curposs[1] = global.battleAnimate
curposs[2] = global.battlePortrait
curposs[3] = global.vcr_on
curposs[4] = global.attackSound
curposs[5] = global.battleHPSound

menuParentUpdateLookAndFeel()

soundfxPlay(sound_select)