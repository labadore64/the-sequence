if(ds_exists(menu_name,ds_type_list)){

	draw_set_color(text_color);

	var stringer = "";
	var xx = 0;
	var yy = 0;

	//draw_set_font(global.largeFont)
	draw_set_halign(fa_center)
	//draw_text_transformed_ratio(x1+410,y1+5,"Energy Cost",1,1,0)
	//draw_text_transformed_ratio(x1+555,y1+5,"Damage",1,1,0)
	//draw_text_transformed_ratio(x1+655,y1+5,"Target",1,1,0)
	//draw_text_transformed_ratio(x1+740,y1+5,"Type",1,1,0)
	draw_set_halign(fa_left)
	//draw_set_font(global.textFont);

	//get max number of items that can be displayed
	var drawmax = 0;
	var counter = 0;
	for(var i = toppos; i < menu_size; i++){
		xx = x1+option_x_offset;
		yy = y1+option_y_offset+option_space*counter;
		if(yy > y2-option_space){
			drawmax = i;
			break;
		}
		counter++;
		drawmax = i+1;
	}
	counter = 0;


	//draw the selection box
	draw_set_alpha(select_alpha);
	draw_set_color(select_color);

	var currpos = menupos - toppos;

	yy = y1+option_y_offset+currpos*option_space - option_space*0.25;

	//scrolldown function

	while(yy > y2-option_space){
		toppos++;
	
		currpos = menupos - toppos;

		yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
	}

	//scrollup function

	while(yy < y1+gradient_height || currpos < 0){
		toppos--;
	
		currpos = menupos - toppos;

		yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
	}



	draw_rectangle_ratio(x1,yy,x1+((x2-x1)*select_percent),yy+option_space,false)

	draw_set_alpha(1)

	for(var i = toppos; i < drawmax; i++){
			if(move_cando[|i]){
				draw_set_color(global.textColor)	
			} else {
				draw_set_color($444444);
			}
		stringer = menu_name_array[ i];
	
		if(!is_undefined(stringer)){
			xx = x1+option_x_offset;
			yy = y1+option_y_offset+option_space*counter;

			draw_text_transformed_ratio(xx,yy,stringer,text_size,text_size,0)
	/*		
	moves_targets = ds_list_create();
	moves_type = ds_list_create();
	moves_damage = ds_list_create();
	moves_mp_cost = ds_list_create();
	*/		

			//var srpite = moves_type[|i];
			//if(srpite > -1){
			//	draw_sprite(srpite,0,mov+xx+370,yy+10)
			//}
		
			draw_set_halign(fa_center)
			//draw_text_transformed_ratio(mov+xx,yy,string(moves_mp_cost[|i]) +"%",text_size,text_size,0)
			//draw_text_transformed_ratio(mov+xx+100,yy,string(moves_damage[|i]),text_size,text_size,0)
			//draw_text_transformed_ratio(mov+xx+290,yy,string(moves_targets[|i]),text_size,text_size,0)
			//menuBattleMovesDrawBar(xx+mov,yy+5,75,8,moves_mp_cost[|i]);
			//menuBattleMovesDrawBar(xx+mov+150,yy+5,75,8,moves_damage[|i]);
			draw_set_halign(fa_left)
			//draw_text_transformed_ratio(xx,yy,stringer,text_size,text_size,0)

		}
		counter++;
	}

	draw_set_color(global.textColor);

}