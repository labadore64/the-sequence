/// @description Done
message_end = array_length_1d(message);
string_lenger = string_length(message[message_current]);

if (done)
{
    instance_destroy();
}

line = 0;
space = 0;
i = 1;

//t is for modifiers
t+= menuControl.timer_diff

	
if(last_message != message_current){
	audio_stop_sound(text_sound1);	
	audio_stop_sound(text_sound2);	
	audio_stop_sound(text_sound3);	
	audio_stop_sound(text_sound4);	
	finish_draw = false;
	clear_surf = true;
	cutoff = 0;
}

var portraitcando = portrait != -1 && instance_exists(portrait);

if(portraitcando){
	portrait.image_index = 0
	portrait.state = PORTRAIT_WAIT_NO_BLINK	
}	

    //Typewriter
    //This is so we print each character one at a time
    if (cutoff < string_lenger)
    {
		

		
		if(portraitcando){
			portrait.state = PORTRAIT_TALK
		}
		
        if (timer > delay)
        {
            cutoff+= menuControl.timer_diff
            timer = 0;
			if(global.textSpeed != -1){
				textboxSoundPlay();
			}
			force_draw = true;
        }
        else timer+= menuControl.timer_diff
		
		if(cutoff >= string_lenger){
			audio_stop_sound(text_sound1);	
			audio_stop_sound(text_sound2);	
			audio_stop_sound(text_sound3);	
			audio_stop_sound(text_sound4);	
			cutoff = string_lenger
		}
    } else {
		if(portraitcando){
			portrait.state = PORTRAIT_WAIT_NO_BLINK	
			portrait.image_index = 0;
		}
	}

if(random_this_step){
	random_val = random_range(jitter_amount*-1, jitter_amount);
	random_valy = random_range(jitter_amount*-1, jitter_amount);
}

if(ScaleManager.updated){
	surface_draw = true;
}