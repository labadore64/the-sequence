		if(sprite_index >= 0){
		if(BattleHandler.shd_bc_shader_enabled  && !global.disableShader){
			shader_set(shd_bright_contrast)
		    shader_set_uniform_f(BattleHandler.shd_bc_uni_time, BattleHandler.shd_bc_var_time_var);
		    shader_set_uniform_f(BattleHandler.shd_bc_uni_mouse_pos, BattleHandler.shd_bc_var_mouse_pos_x, BattleHandler.shd_bc_var_mouse_pos_y);
		    shader_set_uniform_f(BattleHandler.shd_bc_uni_resolution, BattleHandler.shd_bc_var_resolution_x, BattleHandler.shd_bc_var_resolution_y);
		    shader_set_uniform_f(BattleHandler.shd_bc_uni_brightness_amount, flash_intensity);
		    shader_set_uniform_f(BattleHandler.shd_bc_uni_contrast_amount, 0);
			//draw_sprite_ext(BATTLE_TEST_SPRITE_DELETE_LATER,spr_counter,x,y-jump_y*image_yscale,image_xscale,image_yscale,0,image_blend,image_alpha);
			draw_sprite_ext(sprite_index,spr_counter,x-flee_dist,y-jump_y*image_yscale,image_xscale*sprite_scale,image_yscale*sprite_scale,0,image_blend,image_alpha*hide_alpha);
			shader_reset();
		} else {
			draw_sprite_ext(sprite_index,spr_counter,x-flee_dist,y-jump_y*image_yscale,image_xscale*sprite_scale,image_yscale*(sprite_scale+ (.25*(flash_intensity-.5))),0,image_blend,image_alpha*hide_alpha);
		}
		
		}