draw_set_color(global.textColor);
var spaceex = 150;
var spaceey = 30;

draw_text_transformed_ratio(x-spaceex,y-spaceey,"Your Score",3,3,0);
draw_text_transformed_ratio(x+spaceex,y-spaceey,"High Score",3,3,0);

var score_space = 60;

draw_text_transformed_ratio(x-spaceex,y-spaceey+score_space,string(myscore),4,4,0);

draw_set_color(highscore_color);

draw_text_transformed_ratio(x+spaceex,y-spaceey+score_space,string(highscore),4,4,0);

if(new_highscore){
	var moverr = 40;
	var bounce = 20;
	draw_text_transformed_ratio(x+spaceex,y+moverr-spaceey+selected_scale_amount*bounce+score_space,"NEW!",3,3,0);
}