/// @description Insert description here
// You can write your code in this editor


stat_phy_power = 1
stat_phy_guard = 1
stat_mag_power = 1
stat_mag_guard = 1
stat_spe_power = 1
stat_spe_guard = 1

char_val = -1;

color_speed =  2;
character = obj_data.party[|0];
draw_hex_internal(char_val,100);

radius = 100;
newalarm[11] = 1

surface_hex = -1;

color[0] = $0000FF;
color[1] = $FFFF00;
color[2] = $00FF00;
color[3] = $FF00FF;
color[4] = $FF0000;
color[5] = $00FFFF;
draw_rotate_color = 0;
draw_rotate_counter = 0;
counter = false;

multiplier = 1;