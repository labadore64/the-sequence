gml_pragma("forceinline");

blink_rate = 15;
blink = false;
blink_counter = 15;

menuAddOption(global.langini[0,0],global.langini[1,0],menuOverworldMainScriptParty)
menuAddOption(global.langini[0,1],global.langini[1,1],menuOverworldMainScriptItem)
menuAddOption(global.langini[0,2],global.langini[1,2],menuOverworldMainScriptBraille)
menuAddOption(global.langini[0,3],global.langini[1,3],menuOverworldMainScriptOption)

menuParentSetDrawGradient(false);
menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentSetOptionSpacing(200)
menuParentSetOptionXOffset(100)
menuParentSetOptionYOffset(135)

menu_draw = menuOverworldMainDrawMain;

menu_size = menuGetSize();

animation_start = 3 //animation length
animation_len = animation_start; //animation counter

menu_left = menuOverworldPartySubLeft;
menu_right = menuOverworldPartySubRight;



menu_up = menu_left;
menu_down = menu_right;

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

tts_say("Main Menu" + " " + "Party")

x = 410.61;
y = 0;
draw_width = 827;
draw_height = 200; 

menuParentUpdateBoxDimension();


menuControlForceDrawMenuBackground();