/// @description - give an item to a character
/// @param item
/// @param character

var itemzz = argument[0];
var chara = argument[1];

var cando = false;

with(chara){
	for(var i = 0; i < 5; i++){
		if(item[i] == itemzz){
			item[i] = -1;
			cando = true;
			break;
		}
	}
}

//makes sure no holes appear in the character's inventory.
//itemAdjustCharacter(chara);

statUpdate();

if(cando){
	giveItem(itemzz,1);	
}