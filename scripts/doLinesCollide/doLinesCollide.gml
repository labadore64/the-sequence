// argument 0-1 = first line point1 x y
// argument 2-3 = first line point2 x y
// argument 4-5 = second line point1 x y
// argument 6-7 = second line point2 x y
line_collision_resultx = -1000000;
line_collision_resulty = -1000000;

var xx = -1000000;
var yy = -1000000;

var A1 = argument[3] - argument[1];
var B1 = argument[0] - argument[2];
var C1 = A1 * argument[0] + B1 * argument[1];

var A2 = argument[7] - argument[5];
var B2 = argument[4] - argument[6];
var C2 = A2 * argument[4] + B2 * argument[5];

var det = A1 * B2 - A2 * B1;

if (abs(det) < math_get_epsilon())
{
	//parallel
}
else
{
	var detter = 1 / det;
	xx = ((B2 * C1 - B1 * C2) * detter);
	yy = ((A1 * C2 - A2 * C1) * detter);

}

if (isPointOnLine(argument[4],argument[5], argument[6],argument[7], xx,yy)
	&& isPointOnLine(argument[0],argument[1], argument[2],argument[3], xx,yy))
{
	line_collision_resultx = xx;
	line_collision_resulty = yy
}