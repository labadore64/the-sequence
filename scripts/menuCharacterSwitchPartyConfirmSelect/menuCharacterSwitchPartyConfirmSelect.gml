ds_list_copy(obj_data.party,predict_list)


with(menuCharacterSwitchParty){
	var chara = obj_data.party[|menupos].character
	char_color = obj_data.party[|menupos].color
	portrait.character = chara;
	menuOverworldPartyUpdateStats(obj_data.party[|menupos],100);
	with(portrait){
		surface_free(surface)	
		event_perform(ev_alarm,0)
	}
}


calcPartyAuraColor();

with(menuCharacterSwitchPartySub){
	instance_destroy()	
}

with(menuCharacterSwitchRecruit){
	instance_destroy()	
}

with(menuCharacterSwitchRecruitSub){
	instance_destroy()	
}

instance_destroy()	
