var hit = argument[0];
var time = argument[1];
var knockback = argument[2];

if(hit.in_base_position){
	hit.damage_time = time;
	hit.anim_knockback = knockback;
}