sizer = ds_list_size(command_list)
var inst;

for(var i = 0; i < sizer; i++){
	inst = ds_list_find_value(command_list,i);
	with(inst){
		instance_destroy();	
	}
}

ds_list_clear(command_list);