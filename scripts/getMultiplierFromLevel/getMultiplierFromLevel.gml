var grow_id = argument[0];
var level = argument[1];

var arr = -1;

// gets the array of key point exp curves.
// the level is calculated between these.
for(var i = 0; i < 8; i++){
	if(level < objdata_exp.EXPERIENCE[i]){
		break;
	}
}

if(i >=8){
	return 1;	
} else if(i <= 0){
	return 0;	
}

// now that you know the level difference, calculate
var minlev = objdata_exp.EXPERIENCE[i-1];
var maxlev = objdata_exp.EXPERIENCE[i];

var minexp = objdata_exp.growth[grow_id,i-1];
var maxexp = objdata_exp.growth[grow_id,i];

var diff = (maxexp-minexp)/(maxlev - minlev);

while(minexp < maxexp){
	minlev++;
	minexp+=diff;
}

return minexp-diff;