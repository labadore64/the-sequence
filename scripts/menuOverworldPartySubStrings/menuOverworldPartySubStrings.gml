var namerr = "";
with(menuOverworldParty){
	namerr = obj_data.party[|menupos];	
}

character_name = namerr.name
character = namerr;

menuOverworldPartySubSetOptions();


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentSetTitle(replaceStringWithVars(global.langini[4,4],character_name));
menuParentSetSubtitle("");

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

menuParentSetGradientHeight(0)
menuParentSetOptionYOffset(10);
menuParentSetOptionSpacing(40)
draw_height = 160
draw_width = 200

x = 600
y = 350

menu_draw = menuOverworldPartySubDraw;

menu_left = menuOverworldPartySubLeft;
menu_right = menuOverworldPartySubRight;

menu_up = menu_left;
menu_down = menu_right;

//menuParentTTSLabelRead();

menuParentUpdateBoxDimension();

draw_bg = false;