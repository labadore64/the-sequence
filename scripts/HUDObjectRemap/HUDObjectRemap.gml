
	var obj = argument[0];
	var queue = split_string(argument[1],",");
	var stringer;
	while(!ds_queue_empty(queue)){
		stringer = ds_queue_dequeue(queue);
	
		if(!is_undefined(stringer)){
			var pos = string_pos(":",stringer);
			if(pos > 0){
				var old = string_copy(stringer,0,pos-1);
				var newval = string_copy(stringer,pos+1,string_length(stringer)-pos);
		
				var keydata_len = array_length_1d(obj.key_id);
	
				for(var i = 0; i < keydata_len; i++){
					if(obj.key_id[i] == old){
						obj.key_id[i] = newval;	
					}
				}
			}
		}
	}

