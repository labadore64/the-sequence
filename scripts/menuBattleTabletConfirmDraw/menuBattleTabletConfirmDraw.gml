
	if(animation_len == 0){
		draw_set_halign(fa_center);
		gpu_set_colorwriteenable(true,true,true,true)
		menuParentDrawBox();
		gpu_set_colorwriteenable(true,true,true,false)
		menuParentDrawText();
	
		menuBattleTabletDrawOptionsSelect();
	
		var labelsx = x+120;
		var labelsy = y-47;
	
		var element_x = 80;
		var moveicon_y = 35;
	
		draw_text_transformed_ratio(labelsx,labelsy,"Type",1,1,0);
		draw_text_transformed_ratio(labelsx+element_x,labelsy,"Element",1,1,0);
	
		draw_text_transformed_ratio(labelsx+50,labelsy-40,name,3,3,0);
	
		// draw elements
		if(move_type == MOVE_TYPE_PHYSICAL){
			draw_sprite_extended_ratio(spr_typePhys,0,labelsx,labelsy+moveicon_y,1,1,0,c_white,1)
		} else {
			draw_sprite_extended_ratio(spr_typeMag,0,labelsx,labelsy+moveicon_y,1,1,0,c_white,1)
		}
	
		if(move_type_sprite > -1){
			//element
			draw_sprite_extended_ratio(move_type_sprite,0,labelsx+element_x,labelsy+moveicon_y,1,1,0,c_white,1)
		}
	
		draw_text_transformed_ratio(labelsx-65+20,labelsy+70,"Boost:",1,1,0);
		draw_text_transformed_ratio(labelsx-90+20,labelsy+90,"Energy Cost:",1,1,0);
		var drawcolor = global.textColor
		if(tablet_boost > .9){
			drawcolor = rotate_color;	
		}
	
		drawBar(labelsx+30+20,labelsy+74,60,5,tablet_boost,drawcolor)

		//mp_total_cost
		drawBar(labelsx+30+20,labelsy+74+20,60,5,mp_total_cost,global.textColor)
	
		draw_set_halign(fa_left);
	} else {
		gpu_set_colorwriteenable(true,true,true,true)
		menuParentDrawBoxOpen();
	}

	gpu_set_colorwriteenable(true,true,true,true)

	with(braille_preview){
		braillePreviewDraw(id);	
	}

