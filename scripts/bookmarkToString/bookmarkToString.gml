var bookmark = argument[0];

var mapp = ds_map_create();

var returner = "";

with(bookmark){
	ds_map_add(mapp,"pitch",string(pitch))
	ds_map_add(mapp,"name",string(name))
	ds_map_add(mapp,"audio_sound",string(audio_sound))
	ds_map_add(mapp,"index",string(index))
	ds_map_add(mapp,"x",string(x))
	ds_map_add(mapp,"y",string(y))
}

returner = json_encode(mapp);

ds_map_destroy(mapp);

return returner;