//test first word detected in the string
var str = string_lower(string_letters(argument[0]));
var len = array_length_1d(objdata_words.name);
var poss = 0;
var sss = "";
for(var i = 0; i < len; i++){
	sss = objdata_words.name[i];
	poss = string_pos(sss ,str);
	if(poss > 0){
		detected_word=sss;
		detected_length=string_length(detected_word);
		word_move = objdata_words.move[i];
		return poss;
	}
}
detected_word="";
detected_length=0;
return -1;