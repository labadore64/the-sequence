
var hasmoved = false;
if(overworld_item_wait_trigger){
	if(binoculars){
		playerBinocularsInput()
	} else {
		if(!item_use_lock && !push_lock){

			if(!adjust_this_frame){
				no_move = false;
			}
			move_this_turn = false;

			if(!surface_exists(surface)){
				update_surface = true;	
			}

			if(objGenericCanDo()){
				is_running = false;
				
				playerAccessMenu();
			
				if(global.key_state_array[KEYBOARD_KEY_OPEN_MENU] == KEY_STATE_PRESS ||
					global.gamepad_state_array[KEYBOARD_KEY_OPEN_MENU] == KEY_STATE_PRESS){
					if(objGenericCanDo()){
						if(script_menu_open!= -1){
							script_execute(script_menu_open);
						}
					}
				}
				else if(global.key_state_array[KEYBOARD_KEY_BOOKMARK] == KEY_STATE_PRESS ||
					global.gamepad_state_array[KEYBOARD_KEY_BOOKMARK] == KEY_STATE_PRESS){
					playerAXPlaceBookmark();
				} else if(global.key_state_array[KEYBOARD_KEY_SIGHT] == KEY_STATE_PRESS ||
					global.gamepad_state_array[KEYBOARD_KEY_SIGHT] == KEY_STATE_PRESS){
					playerAXSightReadName()
				} else if(keyboard_check_pressed(seeing_desc_key)){
					playerAXSightReadDesc();
				} else if(keyboard_check_pressed(axx_key)){
					tts_say(string(coordConvert(x)) + " x")
				} else if(keyboard_check_pressed(axy_key)){
					tts_say(string(coordConvert(y)) + " y")
				} else if (global.key_state_array[KEYBOARD_KEY_COORDINATES] == KEY_STATE_PRESS ||
					global.gamepad_state_array[KEYBOARD_KEY_COORDINATES] == KEY_STATE_PRESS){
					tts_say(string(coordConvert(x)) + " x, " + string(coordConvert(y)) + " y")
				} else if (mouse_check_button_pressed(mb_right)){
					if(!MouseHandler.clicked){
						script_execute(right_click_menu)	
					}
				} else if (mouse_check_button_pressed(mb_left) && click_delay <= -1){
					playerPressMousePressed();	
					click_delay = 5;
				} else if (mouse_check_button(mb_left) && click_delay <= -1){
					playerPressMouse();	
				}
				else {
					event_inherited();
				}
				
				if(!AXManager.axis_changed){
					playerRunPress();
				}
				if(objGenericAxisTriggered()){
					playerMoveAxis();
				} else {
					if(AXManager.axis_changed){
						playerMoveRelease()
					}
				}
	
				if(zoom_x_previous != zoom_x){
					playerUpdateSize();
				}
	
				image_xscale = draw_scale*zoom_factor_x;
	
				if(draw_flip){
					image_xscale = image_xscale * -1;	
				}
	
	
				playerUpdateRotation();

	
				if(move_this_turn){
					playerStepCollision();
				}
				
				var addx = cos(degtorad(orientation))*interact_dist;
				var addy = sin(degtorad(orientation))*interact_dist;
				interact_x = x+addx;
				interact_y = y+addy+collision_y_adjust+16;
				
				playerDrawDirection();
				var bonk_sound = false;

				if(!no_move){
					if(move_this_frame){
						playerStepMovement();
						move_this_frame = false; 
						hasmoved = true
		
					} else {
						//bonk_sound = true;
						move_rate = 0;	
						draw_action = PLAYER_ACTION_STAND
					}
				} else {
					if(keypress_this_frame){
						bonk_sound = true;
					}
						draw_action = PLAYER_ACTION_STAND	
				}
				if(bonk_sound){
					if(!audio_is_playing(bonk_sound_normal) &&
						!audio_is_playing(bonk_sound_item)){
						if(sound_item){
							soundfxPlay(bonk_sound_item)
						} else {
							soundfxPlay(bonk_sound_normal)
						}
					}
				}
	
				playerUpdateSprite();
	
				depth = y;
	
				move_x = 0; //how much to move x this frame
				move_y = 0; //how much to move y this frame
	
				draw_action = PLAYER_ACTION_STAND
	
				keypress_this_frame = false
	
				//do bird
	
	
			}


		}
	
		camera_x = x;
		camera_y = y;
		camera_zoom_x = zoom_x;
		camera_zoom_y = zoom_y;
	}

	var camx = camera_zoom_x;
	var camy = camera_zoom_y;

	with(overworld_scalable){
		zoom_x = camx;
		zoom_y = camy;
	}

}