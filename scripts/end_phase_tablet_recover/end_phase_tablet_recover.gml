var nope = argument[0];
var perc_damage = .125

battleTextDisplayClearText();

with(nope){
	var can_flash = false;
	for(var i = 0; i < 3; i++){
		if(tablet[i] > -1){
			if (tablet_recover[i] == 0){
				var stringer = "@0's @1 Tablet is recharged!"
		
				var tmp_tablet = instance_create(0,0,tmp);
				var tab_id = tablet[i];
			
				with(tmp_tablet){
					loadTablet(tab_id);
				}
				
				stringer = string_replace_all(stringer,"@0",name);
				stringer = string_replace_all(stringer,"@1",tmp_tablet.name);
				battleTextDisplayAddText(stringer);
				battleTextDisplayAddText(" ");
				with(tmp_tablet){
					instance_destroy();	
				}
				can_flash = true;
				tablet_recover[i]--;
				
				
			}
		}
	}
	
	if(can_flash){
		battlerFlashOnce(id);	
	}
}