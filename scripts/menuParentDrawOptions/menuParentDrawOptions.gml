draw_set_color(text_color);
gpu_set_colorwriteenable(true,true,true,false)
var yy = 0;

//get max number of items that can be displayed
var drawmax = 0;
var counter = 0;
for(var i = toppos; i < menu_size; i++){
	
	yy = menuParent_yy+option_space*counter;
	if(yy > y2-option_space){
		drawmax = i;
		break;
	}
	counter++;
	drawmax = i+1;
}
counter = 0;
for(var i = toppos; i < drawmax; i++){
	
	yy = menuParent_yy+option_space*counter;

	draw_text_transformed_ratio(menuParent_xx,yy,menu_name_array[i],text_size,text_size,0)

	counter++;
}

//draw the selection box
draw_set_alpha(select_alpha);
draw_set_color(select_color);

var currpos = menupos - toppos;

yy = menuParent_yy+currpos*option_space - option_space*0.25;

//scrolldown function

while(yy > y2-option_space){
	toppos++;
	
	currpos = menupos - toppos;

	yy = menuParent_yy+currpos*option_space - option_space*0.25;
}

//scrollup function

while(yy < y1+gradient_height || currpos < 0){
	toppos--;
	
	currpos = menupos - toppos;

	yy = menuParent_yy+currpos*option_space - option_space*0.25;
}


gpu_set_colorwriteenable(true,true,true,false)
draw_rectangle_ratio(x1,yy,x1+(draw_width*select_percent),yy+option_space,false)
gpu_set_colorwriteenable(true,true,true,true)
draw_set_alpha(1)
draw_set_color(global.textColor);