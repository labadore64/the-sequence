var obj = instance_create(0,0,menuBrailleChallengeQuantity);
obj.game_obj = brailleGameElimination; //which object type to use for this game mode
obj.min_value = 1;
obj.max_value = 9;
obj.value_spacer = 1;
obj.braille_obj = brailleDataObj

obj.menu_help = "braillelives"
obj.unit_string = "Lives"
obj.title = "Elimination"