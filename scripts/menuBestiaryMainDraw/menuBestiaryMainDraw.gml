
if(animation_len == 0){
	draw_clear(c_black)
	
	if(!surface_exists(surf)){
		surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){	
			surface_resize(surf,global.window_width,global.window_height);
		}
	}
	surface_set_target(surf)
	draw_clear(c_black)
	
	with(brailleGameBackground){
		draw_surface(surf,0,0);
	}
	
	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_alpha(.5)
	draw_set_color(c_black)
	draw_rectangle(0,0,global.window_width,global.window_height,false)
	draw_set_alpha(1)
	if(monster_sprite > -1){
		if(monster_state == DEX_STATE_OWN){
			gpu_set_blendmode(bm_add)
			draw_sprite_extended_ratio(monster_sprite,0,translation+650,500,-1.5,1.5,0,c_white,1)
			gpu_set_blendmode(bm_normal)
		}
	}
	gpu_set_colorwriteenable(true,true,true,true)
	surface_reset_target();
	
	drawSurfAsVCR(surf)
	
	draw_set_halign(fa_left)
	draw_set_valign(fa_top)
	
	
	if(drawGradient){
		draw_rectangle_color_ratio(x1,0,x2,0+gradient_height,gradient_color1,gradient_color2,gradient_color2,gradient_color1,false)
	}

	menuParentDrawText();
	if(no_creatures){
		draw_set_halign(fa_center)
		draw_set_valign(fa_middle)
		draw_text_transformed_ratio(400,300,"No creatures discovered yet.\n\nWhy not encounter some\n\nby exploring around?",2,2,0);
		draw_set_halign(fa_left)
		draw_set_valign(fa_top)
	} else {
		menuBestiaryMainDrawOptions()
	}
	gpu_set_colorwriteenable(true,true,true,true)
	
	if(!no_creatures){
		draw_set_color(global.textColor)
		draw_set_font(global.textFont)
		draw_set_halign(fa_right)
		var adjuster = 25;
		
		draw_text_transformed_ratio(660,570-adjuster,"Creatures seen:",2,2,0)
		draw_text_transformed_ratio(660,570,"Creatures recorded:",2,2,0)
		
		draw_text_transformed_ratio(720,120,"Sort: ",2,2,0)
	
		draw_text_transformed_ratio(660+65-5,570-adjuster,string(menu_size),2,2,0)
		draw_text_transformed_ratio(660+65-5,570,string(recorded_size),2,2,0)
		
		draw_set_halign(fa_center)
		draw_text_transformed_ratio(660+65,570-adjuster,"/",2,2,0)
		draw_text_transformed_ratio(660+65,570,"/",2,2,0)
	
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(660+65+5,570-adjuster,string(objdata_enemy.dex_size),2,2,0)
		draw_text_transformed_ratio(660+65+5,570,string(objdata_enemy.dex_size),2,2,0)
	
		//sort_name[sort_mode]
		draw_text_transformed_ratio(720,120,sort_name[global.dex_sort_mode],2,2,0)
	}
	draw_letterbox();
}

