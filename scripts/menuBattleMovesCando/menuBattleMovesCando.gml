var mp = argument[0];
var chara = argument[1];
var move = argument[2]

var returner = true;

if(mp > chara.mp.current){
	returner = false;	
}

if(move == objdataToIndex(objdata_moves,"bribe")){
	if(BattleHandler.money < 10){
		returner = false;	
	} else {
	
		var bribe_total = 0;
		var bribe_amount = BattleHandler.money
		//add all bribes of inputs together.
		with(battleAttackInput){
			bribe_total+= bribe;	
		}
		
		bribe_amount -= bribe_total;
		
		if(bribe_amount < 10){
			returner = false;	
		}
	}
}

return returner;