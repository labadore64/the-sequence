//draw the completed message
if(completed){
	if(!surface_exists(result_surf)){
		result_surf = surface_create(global.window_width,global.window_height);
	} else {
		if(ScaleManager.updated){
			surface_resize(result_surf,global.window_width,global.window_height);	
		}
	}
	gpu_set_colorwriteenable(true,true,true,true)
	surface_set_target(result_surf);
	
	draw_clear_alpha(c_black,0);
	draw_set_halign(fa_left);
	drawOutlineText(result_start_counter,250,complete_animation_text,8,8,0,c_white)
	surface_reset_target();
	
	draw_surface_ext(result_surf,global.display_x,global.display_y,1,1,0,c_white,result_fade_counter);
}

draw_letterbox();