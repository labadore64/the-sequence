
//flee animatin
if(fleeing && !flee_complete){
	if(flee_delay <= 0){
		
		flee_counter+=menuControl.timer_diff;
		flee_dist+=flee_speed*menuControl.timer_diff;
		flee_alpha-=flee_spacer*menuControl.timer_diff;
		if(flee_alpha == 0){
			with(BattleHandler){
				fleeing = true;	
			}
			flee_complete = true
		}
	} else {
		flee_delay-=menuControl.timer_diff;	
	}
}