gml_pragma("forceinline");

phy_power = statCalculatePhysPower(0); //attack
phy_guard = statCalculatePhysGuard(0); //defense
mag_power = statCalculateMagPower(0); //magic
mag_guard = statCalculateMagGuard(0); //resistance
spe_agility = statCalculateAgiPower(0); //agility
spe_mpr = statCalculateAgiGuard(0); //mpr
hp = statCalculateHP();
mp = statCalculateMP();

var tablet_pp = 0;
var tablet_pg = 0;
var tablet_mp = 0;
var tablet_mg = 0;
var tablet_sp = 0;
var tablet_sg = 0;

for(var i = 0; i < 3; i++){
	if(tablet[i] > -1){
		tablet_pp+=	objdata_tablet.phy_power[tablet[i]]
		tablet_pg+=	objdata_tablet.phy_guard[tablet[i]]
		tablet_mp+=	objdata_tablet.mag_power[tablet[i]]
		tablet_mg+=	objdata_tablet.mag_guard[tablet[i]]
		tablet_sp+=	objdata_tablet.spe_power[tablet[i]]
		tablet_sg+=	objdata_tablet.spe_guard[tablet[i]]
	}
}

tablet_phy_power = statCalculatePhysPower(tablet_pp); //attack
tablet_phy_guard = statCalculatePhysGuard(tablet_pg); //defense
tablet_mag_power = statCalculateMagPower(tablet_mp); //magic
tablet_mag_guard = statCalculateMagGuard(tablet_mg); //resistance
tablet_spe_power = statCalculateAgiPower(tablet_sp); //agility
tablet_spe_guard = statCalculateAgiGuard(tablet_sg); //mpr