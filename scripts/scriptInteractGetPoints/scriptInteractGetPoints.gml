//get locations of the four sides of the obj

var spritewidth = image_xscale*sprite_get_width(sprite_index)*.5
var spriteheight = image_yscale*sprite_get_height(sprite_index)*.5

var center_x = x
var center_y = y

//var dist = point_distance(x-spritewidth,y-spriteheight, x+spritewidth,y-spriteheight);

var x1 = -spritewidth;
var y1 = -spriteheight;

var x2 = +spritewidth;
var y2 = -spriteheight;

var x5 = -spritewidth;
var y5 = +spriteheight;

var x6 = +spritewidth;
var y6 = +spriteheight;

var a = -degtorad(image_angle-90);

var x3 = y1*cos(a) - x1*sin(a);
var y3 = y1*sin(a) + x1*cos(a);

var x4 = y2*cos(a) - x2*sin(a);
var y4 = y2*sin(a) + x2*cos(a);

var x7 = y5*cos(a) - x5*sin(a);
var y7 = (y5*sin(a) + x5*cos(a));

var x8 = y6*cos(a) - x6*sin(a);
var y8 = (y6*sin(a) + x6*cos(a));

p_x1 = x3+center_x
p_y1 = y3+center_y

p_x2 = x4+center_x
p_y2 = y4+center_y

p_x3 = x7+center_x
p_y3 = y7+center_y

p_x4 = x8+center_x
p_y4 = y8+center_y

pointsx[0]=x3+center_x
pointsy[0]=y3+center_y
pointsx[1]=x4+center_x
pointsy[1]=y4+center_y
pointsx[3]=x7+center_x
pointsy[3]=y7+center_y
pointsx[2]=x8+center_x
pointsy[2]=y8+center_y
