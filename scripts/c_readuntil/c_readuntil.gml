//reads until it finds the sequence in the array.

var checkbytes = argument[0]; //bytes to look out for

var bytes; //bytes that represent the last trailing bytes, length = checkbytes.length

var sizer = array_length_1d(checkbytes);

for(var i = 0; i < sizer; i++){
	bytes[i] = 0;	
}

var returnbytes; //what to return.
returnbytes[0] = -1;

var eof = false; //if the eof was reached
var findseq = false; //if the sequence was found (equals trailing bytes)
var counter = -1;
with(CutsceneParser){

	while(!eof && !findseq){
		
		if(file_bin_position(file) >= file_size-1){
			eof = true;	
		} else {
		
			counter++
			returnbytes[counter] = file_bin_read_byte(file)

			for(var i = 1; i < sizer; i++){
				bytes[i] = bytes[i-1];	
			}
		
			bytes[0] = returnbytes[counter]

		
			if(c_comparebytes(bytes,checkbytes)){
				findseq = true;	
			}
			if(file_bin_position(file) >= file_size-1){
				eof = true;	
			}
		}
	}
}
var returner;
returner[0] = -1;

for(var i = 0; i < array_length_1d(returnbytes)-1; i++){
	returner[i] = returnbytes[i];
}

return returner;