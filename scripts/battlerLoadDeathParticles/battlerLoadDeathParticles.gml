var death_color = -1;
var death_type = argument[0];

if(death_type == -1){
	death_type = TYPE_NONE
}

death_color = death_type.color;


var scaler = .75

part_death1 = part_type_create();
part_type_shape(part_death1,pt_shape_sphere);
part_type_size(part_death1,1,1.50,-0.05,0);
part_type_scale(part_death1,scaler,scaler);
part_type_color1(part_death1,death_color);
part_type_alpha3(part_death1,0,1,0);
part_type_speed(part_death1,0,0,0.10,0);
part_type_direction(part_death1,0,359,0,0);
part_type_gravity(part_death1,0,270);
part_type_orientation(part_death1,0,0,0,0,1);
part_type_blend(part_death1,0);
part_type_life(part_death1,120,120);

part_death2 = part_type_create();
part_type_shape(part_death2,pt_shape_disk);
part_type_size(part_death2,0.50,0.50,0,0);
part_type_scale(part_death2,scaler*.5,scaler*.5);
part_type_color1(part_death2,death_color);
part_type_alpha2(part_death2,1,0);
part_type_speed(part_death2,5,5,-0.15,0);
part_type_direction(part_death2,0,359,0,0);
part_type_gravity(part_death2,0,270);
part_type_orientation(part_death2,0,0,0,0,1);
part_type_blend(part_death2,0);
part_type_life(part_death2,60,60);


