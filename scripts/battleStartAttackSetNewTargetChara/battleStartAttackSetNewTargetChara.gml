var targe = argument[0]
var chara = argument[1]

if(is_undefined(targe)){
	//select a target based on the attack info
	if(target_ally){
		if(chara.is_enemy){
			targe = BattleHandler.enemy_list[|0];
		} else {
			targe = BattleHandler.party_list[|0];
		}
	} else {
		if(!chara.is_enemy){
			targe = BattleHandler.enemy_list[|0];
		} else {
			targe = BattleHandler.party_list[|0];
		}
	}
}

var returner = targe;
if(targe > 0){
if(!battlerIsAlive(targe)){

	var get_party = targe.is_enemy;

	var lister = -1;

	if(get_party){
		lister = BattleHandler.enemy_list	
	} else {
		lister = BattleHandler.party_list	
	}

	var newlist = ds_list_create();
	var lister_size = ds_list_size(lister);
	var obj = -1;
	for(var i =0; i < lister_size; i++){
		obj = lister[|i];
		if(!is_undefined(obj)){
			if(battlerIsAlive(obj) && obj.damage_calc == 0){
				ds_list_add(newlist,obj);	
			}
		}
	}

	var newsize = ds_list_size(newlist);
	if(newsize > 0){
		var nummer = irandom(newsize-1);
		returner = newlist[|nummer];
	}

	ds_list_destroy(newlist);
}
}

return returner;