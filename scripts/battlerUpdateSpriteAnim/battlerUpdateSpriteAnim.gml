if(sprite_anim_counter != -1){
	var scounter = ds_map_size(sprite_anim_map);
	if(scounter > 0){
		var last = sprite_anim_counter;
		sprite_anim_counter+=menuControl.timer_diff;
		for(var i = ceil(last); i < sprite_anim_counter; i++){
			var obj = ds_map_find_value(sprite_anim_map,i);
			if(!is_undefined(obj)){
				battlerDoSprAnimation(obj);
			}
			ds_map_delete(sprite_anim_map,sprite_anim_counter);
		}
	} else {
		sprite_anim_counter = -1;	
	}
}