var chara = argument[0];

var side = chara.is_enemy;

		var stringer = choose("@0 boosts their friends's mood!", "@0 boosts their friends's morale!","@0 boosts their friends's fighting spirit!")
		
		stringer = string_replace_all(stringer,"@0",chara.name);
		battleTextDisplayAddText(stringer);

with(Battler){
	if(is_enemy == side){
		var percent_recover = 0
	
		percent_recover = 10;
	
		var recover_amount = clamp(floor((percent_recover*.01)*mp.base),1,999);
	
		var recover_val = mp.current + recover_amount;
	
		battlerMPChange(id,recover_val);
		
		var stringer = "@0 recovers @1% energy!"
		
		stringer = string_replace_all(stringer,"@0",name);
		stringer = string_replace_all(stringer,"@1",string(percent_recover));
		battleTextDisplayAddText(stringer);
	}
}