

var donotmove = false;

if(abs(angle_difference(orientation,360-dir)) > 50){
	donotmove = true;
	orientation = 360-dir;
	rotation_speed = 0;
	corner_counter = 2
} else if(playerTestRotationRange(dir,rotation_snap_tolerance,orientation)){
	orientation = 360-dir;
	rotation_speed = 0;
}
	
orientation = (orientation + 720) mod 360;

move_x = 0
move_y = 0

if(!donotmove){
	move_x = cos(degtorad(orientation))*move_rate*menuControl.timer_diff;
	move_y = sin(degtorad(orientation))*move_rate*menuControl.timer_diff;
}