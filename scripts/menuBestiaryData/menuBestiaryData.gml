//get the data for the birds.

var birdd = argument[0]
var entryname = "creature."
if(birdd > -1){

	bird_desc = textForceCharsPerLine(objdata_enemy.desc[birdd],char_per_line);

	element_sprite = menuBattleMovesGetElementSprite(stringToType(objdata_enemy.type[birdd]))
	menuParentSetGradient($7f7f00,c_black)
	bird_sprite = objdata_enemy.sprite_idle_front[birdd]
	img_increment = objdata_enemy.sprite_default_speed[birdd]
	
	hudControllerAddData(entryname + "name",bird_name);
	hudControllerAddData(entryname + "number",string(bird_list[|menupos]+1));
	
	hudControllerAddData(entryname + "desc",bird_desc);
	hudControllerAddData(entryname + "element",string(stringToType(objdata_enemy.type[birdd])));
	bird_state = objdata_enemy.state[birdd]
} else {
	bird_state = DEX_STATE_UNSEEN
}
