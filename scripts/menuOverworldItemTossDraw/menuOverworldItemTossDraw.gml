if(animation_len == 0){
	menuParentDrawBox();
	
	//menuParentDrawText();
	//draw how many
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(400,300-60,title,3,3,0)
	draw_text_transformed_ratio(400,300+20+10,"(out of " + string(quantity) + ")",2,2,0)
	draw_set_halign(fa_right)
	draw_text_transformed_ratio(400+60,300-15+10,"x",3,3,0)
	
	draw_set_halign(fa_left)
	
	draw_text_transformed_ratio(400-50,300-15+10,string(menupos),3,3,0)
	
	
} else {
	menuParentDrawBoxOpen();
}

gpu_set_colorwriteenable(true,true,true,true)
with(menuOverworldItems){
	menuParentDrawInfo();
}