/// @description - get keybind value
/// @function keybind_get(bind_name)
/// @param bind_name

var getval = ds_map_find_value(global.gamepad_map,argument[0])

if(!is_undefined(getval)){
	return getval;
}

return -1;