// calculate projected tablets

character = obj_data.party[|menupos];
character_id = -1;
receive_id = character.character;
	
insert_index = -1;

for(var i =0; i < 3; i++){
	if(character.tablet[i] == -1){
		if(insert_index == -1){
			projected_tablets[i] = braille_id;
			insert_index = i;
		} else {
			projected_tablets[i] = character.tablet[i];	
		}
	} else {
		projected_tablets[i] = character.tablet[i];
	}
}

// do stats

var chara = character;
var radd = radius;
with(old_stat){
	character = chara
	boost_phy_power = character.tablet_phy_power
	boost_phy_guard = character.tablet_phy_guard
	boost_mag_power = character.tablet_mag_power
	boost_mag_guard	= character.tablet_mag_guard
	boost_spe_power = character.tablet_spe_power
	boost_spe_guard = character.tablet_spe_guard
	draw_hex_init(character,radd,character.phy_power,
				character.phy_guard ,
				character.mag_power ,
				character.mag_guard ,
				character.spe_agility ,
				character.spe_mpr);
	radius = radd;
}

// do newstat

var temp_pp = 0;
var temp_pg = 0;
var temp_mp = 0;
var temp_mg = 0;
var temp_sp = 0;
var temp_sg = 0;

for(var i = 0; i < 3; i++){
	if(projected_tablets[i] > -1){
		temp_pp+=	objdata_tablet.phy_power[projected_tablets[i]]
		temp_pg+=	objdata_tablet.phy_guard[projected_tablets[i]]
		temp_mp+=	objdata_tablet.mag_power[projected_tablets[i]]
		temp_mg+=	objdata_tablet.mag_guard[projected_tablets[i]]
		temp_sp+=	objdata_tablet.spe_power[projected_tablets[i]]
		temp_sg+=	objdata_tablet.spe_guard[projected_tablets[i]]
	}
}
with(chara){
	temp_phy_power = statCalculatePhysPower(temp_pp); //attack
	temp_phy_guard = statCalculatePhysGuard(temp_pg); //defense
	temp_mag_power = statCalculateMagPower(temp_mp); //magic
	temp_mag_guard = statCalculateMagGuard(temp_mg); //resistance
	temp_spe_power = statCalculateAgiPower(temp_sp); //agility
	temp_spe_guard = statCalculateAgiGuard(temp_sg); //mpr
}
with(new_stat){
	character = chara
	boost_phy_power = character.temp_phy_power
	boost_phy_guard = character.temp_phy_guard
	boost_mag_power = character.temp_mag_power
	boost_mag_guard	= character.temp_mag_guard
	boost_spe_power = character.temp_spe_power
	boost_spe_guard = character.temp_spe_guard
	draw_hex_init(character,radd,character.phy_power,
				character.phy_guard ,
				character.mag_power ,
				character.mag_guard ,
				character.spe_agility ,
				character.spe_mpr);
	radius = radd;
}




	
hudControllerAddData("tablet.action","Giving ");
hudPopulateOverworldStats("character",obj_data.party[|menupos]);
	
hudPopulateOverworldStatsValues("character_new",obj_data.party[|menupos],
character.temp_phy_power,
character.temp_phy_guard,
character.temp_mag_power,
character.temp_mag_guard,
character.temp_spe_power,
character.temp_spe_guard,
projected_tablets[0],
projected_tablets[1],
projected_tablets[2]
	);
infokeys_assign(menu_ax_HUD);