
	//characters

	characters = ds_list_create();

	var obj = -1;

	//all characters
	for(var i = 0; i < objdata_character.data_size; i++){
		obj = instance_create_depth(0,0,0,dataCharacter);
		obj.character = i;
		with(obj){
			newalarm[0] = 1;
		}
	
		ds_list_add(characters,obj);
	}

	//create party
	party = ds_list_create();

	//initialize party

	ds_list_add(party,characters[|0],characters[|1]);
	characters[|0].recruited = true;
	characters[|1].recruited = true;
	characters[|2].recruited = true;