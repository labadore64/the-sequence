var blockToRun = argument[0];
var returnHeader = argument[1];
var pointerval = argument[2];

//gets the actual block to run as an object instead of an index.
with(CutsceneParser){
	blockToRun = ds_list_find_value(block_list,blockToRun);
}

with(blockToRun){
	//if it's less than zero, treat the return header as null.
	//if the return header is null, when it reaches the end without
	//branching, it will immediately end the script and close.
	if(returnHeader < 0){
		returnHeader = noone;	
	}

	//if pointer is less than 0, it is set to 0.
	//if pointer is longer than command list, it is
	//set to the length of the command list.
	//this will cause this block to exit immediately
	//though
	if(pointerval < 0){
		pointerval = 0;	
	} else if (pointerval > ds_list_size(command_list)){
		pointerval = ds_list_size(command_list);	
	}

	//this starts the block to run.
	return_block = returnHeader;	
	pointer = pointerval;
	active = true;

	//if its not in the background, add it to the
	//cutscene parser's list of active blocks.
	//when all the active blocks are destroyed,
	//the processing will continue.

	//obviously, blocks with the background annote
	//are ignored and just keep running!
	if(!blockToRun.background){
		with(CutsceneParser){
			ds_list_add(execution_stack,blockToRun);
		}
	}
}