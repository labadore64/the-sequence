
	//tts_say(menu_name_array[menupos] + " " + menu_desc_array[menupos]);//updates the option based on direction.
	//-1 = left, 1 = right
	var list_to_check = -1;
	var curpos = 0;

	if(menupos == 0){
		//speed
		list_to_check=text_speed_strings;
		curpos = global.disableShader;
	} else if(menupos == 3){
		//color	
		list_to_check=text_color_strings;
		curpos = global.fullscreen;
	} else if (menupos == 1){
		//font	
		list_to_check = text_font_strings;
		curpos = round(global.brightness*100)
	} else if(menupos == 2) {
		//sound	
		list_to_check = text_sound_strings;
		curpos = round(global.contrast*100)
	} else if(menupos == 4){
		//speed
		list_to_check=text_speed_strings;
		curpos = global.menuAnimation;
	} 

	var directionz = argument[0];
	var list_size = ds_list_size(list_to_check);

	if(directionz == -1){
		//if moving left	
		curpos--;
		if(curpos < 0){
			if(wrapz[menupos]){
				curpos = list_size-1;
			} else {
				curpos = 0;	
			}
		}
	} else if (directionz == 1){
		//if moving right	
		curpos++;
		if(curpos > list_size-1){
			if(wrapz[menupos]){
				curpos = 0;	
			} else {
				curpos = list_size - 1;	
			}
		}
	}
	//finally set values accordingly
	if(menupos == 0){
		global.disableShader = curpos;
		tts_say(text_speed_strings[|curpos])
	} else if (menupos == 3){
		global.fullscreen = curpos;
		if(curpos == 1){
			tts_say(text_speed_strings[|0])
		} else {
			tts_say(text_speed_strings[|1]);	
		}
	} else if (menupos == 1){
		global.brightness = curpos*.01;
		tts_say(string(curpos))
	} else if (menupos == 2){
		global.contrast = curpos*.01;
		tts_say(string(curpos))
	} else if(menupos == 4){
		global.menuAnimation = curpos;
		tts_say(text_speed_strings[|curpos])
	} 
	curposs[0] = global.disableShader;
	curposs[3] = global.fullscreen;
	curposs[1] = round(global.brightness*100)
	curposs[2] = round(global.contrast*100);
	curposs[4] = global.menuAnimation

	window_set_fullscreen(global.fullscreen);

	menuParentUpdateLookAndFeel()

	soundfxPlay(sound_select)


