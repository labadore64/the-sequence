
if(part_damage_trigger){
	
	if(orientation < 180){
		part_closer = false;	
	} else {
		part_closer = true;	
	}
	
    distancex = ori_cos*BattleHandler.part_distance[0]
    distancey = ori_sin*BattleHandler.part_distance[0]
    
    var part_center_x = x+distancex
    var part_center_y = y+distancey - 128*image_yscale - jump_y*image_yscale +35
    var part_size_xscale = image_xscale
    var part_size_yscale = image_yscale
    
    var part_x_len = BattleHandler.part_width[0] * part_size_xscale*.5
    var part_y_len = BattleHandler.part_height[0] * part_size_yscale*.5
    
    //draw_rectangle(true)
	//part_system_clear(damage_part_system);
    
    part_emitter_region(damage_part_system,damage_part_emitter,part_center_x-part_x_len*.5,part_center_x+part_x_len*.5,part_center_y-part_y_len*.5,part_center_y+part_y_len*.5,ps_shape_ellipse,ps_distr_linear)
	
    part_direction = round(radtodeg(orientation)) mod 360; //direction of particles.
    
    part_direction = cos(degtorad(part_direction))*30 +90
	
	
	var partsize;
	if(!part_closer){
		partsize = 1
	} else {
		partsize = -1
	}
            
	part_type_size(BattleHandler.damage_particle, .75, 1.5, partsize*part_base_speed, 0);
	part_type_direction(BattleHandler.damage_particle, part_direction-10,part_direction + 10, 0, 0);
	part_type_color_hsv(BattleHandler.damage_particle,0,255,127,255,255,255)
            
            
	part_emitter_burst(damage_part_system, damage_part_emitter, BattleHandler.damage_particle, BattleHandler.part_quantity[0]);
	
	part_time-=menuControl.timer_diff;
	if(part_time < 0){
		part_time = 0
		part_damage_trigger = false;
	}
	
	if(!battlerHasFilter(id,"stone")){
		id.sprite_state = BATTLER_SPRITE_HIT
		id.is_hurting = true;
	}
}
	repeat(menuControl.timer_diff_round){
		part_system_update(damage_part_system)
	}