
//defeat animation
if(defeat_animation_trigger &&
	!defeat_animation_complete){
	if(defeat_animation_counter >= defeat_animation_time){
		defeat_animation_complete = true;	
	}
	
	defeat_animation_counter+=menuControl.timer_diff
	defeat_animation_alpha = cos((defeat_animation_counter*pi*.5)/defeat_animation_time);
}
