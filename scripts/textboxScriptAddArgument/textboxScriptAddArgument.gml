/// @description - adds an argument to the list of arguments for the textbox
/// @param textbox
/// @param name
/// @param value

var arg_name = argument[1];
var arg_value = argument[2]

with(argument[0]){
	ds_map_replace(arguments,arg_name,arg_value);
}