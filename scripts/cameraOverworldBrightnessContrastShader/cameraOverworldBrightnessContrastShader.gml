surface_set_target(surface2)
draw_clear(c_white)
var modifier = 3.5;

if(shd_bc_shader_enabled){
	shader_set(shd_bright_contrast)
    shader_set_uniform_f(shd_bc_uni_time, shd_bc_var_time_var);
    shader_set_uniform_f(shd_bc_uni_mouse_pos, shd_bc_var_mouse_pos_x, shd_bc_var_mouse_pos_y);
    shader_set_uniform_f(shd_bc_uni_resolution, shd_bc_var_resolution_x, shd_bc_var_resolution_y);
    shader_set_uniform_f(shd_bc_uni_brightness_amount, shd_bc_var_brightness_amount+(global.brightness-.05)*modifier);
    shader_set_uniform_f(shd_bc_uni_contrast_amount, shd_bc_var_contrast_amount+(global.contrast-.05)*modifier);
	draw_surface(surface,0,0)
}
shader_reset();

surface_reset_target()