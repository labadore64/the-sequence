var chara = argument[0];

var returner = true;

if(battlerHasFilter(chara,"repeat") ||
	battlerHasFilter(chara,"stone") ||
	battlerHasFilter(chara,"sleep")){
	returner = false;
}

return returner;