
if(move_cando[|menupos]){
	if(!menuBattleSkipTarget(moves[|menupos])){
		var target = menuCreateInstance(menuBattleTarget);
		
		var getmove = moves[|menupos];

		target.move = getmove;
		drawme = false;
	} else {
		menuBattleTargetScript();	
	}
} else {
	if(character.item[menupos] == -1){
		textboxError("invalid_notanitem");
	} else {
		textboxError("invalid_cantuse");
	}
}