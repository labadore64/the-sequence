gml_pragma("forceinline");
event_inherited();

draw_script = drawPropParentMain;
active = true;

name = "";
read_text = false;
read_dist = 25;

event_id = "none";
interact_object = noone; //whether or not an object is to be interacted with. Destroyed if interact object is destroyed.

newalarm[0] = 1;

image_speed = 1