	var modstring = argument[0];
	var char = argument[1];
	
	modstring = string_letters(modstring) + char;
		
	if(string_length(modstring) > count){
		modstring = string_copy(
								modstring,
								(string_length(modstring) -  count)+1,
								count);
	} else {
		while(string_length(modstring) < count){
			modstring = " " + modstring;	
		}
	}
	
	return modstring;