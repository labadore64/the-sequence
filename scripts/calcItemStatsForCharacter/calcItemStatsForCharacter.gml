/// @description - fixes character
/// @param character

var chara = argument[0];

var lister = ds_list_create();

with(chara){
	boost_phy_power = 0; //attack
	boost_phy_guard = 0; //defense
	boost_mag_power = 0; //magic
	boost_mag_guard = 0; //resistance
	boost_spe_agility = 0; //agility
	boost_spe_mpr = 0; //mpr
	

	var itt = -1;
	
	for(var i = 0; i < 5; i++){
		if(ds_list_find_index(lister,item[i]) == -1){
			ds_list_add(lister,item[i]);	
		}
	}
	
	var sizer = ds_list_size(lister);
	
	var dataa = -1;
	for(var i = 0; i < sizer; i++){
		 dataa = instance_create(0,0,tmp);
		
		itt = lister[|i];
		with(dataa){
			itemData(itt);
		}
		
		boost_phy_power += dataa.stat_attack;
		boost_phy_guard += dataa.stat_defense;
		boost_mag_power += dataa.stat_magic;
		boost_mag_guard += dataa.stat_resistance;
		boost_spe_agility += dataa.stat_agility;
		boost_spe_mpr += dataa.stat_mpr;
		with(dataa){
			instance_destroy()	
		}
	}
}

ds_list_destroy(lister);