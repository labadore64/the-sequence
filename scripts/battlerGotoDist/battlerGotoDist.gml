
with(argument[0]){
	if(!is_lunging && can_move){
		is_lunging = true;
	
		lunge_target = argument[1];
		lunge_time = argument[2];
		lunge_dest = argument[3];
		lunge_spacer = (lunge_dest - distance)/lunge_time;
	}
}