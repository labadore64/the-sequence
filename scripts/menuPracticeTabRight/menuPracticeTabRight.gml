braille_menupos[braille_file] = menupos

braille_file--
if(0 > braille_file){
	braille_file = braille_total_charset-1;	
}

show_debug_message(string(braille_file))


braille_current_file = objdata_tablet.brailletype_file[braille_file]
braille_current_charset = objdata_tablet.brailletype_title[braille_file] 

with(braille_data){
	instance_destroy();	
}

braille_data = instance_create(0,0,brailleData);
braille_data.read_desc = true
braille_data.braille_source = braille_current_file;
braille_data.newalarm[0] = TIMER_INACTIVE
with(braille_data){
	event_perform(ev_alarm,0);	
}

active = false;

menupos = braille_menupos[braille_file];
// clear the braille cells
menu_size = menuGetSize();

var counter = 0;

var maxpage = floor((menu_size+1)/8)

for(var i = 0; i < braille_rows+maxpage; i++){
	for(var j = 0; j < braille_columns; j++){
		with(display_cell[i,j]){
			instance_destroy();	
		}
	}
}

braille_columns = objdata_tablet.brailletype_columns[braille_file]

event_perform(ev_alarm,11)