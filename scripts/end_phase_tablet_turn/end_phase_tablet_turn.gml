var nope = argument[0];

battleTextDisplayClearText();

with(nope){
	for(var i = 0; i < 3; i++){
		if(tablet[i] > -1){
			if(tablet_recover[i] > 0){
				tablet_recover[i]--;
			}
		}
	}
}