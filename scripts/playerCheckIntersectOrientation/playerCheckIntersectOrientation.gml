
		px = argument[0];
		py = argument[1];
		qx = argument[2];
		qy = argument[3];
		rx = argument[4];
		ry = argument[5];
	
	
	    intersect_val = (qy - py) * (rx - qx) - (qx - px) * (ry - qy);
 
	    if (intersect_val == 0) return 0;  // colinear
 
	    return (intersect_val > 0)? 1: 2; // clock or counterclock wise
