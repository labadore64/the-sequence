
name = "";
flavor = "";

//move init vars
damage = 0; //damage
mp_cost = 0; //mp cost
element = -1; //type of attack
move_type = MOVE_TYPE_PHYSICAL //physical or magical
target_number = 1; //amount of enemies affected.
target_skip = false; //skips targeting on target screen.
target_ally = false; //does this move target an ally?
target_self = false;
script_battle = -1; //note: all battle scripts must accept character and target

speed_modifier = 1; //how much your speed is affected by this move
speed_priority = 0; //move priority bracket


//animation stuff
bg_animation = -1; //animation for background
sprite_animation = -1; //animation on target
fg_animation = -1; //aniation on foreground
self_animation = -1; //animation on user

//lunge_animation = -1;//script for lunge animation to use
animation_length = 30; //length of animation
animation_knockback = true;

//item stuff
item_id = -1; //what item correlates with this obj

rotate_camera = false;
sprite_show = "";

//effect stuff
effect_percent = 0;

//filters
move_filter = 0;
move_filter[0] = "";
var itemcount = argument[0];
if(itemcount > -1){
	name = objdata_moves.name[itemcount]

	flavor = objdata_moves.flavor[itemcount]

	damage = objdata_moves.damage[itemcount]
	mp_cost = objdata_moves.mp_cost[itemcount]

	element = objdata_moves.element[itemcount]
	move_type = objdata_moves.move_type[itemcount]

	target_number = objdata_moves.target_number[itemcount]

	target_skip = objdata_moves.target_skip[itemcount]
	target_ally = objdata_moves.target_ally[itemcount]
	target_self = objdata_moves.target_self[itemcount]
	script_battle = objdata_moves.script_battle[itemcount]
	rotate_camera = objdata_moves.rotate_camera[itemcount]

	sprite_show = objdata_moves.sprite_show[itemcount]

	speed_modifier = objdata_moves.speed_modifier[itemcount] //how much your speed is affected by this move
	speed_priority = objdata_moves.speed_priority[itemcount] //move priority bracket
	char = objdata_moves.char[itemcount]

	//animation stuff
	bg_animation = objdata_moves.bg_animation[itemcount]  //animation for background
	sprite_animation = objdata_moves.sprite_animation[itemcount]  //animation on target
	fg_animation = objdata_moves.fg_animation[itemcount]  //aniation on foreground
	self_animation = objdata_moves.self_animation[itemcount]  //animation on user

	animation_length = objdata_moves.animation_length[itemcount]  //length of animation
	animation_knockback = objdata_moves.animation_knockback[itemcount] 

	//item stuff
	item_id = objdata_moves.item_id[itemcount]  //what item correlates with this obj
	effect_percent = objdata_moves.effect_percent[itemcount] 

	var filters = moveGetFilters(itemcount);

	if(ds_exists(filters,ds_type_list)){
		var sizer = ds_list_size(filters);
	
		for(var z = 0; z < sizer; z++){
			if(!is_undefined(filters[|z])){
				move_filter[z] = filters[|z];	
			} else {
				move_filter[z] = "";	
			}
		}
	}

	ds_list_destroy(filters);
}
//move_filter