/// @description - set volume of music
/// @param volume

var vol = argument[0]

/*
audio_sound_gain(soundMenuDefaultMove,vol,0);
audio_sound_gain(soundMenuDefaultCancel,vol,0);
audio_sound_gain(soundMenuInvalid,vol,0);
audio_sound_gain(soundFootstep01,vol,0);
audio_sound_gain(soundFootstep02,vol,0);
audio_sound_gain(soundFootstep03,vol,0);
audio_sound_gain(soundPickupItem,vol,0);

//voices
var volvoice = vol;

audio_sound_gain(textbox_sound_default,volvoice,0);
audio_sound_gain(speech_dipper1,volvoice,0);
audio_sound_gain(speech_dipper2,volvoice,0);
audio_sound_gain(speech_dipper3,volvoice,0);
audio_sound_gain(speech_dipper4,volvoice,0);
audio_sound_gain(speech_skullbird1,volvoice,0);
audio_sound_gain(speech_skullbird2,volvoice,0);
audio_sound_gain(speech_skullbird3,volvoice,0);
audio_sound_gain(speech_skullbird4,volvoice,0);
audio_sound_gain(speech_tricorn1,volvoice,0);
audio_sound_gain(speech_tricorn2,volvoice,0);
audio_sound_gain(speech_tricorn3,volvoice,0);
audio_sound_gain(speech_tricorn4,volvoice,0);
*/
//update env sounds

audio_sound_gain(sound3D_crow_caw,global.envVolume,0);