var scripter = argument[0];
var targe = argument[1];
var index = argument[2];

var obj = instance_create(0,0,battleEndPhaseHolder);
obj.script = scripter;
obj.target = targe;
obj.end_id = index;

ds_queue_enqueue(end_phase_stuff,obj);

return obj;