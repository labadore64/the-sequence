var sizer = ds_list_size(menus);
var obj;

for(var i = 0; i < sizer; i++){
	obj = menus[|i];
	
	if(!is_undefined(obj)){
		with(obj){
			instance_destroy();	
		}
	}
}

ds_list_clear(menus);