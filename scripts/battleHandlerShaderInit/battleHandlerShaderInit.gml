//blur shader
shader_enabled = shader_is_compiled(shd_gaussian_horizontal);
uni_resolution_hoz = shader_get_uniform(shd_gaussian_horizontal,"resolution");
var_resolution_x = camera_get_view_width(0);
var_resolution_y = camera_get_view_height(0);

uni_blur_amount_hoz = shader_get_uniform(shd_gaussian_vertical,"blur_amount");
var_blur_amount_multiplier = .5

min_delta = 1;


//brightness/contrast shader
shd_bc_uni_time = shader_get_uniform(shd_bright_contrast,"time");
shd_bc_var_time_var = 0;

shd_bc_uni_mouse_pos = shader_get_uniform(shd_bright_contrast,"mouse_pos");
shd_bc_var_mouse_pos_x = 400
shd_bc_var_mouse_pos_y = 300;

shd_bc_uni_resolution = shader_get_uniform(shd_bright_contrast,"resolution");
shd_bc_var_resolution_x = global.window_width;
shd_bc_var_resolution_y = global.window_height;

shd_bc_uni_brightness_amount = shader_get_uniform(shd_bright_contrast,"brightness_amount");
shd_bc_var_brightness_amount =0;

shd_bc_uni_contrast_amount = shader_get_uniform(shd_bright_contrast,"contrast_amount");
shd_bc_var_contrast_amount = 0;

shd_bc_increment = .01

shd_outline_alpha = shader_get_uniform(shd_shadow,"alpha");
shd_outline_enabled = shader_is_compiled(shd_shadow)

shd_shadow_alpha = shader_get_uniform(shd_shadow_img,"alpha");
shd_shadow_uniUV = shader_get_uniform(shd_shadow_img, "u_uv");
shd_shadow_enabled = shader_is_compiled(shd_shadow_img)

shd_bc_shader_enabled = shader_is_compiled(shd_bright_contrast);

//shd_beep_boop_resolution = shader_get_uniform(shd_default_beepboop,"resolution")