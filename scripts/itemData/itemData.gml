
obj_id = -1;
can_grow = false; //is it a plant
base_item = argument[0]; //object that this item recycles to
color_value = c_black //color of the item
price = 1 //price
overworld_effect = -1; //if not -1 executes a script.
battle_effect = -1; //if not -1 executes a script.
can_toss = true; //can be tossed.
can_give = true; //can be given to a character.
can_sell = true; //can be sold/tossed. 
item_val = argument[0]
single_use = false;//determines if item is single use on the overworld.
item_type = 0; //what kind of item it is for sorting
book_name = "test_book"; //book name for item effects

//following are for stat boosts from items.
stat_attack = 0;
stat_defense = 0;
stat_magic = 0;
stat_resistance = 0
stat_agility = 0
stat_mpr = 0
name = ""
flavor = ""

item_sprite = -1

//type boosts
for(var i = 0; i < 8; i++){
    att_boost[i] = 1;
    def_boost[i] = 1
}
var itemcount = argument[0];

if(!is_undefined(itemcount)){
	if(itemcount >= 0){
		name = objdata_items.name[itemcount]
		flavor = objdata_items.flavor[itemcount]

		item_sprite = objdata_items.item_sprite[itemcount]
			//

		book_name = objdata_items.book_name[itemcount]

		obj_id = objdata_items.obj_id[itemcount]

		overworld_effect = objdata_items.overworld_effect[itemcount]

		battle_effect = objdata_items.battle_effect[itemcount]

		price = objdata_items.price[itemcount]
		can_toss = objdata_items.can_toss[itemcount]
		can_give = objdata_items.can_give[itemcount]
		can_sell = objdata_items.can_sell[itemcount]

		single_use = objdata_items.single_use[itemcount]
		item_type = objdata_items.item_type[itemcount]

			//following are for stat boosts from items.
		stat_attack = objdata_items.stat_attack[itemcount]
		stat_defense = objdata_items.stat_defense[itemcount]
		stat_magic = objdata_items.stat_magic[itemcount]
		stat_resistance = objdata_items.stat_resistance[itemcount]
		stat_agility = objdata_items.stat_agility[itemcount]
		stat_mpr = objdata_items.stat_mpr[itemcount]
	}
}