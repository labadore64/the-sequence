
mouseHandler_clear_main()
menuAddOption(global.langini[0,31],global.langini[1,31],menuOverworldOptionTextLeft)
menuAddOption(global.langini[0,32],global.langini[1,32],menuOverworldOptionTextLeft)
menuAddOption(global.langini[0,33],global.langini[1,33],menuOverworldOptionTextLeft)
menuAddOption(global.langini[0,34],global.langini[1,34],menuOverworldOptionTextLeft)
menuAddOption(global.langini[0,35],global.langini[1,35],menuOverworldOptionTextLeft)
menuAddOption(global.langini[0,57],global.langini[1,57],menuOverworldOptionTextLeft)
menuAddOption("Tutorials","Enable Tutorials.",-1)

text_speed_strings = ds_list_create();
ds_list_add(text_speed_strings,global.langini[12,20],global.langini[12,6],global.langini[12,7],global.langini[12,8],global.langini[12,9]);

text_color_strings = ds_list_create();
text_color_values = ds_list_create()

ds_list_add(text_color_values,$FFFFFF,$AAAAFF,$AAFFFF,$AAFFAA,$FFFFAA,$FFAAAA,$FFAAFF,$AAAAAA);

ds_list_add(text_color_strings,
						global.langini[12,10],
						global.langini[12,11],
						global.langini[12,12],
						global.langini[12,13],
						global.langini[12,14],
						global.langini[12,15],
						global.langini[12,16],
						global.langini[12,17]);
text_font_strings = ds_list_create();
text_font_values = ds_list_create();

ds_list_add(text_font_values,global.normalFont);
ds_list_add(text_font_strings,global.langini[12,18]);

text_sound_strings = ds_list_create();

ds_list_add(text_sound_strings,LANG_SELECT_OFF,LANG_SELECT_ON);

text_metric_strings = ds_list_create();
text_metric_values = ds_list_create();

ds_list_add(text_metric_strings,LANG_SELECT_OFF,LANG_SELECT_ON,);
ds_list_add(text_metric_values,false,true);

text_tts_strings = ds_list_create();
text_tts_values = ds_list_create();

ds_list_add(text_tts_strings,LANG_SELECT_OFF,LANG_SELECT_ON);
ds_list_add(text_tts_values,false,true);

listsss = ds_list_create()

ds_list_add(listsss,text_speed_strings,text_color_strings,text_font_strings,text_sound_strings,text_metric_strings,text_tts_strings,text_tts_strings)

//whether or not the value should wrap around when adjusted.
wrapz[0] = true
wrapz[1] = true;
wrapz[2] = true;
wrapz[3] = true;
wrapz[4] = true;
wrapz[5] = true;
wrapz[6] = true;

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);
menuParentSetSelectWidth(.5)
menu_left = menuOverworldOptionTextLeft;
menu_right = menuOverworldOptionTextRight;

menu_up = menuOverworldOptionUp
menu_down = menuOverworldOptionDown

menu_draw = menuOverworldOptionTextDraw;

curposs[0] =  global.textSpeed+1;
curposs[1] = ds_list_find_index(text_color_values,global.textColor);
if(curposs[1] == -1){
	curposs[1] = 0;	
}
curposs[2] = ds_list_find_index(text_font_values,global.textFont); 
if(curposs[2] == -1){
	curposs[2] = 0;	
}
curposs[3] =  global.voice;
curposs[4] = global.metric
curposs[5] = global.tts
curposs[6] = global.tutorial;

menuControl.transparent = true;
