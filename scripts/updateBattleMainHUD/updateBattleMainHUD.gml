hudControllerClearData();
if(instance_exists(obj_data)){
	if(obj_data.battle_exists){
		// character
		var sizer = ds_list_size(BattleHandler.party_list)
		var obj = noone;


		for(var i = 0; i < sizer; i++){
			obj = BattleHandler.party_list[|i];
			if(!is_undefined(obj)){
				hudPopulateBattle("character"+string(i),obj,true);
			}
		}
		// enemy
		sizer = ds_list_size(BattleHandler.enemy_list)

		for(var i = 0; i < sizer; i++){
			obj = BattleHandler.enemy_list[|i];
			if(!is_undefined(obj)){
				hudPopulateBattle("enemy"+string(i),obj,BattleHandler.see_enemy_stats);
			}
		}
		// signs

		with(brailleDisplayBattle){
			var full_string = "";
			var charr = "";
	
			for(var i = 0; i < 5; i++){
				charr = "\"" + string_char_at(display_string,i+1)+ "\" ";
				full_string += charr;
				hudControllerAddData("chain."+"char"+string(i),charr);
			}
			hudControllerAddData("chain."+"string",full_string);
		
			var result = wordTestInString(display_string);
		
			if(result != -1){	
				hudControllerAddData("chain."+"word",detected_word);
			}
		}
	}
}