var hide = argument[0]

var sizer = ds_list_size(hide);

var obj = -1;

for(var i =0; i < sizer; i++){
	obj = hide[|i];
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			obj.hide = true;	
		}
	}
}