// populates with a data character

var entryname = "game."; 

with(obj_data){
	minute = floor(time_current/60);
	hour = floor(minute/60);
	minute = minute mod 60;
		
	if(hour > 99){
		hour = 99	
	}
		
	hudControllerAddData(entryname + "hours",string(hour));
	hudControllerAddData(entryname + "minutes",string(minute));
		
	hudControllerAddData(entryname + "money",string(money));
		
	var len = ds_list_size(characters);
	var counter = 0;
	var obj;
	for(var i = 0; i < len; i++){
		obj = characters[|i];
		if(!is_undefined(obj)){
			if(obj.recruited){
				counter++;	
			}
		}
	}
		
	hudControllerAddData(entryname + "recruited",string(counter));
	
	var counter = 0;
	for(var i = 0; i < 26; i++){
		if(tablet_obtained[i]){
			counter++;	
		}
	}
	
	hudControllerAddData(entryname + "tablet",string(counter));
		
	hudControllerAddData(entryname + "location",roomGetName(room));
		
	var party1 = "";
	var party2 = "";
	var party3 = "";
	
	obj = party[|0];
	if(!is_undefined(obj)){
		party1 = obj.name;	
	}
	obj = party[|1];
	if(!is_undefined(obj)){
		party2 = obj.name;	
	}
	obj = party[|2];
	if(!is_undefined(obj)){
		party3 = obj.name;	
	}
		
	hudControllerAddData(entryname + "party1",party1);
	hudControllerAddData(entryname + "party2",party2);
	hudControllerAddData(entryname + "party3",party3);
}
