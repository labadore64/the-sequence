var returner = false;
var obj = moves[|menupos];
moveData(obj);

if(!target_ally){
	if(character != -1){
		if(character.ability == "blind" || battlerHasFilter(character,"blind")){
			returner = true;	
		}
	}
}

if(move_skip[|menupos]){
	returner = true;	
}

return returner;