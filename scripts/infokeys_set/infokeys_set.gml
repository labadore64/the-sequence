///@param list

with(HUDController){
	ds_list_clear(infokey_strings);
	ds_list_copy(infokey_strings,argument[0]);

	infokey_size = ds_list_size(infokey_strings);
}