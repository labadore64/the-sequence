with(menuOverworldItems){
	surface_update = true;
	var itemz = item_id [| menupos];
	
	itemData(itemz)
	
	if(overworld_effect != -1){
		var success = script_execute(overworld_effect);
		if(single_use && success){
			removeItem(itemz,1);	
		}
		
	} else {
		//check if standing within range of pos
		
		var xx = -5000;
		var yy = -5000;
		
		with(Player){
			xx = interact_x;
			yy = interact_y;
		}
		
		var playerInBounds = false;
		var obj = noone;
		
		with(OverworldTextUseItem){
			if(!playerInBounds){
				playerInBounds = point_in_triangle(xx, yy, p_x1, p_y1, p_x4, p_y4, p_x3, p_y3);

				if(!playerInBounds){
					playerInBounds = point_in_triangle(xx, yy, p_x2, p_y2, p_x4, p_y4, p_x3, p_y3);
				}
			}
			
			if(playerInBounds){
				obj = id;	
			}
		}
		
		if(playerInBounds){
			if(itemz == obj.item_id){
				
				with(menuOverworldItemSub){
					instance_destroy()	
				}
				with(menuOverworldItems){
					instance_destroy()	
				}
				with(menuOverworldMain){
					instance_destroy()	
				}
				var ob = textboxOverworld(obj.item_text_id_after)
				ob.triggered_by_player = true;
				eventNormalSet(obj.event_id)
				
				if(obj.item_consume){
					removeItem(obj.item_id,1);	
				}
			} else {
				textboxError("invalid_cantuse");
			}
		} else {
			textboxError("invalid_cantuse");
		}
	}
}