//creates a new prop.

var prop_id = argument[0];
var xx = argument[1];
var yy = argument[2];

var obj = instance_create(xx,yy,prop_id);
obj.prop_id = string(irandom(999999999));

var map = obj_data.prop_place;

var json = ds_map_create();

ds_map_add(json,"x",xx);
ds_map_add(json,"y",yy);
ds_map_add(json,"obj_type",prop_id);

var stringer = json_encode(json);
ds_map_add(map,obj.prop_id,stringer);

ds_map_destroy(json);