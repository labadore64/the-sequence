//draw_clear(c_black);

if(!surface_exists(surface_view)){
	surface_view = surface_create_access(global.letterbox_w,global.letterbox_h)	
	view_set_surface_id(0, surface_view)
} else {
	if(ScaleManager.updated){
		view_set_surface_id(0, surface_view)
		surface_resize(surface_view,global.letterbox_w,global.letterbox_h);
	}
}
	
if(!surface_exists(surface)){
	surface = surface_create_access(global.letterbox_w,global.letterbox_h)	
} else {
	if(ScaleManager.updated){
		surface_resize(surface,global.letterbox_w,global.letterbox_h);
	}
}
	
if(!surface_exists(surface2)){
	surface2 = surface_create_access(global.letterbox_w,global.letterbox_h)	
} else {
	if(ScaleManager.updated){
		surface_resize(surface2,global.letterbox_w,global.letterbox_h);
	}
}

if(prop_place){
	if(prop_place_obj != -1){
		if(!surface_exists(prop_place_surface)){
			prop_place_surface = surface_create_access(ceil(256*global.scale_factor),ceil(256*global.scale_factor))	
		
			surface_set_target(prop_place_surface)
			draw_clear_alpha(c_black,0)
			var spr = object_get_sprite(prop_place_obj)
		
			draw_sprite(spr,0,128*global.scale_factor,128*global.scale_factor);
		
			surface_reset_target();
		}
	}
} else {
	if(surface_exists(prop_place_surface)){
		surface_free(prop_place_surface);	
	}
}

surface_set_target(surface);

draw_surface(surface_view,0,0);
//draws objects
//gpu_set_texfilter(true)
cameraOverworldDrawObj();
//gpu_set_texfilter(false)
if(prop_place){
	if(prop_place_obj != -1){
		
		if(surface_exists(prop_place_surface)){
			var strex = (1/zoomx)
			
			var center_offset = 128*strex*global.scale_factor;
			with(Player){
				var xx = (global.window_width*.5+cos(degtorad(orientation))*(prop_place_dist*strex))-center_offset;
				var yy = (global.window_height*.5+sin(degtorad(orientation))*(prop_place_dist*strex))-center_offset;
			}
			var scalerz = strex;
			
			draw_surface_ext(prop_place_surface,xx,yy,scalerz,scalerz,0,c_white,.5);	
		}
	}
}

surface_reset_target();
cameraOverworldShaderMain();

//draw_surface(surface,0,0)


gpu_set_colorwriteenable(true,true,true,true)
surface_set_target(application_surface)
draw_clear(c_black);
if(!binoculars){
	draw_surface(surface,global.display_x,global.display_y)
} else {
	
    if(!global.disableShader){
		shader_set(shd_binoculars);
		    shader_set_uniform_f(binoculars_u_vRatio, 1);
		    shader_set_uniform_f(binoculars_u_threshold, 0.1 * binoculars_amount);
		    texture_set_stage(binoculars_sampler0,binoculars_texture0);
		    draw_surface(surface,global.display_x,global.display_y)
		shader_reset();
	} else {
		draw_surface(surface,global.display_x,global.display_y)
	}
	draw_sprite_extended_ratio(spr_binoculars_foreground,0,0,0,1,1,0,c_white,1)

}

draw_set_color(c_black)
draw_set_alpha(1)
var height = 50;

draw_rectangle_topleft(0,0,800,height)
draw_rectangle_topleft(0,600-height,800,600)

surface_reset_target()


