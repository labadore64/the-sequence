if(battlerHasAbility(target,"static")){
	if(battlerIsAlive(target) && battlerIsAlive(character)){
		var damage_percent = 5;
		character.damage_calc += ceil(character.hp.base*(damage_percent*.01))
		var stringerr = "@0 takes @1% damage from static!";
			
		stringerr = string_replace_all(stringerr,"@0",character.name)
		stringerr = string_replace_all(stringerr,"@1",string(damage_percent))
			
		battleAttackInputAddString(stringerr);	
	}
}