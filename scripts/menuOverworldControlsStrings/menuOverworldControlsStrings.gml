update_image_only_active = false;
menuAddOption(global.langini[0,51],global.langini[1,51],menuOverworldOptionKeyboardScript)
menuAddOption(global.langini[0,52],global.langini[1,52],menuOverworldOptionControllerScript)


menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();

menu_draw = menuOverworldOptionDraw;

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menu_up = menuOverworldOptionRealUp;
menu_down = menuOverworldOptionRealDown;

menu_left = -1
menu_right = -1
