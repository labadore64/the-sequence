// the condition should be checked against the battler, assume
// that the battler is where this script is running.

// it will return true or false if this end phase condition triggers for this battler.
var arg = argument[0];

with(arg){
	for(var i = 0; i < 3; i++){
		if(tablet[i] > -1){
			if(tablet_recover[i] == 0){
				return true;
			}
		}
	}
}

return false;