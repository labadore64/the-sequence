var menupos_old = menupos;

menuParentMoveUp();

var obj = party[|menupos];
with(dataDrawStat){
drawHexStatSetBoosts(obj.phy_power.braille_stat,obj.phy_guard.braille_stat,obj.mag_power.braille_stat,obj.mag_guard.braille_stat,
							obj.spe_power.braille_stat,obj.spe_guard.braille_stat)
}					
menuBattleStatsUpdateStats(obj,min(obj.phy_power.base,obj.phy_power.braille_stat),
								min(obj.phy_guard.base,obj.phy_guard.braille_stat),
								min(obj.mag_power.base,obj.mag_power.braille_stat),
								min(obj.mag_guard.base,obj.mag_guard.braille_stat),
								min(obj.spe_power.base,obj.spe_power.braille_stat),
								min(obj.spe_guard.base,obj.spe_guard.braille_stat));
			
status_count = ds_list_size(obj.status_effects);
status_pos = 0;

current_status = obj.status_effects[|status_pos];	
if(!is_undefined(current_status)){
	if(current_status > 0){
		status_turns = string_replace_all(string(current_status.turns),"-1","@");
	}
}
with(portrait){
	instance_destroy();	
}
portrait = instance_create_depth(0,0,0,drawPortrait)
portrait.character = obj.character

if(menupos != menupos_old){
	translation = translation_start;
	translation_rate = translation_rate_start;
}

element_sprite = menuBattleMovesGetElementSprite(obj.element)

hudPopulateBattleStatsDetails("character",party[|menupos]);