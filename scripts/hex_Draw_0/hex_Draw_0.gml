 /// @description Insert description here
// You can write your code in this editor
var force_draw = false;

if(!surface_exists(surface_hex)){
	surface_hex = surface_create_access(radius*2.5*global.scale_factor,radius*2.5*global.scale_factor);	
	force_draw = true;
}

if(counter){
	force_draw = true;
}

if(force_draw){
	draw_hex_surf(surface_hex,radius*2.5*.5);
}