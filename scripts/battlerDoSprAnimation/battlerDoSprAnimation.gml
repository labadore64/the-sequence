var def = json_decode(argument[0]); // the contents

// sprite definition
var test = def[?"sprite"];

if(!is_undefined(test)){
	test = string_lower(test);
	switch(test){
		case "idle":
		sprite_state = BATTLER_SPRITE_IDLE;
		break;
		
		case "hit":
		sprite_state = BATTLER_SPRITE_HIT;
		break;
		
		case "jump0":
		sprite_state = BATTLER_SPRITE_JUMP0;
		break;
		
		case "jump1":
		sprite_state = BATTLER_SPRITE_JUMP1;
		break;
		
		case "cast0":
		sprite_state = BATTLER_SPRITE_CAST0;
		break;
		
		case "cast1":
		sprite_state = BATTLER_SPRITE_CAST1;
		break;
	}
}

// jump definition
test = def[?"jump"];

if(!is_undefined(test)){
	var height = def[?"jump_height"];
	
	if(!is_undefined(height)){
	
		if(height > -1 && test > -1){
			jump_time_anim = 0; //how much time to wait until jump animation is triggered
			jump_time_duration = max(floor(test),1);
			jump_time_height = height*10
		}
	}
}

// lunge definition
test = def[?"lunge"];

if(!is_undefined(test)){
	if(test > -1){
		if(targeting_enemy != noone){
			var lunge_pos = def[?"lunge_pos"];
			if(is_undefined(lunge_pos) || lunge_pos == 1){
				battleAnimLunge(id,targeting_enemy,test)
			} else {
				battlerGotoDist(id,targeting_enemy,test,lunge_pos)
			}
		}
	}
}

// sound effect
test = def[?"sound_fx"];

if(!is_undefined(test)){
	if(is_string(test)){
		if(test != ""){
			var asset = asset_get_index(test)
			if(asset > -1){
				soundfxPlay(asset)	
			}
		}
	}
}

// particles
test = def[?"particle"];

if(!is_undefined(test)){
	if(is_string(test)){
		if(test != ""){
			var part = battleHandlerGetParticle(test);
			var time = 1;

			var lunge_pos = def[?"particle_time"];
			if(!is_undefined(lunge_pos)){
				time = lunge_pos;
			}

			battlerParticleAddEffect(part,time)
		}
	}
}


ds_map_destroy(def);