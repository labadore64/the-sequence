//args can be an array of any length.

//create the cutscene exec environment.

var header = argument[0];

var inst = instance_create(0,0,CutsceneExecEnvironment)

c_addheader(inst,header);

with(CutsceneParser){
	ds_stack_push(execution_stack,inst);	
}