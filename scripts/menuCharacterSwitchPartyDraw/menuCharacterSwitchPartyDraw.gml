
if(animation_len == 0){
	draw_clear(c_black)
	draw_set_color(c_black)
	draw_rectangle_ratio(0,550,800,600,false)
	
	//start data
	
	gpu_set_colorwriteenable(true,true,true,true)
	
	with(portrait){
		draw_surface(surface,400,180)	
	}
	
	var movexx = -150;
	var moveyy = 20;
	
	var statsxoff = -25+movexx;
	var statsyoff = 25+3+moveyy;

	with(drawstat){
		draw_surface(surface_hex,450+statsxoff,250+statsyoff);	
	}

	//draw stat icons
	var statspritexoff = 500-50+movexx;
	var statspriteyoff = 300-10+moveyy;

	draw_sprite_extended_ratio(spr_magpower_icon,0,10+statspritexoff-75+22,35+statspriteyoff+62,1,1,0,$FF7FFF,1)
	draw_sprite_extended_ratio(spr_physguard_icon,0,170+statspritexoff-7,35+statspriteyoff+185-22,1,1,0,$FFFF7F,1)
	draw_sprite_extended_ratio(spr_magguard_icon,0,170+statspritexoff-165+7,35+statspriteyoff+185-22,1,1,0,$7FFF7F,1)
	draw_sprite_extended_ratio(spr_physpower_icon,0,170+statspritexoff+65-22,35+statspriteyoff+62,1,1,0,$7F7FFF,1)	
	draw_sprite_extended_ratio(spr_speguard_icon,0,170+statspritexoff-165+7,35+statspriteyoff-65+22,1,1,0,$FF7F7F,1)
	draw_sprite_extended_ratio(spr_spepower_icon,0,170+statspritexoff-7,35+statspriteyoff-65+22,1,1,0,$7FFFFF,1)	

	//draw color bars


	menuOverworldPartyDrawInfo();

	
	
	//menuCharacterSwitchPartyDrawLevel(adjusttt)
	
	//drawBar(160,375+adjusttt,60,16,1,char_color)
	
	//end data 
	
	draw_set_color(c_black)
	var ggg = 160+130;
	draw_rectangle_ratio(0,0,800,ggg,false);

	draw_set_color($7F7F7F)
	var toppp = 151
	draw_rectangle_ratio(0,toppp,800,toppp+2,false);
	draw_rectangle_ratio(0,ggg,800,ggg+2,false);

	menuOverworldPartyDrawParty();
	
	menuOverworldPartyDrawSelect();

	var aura_width = 128*.5;
	var aura_height = 32*.5;
	var xxx = 230+75;
	var yyy = 190
	var textxxx = -200;
	var textyyy = -10-10
	var boosttextxxx = 25;
	var boosttextyyy = 50

}
//menuParentDrawInfo();