/// @description - give an item to a character
/// @param item
/// @param character

var itemz = argument[0];
var chara = argument[1];

var cando = false;

with(chara){
	if(object_index == dataCharacter){
		for(var i = 0; i < 5; i++){
			if(item[i] == -1){
				item[i] = itemz;
				cando = true;
				break;
			}
		}
	}
}

if(cando){
	var did = removeItem(itemz,1);
	if(!did){
		//soundfxPlay(soundMenuDefaultCancel)	
		cando = false;
	}
}

statUpdate();

return cando;