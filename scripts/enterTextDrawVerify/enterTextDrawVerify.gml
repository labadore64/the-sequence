var sizerx = 300;
var sizery = 100;

draw_set_color(global.textColor)

draw_rectangle_ratio(x-sizerx-4,y-sizery-4,x+sizerx+4,y+sizery+4,false)
gpu_set_colorwriteenable(true,true,true,false)
draw_set_color(c_black)

draw_rectangle_ratio(x-sizerx,y-sizery,x+sizerx,y+sizery,false)

draw_set_color(global.textColor);

draw_set_halign(fa_center);

draw_text_transformed_ratio(x,y-85,"Is this OK?",3,3,0)
draw_text_transformed_ratio(x,y-20,current_text,2,2,0)


	if(!(blink && menupos == 0)){
		draw_text_transformed_ratio(x-100,y+30,LANG_SELECT_YES,3,3,0)
	}

	if(!(blink && menupos == 1)){
		draw_text_transformed_ratio(x+100,y+30,LANG_SELECT_NO,3,3,0)
	}


draw_set_halign(fa_left);