//returns a string that contains the user data

var chara = argument[0]
var filename = argument[1]

var chara_id = obj_data.characters[|chara];

/// @description Insert description here
// You can write your code in this editor
//represents the character stats.

var map = ds_map_secure_load(filename +"character" + string(chara) + ".sav");

if(ds_exists(map,ds_type_map)){
	with(chara_id){
		experience = map[? "experience"];
		//level = map[? "level"];
		recruited = map[? "recruited"];

		var item_list = map[? "items"];
		var sizer = ds_list_size(item_list);
		for(var i = 0; i < sizer; i++){
			item[i] = item_list[| i];	
		}
		
		item_list = map[? "tablets"];
		sizer = ds_list_size(item_list);
		for(var i = 0; i < sizer; i++){
			tablet[i] = item_list[| i];	
		}
	}
	ds_map_destroy(map);
	return true;
} else {
	return false;	
}