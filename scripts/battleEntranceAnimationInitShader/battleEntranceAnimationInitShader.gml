//brightness/contrast shader
shd_bc_uni_time = shader_get_uniform(shd_bright_contrast,"time");
shd_bc_var_time_var = 0;

shd_bc_uni_mouse_pos = shader_get_uniform(shd_bright_contrast,"mouse_pos");
shd_bc_var_mouse_pos_x = 400
shd_bc_var_mouse_pos_y = 300;

shd_bc_uni_resolution = shader_get_uniform(shd_bright_contrast,"resolution");

shd_bc_uni_brightness_amount = shader_get_uniform(shd_bright_contrast,"brightness_amount");
shd_bc_var_brightness_amount =0;

shd_bc_uni_contrast_amount = shader_get_uniform(shd_bright_contrast,"contrast_amount");
shd_bc_var_contrast_amount = 0;

shd_bc_increment = .01

shd_bc_shader_enabled = shader_is_compiled(shd_bright_contrast);

//radial blur

radi_uni_time = shader_get_uniform(shd_radial_blur,"time");
radi_var_time_var = 0;

radi_uni_mouse_pos = shader_get_uniform(shd_radial_blur,"mouse_pos");

radi_uni_resolution = shader_get_uniform(shd_radial_blur,"resolution");

radi_uni_radial_blur_offset = shader_get_uniform(shd_radial_blur,"radial_blur_offset");
radi_var_radial_blur_offset = 0

radi_uni_radial_brightness = shader_get_uniform(shd_radial_blur,"radial_brightness");
radi_var_radial_brightness = 0

if(shader_is_compiled(shd_radial_blur)){
    radi_shader_enabled = true;
} else {
    radi_shader_enabled = false;
}

shd_bc_var_resolution_x = global.window_width 
shd_bc_var_resolution_y = global.window_height

radi_var_resolution_x = global.window_width 
radi_var_resolution_y = global.window_height

radi_var_mouse_pos_x =  global.window_width*.5
radi_var_mouse_pos_y =  global.window_height*.5