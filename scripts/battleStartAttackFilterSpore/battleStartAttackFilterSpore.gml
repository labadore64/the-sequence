var chara = argument[0];
var targe = argument[1];



if(battlerHasFilter(chara,"Spore")){
	if(!battlerHasFilter(targe,"Spore")){
		if(move_type == MOVE_TYPE_PHYSICAL){
			var filter = battlerGetFilter(chara,"Spore");
			if(filterCanDoEffect(filter)){
				battlerTransferFilter(chara,targe,filter);
		
				var set_string = "The spores transfered to @0!";
		
				set_string = string_replace_all(set_string,"@0",targe.name);
	
				battleAttackInputAddString(set_string);
			}
		
		}
	}
} else if(battlerHasFilter(targe,"Spore")){
	if(!battlerHasFilter(chara,"Spore")){
		if(move_type == MOVE_TYPE_PHYSICAL){
			var filter = battlerGetFilter(targe,"Spore");
			if(filterCanDoEffect(filter)){
				battlerTransferFilter(targe,chara,filter);
		
				var set_string = "The spores transfered to @0!";
		
				set_string = string_replace_all(set_string,"@0",chara.name);
	
				battleAttackInputAddString(set_string);
			}
		
		}
	}
}