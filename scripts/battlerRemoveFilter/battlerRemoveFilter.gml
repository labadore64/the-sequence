var chara = argument[0];
var filtername = string_lower(argument[1]);

with(chara){
	var filsize = ds_list_size(status_effects);
	var filter = 0;
	var important_filter = 0;
	var index = 0;
	
	for(var i = 0; i < filsize; i++){
		filter = status_effects[|i];
		
		if(string_lower(filter.name) == filtername){
			with(filter){
				instance_destroy();	
			}
			index = i;
			break;
		}
	}
	
	ds_list_delete(status_effects,index);
	newalarm[1] = 1;
}