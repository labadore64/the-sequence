draw_set_color(c_black)
draw_set_alpha(.65)

var width = 150;

var dist_adjust = 15;

draw_rectangle_ratio(0,115,width,485,false)

draw_rectangle_ratio(800-width,115,800,485,false)

var statspritexoff = 13
var statspriteyoff = 80+dist_adjust


draw_set_alpha(1);

gpu_set_texfilter(true);
with(dataDrawStat){
	draw_surface_ext(surface_hex,13*global.scale_factor,(130+dist_adjust)*global.scale_factor,1,1,0,c_white,1)
}

var mcale = .6


draw_sprite_extended_ratio(spr_magpower_icon,0,10+statspritexoff-20,35+statspriteyoff+65,mcale,mcale,0,$FF7FFF,1)
draw_sprite_extended_ratio(spr_physguard_icon,0,170+statspritexoff-80,35+statspriteyoff+125,mcale,mcale,0,$FFFF7F,1)
draw_sprite_extended_ratio(spr_magguard_icon,0,170+statspritexoff-150,35+statspriteyoff+125,mcale,mcale,0,$7FFF7F,1)
draw_sprite_extended_ratio(spr_physpower_icon,0,170+statspritexoff-55,35+statspriteyoff+65,mcale,mcale,0,$7F7FFF,1)	
draw_sprite_extended_ratio(spr_speguard_icon,0,170+statspritexoff-145,statspriteyoff+47,mcale,mcale,0,$FF7F7F,1)
draw_sprite_extended_ratio(spr_spepower_icon,0,170+statspritexoff-80,statspriteyoff+47,mcale,mcale,0,$7FFFFF,1)	

gpu_set_texfilter(false);

//draw types

var dist = 80;
var init_dist = 368

var text_dist = -45;

draw_set_halign(fa_center);

draw_set_color(global.textColor);
draw_set_font(global.textFont);

draw_text_transformed_ratio(75,init_dist+text_dist,"TYPE",2,2,0)
draw_text_transformed_ratio(75,init_dist+text_dist+dist,"WEAK",2,2,0)

draw_sprite_extended_ratio(type_sprite,0,75,init_dist,1,1,0,c_white,1)
draw_sprite_extended_ratio(type_weak_sprite,0,75,init_dist+dist,1,1,0,c_white,1)

//draw the move names
var start_dist = 130;
var draw_spacer = 20
var adjust = 10;
draw_text_transformed_ratio(725,start_dist,"MOVES",2,2,0);
for(var i = 0; i < moves_names_size; i++){
	draw_text_transformed_ratio(725,adjust+start_dist+draw_spacer*(i+1),moves_names[|i],1,1,0);
}

draw_set_halign(fa_left);