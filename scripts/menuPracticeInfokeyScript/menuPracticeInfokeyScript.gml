// argument[0] = the index

if(argument[0] < 6){
	//infokey_script	
	var column = floor(clamp(argument[0] mod 2,0,1))
	var row = floor(clamp(argument[0] / 2,0,2))

	if(playing_sound != -1){
		audio_stop_sound(playing_sound);
	}
	playing_sound = audio_play_sound(cells_sound[column,row],2,false);
	
	var stringe = "on";
	
	if(braille_cell.cell[column,row]){
		audio_sound_pitch(playing_sound,1.25)	
	} else {
		audio_sound_pitch(playing_sound,1)
		stringe = "off"
	}
	
	if(global.readBrailleCell){
		tts_say(stringe + " ("+	string(row) + ", " +string(column) +")");
	}
}