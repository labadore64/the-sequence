var anim_size = array_length_2d(objdata_moves.anim_def,attack);
var targsize = ds_list_size(additional_targets);
character.targeting_enemy = noone;
if(anim_size > 0){
	var anim_string = "";
	var mappa;
	
	character.targeting_enemy = target;
	
	var frame;
	var charaz;
	var letarget = character;
	for(var i = 0; i < anim_size; i++){
		anim_string = objdata_moves.anim_def[attack,i]
		
		mappa = json_decode(anim_string);
		
		if(!is_undefined(mappa)){
			frame = mappa[? "frame"];
			charaz = mappa[? "target"];
			
			if(!is_undefined(frame) && !is_undefined(charaz)){
				if(charaz == "target1"){
					letarget = target;
				} else if (charaz == "target2" && targsize >= 1){
					letarget = additional_targets[|0]	
				} else if (charaz == "target3" && targsize >= 1){
					letarget = additional_targets[|1]	
				}
				
				if(is_undefined(letarget)){
					letarget = character;	
				}
				
				if(battlerIsAlive(letarget)){
					letarget.sprite_anim_counter = 0;
					ds_map_add(letarget.sprite_anim_map,frame,anim_string);
				}
			}
		}
	}
} else {
	battlerFlashOnce(character);
}