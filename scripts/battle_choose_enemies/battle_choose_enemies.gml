var choices = ds_list_create();

var choice1 = obj_data.default_spawn[0];
var choice2 = obj_data.default_spawn[1];
var choice3 = obj_data.default_spawn[2];

var shuffle = true;

if(!DEBUG_DEFAULT_MONSTER_ENCOUNTER){
	var encounters = objdataToIndex(objdata_encountermap,room_get_name(room));

	if(encounters != -1){
		var possibilities = array_length_2d(objdata_encountermap.encounter_a,encounters);
		var val = irandom(possibilities-1);
		
		var rate = objdata_encountermap.rate_a[encounters,val]
	
		if (obj_data.encounter_id == 1
			&& objdata_encountermap.encounter_type_count[encounters] > 1){
			possibilities = array_length_2d(objdata_encountermap.encounter_b,encounters);
			val = irandom(possibilities-1);;
			rate = objdata_encountermap.rate_b[encounters,val]
		} else if (obj_data.encounter_id == 2
			&& objdata_encountermap.encounter_type_count[encounters] > 2){
			possibilities = array_length_2d(objdata_encountermap.encounter_c,encounters);
			val = irandom(possibilities-1);;
			rate = objdata_encountermap.rate_c[encounters,val]
		}
		
		var encounter = "";
		
		var encounter_id = -1;
		
		while(encounter_id == -1){
			if(rate <= 0){
				rate = 1;	
			}
			
			if(random(1) < rate){
				
				if (obj_data.encounter_id == 0){
					encounter = objdata_encountermap.encounter_a[encounters,val];
				}
				else if (obj_data.encounter_id == 1
					&& objdata_encountermap.encounter_type_count[encounters] > 1){
					encounter = objdata_encountermap.encounter_b[encounters,val];
				} else if (obj_data.encounter_id == 2
					&& objdata_encountermap.encounter_type_count[encounters] > 2){
					encounter = objdata_encountermap.encounter_c[encounters,val];
				}
				
				encounter_id = objdataToIndex(objdata_encounter,encounter);
			
				// test events
				if(encounter_id > -1){
					var event = objdata_encounter.event_enabled[encounter_id];
					if(event != -1){
						// if enable event has been triggered
						if(!eventNormalCheckIndex(event)){
							//reset
							encounter = "";
							encounter_id = -1;
						}
					}
				
					event = objdata_encounter.event_disabled[encounter_id];
					if(event != -1){
						// if disable event has been triggered
						if(eventNormalCheckIndex(event)){
							//reset
							encounter = "";
							encounter_id = -1;
						}
					}
				} else {
					encounter_id = 0;
					show_debug_message("Encounter failed to load.");
				}
			}
			val = irandom(possibilities-1);
			if (obj_data.encounter_id == 0){
				var rate = objdata_encountermap.rate_a[encounters,val]
			}else if (obj_data.encounter_id == 1
				&& objdata_encountermap.encounter_type_count[encounters] > 1){
				possibilities = array_length_2d(objdata_encountermap.encounter_b,encounters);
				val = irandom(possibilities-1);;
				rate = objdata_encountermap.rate_b[encounters,val]
			} else if (obj_data.encounter_id == 2
				&& objdata_encountermap.encounter_type_count[encounters] > 2){
				possibilities = array_length_2d(objdata_encountermap.encounter_c,encounters);
				val = irandom(possibilities-1);;
				rate = objdata_encountermap.rate_c[encounters,val]
			}
		}
	
		if(encounter_id > -1){
			for(var i = 0; i < 3; i++){
				ds_list_add(choices,objdata_encounter.enemy[encounter_id,i]);
			}
		}
	
		shuffle = objdata_encounter.random_order[encounter_id]
	}
}
if(ds_list_empty(choices)){
	ds_list_add(choices,
			obj_data.default_spawn[0],
			obj_data.default_spawn[1],
			obj_data.default_spawn[2]);
}

if(shuffle){
	ds_list_shuffle(choices);
}

with(obj_data){
	monster_spawn[0] = choices[|0]
	monster_spawn[1] = choices[|1]
	monster_spawn[2] = choices[|2]
}
ds_list_destroy(choices);