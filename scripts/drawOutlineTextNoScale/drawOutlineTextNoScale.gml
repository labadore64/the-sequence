/// @description - set current menu gradient
/// @param x
/// @param y
/// @param string
/// @param xscale
/// @param yscale
/// @param rotation
/// @param color

draw_set_color(c_black)
draw_text_transformed(argument[0]+2,argument[1],argument[2],argument[3],argument[4],argument[5])
draw_text_transformed(argument[0]-2,argument[1],argument[2],argument[3],argument[4],argument[5])
draw_text_transformed(argument[0],argument[1]-2,argument[2],argument[3],argument[4],argument[5])
draw_text_transformed(argument[0],argument[1]+2,argument[2],argument[3],argument[4],argument[5])
draw_set_color(argument[6])
draw_text_transformed(argument[0],argument[1],argument[2],argument[3],argument[4],argument[5])