/// @description - script executed when pressing up
/// @param x
/// @param y
/// @param color
/// @param sprite
/// @param current_value
/// @param max_value

gpu_set_texfilter(true);

var xx = argument[0];
var yy = argument[1];
var colorz = argument[2];
var spritez = argument[3];
var curr = argument[4];
var maxx = argument[5];

if(maxx == 0){
	maxx = 1;	
}

var tester = curr/maxx;

if(tester > 1){
	tester = 1;	
}

draw_sprite_extended_ratio(spritez,0,xx,yy,.75,.75,0,colorz ,1);

var trans = 30;
var moo = 10;
var dist = 150;


draw_set_color($111111)
draw_rectangle_ratio(xx+trans-2,yy-5+moo-2,2+trans+xx+dist,2+yy+5+moo,false)
draw_set_color(c_black)
draw_rectangle_ratio(xx+trans-1,yy-5+moo-1,1+trans+xx+dist,1+yy+5+moo,false)

draw_set_color(colorz)
if(tester > 0){
	draw_rectangle_ratio(xx+trans,yy-5+moo,trans+xx+dist*tester,yy+5+moo,false)
}
gpu_set_texfilter(false);
