event_inherited();

flash = false;
flash_timer = 0;
flash_amount = 30;

enter_script = menuOverworldItemScriptSub;



skip = 8;

item_id = ds_list_create();
item_quantity = ds_list_create();

//menuOverworldItemInventory(menuOverworldItemScriptSub);

itemData(item_id[|0]);

menu_size = menuGetSize();

animation_start = 3 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,1])
menuParentSetSubtitle(global.langini[3,1])
menuParentSetInfoboxSubtitleYOffset(45)
menuParentSetSelectWidth(.45)

menuParentSetOptionYOffset(160)

//cash label var global.langini[6,i]

menu_draw =menuOverworldItemDrawMain;


menuParentUpdateBoxDimension();

itemData(item_id [| menupos])

menuOverworldItemsListStart(menuOverworldItemScriptSub);

menuParentTTSTitleRead();

//menuParentTTSLabelRead();
draw_time = false;
draw_money = false;

info_height = 485-20; //height of the info box
