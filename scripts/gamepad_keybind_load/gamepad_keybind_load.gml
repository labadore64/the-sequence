/// @function gamepad_keybind_load(file)
/// @description Loads the state of the gamepad keybind from a JSON file.
/// @param {string} file The file to load from.

var file = file_text_open_read(argument[0]);

if(file > -1){
	var stringer = "";

	while (!file_text_eof(file))
	{
		stringer+= file_text_readln(file);
	}
	file_text_close(file);

	var save_map = json_decode(stringer);

	// clears the current map
	// when setting the binding, it will just clear out the map
	ds_map_clear(global.gamepad_map);
	
	var sizer = global.gamepad_size;
	global.gamepad_size = 0;

	for(var i = 0; i < sizer; i++){
		gamepad_bind_set(global.gamepad_array[i],stringToGamepad(save_map[? global.gamepad_array[i]]))
	}
	
	var reset = false;
	
	// validate the keybinds
	for(var i = 0; i < sizer; i++){
		if(!gamepad_legal_change(i)){
			reset = true;
		}
	}
	
	if(reset){
		gamepad_bind_init();
	}

	ds_map_destroy(save_map);

}