var list = argument[0];

ds_list_clear(list);
if(object_index == menuOverworldKeyboardOption){
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list,keyboardToString(keyboard_bind_get("select")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("select2")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("cancel")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("up")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("down")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("left")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("right")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("lefttab")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("righttab")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("open_menu")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("control")));
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list,keyboardToString(keyboard_bind_get("help")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("screenshot")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("exit")));
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list,keyboardToString(keyboard_bind_get("run")));
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list,keyboardToString(keyboard_bind_get("bookmark")));
	ds_list_add(list,keyboardToString(keyboard_bind_get("hud")));
	for(var i = 0; i < 10; i++){
		ds_list_add(list,keyboardToString(keyboard_bind_get("access"+string(i))));
	}
} else {
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list," ");
	ds_list_add(list,gamepadToString(gamepad_bind_get("select")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("cancel")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("up")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("down")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("left")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("right")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("lefttab")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("righttab")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("open_menu")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("control")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("run")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("bookmark")));
	ds_list_add(list,gamepadToString(gamepad_bind_get("sight")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("coordinates")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("help")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("hud")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("screenshot")));	
	ds_list_add(list,gamepadToString(gamepad_bind_get("exit")));	
}