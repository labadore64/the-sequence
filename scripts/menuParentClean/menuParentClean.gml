gml_pragma("forceinline");
visible = false;
if(!ignore_clean_script){
	if(clean_script != -1){
		script_execute(clean_script);	
	}
}

/*
with(menuControl){
	if(menu_stack_size <= 1){
		with(AudioEmitter){
			if(audio_sound > -1){
				audio_resume_sound(audio_sound);	
			}
		}
	}
}
*/

menuControlPop();
ds_list_destroy(menu_name);
ds_list_destroy(menu_description);
ds_list_destroy(menu_script);

if(surface_exists(surf)){
	surface_free(surf);	
}

tts_stop()

/*
if(instance_number(menuParent) == 1){
	with(CollisionDetector){
		if(object_index == CollisionDetector){
			audio_resume_sound(sound_emit.audio_sound)
		}
	}
}
*/