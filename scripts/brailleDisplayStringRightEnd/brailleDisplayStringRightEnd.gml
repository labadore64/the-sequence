	var modstring = argument[0];
	var char = argument[1];
	
	modstring = char + string_letters(modstring);
		
	if(string_length(modstring) > count){
		modstring = string_copy(
								modstring,
								1,
								count);
	} else {
		while(string_length(modstring) < count){
			modstring = modstring + " ";	
		}
	}
	
	return modstring;