
	var filename = working_directory + "resources\\lang.ini";

	ini_open(filename)

	//menu options
	for(var i = 0; i < 58; i++){
		ini_read_string("strings","menu"+string(i),"")
	
		global.langini[0,i] = ini_read_string("menu","menu"+string(i),"")//menu option names 
		global.langini[1,i] = ini_read_string("menu","menu_desc"+string(i),"")//menu descriptions
	}

	//menu titles and subtitles
	for(var i = 0; i < 21; i++){
		ini_read_string("strings","menu"+string(i),"")
	
		global.langini[2,i] = ini_read_string("menu","menu_title"+string(i),"")//menu option names 
		global.langini[3,i] = ini_read_string("menu","menu_subtitle"+string(i),"")//menu descriptions
	}

	// Various menu texts
	for(var i = 0; i < 12; i++){
		global.langini[4,i] = ini_read_string("strings","text"+string(i),"")//menu option names 
	}

	//labels
	for(var i = 0; i < 8; i++){
		global.langini[6,i] = ini_read_string("strings","label"+string(i),"")//menu option names 
	}

	//aura ability descriptions
	for(var i = 0; i < 5; i++){
		global.langini[7,i] = ini_read_string("strings","type"+string(i),"")//type names
	}

	for(var i = 0; i < 2; i++){
		global.langini[8,i] = ini_read_string("strings","movetype" + string(i),"");	
	}

	// binding descriptions
	for(var i = 0; i < 22; i++){
		global.langini[9,i] = ini_read_string("binding","binding_name" + string(i),"");	
		global.langini[10,i] = ini_read_string("binding","binding_desc" + string(i),"");	
	}

	// common selections
	for(var i = 0; i < 21; i++){
		global.langini[12,i] = ini_read_string("strings","selection" + string(i),"");
	}

	// battle strings
	for(var i = 0; i < 8; i++){
		global.langini[5,i] = ini_read_string("strings","battle" + string(i),"");
	}

	// phone strings
	for(var i = 0; i < 2; i++){
		global.langini[11,i] = ini_read_string("strings","phone" + string(i),"");
	}


	ini_close();

