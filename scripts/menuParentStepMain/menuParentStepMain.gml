gml_pragma("forceinline");

	if(destroy > 0){
		destroy--;
	} else if(destroy == 0){
		instance_destroy();
	}

	if(objGenericCanDo()){
		with(battleTextDisplay){
			draw_me = false;	
		}
	
		keypress_this_frame = false;
		if(animation_len == 0){
			objGenericStepKeys();
		
			infokey_keycheck();
			if(objGenericCanDo()){
				if(!keypress_this_frame){
					if(mouse_check_button_pressed(mb_right)){
						if(!MouseHandler.clicked){
							if(menu_cancel > -1){
								script_execute(menu_cancel)	
								keypress_this_frame = true;
								with(MouseHandler){
									clicked = true
									clicked_countdown = 5	
								}
							}
						}
					} 
				
					if(enable_scrollwheel && !keypress_this_frame){
						if(mouse_wheel_down()){
							if(menu_up > -1){
								toppos+=scrollwheel_skip;
								if(toppos > menu_size - skip+1){
									toppos = menu_size - skip+1
								}
								if (toppos < 0){
									toppos = 0	
								}
							}
						} else if(mouse_wheel_up()){
							if(menu_down > -1){
								toppos-=scrollwheel_skip;
								if (toppos < 0){
									toppos = 0	
								}
							}
						}
					}
				}
			}
		}
		//menuParentUpdateDebug();

		if(update_image_only_active){
			image_index++;
		}
	
	}

menuParentStepAnimation();

	if(!update_image_only_active){
		image_index++;
	}
//menuParentUpdateBoxDimension();