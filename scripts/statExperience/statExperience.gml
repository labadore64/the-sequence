var chara = argument[0]

level = getLevelFromMultiplier(
					objdata_character.exp_grow[chara.character],
					chara.experience,
					objdata_character.total_exp[chara.character]);

exp_of_level = getLevelEXP(
					objdata_character.exp_grow[chara.character],
					level+1,
					objdata_character.total_exp[chara.character]);
					
					
exp_of_prev_level = getLevelEXP(
					objdata_character.exp_grow[chara.character],
					level,
					objdata_character.total_exp[chara.character]);

exp_to_next = exp_of_level - experience; //required exp to next level

if(level != 100){
	exp_percent = 1-((exp_to_next)/(exp_of_level-getLevelEXP(
						objdata_character.exp_grow[chara.character],
						level,
						objdata_character.total_exp[chara.character])));
} else {
	exp_percent = 1	
}
