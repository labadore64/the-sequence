var chara = argument[0];
var level = argument[1];

var move_list = chara.moves;
var char_id = chara.character;

ds_list_clear(move_list);

var moves = ds_list_create();
var moves_level = ds_list_create();
var moves_event = ds_list_create();


var stringer = "move_id"

var returner = ds_list_create();

var filename = working_directory + "resources\\stats\\move_learn\\monster"+string(character)+".ini";

ini_open(filename)

var i = 0;

while(true){
	if(!ini_key_exists("moves", stringer+ string(i))){
		break;
    }
	i++
}

var strrr = "";

for(var j = 0; i > j; j++){
	
    strrr = ini_read_real("moves", "move_id"+ string(j),0)
	ds_list_add(moves,strrr);
    strrr = ini_read_real("moves", "move_level"+ string(j) ,0)
	ds_list_add(moves_level,strrr);
    strrr = "none"
	ds_list_add(moves_event,strrr);
}

ini_close();

var move_id = 0;
var move_level = -1;
var move_event = "none"

var sizer = ds_list_size(moves);

for(var i = 0; i < sizer; i++){
	move_id = moves[|i];
	move_level = moves_level[|i];
	move_event = moves_event[|i];
	movesLearnedLoadInsert(move_list,move_id,move_level,move_event,chara);
}

if(ds_list_empty(move_list)){
	ds_list_add(move_list,objdataToIndex(objdata_moves,"attack"));	
}

ds_list_destroy(moves);
ds_list_destroy(moves_level);
ds_list_destroy(moves_event);