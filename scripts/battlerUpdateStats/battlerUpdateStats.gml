
	if(instance_exists(id)){

		if(instance_exists(phy_power) &&
			instance_exists(phy_guard)){
				phy_power.current = battlerCalculateStat(phy_power,id,BATTLE_STAT_ATTACK);
				phy_guard.current = battlerCalculateStat(phy_guard,id,BATTLE_STAT_DEFENSE);
				mag_power.current = battlerCalculateStat(mag_power,id,BATTLE_STAT_MAGIC);
				mag_guard.current = battlerCalculateStat(mag_guard,id,BATTLE_STAT_RESISTANCE);
				spe_power.current = battlerCalculateStat(spe_power,id,BATTLE_STAT_AGILITY);
				spe_guard.current = battlerCalculateStat(spe_guard,id,BATTLE_STAT_MP_RECOVER);

				battleStatCalculateBraille(id);
			
		}
	}

