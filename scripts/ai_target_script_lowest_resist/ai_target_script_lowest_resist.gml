var movezz = argument[0];
var targ = argument[1];

var list = -1;

if(targ.is_enemy){
	list = BattleHandler.party_list;
} else {
	list = BattleHandler.enemy_list;	
}

var returner = list[|0];
var sizer = ds_list_size(list);

var obj = -1;

for(var i = 0; i < sizer; i++){
	obj = list[|i];
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			if(obj.mag_guard.current < returner.mag_guard.current){
				returner = obj;	
			}
		}
	}
}

returner = ds_list_find_index(list,returner);

return returner;