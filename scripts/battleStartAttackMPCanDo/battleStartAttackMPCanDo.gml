var returner = true;

if(character.mp.current < mp_cost){
	returner = false;	
}

if(battlerHasFilter(character,"cough")){
	if(character.mp.current < mp_cost*2){
		returner = false;	
	}	
}

if(battlerHasFilter(character,"ether")){
	returner = true;	
}

return returner;