var chara = argument[0];
var targe = argument[1];

var returner = 1;

//returns damage multiplier.

if(battlerHasAbility(targe,"hard_shell")){
	if(targe.hp.current >= 	targe.hp.base*.75){
		returner = .5;
	}
}

if(battlerHasAbility(targe,"stone")){
	returner = .5;	
}

return returner;