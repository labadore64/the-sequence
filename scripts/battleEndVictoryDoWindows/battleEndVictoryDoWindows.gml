var allies = BattleHandler.allies;
var ally = -1;
var ally_overworld = -1;

for(var i = 0; i < 3; i++){
	ally = allies[|i];
	
	if(!is_undefined(ally)){
		if(instance_exists(ally)){
			ally_overworld = ally.overworld_character;
			
			battleEndVictoryAddMovesToList(ally_overworld.moves,new_moves_character[i]);	
		}
	}
}

//fill out the differences in the moves for each character

var old_list = -1;
var new_list = -1;
var change_list = -1;

var sizer = 0;

var unit = 0;

for(var i = 0; i < 3; i++){
	old_list = old_moves_character[i]
	new_list = new_moves_character[i]
	change_list = learned_moves_character[i];
	
	sizer = ds_list_size(new_list);
	
	for(var j = 0; j < sizer; j++){
		unit = new_list[|j];
		
		if(ds_list_find_index(old_list,unit) == -1){
			ds_list_add(change_list,unit);
		}
	}
}

battleEndVictoryDoMoney();

battleEndVictoryDoDrops();

battleEndVictoryDoMoves();

//do new moves learned