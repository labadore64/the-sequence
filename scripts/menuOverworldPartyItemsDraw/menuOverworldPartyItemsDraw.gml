draw_clear(c_black)

	if(animation_len == 0){
		menuOverworldPartyItemsDrawOptions();


		var baseheight = 150;
		var basewidth = 400;
		var base_space = 200;
		var vertical_space = 40;

			menuOverworldItemDrawStat(basewidth,baseheight,$7F7FFF,spr_physpower_icon,stat_attack,25)
			menuOverworldItemDrawStat(basewidth,baseheight+vertical_space,$FFFF7F,spr_physguard_icon,stat_defense,25)
			menuOverworldItemDrawStat(basewidth,baseheight+vertical_space*2,$7FFFFF,spr_spepower_icon,stat_agility,25)
	
			menuOverworldItemDrawStat(base_space+basewidth,baseheight,$FF7FFF,spr_magpower_icon,stat_magic,25)
			menuOverworldItemDrawStat(base_space+basewidth,baseheight+vertical_space,$7FFF7F,spr_magguard_icon,stat_resistance,25)
			menuOverworldItemDrawStat(base_space+basewidth,baseheight+vertical_space*2,$FF7F7F,spr_speguard_icon,stat_mpr,10)

		menuParentDrawText();

		if(character.item[menuOverworldPartyItems.menupos]>0){
			menuParentDrawInfo();
		}
		

		if(item_sprite != -1){
			draw_sprite_extended_ratio(item_sprite,0,575,375-jump_y,.55,.55,0,c_white,1);
		}
	}
	
