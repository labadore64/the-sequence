/// @function stringUppercaseOnly(string)
/// @description Returns a string with all lowercase characters removed.
/// @param {string} string The initial string.

	var stringer = argument[0];

	stringer =string_replace_all(stringer,"a","")
	stringer =string_replace_all(stringer,"b","")
	stringer =string_replace_all(stringer,"c","")
	stringer =string_replace_all(stringer,"d","")
	stringer =string_replace_all(stringer,"e","")
	stringer =string_replace_all(stringer,"f","")
	stringer =string_replace_all(stringer,"g","")
	stringer =string_replace_all(stringer,"h","")
	stringer =string_replace_all(stringer,"i","")
	stringer =string_replace_all(stringer,"j","")
	stringer =string_replace_all(stringer,"k","")
	stringer =string_replace_all(stringer,"l","")
	stringer =string_replace_all(stringer,"m","")
	stringer =string_replace_all(stringer,"n","")
	stringer =string_replace_all(stringer,"o","")
	stringer =string_replace_all(stringer,"p","")
	stringer =string_replace_all(stringer,"q","")
	stringer =string_replace_all(stringer,"r","")
	stringer =string_replace_all(stringer,"s","")
	stringer =string_replace_all(stringer,"t","")
	stringer =string_replace_all(stringer,"u","")
	stringer =string_replace_all(stringer,"v","")
	stringer =string_replace_all(stringer,"w","")
	stringer =string_replace_all(stringer,"x","")
	stringer =string_replace_all(stringer,"y","")
	stringer =string_replace_all(stringer,"z","")

	return stringer;