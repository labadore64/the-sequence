var umm = character.tablet[menupos];

takeTablet(character,umm);
giveTablet(character,menupos,braille_id);
if(braille_id > -1 && umm > -1){
	tts_say("Swapped " +character.name + "'s " +
			+ "\"" + objdata_tablet.char[umm] + "\" " 
			+ objdata_tablet.name[umm] +
			" with "
			+ "\"" + objdata_tablet.char[braille_id] + "\" " 
			+ objdata_tablet.name[braille_id]
			);
} else if (umm == -1){
	tts_say("Gave " 
			+ "\"" + objdata_tablet.char[braille_id] + "\" " 
			+ objdata_tablet.name[braille_id] +
			" to " + character.name);
}
instance_destroy();
with(menuTabletSwitch){
	instance_destroy();	
}
with(menuTabletSub){
	instance_destroy();	
}