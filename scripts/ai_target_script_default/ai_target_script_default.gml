var movezz = argument[0];
var chara = argument[1];

moveData(movezz)

var returner = irandom(2);

//tests if move has a filter.
var list_size = ds_list_size(BattleHandler.party_list);
var testchara = BattleHandler.party_list[|returner];
var list = BattleHandler.party_list;
if((chara.is_enemy && target_ally) ||
	(!chara.is_enemy && !target_ally)){
	testchara = BattleHandler.enemy_list[|returner];
	list_size = ds_list_size(BattleHandler.enemy_list);
	list = BattleHandler.enemy_list;
}

testchara = battleStartAttackSetNewTargetChara(testchara,chara);

//make sure target is valid

if(move_filter[0] != ""){
	var fil = move_filter[0];
	var count = battlerCountFilter(testchara,fil);
	
	if(count < list_size){
		if(battlerHasFilterIgnoreNull(testchara,fil)){
			for(var i = 0; i < list_size; i++){
				
				
				if(!is_undefined(list[|i])){
					testchara = list[|i];
					if(!battlerHasFilterIgnoreNull(testchara,fil)){
						break;
					}
				}
			}
		}
	}
	
	var inde = ds_list_find_index(list,testchara);
	
	if(inde != -1){
		returner = inde;	
	}
}

return returner;