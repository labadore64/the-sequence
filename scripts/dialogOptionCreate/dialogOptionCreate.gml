option_strings = ds_list_create(); //strings for options 
option_goto = ds_list_create(); //next dialog for this current session
option_event = ds_list_create(); //event set by selected option
option_set_next = ds_list_create(); //sets next dialog after completed.
option_class_update = ds_list_create(); //updates the class of the character if selected.
option_end_script = ds_list_create();

//option_endScript

draw_script = dialogOptionDraw

menupos = 0;

active = false;
animation_time = 7;
animation_counter = 0;

menu_up = dialogOptionMoveUp;
menu_down = dialogOptionMoveDown;

menu_left = menu_up;
menu_right = menu_down;

menu_select = dialogOptionSelect
sound_select = soundMenuDefaultMove;

stringsize = 0

newalarm[0] = animation_time;
newalarm[1] = 1

spacer = (((600-480)*.5)/animation_time)