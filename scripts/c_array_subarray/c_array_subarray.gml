//remove certain array values from an array.
var arrayer = argument[0];
var start = argument[1];
var count = argument[2];

var returner = -1;

var sizer = array_length_1d(arrayer);

var counter = 0;

var cando = true;

for(var i = 0; i < sizer; i++){
	if(i == start){
		cando = false;	
	}
	
	if(cando){
		returner[counter] = arrayer[i]
		counter++;
	} else {
		if(count == 0){
			cando = true;	
		}
		count--;	
	}
}

return returner;