if(!completed){

	if(!instance_exists(input_queue)){
		input_queue = instance_create(0,0,brailleInputQueue);
	}
	
	input_queue.parent_obj = id;
	input_queue.braille_data = braille_data
	input_queue.destroy_when_empty = false;
	input_queue.menu_cancel_child = brailleGameGenericCancel;

	active = true;

	if(object_index != brailleGamePracticeMode){
		var mapp = braille_data.braille_map;

		var size, key, i;
		size = ds_map_size(mapp);
		key = ds_map_find_first(mapp);
		for (i = 0; i < size; i++;)
		   {
			   key = ds_map_find_next(mapp, key)
			   if(!is_undefined(key)){
					ds_list_add(all_braillecells,key);
			   }
		   }
	}
	
	brailleCharsRefill();

	addToBrailleQueue(ds_queue_dequeue(braille_insert_queue),input_queue);
}