/// @description Insert description here
// You can write your code in this editor
with(MouseHandler){
	if(false){
		clicked_this_frame = false

		var clickedz = false;
		var posi = mouse_active_position 
		if(clicked_countdown > -1){
			if(clicked_countdown == 0){
				clicked = false;
			}
			clicked_countdown--
		}
		if(ScaleManager.updated){
			mouseHandler_update_values();
		}

		var mousex = window_mouse_get_x();
		var mousey = window_mouse_get_y()

		selected = -1;	

		if(!clicked){
			for(var i = 0; i < mouse_count; i++){
				mouse_hovered[i] = false;
				if(instance_exists(mouse_obj[i])){
					if(mousex >= mouse_points_trans[i,0] && 
						mousex <= mouse_points_trans[i,2] &&
						mousey >= mouse_points_trans[i,1] && 
						mousey <= mouse_points_trans[i,3]){
						mouse_hovered[i] = true;
					
						if(mouse_check_button_pressed(mb_left)){
							if(mouse_script[i] != -1){
								var script = mouse_script[i];
								clicked_this_frame = true
								with(mouse_obj[i]){
									script_execute(script)
								}
								clickedz = true
								break;
							}
						}
						
						if(mouse_menupos[i]){
							if(posi != i){
								with(mouse_obj[i]){
									if(!keypress_this_frame){
										menupos = i;
										if(menupos > menu_size-1){
											menupos = menu_size-1;	
										}
										posi = menupos;

										if(menu_update_values > -1){
											script_execute(menu_update_values);	
										}
										
										tts_say(MouseHandler.mouse_text[i])
									} else {
										posi = menupos;	
									}
								}
							}
						} else {
							if(!mouse_hovered_last[i]){
								tts_say(mouse_text[i])
							}	
						}

					}
				}
			}


		}
		
		if(mouse_xprevious != mouse_x || mouse_yprevious != mouse_y){
			ignore_menu = false
		}

		if(!ignore_menu){
			if(instance_exists(mouse_menu_obj)){
				var arrays = mouse_menu_script;
	
	
				with(mouse_menu_obj){
					posi = menupos
					if(active){
						if(!keypress_this_frame){
							var sizer = min(toppos + skip-1, MouseHandler.mouse_menu_count);
							var counter = 0;
							for(var i = toppos; i < sizer; i++){
								if(mousex >= MouseHandler.mouse_menu_points_trans[i,0] && 
									mousex <= MouseHandler.mouse_menu_points_trans[i,2] &&
									mousey >= MouseHandler.mouse_menu_points_trans[i,1]-MouseHandler.mouse_menu_spacer*toppos &&
									mousey <= MouseHandler.mouse_menu_points_trans[i,3]-MouseHandler.mouse_menu_spacer*toppos){
							
									if(mouse_check_button_pressed(mb_left)){
										if(arrays[i] != -1){
											script_execute(arrays[i])	
											clickedz = true
											break;
										}
									}
						
									if(posi != i){
										menupos = i;
										if(menupos > menu_size-1){
											menupos = menu_size-1;	
										}
										posi = menupos;
										with(MouseHandler){
											clicked_this_frame = true	
										}
						
										if(menu_update_values > -1){
											script_execute(menu_update_values);	
										}
										tts_say(menu_name[|menupos])
									}

								}
								counter++;
							}
						} else {
							// if key pressed, update menupos so it doesn't trigger tts	
							posi = menupos;
						}
					}
				}
			}
		}
	
		if(clickedz){
			clicked = true
			clicked_countdown = 5	
		}
	
		mouse_active_position = posi
	
		mouse_xprevious = mouse_x
		mouse_yprevious = mouse_y
	
		for(var i = 0; i < mouse_count; i++){
			mouse_hovered_last[i] = mouse_hovered[i]
		}
	}
}