gml_pragma("forceinline");

event_inherited();

//monster trigger vars
monster_step_counter = 0;
playerSetMonsterStep();
//movement variables
orientation = 90; //orientation in degrees
last_orientation = orientation
rotation_speed = 0; //how many degrees to move when rotating.
rotation_delta = 30; //how fast it will accelerate when moving
rotation_max = 30; //max rotation speed
rotation_snap_tolerance = 30;// how many degrees from direction must be to snap to direction
dir = 0; //current turning direction
last_dir = 0; //last turning direction
axis_last_dir = 0; //last dir to fix axis control bugs
distance_travelled = 0;
footstep_counter = 0;
footstep_max = 43;

move_rate = 0; //how much you move each frame
move_rate_max = 3*.65; //default move rate
move_rate_run_max = 5*.75; //maximum running rate
move_delta = 1; //acceleration of default movement
move_this_frame = false; //whether or not movement happened
move_x = 0; //how much to move x this frame
move_y = 0; //how much to move y this frame
collision_y_adjust = 32; //how much to adjust for collisions
is_running = false; //whether or not running

run_multiplier = 1.75; //how much faster you can go while running

//drawing variables
draw_scale = .18; //how large to draw the sprite
draw_direction = DIR_SOUTH; //direction of the current sprite
draw_flip = false; //whether or not to flip the sprite
draw_action = PLAYER_ACTION_STAND; //what action for animation
surface = -1 //surface for your character
update_surface = false; //whether or not to update the surface

//zoom variables

zoom_x = .25
zoom_y = .25
zoom_rate = .005; //how fast it zooms in and out
zoom_min = .25; //min zoom
zoom_max = .65; //max zoom

zoom_x_previous = 1; //updates at the end of each frame
zoom_factor_x = (1/zoom_x);
zoom_factor_y = (1/zoom_y);

adjust_this_frame = false;
avg_dir = 0;
accu_level = 4; //size of frame

draw_ori = 0;

dir_queue = ds_queue_create();
calc_queue = ds_queue_create();



//key inputs

setScriptHoldRight(playerMoveRight);
setScriptHoldUp(playerMoveUp);
setScriptHoldDown(playerMoveDown);
setScriptHoldLeft(playerMoveLeft);
setScriptReleaseRight(playerMoveRelease);
setScriptReleaseLeft(playerMoveRelease);
setScriptReleaseUp(playerMoveRelease);
setScriptReleaseDown(playerMoveRelease);

menu_control = playerControl//;

menu_create_bookmark = playerControlPress;

menu_access_menu = playerAXBookmarkMenu

menu_cancel = playerMoveCancel;

script_run = playerRun;

draw_script = playerDrawMain;
script_menu_open = playerOpenMenu
key_openAccess = keybind_get("hud")
gamepad_openAccess = gamepadbind_get("hud")
//create camera

camera = instance_create_depth(0,0,0,CameraOverworld);

//footstep sounds

footstep_grass = soundFootstep01;
footstep_inside = soundFootstep02;
footstep_wood = soundFootstep03;
footstep_current = soundFootstep01;

audio_listener_position(x, y+collision_y_adjust, 0);

audio_listener_orientation(cos(degtorad(orientation)), -1, sin(degtorad(orientation)), 0, 0, -1);
audio_falloff_set_model(audio_falloff_linear_distance_clamped);

for(var i = 0; i < 4; i++){
	collision[i] = noone
}

newalarm[5] = 1
collision_interacting = noone; //which collision is being interacted with
last_good_x = x;
last_good_y = y;
/*
listColl_left = ds_list_create()
listColl_right = ds_list_create()
listColl_up = ds_list_create()
listColl_down = ds_list_create()
*/
newalarm[1] = 1

no_move = false;
sound_item = false; //whether or not the character stepping on interactable area
move_this_turn = false;

//bonk sounds
bonk_sound_normal = sound_hitWall;
bonk_sound_item = sound_hitWallObject;

//the following is for evaluating locations of interactables
interact_x = 0;
interact_y = 0;
interact_dist = 15; // how far out should interactions go

item_use_lock = false; //if using certain items, lock the player.

spray_lock = false; //locks non movement actions if spraying.
spray_trigger_counter = 0;
spray_amount = 1;


//particles for sprays
current_particle = -1;
sname = part_system_create();
part_emit = part_emitter_create(sname);

part_dist = 10;
part_emit_size = 3;

part_system_automatic_draw(sname,false)
part_system_automatic_update(sname,false)

spray_type = -1;
spray_release_type = -1;

push_lock = false;
push_count = 0;
push_time = 25; //how much time the push animation shows for

right_click_menu = playerOpenMenu;