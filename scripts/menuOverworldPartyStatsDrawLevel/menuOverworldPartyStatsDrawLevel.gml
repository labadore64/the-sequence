var lv_xx = 760;
var lv_yy = 20;

var tolevelxx = -270+70;
var tolevelyy = 45

var exp_box_xx = -50;
var exp_box_yy = 53;
var exp_box_width = 140*.5;
var exp_box_height = 20*.5;


draw_set_halign(fa_right)
drawOutlineText(lv_xx,lv_yy,"Lv." + string(chaa.level),3,3,0,global.textColor);
draw_set_halign(fa_left)

if(chaa.level < 100){

	drawOutlineText(lv_xx+tolevelxx,lv_yy+tolevelyy,"Next:",2,2,0,global.textColor)

	drawBar(lv_xx+exp_box_xx,lv_yy+exp_box_yy,exp_box_width,exp_box_height,chaa.exp_percent,global.textColor)
}