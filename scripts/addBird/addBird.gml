var name = argument[0];

with(obj_data){
	var date = date_current_datetime();
	var findvalue = ds_map_find_value(bird_seen,name);
	if(is_undefined(findvalue)){
		ds_map_replace(bird_seen,name,date);
	}
}