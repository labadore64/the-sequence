var map = argument[0];

var returner = -1;

if(ds_exists(map,ds_type_map)){
	returner = ds_list_create();
	
	var size = ds_map_size(map) ;
	for (var i = 0; i < size; i++;)
	   {
			  ds_list_add(returner,map[?string(i)]);
	   }
}

return returner;