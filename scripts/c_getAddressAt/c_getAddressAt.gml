//returns the array as an address

var tablepointer = argument[0];

if(array_length_1d(tablepointer) != 4){
	return -1;
}

var hex = base_convert(string(tablepointer[3]),10,16)
			+ base_convert(string(tablepointer[2]),10,16)
			+ base_convert(string(tablepointer[1]),10,16)
			+ base_convert(string(tablepointer[0]),10,16);
			
//set the header address
return real(base_convert(hex,16,10))