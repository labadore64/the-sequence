//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];


stringer = string_replace_all(stringer,"@0",chara.name);
stringer = string_replace_all(stringer,"@1",targe.name);

battleAttackInputAddString(stringer);

if(random(1) < .2){
	var cando = battlerAddFilter(targe,"poison");
	
	if(cando == 1){
		var stringer = battletext[|cando];
	
		stringer = string_replace_all(stringer,"@0",chara.name);
		stringer = string_replace_all(stringer,"@1",targe.name);

		battleAttackInputAddString(stringer);
	}
}

ds_list_destroy(battletext);