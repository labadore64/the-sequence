//var scale_diff = maxxsize-minsize;

//180 is largest
//360 is smallest

max_y_pos = sin(degtorad(90))*BattleHandler.v_size + BattleHandler.origin_y;
min_y_pos = sin(degtorad(270))*BattleHandler.v_size + BattleHandler.origin_y;

diff_y_pos = max_y_pos - min_y_pos;

var test_ori = y - min_y_pos;

var tester = test_ori/diff_y_pos;
var scale_diff = maxxsize-minsize;


curr_scale = scale_diff*tester + minsize;

var scaler = curr_scale;

if(flip){
	scaler = scaler*-1;	
}

image_xscale = scaler;
image_yscale = curr_scale;
