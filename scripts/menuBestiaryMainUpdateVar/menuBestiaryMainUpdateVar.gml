if(!no_creatures){
	var inde = display_list[|menupos];
	
	monster_sprite = objdata_enemy.sprite_idle_front[inde]
	monster_state = objdata_enemy.state[inde];
	var entryname = "creature."

	translation = translation_start;
	translation_rate = translation_rate_start;

	hudControllerAddData(entryname + "name",objdata_enemy.name[inde]);
	hudControllerAddData(entryname + "number",string(inde+1));
		
	hudControllerAddData(entryname + "seen",string(ds_list_size(bird_list)));
}