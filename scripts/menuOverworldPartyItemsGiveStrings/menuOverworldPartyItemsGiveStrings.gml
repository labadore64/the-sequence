event_inherited();

flash = false;
flash_timer = 0;
flash_amount = 30;

skip = 8;

item_id = ds_list_create();
item_quantity = ds_list_create();

//menuOverworldItemInventory(menuOverworldPartyGiveGiveScript);

itemData(item_id[|0]);

menu_size = menuGetSize();

animation_start = 3 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,11])
menuParentSetSubtitle(global.langini[3,11])
menuParentSetInfoboxSubtitleYOffset(45)
menuParentSetOptionXOffset(10)
menuParentSetSelectWidth(.45)
menuParentSetDrawGradient(false);
menuParentSetTitleXOffset(30)
menuParentSetOptionYOffset(160)

//cash label var global.langini[6,i]

menu_draw = menuOverworldItemDrawMain;


menuParentUpdateBoxDimension();


var plx = 620;
var ply = 575;

itemData(item_id [| menupos])

menuOverworldItemsListStart(menuOverworldPartyGiveGiveScript);
enter_script = menuOverworldPartyGiveGiveScript;

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

tts_clear();

var varname = "";

if(instance_exists(menuOverworldParty)){
	varname = menuOverworldPartyItems.character.name
}

draw_money = false;