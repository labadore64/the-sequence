#macro BATTLER_PARTICLE_MAX_EMITTER 5

for(var i = 0; i < BATTLER_PARTICLE_MAX_EMITTER; i++){
	particle_bank[i] = -1;	
	particle_type[i] = -1;
	particle_time[i] = -1;
}

main_part_system = part_system_create()
    
part_system_automatic_draw(main_part_system, false);
part_system_automatic_update(main_part_system,false)