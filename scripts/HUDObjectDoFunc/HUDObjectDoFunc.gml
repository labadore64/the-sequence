var func = argument[0]
var args = argument[1];
var reqi = argument[2]

if(func == 0){
	// do text	
	if(current_object.hud_id > -1){	
		if(reqi == "" || hudControllerContainsData(reqi)){
			tts_say(hudControllerReplaceData(args,current_object));
		}
	}
	
} else if (func == 1){
	// do move
	var data = json_decode(args);
	
	// get destination
	var dest = data[? "dest"];
	if(reqi == "" || hudControllerContainsData(reqi)){
		if(!is_undefined(dest)){
			// do sound effect
			if(!(dest == "parent" && current_object.parent <= -1)){
			
				var req = data[? "requires"];
			
				if(is_undefined(req) || hudControllerContainsData(req)){
					
					tts_stop();
					
					var sound = data[? "sound"];
		
					if(is_undefined(sound)){
						sound = sound_move;	
					} else {
						if(sound == "cancel"){
							sound = soundMenuHUDCancel;	
						} else if (sound == "exit"){
							sound = sound_cancel;	
						}else {
							sound = sound_move;	
						}
					}
		
					// do remap
					var remap = data[? "remap"];
		
					if(is_undefined(remap)){
						remap = "";
					}
		
					if(string_pos("none",remap) < 1){
						HUDNavigatorGotoObject(dest,remap);
						soundfxPlay(sound);
					}
				}
			}
		}
	}
	
	ds_map_destroy(data);
} else if (func == 2){
	// do exit
	menuParentCancel();
}