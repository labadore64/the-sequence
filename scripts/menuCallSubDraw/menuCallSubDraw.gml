if(!dialing){
	if(animation_len == 0){
		menuParentDrawBox();


		menuParentDrawText();
	
		//stat_list
	
		menuParentDrawOptions();
	
		draw_set_halign(fa_left)
		draw_set_valign(fa_top)
	} else {
		draw_set_color(c_black)
		draw_rectangle_ratio(x1,y1,x2,y2,false)
	}
	draw_set_color(c_black)
	draw_rectangle_ratio(x1,170,x2,300,false)

	if(character > -1){
		var obj = objdata_character.face_sprite[character.character];
		if(obj > -1){
			draw_sprite_extended_ratio(obj, 0,
										200,200,
										1,1,
										0,
										c_white,1)
		}
		
		if(in_party){
			draw_sprite_extended_ratio(spr_menu_star, 0,
									385,235,1.25,1.25,0,c_white,1)	
		}
	
		draw_set_color(global.textColor)
		draw_text_transformed_ratio(420, 220, character.name,4,4,0)
		draw_text_transformed_ratio(370, 280, objdata_character.phone[character.character],2,2,0)
	}
} else {
	draw_set_color(c_black)
	draw_rectangle_ratio(x1,y1-100,x2,y2,false)
	draw_set_color(global.textColor)
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(400, 350,"Dialing...",4,4,0)
	draw_set_halign(fa_left)
}
cellPhoneDrawCase();