if(instance_exists(Player)){
	
	if(!Player.bird_millet){

		playerSetBirdFood("millet");

		with(menuParent){
			ignore_clean_script = false;
			instance_destroy();
		}

		var ob = textboxOverworld("item_millet")
		ob.triggered_by_player = true;
		return true;
	} else {
		soundfxPlay(soundMenuInvalid)	
		return false;
	}

}
return false;
//add cancel script if bird food is set.