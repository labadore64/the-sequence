
	if(animation_len == 0){
	
		draw_clear(c_black);
	
		var yy = -30;
		var pls = -50
	
		with(menuCharacterSwitchParty){
			if(object_index == menuCharacterSwitchParty){
				with(portrait){
					draw_surface(surface,-90,yy+pls);	
				}
			}
		}
	
		with(menuCharacterSwitchRecruit){
			with(portrait){
				draw_surface(surface,400,yy+pls);	
			}
		}
	
		draw_sprite_ext(spr_arrow_switch,select_counter,400-15,300-60+pls-10,2,1,0,global.textColor,1)
		draw_sprite_ext(spr_arrow_switch,select_counter,400+20,300+70+pls-10,-2,1,0,global.textColor,1)
	
		draw_set_color(c_black)
		draw_rectangle(0,390+pls,800,600,false)
	
		draw_set_color(global.textColor);
		draw_set_font(global.textFont)
	
		draw_set_halign(fa_center)
	
		var spacer = 150;
		var xx = 400;
		var yy = 550;

		if(menupos == 0){
			draw_text_transformed_ratio(xx-spacer,yy,menu_name_array[0],select_scale,select_scale,0)
		} else {
			draw_text_transformed_ratio(xx-spacer,yy,menu_name_array[0],normal_scale,normal_scale,0)
		}

		if(menupos == 1){	
			draw_text_transformed_ratio(xx+spacer,yy,menu_name_array[1],select_scale,select_scale,0)
		} else {
			draw_text_transformed_ratio(xx+spacer,yy,menu_name_array[1],normal_scale,normal_scale,0)
		}
	
		draw_set_font(global.largeFont);
		draw_text_transformed_ratio(xx,yy-45,"Confirm?",2,2,0)
	
		draw_set_font(global.textFont)
		draw_text_transformed_ratio(xx,yy-200,"Resulting Aura:",2,2,0)
	
		draw_set_halign(fa_right)
	
		draw_text_transformed_ratio(500,300-105+pls-10,recruit_char + "\nto party",2,2,0)
	
		menuCharacterSwitchPartyConfirmDrawAura();
	
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(400-75-20,300+25+pls-10,party_char + "\nto bench",2,2,0)
	}

