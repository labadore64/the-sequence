if(animation_len == 0){
	menuParentDrawBox();
	
	draw_set_font(global.textFont);
	draw_set_color(global.textColor);
	
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle)
	
	if(sprite_face > -1){
		draw_sprite_extended_ratio(sprite_face,0,520,180,1.75,1.75,0,c_white,1)
	}
	
	draw_text_transformed_ratio(65,y,windowText,2,2,0)

	draw_set_halign(fa_left);
	draw_set_valign(fa_top)
	
} else {
	menuParentDrawBoxOpen();
}

gpu_set_colorwriteenable(true,true,true,true)