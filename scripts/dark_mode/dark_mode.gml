//for any time that you're in the dark, blind mode or not.

var returner = false;

returner = (access_mode() && (!global.blindForceGraphics && !global.cantSeeMode));

return returner;