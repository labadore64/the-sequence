	// detect words
	word_move = -1;
	if(detect_words){
		for(var i = 0; i < 5; i++){
			preview_word[i] = false;
			braille_display[i].surf_update = true;
			braille_display[i].highlighted = false;
		}
		var result = wordTestInString(display_string);
		
		if(result != -1){
			for(var i = result-1; i < result+detected_length-1;i++){
				preview_word[i] = true;	
				braille_display[i].highlighted = true;
			}
		}
	}