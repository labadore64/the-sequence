var stringon = "on"
var stringoff = "off"
tts_clear();
if(menupos == 0){
	tts_say(string(global.BrailleTimerSpeed) + " " + menu_name_array[menupos]);
} else if (menupos == 1){
	tts_say(text_color_strings[|ds_list_find_index(text_color_values,global.braille_practice)] + " " + menu_name_array[menupos])
} else if (menupos == 2){
	var readstring = stringoff;
	if(global.BrailleSound){
		readstring = stringon;
	}
	tts_say(readstring + " " + menu_name_array[menupos]);
} else if (menupos == 3){
	var readstring = stringoff;
	if(global.brailleRepeat){
		readstring = stringon;
	}
	tts_say(readstring + " " + menu_name_array[menupos]);
} else if (menupos == 4){
	var readstring = stringoff;
	if(global.brailleBackground){
		readstring = stringon;
	}
	tts_say(readstring + " " + menu_name_array[menupos]);
} else if (menupos == 5){
	var readstring = stringoff;
	if(global.brailleHint){
		readstring = stringon;
	}
	tts_say(readstring + " " + menu_name_array[menupos]);
}