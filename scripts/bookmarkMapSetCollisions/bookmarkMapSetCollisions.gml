var grids = argument[0];
var hcells = argument[1];
var vcells = argument[2];
var size = argument[3];

var x_left = 0;
var x_right = 0;
var y_up = 0;
var y_down = 0;

var setMe = false;

for(var i = 0; i < hcells; i++){
	x_left = i*size;
	x_right = (i+1)*size
	for(var j = 0; j < vcells; j++){
		y_up = j*size;
		y_down = (j+1)*size;

		with(Collision){
			if(collisionBoxTest(id,x_left,y_up,x_right,y_down) != 0){
				setMe = true;
			}
		}
		
		if(setMe){
			mp_grid_add_cell(grids, i, j);
		}
		setMe = false;
	}
}