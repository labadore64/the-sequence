
	gml_pragma("forceinline");

	if(destroy > 0){
		destroy--;
	} else if(destroy == 0){
		instance_destroy();
	}

	if(objGenericCanDo()){

		//get axis data
		

		if(!check_input_select_pressed()){
			check_input_cancel_pressed();	
		}
			
		check_input_select();

	
		check_input_control_pressed();

		check_input_left_pressed();
		check_input_right_pressed();
		check_input_up_pressed();
		check_input_down_pressed();
	
		check_input_left();
		check_input_right();
		check_input_up();
		check_input_down();
		
		check_input_lefttab();
		check_input_righttab();
		
		check_input_left_released();
		check_input_right_released();
		check_input_up_released();
		check_input_down_released();

		if(menu_help != ""){
			if(global.inputEnabled){
				if(!keypress_this_frame){
					if(global.key_state_array[KEYBOARD_KEY_HELP] == KEY_STATE_PRESS ||
						global.gamepad_state_array[KEYBOARD_KEY_HELP] == KEY_STATE_PRESS){
							createHelpMenu(menu_help)
							keypress_this_frame = true;
					}
				}
			}
		} 
		
		if(menu_ax_HUD != ""){
			if(global.inputEnabled){
				if(access_mode() || global.tts){
					if(!keypress_this_frame){
						if(global.key_state_array[KEYBOARD_KEY_HUD] == KEY_STATE_PRESS ||
							global.gamepad_state_array[KEYBOARD_KEY_HUD] == KEY_STATE_PRESS){
								openHUD(menu_ax_HUD)
								keypress_this_frame = true;
						}
					}
				}
			}
		} 
		//characterStatMain
	}

