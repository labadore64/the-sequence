if(!binoculars){
if(visible){
	if(ScaleManager.updated){
		if(!surface_exists(surface)){
			surface = surface_create(512*global.scale_factor,512*global.scale_factor)
		} else {
			surface_resize(surface,	512*global.scale_factor,512*global.scale_factor);
		}
	
		update_surface = true;
		
		draw_the_xx = 256*global.scale_factor;
		draw_the_yy = 256*global.scale_factor;
	}
	playerDrawUpdate();		
		
	gpu_set_texfilter(true)

	gpu_set_blendmode(bm_normal)
	draw_surface_ext(surface,draw_posx,draw_posy,draw_strex,draw_strey,0,c_white,1);

	gpu_set_texfilter(false)
}
}