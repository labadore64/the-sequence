
if(active){
	draw_set_color(c_black)
	draw_rectangle_ratio(0,550,800,600,false)

	draw_set_color(global.textColor)

	draw_set_halign(fa_left)

	if(animation_len == 0){
		menuParentDrawBox();
	
		//menuParentDrawText();
	
	
		menuParentDrawOptionsNoGradient();
	
	}

	gpu_set_colorwriteenable(true,true,true,true)

	menuParentDrawInfo();

	// draw move stuff
	if(move_id > -1){
		draw_set_color(global.textColor)
		var xx = 500;
		var yy = 40;
	
		with(braille_obj){
			brailleCharacterDrawAgain();
		}
	
		draw_set_halign(fa_center);
		draw_text_transformed_ratio(xx-290,yy,title,4,4,0)
		draw_text_transformed_ratio(xx,yy,objdata_moves.name[move_id],4,4,0)
		draw_text_transformed_ratio(xx,yy+50,objdata_moves.flavor[move_id],1,1,0)
	
		var spr_y = 170;
		var spr_my = 70
		var spr_scale = 2;
		var spacee = 75;
	
		if(move_type_sprite != -1){
			draw_text_transformed_ratio(xx+spacee,yy+spr_y,"Type",2,2,0)
			draw_sprite_extended_ratio(move_type_sprite,0,xx+spacee,yy+spr_y+spr_my,spr_scale,spr_scale,0,c_white,1)
		}
		if(move_element_sprite != -1){
			draw_text_transformed_ratio(xx-spacee,yy+spr_y,"Element",2,2,0)
			draw_sprite_extended_ratio(move_element_sprite,0,xx-spacee,yy+spr_y+spr_my,spr_scale,spr_scale,0,c_white,1)
		}
	
		draw_set_halign(fa_left);
	
		if(obj_data.tablet_character[braille_id] > -1){
			menuTabletTakeDrawCharacter(obj_data.tablet_character[braille_id].character,180,yy+180);
		}
	
		// draw stats
	
		var baseheight = 330;
		var basewidth = 360;
		var base_space = 150;
		var vertical_space = 40;
	
		menuTabletSubDrawStat(basewidth,baseheight,$7F7FFF,spr_physpower_icon,objdata_tablet.phy_power[braille_id],20)
		menuTabletSubDrawStat(basewidth,baseheight+vertical_space,$FFFF7F,spr_physguard_icon,objdata_tablet.phy_guard[braille_id],20)
		menuTabletSubDrawStat(basewidth,baseheight+vertical_space*2,$7FFFFF,spr_spepower_icon,objdata_tablet.spe_power[braille_id],20)
	
		menuTabletSubDrawStat(base_space +basewidth,baseheight,$FF7FFF,spr_magpower_icon,objdata_tablet.mag_power[braille_id],20)
		menuTabletSubDrawStat(base_space +basewidth,baseheight+vertical_space,$7FFF7F,spr_magguard_icon,objdata_tablet.mag_guard[braille_id] ,20)
		menuTabletSubDrawStat(210+base_space +base_space,baseheight+vertical_space*2,$FF7F7F,spr_speguard_icon,objdata_tablet.spe_guard[braille_id],10)

		// draw thing
	
		draw_set_halign(fa_left)

		var stat_pox = 330;
		var stat_poy = 155;
	
		draw_set_color(global.textColor);

		draw_text_transformed_ratio(stat_pox+38,stat_poy,"Damage:",2,2,0)

		var bar_width = 140;
		var bar_height = 15;
		var bar_xx = 175;
		var bar_yy = 5-2;

		draw_set_color($111111)

		draw_rectangle_ratio(bar_xx+stat_pox-2,bar_yy+stat_poy-2,bar_xx+stat_pox+bar_width+2,bar_yy+stat_poy+bar_height+2,false)

		draw_set_color($000000)

		draw_rectangle_ratio(bar_xx+stat_pox,bar_yy+stat_poy,bar_xx+stat_pox+bar_width,bar_yy+stat_poy+bar_height,false)

		draw_set_color(global.textColor);

		if(multtt != 0){
			draw_rectangle_ratio(bar_xx+stat_pox,bar_yy+stat_poy,bar_xx+stat_pox+bar_width*multtt,bar_yy+stat_poy+bar_height,false)
		}

	}
}

draw_letterbox()