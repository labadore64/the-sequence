	if(active){
		if(!surface_exists(surface)){
			surface = surface_create_access(global.window_width,global.window_height);	
		} else {
			if(ScaleManager.updated){
				surface_resize(surface,global.window_width,global.window_height);
			}
		}
		
		surface_set_target(surface)
	
		draw_clear(c_black)	
		if(background != -1){
			draw_sprite_extended_ratio(background,bg_index,0,0,1,1,0,c_white,1);
		}
	
		surface_reset_target();
	
		draw_surface_ext(surface,0,0,1,1,0,0,surface_alpha);
	
		if(!dontdrawportrait){
			with(portrait){
				if(!surface_exists(surface)){
					script_execute(draw_script)
				}
				
				drawSurfAsVCRPos(surface,
							global.display_x+140*global.scale_factor,
							global.display_y+20*global.scale_factor)
			}
		} else {
			with(TextBox){
				with(portrait){
					if(!surface_exists(surface)){
						script_execute(draw_script)
					}

					drawSurfAsVCRPos(surface,
							global.display_x+140*global.scale_factor,
							global.display_y+20*global.scale_factor)
				}
			}
		}
	
		if(!sprite_stop){
			if(sprite != -1){
				draw_sprite_extended_ratio(sprite,sprite_in,sprite_x,sprite_y,1,1,0,c_white,1);
			}
		}
	
		var heiiii = 430;
		var spacerrr = 30;
		var lengeee = string_length(character.name);
		
		draw_set_alpha(1);
		
		draw_set_colour(global.textColor);
		if(show_chara_name){
			draw_rectangle_ratio(0,heiiii-4,15 + spacerrr*lengeee+4,480,false)
		}
		draw_rectangle_ratio(0,480-4,800,600,false)
	
		draw_set_color(c_black)

		draw_rectangle_ratio(0,480,800,600,false)
		draw_set_halign(fa_left)
		if(show_chara_name){

			draw_rectangle_ratio(0,heiiii,15 + spacerrr*lengeee,480,false)
			draw_set_color(global.textColor)
			draw_set_font(global.largeFont)
			draw_text_transformed_ratio(15,heiiii+10,character.name,2,2,0)
			draw_set_font(global.textFont)
		}

	}
	
	draw_letterbox();