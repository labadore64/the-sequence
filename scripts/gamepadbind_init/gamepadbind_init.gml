
//gamepad binds

gamepadbind_set("left",gp_padl);
gamepadbind_set("right",gp_padr);
gamepadbind_set("up",gp_padu);
gamepadbind_set("down",gp_padd);

gamepadbind_set("control",gp_face4);

gamepadbind_set("cancel",gp_face2);
gamepadbind_set("select",gp_face1);

//overworld gamepadbinds

gamepadbind_set("open_menu",gp_start);
gamepadbind_set("run",gp_face3);

gamepadbind_set("help",gp_shoulderl);
gamepadbind_set("exit",gp_select);
gamepadbind_set("hud",gp_shoulderlb);
gamepadbind_set("bookmark",gp_stickl);

gamepadbind_set("accessMenu", gp_stickr);

gamepadbind_set("sight",gp_shoulderrb)
gamepadbind_set("coordinates",gp_shoulderr);

//Battle gamepadbinds

//whether or not the left or right stick is used
//gamepadbind_set("left_stick",true);