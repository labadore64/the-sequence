/// @description Insert description here
// You can write your code in this editor


if(!surface_exists(surf)){
	surf = surface_create(width*global.scale_factor*cell_scale,height*global.scale_factor*cell_scale);	
	surf_update = true;
} else {
	if(ScaleManager.updated){
		surface_resize(surf,width*global.scale_factor*cell_scale,height*global.scale_factor*cell_scale);
		surf_update = true;
	}
}

var sprite = -1;

var xx = 0;
var yy = 0;

var spacer = 20;
var spr_index = 0;

if(surf_update){
	surface_set_target(surf)

	draw_clear_alpha(c_black,0);
	
	if(highlighted){
		draw_set_color(highlight_text_color);	
	} else {
		draw_set_color(default_text_color);	
	}

	for(i = 0; i < 2; i++){
		for(j = 0; j < 3; j++){
			sprite = braillecell_off;

	
			spr_index = cell_transition[i,j];
	
			if(spr_index == -1){
				spr_index = 0;	
				if(cell[i,j]){
					sprite = braillecell_on;
				}
			} else {
				if(cell[i,j]){
					sprite = braillecell_turnon;
				} else {
					sprite = braillecell_turnoff;	
				}
			}
	
			xx = i*spacer*cell_scale;
			yy = 30+j*spacer*cell_scale;
			
			if(display_character){
				if(draw_character){
					draw_set_halign(fa_center);
					draw_set_font(global.largeFont)
					draw_text_transformed_textbox_ratio(width*.5,10+highlight_move_amount_y*highlight_move_multi*global.scale_factor,cell_character,1,1,0)
					draw_set_font(global.normalFont)
					draw_set_halign(fa_left);
				}
			}
	
			draw_sprite_extended_nopos_ratio(sprite,spr_index,xx,yy,.5*cell_scale,.5*cell_scale,0,c_white,1);
		}
	}
	surf_update = false;
	surface_reset_target();
}

draw_surface_ext(surf,
			global.display_x+x*global.scale_factor,
			global.display_y+y*global.scale_factor,image_xscale,image_yscale,image_angle,image_blend,image_alpha);