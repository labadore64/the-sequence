menuControlForceDrawMenuBackground();

if(global.signal_strength >= minimum_range){
	var obj = instance_create(0,0,cellphoneDialer);
	var stringg = string_digits(objdata_character.phone[character.character]);
	var stringlen = string_length( stringg )
	for(var i = 1; i <= stringlen; i++){
		obj.dial_array[i-1] = real(string_char_at( stringg ,i));
	}
	obj.end_script = menuCallSubScriptCallCompleteDial;
	obj.calling_object = id;
	obj.dial_timer = 8
	obj.newalarm[0] = 15;
	dialing = true;
} else {
	var obj = instance_create(0,0,battleWindow);
	obj.windowText = "There's not enough cell phone signal!";
	with(obj){
		menuParentSetBoxHeight(200)	
	}
}