draw_set_color(text_color);
var stringer = "";
var xx = 0;
var yy = 0;
draw_set_font(global.largeFont)
draw_set_halign(fa_center)
if(ds_exists(menu_name,ds_type_list)){
	for(var i = 0; i < menu_size; i++){
	
		stringer = menu_name_array[ i];
	
		if(!is_undefined(stringer)){
			xx = x1+option_x_offset+option_space*i;
			yy = y1+option_y_offset;

			if(i != menupos || !blink){
				draw_text_transformed_ratio(xx,yy,stringer,text_size,text_size,0)
			}
		}
	}
}
draw_set_halign(fa_left);
draw_set_font(global.textFont)