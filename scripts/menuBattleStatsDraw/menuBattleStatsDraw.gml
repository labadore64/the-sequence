draw_set_alpha(.85)
draw_set_color(c_black)
draw_rectangle_ratio(0,115,800,600,false)
draw_set_halign(fa_left)
draw_set_alpha(1)
var x_translate = translation;
if(global.battlePortrait){
	if(portrait != -1){
		with(portrait){
			if(!surface_exists(surface)){
				script_execute(draw_script)
			}
			var xx = 440;
			var yy = 120;
			var cutter = 50;
	
			draw_surface_part_ext(
								surface,
								0,
								(cutter)*global.scale_factor,
								512*global.scale_factor,
								(512-yy+cutter)*global.scale_factor,
								x_translate+global.display_x + xx*global.scale_factor,
								global.display_y + yy*global.scale_factor,
								1,1,c_white,.25)	
		}
	}
}

draw_rectangle_ratio(0,485,800,600,false)

var movetext = 25;

menuBattleStatsDrawTitle(130-movetext,135)

menuBattleStatsDrawStats(100-movetext,215)

draw_set_color(global.textColor)
draw_set_font(global.textFont)
drawOutlineText(385-movetext,135,chaa.battle_desc,2,2,0,global.textColor)
//drawOutlineText(385-movetext,460,"[CTRL] - Battle History",2,2,0,global.textColor)

menuBattleStatsDrawStatus();

// draw element info
if(element_sprite != -1){
	var movx = 45;
	var movy = 150;
	draw_sprite_extended_ratio(element_sprite,0,movx,movy,2,2,0,c_white,1)
}

draw_letterbox();