if(!menuControl.ignore_tab){
	draw_set_alpha(1)
	draw_set_color(c_black)
	draw_rectangle_ratio(0,550,800,600,false)
	draw_set_color(global.textColor)

	draw_set_halign(fa_right)
	draw_text_transformed_ratio(715,565,argument[1],2,2,0)
	draw_sprite_extended_ratio(spr_scroll_right,ScaleManager.spr_counter,755,570,.45,.45,0,global.textColor,1)

	draw_set_halign(fa_left)
	draw_text_transformed_ratio(85,565,argument[0],2,2,0)
	draw_sprite_extended_ratio(spr_scroll_right,ScaleManager.spr_counter,45,570,-.45,.45,0,global.textColor,1)
}