	if(draw_direction == DIR_SOUTH){
		sprite_index = spr_overworld_leon_front//spr_mainCharacterStandFront;
	} else if (draw_direction == DIR_NORTH){
		sprite_index = spr_overworld_leon_back//spr_mainCharacterStandBack;
	} else if (draw_direction == DIR_WEST){
		sprite_index = spr_overworld_leon_side//spr_mainCharacterStandSide;
		draw_flip = false;
	} else if (draw_direction == DIR_EAST){
		sprite_index = spr_overworld_leon_side//spr_mainCharacterStandSide;
		draw_flip = true;
	} else if (draw_direction == DIR_NORTHWEST){
		//sprite_index = spr_mainCharacterWalkSideBack;
		draw_flip = false;
	} else if (draw_direction == DIR_NORTHEAST){
		//sprite_index = spr_mainCharacterWalkSideBack;
		draw_flip = true;
	} else if (draw_direction == DIR_SOUTHWEST){
		//sprite_index = spr_mainCharacterWalkSideFront;
		draw_flip = false;
	} else if (draw_direction == DIR_SOUTHEAST){
		//sprite_index = spr_mainCharacterWalkSideFront;
		draw_flip = true;
	}