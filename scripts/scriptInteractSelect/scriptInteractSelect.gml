

	if(instance_exists(Player)){

		if(Player.overworld_item_active){
			return false;	
		}
	
		with(Player){
			if(!objGenericCanDo()){
				return false;	
			}
		}
	
		var playerInBounds = false;
	
		//get if there is a collision with the space with the player
		var Mx = Player.interact_x
		var My = Player.interact_y//y + Player.collision_y_adjust;
		
		// if its mouse use the mouse position instead
		if(mouse_check_button_pressed(mb_left)){
			var getValx = 0;
			var getValy = 0;
			var pl_xx = 0;
			var pl_yy = 0;
			
			with(Player){
				getValx =camera_zoom_x;
				getValy =camera_zoom_y;
				pl_xx = x;
				pl_yy = y + collision_y_adjust
			}
			
			// check if the player position is within a certain distance of the closest collision.
			// first, calculate the closest intersection
			
			
			var widd = 800 * (getValx)*global.scale_factor;
			var heii = 600 * (getValy)*global.scale_factor;
			
			var Cx = x;
			var Cy = y;
			
			var Ex = .5*sprite_width;
			var Ey = sprite_height*.5;
			
			var posxx = pl_xx-widd*.5;
			var posyy = pl_yy-heii*.5;
			
			// this is the mouse point
			Mx = pl_xx//posxx + Mx*widd;
			My = pl_yy//posyy + My*heii;
			
	        // relative position of cursor 
	        var Px = Mx - Cx;
	        var Py = My - Cy;
		
	        // calculate closest point on perimeter
	        var Sx = Cx + min(max(Px, -Ex), Ex);
	        var Sy = Cy + min(max(Py, -Ey), Ey);

	        // delta
	        var dx = Mx - Sx;
	        var dy = My - Sy;
 
	        // distance
			var disty = sqrt(dx*dx + dy*dy);

			show_debug_message(string(disty))

			if(disty < Player.interact_dist){
				
				with(Player){
					pl_xx = x;
					pl_yy = y
				}
				
				var posxx = pl_xx-widd*.5;
				var posyy = pl_yy-heii*.5;
				
				Mx = window_mouse_get_x()/global.window_width;
				My = window_mouse_get_y()/global.window_height;
				
				// set the mouse position
				Mx = posxx + Mx*widd;
				My = posyy + My*heii;
			
		        // relative position of cursor 
		        var Px = Mx - Cx;
		        var Py = My - Cy;
		
		        // calculate closest point on perimeter
		        var Sx = Cx + min(max(Px, -Ex), Ex);
		        var Sy = Cy + min(max(Py, -Ey), Ey);

		        // delta
		        var dx = Mx - Sx;
		        var dy = My - Sy;
				
		        // distance
				if(sqrt(dx*dx + dy*dy) < 1){
					 return true;
				} else {
					return false;	
				}
			}
			return false;
		}
		

			playerInBounds = point_in_triangle(Mx, My, p_x1, p_y1, p_x4, p_y4, p_x3, p_y3);

			if(!playerInBounds){
				playerInBounds = point_in_triangle(Mx, My, p_x2, p_y2, p_x4, p_y4, p_x1, p_y1);
			}
		
			if(playerInBounds){
				// if they're in bounds, check to make sure they're facing the right direction.
			
			}

	
		return playerInBounds;
	}

return false;