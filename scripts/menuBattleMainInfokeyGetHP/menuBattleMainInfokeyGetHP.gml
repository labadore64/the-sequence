var chara = argument[0]

var returner = "";

if(is_undefined(chara) || chara == -1){
	returner = ""	
} else {
	var mps = floor(100*chara.hp.current/chara.hp.base);
	
	var returner = string(mps) + "% HP " + chara.name;
}

return returner;