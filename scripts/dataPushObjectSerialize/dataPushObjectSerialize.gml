var mapper = ds_map_create();

var json;

with(PropParent){
	if(prop_push_id != ""){
		var json = ds_map_create();

		ds_map_add(json,"x",x);
		ds_map_add(json,"y",y);

		var stringer = json_encode(json);
		ds_map_add(mapper,prop_push_id,stringer);

		ds_map_destroy(json);
	}
}

ds_map_secure_save(mapper,argument[0]+"pushedobj.sav")

ds_map_destroy(mapper)