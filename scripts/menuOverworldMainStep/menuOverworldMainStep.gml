// Inherit the parent event
gml_pragma("forceinline");
event_inherited();

blink_counter--;
if(blink_counter < 0){
	blink = !blink;
	blink_counter = blink_rate;
}