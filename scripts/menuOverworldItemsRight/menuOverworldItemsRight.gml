var sizer = 0;

do{

	menu_option++;

	if(menu_option >= ITEM_TYPE_QUANTITY){
		menu_option = 0;	
	}
	
	sizer = ds_list_size(list_of_lists[|menu_option]);
} until (sizer != 0)

menupos = 0;
toppos = 0;

menuOverworldItemSetList(enter_script);
jump_y = 1
jump_speed = jump_start_speed;
menuParentUpdate();
hudPopulateItem("item",item_id[|menupos]);

surface_update = true;
event_perform(ev_alarm,11)

tts_clear();
tts_say(menu_option_names[|menu_option])