//reads prop from the list and creates the prop in the overworld.

var xx;
var yy;
var obj_type;
var _prop_id = argument[0];

var map = obj_data.prop_place;

var data = ds_map_find_value(map,_prop_id);

if(!is_undefined(data)){
	var json = json_decode(data);
	
	xx = ds_map_find_value(json,"x");
	yy = ds_map_find_value(json,"y");
	obj_type = ds_map_find_value(json,"obj_type")
	
	if(!is_undefined(xx) &&
		!is_undefined(yy) &&
		!is_undefined(obj_type)){
	
		var cando = true;
		
		//makes sure the object doesn't already exist
		with(PropParent){
			if(prop_id == _prop_id){
				cando = false;
			}
		}
		
		if(cando){
			var obj = instance_create(xx,yy,obj_type);
			obj.prop_id = _prop_id;
		}
	
	}
	
	ds_map_destroy(json);
}