with(argument[0]){
	var test = current_dist - last_dist;
	if(directional == AX_COLLISION_DIRECTION_LEFT ||
		directional == AX_COLLISION_DIRECTION_RIGHT){
		
		if(test > 0){
			var newtest = abs(test);
			if(newtest > global.AXCollisionDistance*.7){
				return true
			}
		}
	}
}

return false;