var args = argument[0];

var item_id = real(textboxArgumentGet("item_id"));
var item_event_id = textboxArgumentGet("item_event_id");
var item_quantity = real(textboxArgumentGet("item_quantity"));

soundfxPlay(soundPickupItem)

giveItem(item_id,item_quantity);

eventItemSet(item_event_id);