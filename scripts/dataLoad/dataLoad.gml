var filename = working_directory + "save\\";

var sizer = ds_list_size(obj_data.characters);

var cando = true;

for(var i = 0; i < sizer; i++){
	cando = dataCharacterLoad(i,filename);
	if(!cando){
		break;	
	}
}

if(cando){
	cando = dataItemLoad(filename)
}

if(cando){
	cando = dataMainLoad(filename);
}

if(cando){
	cando = dataOptionLoad(filename);	
}

dataPushObjectDeserialize(filename)

if(cando){
	cando = dataLoadValidate();	
}

if(cando){
	
	with(dataCharacter){
		statLoadMain(id);	
	}
	playerUpdatePos();
	
	with(AXManager){
		keyboard_keybind_load("keyboard.json");
		gamepad_keybind_load("gamepad.json")
	}
	
} else {

	
}

return cando;