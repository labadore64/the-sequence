
	var nope = argument[0];

	with(Battler){
		var percent_recover = spe_guard.current*BATTLE_MP_RECOVER_RATIO;
	
		var recover_amount = clamp(floor((percent_recover*.01)*mp.base),1,999);
	
		var multiplier = 0;
		
		if(defended_this_turn){
			multiplier = 15;	
		}
	
		var recover_val = mp.current + floor(recover_amount+multiplier);
	
		battlerMPChange(id,recover_val);	
	}
