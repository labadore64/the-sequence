if(objGenericCanDo()){
	if(access_mode()){
		var bookmark = bookmarkGetNearby(x,y-collision_y_adjust);

		if(bookmark > 0){
			if(bookmark.object_index == AXBookmark){
				instance_create(0,0,menuBookmark);
				active = false;

				menuBookmark.bookmark_id = bookmark;
			}
		} else {
		
			var success = bookmarkCreate(x,y);
			if(success > -1){
				tts_say(success.name + " placed");
			}
		}
	}
}