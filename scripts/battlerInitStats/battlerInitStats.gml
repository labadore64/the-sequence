phy_power = instance_create(0,0,battleStat);
phy_guard = instance_create(0,0,battleStat);
mag_power = instance_create(0,0,battleStat);
mag_guard = instance_create(0,0,battleStat);
spe_power = instance_create(0,0,battleStat);
spe_guard = instance_create(0,0,battleStat);

hp = instance_create(0,0,battleStatSlider);
mp = instance_create(0,0,battleStatSlider);

color = $000000;

stat_boosts = ds_list_create();
status_effects = ds_list_create();

for(var i = 0; i < 5; i++){
	item[i] = 0;	
}

name = "";

character = 0;

experience = 0;
level = 1;


element = -1;