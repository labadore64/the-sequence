//this is for all particles that are not dynamically generated.
//for example sprays and shit
var scaler = .25

global.fireParticle = part_type_create()
part_type_shape(global.fireParticle,pt_shape_disk)
part_type_size(global.fireParticle,0.20,0.60,0,0)
part_type_scale(global.fireParticle,.4,.6)
part_type_color3(global.fireParticle,16749459,33023,255)
part_type_alpha3(global.fireParticle,0.04,0.06,0.07)
part_type_speed(global.fireParticle,0.70,2.60,-0.02,0)
part_type_direction(global.fireParticle,85,95,0,9)
part_type_gravity(global.fireParticle,0,270)
part_type_orientation(global.fireParticle,0,0,10,20,1)
part_type_blend(global.fireParticle,1)
part_type_life(global.fireParticle,60,80)

global.steamParticle = part_type_create();
part_type_shape(global.steamParticle,pt_shape_smoke);
part_type_size(global.steamParticle,0.60,0.60,-0.01,0);
part_type_scale(global.steamParticle,1,1);
part_type_color3(global.steamParticle,9922907,8421504,0);
part_type_alpha3(global.steamParticle,0.04,0.06,0.07);
part_type_speed(global.steamParticle,0.70,3,-0.02,0);
part_type_direction(global.steamParticle,85,95,0,9);
part_type_gravity(global.steamParticle,0.01,90);
part_type_orientation(global.steamParticle,0,0,10,20,1);
part_type_blend(global.steamParticle,1);
part_type_life(global.steamParticle,60,80);

global.playerWaterDie = part_type_create()
part_type_shape(global.playerWaterDie,pt_shape_cloud)
part_type_size(global.playerWaterDie,.15,.15,.02,0)
part_type_scale(global.playerWaterDie,1*scaler,1*scaler)
part_type_color3(global.playerWaterDie,c_white,$ffCCAA,$ffaa88)
part_type_alpha3(global.playerWaterDie,0.04,0.06,0.07)
part_type_speed(global.playerWaterDie,3*scaler,3*scaler,0,0)
part_type_direction(global.playerWaterDie,15,175,0,0)
part_type_gravity(global.playerWaterDie,.1*scaler,270)
part_type_orientation(global.playerWaterDie,0,0,10,20,1)
part_type_blend(global.playerWaterDie,1)
part_type_life(global.playerWaterDie,10,10)


global.playerWaterParticle = part_type_create()


part_type_shape(global.playerWaterParticle,pt_shape_disk)
part_type_size(global.playerWaterParticle,.35,0.35,0,0)
part_type_scale(global.playerWaterParticle,scaler,scaler)
part_type_color3(global.playerWaterParticle,$ffaa33,$cc7700,$770000)
part_type_alpha3(global.playerWaterParticle,.15,.25,0.05)
part_type_speed(global.playerWaterParticle,9.5*.25,9.5*.25,0,0)
part_type_direction(global.playerWaterParticle,85,95,0,0)
//part_type_gravity(global.playerWaterParticle,.1*scaler,270)
part_type_orientation(global.playerWaterParticle,0,0,10,20,1)
part_type_blend(global.playerWaterParticle,0)
part_type_life(global.playerWaterParticle,20,20)
part_type_death(global.playerWaterParticle,1,global.playerWaterDie)


global.playerFireParticle = part_type_create()
part_type_shape(global.playerFireParticle,pt_shape_disk)
part_type_size(global.playerFireParticle,0.20,0.60,0.01*scaler,0)
part_type_scale(global.playerFireParticle,1.25*scaler,1.25*scaler)
part_type_color3(global.playerFireParticle,16749459,33023,255)
part_type_alpha3(global.playerFireParticle,0.04,0.06,0.07)
part_type_speed(global.playerFireParticle,2,3,-0.02,0)
part_type_direction(global.playerFireParticle,85,95,0,9)
part_type_gravity(global.playerFireParticle,0,270)
part_type_orientation(global.playerFireParticle,0,0,10,20,1)
part_type_blend(global.playerFireParticle,1)
part_type_life(global.playerFireParticle,15,20)
