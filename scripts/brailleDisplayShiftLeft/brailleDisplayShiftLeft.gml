with(argument[0]){
	var replacechar;
	display_string = "";
	for(var i = 0; i < count; i++){
		if(i < count - 1){
			replacechar = braille_display[i+1].cell_character;
		} else {
			replacechar = " ";	
		}
		brailleDataSetCharacter(replacechar,braille_display[i])
		
		display_string+= braille_display[i].cell_character;
	}	
}