var getValx = zoomx
var getValy = zoomy

var player_x = x;
var player_y = y;

if(follow_obj != -1){
	if(follow_obj == Player){
		with(Player){
			getValx =camera_zoom_x;
			getValy =camera_zoom_y;
			player_x = camera_x//x;
			player_y = camera_y//y;
		}
	} else {
		with(follow_obj){
			player_x = x;
			player_y = y;	
		}
	}
	x = player_x;
	y = player_y;
} else {
	
}

/*
with(Player){
	getValx = camera_zoom_x;
	getValy = camera_zoom_y;
	player_x = camera_x//x;
	player_y = camera_y//y;
}
*/

zoomx = getValx;
zoomy = getValy;

var curviewx = getValx*800//camera_get_view_x(curview);
var curviewy = getValy*600//camera_get_view_y(curview);


draw_cornerx1 = -curviewx*.5 +x; //top left corner x 
draw_cornery1 = -curviewy*.5 + y; //top left corner y

draw_cornerx2 = curviewx*.5 +x; //bottom right corner x
draw_cornery2 = curviewy*.5 + y; //bottom right corner y

bird_cornerx1 = -curviewx*.45 +x; //top left corner x 
bird_cornery1 = -curviewy*.45 + y; //top left corner y
bird_cornerx2 = curviewx*.45 +x; //bottom right corner x
bird_cornery2 = curviewy*.45 + y; //bottom right corner y

// update view which is used for tiles

camera_set_view_pos(bg_camera, draw_cornerx1,draw_cornery1)
camera_set_view_size(bg_camera,draw_cornerx2-draw_cornerx1,draw_cornery2-draw_cornery1)