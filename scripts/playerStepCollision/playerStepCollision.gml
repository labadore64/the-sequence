	
	var collision = playerCollisionTest();
	var ori_ori = orientation;
	
	if(collision){
		var cutoff = 75; //any degrees with more difference are automatically treated as perpendicular
		var line_angle = -1; //angle of hit line
		//orientation is ori val

		with(collision_interacting){

			if(line12_test){
				line_angle = line12_angle;	
			} else if (line23_test){
				line_angle = line23_angle;	
			} else if (line34_test){
				line_angle = line34_angle;	
			} else if (line41_test){
				line_angle = line41_angle;	
			}
		}
		
		var line_angle_ops = (line_angle+180) mod 360;
		
		var test_orientation = dir

		var test1 = abs(angle_difference(test_orientation,line_angle))
		var test2 = abs(angle_difference(test_orientation,line_angle_ops))

		if(min(test1,test2) < cutoff){

			if(test1 < test2){
				orientation = -line_angle;
			} else {
				orientation = -line_angle_ops;	
			}
		
			playerUpdateOrienMove();
		
			collision = playerCollisionTest();
		}
	}
	
if(collision){
	move_this_frame=false;
	draw_action = PLAYER_ACTION_STAND;
	orientation = ori_ori
	no_move = true
	newalarm[0] = 3;
	adjust_this_frame = true;
	if(instance_exists(collision_interacting)){
		if(collision_interacting.object_index == Collision){
			if(collision_interacting.can_push){
				push_count++;
				if(push_count > push_time){
					with(collision_interacting){
						push_caused_by_player = true;
						push_this_frame = true;	
						collisionPushSet();
					}
				}
			} else {
				push_count = 0;	
			}
		} else {
			push_count = 0;	
		}
	} else {
		push_count = 0;	
	}
} else {
	push_count = 0;	
}
	orientation = (orientation+720) mod 360;
	