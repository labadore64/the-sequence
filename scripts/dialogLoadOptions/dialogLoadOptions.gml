var dio = -1;

with(DialogHandler){
	if(ds_exists(option_stringList,ds_type_list)){
	if(ds_list_empty(option_stringList)){
		instance_destroy();	
	} else {
		var sizer = ds_list_size(option_stringList);
		dio = instance_create(0,0,DialogOption);
		var counter = 0;
		
		for(var i = 0; i < sizer; i++){
			ds_list_add(dio.option_strings,option_stringList[|i])
			ds_list_add(dio.option_goto,option_gotoList[|i])
			ds_list_add(dio.option_event,option_eventSetList[|i])
			//ds_list_add(dio.option_set_next,option_nextTree[|i])
			//ds_list_add(dio.option_end_script,option_endScript[|i])
			counter++;
			
			if(counter == 4){
				break;	
			}
		}
	}
	}
}

instance_destroy()