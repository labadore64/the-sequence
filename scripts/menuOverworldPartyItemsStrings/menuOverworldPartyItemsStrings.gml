
	event_inherited();

	//menuOverworldSetStringsAsParty(menuOverworldSubScript) 

	item_id = 0
	
	newalarm[2] = 1
	
	itemData(item_id);

	menuParentSetSubtitle("")

	menuParentSetDrawSubtitle(false)
	menuParentSetTitleXOffset(30)

	animation_start = 1 //animation length
	animation_len = animation_start; //animation counter

	menu_draw = menuOverworldPartyItemsDraw

	menuParentSetDrawGradient(false)


	menuParentUpdateBoxDimension();

	menuParentSetSelectWidth(.45)

	menuParentSetOptionXOffset(10)

	menuParentSetInfoboxSubtitleYOffset(45)

	menu_up = menuOverworldPartyItemsLeft;
	menu_down = menuOverworldPartyItemsRight;

	menu_left = menu_up;
	menu_right = menu_down;

	//menuParentTTSLabelRead();