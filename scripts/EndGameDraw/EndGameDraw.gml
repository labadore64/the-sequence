draw_set_color(global.textColor)
draw_set_alpha(.3);
draw_rectangle_ratio(0,0,800,600,false)
draw_set_alpha(1);
if(animation_len == 0){
	menuParentDrawBox();
	draw_set_halign(fa_center);
	menuParentDrawText();
	var counter = 0;
	for(var i = 0; i < menu_size; i++){
	
		var stringer = menu_name_array[ i];
	
		if(!is_undefined(stringer)){
			var xx = x1+option_x_offset+option_space*counter;
			var yy = y1+option_y_offset;

			var mysize = text_size;
			
			if(menupos == i){
				mysize = mysize * selected_scale_amount;	
			}

			draw_text_transformed_ratio(xx,yy,stringer,mysize,mysize,0)

		}
		counter++;
	}
	draw_set_halign(fa_left);
} else {
	menuParentDrawBoxOpen();
}

gpu_set_colorwriteenable(true,true,true,true)

menuParentDrawInfo();