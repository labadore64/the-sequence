
if(animation_len == 0){
	//menuParentDrawBox();

	

	draw_clear(c_black)
	
	draw_set_color($333333)
	
	var item_posx = 600;
	var item_posy = 415;
	
	var item_shadow = 45;
	
	var item_shadow_width = 55;
	var item_shadow_height = 15;
	
	if(item_sprite != -1){
		
		draw_ellipse(
				global.display_x+(item_posx-item_shadow_width)*global.scale_factor,
				global.display_y+(item_posy+item_shadow-item_shadow_height)*global.scale_factor,
				global.display_x+(item_posx+item_shadow_width)*global.scale_factor,
				global.display_y+(item_posy+item_shadow+item_shadow_height)*global.scale_factor,
				false);
	}
	
	if(item_sprite != -1){
		draw_sprite_extended_ratio(item_sprite,0,600,405,.55,.55,0,c_white,1);
	}


	
	if(drawGradient){
		draw_rectangle_color_ratio(x1,0,x2,0+gradient_height,gradient_color1,gradient_color2,gradient_color2,gradient_color1,false)
	}

	menuParentDrawText();

	menuCharacterBuyDrawOptions();
	
	menuOverworldItemDrawInfo();
	
	var baseheight = 175;
	var basewidth = 400;
	var base_space = 200;
	var vertical_space = 40;
	
	menuOverworldItemDrawStat(basewidth,baseheight,$7F7FFF,spr_physpower_icon,stat_attack,25)
	menuOverworldItemDrawStat(basewidth,baseheight+vertical_space,$FFFF7F,spr_physguard_icon,stat_defense,25)
	menuOverworldItemDrawStat(basewidth,baseheight+vertical_space*2,$7FFFFF,spr_spepower_icon,stat_agility,25)
	
	menuOverworldItemDrawStat(200+basewidth,baseheight,$FF7FFF,spr_magpower_icon,stat_magic,25)
	menuOverworldItemDrawStat(200+basewidth,baseheight+vertical_space,$7FFF7F,spr_magguard_icon,stat_resistance,25)
	menuOverworldItemDrawStat(base_space+basewidth,baseheight+vertical_space*2,$FF7F7F,spr_speguard_icon,stat_mpr,10)
	
	
	var heig = 135;
	//var scalez = .65;
	
	var stringer = "For Sale"
	
	draw_set_halign(fa_center)
	draw_set_valign(fa_middle)
	var type_posx = 200-10;
	var type_posy = 5;
	draw_set_color(global.textColor)

	draw_text_transformed_ratio(type_posx,heig+type_posy,stringer,3,3,0)


	draw_set_valign(fa_top)
	draw_set_halign(fa_left)
	gpu_set_colorwriteenable(true,true,true,true)

	menuParentDrawInfo();
	
	
} else {
	//draw_clear(c_black)
	//menuParentDrawBoxOpen();
}

