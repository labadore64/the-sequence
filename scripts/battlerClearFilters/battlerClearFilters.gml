var chara = argument[0];

with(chara){
	var filsize = ds_list_size(status_effects);
	var filter = 0;
	
	for(var i = 0; i < filsize; i++){
		filter = status_effects[|i];
		
		with(filter){
			instance_destroy();	
		}
	}
	
	ds_list_clear(status_effects);
}