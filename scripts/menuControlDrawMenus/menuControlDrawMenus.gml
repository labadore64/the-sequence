gpu_set_texfilter(false);
if(active_menu > 0){


if(!surface_exists(menuControlSurface)){
	menuControlSurface = surface_create_access(global.window_width,global.window_height);
	menuControlDrawBG();
}

if(instance_exists(active_menu)){
	if(active_menu.drawOtherMenus){
		draw_surface(menuControlSurface,0,0);
	}
} else if (please_draw_this_frame){
	//draw_clear_alpha(c_lime,1);
	draw_surface(menuControlSurface,0,0);
	
}

var trans = transparent;
var alpsa = bg_alpha;
var cola = bg_color


draw_set_alpha(1)

//if(!delay){
	with(active_menu){
		if(visible){
			if(trans){
				gpu_set_colorwriteenable(true,true,true,false)
				if(animation_len == 0){
					if(draw_bg){
						draw_set_alpha(draw_bg_alpha);
						draw_set_color(cola);
						draw_rectangle_ratio(-1,-1,800,600,false)
						}
					}
				}
				draw_set_alpha(1);
				draw_set_color(global.textColor)
				if(menu_draw != -1){
					script_execute(menu_draw);	
				}
			}
		}
	}
//}