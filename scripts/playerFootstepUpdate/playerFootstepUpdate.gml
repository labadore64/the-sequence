	distance_travelled+=move_rate*menuControl.timer_diff;
	footstep_counter+= move_rate*menuControl.timer_diff;
	if(footstep_counter > footstep_max){
		footstep_counter = 0;
		var multi = 1;
		
		// get the type of footstep at the tile you're at
		if(CameraOverworld.tiles_stepsound > -1){
			var soundid = tilemap_get_at_pixel(CameraOverworld.tiles_stepsound,Player.x,Player.y+32)
			var sounde = -1;
			if(soundid > -1){
				sounde = CameraOverworld.stepsound[soundid]
			}
			//play the sound
			audio_sound_gain(sounde,global.sfxVolume*multi,0)
			audio_sound_pitch(sounde,random_range(.85,1.15))
			soundfxPlay(sounde)
		}
		
		incrementGameStat(GAMESTAT_STEPS)
		playerDecreaseStep();
	}