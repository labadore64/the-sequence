if(!instance_exists(TextBox)){
	if(!menuControl.help_open){
		var canDoItem = !eventItemCheck(item_event_id);

		if(canDoItem){
			if(instance_exists(Player)){
				if(Player.active){
					with(Player){
						active = false;	
					}
		
					var ob = textboxOverworld(item_text_id)
					ob.triggered_by_player = true;
					textboxScriptSet(ob,overworldTextItemGive,-1)
					textboxScriptAddArgument(ob,"item_id",item_id)
					textboxScriptAddArgument(ob,"item_event_id",item_event_id)
					textboxScriptAddArgument(ob,"item_quantity",item_quantity)
					if(item_id != -1){
						itemData(item_id);
				
						if(item_quantity != 1){
							textboxAfterTextAddLine(ob,global.playerName + " found " + string(item_quantity) + " " + name + "s!");
						} else {
							var getchar = string_lower(string_char_at(name,1));
							if(getchar != "a" &&
								getchar != "e" &&
								getchar != "i" &&
								getchar != "o" &&
								getchar != "u"){
									textboxAfterTextAddLine(ob,global.playerName + " found a" + " " + name + "!");
								} else {
									textboxAfterTextAddLine(ob,global.playerName + " found an" + " " + name + "!");	
								}
						}
					}
				
					ob.clean_script = textboxItemTutorial;
		
				}
			}
		} else {
			//act like a normal textbox if you've already gotten the item.
			overworldTextStaticCreateTextbox();	
		}
	}
}