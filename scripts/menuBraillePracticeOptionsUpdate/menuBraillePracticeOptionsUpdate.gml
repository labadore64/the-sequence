//updates the option based on direction.
//-1 = left, 1 = right
var curpos = 0;

var list_size = 0;

if(menupos == 0){
	//speed
	list_size = 11
	curpos = global.BrailleTimerSpeed;
} else if(menupos == 1){
	//color	
	list_size = ds_list_size(text_color_values);
	curpos = ds_list_find_index(text_color_values,global.braille_practice);
	if(curpos == -1){
		curpos = 0;	
	}
} else if (menupos == 2){
	//font	
	list_size = 2
	curpos = global.BrailleSound
} else if (menupos == 3){
	//font	
	list_size = 2
	curpos = global.brailleRepeat
} else if (menupos == 4){
	list_size = 2;
	curpos = global.brailleBackground;
} else if (menupos == 5){
	list_size = 2;
	curpos = global.brailleHint;
}

var directionz = argument[0];

if(directionz == -1){
	//if moving left	
	curpos--;
	if(curpos < 0){
		if(wrapz[menupos]){
			curpos = list_size-1;
		} else {
			curpos = 0;	
		}
	}
} else if (directionz == 1){
	//if moving right	
	curpos++;
	if(curpos > list_size-1){
		if(wrapz[menupos]){
			curpos = 0;	
		} else {
			curpos = list_size - 1;	
		}
	}
}

curposs[0] =  global.BrailleTimerSpeed;
curposs[1] = ds_list_find_index(text_color_values,global.braille_practice);
if(curposs[1] == -1){
	curposs[1] = 0;	
}
curposs[2] = global.BrailleSound
curposs[3] = global.brailleRepeat;
curposs[4] = global.brailleBackground;
curposs[5] = global.brailleHint;

//finally set values accordingly
if(menupos == 0){
	//speed
	global.BrailleTimerSpeed = curpos;
	curposs[0] = curpos;
} else if(menupos == 1){
	//color	
	global.braille_practice = text_color_values[|curpos];
	curposs[1] = curpos;
} else if (menupos == 2){
	//font	
	global.BrailleSound = curpos;
	curposs[2] = curpos;
} else if (menupos == 3){
	global.brailleRepeat = curpos;
	curposs[3] = curpos;
} else if (menupos == 4){
	global.brailleBackground = curpos;
	curposs[4] = curpos;
} else if (menupos ==5){
	global.brailleHint = curpos;
	curposs[5] = curpos;
}
menuParentUpdateLookAndFeel()

menuBraillePracticeOptionInfokeyStrings();

soundfxPlay(sound_select)