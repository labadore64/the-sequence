
		instance_destroy();

	with(menuBattleFight){
		menuBattleFightSetHowmany();

		character = BattleHandler.char_input_character;
		character_load = BattleHandler.char_input_counter;

		battleHandlerSelectAllyClear()
		battleHandlerSelectAlly(ds_list_find_index(BattleHandler.allies,character),true);

		battleHandlerClearFlash();
		battleHandlerFlashSelected();	
		menupos = 0;
		if(character > 0){

			ds_list_clear(menu_name); //name of all menu items
			ds_list_clear(menu_description); //descriptions of all menu items
			ds_list_clear(menu_script); //scripts for all menu items
		
			menuBattleFightPopulateItems()
			//menuAddOption("Color","Use your color to change aura.",menuBattleFightPaintScript)
			menu_size = menuGetSize();

			tts_say(character.name + " " + menu_name_array[0])
		}
	}

	with(BattleHandler){
		var sizer = ds_list_size(select_list);
		if(char_input_counter >= sizer){
			instance_create(0,0,battleStartAttack)
			with(menuParent){
				active=false;
				battleHandlerClearFlash();
				battleHandlerSelectAllyClear()
				battleTextDisplayClearText();
				instance_destroy()	
			}
		}
	}
