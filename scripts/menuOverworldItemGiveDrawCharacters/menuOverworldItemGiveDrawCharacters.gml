	counter = 0;
	var divid = draw_width*.333
	
	for(var i = 0 ; i < 2; i++){
		draw_set_color(global.textColor);
		draw_rectangle_ratio(x1+divid*(i+1)-2,y1,x1+divid*(i+1)+2,y2,false)
	}
	
	draw_set_halign(fa_center)
	for(var i = 0; i < menu_size; i++){
	
		stringer = menu_name_array[ i];
	
		if(!is_undefined(stringer)){
			xx = x1+divid*counter+divid*.5;
			yy = y1+option_y_offset;

			draw_text_transformed_ratio(xx,yy,stringer,text_size,text_size,0)
			
		}
		counter++;
	}
	draw_set_halign(fa_left)