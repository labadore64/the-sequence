var chara = argument[0];
var filtername = string_lower(argument[1]);

with(chara){
	var sizer = ds_list_size(status_effects);

	var fill = -1;

	for(var i = 0; i < sizer; i++){
		fill = status_effects[|i];
		if(!is_undefined(fill)){
			if(instance_exists(fill)){
				if(string_lower(fill.name) == filtername){
					return fill;	
				}
			}
		}
	}

}

return noone;