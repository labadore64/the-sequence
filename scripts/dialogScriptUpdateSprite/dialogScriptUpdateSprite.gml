var list = argument[0]

var val = list[|line];

var sprite_id = -1;

if(val != "none"){
	sprite_id = asset_get_index(val);
}

with(DialogHandler){
	sprite = sprite_id;
	sprite_in = 0;
	if(sprite_id != -1){
		sprite_len = sprite_get_number(sprite_id)	
		sprite_stop = false;
	}
}