// replaces a single string in the hud controller
var stringer = argument[0];
var map = argument[1];

if(ds_exists(map,ds_type_map)){

	var str_size = string_length(stringer);
	var position = string_pos("{{",stringer);
	var initpos = position;
	if(position > 0){
		while(position <= str_size && position > 0){
			while(!(
					string_char_at(stringer,position) == "}" &&
					string_char_at(stringer,position+1) == "}"
				)){
				position++;
			}

			var val = string_replace_all(string_copy(stringer,initpos+2,(position-initpos-2))," ","");

			var diagram = objdataToIndex(objdata_diagram,val);
			var replacestring = " ";
			if(diagram > -1){
				if(access_mode() || global.tts){
					replacestring = objdata_diagram.alt_text[diagram]
				} else {
					ds_map_replace(map,initpos,diagram)
				}
			}
			stringer=string_delete(stringer,initpos,((position-initpos+2)));
			stringer=string_insert(replacestring,stringer,initpos);
			position = string_pos("{{",stringer);
			initpos = position;
			str_size = string_length(stringer);
		}
	}
	
	diagram_length = ds_map_size(map)
}

return stringer;