	gpu_set_colorwriteenable(true,true,true,true)

	if(surface_fade > 0){
		if(!surface_exists(surf)){
			surf = surface_create(global.window_width,global.window_height);
		} else {
			if(ScaleManager.updated){
				surface_resize(surf,global.window_width,global.window_height);	
			}
		}
	
		surface_set_target(surf)

		draw_clear_alpha(c_black,.5)

		var xx = 80;
		var yy = 20;

		var borderr = 4;

		var spacer = 180;

		var size = 130;

		draw_set_alpha(1);

		// input cells
		for(var i = 0; i < 2; i++){
			for(var j = 0; j < 3; j++){
		
				if(column_pos == i &&
					menupos == j){
						draw_set_color(global.textColor);
						draw_rectangle_ratio(
								xx+spacer*i-borderr*2,
								yy+spacer*j-borderr*2,
								xx+spacer*i+size+borderr*2,
								yy+spacer*j+size+borderr*2,
								false)		
				}
		
				draw_set_color($222222);
				draw_rectangle_ratio(
						xx+spacer*i-borderr,
						yy+spacer*j-borderr,
						xx+spacer*i+size+borderr,
						yy+spacer*j+size+borderr,
						false)		
		
				if(cells[i,j]){
					draw_set_color(global.textColor)	
				} else {
					if(cells_correct[i,j]){
						draw_set_color(hint_color);	
					} else {
						draw_set_color($333333)
					}
				}
		
				draw_rectangle_ratio(
								xx+spacer*i,
								yy+spacer*j,
								xx+spacer*i+size,
								yy+spacer*j+size,
								false)
			}
		}
	
		draw_set_color(c_black)
		draw_set_halign(fa_center);
	
		var textsize = 12;
	
		if(cell_character_length > 1){
			textsize = 5;	
		}

		draw_text_transformed_ratio(600,250,cell_character_display,textsize,textsize,0)

		draw_set_color(global.textColor)

		draw_text_transformed_ratio(600,250,cell_character_display,textsize,textsize,0)

		// draw button
		if(global.mouse_active){
			draw_set_color(c_white)
			var xxpo = 575;
			var yypo = 450;
			
			draw_rectangle_ratio(xxpo-80-5,yypo-40-5,xxpo+80+5,yypo+40+5,false)	
			draw_set_color($111111)
			draw_rectangle_ratio(xxpo-80,yypo-40,xxpo+80,yypo+40,false)	
			draw_set_color(c_white)
			draw_set_halign(fa_center)
			draw_set_valign(fa_middle)
			draw_text_transformed_ratio(xxpo,yypo,"Submit",2,2,0)
			draw_set_valign(fa_top)
			draw_set_halign(fa_left)
		}

		// draw the timer
		if(timer_active){
			xx = 400;
			yy = 550
			width = 200;
			height = 10;

			drawBar(xx,yy,width,height,timer_percent,timer_color);
		}

		surface_reset_target();

		draw_surface_ext(surf,0,0,1,1,0,c_white,surface_fade);
	}

	//draw the completed message
	if(completed){
		if(!surface_exists(result_surf)){
			result_surf = surface_create(global.window_width,global.window_height);
		} else {
			if(ScaleManager.updated){
				surface_resize(result_surf,global.window_width,global.window_height);	
			}
		}

		surface_set_target(result_surf);
		draw_clear_alpha(c_black,0);
		draw_set_halign(fa_left);
		drawOutlineText(result_start_counter,250,result_text[result_value],8,8,0,global.textColor)
		surface_reset_target();
	
		draw_surface_ext(result_surf,global.display_x,global.display_y,1,1,0,c_white,result_fade_counter);
	}

	with(brailleGameTimeAttack){
		if(visible){
			with(countdownTimer){
				countdownTimerDraw();
			}
	
			draw_set_halign(fa_right);
			draw_set_color(global.textColor)
			var xxx = 780
			var yyy = 30;
			var spacee = 35;
			var scaler = 3;
			draw_text_transformed_ratio(xxx,yyy,"Score:",scaler,scaler,0);
			draw_text_transformed_ratio(xxx,yyy+spacee,display_score,scaler,scaler,0);
			draw_text_transformed_ratio(xxx,yyy+spacee*2,"Correct:",scaler,scaler,0);
			draw_text_transformed_ratio(xxx,yyy+spacee*3,string(current_success),scaler,scaler,0);
		}
	}

	with(brailleGameElimination){
		if(visible){
			draw_set_halign(fa_right);
			draw_set_color(global.textColor)
			var xxx = 780
			var yyy = 30;
			var spacee = 35;
			var scaler = 3;
			draw_text_transformed_ratio(xxx,yyy,"Score:",scaler,scaler,0);
			draw_text_transformed_ratio(xxx,yyy+spacee,string(current_success),scaler,scaler,0);
			draw_text_transformed_ratio(xxx,yyy+spacee*2,"Lives Left:",scaler,scaler,0);
			draw_text_transformed_ratio(xxx,yyy+spacee*3,string(rounds),scaler,scaler,0);
		}
	}


	draw_letterbox();
