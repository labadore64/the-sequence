/// @description - loads all current keybind values
/// @function keybind_load()

pad = 0;

for(var i = 0; i < 12; i++){
	if(gamepad_is_connected(i)){
		pad = i;
		break;
	}
}

key_left = keybind_get("left");
key_right = keybind_get("right");
key_up = keybind_get("up");
key_down = keybind_get("down");

key_cancel = keybind_get("cancel");
key_select = keybind_get("select");
key_select2 = keybind_get("select2");

key_control = keybind_get("control");

key_help = keybind_get("help");

key_hud = keybind_get("hud");

//overworld keybinds

key_open_menu = keybind_get("open_menu");
key_run = keybind_get("run");

key_zoom_in = keybind_get("zoomIn");
key_zoom_out = keybind_get("zoomOut");

key_bookmark_place = keybind_get("bookmark");

//Battle keybinds

key_status = keybind_get("control");

//gamepads

gamepad_left = gamepadbind_get("left");
gamepad_right = gamepadbind_get("right");
gamepad_up = gamepadbind_get("up");
gamepad_down = gamepadbind_get("down");

gamepad_cancel = gamepadbind_get("cancel");
gamepad_select = gamepadbind_get("select");

gamepad_control = gamepadbind_get("control");

//overworld gamepadbinds

gamepad_open_menu = gamepadbind_get("open_menu");
gamepad_run = gamepadbind_get("run");

//Battle gamepadbinds

gamepad_status = gamepadbind_get("control");

gamepad_help = gamepadbind_get("help");

gamepad_hud = gamepadbind_get("hud");
gamepad_bookmark = gamepadbind_get("bookmark");

gamepad_see_name = gamepadbind_get("sight");
gamepad_coordinates = gamepadbind_get("coordinates")