/// @description - tests if the rotation is between two values
/// @param angle
/// @param tolerance

var oriz = argument[0]+360;
var tolz = argument[1];
var orienta = argument[2];

orienta = (orienta + 720) mod 360;

var leftside = 360-oriz-tolz+360;
var rightside = 360-oriz+tolz+360;

var returner = false;

if(leftside > 0 && rightside < 360){
	if(leftside <= orienta && orienta <= rightside){
		returner = true;
	}
} else {
	if(leftside < 0){
		if(orienta > 360+leftside){
			returner = true;
		}
	} else {
		if(orienta < rightside - 360){
			returner = true;	
		}
	}
}

	if(leftside <= orienta && orienta <= rightside){
		returner = true;
	}

return returner;