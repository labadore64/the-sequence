if(global.battleAnimate){
	
	if(sprite_animation != -1){
		var spritez = sprite_animation;
	
		with(target){
			sprite_attack = spritez
			sprite_attack_complete = false;
			sprite_attack_frames = sprite_get_number(spritez);
			sprite_attack_current_frame = 0;	
		}
	
	
	
		var other_targs = additional_targets;
	
		var sizer = ds_list_size(other_targs)
	
		var newtarg = noone;
	
		for(var i = 0; i < sizer; i++){
			newtarg = other_targs[|i];
			if(!is_undefined(newtarg)){
				with(newtarg){
					sprite_attack = spritez
					sprite_attack_complete = false;
					sprite_attack_frames = sprite_get_number(spritez);
					sprite_attack_current_frame = 0;	
				}
			}
		}
	
	}

	if(self_animation != -1){
		var spritez = self_animation;
	
		with(character){
			sprite_attack = spritez
			sprite_attack_complete = false;
			sprite_attack_frames = sprite_get_number(spritez);
			sprite_attack_current_frame = 0;	
		}	
	}

	var hide_list = ds_list_create();

	//calculate hide based on hide value
	var target_list = ds_list_create();
	ds_list_copy(target_list,additional_targets);
	ds_list_add(target_list,target);
	if(sprite_show == "Show Only User"){
		var user = character;
	
		with(Battler){
			if(id != user){
				ds_list_add(hide_list,id);	
			}
		}
	
	} else if(sprite_show == "Show Only Target"){
		with(Battler){
			if(ds_list_find_index(target_list,id) == -1){
				ds_list_add(hide_list,id);
			}
		}
	} else if(sprite_show == "Show Only User and Target"){
		var user = character;
	
		with(Battler){
			if(ds_list_find_index(target_list,id) == -1){
				if(id != user){
					ds_list_add(hide_list,id);
				}
			}
		}
	} else if(sprite_show == "Hide All"){
		with(Battler){
			ds_list_add(hide_list,id);	
		}
	}

	//end calculate hide

	var obj = instance_create(0,0,battleAttackSpriteHandler);
	obj.bg_sprite = bg_animation;
	obj.fg_sprite = fg_animation;
	obj.self_sprite = self_animation;
	obj.targ_sprite = sprite_animation;
	obj.anim_len = animation_length;
	ds_list_copy(obj.hide_chars,hide_list);

	ds_list_destroy(hide_list);
	ds_list_destroy(target_list);

	if(global.battleAnimate){
		battleStartAttackPrepareAnimation();
	}
	
}