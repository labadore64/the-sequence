var stringer = argument[0];

var map = ds_map_secure_load(stringer + "main.sav");

var map1 = ds_map_secure_load(stringer + "bird.sav");
var map2 = ds_map_secure_load(stringer + "itemevent.sav");
var map3 = ds_map_secure_load(stringer + "event.sav");
var map4 = ds_map_secure_load(stringer + "dialog.sav");
var map5 = ds_map_secure_load(stringer + "monster_encounter.sav");

var map6 = ds_map_secure_load(stringer + "tablet_found.sav")
var map7 = ds_map_secure_load(stringer + "tablet_char.sav")

var map8 = ds_map_secure_load(stringer + "highscore.sav")

var stats = ds_map_secure_load(stringer + "game_stats.sav")

var data_map = ds_map_secure_load(stringer + "data.sav")
if(ds_exists(map5,ds_type_map)){
	var lemap = ds_map_create();
	ds_map_copy(lemap,map5);
	obj_data.monster_talk_list = lemap;
	
	ds_map_destroy(map5);
}
if(ds_exists(map8,ds_type_map)){
	var lemap = ds_map_create();
	ds_map_copy(lemap,map8);
	obj_data.braille_highscores = lemap;
	
	ds_map_destroy(map8);
}
if(ds_exists(stats,ds_type_map)){
	var lemap = ds_map_create();
	ds_map_copy(lemap,stats);
	obj_data.game_stats = lemap;
	ds_map_destroy(stats)
}

// this loads data like dex, tasks ect
if(ds_exists(data_map,ds_type_map)){
	var email = data_map[? "email"];
	var voice = data_map[? "voice"];
	var task = data_map[? "task"];
	var dex = data_map[? "dex"];
	var val = 0;
	
	for(var i = 0; i < objdata_enemy.data_size; i++){
		val = dex[| i];
		
		if(!is_undefined(val)){
			val = floor(dex[| i]);
			if(val > -1 && val < 3){
				objdata_enemy.state[i] = dex[| i];
			}
		}
	}
	
	for(var i = 0; i < objdata_task.data_size; i++){
		val = task[| i];
		
		if(!is_undefined(val)){
			val = floor(task[| i]);
			if(val > -1 && val <3){
				objdata_task.unread[i] = val;
			}
		}
	}
	
	for(var i = 0; i < objdata_email.data_size; i++){
		val = email[| i];
		
		if(!is_undefined(val)){
			val = floor(email[| i]);
			if(val > -1 && val < 3){
				objdata_email.state[i] = val;
			}
		}
	}
	
	for(var i = 0; i < objdata_voicemail.data_size; i++){
		val = voice[| i]

		if(!is_undefined(val)){
			val = floor(voice[| i]);
			if(val > -1 && val < 3){
				objdata_voicemail.state[i] = val;
			}
		}
	}
	ds_map_destroy(data_map)	
}

if(ds_exists(map,ds_type_map)){
	with(obj_data){
		money = ds_map_find_value(map,"money")
		set_item = ds_map_find_value(map,"set_item");
		time_start = ds_map_find_value(map,"time_seconds");
		pos_x = ds_map_find_value(map,"x_coord");
		pos_y = ds_map_find_value(map,"y_coord");
		current_room = ds_map_find_value(map,"current_room");
		defeat_room = ds_map_find_value(map,"defeat_room");
		defeat_x = ds_map_find_value(map,"defeat_x");
		defeat_y = ds_map_find_value(map,"defeat_y");
		//bird_seen_count = ds_map_find_value(map,"bird_seen_count")
		global.vcr_on = ds_map_find_value(map,"vcr_on");
		global.overworld_on = ds_map_find_value(map,"overworld_on")

		var list2 = dataMapToList(map2)
		var list3 = dataMapToList(map3)
		var list4 = dataMapToList(map4)
		
		var list5 = dataMapToList(map6);
		var list6 = dataMapToList(map7);
		//var list5 = dataMapToList(map5)

		for(var i = 0; i < obj_data.item_event_total; i++){
			if(!is_undefined(list2[|i])){
				obj_data.item_event_set[i] = list2[|i];
			} else {
				obj_data.item_event_set[i] = false;	
			}
		}
	
		for(var i = 0; i < obj_data.event_total; i++){
			if(!is_undefined(list3[|i])){
				obj_data.event_set[i] = list3[|i];
			} else {
				obj_data.event_set[i] = false;	
			}
		}
		ds_list_copy(dialog_events,list4);
		ds_map_copy(bird_seen,map1);
		
		bird_seen_count = ds_map_size(bird_seen);
		
		dataLoadValidateTrimList(dialog_events,IMPORT_MAX_SIZE_EVENT);
		
		//var list_of_party = list5
		var val = -1;
		
		for(var i= 0; i < 3; i++){
			val = ds_map_find_value(map,"party" + string(i));
			if(!is_undefined(val)){
				if(val >= 0){
					ds_list_replace(party,i,characters[|val])
				}
			}
		}
		
		var ob = -1;
		for(var i = 0; i < 26; i++){
			ob = list5[|i];
			if(!is_undefined(ob)){
				tablet_obtained[i] = floor(real(ob));
			}
			ob = list6[|i];
			if(!is_undefined(ob)){
				if(ob > -1){
					tablet_character[i] = characters[|floor(real(ob))];
				}
			}
		}
		
		ds_list_destroy(list5);
		ds_list_destroy(list6);

	}
	ds_map_destroy(map);
	
	ds_map_destroy(map1);
	ds_map_destroy(map2);
	ds_map_destroy(map3);
	ds_map_destroy(map4);
	ds_map_destroy(map6);
	ds_map_destroy(map7);
	//ds_map_destroy(map5);
	
	ds_list_destroy(list2)
	ds_list_destroy(list3)
	ds_list_destroy(list4)
	//ds_list_destroy(list5)
	return true;
} else {
	return false;	
}