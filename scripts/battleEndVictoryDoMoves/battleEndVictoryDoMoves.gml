
var sizer = -1

var move = -1;

var change_list = -1;

var default_string = "@0 mastered the move @1!"
var stringer = "";
var charname = "";
var testWindow = -1;
var ally = -1;
var allies = BattleHandler.allies;

for(var i = 0; i < 3; i++){
	change_list = learned_moves_character[i];
	
	sizer = ds_list_size(change_list);
	ally = allies[|i];
	if(!is_undefined(ally)){

		for(var j = 0; j < sizer; j++){
			move = change_list[|j];
			
			if(!is_undefined(move)){
				stringer = default_string;
				moveData(move);
				charname = ally.name;
				stringer = string_replace_all(stringer,"@0",charname);
				stringer = string_replace_all(stringer,"@1",name);
				testWindow = battleWindowStore(stringer,300,100);

				ds_queue_enqueue(windows,testWindow);
			}
		}
	}
}