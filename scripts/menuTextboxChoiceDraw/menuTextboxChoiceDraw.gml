

//draw box

var yy_spacer = 50;

draw_set_alpha(1)
draw_set_color(c_white)
draw_rectangle(h_start-5,485-(yy_spacer*menu_size)-10,800,485,false)
draw_set_color(c_black)
draw_rectangle(h_start,485-(yy_spacer*menu_size)-5,800-5,480-5,false)

//draw text

draw_set_color(global.textColor)
draw_set_font(global.textFont)

var padd = 10;
var vpadd = 10;

var stringer = "";
draw_set_halign(fa_center)
for(var i = 0; i < menu_size; i++){

	stringer = menu_name_array[i];
	if(!is_undefined(stringer)){
		draw_text_transformed_ratio(h_start + (800-h_start)*.5, 485-(yy_spacer*menu_size)+(i*yy_spacer)+vpadd-5,stringer,3,3,0) 
	}
}
draw_set_halign(fa_left)

draw_set_alpha(.45)
draw_rectangle(h_start,485-(yy_spacer*menu_size)+((menupos)*yy_spacer)-5,800-5,485-(yy_spacer*menu_size)+((menupos+1)*yy_spacer)-5,false)

draw_set_alpha(1)