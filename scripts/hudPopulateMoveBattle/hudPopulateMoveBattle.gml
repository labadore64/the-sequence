// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from
var mp_cost = argument[2];


	if(obj > -1 && obj < objdata_moves.data_size){
		hudControllerAddData(entryname + "element",string(objdata_moves.element[obj]));
		hudControllerAddData(entryname + "type",string(objdata_moves.move_type[obj]));
		hudControllerAddData(entryname + "target",string(objdata_moves.target_number[obj]));
		
		// name and character
		hudControllerAddData(entryname + "name",objdata_moves.name[obj]);
		hudControllerAddData(entryname + "desc",objdata_moves.flavor[obj]);
		
		hudControllerAddData(entryname + "damage",string(objdata_moves.damage[obj]));
		hudControllerAddData(entryname + "mp_cost",string(objdata_moves.mp_cost[obj]));
		hudControllerAddData(entryname + "mp_percent",string(mp_cost));

		// items and tablets
		var len = array_length_2d(objdata_moves.filter,obj);
		for(var i = 0; i < len; i++){
			hudControllerAddData(entryname + "status"+string(i),string(objdata_moves.filter[obj,i]));
		}
	}
