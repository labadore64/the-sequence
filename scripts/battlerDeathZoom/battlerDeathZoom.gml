    //zoom in on character
	var death_amount = 20;
	//death_zoom_do = true;
    distancex = cos(orientation)*20
    distancey = sin(orientation)*20
    var part_center_x = x+distancex
    var part_center_y = y+distancey *image_yscale - 128*image_yscale
    var part_size_xscale = image_yscale
    var part_size_yscale = -image_yscale
    
    var part_x_len = 64 * part_size_xscale*.5
    var part_y_len = 64 * part_size_yscale*.5
	
	var zoom_time = argument[0]
	var height = 600*.45;
	var width = 800*.45;
	
	var poss_x = part_center_x - width*.5;
	var poss_y = part_center_y - height * .5
	
	battleZoomStart(poss_x,poss_y,width,height,zoom_time)