
if(animation_len == 0){
	//menuParentDrawBox();

	gpu_set_colorwriteenable(true,true,true,true)

	
	// draw background
	with(menuBestiaryMain){
		
		if(!surface_exists(surf)){
			surf = surface_create_access(global.window_width,global.window_height)	
		} else {
			if(ScaleManager.updated){	
				surface_resize(surf,global.window_width,global.window_height);
			}
		}
		surface_set_target(surf)
		draw_clear_alpha(c_black,1)
	
		with(brailleGameBackground){
			draw_surface(surf,0,0);
		}
	
		draw_set_alpha(.5)
		draw_set_color(c_black)
		draw_rectangle(0,0,global.window_width,global.window_height,false)
		draw_set_alpha(1)
		surface_reset_target();
		gpu_set_colorwriteenable(true,true,true,true)
		draw_clear_alpha(c_black,1)
		gpu_set_colorwriteenable(true,true,true,false)
		drawSurfAsVCR(surf)	
	}
	gpu_set_colorwriteenable(true,true,true,true)
	
	if(drawGradient){
		draw_rectangle_color_ratio(x1,0,x2,0+gradient_height,gradient_color1,gradient_color2,gradient_color2,gradient_color1,false)
	}

	menuParentDrawText();
	
	menuBestiaryDrawBird();
	
	draw_set_color(global.textColor)
	draw_set_font(global.textFont)
	draw_set_halign(fa_right)
	draw_text_transformed_ratio(65-5,570,string(menupos+1),2,2,0)
	
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(65,570,"/",2,2,0)
	
	draw_set_halign(fa_left)
	draw_text_transformed_ratio(65+5,570,string(menu_size),2,2,0)
	
	with(waveform){
		waveDrawerDraw();
	}
	
	gpu_set_colorwriteenable(true,true,true,true)
	draw_letterbox();
} else {
	//draw_clear(c_black)
	//menuParentDrawBoxOpen();
}
