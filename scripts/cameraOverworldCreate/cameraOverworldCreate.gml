list = ds_priority_create(); //list of objs to draw

surface = -1
surface2 = -1

//the following indicates, relative to the player, what can be drawn
draw_cornerx1 = 0; //top left corner x 
draw_cornery1 = 0; //top left corner y

draw_cornerx2 = 0; //bottom right corner x
draw_cornery2 = 0; //bottom right corner y

//corner for bird detection for binoculars.
bird_cornerx1 = 0;
bird_cornery1 = 0;
bird_cornerx2 = 0;
bird_cornery2 = 0;

zoomx = 1;
zoomy = 1;

draw_script = cameraOverworldDrawGUI;

//shader stuff

//bright/contrast

shd_bc_uni_time = shader_get_uniform(shd_bright_contrast,"time");
shd_bc_var_time_var = 0;

shd_bc_uni_mouse_pos = shader_get_uniform(shd_bright_contrast,"mouse_pos");
shd_bc_var_mouse_pos_x = 400
shd_bc_var_mouse_pos_y = 300;

shd_bc_uni_resolution = shader_get_uniform(shd_bright_contrast,"resolution");
shd_bc_var_resolution_x = 800;
shd_bc_var_resolution_y = 600;

shd_bc_uni_brightness_amount = shader_get_uniform(shd_bright_contrast,"brightness_amount");
shd_bc_var_brightness_amount =0;

shd_bc_uni_contrast_amount = shader_get_uniform(shd_bright_contrast,"contrast_amount");
shd_bc_var_contrast_amount = 0;

shd_bc_increment = .01

shd_bc_shader_enabled = shader_is_compiled(shd_bright_contrast);

//vignete

uni_settings = shader_get_uniform(shd_vignette_noise, "u_settings");
uni_vignette_colour = shader_get_uniform(shd_vignette_noise, "u_vignette_colour");

shd_vig_noise = .09

