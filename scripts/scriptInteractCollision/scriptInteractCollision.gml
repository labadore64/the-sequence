

if(instance_exists(Player)){
	
	var playerInBounds = false;
	
	//get if there is a collision with the space with the player
	var xxx = Player.x
	var yyy = Player.y + Player.collision_y_adjust;
	playerInBounds = point_in_triangle(xxx, yyy, p_x1, p_y1, p_x4, p_y4, p_x3, p_y3);

	if(!playerInBounds){
		playerInBounds = point_in_triangle(xxx, yyy, p_x2, p_y2, p_x4, p_y4, p_x3, p_y3);
	}

	return playerInBounds;
}

return false;