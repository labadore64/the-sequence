var base_mp_cost = (clamp(argument[0]/argument[1].mp.current,0,1));

if(!argument[1].is_enemy){
	if(BattleHandler.MP_cheaper){
		base_mp_cost = floor(base_mp_cost*.33)
	}
}

if(battlerHasFilter(argument[1],"cough")){
	base_mp_cost = (clamp(2*argument[0]/argument[1].mp.current,0,1));	
}
if(battlerHasFilter(argument[1],"ether")){
	base_mp_cost = 0;	
}

return base_mp_cost;