if(!prop_menu){
	if(visible){
		if(object_index == overworld_scalable){
			var scalingx = draw_scalex;
			var scalingy = draw_scaley

			image_xscale = scalingx;
			image_yscale = scalingy;

			image_xscale = image_xscale * (1/zoom_x)*global.scale_factor;
			image_yscale = image_yscale * (1/zoom_y)*global.scale_factor;
	
		}
	}

	stepPropParent();

	if(visible){
		var getValx = 1;
		var getValy = 1;

		var player_x = 0;
		var player_y = 0;

		with(CameraOverworld){
			getValx = zoomx;
			getValy = zoomy;
			player_x = x;
			player_y = y;
		}
		
		var diffx = getValx
		var diffy = getValy
    

	
		var curviewx = getValx*800//camera_get_view_x(curview);
		var curviewy = getValy*600//camera_get_view_y(curview);

		//represents x and y position of the view

		var viewx = curviewx*.5 -player_x
		var viewy = curviewy*.5 - player_y

		draw_temp_xx =(global.scale_factor*(viewx+x)/diffx);
		draw_temp_yy = (global.scale_factor*(viewy+y)/diffy);

	}
}