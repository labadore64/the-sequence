var filename = argument[0];

var map = ds_map_create();

var ds_map1 = ds_map_create();
var ds_map2 = ds_map_create();
var ds_map3 = ds_map_create();
var ds_map4 = ds_map_create();
var ds_map5 = ds_map_create();
var ds_list4 = ds_list_create();

var ds_list5 = ds_list_create();
var ds_list6 = ds_list_create();
var ds_map6 = ds_map_create();
var ds_map7 = ds_map_create();

// map stuff
var data_map = ds_map_create();

with(obj_data){
	
	ds_map_copy(ds_map1,bird_seen)
	
	ds_map_add(map,"set_item",set_item);
	ds_map_add(map,"money",money);
	ds_map_add(map,"time_seconds",time_seconds+time_start);

	ds_map_add(map,"x_coord",pos_x)
	ds_map_add(map,"y_coord",pos_y)
	
	ds_map_add(map,"current_room",current_room)
	ds_map_add(map,"defeat_room",defeat_room)
	ds_map_add(map,"defeat_x",defeat_x)
	ds_map_add(map,"defeat_y",defeat_y)
	//ds_map_add(map,"bird_seen_count",bird_seen_count)
	ds_map_add(map,"vcr_on",global.vcr_on)
	ds_map_add(map,"overworld_on",global.overworld_on)
	
	var party0 = ds_list_find_index(characters,party[|0])
	var party1 = ds_list_find_index(characters,party[|1])
	var party2 = ds_list_find_index(characters,party[|2])
	
	if(is_undefined(party0)){
		party0 = -1;	
	}
	if(is_undefined(party1)){
		party1 = -1;	
	}
	if(is_undefined(party2)){
		party2 = -1;	
	}
	
	ds_map_add(map,"party0",party0)
	ds_map_add(map,"party1",party1)
	ds_map_add(map,"party2",party2)
	
	var items_obtained_list = ds_list_create();
	var events_list = ds_list_create();
	
	for(var i = 0; i < obj_data.item_event_total; i++){
		ds_list_add(items_obtained_list,obj_data.item_event_set[i]);	
	}
	
	for(var i = 0; i < obj_data.event_total; i++){
		ds_list_add(events_list,obj_data.event_set[i]);	
	}
	
	ds_map2 = dataListToMap(items_obtained_list)
	ds_map3 = dataListToMap(events_list)
	ds_map4 = dataListToMap(dialog_events)
	
	ds_list_destroy(items_obtained_list);
	ds_list_destroy(events_list);
	//ds_list_copy(ds_list4,party)

	for(var i = 0; i < 26; i++){
		// whether or not you collected a tablet
		ds_list_add(ds_list5,string(tablet_obtained[i]));
		if(tablet_character[i] > -1){
			ds_list_add(ds_list6,string(tablet_character[i].character));
		} else {
			ds_list_add(ds_list6,string(-1));
		}
	}
	
	ds_map6 = dataListToMap(ds_list5);
	ds_map7 = dataListToMap(ds_list6);
	
	ds_list_clear(ds_list4);
	
	// do the array data here
	var dex = ds_list_create();
	for(var i = 0; i < objdata_enemy.data_size; i++){
		ds_list_add(dex,objdata_enemy.state[i]);
	}
	
	var task = ds_list_create();
	for(var i = 0; i < objdata_task.data_size; i++){
		ds_list_add(task,objdata_task.unread[i]);
	}
	
	var email = ds_list_create();
	for(var i = 0; i < objdata_email.data_size; i++){
		ds_list_add(email,objdata_email.state[i]);
	}
	
	var voice = ds_list_create();
	for(var i = 0; i < objdata_voicemail.data_size; i++){
		ds_list_add(voice,objdata_voicemail.state[i]);
	}
	
	ds_map_add_list(data_map,"dex",dex);
	ds_map_add_list(data_map,"task",task)
	ds_map_add_list(data_map,"email",email)
	ds_map_add_list(data_map,"voice",voice)
	
	ds_map_secure_save(data_map,filename + "data.sav")
	
	ds_list_destroy(dex);
	ds_list_destroy(task);
	ds_list_destroy(email);
	ds_list_destroy(voice)
}

//var returner = json_encode(map);

ds_map_secure_save(map,filename + "main.sav")
ds_map_secure_save(ds_map1,filename+"bird.sav");

ds_map_secure_save(ds_map2,filename+"itemevent.sav");
ds_map_secure_save(ds_map3,filename+"event.sav");
ds_map_secure_save(ds_map4,filename+"dialog.sav");
ds_map_secure_save(ds_map6,filename+"tablet_found.sav");
ds_map_secure_save(ds_map7,filename+"tablet_char.sav");
ds_map_secure_save(monster_talk_list,filename+"monster_encounter.sav");
ds_map_secure_save(braille_highscores,filename+"highscore.sav");
ds_map_secure_save(game_stats,filename+"game_stats.sav");

ds_list_destroy(ds_list4)
ds_map_destroy(ds_map1);
ds_map_destroy(ds_map2);
ds_map_destroy(ds_map3);
ds_map_destroy(ds_map4);
ds_map_destroy(ds_map5);
ds_list_destroy(ds_list5);
ds_list_destroy(ds_list6);
ds_map_destroy(ds_map6);
ds_map_destroy(ds_map7);
ds_map_destroy(data_map)

ds_map_destroy(map);

//return returner;