/// @description - remove an item
/// @param item
/// @param quantity
var it = string(argument[0]);
var quan = string(argument[1]);

var ifdel = false;

with(obj_data){
	var val = ds_map_find_value(inventory,it);
	if(!is_undefined(val)){
		quan = string(real(val)-argument[1]);
		if(real(quan) > 0){
			ds_map_replace(inventory,it,quan);
			ifdel = true;
		} else {
			ds_map_delete(inventory,it);	
		}
	} else {
		ds_map_delete(inventory,it);	
	}
}

return ifdel;