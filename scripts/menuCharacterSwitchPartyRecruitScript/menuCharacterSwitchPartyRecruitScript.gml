

var cando = false;

var recruit_list = ds_list_create();

var list = recruit_list;

with(dataCharacter){
	if(recruited){
		ds_list_add(list,id);	
	}
}

var newlist = obj_data.party;

var sizer = ds_list_size(newlist);
var obj = -1;
var index = 0;
for(var i = 0; i < sizer; i++){
	obj = newlist[|i];
	
	if(!is_undefined(obj)){
		index = ds_list_find_index(list,obj);
		
		if(index != -1){
			ds_list_delete(list,index);	
		}
	}
}

if(!ds_list_empty(recruit_list)){
	cando = true;	
}

ds_list_destroy(recruit_list);

if(cando){
	menuControlForceDrawMenuBackground();
	
	if(!instance_exists(menuCharacterSwitchRecruit)){
		instance_create(0,0,menuCharacterSwitchRecruit)
	}
} else {
	soundfxPlay(soundMenuInvalid)	
}