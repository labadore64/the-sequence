var filename = working_directory + "save\\base.ini";


ini_open(filename)

with(obj_data){
	var tabletcount = 0;
	for(var i = 0; i < 26; i++){
	// whether or not you collected a tablet
		if(tablet_obtained[i]){
			tabletcount++;	
		}
	}
	
	ini_write_real("main","tablets_total",tabletcount);
	
	ini_write_real("main","time_seconds",time_seconds+time_start);
	ini_write_real("main","room",current_room);
	var sizer = ds_list_size(party);
	
	var charaz = -1;
	
	for(var i = 0; i < 3; i++){
		charaz = party[|i];
		if(!is_undefined(charaz)){
			ini_write_real("main","party"+string(i),charaz.character);
		} else {
			ini_write_real("main","party"+string(i),-1);
		}
	}
}

var counter = 0;
with(dataCharacter){
	if(recruited){
		counter++	
	}
}

ini_write_real("main","recruited",counter);

ini_close();