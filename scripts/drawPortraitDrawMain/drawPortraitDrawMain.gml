gpu_set_colorwriteenable(true,true,true,true)

var doupdate = false;

if(!surface_exists(surface)){
	surface = surface_create_access(
							512*global.scale_factor*x_scalefactor,
							512*global.scale_factor*y_scalefactor);	
	doupdate = true;
	
} else {
	if(ScaleManager.updated){
		surface_resize(surface,
						512*global.scale_factor*y_scalefactor,
						512*global.scale_factor*y_scalefactor);
		doupdate = true;
	}
}

if(doupdate || update_surf){
	surface_set_target(surface);
	draw_clear_alpha(c_black,0)
	if(sprite_index != -1){
		draw_self();
	}
	surface_reset_target();
	update_surf = false;
}
