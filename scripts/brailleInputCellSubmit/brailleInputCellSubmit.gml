if(start_going){
	if(completed){
		instance_destroy();
	} else {
		// confirm input	
		completed = true;
		mouseHandler_clear()
		mouseHandler_add(0, 0, 800, 600, brailleInputCellSubmit,"")
		if(brailleInputCellTest()){
			if(timer_percent > .2){
				if(timer_percent > .45){
					if(timer_percent > .8){
						result_value = 0;		
						soundfxPlay(sound_braille_perfect);
					} else {
						result_value = 1;	
						soundfxPlay(sound_braille_great);
					}
				} else {
					result_value = 2;
					soundfxPlay(sound_braille_good);
				}
			} else {
				result_value = 3;
				soundfxPlay(sound_braille_ok);
			}
		} else {
			soundfxPlay(sound_braille_miss);
			result_value = 4;
			timer_percent = 0;
		}
	
		if(braille_sound != -1){
			audio_stop_sound(braille_sound)	
		}
		result_score = timer_percent;
		
		if(result_score < BRAILLE_LOWEST_SCORE &&
			result_value != 4){
			result_score = BRAILLE_LOWEST_SCORE;	
		}
		
		tts_say(result_text[result_value]);
	}
}