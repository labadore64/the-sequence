if(battlerHasAbility(character,"frighten")){
	if(battlerIsAlive(target) && battlerIsAlive(character)){
		if(random(1) < .1){
			battleStatChangeAdd(target,BATTLE_STAT_DEFENSE,.66,2);
		
			var stringerr = "@0 frightens @1! @1's physical guard is temporarily cut!";
			
			stringerr = string_replace_all(stringerr,"@0",target.name)
			stringerr = string_replace_all(stringerr,"@1",character.name)
			
			battleAttackInputAddString(stringerr);
		}
	}
}

if(battlerHasAbility(character,"rude")){
	if(battlerIsAlive(target) && battlerIsAlive(character)){
		if(random(1) < .1){
			battleStatChangeAdd(target,BATTLE_STAT_RESISTANCE,.66,2);
		
			var stringerr = "@0 offends @1! @1's magic guard is temporarily cut!";
			
			stringerr = string_replace_all(stringerr,"@0",target.name)
			stringerr = string_replace_all(stringerr,"@1",character.name)
		
			battleAttackInputAddString(stringerr);
		}
	}
}