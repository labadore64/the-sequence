var arrayer = argument[0];
var check = argument[1];

var sizer = array_length_1d(arrayer);

for(var i = 0; i < sizer; i++){
	if(arrayer[i] == check){
		return i;	
	}
}

return -1;