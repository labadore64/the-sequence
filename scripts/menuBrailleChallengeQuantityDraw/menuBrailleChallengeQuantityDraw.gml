gpu_set_colorwriteenable(true,true,true,true)
		menuParentDrawBox();
gpu_set_colorwriteenable(true,true,true,false)	
		draw_set_halign(fa_center)
	
		draw_sprite_extended_ratio(spr_scroll_right,sprite_counter,x+80,y+32,.5,.5,0,global.textColor,1);
		draw_sprite_extended_ratio(spr_scroll_right,sprite_counter,x-90,y+32,-.5,.5,0,global.textColor,1);
	
		draw_text_transformed_ratio(x,y-80,title,4,4,0);
	
		draw_text_transformed_ratio(x,y-30,unit_string,3,3,0);
		draw_text_transformed_ratio(x,y+20,string(current_value),4,4,0);
		draw_set_halign(fa_left)

		// draw button
		if(global.mouse_active){
			draw_set_color(c_white)
			var xxpo = 395;
			var yypo = 395
			
			draw_rectangle_ratio(xxpo-40-3,yypo-22-3,xxpo+40+3,yypo+22+3,false)	
			draw_set_color(c_black)
			draw_rectangle_ratio(xxpo-40,yypo-22,xxpo+40,yypo+22,false)	
			draw_set_color(c_white)
			draw_set_halign(fa_center)
			draw_set_valign(fa_middle)
			draw_text_transformed_ratio(xxpo,yypo,"Go",2,2,0)
			draw_set_valign(fa_top)
			draw_set_halign(fa_left)
		}

	gpu_set_colorwriteenable(true,true,true,true)
