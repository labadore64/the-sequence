//var testWindow = battleWindowStore("This is a test window. Let's see how it does.",300,200);

//ds_queue_enqueue(windows,testWindow);

//first, do mandatory drop.
//This is the normal drop, and will have a quantity ranging from 1-5 depending
//on the party level.

var level_test = clamp(ceil(BattleHandler.partyLevel/20),1,5);

var quantity = irandom_range(1,level_test);

//select randomly which enemy will have the drop.

var countie = ds_list_size(BattleHandler.opponents);

var drop_default_enemy = -1;

if(countie == 1){
	drop_default_enemy = BattleHandler.opponents[|0];	
} else {
	
	drop_default_enemy = BattleHandler.opponents[|irandom(countie-1)];
}
var drop_default = 1;
if(drop_default_enemy != -1){
	if(!is_undefined(drop_default_enemy)){
		var drop_default = drop_default_enemy.drop_normal;
	}
}

var stringer = "";

var item_obtain_string = "You obtained:\n @0 x@1";

var special_drops = ds_queue_create();

var drop_map = ds_map_create();

if(drop_default > 0){
	battleEndVictoryDSMapAdder(drop_map,drop_default,quantity)

}

//do special drops.

quantity = 1;

with(Battler){
	if(is_enemy){
		//get drops	

		if(trigger_drop_talk){
			battleEndVictoryDSMapAdder(drop_map,drop_talk,quantity)
		}
		
		if(trigger_drop_betray){
			battleEndVictoryDSMapAdder(drop_map,drop_betray,quantity)
		}
		
		if(trigger_drop_bribe){
			battleEndVictoryDSMapAdder(drop_map,drop_bribe,quantity)
		}
		
		if(trigger_drop_megabribe){
			battleEndVictoryDSMapAdder(drop_map,drop_megabribe,quantity)
		}
		
		if(trigger_drop_special){
			battleEndVictoryDSMapAdder(drop_map,drop_special,quantity)
		}
	}
}

//make messages

var size = ds_map_size(drop_map) ;
var key = ds_map_find_first(drop_map);
for (var i = 0; i <= size; i++;)
   {
		my_item = key
		quantity = drop_map[? key];
	
		if(!is_undefined(my_item)){
			if(my_item > 0){
				itemData(my_item);
				stringer = item_obtain_string;
				stringer = string_replace_all(stringer,"@0",name);
				stringer = string_replace_all(stringer,"@1",string(quantity));
				var testWindow = battleWindowStore(stringer,300,100);

				ds_queue_enqueue(windows,testWindow);
	
				giveItem(my_item,quantity);
			}	
		}
	   
      key = ds_map_find_next(drop_map, key);
	  
   }

var my_item = -1;


ds_queue_destroy(special_drops)

ds_map_destroy(drop_map);