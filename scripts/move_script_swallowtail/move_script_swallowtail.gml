//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);

battleAttackInputAddString(stringer);

if(random(1) < .333){
	var stringer = battletext[|1];
	
	damage = damage * 3.5;

	stringer = string_replace_all(stringer,"@0",chara.name);

	battleAttackInputAddString(stringer);
}

ds_list_destroy(battletext);