
	with(BattleHandler){
		var rotation_point = argument[0]; //note: add script to restrict potential endpoints.
		var rotation_time = argument[1]; //deg to move per tep
		var frictionz = argument[2];
		//var rotation_direction = argument[2];

		if(rotation_time == 0){
			rotation_time = 1;
		}	

		var diff = 0;
		var halfpoint = 0;
		var spacerr = 0;

		var adder = 0;
		var dir = 1;

		rotation_point = rotation_point + 36000;
		var test_ori = orientation + 36000;

		if(rotation_point > test_ori){
			dir = 1	
		} else {
			dir = -1;	
		}

		diff = abs(rotation_point - test_ori);
		spacerr = diff/rotation_time;

		var main_size = .6
		var sizer = ceil(rotation_time*main_size)

		var remain_dist = 0;

		var submit_ori = test_ori;

		var another_ori = 0;

		var init_rotate = ds_list_create();
		var slowdown_rotate = ds_list_create();

		var cando = true

		if(diff > 1){
			with(BattleHandler){
				if(ds_list_empty(rotation_amounts)){
				
					//initial turning frames.
					for(var i = 0; i < sizer; i++){
						submit_ori+= spacerr*dir;
						ds_list_add(init_rotate,(submit_ori mod 360));
					}
					
					while(cando){
						//slowdown frames.
						remain_dist = abs(rotation_point-submit_ori);
						var dister = 3600;
						var storer = (spacerr*dir);
						var holl = 0
						while(abs(storer) > .5){
							storer = frictionz*storer
							submit_ori += storer;
							ds_list_add(slowdown_rotate,(submit_ori mod 360));
						}
						
						if(battleValidSpinTarget(submit_ori)){
							cando = false;	
						} else {
							//if its not valid, add another one and redo the process.
							sizer = ds_list_size(init_rotate);
							submit_ori = init_rotate[|sizer-1];
							
							submit_ori-= spacerr*dir;
							ds_list_delete(init_rotate,sizer-1);	
							ds_list_clear(slowdown_rotate);
							
							if(ds_list_size(init_rotate) == 0){
								cando = false;	
							}
						}
		
					}
					sizer = ds_list_size(init_rotate);
					
					for(var i = 0; i < sizer; i++){
						ds_list_add(rotation_amounts,init_rotate[|i]);	
					}
					
					sizer = ds_list_size(slowdown_rotate);
					
					for(var i = 0; i < sizer; i++){
						ds_list_add(rotation_amounts,slowdown_rotate[|i]);	
					}
					
				}
			}
		}
		
		ds_list_destroy(init_rotate);
		ds_list_destroy(slowdown_rotate);
		
	}
