/// @description - get keybind value
/// @function keybind_get(bind_name)
/// @param bind_name

var getval = ds_map_find_value(global.keybind_map,argument[0])

if(!is_undefined(getval)){
	return getval;
}

return -1;