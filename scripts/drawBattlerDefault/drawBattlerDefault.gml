
//draw_sprite_ext(BATTLE_TEST_SPRITE_DELETE_LATER,spr_counter,x,y-jump_y*image_yscale,image_xscale,image_yscale,0,image_blend,image_alpha);
if(sprite_index >= 0){

	if(shader_to_use != -1 && !global.disableShader){
		if(shader_is_compiled(shader_to_use)){
			shader_set(shader_to_use);
			shader_set_uniform_f(shader_uniform_alpha,image_alpha*hide_alpha)
			draw_sprite_ext(sprite_index,spr_counter,x,y-jump_y*image_yscale,image_xscale*sprite_scale,image_yscale*sprite_scale,0,image_blend,image_alpha*hide_alpha);
			shader_reset();
		}
	}
	else {
		draw_sprite_ext(sprite_index,spr_counter,x,y-jump_y*image_yscale,image_xscale*sprite_scale,image_yscale*sprite_scale,0,image_blend,image_alpha*hide_alpha);
	}
}