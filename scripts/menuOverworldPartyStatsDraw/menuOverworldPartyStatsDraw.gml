

draw_set_halign(fa_left)
var statsxoff = -75-50-25-20;
var statsyoff = -40
//draw stat icons
var statspritexoff = 500-100-50-20;
var statspriteyoff = 300-10-40
	
var adjust_portrait = 75;
var adjust_me = 0;
if(noItems){
	statsxoff += 150	
	statspritexoff +=150
	adjust_portrait = -25;
	adjust_me = -50
}
var trans = translation;
	
if(animation_len == 0){
	menuParentSetSurface();
	
	if(surface_update){
		surface_set_target(surf);
	
		draw_clear(c_black)
	
		draw_set_color(c_black)
		draw_rectangle_ratio(0,550,800,600,false)


		var heig = 100;


		draw_rectangle_color_ratio(0,0,800,heig,draw_black,draw_chara,draw_chara,draw_black,false)

		draw_set_color(global.textColor)
		draw_set_font(global.largeFont)
		draw_text_transformed_ratio(20,25,chaa.name,3,3,0)
		draw_set_font(global.textFont)

		//color info

		//menuOverworldPartyStatsDrawColor();

		//stats
		draw_set_color(global.textColor)
		draw_set_halign(fa_center)

		draw_text_transformed_ratio(575+statsxoff+6,190+statsyoff,"Stats",3,3,0)

		draw_set_halign(fa_left);

		draw_sprite_extended_ratio(spr_magpower_icon,0,10+statspritexoff-75,5+statspriteyoff+62,1,1,0,$FF7FFF,1)
		draw_sprite_extended_ratio(spr_physguard_icon,0,125+statspritexoff,30+statspriteyoff+155,1,1,0,$FFFF7F,1)
		draw_sprite_extended_ratio(spr_magguard_icon,0,160+statspritexoff-165,30+statspriteyoff+155,1,1,0,$7FFF7F,1)
		draw_sprite_extended_ratio(spr_physpower_icon,0,170+statspritexoff+15,5+statspriteyoff+62,1,1,0,$7F7FFF,1)	
		draw_sprite_extended_ratio(spr_speguard_icon,0,160+statspritexoff-165,25+statspriteyoff-65,1,1,0,$FF7F7F,1)
		draw_sprite_extended_ratio(spr_spepower_icon,0,125+statspritexoff,25+statspriteyoff-65,1,1,0,$7FFFFF,1)	

		menuOverworldPartyStatsDrawLevel();

		// draw element info
		if(element_sprite != -1){
			var movx = 220-adjust_me;
			var movy = 150;
			draw_text_transformed_ratio(movx-160,movy-6,"Element",2,2,0)
			draw_sprite_extended_ratio(element_sprite,0,movx,movy,2,2,0,c_white,1)
		}
	
		// tablets and items
		if(!noItems){
			var moxx = 600-10;
			var moyy = 150
			var spacey = 30;
			draw_text_transformed_ratio(moxx-10,moyy,"Tablets:",2,2,0)
	
			for(var i =0; i < 3; i++){
				draw_circle_ratio(moxx-10,6+moyy + spacey*(i+1),3,false)
				if(chaa.tablet[i] != -1){
					draw_text_transformed_ratio(moxx,moyy + spacey*(i+1),objdata_moves.name[objdata_tablet.move_id[chaa.tablet[i]]],2,2,0)
				} else {
					draw_text_transformed_ratio(moxx,moyy + spacey*(i+1),"--",2,2,0)	
				}
			}
	
			moyy += 150;
			draw_text_transformed_ratio(moxx-10,moyy,"Items:",2,2,0)
			for(var i =0; i < 5; i++){
				draw_circle_ratio(moxx-10,6+moyy + spacey*(i+1),3,false)
				if(chaa.item[i] != -1){
					draw_text_transformed_ratio(moxx,moyy + spacey*(i+1),objdata_items.name[chaa.item[i]],2,2,0)
				} else {
					draw_text_transformed_ratio(moxx,moyy + spacey*(i+1),"--",2,2,0)	
				}
			}
		}
	
		draw_set_color(info_bg_color)

		draw_rectangle_ratio(0,info_height,800,600,false)
		menuParentDrawInfo();
		surface_reset_target();
		
		surface_update = false;
	}
	draw_clear_alpha(c_black,1)
	gpu_set_colorwriteenable(true,true,true,false);
	draw_set_alpha(1)
	draw_surface_ext(surf,0,0,1,1,0,global.textColor,1);
	//draw portrait
	with(portrait){
		if(!surface_exists(surface)){
			script_execute(draw_script)
		}
	
		draw_surface(surface,global.display_x-(trans+adjust_portrait)*global.scale_factor,global.display_y+167*global.scale_factor)	
	}
	
	// draw character select
	if(global.mouse_active){
		var moveleft = -90
		var moveup = -490
		
		var obj = objdata_character.face_sprite[othername[1]];
		if(obj > -1){
			draw_sprite_extended_ratio(obj, 0,
										450+40+moveleft,500+moveup,
										.65,.65,0,c_white,1)
		}
		
		var obj = objdata_character.face_sprite[othername[0]];
		if(obj > -1){
			draw_sprite_extended_ratio(obj, 0,
										450+30-90+moveleft,500+moveup,
										.65,.65,0,c_white,1)
		}

		draw_sprite_extended_ratio(spr_scroll_right,ScaleManager.spr_counter,
		510-110+moveleft+210,540+moveup,.33,.45,0,global.textColor,1)

		draw_sprite_extended_ratio(spr_scroll_right,ScaleManager.spr_counter,
		470-110+moveleft,540+moveup,-.33,.45,0,global.textColor,1)
		
	}
	
	with(dataDrawStat){
		draw_surface(surface_hex,global.display_x+(450+statsxoff)*global.scale_factor,global.display_y+(250+statsyoff)*global.scale_factor);	
	}
	menuOverworldMenuDrawSwitch("Options","Items")

	draw_letterbox()
}