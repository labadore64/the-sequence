with(CutsceneParser){
	//resets the address.
	file_bin_seek(file, 0);
	reading = 0;
	
	//read the first 4 bytes of the file.
	var startfile;
	startfile[0] = C_START_FILE1;
	startfile[1] = C_START_FILE2;
	startfile[2] = C_START_FILE3;
	startfile[3] = C_START_FILE4;

	if(c_comparebytes(c_readbytes(file,4),startfile)){
		//get the pointer to the table.
		var tablepointer = c_readbytes(file,4);
		
		//set header address		
		header_address = c_getAddressAt(tablepointer);

		if(header_address < file_size){
			//load addresses for headers.f
			c_executionLoadAddresses();
		} else {
			c_executionThrowError("Entry point goes past EOF.");		
		}
		
	} else {
		c_executionThrowError("File is invalid.");	
	}
}