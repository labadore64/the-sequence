var stringer = argument[0];


var mapp = json_decode(stringer);

var xx = real(ds_map_find_value(mapp,"x"));
var yy = real(ds_map_find_value(mapp,"y"));

returner = instance_create_depth(xx,yy,0,AXBookmark);

returner.pitch =  real(ds_map_find_value(mapp,"pitch"));
returner.audio_sound =  real(ds_map_find_value(mapp,"audio_sound"));
returner.index =  real(ds_map_find_value(mapp,"index"));
returner.name = ds_map_find_value(mapp,"name");

ds_map_destroy(mapp);

return returner;