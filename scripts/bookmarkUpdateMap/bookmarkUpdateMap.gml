//

if(mapIsOverworld()){
	if(grid != -1){
		mp_grid_destroy(grid);	
	}

	var lay_id = layer_background_get_id(layer_get_id("Background"));

	var bg_sprite = layer_background_get_sprite(lay_id);

	var width = sprite_get_width(bg_sprite);
	var height = sprite_get_height(bg_sprite);
	
	var sizer = 8
	
	var hcells = width/sizer;
	var vcells = height/sizer;
	
	grid = mp_grid_create(0, 0, hcells, vcells, sizer, sizer);
	
	bookmarkMapSetCollisions(grid,hcells,vcells,sizer)
	
}