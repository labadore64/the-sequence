if(active){
	
	for(var i = 0; i < draw_count; i++){
		with(draw_array[i]){
			draw_sprite_extended_ratio(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha)	
		}
	}

	if(!instance_exists(DialogHandler)){
	if(portrait != -1){
		with(portrait){
			if(!surface_exists(surface)){
				script_execute(draw_script)
			}
	
			draw_surface(surface,global.display_x + 140*global.scale_factor,global.display_y +20*global.scale_factor)	
		}
	}
	}
	with(display_object){
		with(BattleHandler){
			if(is_talking){
				draw_sprite_extended_ratio(spr_battle_talk,spr_counter_talk,0,0,1,1,0,c_white,1);
			}
		}
	}
	draw_set_color(c_black)
	draw_set_alpha(1);
	draw_rectangle_ratio(0,480,800,600,false)
	
	with(display_object){
		if(object_index == obj_dialogue){
			objdialogDraw();	
		}
	}
	
}