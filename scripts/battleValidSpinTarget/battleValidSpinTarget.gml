//checks if the spin target is allowed.

var deg = (argument[0] + 360) mod 360;

if(deg > 117 && deg < 138){
	return true;	
}

if(deg > 205 && deg < 227){
	return true;	
}

if(deg > 296 && deg < 322){
	return true;	
}

if(deg > 23 && deg < 47){
	return true;	
}

return false;