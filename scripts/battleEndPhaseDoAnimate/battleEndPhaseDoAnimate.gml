var index = argument[0];
var chara = argument[1];

if(global.battleAnimate){

	if(index == objdataToIndex(objdata_endphase,"tablet_recover")){
		soundfxPlay(choose(soundBrailleRecover0,soundBrailleRecover1,soundBrailleRecover2))
	}
	if(index > -1){
		if(objdata_endphase.sprite_animation[index] != -1){
			var spritez = objdata_endphase.sprite_animation[index];
	
			with(chara ){
				sprite_attack = spritez
				sprite_attack_complete = false;
				sprite_attack_frames = sprite_get_number(spritez);
				sprite_attack_current_frame = 0;	
			}
		}
		
	
		if(objdata_endphase.animation_length[index] > 0){
			var obj = instance_create(0,0,battleAttackSpriteHandler);
			obj.bg_sprite = objdata_endphase.bg_animation[index];
			obj.fg_sprite = objdata_endphase.fg_animation[index];
			obj.self_sprite = objdata_endphase.sprite_animation[index];
			obj.anim_len = objdata_endphase.animation_length[index];
		}
	}
	
}