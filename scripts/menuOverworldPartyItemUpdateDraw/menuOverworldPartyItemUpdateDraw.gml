var chara = character;
with(menuOverworldPartyItems){
	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items
	
	character = chara;

	for(var i = 0; i < 5; i++){
		var itemz = character.item[i];
		itemData(itemz);
		menuAddOption(name,flavor,menuOverworldPartyItemsSubScript)
	}	
	
	itemData(character.item[menuOverworldPartyItems.menupos]);
}

with(menuOverworldPartyItemsSub){
	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items
	
	namerr = -1;
	//with(menuOverworldParty){
		namerr = chara.item[menuOverworldPartyItems.menupos];	
	//}

	itemData(namerr);
	if(namerr < 0){
		menuAddOption("Give","Give an item.",menuOverworldPartyItemsGiveScript)//menuOverworldStatsSubMovesScript)
	} else {
		menuAddOption("Take","Take the " + name + ".",menuOverworldPartyTakeScript)//menuOverworldStatsSubMovesScript)
	}
	menuAddOption("Switch","Switch the " + name + " with another item.",-1)
	SHORTHAND_CANCEL_OPTION	

}

with(menuControl){
	if(!surface_exists(menuControlSurface)){
		menuControlSurface = surface_create_access(800,600);
	}
	menuControlDrawBG();	
}