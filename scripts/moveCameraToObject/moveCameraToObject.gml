with(CameraOverworld){
	follow_obj = -1;
	set_obj = argument[0];
	var pathspeed = argument[1];
	path = path_add();

	path_add_point(path,x,y,pathspeed);


	path_add_point(path,set_obj.x,set_obj.y,pathspeed);	


	path_set_closed(path, false);

	path_start(path, pathspeed, path_action_stop, true);
}