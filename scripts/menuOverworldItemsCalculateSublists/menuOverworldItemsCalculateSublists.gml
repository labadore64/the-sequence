var mapp = -1;

with(obj_data){
	mapp = inventory;	
}

ds_list_clear(item_id)
ds_list_clear(item_quantity)

ds_list_clear(other_list)
ds_list_clear(equip_list)
ds_list_clear(heal_list)
ds_list_clear(battle_list)
ds_list_clear(field_list)
ds_list_clear(book_list)
ds_list_clear(food_list)
ds_list_clear(craft_list)
ds_list_clear(decor_list)

ds_list_clear(other_list_quant)
ds_list_clear(equip_list_quant)
ds_list_clear(heal_list_quant)
ds_list_clear(battle_list_quant)
ds_list_clear(field_list_quant)
ds_list_clear(book_list_quant)
ds_list_clear(food_list_quant)
ds_list_clear(craft_list_quant)
ds_list_clear(decor_list_quant)

var mapp_size = ds_map_size(mapp);

var tester = 0;

var key = ds_map_find_first(mapp);
for (i = 0; i < mapp_size; i++;)
   {
	
	if(!is_undefined(key)){
		ds_list_add(item_id,real(key));
		ds_list_add(item_quantity,real(ds_map_find_value(mapp,key)));
		
	}
	key = ds_map_find_next(mapp, key);
   }
   
var sizer = ds_list_size(item_id);

var holder = 0;

//menuOverworldItemSortMain();

for(var i = 0; i < sizer; i++){
	holder = ds_list_find_value(item_id,i);
	if(!is_undefined(holder)){
		itemData(holder);
		
		if(item_type == ITEM_TYPE_OTHER){
			ds_list_add(other_list,holder);
			ds_list_add(other_list_quant,item_quantity[|i]);
		} else if (item_type == ITEM_TYPE_EQUIP){
			ds_list_add(equip_list,holder);
			ds_list_add(equip_list_quant,item_quantity[|i]);
		} else if (item_type == ITEM_TYPE_HEAL){
			ds_list_add(heal_list,holder);	
			ds_list_add(heal_list_quant,item_quantity[|i]);
		} else if (item_type == ITEM_TYPE_BATTLE_EFFECT){
			ds_list_add(battle_list,holder);
			ds_list_add(battle_list_quant,item_quantity[|i]);
		} else if (item_type == ITEM_TYPE_OVERWORLD_EFFECT){
			ds_list_add(field_list,holder);
			ds_list_add(field_list_quant,item_quantity[|i]);
		} else if (item_type == ITEM_TYPE_BOOK){
			ds_list_add(book_list,holder);
			ds_list_add(book_list_quant,item_quantity[|i]);	
		} else if (item_type == ITEM_TYPE_FOOD){
			ds_list_add(food_list,holder);
			ds_list_add(food_list_quant,item_quantity[|i]);	
		} else if (item_type == ITEM_TYPE_CRAFT){
			ds_list_add(craft_list,holder);
			ds_list_add(craft_list_quant,item_quantity[|i]);	
		} else if (item_type == ITEM_TYPE_DECOR){
			ds_list_add(decor_list,holder);
			ds_list_add(decor_list_quant,item_quantity[|i]);	
		}
		menuAddOption(name,flavor,argument[0])	
	}
}