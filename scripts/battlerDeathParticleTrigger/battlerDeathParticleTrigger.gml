var death_amount = 20;
    distancex = cos(orientation)*20
    distancey = sin(orientation)*20
    var part_center_x = x+distancex
    var part_center_y = y+distancey *image_yscale  - 128*image_yscale
    var part_size_xscale = image_xscale
    var part_size_yscale = image_yscale
    
    var part_x_len = 64 * part_size_xscale*.5
    var part_y_len = 64 * part_size_yscale*.5
    
    //draw_rectangle(true)

	
	
    part_emitter_region(damage_part_system,damage_part_emitter,part_center_x-part_x_len*.5,part_center_x+part_x_len*.5,part_center_y-part_y_len*.5,part_center_y+part_y_len*.5,ps_shape_ellipse,ps_distr_linear)

part_emitter_burst(damage_part_system, damage_part_emitter, part_death1, death_amount+10);
part_emitter_burst(damage_part_system, damage_part_emitter, part_death2, death_amount);