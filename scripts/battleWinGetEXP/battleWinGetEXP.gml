	var multi = 1;
	
	with(Battler){
		if(ability == "doubleEXP"){
			multi = 2;	
		}
	}
	
	var expp = 0;
	
	var level_multiplier = 1;

	var level = BattleHandler.partyLevel;
	
	level_multiplier = .1+(10-(level*.1))/10;
	
	var multi = 1;
	
	with(Battler){
		if(is_enemy){
			if(trigger_drop_bribe){
				multi = .66;
			}
			
			if(trigger_drop_talk || trigger_drop_betray){
				multi = 1.5;	
			}
			
			if(trigger_drop_special){
				multi = 1.25	
			}
			
			expp += clamp(floor(experience*exp_multiplier*level_multiplier*multi),1,999999);
			multi = 1;
		}
	}
	
	expp = expp*multi;
	
	return expp;