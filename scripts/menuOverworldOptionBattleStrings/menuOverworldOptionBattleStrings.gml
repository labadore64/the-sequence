

menuAddOption(global.langini[0,46],global.langini[1,46],menuOverworldOptionsBattleLeft)
menuAddOption(global.langini[0,47],global.langini[1,47],menuOverworldOptionsBattleLeft)
menuAddOption(global.langini[0,48],global.langini[1,48],menuOverworldOptionsBattleLeft)
menuAddOption(global.langini[0,49],global.langini[1,49],menuOverworldOptionsBattleLeft)
menuAddOption(global.langini[0,50],global.langini[1,50],menuOverworldOptionsBattleLeft)
menuAddOption("Health Sound","Play a sound indicating health when hit.",menuOverworldOptionsBattleLeft)

rotation_strings = ds_list_create();
rotation_values = ds_list_create();

ds_list_add(rotation_strings,LANG_SELECT_OFF,LANG_SELECT_ON)
ds_list_add(rotation_values,false,true)

animation_strings = ds_list_create();
animation_values = ds_list_create();

ds_list_add(animation_strings,LANG_SELECT_OFF,LANG_SELECT_ON)
ds_list_add(animation_values,false,true)

particle_strings = ds_list_create();
particle_values = ds_list_create();

ds_list_add(particle_strings,LANG_SELECT_OFF,LANG_SELECT_ON)
ds_list_add(particle_values,false,true)

crt_strings = ds_list_create();
crt_values = ds_list_create();

ds_list_add(crt_strings,LANG_SELECT_OFF,LANG_SELECT_ON)
ds_list_add(crt_values,false,true)

sfx_strings = ds_list_create();
sfx_values = ds_list_create();

ds_list_add(sfx_strings,LANG_SELECT_OFF,LANG_SELECT_ON)
ds_list_add(sfx_values,false,true)

listsss = ds_list_create()

ds_list_add(listsss,rotation_strings,animation_strings,particle_strings,crt_strings,sfx_strings,sfx_strings)

//whether or not the value should wrap around when adjusted.
wrapz[0] = true;
wrapz[1] = true;
wrapz[2] = true;
wrapz[3] = true;
wrapz[4] = true;
wrapz[5] = true;

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);
menuParentSetSelectWidth(.5)


menu_left = menuOverworldOptionsBattleLeft;
menu_right = menuOverworldOptionsBattleRight;

menu_up = menuOverworldOptionUp
menu_down = menuOverworldOptionDown

menu_draw = menuOverworldOptionBattleDraw;

curposs[0] = ds_list_find_index(rotation_values,global.battleRotate);
if(curposs[0] == -1){
	curposs[0] = 0;	
}
curposs[1] = ds_list_find_index(animation_values,global.battleAnimate);
if(curposs[1] == -1){
	curposs[1] = 0;	
}
curposs[2] = ds_list_find_index(particle_values,global.battlePortrait); 
if(curposs[2] == -1){
	curposs[2] = 0;	
}
curposs[3] = ds_list_find_index(crt_values,global.vcr_on); 
if(curposs[3] == -1){
	curposs[3] = 0;	
}
curposs[4] = ds_list_find_index(sfx_values,global.attackSound); 
if(curposs[4] == -1){
	curposs[4] = 0;	
}
curposs[5] = ds_list_find_index(sfx_values,global.battleHPSound); 
if(curposs[5] == -1){
	curposs[5] = 0;	
}

menuControl.transparent = true;
