var itemcount = argument[0]

var frontsprite = spr_battle_enemy_skullbird_idle_front
var backsprite = spr_battle_enemy_skullbird_idle_back

if(sprite_idle_front[itemcount] == -1){
	sprite_idle_front[itemcount]=frontsprite 
}
if(sprite_idle_back[itemcount] == -1){
	sprite_idle_back[itemcount]=backsprite
}
if(sprite_hit_front[itemcount] == -1){
	sprite_hit_front[itemcount]=frontsprite
}
if(sprite_hit_back[itemcount] == -1){
	sprite_hit_back[itemcount]=backsprite
}
if(sprite_jump0_front[itemcount] == -1){
	sprite_jump0_front[itemcount]=frontsprite
}
if(sprite_jump0_back[itemcount] == -1){
	sprite_jump0_back[itemcount]=backsprite
}
if(sprite_jump1_front[itemcount] == -1){
	sprite_jump1_front[itemcount]=frontsprite
}
if(sprite_jump1_back[itemcount] == -1){
	sprite_jump1_back[itemcount]=backsprite
}
if(sprite_cast0_front[itemcount] == -1){
	sprite_cast0_front[itemcount]=frontsprite
}
if(sprite_cast1_front[itemcount] == -1){
	sprite_cast1_front[itemcount]=frontsprite
}
if(sprite_cast0_back[itemcount] == -1){
	sprite_cast0_back[itemcount]=backsprite
}
if(sprite_cast1_back[itemcount] == -1){
	sprite_cast1_back[itemcount]=backsprite
}