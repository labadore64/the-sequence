ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items

var list_to_use = list_of_lists[|menu_option];
var list_to_use_quan = list_of_lists_quant[|menu_option];

var sizer = ds_list_size(list_to_use);
var movez = -1;

for(var i = 0; i < sizer; i++){
	movez = list_to_use[|i];
	if(!is_undefined(movez)){
		itemData(movez);
		menuAddOption(name,flavor,argument[0])
	}
}

ds_list_clear(item_id);
ds_list_clear(item_quantity);

ds_list_copy(item_id,list_to_use);
ds_list_copy(item_quantity,list_to_use_quan);

var sizer = ds_list_size(item_id);

for(var i = 0; i < sizer; i++){
	item_id_array[i] = item_id[|i];
	item_quantity_array[i] = item_quantity[|i];
}

menu_size = menuGetSize();

menuOverworldItemSetInfokeyStrings();