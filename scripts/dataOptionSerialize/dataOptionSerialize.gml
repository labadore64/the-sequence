var map = ds_map_create();
var filename = argument[0]
ds_map_add(map,"textSpeed",global.textSpeed)
ds_map_add(map,"textColor",global.textColor)
ds_map_add(map,"textFont",global.textFont)
ds_map_add(map,"textVoice",global.voice)

ds_map_add(map,"drawVignette",global.drawVignette)
ds_map_add(map,"menuParticles",global.menuParticles)
ds_map_add(map,"brightness",global.brightness)
ds_map_add(map,"contrast",global.contrast)
ds_map_add(map,"menuAnimation",global.menuAnimation)


ds_map_add(map,"sfxVolume",global.sfxVolume)
ds_map_add(map,"musicVolume",global.musicVolume)
ds_map_add(map,"envVolume",global.envVolume)

ds_map_add(map,"axVolume",global.AXVolume)

ds_map_add(map,"battleRotate",global.battleRotate)
ds_map_add(map,"battlePortrait",global.battlePortrait)
ds_map_add(map,"attackSound",global.attackSound)

ds_map_add(map,"braille_practice",global.braille_practice)
ds_map_add(map,"BrailleSound",global.BrailleSound)
ds_map_add(map,"BrailleTimerSpeed",global.BrailleTimerSpeed)
ds_map_add(map,"brailleRepeat",global.brailleRepeat)
ds_map_add(map,"brailleBackground",global.brailleBackground)
ds_map_add(map,"countdownSound",global.countdownSound)
ds_map_add(map,"overworld_on",global.overworld_on)
ds_map_add(map,"brailleHint",global.brailleHint);

ds_map_add(map,"compatibility_version",string(GAME_COMPATIBILITY_VERSION))

//var returner = json_encode(map);

ds_map_secure_save(map,filename + "option.sav")

ds_map_destroy(map);

//return returner;