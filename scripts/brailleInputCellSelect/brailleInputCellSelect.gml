
if(completed){
	instance_destroy();
} else {
	if(menupos < 3 && menupos >= 0 &&
		(column_pos == 0 || column_pos == 1)){
		cells[column_pos,menupos] = !cells[column_pos,menupos];
	} 

	brailleInputMoveSound();
}