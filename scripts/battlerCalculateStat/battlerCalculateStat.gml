
var char = argument[0]

var character = argument[1];

var statt = argument[2]

var returner = 1;
if(instance_exists(char)){
var list = character.stat_boosts;

var multiplier = 1;
if(ds_exists(list,ds_type_list)){
var sizer = ds_list_size(list);

var obj = -1;

var bless = false;

if(battlerHasFilter(character,"bless")){
	bless = true;	
}

//bless like effects for specific types

for(var i =0; i < sizer; i++){
	obj = list[|i];
	if(!is_undefined(obj)){
		if(statt == obj.stat){
			if(bless){
				if(obj.stat_amount > 1){
					multiplier = multiplier * obj.stat_amount;
				}
			} else {
				multiplier = multiplier * obj.stat_amount;
			}
		}
	}
}

returner = char.base*multiplier;
}
}
return returner;
