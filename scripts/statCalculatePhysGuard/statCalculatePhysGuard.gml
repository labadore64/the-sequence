/// @description - returns adjusted PHYSGUARD stat

var minvalue = 10;

var multiplier = getMultiplierFromLevel(objdata_character.phy_guard_grow[character],level);

var returner = ceil((minvalue + multiplier * (phy_guard_base+(boost_phy_guard)*BATTLE_STAT_MULTIPLIER_ITEM)));

returner += statCalculateBrailleBoost(multiplier,argument[0]);

return returner;