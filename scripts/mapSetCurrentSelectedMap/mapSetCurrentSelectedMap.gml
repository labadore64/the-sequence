var complete = false;
var obj = -1;
last_selected_text_name = selected_text_name;
var xx = x;
var yy = y
last_selected = selected_pos;
with(mapPoint){
	if(obj == -1){
		complete = point_in_rectangle(xx,yy,check_x1,check_y1,check_x2,check_y2);
		if(complete){
			obj = id;	
		}
	}
}

if(obj != -1){
	selected_pos = ds_list_find_index(map_list,obj)
	selected_text_name = map_list[|selected_pos].name;
	if(last_selected != selected_pos){
		if(selected_text_name != last_selected_text_name){
			tts_say(selected_text_name);	
		}
	}
} else {
	selected_pos = -1;	
	selected_text_name = "";
}