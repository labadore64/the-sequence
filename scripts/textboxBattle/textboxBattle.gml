

var dialogFolder = "resources\\stats\\monster_talk\\";
var dialogFile = "monster_talk"+string(argument[0].character)+".ini";
var filename = dialogFolder+dialogFile;

var talking_character = -1;

with(Battler){
	if(!is_enemy){
		if(battlerHasFilter(id,"Listen")){
			talking_character = id;
		}
	}
}

if(id != -1){
	if(!instance_exists(TextBox)){
	var teee = instance_create_depth(0,30,0,TextBox);

		var get_id = howManyTimesTalkedMonster(argument[0].character);

		teee.filename_id = filename
		teee.text_section = "talk";
		teee.text_id = "character" + string(get_id) + "_" + string(argument[0].talk_counter) + "_"
		
		teee.character = argument[0]
		teee.is_monster = true;
		teee.x_align = 125
		teee.delay_display = 10
		//set talk counter to how many values there are.
		var getMax = textboxBattleGetMaxTalk(filename,"character" + string(get_id)  + "_");
		argument[0].talk_max = getMax;
		argument[0].talk_counter++;
		if(argument[0].talk_max <= argument[0].talk_counter){
			teee.end_script = textboxBattleFinish;
		}
	}
}