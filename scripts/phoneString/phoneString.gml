var stringer = string_lettersdigits(LANG_PHONE_DEFAULT_AREA_CODE +argument[0]);

var returner = string_lower(LANG_PHONE_STYLE)

// converts the string into a phone string
// LANG_PHONE_DEFAULT_AREA_CODE
// LANG_PHONE_STYLE
var char = "";
while(string_length(stringer) > 0){
	char = string_char_at(stringer,1);
	
	stringer = string_delete(stringer,1,1);
	
	returner = string_replace(returner,"n",char);
}

return returner;