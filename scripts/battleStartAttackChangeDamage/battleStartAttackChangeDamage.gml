var chara = argument[0];
var targe = argument[1];
var dmg = argument[2];

var returner = dmg;

if(battlerHasAbility(chara,"binary")){
	//round up to nearest binary value up to 512
	
	if(returner <= 2){
		returner = 2;	
	} else if (returner <= 4){
		returner = 4;	
	} else if (returner <= 8){
		returner = 8;	
	} else if (returner <= 16){
		returner = 16;	
	} else if (returner <= 32){
		returner = 32;	
	} else if (returner <= 64){
		returner = 64;	
	} else if (returner <= 128){
		returner = 128;	
	} else if (returner <= 256){
		returner = 256;	
	} else {
		returner = 512;	
	}

}

return returner;