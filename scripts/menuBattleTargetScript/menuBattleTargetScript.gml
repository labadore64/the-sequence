if(!is_bribe){

	var targ = noone;
	var briber = 0;
	with(menuBattleTarget){
		targ = character;	
		briber = bribe;
	}

	if(!target_ally){
		if(character.ability == "blind" || battlerHasFilter(character,"blind")){
			var lister = -1;
	
			with(BattleHandler){
				lister = enemy_list;	
			}
	
			var sizer = ds_list_size(lister);
	
			targ = lister[|irandom(sizer-1)]
	
	
		}
	}

	var selected_move =-1

	with(menuBattleMoves){
		var obj = menuBattleTargetDoTarget(character,targ);
		obj.bribe = briber;
	}

	with(menuBattleItems){
		menuBattleTargetDoTarget(character,targ)
	}
	
	with(menuBattleTablet){
		menuBattleTargetDoTarget(character,targ)
	}



	menuBattleTargetUpdateCharacter();

	soundfxPlay(soundBattleSelectTarget)
} else {
	soundfxPlay(sound_select)
	instance_create(0,0,menuBattleBribe)
}

//menuCreateInstance(menuBattleFight)