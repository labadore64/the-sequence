
	updateBattleMainHUD();
	hudPopulateTablet("tablet",character.tablet[menuBattleTablet.menupos])
	hudPopulateMoveBattle("move",move_id,0);

	var fixstring = "";
	var len = string_length(add_string);
	for(var i = 0; i < len; i++){
		fixstring += "\"" + string_char_at(add_string,i+1) + "\" ";	
	}

	menuBattleTabletConfirmUpdatePreview();

	hudControllerAddData("chain." + "add",fixstring);
	hudControllerAddData("chain." + "boost",string(floor(tablet_boost*100)));

	hudControllerAddData("chain." + "direction",menu_name_array[menupos]);

	// newchain


			var full_string = "";
			var charr = "";
	
			for(var i = 0; i < 5; i++){
				charr = "\"" + string_char_at(preview_string,i+1)+ "\" ";
				full_string += charr;
				hudControllerAddData("newchain."+"char"+string(i),charr);
			}
			hudControllerAddData("newchain."+"string",full_string);
		
			var result = wordTestInString(preview_string);
		
			if(result != -1){	
				hudControllerAddData("newchain."+"word",detected_word);
			}
	hudControllerAddData("tablet." + "mp_cost",string(floor(mp_total_cost*100)));
