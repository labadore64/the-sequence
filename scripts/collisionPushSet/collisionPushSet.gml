//first, get all push areas that are in range of this collision.

var coll = id;

var list = ds_list_create();

with(pushArea){
	var width = sprite_width*.5
	var height = sprite_height*.5
	
	var x1 = x-width;
	var y1 = y-height;
	
	var x2 = x+width;
	var y2 = y+height;
	
	if(x > x1 && x < x2 &&
		y > y1 && y < y2){
		ds_list_add(list,id);		
	}
}

if(!ds_list_empty(list)){
	//can move, as a collision area was detected.
	
	//determine the direction of the push
	var cando = false;
	//if(push_caused_by_player){
	if(!is_pushing){
		//if pushing animation isn't already triggered
		
		var xxx = Player.x;
		var yyy = Player.y + Player.collision_y_adjust

		var cando = false;
	
		var xxx_temp = 0;
		var yyy_temp = 0;
	
		if(xxleft < xxx && xxright > xxx){
			//up or down collide.
			if(yyy > y){
				xxx_temp = x;
				yyy_temp = y-push_dist;
				cando = true;
			} else {
				xxx_temp = x;
				yyy_temp = y+push_dist;
				cando = true;
			}
		
		} else if (ytoptop < yyy && ybotbottom > yyy){
			//left or right collide	
			if(xxx > x){
				xxx_temp = x-push_dist;
				yyy_temp = y;
				cando = true;
			} else {
				xxx_temp = x+push_dist;
				yyy_temp = y;
				cando = true;
			}
		}
	
		//add corrections later.
		var sizer = ds_list_size(list);
	
		//get predicted borders of collision.
	
		var sspritewidth = draw_scalex*sprite_get_width(sprite_index)*.5
		var sspriteheight = draw_scaley*sprite_get_height(sprite_index)*.5
	
		var xx1 = xxx_temp-sspritewidth;
		var yy1 = yyy_temp-sspriteheight;
	
		var xx2 = xxx_temp+sspritewidth;
		var yy2 = yyy_temp+sspriteheight;
	
	
		//differences in movement, so that the edges of the sprite are accomidated properly.
		var xdifff = 0;
		var ydifff = 0;
	 
	
		var obj = -1;
		for(var i = 0; i < sizer; i++){
			obj = list[|i];
			if(!is_undefined(obj)){
				if(instance_exists(obj)){
					//	
					with(obj){
					
						if(x_left > xx1){
							xdifff = xx1-x_left;
							xxx_temp-=xdifff;
							push_direction = 180
						}
						if(x_right < xx2){
							xdifff = x_right - xx2;
							xxx_temp+=xdifff;
							push_direction = 0;
						}
					
						if(y_up > yy1){
							ydifff = yy1-y_up;
							yyy_temp-=ydifff;
							push_direction = 90
						}
					
						if(y_down < yy2){
							ydifff = y_down - yy2;
							yyy_temp+=ydifff;
							push_direction = 270
						}
					}
				}
			}
		}
	
		push_dest_x = xxx_temp;
		push_dest_y = yyy_temp;
	
	}
	
	if(push_dest_x == x &&
		push_dest_y == y){
		cando = false;		
	}
	
	//push_counter = 0;
	if(cando){
		push_counter = 0;
		is_pushing = true;
		push_diff_x = (push_dest_x-x)/push_time
		push_diff_y = (push_dest_y-y)/push_time
		if(push_caused_by_player){
			player_pushing = true;
			with(Player){
				push_lock = true;	
			}
		}
	}
}


ds_list_destroy(list);