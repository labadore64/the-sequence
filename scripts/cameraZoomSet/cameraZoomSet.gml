with(Player){
	obj_data.screen_scale = argument[0];

	zoom_x = argument[0]+.1;
	zoom_y = argument[0]+.1;

	var xxx = zoom_x;
	var yyy = zoom_y;

	with(overworld_scalable){
		zoom_x = xxx;
		zoom_y = yyy;
	}

	zoom_factor_x = (1/zoom_x);
	zoom_factor_y = (1/zoom_y);
	
	if(zoom_x_previous != zoom_x){
		playerUpdateSize();
	}
	
	image_xscale = draw_scale*zoom_factor_x;
	
	if(draw_flip){
		image_xscale = image_xscale * -1;	
	}
	
	update_surface = true;	
}