
	move_rate+=move_delta;

	var move_val = move_rate_max;
	
	if(is_running){
		spray_lock = false
		move_val = move_rate_run_max;	
	}
	if(move_rate > move_val){
		move_rate = move_val;	
	}
	
	x+= move_x
	y+= move_y
	
	with(obj_data){
		current_x = Player.x;
		current_y = Player.y;
	}
	
	playerFootstepUpdate();
	
	/*
	var addx = cos(degtorad(orientation))*interact_dist;
	var addy = sin(degtorad(orientation))*interact_dist;
	interact_x = x+addx;
	interact_y = y+addy+collision_y_adjust+16;
	*/
	with(AudioEmitter){
		audioEmitCheckCollision();
	}
	
	var player_dist = 0;
	with(AXBookmark){
		player_dist = distance_to_point(Player.x,Player.y);
		if(player_dist < read_dist){
			if(!triggered){
				triggered = true;
				tts_say(name);
			}
		} else {
			triggered = false;	
		}
	}
	

		playerUpdateCollisionDistances()

		//playerUpdateSoundNoBlind()

	obj_data.pos_x = x;
	obj_data.pos_y = y
	
	audio_listener_orientation(cos(degtorad(orientation)), -1, sin(degtorad(orientation)), 0, 0, -1);
	audio_listener_position(x, y+collision_y_adjust, 0);
	
	playerBirdUpdate(Player.x,Player.y+Player.collision_y_adjust);