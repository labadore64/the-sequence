
	if(ds_list_size(braille_select) < max_tablet_count){
		if(move_cando[|menupos]){
			var pos = ds_list_find_index(braille_select,braille_display[menupos])
	
			if(pos == -1){
				ds_list_add(braille_select,braille_display[menupos]);
			}
	
			soundfxPlay(sound_move);
	
			if(!ds_list_empty(braille_select)){
				main_tablet = braille_select[|0]	
				for(var i = 0; i < 3; i++){
					if(main_tablet == braille_display[i]){
						main_index = i;
						break;
					}
				}
			} else {
				main_tablet = noone	
				main_index = -1;
			}
	
			for(var i = 0; i < 3; i++){
				numbering[i] =  ds_list_find_index(braille_select,braille_display[i])
			}
	
			var len = ds_list_size(braille_select);
	
			var stringer = "";
	
			for(var i = 0; i < len; i++){
				stringer+=string_upper(braille_select[|i].cell_character)
			}
	
			selected_string = stringer;
		
			menuParentUpdate();
		
			tts_clear();
		
			tts_say(string(numbering[menupos]+1) + " " + 
						"\"" + string(braille_display[menupos].cell_character) + "\"" 
						+ " Selected ")
		
			if(ds_list_size(braille_select) == 1){
				menuParentTTSLabelRead();
			}
		} else {
			soundfxPlay(soundMenuInvalid);	
		}
	total_mpcost = menuBattleTabletCalculateMPCostMultiplier(string_length(selected_string));
	menuBattleTabletUpdateHUD();
	}

