var list = argument[0]

var val = list[|line];

var sprite_id = -1;

if(val != "none"){
	sprite_id = asset_get_index(val);
}

with(DialogHandler){
	background = sprite_id;
	if(sprite_id != -1){
		background_len = sprite_get_number(sprite_id)	
	}
	bg_index = 0;
}