spray_release_type = SPRAY_NONE
spray_lock = false; 
binocular_item = false;
overworld_item_active = false;

prop_place_id = -1;
prop_placing = false;
prop_place_item = -1;

with(CameraOverworld){
	prop_place_surface = -1;
	prop_place = false;
	prop_place_obj = -1;	
	binoculars = false;

}