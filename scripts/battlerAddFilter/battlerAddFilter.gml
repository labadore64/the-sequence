var chara = argument[0];
var filtername = argument[1];

if(!battlerHasFilter(chara,filtername) && !battlerHasNullFilter(chara)){

	if(battlerFilterCanDo(chara,filtername)){
		with(chara){
			var filter = instance_create(0,0,battleStatusEffect);
			filter.status_id = objdataToIndex(objdata_status,filtername);
			filter.name = filtername;
			filter.target = chara;
			filter.script_destroy = objdata_status.script_destroy[filter.status_id]
			filter.script_end_turn = objdata_status.script_end_turn[filter.status_id]
			filter_add_later = filter;
			//ds_list_add(status_effects,filter);

			return 1;
		}
	}
}

if(battlerHasFilter(chara,filtername)){
	return 3;	
}

return 2;