
	dataBasicLoadDataSave();

	var filename = working_directory + "save\\";

	dataMainSerialize(filename);
	dataOptionSerialize(filename);

	var sizer = ds_list_size(obj_data.characters);

	for(var i = 0; i < sizer; i++){
		dataCharacterSerialize(obj_data.characters[|i],filename);
	}

	dataItemSerialize(filename);

	dataPushObjectSerialize(filename);

	ini_open("config.ini")

	ini_write_string("access","tts",boolToString(global.tts));
	ini_write_string("access","corner_sound",boolToString(global.AXCornerSound));
	ini_write_string("access","vision",boolToString(global.AXVisionEnabled));
	ini_write_real("access","vision_distance",global.AXSightDistance);
	ini_write_real("access","proximity_distance",global.AXCollisionDistance);

	ini_write_real("config","music_vol",global.musicVolume)
	ini_write_real("config","sfx_vol",global.sfxVolume);
	ini_write_real("config","env_vol",global.envVolume);
	ini_write_real("config","access_vol",global.AXVolume);
	ini_write_string("config","fullscreen",boolToString(global.fullscreen));
	ini_write_string("config","disable_shader",boolToString(global.disableShader));

	ini_close()
	
	with(AXManager){
		keyboard_keybind_save("keyboard.json");
		gamepad_keybind_save("gamepad.json")
	}