var posx = argument[0]*global.scale_factor;
var posy = argument[1]*global.scale_factor;
var width = argument[2]*.5*global.scale_factor;
var length = argument[3]*.5*global.scale_factor;


draw_rectangle(global.display_x 
				+posx-width,
				global.display_y 
				+posy-length,
				global.display_x
				+posx+width,
				global.display_y
				+posy+length,
				false);