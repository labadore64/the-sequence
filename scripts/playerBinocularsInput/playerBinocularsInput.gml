	//these scripts just change orientation
	//objGenericDoGamepad()
	
	var bino_left = playerBinocularLeft;
	var bino_right = playerBinocularRight
	var bino_up = playerBinocularUp
	var bino_down = playerBinocularDown
	
	var bino_cancel = playerBinocularCancel
	var bino_select = playerBinocularSelect;
	
	var did = false;
	binocular_multiplier = 1;
	if(AXManager.axis_amount > .8){
		var dirz = (180- AXManager.axis_angle+360) mod 360;
		var cutoff_change = 60;
		
		if(abs(orientation - dirz) < cutoff_change){
			orientation=dirz
		} else {
			binocular_rotate = 10;
			playerBinocularUpdatePosition(dirz)
		}
	} else {
	
		if(bino_left != -1){
			if(global.key_state_array[KEYBOARD_KEY_LEFT] == KEY_STATE_HOLD ||
				global.gamepad_state_array[KEYBOARD_KEY_LEFT] == KEY_STATE_HOLD){
				script_execute(bino_left);
				//keypress_this_frame = true;
				did = true;
			}
	
		}  
	
		if(!did){
		if(bino_right != -1){
			if(global.key_state_array[KEYBOARD_KEY_RIGHT] == KEY_STATE_HOLD ||
				global.gamepad_state_array[KEYBOARD_KEY_RIGHT] == KEY_STATE_HOLD){
			script_execute(bino_right);
			//keypress_this_frame = true;
			did = true;
			}
		} 
		}
	
		if(!did){
		if(bino_up != -1){
			if(global.key_state_array[KEYBOARD_KEY_UP] == KEY_STATE_HOLD ||
				global.gamepad_state_array[KEYBOARD_KEY_UP] == KEY_STATE_HOLD){
			script_execute(bino_up);
			//keypress_this_frame = true;
			did = true;
			}

		} 
		}
	
		if(!did){
		if(bino_down != -1){
			if(global.key_state_array[KEYBOARD_KEY_DOWN] == KEY_STATE_HOLD ||
				global.gamepad_state_array[KEYBOARD_KEY_DOWN] == KEY_STATE_HOLD){
			script_execute(bino_down);	
			//keypress_this_frame = true;
			did = true;
			}

		}  
		}
	
		if(bino_cancel != -1){
			if(global.key_state_array[KEYBOARD_KEY_CANCEL] == KEY_STATE_PRESS ||
				global.gamepad_state_array[KEYBOARD_KEY_CANCEL] == KEY_STATE_PRESS ||
				global.key_state_array[KEYBOARD_KEY_CONTROL] == KEY_STATE_PRESS ||
				global.gamepad_state_array[KEYBOARD_KEY_CONTROL] == KEY_STATE_PRESS){


				script_execute(bino_cancel);
			}
		} 
	
		if(bino_select != -1){
			if(global.key_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
				global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

				script_execute(bino_select);
			}
		} 
	}
	camera_x = cos(degtorad(orientation))*binoculars_distance+x;
	camera_y = sin(degtorad(orientation))*binoculars_distance+y;
	camera_zoom_x = zoom_x-binoculars_zoom;
	camera_zoom_y = zoom_y-binoculars_zoom;