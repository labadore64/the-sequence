with(battleTextDisplay){
	if(ds_exists(display_list,ds_type_list)){
		ds_list_clear(display_list);
		ds_list_clear(text_list);
		display_list_size = 0;
		draw_icon_counter = 0;
		draw_next_icon = false;
		no_text = true;
		show_up_to = 0;
	}
}
