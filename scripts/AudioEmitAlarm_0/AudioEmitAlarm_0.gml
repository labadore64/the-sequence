
var pause = false
if(access_only && !global.AXNotify){
    pause = true
} else if (not_access && global.AXNotify){
    pause = true
}
 else {
    //stupid dumb redudancy check
    
	var myid = bookmark_id;
    var setfordestruction = false;
    var me = id;
    
	if(myid != -1){
	    with(AudioEmitter){
	        if(bookmark_id == myid){
	            if(me != id){ //is not self
					setfordestruction = true; 
	            }
	        }
	    }
	}
    var setfordestruction = false;
    if(setfordestruction){
        instance_destroy()
    } else {
        if(audio_sound > 0){
            play_sound = true;
            audio_emitter_falloff(emit, falloff_min, falloff_max , falloff_factor);
            audio_emitter_position(emit, x, y, 0);
			
            audio_stop_sound(emit_sound)
            if(delay != 0){
				newalarm[3] = delay
			} else {
				emit_sound = audio_play_sound_on(emit, audio_sound, loop, 1);
				audio_sound_pitch(emit_sound,pitch_base)
				
				if(pause){
					audio_pause_sound(emit_sound)
				}
			}
        }
    }
 }

