// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from


	if(obj > -1 && obj < objdata_items.data_size){
		hudControllerAddData(entryname + "itemtype",string(objdata_items.item_type_string[obj]));
		
		// name and character
		hudControllerAddData(entryname + "name",objdata_items.name[obj]);
		hudControllerAddData(entryname + "desc",objdata_items.flavor[obj]);
		
		hudControllerAddData(entryname + "phy_power",string(objdata_items.stat_attack[obj]));
		hudControllerAddData(entryname + "phy_guard",string(objdata_items.stat_defense[obj]));
		hudControllerAddData(entryname + "mag_power",string(objdata_items.stat_magic[obj]));
		hudControllerAddData(entryname + "mag_guard",string(objdata_items.stat_resistance[obj]));
		hudControllerAddData(entryname + "spe_power",string(objdata_items.stat_agility[obj]));
		hudControllerAddData(entryname + "spe_guard",string(objdata_items.stat_mpr[obj]));
		
	} else {
		hudControllerAddData(entryname + "itemtype",string("none"));
		
		// name and character
		hudControllerAddData(entryname + "name",global.langini[4,7]);
		hudControllerAddData(entryname + "desc","");
		
		hudControllerAddData(entryname + "phy_power",string(0));
		hudControllerAddData(entryname + "phy_guard",string(0));
		hudControllerAddData(entryname + "mag_power",string(0));
		hudControllerAddData(entryname + "mag_guard",string(0));
		hudControllerAddData(entryname + "spe_power",string(0));
		hudControllerAddData(entryname + "spe_guard",string(0));
	}
