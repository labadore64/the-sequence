/// @function keyboard_bind_init()
/// @description Initializes the game's default keyboard binding state.

// You can change these to match the controls in your game.
// This is just a suggestion.
keybind_restrictions_init();

keyboard_bind_set("left",vk_left);
keyboard_bind_set("right",vk_right);
keyboard_bind_set("up",vk_up);
keyboard_bind_set("down",vk_down);

keyboard_bind_set("lefttab",ord("A"));
keyboard_bind_set("righttab",ord("S"));

keyboard_bind_set("control",vk_control);
keyboard_bind_set("cancel",ord("X"));

keyboard_bind_set("select",ord("Z"));
keyboard_bind_set("select2",vk_enter);

//overworld keybinds

keyboard_bind_set("open_menu",vk_enter);
keyboard_bind_set("run",ord("X"));
keyboard_bind_set("scopeFunc1",vk_up); // what happens when you usually press up on the scope
keyboard_bind_set("scopeFunc2",vk_down); // what happens when you usually press down on the scope
keyboard_bind_set("accessMenu", ord("Q"));
keyboard_bind_set("bookmark", vk_control);

keyboard_bind_set("sight", vk_f3);
keyboard_bind_set("coordinates", vk_f4);

// additional keys for accessibility
keyboard_bind_set("access1",ord("1"));
keyboard_bind_set("access2",ord("2"));
keyboard_bind_set("access3",ord("3"));
keyboard_bind_set("access4",ord("4"));
keyboard_bind_set("access5",ord("5"));
keyboard_bind_set("access6",ord("6"));
keyboard_bind_set("access7",ord("7"));
keyboard_bind_set("access8",ord("8"));
keyboard_bind_set("access9",ord("9"));
keyboard_bind_set("access0",ord("0"));

keyboard_bind_set("exit", vk_escape);
keyboard_bind_set("help", vk_f1);
keyboard_bind_set("hud", vk_f2);
keyboard_bind_set("screenshot", vk_f5);

for(var i = 0; i < global.keybind_size; i++){
	global.key_state_array[i] = KEY_STATE_NONE;
}