//@param section
//@param key
//@param default_as_string

//NOTE ONLY USE THIS IF AN INI FILE IS ALREADY OPEN
//It will only return true if the value read is (case insensitive) "true"
var val = ini_read_string(argument[0],argument[1],argument[2])

if(string_lower(val) == "true"){
	return true;	
}

return false;