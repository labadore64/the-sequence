gml_pragma("forceinline");
menuAddOption("","",menuOverworldScriptToss)

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

menuParentSetTitle(global.langini[6,2]);
menuParentSetSubtitle("");

menuParentSetGradientHeight(0)
menuParentSetOptionYOffset(10);
menuParentSetOptionSpacing(40)
draw_height = 160
draw_width = 400

x = 400
y = 300

var quan = 0;
quantity = 0;

with(menuOverworldItems){
	quan = item_quantity [| menupos]
}

quantity = quan;

if(is_undefined(quantity)){
	quantity = 0;	
}

menu_size = quantity+1;
menupos = 1

menu_draw = menuOverworldItemTossDraw;

menu_up = menuTossMoveRight;//menuParentMoveUp;
menu_down = menuTossMoveLeft;//menuParentMoveDown;
menu_left = menuTossMoveUp;//menuParentMoveLeft;
menu_right = menuTossMoveDown;//menuParentMoveRight;
menu_select = menuOverworldScriptToss;

menuParentUpdateBoxDimension();

if(menu_size == 0){
	instance_destroy();	
}
