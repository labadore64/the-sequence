draw_clear(c_black)



draw_set_font(global.textFont);
draw_set_color(global.textColor);

var x_pos = 400;
var y_pos = 120;
var v_size = 215;
var h_size = 750;

var i = 5;

draw_rectangle_center(x_pos,y_pos,h_size+i,v_size+i);

draw_set_color(c_black)

draw_rectangle_center(x_pos,y_pos,h_size,v_size);

draw_set_color(global.textColor);


x_pos = 400;
y_pos = 525;
v_size = 110;
h_size = 750;

draw_rectangle_center(x_pos,y_pos,h_size+i,v_size+i);

draw_set_color(c_black)

draw_rectangle_center(x_pos,y_pos,h_size,v_size);

draw_set_color(global.textColor);


var lside = 35;
var vspace = 40;
var vspacer = 45;

var colon = ": "

draw_text_transformed_ratio(lside,vspace,"Time" + colon + string(time_hour) + ":" + time_min_draw,normal_scale,normal_scale,0)
draw_text_transformed_ratio(lside,vspace+vspacer*3,location_name,normal_scale,normal_scale,0)
draw_text_transformed_ratio(lside,vspace+vspacer,"Tablets" + colon + string(tablet_count),normal_scale,normal_scale,0)
draw_text_transformed_ratio(lside,vspace+vspacer*2,"Recruited" + colon + string(recruited),normal_scale,normal_scale,0)

draw_text_transformed_ratio(405,vspace,"Current Party:",normal_scale,normal_scale,0)
var obj = -1;

draw_set_color(global.textColor);

for(var i = 0; i < 3; i++){
	obj = party[|i];
	if(!is_undefined(obj)){
		
		draw_text_transformed_ratio(405,vspace+vspacer*(i+1),obj.name,normal_scale,normal_scale,0)
	}
}

var spacer = 150;
var xx = 400;
var yy = 485;

draw_set_halign(fa_center)

if(menupos == 0){
	draw_text_transformed_ratio(xx-spacer,yy,menu_name_array[0],select_scale,select_scale,0)
} else {
	draw_text_transformed_ratio(xx-spacer,yy,menu_name_array[0],normal_scale,normal_scale,0)
}

if(menupos == 2){	
	draw_text_transformed_ratio(xx+spacer,yy,menu_name_array[2],select_scale,select_scale,0)
} else {
	draw_text_transformed_ratio(xx+spacer,yy,menu_name_array[2],normal_scale,normal_scale,0)
}
if(menupos == 1){
	draw_text_transformed_ratio(xx-spacer,yy+50,menu_name_array[1],select_scale,select_scale,0)
} else {
	draw_text_transformed_ratio(xx-spacer,yy+50,menu_name_array[1],normal_scale,normal_scale,0)
}

if(menupos == 3){	
	draw_text_transformed_ratio(xx+spacer,yy+50,menu_name_array[3],select_scale,select_scale,0)
} else {
	draw_text_transformed_ratio(xx+spacer,yy+50,menu_name_array[3],normal_scale,normal_scale,0)
}

draw_set_halign(fa_left)

draw_letterbox();
