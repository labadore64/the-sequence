/// @description - returns the argument
/// @param name

var returner = ds_map_find_value(arguments,argument[0]);

if(is_undefined(returner)){
	returner = "-1";	
}

return returner;