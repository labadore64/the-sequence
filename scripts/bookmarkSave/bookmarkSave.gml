var idzz = room;

var filename = bookmarkFilename(idzz);

var list = ds_list_create();

var list_check = -1;
with(AXBookmarkHandler){
	list_check = bookmarks;	
}

var sizer = 0;

if(list_check != -1){
	sizer = ds_list_size(list_check);
	for(var i = 0; i < sizer; i++){
		ds_list_add(list,bookmarkToString(list_check[|i]));	
	}
	
	ds_list_clear(list_check);
}

if(ds_list_size(list) > 0){
	var file = file_text_open_write(filename);
	sizer = ds_list_size(list);

	for(var i = 0; i < sizer; i++){
		file_text_write_string(file,list[|i]);
		file_text_writeln(file);
	}

	file_text_close(file);
} else {

	if(file_exists(filename)){
		file_delete(filename);	
	}
}

ds_list_destroy(list);

