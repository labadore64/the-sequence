
var _radius = argument[1];
var _character = argument[0];


stat_phy_power = argument[2]
stat_phy_guard = argument[3]
stat_mag_power = argument[4]
stat_mag_guard = argument[5]
stat_spe_power = argument[6]
stat_spe_guard = argument[7]


var angle = 0;

var _multi_see = .9

for(var i = 0; i < 6; i++){
	
	
	stat_x[i] = cos(degtorad(angle))*_radius;
	stat_y[i] = sin(degtorad(angle))*_radius;

	stat_icon_x[i] = cos(degtorad(angle))*_radius*1.25;
	stat_icon_y[i] = sin(degtorad(angle))*_radius*1.25;

	stat_display_x[i] = 0;
	stat_display_y[i] = 0;
	
	stat_display_val_x[i] = 0
	stat_display_val_y[i] = 0
	
	stat_boost_val[i] = 1

	angle += 60;
}


//_character

if(_character >= 0){

	stat_val[0] = stat_phy_power// + draw_hex_get_item_stats(_character,BATTLE_STAT_ATTACK);
	stat_val[1] = stat_phy_guard// + draw_hex_get_item_stats(_character,BATTLE_STAT_DEFENSE);
	stat_val[3] = stat_mag_power// + draw_hex_get_item_stats(_character,BATTLE_STAT_MAGIC);
	stat_val[2] = stat_mag_guard//  + draw_hex_get_item_stats(_character,BATTLE_STAT_RESISTANCE);
	stat_val[4] = stat_spe_guard// + draw_hex_get_item_stats(_character,BATTLE_STAT_MP_RECOVER);
	stat_val[5] = stat_spe_power// + draw_hex_get_item_stats(_character,BATTLE_STAT_AGILITY);
	
	var isbattle = false;
	
	with(BattleHandler){
		isbattle = true;
	}
	
	stat_boost_val[0] = boost_phy_power/stat_val[0]
	stat_boost_val[1] = boost_phy_guard/stat_val[1]
	stat_boost_val[3] = boost_mag_power/stat_val[3]
	stat_boost_val[2] = boost_mag_guard/stat_val[2]
	stat_boost_val[5] = boost_spe_power/stat_val[5]
	stat_boost_val[4] = boost_spe_guard/stat_val[4]
	
	/*
	if(isbattle){

		
		if(phytype == BATTLE_STAT_ATTACK){
			stat_boost_val[0] = phyboss;
			stat_boost_val[1] = 1;
		} else {
			stat_boost_val[0] = 1;
			stat_boost_val[1] = phyboss;
		}
		
		if(magtype == BATTLE_STAT_RESISTANCE){
			stat_boost_val[2] = magboss;
			stat_boost_val[3] = 1;
		} else {
			stat_boost_val[2] = 1;
			stat_boost_val[3] = magboss;
		}
		
		if(spetype == BATTLE_STAT_MP_RECOVER){
			stat_boost_val[4] = speboss;
			stat_boost_val[5] = 1;
		} else {
			stat_boost_val[4] = 1;
			stat_boost_val[5] = speboss;
		}

	}
	*/
	
	var maxval = maxi_val;
	var sizee = 0;
	angle = 0;
	
	var multiplier = 1;
	for(var i = 0; i < 6; i++){
		if(i != 4){
			sizee = ((stat_val[i])/(maxval));
		} else {
			sizee = ((stat_val[i])/(15));
		}
		
		sizee = clamp(sizee,0,.9)+.1
		char_stat_x[i] = cos(degtorad(angle))*sizee*_radius*_multi_see*multiplier//*((_character.level)*.01);
		char_stat_y[i] = sin(degtorad(angle))*sizee*_radius*_multi_see*multiplier//*((_character.level)*.01);
		
		stat_display_x[i] = 0;
		stat_display_y[i] = 0;
	
		stat_display_val_x[i] = sign(stat_x[i])*clamp(abs(char_stat_x[i]*stat_boost_val[i]),0,abs(stat_x[i]))
		stat_display_val_y[i] = sign(stat_y[i])*clamp(abs(char_stat_y[i]*stat_boost_val[i]),0,abs(stat_y[i]))
		angle += 60;
	}
	
} else {
	for(var i = 0; i < 6; i++){
		char_stat_x[i] = 0;
		char_stat_y[i] = 0;
	}
}