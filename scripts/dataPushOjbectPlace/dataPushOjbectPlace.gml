var mapper = obj_data.push_obj_position;

var size = ds_map_size(mapper);
var obj_name_id = "";
var myid = noone;
	
var key = ds_map_find_first(mapper);
for (var i = 0; i < size; i++;)
{
	if(!is_undefined(key)){
		//find id of map.
		
		with(PropParent){
			if(prop_push_id == key){
				myid = id;
			}
		}
		
		if(myid > 0){
			json = json_decode(ds_map_find_value(mapper,key));
			
			var xx = ds_map_find_value(json,"x");
			var yy = ds_map_find_value(json,"y");
			with(myid){
				x = xx;
				y = yy;
			}
			
			ds_map_destroy(json);
		}
		myid = noone;
	} else {
		break;  
	}
	
	key = ds_map_find_next(mapper, key);	  
}

