/// @description Insert description here
// You can write your code in this editor

if(active){
	if(!surface_exists(surf)){
		surf = surface_create(global.letterbox_w,global.letterbox_h);	
		surface_update = true
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,global.letterbox_w,global.letterbox_h)	
			surface_update = true
		}
	}

	var spacee_x = space_x
	var spacee_y = space_y

	if(surface_update){
		surface_set_target(surf)
		draw_clear_alpha(c_black,0);
		gpu_set_colorwriteenable(true,true,true,true);

		var rowdone = page_increment_start+braille_rows;

		for(var i = page_increment_start; i < rowdone; i++){
			for(var j = 0; j < braille_columns; j++){
					//draw the boundary box
					draw_set_color(global.textColor);
					var z = 3;
					var x_pos = start_x + space_x*j
					var y_pos = start_y + space_y*(i-page_increment_start)

					draw_rectangle_noratio(x_pos-z,y_pos-z,x_pos+space_x+z,y_pos+space_y+z,false);

					draw_set_color(c_black)

					draw_rectangle_noratio(x_pos,y_pos,x_pos+space_x,y_pos+space_y,false);
				
				var spaceex = space_x;
				var spaceey = space_y;
				
				with(display_cell[i,j]){
					brailleCharacterDraw();
					if(!draw_character){
						draw_set_font(global.normalFont)
						draw_set_color(global.textColor)
						draw_set_halign(fa_center)
						draw_text_transformed_noratio(x_pos+spacee_x*.5,y_pos+spacee_y*.05,cell_character,2,2,0);
						draw_set_halign(fa_left)
					}
				}
			}
		}

		surface_reset_target()
	}

	gpu_set_colorwriteenable(true,true,true,false);

	draw_set_alpha(.75)
	draw_set_color(c_black)
	draw_rectangle_ratio(0,0,800,600,false);

	draw_set_alpha(1)
	draw_surface_ext(surf,global.display_x,global.display_y,1,1,0,global.textColor,1);

	gpu_set_colorwriteenable(true,true,true,false)
	if(drawTitle){
		drawOutlineText(20,20,title,titleFontSize,titleFontSize,0,title_color)
	}
	//draw_set_color(subtitle_color)

	//draw subtitle
	if(drawSubtitle){
		drawOutlineText(20,75,subtitle,subtitleFontSize,subtitleFontSize,0,subtitle_color)
	}
	gpu_set_colorwriteenable(true,true,true,true)

	//draw selected rectangle
	var z = 5;

	var x_pos = start_x + space_x*selected_column
	var y_pos = start_y + space_y*(selected_row)

	draw_set_color(global.textColor);
	draw_set_alpha(select_alpha)
	draw_rectangle_ratio(x_pos,y_pos,x_pos+space_x,y_pos+space_y,false);

	draw_set_alpha(1);

		draw_set_halign(fa_center)
		draw_text_transformed_ratio(400,550,braille_current_charset,3,3,0)
		draw_sprite_extended_ratio(spr_scroll_right,ScaleManager.spr_counter,100,560,-.5,.5,0,global.textColor,1)
		draw_sprite_extended_ratio(spr_scroll_right,ScaleManager.spr_counter,700,560,.5,.5,0,global.textColor,1)
		draw_set_halign(fa_left)
}