if(bird_sprite != -1){
	draw_rectangle_color_ratio(500,180,800,500,c_black,c_black,$7f7f7f,$7f7f7f,false)
	draw_rectangle_color_ratio(500,500,800,550,$7f7f7f,$7f7f7f,c_black,c_black,false)
	draw_sprite_extended_ratio(bird_sprite,0,650,400+bird_adjust,bird_scale,bird_scale,0,c_white,1)
	
	draw_set_font(global.largeFont)
	draw_set_color(global.textColor)
	
	draw_text_transformed_ratio(30,130,bird_long_name,2,2,0)
	
	draw_set_font(global.textFont);
	draw_text_transformed_ratio(30,180,bird_science_name,2,2,0)
	
	draw_text_transformed_ratio(30,210+15,"Height: " + bird_height,2,2,0)
	draw_text_transformed_ratio(30,240+15,"Weight: " + bird_weight,2,2,0)
	
	draw_text_transformed_ratio(30,300,bird_desc,2,2,0)
	
	draw_text_transformed_ratio(30,515,"First recorded: " + bird_time,2,2,0)
}