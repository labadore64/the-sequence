

//menuAddOption("Vignette","Enable the Vignette on the Field.",-1)
/*
global.sfxVolume = 1; //volume for sfx
global.musicVolume = 1; //volume for music
global.AXVolume = 1; //volume for AX utils
*/

menuAddOption(global.langini[0,39],global.langini[1,39],menuOverworldOptionSoundLeft)
menuAddOption(global.langini[0,40],global.langini[1,40],menuOverworldOptionSoundLeft)
menuAddOption(global.langini[0,41],global.langini[1,41],menuOverworldOptionSoundLeft)
menuAddOption(global.langini[0,42],global.langini[1,42],menuOverworldOptionAXScript)

text_speed_strings = ds_list_create();

text_color_strings = ds_list_create();
text_color_values = ds_list_create()

text_font_strings = ds_list_create();
text_font_values = ds_list_create();

text_sound_strings = ds_list_create();

for(var i = 0; i < 11; i++){
	ds_list_add(text_color_strings,"Lv. " + string(i));
	ds_list_add(text_font_strings,"Lv. " + string(i));
	ds_list_add(text_speed_strings,"Lv. " + string(i));
}

ds_list_add(text_sound_strings,global.langini[12,19]);

listsss = ds_list_create()

ds_list_add(listsss,text_speed_strings,text_color_strings,text_font_strings,text_sound_strings)



//whether or not the value should wrap around when adjusted.
wrapz[0] = false;
wrapz[1] = false;
wrapz[2] = false;
wrapz[3] = false;

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);
menuParentSetSelectWidth(.5)

menu_left = menuOverworldOptionSoundLeft
menu_right = menuOverworldOptionSoundRight

menu_up = menuOverworldOptionUp
menu_down = menuOverworldOptionDown

menu_draw = menuOverworldOptionSoundDraw;

curposs[0] = round(global.sfxVolume*10);
curposs[1] = round(global.musicVolume*10);
curposs[2] = round(global.envVolume*10)
curposs[3] = 0;

with(menuControl){
	transparent = true;
}