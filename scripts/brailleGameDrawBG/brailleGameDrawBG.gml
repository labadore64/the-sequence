with(brailleGameBackground){
	
	if(!surface_exists(surf)){
		surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,global.window_width,global.window_height);
		}
	}
	
	surface_set_target(surf)
	draw_clear_alpha($333333,1);
	part_system_drawit(Sname);
	surface_reset_target();
	
	if(draw_me){
		if(shader_enabled && !global.disableShader){
			shader_set(shd_background_rainbow);

			shader_set_uniform_f(shd_vcr_counter,shader_counter)
			shader_set_uniform_f(shd_vcr_seed,shader_seed)

			shader_set_uniform_f(shd_vcr_u_uv,global.window_width,global.window_height)
	
			draw_surface(surf,0,0);
	
			shader_reset();
		} else {
			draw_surface(surf,0,0);	
		}

		draw_letterbox();
	}
}