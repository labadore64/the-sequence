var goto = option_goto[|menupos];
var event = option_event[|menupos];
var set_next = option_set_next[|menupos];
//var endscript = option_end_script[|menupos];

with(DialogHandler){
	//finish_script = endscript;
	
	dialogRef = goto;
	if(update_next){
		character.next_dialog = set_next;
	}
	newalarm[0] = 1;

	ds_list_clear(dialog_strings);

	ds_list_clear(dialog_emotionList)

	ds_list_clear(dialog_spriteList)

	ds_list_clear(dialog_pitchList)
	ds_list_clear(dialog_background);

	ds_list_clear(option_stringList) //list of string options.
	ds_list_clear(option_gotoList)//list representing the next dialog to call. -1 if end of tree.
	//ds_list_clear(option_eventTrueList)//list representing 1 event for each option that must be true to display. usually -1.
	//ds_list_clear(option_eventFalseList) //list representing 1 event for each option that must be false to display. usually -1.
	ds_list_clear(option_eventSetList)//list representing the event that is set if you select a certain option. usually -1.
	//ds_list_clear(option_displayList) //list representing whether or not options are displayed based on true/false lists.
	//ds_list_clear(option_nextTree)//list representing the next tree to go to after conversation is finished. Usually -1 unless end of tree.
	//ds_list_clear(option_endScript);
	
}

if(event != "none"){
	eventNormalSet(event);
}

instance_destroy();