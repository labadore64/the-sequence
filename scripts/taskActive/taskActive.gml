var task = argument[0]

if(task > -1){
	if(eventNormalCheckIndex(objdata_task.event_id[task]) 
		&& !eventNormalCheckIndex(objdata_task.completed_event_id[task])){
			return true;
	}
}
return false;