with(obj_data){
	if(ds_exists(game_stats, ds_type_map)){
		if(ds_map_exists(game_stats, argument[0])){
			game_stats[? argument[0]]++;	
		} else {
			ds_map_add(game_stats, argument[0], 1);	
		}
	}
}