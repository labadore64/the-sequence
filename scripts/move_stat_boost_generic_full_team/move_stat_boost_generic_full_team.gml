var chara = argument[0];
var targe = argument[1];

var map = argument[2]; //map of stat boosts
var turns = argument[3];

var size, key, i;
size = ds_map_size(map);
key = ds_map_find_first(map);

var list = BattleHandler.party_list;

if(targe.is_enemy){
	list = BattleHandler.enemy_list;	
}

var list_sizer = ds_list_size(list);

var obj = -1;

for (i = 0; i < size; i++;)
{
	for(var j = 0; j < list_sizer; j++){
	obj = list[|j];
		if(!is_undefined(obj)){
			if(instance_exists(obj)){
				battleStatChangeAdd(obj,key,ds_map_find_value(map,key),turns);
				key = ds_map_find_next(map, key);
			}
		}
	}
}

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);
stringer = string_replace_all(stringer,"@1",targe.name);

battleAttackInputAddString(stringer);

	var stringer = battletext[|1];

	if(!is_undefined(stringer)){
		stringer = string_replace_all(stringer,"@0",chara.name);
		stringer = string_replace_all(stringer,"@1",targe.name);

		battleAttackInputAddString(stringer);
	}


battleAttackInputAddString(" ");

ds_list_destroy(battletext);

ds_map_destroy(map);