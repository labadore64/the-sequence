var email = objdataToIndex(objdata_email,argument[0]);

if(email > -1){
	// adds the email to the unread
	with(obj_data){
		if(objdata_email.state[email] == EMAIL_STATE_NONE){
			objdata_email.state[email] = EMAIL_STATE_UNREAD
		}
	}
}