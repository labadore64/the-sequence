with(obj_data){
	if(ds_exists(game_stats, ds_type_map)){
		if(ds_map_exists(game_stats, argument[0])){
			return game_stats[? argument[0]];	
		}
	}
}

return 0;