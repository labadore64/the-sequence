if(active){

draw_set_color(c_black)
draw_rectangle_ratio(0,485,800,600,false)

draw_set_color(global.textColor);
draw_set_font(global.largeFont);
var xx_base = 500;

if(menu_size == 3){
	xx_base = 600	
}
var yyy = 510;
var x_spacer = 185;

var scale = 2;

var select_scale_factor = 1;

with(BattleHandler){
	select_scale_factor = scale_value;	
}

draw_set_halign(fa_center)
draw_set_valign(fa_middle)
var myscale = scale;

var stringer = "";

for(var i = 0; i < menu_size; i++){
	stringer =  menu_name_array[i];
		if(menupos == i){
			myscale = scale*select_scale_factor
		} else {
			myscale = scale;	
		}
		draw_text_transformed_ratio(xx_base+(i-2)*x_spacer,yyy+20,stringer,myscale,myscale,0)
}
draw_set_valign(fa_top)
draw_set_font(global.textFont);
stringer = menu_desc_array[menupos];
if(!is_undefined(stringer)){
	draw_text_transformed_ratio(400,yyy+50,stringer,2,2,0)	
}

draw_set_halign(fa_left)

draw_set_font(global.textFont);
}