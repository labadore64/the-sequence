if(objGenericCanDo()){
	if(keyboard_check_pressed(vk_anykey)){
		//any key is pressed
		if(keyboard_check_pressed(vk_enter)){
			//complete	
			enterTextEnter();
		} else if(keyboard_check_pressed(vk_backspace)){
			//backspace
			if(string_length(current_text) != 0){
				enterTextDeleteChar();
				tts_say(string_char_at(current_text,position-1));
				soundfxPlay(sound_move)
			}
		} else if (keyboard_check_pressed(vk_left)){
				position--;	
				tts_say(string_char_at(current_text,position-1));
				soundfxPlay(sound_move)
		} else if (keyboard_check_pressed(vk_right)){
			position++;
			if(position > string_length(current_text)){
				position = string_length(current_text)+1;	

			}
			tts_say(string_char_at(current_text,position-1));
			soundfxPlay(sound_move)
		} else if (keyboard_check_pressed(vk_shift)) {
			tts_say(current_text);	
		} else {
			var current_size = string_length(current_text);
			
			if(current_size <= max_character){
				if(position == current_size+1){
					current_text += keyboard_lastchar;
				} else {
					current_text = string_insert(keyboard_lastchar,current_text,position);	
				}
				tts_say(keyboard_lastchar)
				if(current_size+1 == string_length(current_text)){
					position++;
					soundfxPlay(sound_move)
				}
				
			} else {
				//fail	
				state = 1;
				soundfxPlay(sound_move)
				tts_say(current_text + " " + "Is this okay?")
			}
		}
		
		if(position < 1){
			position = 1;	
		}
		keyboard_lastchar = "";
	}
}