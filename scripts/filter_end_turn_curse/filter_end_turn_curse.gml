var chara = argument[0];

var texts = filterGetText("curse")

with(chara){
	var damg = floor(hp.base * .04)
	
	var changed = hp.current - damg;
		
	battlerHPChange(id,changed)
		
	var stringer = texts[|0];
	
	stringer = string_replace_all(stringer,"@0",name);
	stringer = string_replace_all(stringer,"@1",string(4));
	battleTextDisplayAddText(stringer);
	
	var stringer = texts[|1];
	
	battleAnimHit(id);
		
	stringer = string_replace_all(stringer,"@0",name);
	stringer = string_replace_all(stringer,"@1",string(4));
	battleTextDisplayAddText(stringer);
	
	if(!battlerIsAlive(id)){
		stringer = "@0 was defeated!"
		stringer = string_replace_all(stringer,"@0",name);
		battleTextDisplayAddText(stringer);
	}
		
}

ds_list_destroy(texts);