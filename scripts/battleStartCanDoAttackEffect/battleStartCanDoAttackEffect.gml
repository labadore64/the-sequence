var returner = true;

if(battlerHasFilter(character,"smoke")){
	if(move_type == MOVE_TYPE_PHYSICAL){
		if(random(1) < .25){
			returner = false;
			var set_string = "@0's attack misses!"
			set_string = string_replace_all(set_string,"@0",character.name);
			battleAttackInputAddString(set_string);	
		}
	}
}

if(battlerHasFilter(character,"blind")){
	if(move_type == MOVE_TYPE_PHYSICAL){
		if(random(1) < .1){
			returner = false;
			var set_string = "@0's attack misses!"
			set_string = string_replace_all(set_string,"@0",character.name);
			battleAttackInputAddString(set_string);	
		}
	}
}

return returner;