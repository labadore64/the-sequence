/// @description - sets stats based on character
/// @param character

var chara = argument[0];
calcItemStatsForCharacter(chara);

sell_multiplier_script = characterSellMultiplier(chara.character);
buy_multiplier_script = characterBuyMultiplier(chara.character);
buy_item_script = characterBuyList(chara.character)

var itemcount = argument[0].character;

if(!is_undefined(itemcount)){
	if(itemcount > -1){
		phy_power = objdata_character.phy_power[itemcount];
		phy_guard = objdata_character.phy_guard[itemcount];
		mag_power = objdata_character.mag_power[itemcount];
		mag_guard = objdata_character.mag_guard[itemcount];
		spe_agility = objdata_character.spe_agility[itemcount];
		spe_mpr = objdata_character.spe_mpr[itemcount];
		hp = objdata_character.hp[itemcount];
		mp = objdata_character.mp[itemcount];

		r = 0
		g = 0
		b = 0

		r_bat = 0
		g_bat = 0
		b_bat = 0
	
		element = objdata_character.element[itemcount];

		color = objdata_character.color[itemcount];

		name = objdata_character.name[itemcount];

		face_sprite = objdata_character.face_sprite[itemcount];
	}
}

statExperience(chara);

phy_power_base = phy_power; //attack base
phy_guard_base = phy_guard; //defense base
mag_power_base = mag_power; //magic base
mag_guard_base = mag_guard; //resistance base
spe_agility_base = spe_agility; //agility base
spe_mpr_base = spe_mpr; //mpr

hp_base = hp;
mp_base = mp;

//inventory

//moves
//moves = ds_list_create();

statCalculate();

moveAllyLoad(chara)//statLoadMoves(chara);