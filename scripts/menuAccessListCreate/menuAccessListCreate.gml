event_inherited();

bookmark_id = -1;

var lister = ds_list_create();

with(AXBookmarkOverworld){
	if(object_index == AXBookmarkOverworld){
		ds_list_add(lister,id);
	}
} 

with(AXBookmark){
	if(object_index == AXBookmark){
		ds_list_add(lister,id);
	}
}

var o = -1;

var sizer = ds_list_size(lister)

for(var i = 0; i < sizer; i++){
	o = lister[|i];
	if(!is_undefined(o)){
		if(instance_exists(o)){
		
			menuAddOption(o.name,"x: " + string(coordConvert(o.x)) + " " + "y: " + string(coordConvert(o.y)),-1)
		}
	}
}
list_of_objs = lister;



menuParentSetTitle("Locations")
menuParentSetSubtitle("")

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetDrawGradient(false)

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

x = 400
y = 300

menuParentSetBoxWidth(600)
menuParentSetBoxHeight(350)

menuParentUpdateBoxDimension();

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(c_white);



menu_left = menuAccessMoveUp
menu_right = menuAccessMoveDown;

menu_down = menu_right;
menu_up = menu_left;

menu_input_text = menuBookmarkInputText

tts_clear();

newalarm[2] = 1
