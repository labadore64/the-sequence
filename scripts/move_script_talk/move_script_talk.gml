
var chara = argument[0];
var targe = argument[1];


var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);
stringer = string_replace_all(stringer,"@1",targe.name);

battleAttackInputAddString(stringer);

//check if talking can be done.

var count = ds_list_size(BattleHandler.enemy_list)
var cando = true;

var hp_ratio = 5//1-(targe.hp.current/targe.hp.base)+.15;

if(random(1) > hp_ratio){
	cando = false	
}

if(count > 1){
	cando = false;	
}

if(targe.trigger_drop_betray){
	var sizer = ds_list_size(battletext)-2
	
	var stringer = battletext[|sizer];
	
	stringer = string_replace_all(stringer,"@0",targe.name);

	battleAttackInputAddString(stringer);
} else if (BattleHandler.battle_sim){
	var sizer = ds_list_size(battletext)-1
	
	var stringer = battletext[|sizer];
	
	stringer = string_replace_all(stringer,"@0",targe.name);

	battleAttackInputAddString(stringer);	
}else {

	if(cando){
		var val = 1;
	
		if(battlerHasFilter(targe,"Talk")){
			val = 2
		} else {
			battlerAddFilter(targe,"Talk")	
			battlerAddFilter(chara,"Listen")
			targe.trigger_drop_talk = true;
		}
		var stringer = battletext[|val];
	
		stringer = string_replace_all(stringer,"@0",targe.name);

		battleAttackInputAddString(stringer);
		
		with(battleAttackInput){
			if(character == targe){
				instance_destroy();	
			}
		}
	
	} else {
		var sizer = ds_list_size(battletext)-3
	
		var stringer = battletext[|irandom_range(3,sizer)];
	
		stringer = string_replace_all(stringer,"@0",targe.name);

		battleAttackInputAddString(stringer);
	}
}

ds_list_destroy(battletext);