var chara_first = argument[0];
var chara_receiver = argument[1];
var filter_id = argument[2]; //actual filter

//first reassign filter to the new target
with(filter_id){
	target = chara_receiver;	
}

//remove filter from initial character's filter list
with(chara_first){
	var index = ds_list_find_index(status_effects,filter_id);
	ds_list_delete(status_effects,index);
	newalarm[1] = 1;
}

//add filter to new character's filter list
with(chara_receiver){
	ds_list_add(status_effects,filter_id);	
	newalarm[1] = 1;
}