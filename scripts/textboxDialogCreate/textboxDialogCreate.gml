/// @description newChat(messageArray, Portrait);
/// @function newChat
/// @param messageArray
/// @param  Portrait
/// @param  xpoint
var sent = argument0;
var portrait = argument1;
var text = instance_create(argument2, 0, obj_dialogue);

text.portrait = portrait;

var sizer = ds_list_size(sent)

for (i = 0; i < sizer; i++)
{
    text.message[i] = sent[|i];
}

mouseHandler_clear()

// update this later to update the text...
mouseHandler_add(0,0,800,600,menu_select,"")

return text;