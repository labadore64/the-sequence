if(battlerHasFilter(character,"lock")){
	soundfxPlay(soundMenuInvalid)
} else {
	var moves_instance = menuCreateInstance(menuBattleMoves)

	var moves_list = character.moves;

	var moves_len = ds_list_size(moves_list);

	moves_instance.character = character;

	var obj = -1;

	for(var i = 0; i < moves_len; i++){
		obj = moves_list[|i]

		if(!is_undefined(obj)){
			ds_list_add(moves_instance.moves,obj);	
		}
	}
}