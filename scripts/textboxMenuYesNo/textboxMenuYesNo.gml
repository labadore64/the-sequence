var choices = ds_list_create();
var choices_scripts = ds_list_create();

ds_list_add(choices,LANG_SELECT_YES);
ds_list_add(choices,LANG_SELECT_NO)

ds_list_add(choices_scripts,yes_script)
ds_list_add(choices_scripts,no_script)

textboxMenuGeneric(text_box_id,choices,choices_scripts);

ds_list_destroy(choices);
ds_list_destroy(choices_scripts);