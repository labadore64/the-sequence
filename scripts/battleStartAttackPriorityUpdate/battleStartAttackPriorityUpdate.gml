with(battleStartAttack){
	var lister = ds_priority_create();

	var sizer = 0;
	var obj = -1;
	var lis = 0;
	for(var i =0; i < 7; i++){
		if(!ds_list_empty(priority[i])){
			ds_priority_clear(lister);
			sizer = ds_list_size(priority[i]);
			for(var j = 0; j < sizer; j++){
				lis = priority[i];
				obj = lis[|j];
				with(obj){
					battleAttackInputUpdate()	
				}
				ds_priority_add(lister,obj,obj.agility);
			}
		
			ds_list_clear(priority[i]);
			while(!ds_priority_empty(lister)){
				obj = ds_priority_delete_max(lister)
				ds_list_add(priority[i],obj);
			}
		}
	}

	ds_priority_destroy(lister)

}