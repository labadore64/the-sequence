event_inherited();
multtt = 0;
multttz = 0;
maxdamage = 50;

menuParentSetTitle(global.langini[2,9])
menuParentSetSubtitle(global.langini[3,9])

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menu_draw = menuOverworldPartyMovesDraw

menuParentSetDrawGradient(false)


menuParentUpdateBoxDimension();

menuParentSetInfoboxSubtitleColor(global.textColor);
menuParentSetInfoboxTitleColor(global.textColor)

menuParentSetOptionYOffset(200-65)
menuParentSetOptionXOffset(40)
menuParentSetSelectWidth(.5)

var xxx = 400;
var yyy = 550;

newalarm[2] = 1;

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

menu_up = menuOverworldPartyMovesUp
menu_down = menuOverworldPartyMovesDown

menu_type = MOVE_TYPE_PHYSICAL;

physical_moves = ds_list_create();
magic_moves = ds_list_create();