var args = argument[0];

var item_id = real(textboxArgumentGet("item_id"));
var obj_id = textboxArgumentGet("obj_id");

soundfxPlay(soundPickupItem)

giveItem(item_id,1);

if(!is_undefined(obj_id)){
	var prop_place_id = "";
	
	with(obj_id){
		prop_place_id = prop_id;
		instance_destroy();
	}
	
	with(obj_data){
		ds_map_delete(prop_place,prop_place_id);
	}
}