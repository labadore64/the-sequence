var charaz = argument[0];
var targ = argument[1]

selected_move =  moves[|menupos];
	
	var getmove = moves[|menupos];
	//itemData(getmove);
	
	if(object_index == menuBattleTablet){
		getmove = moves[|main_index]
	}
	
	var dir = false;;
	
	with(menuBattleTabletConfirm){
		if(menupos == 1){
			dir = true
		}
	}
	
	selected_move = getmove;
	
	moveData(selected_move);
	var ha = battleAttackInputCreate(charaz,targ,selected_move)
	
	ha.braille_left = dir;
		
	// braille stuff
	
	if(object_index == menuBattleTablet){
		ha.braille_string = selected_string;
		ha.braille_string_base = selected_string;
	}
	
	// end braille stuff
	
	var targ_list = -1;
	if(instance_exists(charaz) &&
		instance_exists(targ)){
		if(charaz.is_enemy == targ.is_enemy){
			if(charaz.is_enemy){
				targ_list = BattleHandler.enemy_list;	
			} else {
				targ_list = BattleHandler.party_list	
			}
		} else {
			if(!charaz.is_enemy){
				targ_list = BattleHandler.enemy_list;	
			} else {
				targ_list = BattleHandler.party_list	
			}

		}		
		} else {
			if(target_ally){
				targ_list = BattleHandler.party_list;	
			} else {
				targ_list = BattleHandler.enemy_list;
			}
	}
	
	var add_target = ha.additional_targets;
	if(target_number == 2){
		var next_index = ds_list_find_index(targ_list,targ);
		if(next_index != -1){
			next_index = (next_index+1) mod ds_list_size(targ_list);
		}
		var obj = targ_list[|next_index];
		if(!is_undefined(obj)){
			ds_list_add(add_target,obj);	
		}
	} else if (target_number == 3){
		//put in every other index
		var sizer = ds_list_size(targ_list);
		var obj = -1;
		for(var i =0; i < sizer; i++){
			obj = targ_list[|i];
			if(!is_undefined(obj)){
				ds_list_add(add_target,obj);	
			}
		}
		
		//delete target from the list.
		ds_list_delete(add_target,ds_list_find_index(add_target,targ))
	}
	instance_destroy();
	
	with(menuBattleTabletConfirm){
		instance_destroy();
	}
	
	return ha;