var itemcount = getHUDFromName(argument[0]);

if(itemcount > -1){
	
	var obj = instance_create(0,0,HUDObject);
	
	var parentval = noone;

	if(argument_count > 1){
		parentval = argument[1];
	}

	var name_id = "";

	if(argument_count > 2){
		name_id = argument[2];
	}
	
	var varname = "";
	
	if(argument_count > 3){
		varname = argument[3];
	}
	
	if(name_id == ""){
		obj.name = objdata_UI.name[itemcount]; // name used as reference for this object
	} else {
		obj.name = name_id;	
	}
	
	if(varname == ""){
		obj.variable_name = obj.name;	
	} else {
		obj.variable_name = varname;	
	}
	
	obj.hud_id = itemcount;
	
	obj.hudobj_name = objdata_UI.ui_id[itemcount]; // name used for the filename of this object

	obj.read_text = objdata_UI.read_text[itemcount];

	obj.parent = parentval; // this object's parent object, used for the origin command and the parent keyword

	// get key data
		
	var keydata_len = array_length_2d(objdata_UI.key_id,itemcount);
	
	for(var i = 0; i < keydata_len; i++){
		obj.key_id[i] = objdata_UI.key_id[itemcount,i];
		obj.key_func[i] = objdata_UI.key_func[itemcount,i];
		obj.key_arg[i] = objdata_UI.key_arg[itemcount,i];
		obj.key_req[i] = objdata_UI.key_req[itemcount,i];
	}
		
	keydata_len = array_length_2d(objdata_UI.infokey,itemcount)
		
	for(var i = 0; i < keydata_len; i++){
		obj.infokey[i] = objdata_UI.infokey[itemcount,i]
	}


	// create children from templates
	var len = array_length_2d(objdata_UI.children_strings,itemcount)

	var children = ds_list_create();

	for(var i = 0; i < len; i++){
		var stringe = objdata_UI.children_strings[itemcount,i]
		var template = objdata_UI.children_templates[itemcount,i]
		var varname = objdata_UI.children_variable[itemcount,i]
		if(stringe != "" && template != ""){
	
			ds_list_add(children,HUDObjectCreate(template,obj,stringe,varname));
		}
	}
	
	var len = ds_list_size(children);
	
	for(var i = 0; i < len; i++){
		obj.children[i] = children[|i];	
		if( objdata_UI.children_customhud[itemcount,i] != ""){
			obj.children[i].read_text = objdata_UI.children_customhud[itemcount,i]
		}
	}

	ds_list_destroy(children);

	with(HUDController){
		ds_map_add(HUDObjects,obj.name,obj);
	}
	
	// finally, check if the object has a cancel event.
	// if it doesnt, add a default exit cancel event.
	// this way if you forget you won't hang the game

	var keydata_len = array_length_1d(obj.key_id);
	
	var counter = 0;
	var cancelid = -1;
	
	for(var i = 0; i < keydata_len; i++){
		if(obj.key_id[i] == "cancel"){
			cancelid = i;
			counter++;
		}
	}
	
	if(counter == 0){
		cancelid = keydata_len
		if(obj.parent != noone){
			obj.key_id[cancelid] = "cancel"
			obj.key_func[cancelid] = 1
			obj.key_arg[cancelid] = "{\"dest\":\"parent\", \"sound\":\"cancel\"}";
			obj.key_req[cancelid] = "";
		} else {
			obj.key_id[cancelid] = "cancel"
			obj.key_func[cancelid] = 2
			obj.key_arg[cancelid] = "";
			obj.key_req[cancelid] = "";
		}
	}

	return obj;
}
show_debug_message("Error loading HUD " + argument[0]);
return noone;