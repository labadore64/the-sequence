var mysound = argument[0];

if(mysound != text_sound1){
	audio_stop_sound(text_sound1);	
}

if(mysound != text_sound2){
	audio_stop_sound(text_sound2);	
}

if(mysound != text_sound3){
	audio_stop_sound(text_sound3);	
}

if(mysound != text_sound4){
	audio_stop_sound(text_sound4);	
}

if(!audio_is_playing(mysound)){
	current_sound = mysound;
	soundfxPlayLoop(mysound);
}