if(!global.disableShader){
	shader_set(shd_shadow)
	shader_set_uniform_f(BattleHandler.shd_outline_alpha, image_alpha*hide_alpha*.75);
	draw_sprite_ext(sprite_index,spr_counter,x-flee_dist,y-2-jump_y*image_yscale,image_xscale*sprite_scale,image_yscale*sprite_scale,0,image_blend,image_alpha*hide_alpha);
	shader_reset();
}