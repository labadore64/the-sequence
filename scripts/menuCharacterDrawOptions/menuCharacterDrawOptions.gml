draw_set_color(text_color);

//draw the rectangle of the area

var movee = 4;
var fixer = 10;

var xmover = 40;

draw_rectangle_ratio(x1-movee+xmover,-movee+y1+option_y_offset-fixer,movee+x1+((x2-x1)*select_percent)+xmover,movee-fixer+y1+option_y_offset+option_space*menu_size,false);

draw_set_color(c_black)

draw_rectangle_ratio(x1+xmover,y1+option_y_offset-fixer,x1+((x2-x1)*select_percent)+xmover,y1-fixer+option_y_offset+option_space*menu_size,false);


draw_set_color(text_color);
var stringer = "";
var xx = 0;
var yy = 0;

//get max number of items that can be displayed
var drawmax = 0;
var counter = 0;
for(var i = toppos; i < menu_size; i++){
	xx = x1+option_x_offset;
	yy = y1+option_y_offset+option_space*counter;
	if(yy > y2-option_space){
		drawmax = i;
		break;
	}
	counter++;
	drawmax = i+1;
}
counter = 0;
for(var i = toppos; i < drawmax; i++){
	
	stringer = menu_name_array[ i];
	
	if(!is_undefined(stringer)){
		xx = x1+option_x_offset;
		yy = y1+option_y_offset+option_space*counter;

		draw_text_transformed_ratio(xx+xmover,yy,stringer,text_size,text_size,0)

	}
	counter++;
}

//draw the selection box
draw_set_alpha(select_alpha);
draw_set_color(select_color);

var currpos = menupos - toppos;

yy = y1+option_y_offset+currpos*option_space - option_space*0.25;

//scrolldown function

while(yy > y2-option_space){
	toppos++;
	
	currpos = menupos - toppos;

	yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
}

//scrollup function

while(yy < y1+gradient_height || currpos < 0){
	toppos--;
	
	currpos = menupos - toppos;

	yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
}



draw_rectangle_ratio(x1+xmover,yy,x1+((x2-x1)*select_percent)+xmover,yy+option_space,false)

draw_set_alpha(1)
draw_set_color(global.textColor);