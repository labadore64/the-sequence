if(animation_len == 0){
	gpu_set_colorwriteenable(true,true,true,true)
	menuParentDrawBox();
	draw_set_halign(fa_center);
	gpu_set_colorwriteenable(true,true,true,false)
	menuParentDrawText();
	draw_set_halign(fa_left);
	
	// draw options
	
	draw_set_color(text_color);

	var stringer = "";
	var xx = 0;
	var yy = 0;

	//get max number of items that can be displayed
	var drawmax = 0;
	var counter = 0;
	for(var i = toppos; i < menu_size; i++){
		xx = x1+option_x_offset;
		yy = y1+option_y_offset+option_space*counter;
		if(yy > y2-option_space){
			drawmax = i;
			break;
		}
		counter++;
		drawmax = i+1;
	}
	counter = 0;
	for(var i = toppos; i < drawmax; i++){
	
		stringer = menu_name_array[ i];
	
		if(!is_undefined(stringer)){
			xx = x1+option_x_offset;
			yy = y1+option_y_offset+option_space*counter;

			draw_text_transformed_ratio(xx,yy,stringer,text_size,text_size,0)

		}
		counter++;
	}

	//draw the selection box
	draw_set_alpha(select_alpha);
	draw_set_color(select_color);

	var currpos = menupos - toppos;

	yy = y1+option_y_offset+currpos*option_space - option_space*0.25;

	//scrolldown function

	while(yy > y2-option_space){
		toppos++;
	
		currpos = menupos - toppos;

		yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
	}

	//scrollup function

	while(yy < y1+gradient_height || currpos < 0){
		toppos--;
	
		currpos = menupos - toppos;

		yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
	}
	gpu_set_colorwriteenable(true,true,true,false)
	draw_rectangle_ratio(x1+option_x_offset-7,yy,x1+option_x_offset+((x2-x1)*select_percent),yy+option_space,false)

	draw_set_alpha(1)
	draw_set_color(global.textColor);
	
	var sprite_posx = 550;
	var sprite_posy = 270;
	
	if(spr_img[menupos] != -1){
		draw_sprite_extended_ratio(spr_img[menupos],spr_counter,sprite_posx,sprite_posy,2,2,0,c_white,1)	
	}


	gpu_set_colorwriteenable(true,true,true,true)

	menuParentDrawInfo();
	
	if(object_index == menuBrailleTabletsMain){
		menuOverworldMenuDrawSwitch("Phone","Options")	
	}
}