start = 1;

sound = working_directory + "//resources//cry//" + argument[0] + ".wav";
audio_buff = buffer_create(1024, buffer_grow, 1);

if start = 1
{
    start = 2
	var size = 3500000;
    if(file_exists(sound))
    {
		var datastart = 40;
		
		
        file = file_bin_open(sound,0);
        size = file_bin_size(file);
       var val = 0;
        for(var i=0;i<size;i++;)
        {
			val = file_bin_read_byte(file);
            buffer_write(audio_buff, buffer_u8, val);
        }
       
        file_bin_close(file);
    }
	buffer_seek(audio_buff, buffer_seek_start, 0);
    return audio_create_buffer_sound(audio_buff, buffer_s16, 44100, 0, size, audio_mono);

}

return -1;