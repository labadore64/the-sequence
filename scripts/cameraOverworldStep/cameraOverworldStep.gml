cameraOverworldUpdateDepth();

cameraOverworldUpdatePosition();

if(path != -1){
	if(path_exists(path)){
		if(instance_exists(set_obj)){
			path_change_point(path, 1, set_obj.x, set_obj.y, path_speed);	
		}
	}
}

with(overworld_scalable){
	propParentStepDo();	
}