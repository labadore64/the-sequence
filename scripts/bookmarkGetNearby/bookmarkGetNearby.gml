var xx = argument[0];
var yy = argument[1];

var returner = noone;

	var checkDist = 100000000;

	var mydist = checkDist;

	with(AXBookmark){
		mydist = point_distance(x,y,Player.x,Player.y)

		if(mydist < checkDist){
			checkDist = mydist;
			returner = id;
		}
	}
	
	if(checkDist < AX_BOOKMARK_CHECK_DIST){
		return returner;
	} else {
		return noone;
	}