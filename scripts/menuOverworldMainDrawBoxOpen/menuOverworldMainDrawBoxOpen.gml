var xx1 = 0;
var xx2 = 0;
var yy1 = 0;
var yy2 = 0;

if(draw_centered){
	xx1 = x-draw_width*.5;
	yy1 = 50;
	xx2 = x+draw_width*.5;
	yy2 = y+((animation_start-animation_len)/animation_start)*draw_height*.5;
} else {
	xx1 = x;
	yy1 = y;
	xx2 = x+anim_width;
	yy2 = y+anim_height;
}

draw_set_color(c_black)
draw_rectangle_ratio(xx1,yy1,xx2,yy2,false)

draw_set_color(global.textColor);

if(drawGradient){
	draw_rectangle_color_ratio(xx1,yy1,xx2,yy1+gradient_height,gradient_color1,gradient_color2,gradient_color2,gradient_color1,false)
}