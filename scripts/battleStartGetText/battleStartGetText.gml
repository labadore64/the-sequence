var lists = argument[0];

var returner = "";

var sizer = ds_list_size(lists);

if(sizer > 0){
	var text_search_max = 29;

	var check = irandom(text_search_max);

	var filename = "resources\\battle_text.ini"

	ini_open(filename)

	returner = newline(ini_read_string("characters_"+string(sizer),"battle_entry"+string(check),"@0, @1 and @2 appeared."));

	ini_close()

	for(i = 0; i < sizer; i++){
		returner = string_replace_all(returner,"@"+string(i),lists[|i].name);
	}

}

return returner;