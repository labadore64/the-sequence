gml_pragma("forceinline");

var chara = argument[0];
var xx = argument[1];
var yy = argument[2];

draw_set_color(global.textColor);
draw_set_font(global.largeFont);
draw_text_transformed_noratio(xx,yy,chara.name,1,1,0)
draw_set_font(global.textFont)

var y_spacer = 35;
var y_off = -5

var ypos = yy+y_off+y_spacer-5;
var widd = 55;
var he = 5;


draw_text_transformed_noratio(xx,ypos,"Health",1,1,0)

drawBarNoScale((xx+55)*global.scale_factor,(ypos+20)*global.scale_factor,widd*global.scale_factor,he*global.scale_factor,chara.hp.display,$0000FF)

ypos = yy+y_off+y_spacer*2-5;

draw_set_color(global.textColor);
draw_text_transformed_noratio(xx,ypos,"Energy",1,1,0)

drawBarNoScale((xx+55)*global.scale_factor,(ypos+20)*global.scale_factor,widd*global.scale_factor,he*global.scale_factor,chara.mp.display,$FF0000)