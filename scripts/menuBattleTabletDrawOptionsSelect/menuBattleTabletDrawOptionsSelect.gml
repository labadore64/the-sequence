draw_set_color(text_color);

var stringer = "";
var xx = 0;
var yy = 0;
var scaling = 1;

for(var i = 0; i < 2; i++){
	
	xx = x1+option_x_offset+option_space*i;
	yy = y1+option_y_offset;
	
	stringer = menu_name_array[ i];
	
	if(menupos == i){
		scaling = selected_scale_amount*text_size;	
	} else {
		scaling = text_size;
	}
	
	if(!is_undefined(stringer)){
		xx = x1+option_x_offset+option_space*i;
		yy = y1+option_y_offset

		draw_text_transformed_ratio(xx,yy,stringer,scaling,scaling,0)

	}
}

draw_set_color(global.textColor);