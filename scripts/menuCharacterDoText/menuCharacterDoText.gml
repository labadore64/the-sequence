if(instance_exists(obj_data)){
	var chara = obj_data.characters[|character_id];
	character = chara;
	if(instance_exists(character_sprite)){
		with(character_sprite){
			instance_destroy();	
		}
	}
	character_sprite = instance_create(0,0,drawPortrait)
	
	character_name = chara.name;
	character_sprite.character = character_id;
	character_sprite.x_scalefactor = character_sprite_xscale;
	character_sprite.y_scalefactor = character_sprite_yscale;
	
	with(character_sprite){
		image_xscale = global.scale_factor*x_scalefactor;
		image_yscale = global.scale_factor*y_scalefactor;
	
		x = 256*global.scale_factor*x_scalefactor;
		y = 256*global.scale_factor*y_scalefactor;	
	}

	text_box = instance_create(0,0,obj_dialogue)

	text_box.message[0] = text_to_use
	text_box.lineEnd = 18
	text_box.tY = text_y_start
	text_box.tX = talk_sprite_x-150
	text_box.background_alpha = 0;
	text_box.portrait = character_sprite;

	with(text_box){
		textboxGetVoice(chara.character);	
	}
}