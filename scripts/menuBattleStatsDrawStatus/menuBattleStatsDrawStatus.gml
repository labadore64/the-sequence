if(status_count > 0){
	draw_set_color(global.textColor);
	
	draw_set_font(global.largeFont);
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(100,510,current_status.name,2,2,0)
	draw_set_halign(fa_left)
	draw_set_font(global.textFont);
	
	draw_text_transformed_ratio(200,500,current_status.flavor,2,2,0);
	
	draw_text_transformed_ratio(30,570,"Turns: " + status_turns,2,2,0);
	
	var small_h = 570;
	
	draw_text_transformed_ratio(760+3,small_h,string(status_count),2,2,0);
	
	draw_set_halign(fa_center)
	
	draw_text_transformed_ratio(760,small_h,"/",2,2,0);
	
	draw_set_halign(fa_right)
	
	draw_text_transformed_ratio(760-3,small_h,string(status_pos+1),2,2,0);
	
	draw_set_halign(fa_left)
}