
// argument 0-1: starting point 
// argument 2-3: testing point 
var returner = 1000000;
with(argument[0]){

	line_collision_resultx = 0
	line_collision_resulty = 0


	for (var i = 0; i < 4; i++)
	{
		doLinesCollide(argument[1],argument[2],argument[3],argument[4],
			pointsx[i],pointsy[i],
			pointsx[(i+1)%4],pointsy[(i+1)%4],
		);
		if (line_collision_resultx > 0 && 
			line_collision_resulty > 0)
		{
			returner = min(returner, point_distance(argument[1],argument[2], line_collision_resultx,line_collision_resulty));
		}

	}
}
return returner;
