var definition = json_decode(argument[0]);

if(definition > -1){
	//parse through entries
	var defa = definition[? "default"];
	
	if(!is_undefined(defa)){
		if(!is_string(defa)){
			if(ds_exists(defa,ds_type_list)){
				var sizer = ds_list_size(defa);
				var obj;
				for(var i = 0; i < sizer; i++){
					obj = defa[| i];
					if(!is_undefined(obj)){
						// obj should be a map of the properties for each graphic.
						menuTutorialLoadAGraphic(obj)
					}
				}
			}
		}
	}	
}

if(ds_exists(definition,ds_type_map)){
	ds_map_destroy(definition);	
}