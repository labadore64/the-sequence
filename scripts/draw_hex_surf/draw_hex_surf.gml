//arument0 = surface
//argument1 = center
surface_set_target(argument0)
draw_clear_alpha(c_white,0);

draw_set_alpha(1);

draw_set_color(c_black)

draw_primitive_begin(pr_trianglelist)

for(var i = 0; i < 6; i++){
	draw_vertex(
		(stat_x[i]+argument1)*global.scale_factor,
		(stat_y[i]+argument1)*global.scale_factor)
	draw_vertex(
		argument1*global.scale_factor,
		argument1*global.scale_factor)
	draw_vertex(
		(stat_x[(i+1) mod 6]+argument1)*global.scale_factor,
		(stat_y[(i+1) mod 6]+argument1)*global.scale_factor)
}
//draw_vertex(stat_x[0]+argument1,stat_y[0]+argument1)

draw_primitive_end();

draw_set_color($7F7F7F)

for(var i = 0; i <6; i++){
	draw_line_width_ratio(stat_x[i]+argument1, stat_y[i]+argument1, argument1, argument1, 2);
}

if(!dontdraw ){
	draw_set_color(draw_rotate_color)

	draw_primitive_begin(pr_trianglestrip)

	for(var i = 0; i < 6; i++){
		draw_vertex(
					(stat_display_val_x[i]+argument1)*global.scale_factor,
					(stat_display_val_y[i]+argument1)*global.scale_factor)
		draw_vertex(
					argument1*global.scale_factor,
					argument1*global.scale_factor)
	}
	draw_vertex(
		(stat_display_val_x[0]+argument1)*global.scale_factor,
		(stat_display_val_y[0]+argument1)*global.scale_factor)

	draw_primitive_end();

}

draw_set_color(global.textColor)

draw_primitive_begin(pr_trianglestrip)

for(var i = 0; i < 6; i++){
	draw_vertex_ratio(
		(char_stat_x[i]+argument1),
		(char_stat_y[i]+argument1))
	draw_vertex_ratio(
		argument1,
		argument1)
}
draw_vertex_ratio(
	(char_stat_x[0]+argument1),
	(char_stat_y[0]+argument1))

draw_primitive_end();

for(var i = 0; i < 6; i++){
	draw_set_color(c_black)
	draw_line_width_ratio(char_stat_x[i]+argument1, char_stat_y[i]+argument1, char_stat_x[(i+1) mod 6]+argument1, char_stat_y[(i+1) mod 6]+argument1, 3);
	//draw_circle(char_stat_x[i]+argument1,char_stat_y[i]+argument1,6,false);
	draw_set_color(color[i]);
	draw_line_width_color_ratio(stat_x[i]+argument1, stat_y[i]+argument1, stat_x[(i+1) mod 6]+argument1, stat_y[(i+1) mod 6]+argument1, 5, color[i], color[(i+1) mod 6]);
	draw_circle_hex_ratio(stat_x[i]+argument1,stat_y[i]+argument1,2,false);
	//draw_set_color(c_white)
	//draw_circle(char_stat_x[i]+argument1,char_stat_y[i]+argument1,4,false);
}



surface_reset_target();