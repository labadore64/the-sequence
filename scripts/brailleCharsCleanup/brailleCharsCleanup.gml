if(ds_exists(all_braillecells,ds_type_list)){
	ds_list_destroy(all_braillecells);	
}

//braille_insert_queue
if(ds_exists(braille_insert_queue,ds_type_queue)){
	ds_queue_destroy(braille_insert_queue);	
}

if(ds_exists(braille_inserted_queue,ds_type_queue)){
	ds_queue_destroy(braille_inserted_queue);	
}

with(input_queue){
	instance_destroy();	
}