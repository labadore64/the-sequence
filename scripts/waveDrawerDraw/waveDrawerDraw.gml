/// @description Insert description here
// You can write your code in this editor

if(!surface_exists(surface)){
	surface = surface_create_access(width,height)	
	surface_set_target(surface)
	
	draw_set_color(c_aqua)

	draw_line(0,
				height*.5,
				width,
				height*.5
				);

	surface_reset_target();
	draw_set_color(c_black)
} else {
	if(ScaleManager.updated){
		surface_resize(surface,width,height);
		surface_set_target(surface)
		draw_set_color(c_aqua)

		draw_line(0,
					height*.5,
					width,
					height*.5
					);

		surface_reset_target();
		draw_set_color(c_black)
	}
}
if(!surface_exists(surface_last)){
	surface_last = surface_create_access(width,height)	
} else {
	if(ScaleManager.updated){
		surface_resize(surface_last,width,height);
	}
}

surface_set_target(surface_last)

draw_surface(surface,0,0);

surface_reset_target();

surface_set_target(surface)

draw_surface(surface_last,-4,0);

// draw cursor

draw_set_color(c_black);

draw_rectangle((width-4),0,width,height,false);

draw_set_alpha(1)

draw_set_color(c_aqua)

for(var i = 4; i >= 0; i--){
	draw_rectangle(width-i-1,
				.9*min_peak[i]*height + height*.5,
				width-i,
				.9*max_peak[i]*height + height*.5,
				false);
}
draw_set_alpha(1)
surface_reset_target();
gpu_set_texfilter(true);
gpu_set_blendmode(bm_add)
draw_surface_ext(surface,global.display_x+x*global.scale_factor,global.display_y+y*global.scale_factor,global.scale_factor,global.scale_factor,0,c_white,1);
gpu_set_blendmode(bm_normal)
gpu_set_texfilter(false);