if(skip_spaces){
	display_string = brailleDisplayStringRightEnd(display_string,argument[0])
	
	var lengu = string_length(display_string);
	for(var i = 0; i < lengu; i++){
		brailleDataSetCharacter(string_char_at(display_string,i+1),braille_display[i])	
	}
} else {
	brailleDisplayInsertLeft(argument[0],argument[1].count-1,argument[1]);
}
brailleDisplayCheckWords();