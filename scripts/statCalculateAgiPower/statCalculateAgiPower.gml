/// @description - returns adjusted AGIPOWER stat

var minvalue = 10;

var multiplier = getMultiplierFromLevel(objdata_character.spe_power_grow[character],level);

var returner = ceil((minvalue + multiplier * (spe_agility_base+(boost_spe_agility)*BATTLE_STAT_MULTIPLIER_ITEM)));

returner += statCalculateBrailleBoost(multiplier,argument[0]);

return returner;