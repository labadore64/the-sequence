/// @description - set volume of music
/// @param volume

var vol = argument[0]

audio_sound_gain(soundAXMenuBank01,vol,1);
audio_sound_gain(soundAXMenuBank02,vol,1);

audio_sound_gain(sound3D_bookmark1,vol,1);
audio_sound_gain(sound3D_bookmark2,vol,1);
audio_sound_gain(sound3D_bookmark3,vol,1);
audio_sound_gain(sound3D_bookmark4,vol,1);

audio_sound_gain(sound3D_AXpath,vol,1);