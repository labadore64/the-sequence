var list = argument[0];

var all_characters = obj_data.characters;

var sizer = ds_list_size(all_characters);

var obj = -1;

for(var i = 0; i < sizer; i++){
	obj = all_characters[|i];
	
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			if(obj.recruited){
				ds_list_add(list,obj);	
			}
		}
	}
}