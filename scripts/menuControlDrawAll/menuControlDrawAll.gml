var stack = -1;

with(menuControl){
	surface_set_target(menuControlSurface);
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,0);
	
	stack = ds_stack_create();
	
	ds_stack_copy(stack,menu_stack);
	

}

if(stack != -1){

	var stacksize = ds_stack_size(stack);
	
	var holder = -1;
	//ds_stack_pop(stack);
	
	var newstack = ds_stack_create();
	
		for(var i = 0; i < stacksize; i++){
			holder = ds_stack_pop(stack);
			if(!is_undefined(holder)){
				ds_stack_push(newstack,holder);
			}
		}

	
	stacksize = ds_stack_size(newstack);
	//if(!menuControl.delay){
	for(var i = 0; i < stacksize; i++){
		holder = ds_stack_pop(newstack);
		if(!is_undefined(holder)){
			if(active_menu != holder){
				with(holder){
					if(!do_not_draw_me){
						if(menu_draw != -1){
							if(visible){
								gpu_set_colorwriteenable(true,true,true,false)

								draw_set_alpha(1);
								script_execute(menu_draw);	
							}
						}
					}
				}
			}
		}
	}
	//}
	
	ds_stack_destroy(stack);
	ds_stack_destroy(newstack);
	
	surface_reset_target();
	
}