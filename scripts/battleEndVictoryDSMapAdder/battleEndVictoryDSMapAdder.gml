var map = argument[0];
var key = argument[1];
var value = argument[2];

var get = map[? key];

if(!is_undefined(get)){
	value += get;
	ds_map_replace(map,key,value);
} else {
	ds_map_add(map,key,value);
}