var instanceee = -1;

if(!instance_exists(menuCharacterSwitchPartyItems)){
	instanceee = instance_create(0,0,menuCharacterSwitchPartyItems)	
}
menuControlForceDrawMenuBackground();

if(instanceee != -1){
	if(object_index == menuCharacterSwitchRecruitSub){
		with(menuCharacterSwitchRecruit){
			instanceee.character = recruit_list[|menupos]
		}
	} else {
		with(menuCharacterSwitchParty){
			instanceee.character = obj_data.party[|menupos]
		}
	}
}