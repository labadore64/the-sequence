var birdcornerx1 = 0;
var birdcornery1 = 0;
var birdcornerx2 = 0;
var birdcornery2 = 0;

with(CameraOverworld){
	birdcornerx1 = bird_cornerx1
	birdcornery1 = bird_cornery1 
	birdcornerx2 = bird_cornerx2 
	birdcornery2 = bird_cornery2 
}

with(prop_bird_parent){
	if(x > birdcornerx1 && x < birdcornerx2){
		if(y > birdcornery1 && y < birdcornery2){
			addBird(name);
		}
	}
}
