//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);
stringer = string_replace_all(stringer,"@1",targe.name);

battleAttackInputAddString(stringer);

var stringer = battletext[|1];

stringer = string_replace_all(stringer,"@1",targe.name);

//do healing effect
var heal_per = objdata_moves.effect_percent[attack];

var heal_amount = floor(targe.hp.base * heal_per) + targe.hp.current;

if(targe.hp.base < heal_amount){
	heal_amount = targe.hp.base;
	stringer = battletext[|2];
	stringer = string_replace_all(stringer,"@0",chara.name);
	stringer = string_replace_all(stringer,"@1",targe.name);
} else {
	stringer = string_replace_all(stringer,"@0",chara.name);
	stringer = string_replace_all(stringer,"@1",targe.name);
	stringer = string_replace_all(stringer,"@2",string(floor(heal_per*100)));
}
battlerHPChange(targe,heal_amount)

battleAttackInputAddString(stringer);


ds_list_destroy(battletext);