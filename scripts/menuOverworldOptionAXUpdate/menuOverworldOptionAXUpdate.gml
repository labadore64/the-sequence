//updates the option based on direction.
//-1 = left, 1 = right
var list_to_check = -1;
var curpos = 0;

if(menupos == 1){
	//speed
	list_to_check=text_speed_strings;
	curpos = global.AXVisionEnabled
} else if(menupos == 0){
	//color	
	list_to_check=text_color_strings;
	curpos = round(global.AXVolume*10);
} else if (menupos == 2){
	//font	
	list_to_check = text_speed_strings;
	curpos = global.AXCornerSound;
} else if (menupos == 3) {
	//sound	
	list_to_check = text_speed_strings;
	curpos = !global.AXNotify
}

var directionz = argument[0];
var list_size = ds_list_size(list_to_check);

if(directionz == -1){
	//if moving left	
	curpos--;
	if(curpos < 0){
		if(wrapz[menupos]){
			curpos = list_size-1;
		} else {
			curpos = 0;	
		}
	}
} else if (directionz == 1){
	//if moving right	
	curpos++;
	if(curpos > list_size-1){
		if(wrapz[menupos]){
			curpos = 0;	
		} else {
			curpos = list_size - 1;	
		}
	}
}
//finally set values accordingly
if(menupos == 2){
	global.AXCornerSound = curpos;
	tts_say(text_speed_strings[|curpos])
} else if (menupos == 0){
	global.AXVolume = curpos*.1;
	tts_say(string(curpos))
} else if (menupos == 1){
	global.AXVisionEnabled = curpos;
	tts_say(text_speed_strings[|curpos])
} else if (menupos == 3){
	global.AXNotify = !curpos
}
curposs[1] = global.AXVisionEnabled
curposs[0] = round(global.AXVolume*10);
curposs[2] = global.AXCornerSound
curposs[3] = !global.AXNotify

soundSFXVolume(global.sfxVolume);
soundAXVolume(global.AXVolume);
soundMusicVolume(global.musicVolume);

menuParentUpdateLookAndFeel()
if(menupos != 3){
	soundfxPlay(sound_select)
}