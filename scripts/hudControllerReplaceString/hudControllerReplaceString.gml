// replaces a single string in the hud controller
var key = argument[0];
var stringer = argument[1];

var replacestring = hudControllerGetData(key);

var str_size = string_length(stringer);
var position = string_pos("{{"+key + "}}",stringer);
var initpos = position;
if(position > 0){
	while(position <= str_size && position > 0){
		while(!(
				string_char_at(stringer,position) == "}" &&
				string_char_at(stringer,position+1) == "}"
			)){
			position++;
		}

		var val = string_replace_all(string_copy(stringer,initpos+2,(position-initpos-2))," ","");

		stringer=string_delete(stringer,initpos,((position-initpos+2)));
		stringer=string_insert("[["+replacestring+"]]",stringer,initpos);
		position = string_pos("{{"+key + "}}",stringer);
		initpos = position;
		str_size = string_length(stringer);
	}
}

return stringer;