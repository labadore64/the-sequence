var fname = argument[0];
var section_name = "talk"
var talk_string = argument[1];

ini_open(fname);

var i = 0;

if(ini_section_exists(section_name)){
    while(true){
        if(!ini_key_exists(section_name, talk_string + string(i) + "_0" )){
            break;
        }
        i++
    }
}

return i;