	//draw extra stuff, add this into a function later
	//draw cash
	
	if(draw_money){
		draw_set_color(global.textColor)
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(400,130,global.langini[6,0] + ":",2,2,0)
		draw_set_halign(fa_right)
		with(obj_data){
			draw_text_transformed_ratio(780,130,"$" + string(money),2,2,0)
		}
		draw_set_halign(fa_left)
	}
	
	if(draw_time){
		//draw time
		var hour = 0;
		var minute = 0;
	
		with(obj_data){
			minute = floor(time_current/60);
			hour = floor(minute/60);
			minute = minute mod 60;
		
			if(hour > 99){
				hour = 99	
			}
		
			if(hour < 10){
				hour = "0" + string(hour);	
			}
		
			if(minute < 10){
				minute = "0" + string(minute);	
			}
		}
	
		draw_text_transformed_ratio(400,160,global.langini[6,1] + ": ",2,2,0)
	
		draw_set_halign(fa_center)
		var adjust_left = 55;
	
		if(flash){
			draw_text_transformed_ratio(690+adjust_left,160," " ,2,2,0)
			//draw_text_transformed_ratio(780,160,string(hour) + " " + string(minute),2,2,0)
		} else {
			draw_text_transformed_ratio(690+adjust_left,160,":" ,2,2,0)
			//draw_text_transformed_ratio(780,160,string(hour) + ":" + string(minute),2,2,0)
		}
		draw_set_halign(fa_right)
		draw_text_transformed_ratio(685+adjust_left,160,string(hour),2,2,0)
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(695+adjust_left,160,string(minute),2,2,0)
	
	}