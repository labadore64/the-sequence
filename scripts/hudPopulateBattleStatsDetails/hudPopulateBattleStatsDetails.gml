// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from

if(obj < 1000){
	if(obj > -1 && obj < objdata_character.data_size){
		obj = obj_data.characters[|obj];	
	}
}

if(instance_exists(obj)){
	if(obj.object_index == Battler){
		if(obj.overworld_character > -1){
			hudControllerAddData(entryname + "element",string(obj.element));
		
			// name and character
			hudControllerAddData(entryname + "name",string(obj.name));
			hudControllerAddData(entryname + "desc",objdata_character.battle_desc[obj.overworld_character.character]);
			hudControllerAddData(entryname + "axdesc",objdata_character.ax_desc[obj.overworld_character.character]);
		
			// stats
			hudControllerAddData(entryname + "phy_power",string(obj.phy_power.current));
			hudControllerAddData(entryname + "phy_guard",string(obj.phy_guard.current));
			hudControllerAddData(entryname + "mag_power",string(obj.mag_power.current));
			hudControllerAddData(entryname + "mag_guard",string(obj.mag_guard.current));
			hudControllerAddData(entryname + "spe_power",string(obj.spe_power.current));
			hudControllerAddData(entryname + "spe_guard",string(obj.spe_guard.current));
		
			hudControllerAddData(entryname + "hp",string(obj.hp.current));
			hudControllerAddData(entryname + "hp_max",string(obj.hp.base));
			hudControllerAddData(entryname + "hp_percent",string(floor(100*obj.hp.current/obj.hp.base)));
	
			hudControllerAddData(entryname + "mp",string(obj.mp.current));
			hudControllerAddData(entryname + "mp_max",string(obj.mp.base));
			hudControllerAddData(entryname + "mp_percent",string(floor(100*obj.mp.current/obj.mp.base)));

			// items and tablets
			for(var i = 0; i < 5; i++){
				hudControllerAddData(entryname + "item"+string(i),string(obj.item[i]));
			}
		
			for(var i = 0; i < 3; i++){
				hudControllerAddData(entryname + "tablet"+string(i),string(obj.tablet[i]));
			}
		}
	}
}