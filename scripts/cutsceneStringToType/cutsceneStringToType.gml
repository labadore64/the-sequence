var stringer = string_lower(argument[0]);

if(stringer == "none"){
	return CUTSCENE_TYPE_NONE;	
} else if (stringer == "dialog"){
	return CUTSCENE_TYPE_DIALOG;	
} else if (stringer == "exit"){
	return CUTSCENE_TYPE_EXIT;	
} else if (stringer == "text"){
	return CUTSCENE_TYPE_TEXT;	
} else if (stringer == "move_character"){
	return CUTSCENE_TYPE_MOVE_CHARACTER;	
} else if (stringer == "move_camera"){
	return CUTSCENE_TYPE_MOVE_CAMERA;	
} else if (stringer == "move_character_and_camera"){
	return CUTSCENE_TYPE_MOVE_CHARACTER_AND_CAMERA;	
} else if (stringer == "graphic"){
	return CUTSCENE_TYPE_GRAPHIC;
} else if (stringer == "recruit"){
	return CUTSCENE_TYPE_RECRUIT;
} else if (stringer == "item_get"){
	return CUTSCENE_TYPE_ITEM_GET;
}
 else if (stringer == "battle"){
	return CUTSCENE_TYPE_BATTLE;
}
 else if (stringer == "check_event"){
	return CUTSCENE_TYPE_CHECK_EVENT;
}
 else if (stringer == "check_party"){
	return CUTSCENE_TYPE_CHECK_PARTY;
}
 else if (stringer == "check_item"){
	return CUTSCENE_TYPE_CHECK_ITEM;
} else if (stringer == "check_inventory"){
	return CUTSCENE_TYPE_CHECK_INVENTORY;
}
 else if (stringer == "music"){
	return CUTSCENE_TYPE_MUSIC;
}
 else if (stringer == "wait"){
	return CUTSCENE_TYPE_WAIT;
} else if (stringer == "soundfx"){
	return CUTSCENE_TYPE_SOUNDFX;	
} else if (stringer == "script"){
	return CUTSCENE_TYPE_SCRIPT;	
}

return CUTSCENE_TYPE_NONE;