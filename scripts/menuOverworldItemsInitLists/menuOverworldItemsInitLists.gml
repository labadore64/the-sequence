menu_option = 0;

menu_option_names = ds_list_create();

ds_list_add(menu_option_names,"Other","Equip","Heal","Battle","Field","Books","Food","Craft","Decor");
var len = ds_list_size(menu_option_names);

for(var i=0; i < len; i++){
	menu_option_names_array[i] = menu_option_names[|i];	
}

other_list = ds_list_create();
equip_list = ds_list_create();
heal_list = ds_list_create();
battle_list = ds_list_create();
field_list = ds_list_create();
book_list = ds_list_create();
food_list = ds_list_create();
craft_list = ds_list_create();
decor_list = ds_list_create();

other_list_quant = ds_list_create();
equip_list_quant = ds_list_create();
heal_list_quant = ds_list_create();
battle_list_quant = ds_list_create();
field_list_quant = ds_list_create();
book_list_quant = ds_list_create();
food_list_quant = ds_list_create();
craft_list_quant = ds_list_create();
decor_list_quant = ds_list_create();

list_of_lists = ds_list_create();
list_of_lists_quant = ds_list_create();

menu_tableft = menuOverworldItemsLeft;
menu_tabright = menuOverworldItemsRight;

menu_left = -1;
menu_right = -1;

ds_list_add(list_of_lists,other_list,equip_list,heal_list,battle_list,field_list,book_list,food_list,craft_list,decor_list);
ds_list_add(list_of_lists_quant,other_list_quant,equip_list_quant,heal_list_quant,battle_list_quant,field_list_quant,book_list_quant,food_list_quant,craft_list_quant,decor_list_quant);
