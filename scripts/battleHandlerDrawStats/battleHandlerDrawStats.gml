//draw bg for menus

gml_pragma("forceinline");

if(!surface_exists(stat_surf)){
	stat_surf = surface_create_access(800*global.scale_factor,stat_bar_height*global.scale_factor)	
	stat_redraw = true;
} else {
	if(ScaleManager.updated){
		surface_resize(stat_surf,800*global.scale_factor,stat_bar_height*global.scale_factor)
		stat_redraw = true;
	}
}
var spacer = 140;
var xx_init = 380;
var yy_init = 10;

if(stat_redraw){
	surface_set_target(stat_surf);
	gpu_set_colorwriteenable(true,true,true,true)
	draw_clear_alpha(c_black,1)
	draw_set_alpha(1)
	draw_set_color(c_black)

	var oz = -1;
	gpu_set_colorwriteenable(true,true,true,false)
	for(var i =0; i < 3; i++){
		oz = draw_char[i];
		if(oz > -1){
			if(instance_exists(oz)){
				battleHandlerDrawCharacterInfo(oz,xx_init+spacer*i,yy_init);	
			}
		}
	}

	surface_reset_target();
	stat_redraw = false;
}
draw_set_alpha(1)
gpu_set_colorwriteenable(true,true,true,false)
draw_surface(stat_surf,global.display_x,global.display_y);
gpu_set_colorwriteenable(true,true,true,true)

// draw selection
	gpu_set_blendmode(bm_add)

	draw_set_alpha(char_select_value*.75)
	draw_set_color($CCCCCC)
	//first selection
	if(character_select[0]){
	draw_rectangle_ratio(360+4,0,502,114,false)
	}
	//second selection
	if(character_select[1]){
	draw_rectangle_ratio(360+spacer+6,0,502+spacer,105,false)
	}
	//third selection

	if(character_select[2]){
	draw_rectangle_ratio(360+6+spacer*2,0,502+spacer*2,105,false)
	}
	gpu_set_blendmode(bm_normal)

	draw_set_alpha(1)


draw_set_alpha(1)
//draw HP

//with(Battler){

//}