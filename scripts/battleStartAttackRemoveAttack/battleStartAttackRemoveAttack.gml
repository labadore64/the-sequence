with(battleAttackInput){
	if(character.object_index == Battler){
		if(battlerHasFilter(character,"talkcancel") ||
			battlerHasFilter(character,"stone") ||
			battlerHasFilter(character,"sleep")){
		
			instance_destroy();	
		}
	} else {
		// for some reason the attack is corrupted so kill it
		instance_destroy();	
	}
}