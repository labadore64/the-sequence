
	//updates the option based on direction.
	//-1 = left, 1 = right
	var list_to_check = -1;
	var curpos = 0;

	if(menupos == 0){
		//speed
		list_to_check=text_speed_strings;
		curpos = round(global.sfxVolume*10)
	} else if(menupos == 1){
		//color	
		list_to_check=text_color_strings;
		curpos = round(global.musicVolume*10);
	} else if (menupos == 2){
		//font	
		list_to_check = text_font_strings;
		curpos = round(global.envVolume*10);
	} else {
		//sound	
		list_to_check = text_sound_strings;
		curpos = 0
	}

	var directionz = argument[0];
	var list_size = ds_list_size(list_to_check);

	if(directionz == -1){
		//if moving left	
		curpos--;
		if(curpos < 0){
			if(wrapz[menupos]){
				curpos = list_size-1;
			} else {
				curpos = 0;	
			}
		}
	} else if (directionz == 1){
		//if moving right	
		curpos++;
		if(curpos > list_size-1){
			if(wrapz[menupos]){
				curpos = 0;	
			} else {
				curpos = list_size - 1;	
			}
		}
	}
	//finally set values accordingly
	if(menupos == 0){
		global.sfxVolume = curpos*.1;
		tts_say(string(curpos))
	} else if (menupos == 1){
		global.musicVolume = curpos*.1;
		tts_say(string(curpos))
	} else if (menupos == 2){
		global.envVolume = curpos*.1;
		tts_say(string(curpos))
	} else if (menupos == 3){
		//global.contrast = curpos*.01;
	}
	curposs[0] = round(global.sfxVolume*10);
	curposs[1] = round(global.musicVolume*10);
	curposs[2] = round(global.envVolume*10)
	curposs[3] = 0

	soundSFXVolume(global.sfxVolume);
	soundAXVolume(global.AXVolume);
	soundMusicVolume(global.musicVolume);

	menuParentUpdateLookAndFeel()
	if(menupos != 3){
		soundfxPlay(sound_select)
	}
