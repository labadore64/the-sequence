var targe = argument[0];

if(!target_self){
	if(!battlerIsAlive(targe)){
		targe = battleStartAttackSetNewTarget(targe);
		target = targe;
	}
} else {
	target = character;	
}



	if(script_battle != -1 && !has_executed){
		//note: all battle scripts must accept character and target
		script_execute(script_battle,character,targe);	
		has_executed = true;
	}

if(targe > 0){
	var multiplier = argument[1];



	if(damage > 0){
		var stat_power = 0;
		var stat_guard = 0;
		
		var canDo = battleStartCanDoAttackEffect();
	
		if(canDo){
			if(move_type == MOVE_TYPE_PHYSICAL){
				stat_power = character.phy_power.current;
				stat_guard = targe.phy_guard.current;
				if(battlerHasFilter(character,"damp")){
					stat_power = stat_power * .5	
				}
				if(battlerHasFilter(targe,"damp")){
					stat_guard = stat_guard * .5	
				}
			} else {
				stat_power = character.mag_power.current;
				stat_guard = targe.mag_guard.current;
				if(battlerHasFilter(character,"dry")){
					stat_power = stat_power * .5	
				}
				if(battlerHasFilter(targe,"dry")){
					stat_guard = stat_guard * .5	
				}
			}
			
			if(battlerHasFilter(character,"rage")){
				stat_power = stat_power*2;	
			}
			
			if(battlerHasFilter(targe,"rage")){
				stat_guard = stat_guard*.5	
			}
	
			damage_calc = battleStartAttackDoDamage(character,targe,stat_power,stat_guard,element);
	
			if(damage_calc > targe.hp.base){
				damage_calc = targe.hp.base;	
			}

			var damage_string = battleStartAttackGetDamageString(damage_calc,targe)
	
			var set_string = "@0 took @1 damage!"
	
			set_string = string_replace_all(set_string,"@0",targe.name);
			set_string = string_replace_all(set_string,"@1",damage_string);
	
			battleAttackInputAddString(set_string);
		
			if(damage_type_multiplier > 1){
				var set_string = "@0 took extra damage!"
				set_string = string_replace_all(set_string,"@0",targe.name);
				battleAttackInputAddString(set_string);	
			} else if (damage_type_multiplier < 1){
				var set_string = "@0 resisted the attack!"
				set_string = string_replace_all(set_string,"@0",targe.name);
				battleAttackInputAddString(set_string);	
			}
		
			if(battlerHasFilter(targe,"Talk")){
				with(Battler){
					battlerRemoveFilter(id,"Talk");	
					battlerRemoveFilter(id,"Listen");	
				}
				targe.trigger_drop_talk = false;
				targe.trigger_drop_betray = true;
				var set_string = "@0 was betrayed! It's furious!"
				removeTalkedMonster(targe.character);
				//boost stats in retaliation!
				var turns = 5;
			
				battleStatChangeAdd(targe,BATTLE_STAT_ATTACK,1.5,turns);
				battleStatChangeAdd(targe,BATTLE_STAT_MAGIC,1.5,turns);
				battleStatChangeAdd(targe,BATTLE_STAT_DEFENSE,2,turns);
				battleStatChangeAdd(targe,BATTLE_STAT_RESISTANCE,2,turns);
				battleStatChangeAdd(targe,BATTLE_STAT_MP_RECOVER,5,turns);
				battlerMPChange(targe,targe.mp.base)
				battlerMPChange(targe,targe.hp.current*2)
				//end boost stats
			
				set_string = string_replace_all(set_string,"@0",targe.name);
				battleAttackInputAddString(set_string);		
				with(targe){
					battlerDeathZoom(20);
				}
				/*
				set_string = "@0's stats are boosted!"
			
				set_string = string_replace_all(set_string,"@0",targe.name);
				battleAttackInputAddString(set_string);		
				*/
			}
		
			targe.damage_calc = damage_calc;
			
			//do metal
			var reflect = 0;
			
			if(BattleHandler.reflect_attack){
				if(!targe.is_enemy){
					reflect = .5;
				}
			}
			
			if(battlerHasFilter(targe,"mirror")){
				reflect = .33;
			}
			
			if(battlerHasFilter(character,"curse")){
				reflect = .25;	
			}
			
				
			var reflect_damage = ceil(damage_calc*reflect);
					
			if(reflect_damage > 0){		
				character.damage_calc = reflect_damage;
			}
			if(reflect > 0){
				var stringerr = "@0 took @1% damage, too!";
				
				var percent = ceil((character.damage_calc*100)/character.hp.base);
				
				stringerr = string_replace_all(stringerr,"@0",character.name)
				stringerr = string_replace_all(stringerr,"@1",string(percent))
			
				battleAttackInputAddString(stringerr);
			}
				
		
			battleStartAttackDoFilter(character,targe);
			
			if(targe.damage_calc >= targe.hp.current){
				character_defeated = true;
				var stringerr = "@0 was defeated!";
			
				stringerr = string_replace_all(stringerr,"@0",targe.name)
			
				battleAttackInputAddString(stringerr);
				//if(lunge_animation != -1){
					with(targe){
						death_zoom = 10;
					}
				//}
			}
			
			if(character.damage_calc >= character.hp.current){
				var stringerr = "@0 was defeated!";
			
				stringerr = string_replace_all(stringerr,"@0",targe.name)
			
				battleAttackInputAddString(stringerr);
			}
		
			if(damage > 0){
				if(battlerIsAlive(targe)){
					battleStartAttackDamageEffect()
				} else if (battlerIsAlive(character)){
					battleStartAttackAttackerEffect();
				}
			}
		
			if(move_type == MOVE_TYPE_PHYSICAL){
				if(damage > 0){
					if(battlerIsAlive(targe)){
						battleStartAttackContactEffect();
					}
				}
			} else {
				if(damage > 0){
					if(battlerIsAlive(targe)){
						battleStartAttackMagicEffect();	
					}
				}
			}
		
			battleDamageAnimation(targe,animation_length,animation_knockback)
		}
	}
}