/// @description Insert description here
// You can write your code in this editor

if(active){
	if(!surface_exists(surf)){
		surf = surface_create(global.letterbox_w,global.letterbox_h);	
		surface_update = true
	} else {
		if(ScaleManager.updated){
			surface_resize(surf,global.letterbox_w,global.letterbox_h)	
			surface_update = true
		}
	}

	var spacee_x = space_x
	var spacee_y = space_y

	if(surface_update){
		surface_set_target(surf)
		gpu_set_colorwriteenable(true,true,true,true);

		for(var i = 0; i < braille_rows; i++){
			for(var j = 0; j < braille_columns; j++){
					//draw the boundary box
					draw_set_color(global.textColor);
					var z = 5;

					var x_pos = start_x + space_x*j + space_x*.22;
					var y_pos = start_y + space_y*(i) +spacee_y*.5;
					var v_size = spacee_y-z;
					var h_size = spacee_x-z;

					draw_rectangle_center_noratio(x_pos,y_pos,h_size+z,v_size+z);

					draw_set_color(c_black)

					draw_rectangle_center_noratio(x_pos,y_pos,h_size,v_size);
			
				with(display_cell[i,j]){
					brailleCharacterDraw();
				}
			}
		}

		surface_reset_target()
	}

	gpu_set_colorwriteenable(true,true,true,false);

	draw_set_alpha(.75)
	draw_set_color(c_black)
	draw_rectangle_ratio(0,0,800,600,false);

	draw_set_alpha(1)
	draw_surface_ext(surf,global.display_x,global.display_y,1,1,0,global.textColor,1);

	gpu_set_colorwriteenable(true,true,true,false)
	if(drawTitle){
		drawOutlineText(20,20,title,titleFontSize,titleFontSize,0,title_color)
	}
	//draw_set_color(subtitle_color)

	//draw subtitle
	if(drawSubtitle){
		drawOutlineText(20,75,subtitle,subtitleFontSize,subtitleFontSize,0,subtitle_color)
	}
	gpu_set_colorwriteenable(true,true,true,true)

	//draw selected rectangle
	var z = 5;

	var x_pos = start_x + space_x*selected_column + space_x*.22;
	var y_pos = start_y + space_y*(selected_row) +spacee_y*.5;
	var v_size = spacee_y-z;
	var h_size = spacee_x-z;

	draw_set_color(global.textColor);
	draw_set_alpha(select_alpha)
	draw_rectangle_center(x_pos,y_pos,h_size,v_size);

	draw_set_alpha(1);

		var xx = 710
		var yy = 560
	
		draw_set_halign(fa_right)
		draw_text_transformed_ratio(xx-75,yy,"Collected:",3,3,0)
		draw_text_transformed_ratio(xx-7,yy,string(tablet_seen),3,3,0)
		draw_set_halign(fa_center)
		draw_text_transformed_ratio(xx,yy,"/",3,3,0)
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(xx+7,yy,string(tablet_total),3,3,0)
}