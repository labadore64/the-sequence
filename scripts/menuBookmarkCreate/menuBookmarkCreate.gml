event_inherited();

bookmark_id = -1;

if(!gamepad_is_connected(global.gamepad)){
	menuAddOption(global.langini[0,53],global.langini[1,53],menuBookmarkRename)
}
menuAddOption(global.langini[0,54],global.langini[1,54],menuBookmarkDelete)

menuParentSetTitle(global.langini[2,20])
menuParentSetSubtitle("")

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetDrawGradient(false)

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

x = 400
y = 300

menuParentSetBoxWidth(600)

menuParentUpdateBoxDimension();

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(c_white);



menu_left = menuParentMoveUp
menu_right = menuParentMoveDown;

menu_down = menu_right;
menu_up = menu_left;

menu_input_text = menuBookmarkInputText

tts_clear();

newalarm[2] = 1