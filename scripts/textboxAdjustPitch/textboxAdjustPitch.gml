
var speech_string = argument0;
var index = argument1;

if(string_char_at(speech_string,index) == "["){
    talking_sound = false
		audio_stop_sound(text_sound1);
		audio_stop_sound(text_sound2);
		audio_stop_sound(text_sound3);
		audio_stop_sound(text_sound4);
} else if(string_char_at(speech_string,index) == "]"){
    talking_sound = true
}

var pitchOfSound = 1;

var check1 = string_lower(string_char_at(speech_string,index+1));
var check2 = string_lower(string_char_at(speech_string,index+2));
var check3 = string_lower(string_char_at(speech_string,index+3));
var check4 = string_lower(string_char_at(speech_string,index+4));
var check5 = string_lower(string_char_at(speech_string,index+5));

if(check1 == "?"){
	pitchOfSound = 1.2
} else if(check2 == "?"){
	pitchOfSound = 1.16
} else if(check3 == "?"){
	pitchOfSound = 1.12
} else if(check4 == "?"){
	pitchOfSound = 1.08
} else if(check5 == "?"){
	pitchOfSound = 1.04
}

if(check1 == "."){
	pitchOfSound = .8
} else if(check2 == "."){
	pitchOfSound = .84
} else if(check3 == "."){
	pitchOfSound = .88
} else if(check4 == "."){
	pitchOfSound = .92
} else if(check5 == "."){
	pitchOfSound = .96
}

if(check1 == ","){
	pitchOfSound = .9
} else if(check2 == ","){
	pitchOfSound = .92
} else if(check3 == ","){
	pitchOfSound = .94
} else if(check4 == ","){
	pitchOfSound = .96
} else if(check5 == ","){
	pitchOfSound = .98
}

if(check1 == "!"){
	pitchOfSound = 1.15
} else if(check2 == "!"){
	pitchOfSound = 1.12
} else if(check3 == "!"){
	pitchOfSound = 1.09
} else if(check4 == "!"){
	pitchOfSound = 1.06
} else if(check5 == "!"){
	pitchOfSound = 1.03
}

var pitchChanger = (sin(index/flux_rate)*flux)+1;

var pitchInWord = 1;

if(pitchOfSound == 1){
	if(check1 == " "){
		pitchInWord = .98
	} else if(check2 == " "){
		pitchInWord = .99
	} else if(check3 == " "){
		pitchInWord = .995
	}
}

pitchOfSound = pitchOfSound * pitchInWord*pitch_base;

return pitchOfSound;