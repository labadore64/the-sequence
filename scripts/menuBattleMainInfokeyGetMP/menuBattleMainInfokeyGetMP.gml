var chara = argument[0]

var returner = "";

if(is_undefined(chara) || chara == -1){
	returner = ""	
} else {
	var mps = floor(100*chara.mp.current/chara.mp.base);
	
	var returner = string(mps) + "% MP " + chara.name;
}

return returner;