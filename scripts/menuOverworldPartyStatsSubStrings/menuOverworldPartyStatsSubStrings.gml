var namerr = "";
with(menuOverworldPartyStats){
	namerr = character;	
}

character_name = namerr.name
character = namerr;
menuParentClearOptions()
lock = false;
menuAddOption(global.langini[0,19],replaceStringWithVars(global.langini[1,19],character.name),menuOverworldStatsSubMovesScript)//menuOverworldStatsSubMovesScript)
if(!noItems){
	menuAddOption(global.langini[0,20],replaceStringWithVars(global.langini[1,20],character.name),menuOverworldPartyTablet)
	menuAddOption(global.langini[0,21],replaceStringWithVars(global.langini[1,21],character.name),menuOverworldStatsSubItemsScript)
	if(character.character != 0){
		menuAddOption("Talk",replaceStringWithVars("Talk to @1.",character.name),menuOverworldStatsSubTalkScript)
	}
}
menuAddOption(global.langini[0,22],replaceStringWithVars(global.langini[1,22],character.name),menuOverworldStatsSubBioScript)

SHORTHAND_CANCEL_OPTION


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentSetTitle(replaceStringWithVars(global.langini[4,6],character_name));
menuParentSetSubtitle("");

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

menuParentSetGradientHeight(0)
menuParentSetOptionYOffset(10);
menuParentSetOptionSpacing(40)
draw_width = 200

menu_size = menuGetSize();

x = 640
y = 300
draw_height = 40 * menu_size

menu_draw = menuOverworldPartyStatsSubDraw;

menu_left = menuOverworldPartySubLeft;
menu_right = menuOverworldPartySubRight;
menu_up = menu_left;
menu_down = menu_right;

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

menuParentUpdateBoxDimension();

portrait = instance_create_depth(0,0,0,drawPortrait)

portrait.character = character.character
