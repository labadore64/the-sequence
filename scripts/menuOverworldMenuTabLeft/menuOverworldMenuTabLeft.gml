if(!instance_exists(TextBox) &&
	!instance_exists(DialogHandler) && 
	!instance_exists(keyboardSetKey) &&
	!instance_exists(controllerSetKey)){
	if(menuControl.active_menu.menu_left == -1 && !menuControl.active_menu.ignore_tab){
		menupos--;

		if(menupos < 0){
			menupos = array_length_1d(menu_type)-1;
		}
		running = false;
		newalarm[2] = 2

		with(menuControl){
			please_draw_black = true;	
		}
		moving = OVERWORLD_MENU_MOVING_TIME;
		menuOverworldMenuClearMenus();
	}
}