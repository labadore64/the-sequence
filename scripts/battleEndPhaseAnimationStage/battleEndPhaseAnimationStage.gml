
battleAnimReturnAll();
battleTextDisplayClearText();
		
with(battleTextDisplay){
	counter = 0;
	display_number = 0;
}
		
current_end_phase = ds_queue_dequeue(end_phase_stuff);

if(state != BATTLE_PHASE_STATE_ANIMATE){
	if(!is_undefined(current_end_phase)){
		if(!BattleHandler.fleeing){
			battleEndPhaseDoAnimate(current_end_phase.end_id,current_end_phase.target);
		}
	}
}
		
state = BATTLE_PHASE_STATE_ANIMATE;