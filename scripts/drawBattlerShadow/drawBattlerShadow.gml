	if(!defeat_animation_trigger){
		if(draw_shadow){
			if(!global.disableShader){
				shader_set(shd_shadow_img)
				shader_set_uniform_f(BattleHandler.shd_shadow_alpha, image_alpha*hide_alpha*.75);
				var uv = sprite_get_uvs(sprite_index, spr_counter);
				shader_set_uniform_f(BattleHandler.shd_shadow_uniUV , uv[1], uv[3]);
				draw_sprite_ext(sprite_index,spr_counter,x-flee_dist,y-(10-shadow_y),image_xscale*sprite_scale,image_yscale*sprite_scale*-.25,0,image_blend,image_alpha*hide_alpha);
				shader_reset();
			}
		}
	}