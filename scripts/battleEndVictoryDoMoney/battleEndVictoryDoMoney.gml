//var testWindow = battleWindowStore("This is a test window. Let's see how it does.",300,200);

//ds_queue_enqueue(windows,testWindow);

//first, do mandatory drop.
//This is the normal drop, and will have a quantity ranging from 1-5 depending
//on the party level.

var level_test = clamp(ceil(BattleHandler.partyLevel/20),1,5);

var quantity = irandom_range(1,level_test);

//select randomly which enemy will have the drop.

var countie = ds_list_size(BattleHandler.opponents);

var money_drop = 0;

for(i = 0; i < countie; i ++){
	var obj = BattleHandler.opponents[|i];
	if(!obj.bribed && obj.character > -1){
		money_drop += floor(irandom_range(
							objdata_enemy.min_money[obj.character],
							objdata_enemy.max_money[obj.character]
							)*random_range(.1,.33)
						)
	}
}

if(money_drop > 0){
	stringer = "You found:\n$@0"
	stringer = string_replace_all(stringer,"@0",string(money_drop));
	BattleHandler.money+=money_drop;
	testWindow = battleWindowStore(stringer,300,100);

	ds_queue_enqueue(windows,testWindow);
}