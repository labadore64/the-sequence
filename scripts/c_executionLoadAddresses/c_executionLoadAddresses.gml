//first, jump to the header address
file_bin_seek(file, header_address);

//check the next four bytes.
var checkbytes;

checkbytes[0] = C_START_ADDRESS_HEADER1;
checkbytes[1] = C_START_ADDRESS_HEADER2;
checkbytes[2] = C_START_ADDRESS_HEADER3;
checkbytes[3] = C_START_ADDRESS_HEADER4;

var checkcounter = 0;

var actualBytes = c_readbytes(file,4);

var end_spacer;

end_spacer[0] = C_EOF1;
end_spacer[1] = C_EOF2;
end_spacer[2] = C_EOF3
end_spacer[3] = C_EOF4

var array_output;
var keep_going = true;

if(c_comparebytes(checkbytes,actualBytes)){
	//start reading headers.
	while(keep_going){
		array_output = c_readbytes(file,4);
		if(c_comparebytes(array_output,end_spacer)){
			keep_going = false;	
		} else {
			ds_list_add(header_list,c_getAddressAt(array_output))
		}
	}
	
	//load headers.
	c_loadBlocks()
} else {
	c_executionThrowError("Address header is corrupt.");	
}