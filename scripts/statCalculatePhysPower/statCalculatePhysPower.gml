/// @description - returns adjusted PHYSPOWER stat

var minvalue = 10;

var multiplier = getMultiplierFromLevel(objdata_character.phy_power_grow[character],level);

var returner = ceil((minvalue + multiplier * (phy_power_base+(boost_phy_power)*BATTLE_STAT_MULTIPLIER_ITEM)));

returner += statCalculateBrailleBoost(multiplier,argument[0]);

return returner;