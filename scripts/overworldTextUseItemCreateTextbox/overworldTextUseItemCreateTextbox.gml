var canDoItem = !eventNormalCheck(event_id);
if(!instance_exists(TextBox)){
	if(instance_exists(Player)){
		if(Player.active && !Player.lock){
		
			with(Player){
				active = false;	
			}
		
			if(canDoItem){
				var ob = textboxOverworld(item_text_id)
				ob.triggered_by_player = true;
				ob.end_script = overworldTextUseItemComplete; //set to opening item menu.
			} else {
				var ob = textboxOverworld(text_box_id)
				ob.triggered_by_player = true;	
			}
		}
	}
}