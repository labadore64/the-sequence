var surf = surface;

if(!global.disableShader){
	with(CameraOverworld){
		if(shd_bc_shader_enabled){
			shader_set(shd_bright_contrast)
		    shader_set_uniform_f(shd_bc_uni_time, shd_bc_var_time_var);
		    shader_set_uniform_f(shd_bc_uni_mouse_pos, shd_bc_var_mouse_pos_x, shd_bc_var_mouse_pos_y);
		    shader_set_uniform_f(shd_bc_uni_resolution, shd_bc_var_resolution_x, shd_bc_var_resolution_y);
		    shader_set_uniform_f(shd_bc_uni_brightness_amount, -3);
		    shader_set_uniform_f(shd_bc_uni_contrast_amount,0);
			draw_surface_ext(surf,0,461,1,-.15,0,c_white,.25)
		}	
	}

	shader_reset();
}