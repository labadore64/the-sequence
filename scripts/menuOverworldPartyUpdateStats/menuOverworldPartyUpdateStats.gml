
	chaa = argument[0];
	var radd = argument[1];
if(instance_exists(chaa)){

	with(dataDrawStat){
		character = argument[0];
		boost_phy_power = character.tablet_phy_power
		boost_phy_guard = character.tablet_phy_guard
		boost_mag_power = character.tablet_mag_power
		boost_mag_guard	= character.tablet_mag_guard
		boost_spe_power = character.tablet_spe_power
		boost_spe_guard = character.tablet_spe_guard
		draw_hex_init(character,radd,character.phy_power,
					character.phy_guard ,
					character.mag_power ,
					character.mag_guard ,
					character.spe_agility ,
					character.spe_mpr);
		radius = radd;
	}

	element_sprite = menuBattleMovesGetElementSprite(chaa.element)

	if(object_index == menuOverworldPartyStats){
		translation = translation_start;
		translation_rate = translation_rate_start;
	}
}