/// @description - give an item
/// @param item
/// @param quantity

var val = string(argument[0]);
var quan = string(argument[1]);

if(real(quan) > 999){
	quan = "999";	
}

with(obj_data){
	var ss = ds_map_find_value(inventory,val);
	//if(ds_map_find_value(inventory,val)){
		if(is_undefined(ss)){
			ds_map_replace(inventory,val,quan)
		} else {
			var newval = real(quan) + real(ss);
			
			if(newval > 999){
				newval = 999	
			}
			
			ds_map_replace(inventory,val,string(newval))
		}
	//} else {
	//	ds_map_add(inventory,val,quan);
	//}
}