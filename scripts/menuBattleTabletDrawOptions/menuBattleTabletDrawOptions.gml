
	if(ds_exists(menu_name,ds_type_list)){

		draw_set_color(text_color);

		var stringer = "";
		var xx = 0;
		var yy = 0;

		//draw_set_font(global.largeFont)
		draw_set_halign(fa_center)
		draw_text_transformed_ratio(x1+555,y1+5,"Damage",1,1,0)
		draw_text_transformed_ratio(x1+740,y1+5,"Type",1,1,0)
		draw_text_transformed_ratio(x1+655,y1+5,"Element",1,1,0)
		draw_set_halign(fa_left)
		//draw_set_font(global.textFont);

		//get max number of items that can be displayed
		var drawmax = 0;
		var counter = 0;
		for(var i = toppos; i < menu_size; i++){
			xx = x1+option_x_offset;
			yy = y1+option_y_offset+option_space*counter;
			if(yy > y2-option_space){
				drawmax = i;
				break;
			}
			counter++;
			drawmax = i+1;
		}
		counter = 0;

		var current_shift = 0;

		//draw the selection box
		draw_set_alpha(select_alpha);
		draw_set_color(select_color);

		var currpos = menupos - toppos;

		yy = y1+option_y_offset+currpos*option_space - option_space*0.25;

		//scrolldown function

		while(yy > y2-option_space){
			toppos++;
	
			currpos = menupos - toppos;

			yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
		}

		//scrollup function

		while(yy < y1+gradient_height || currpos < 0){
			toppos--;
	
			currpos = menupos - toppos;

			yy = y1+option_y_offset+currpos*option_space - option_space*0.25;
		}

		draw_rectangle_ratio(x1+current_shift,yy+25,x1+((x2-x1)*select_percent),yy+option_space,false)
	
		if(main_index > -1){
			draw_set_alpha(.25)
			currpos = main_index;

			yy = y1+option_y_offset+currpos*option_space - option_space*0.25;

			draw_rectangle_ratio(x1+current_shift,yy+25,x1+((x2-x1)*select_percent),yy+option_space,false)
		}
		draw_set_alpha(1)
		var coll;
		for(var i = toppos; i < drawmax; i++){
			if(moves[|i] > 0){
			
				if(numbering[i] != -1){
					current_shift = shift_value;
				} else {
					current_shift = 0;	
				}
			
				if(move_cando[|i]){
					coll = global.textColor
				} else {
					coll = $444444;
				}
			
				draw_set_color(coll);
			
				if(!is_undefined(stringer)){
					xx = x1+option_x_offset;
					yy = 25+y1+option_y_offset+option_space*counter;

					stringer = menu_name_array[ i];
	
					if(numbering[i] != -1){
						draw_text_transformed_ratio(20,yy-10,string(numbering[i]+1),4,4,0)	
					}
	

					draw_text_transformed_ratio(current_shift+xx,yy,stringer,text_size,text_size,0)	

					var mov = 360;
				
					if(move_list_type[|i] == MOVE_TYPE_PHYSICAL){
						draw_sprite_extended_ratio(spr_typePhys,0,mov+xx+370,yy+10,1,1,0,c_white,1)
					} else {
						draw_sprite_extended_ratio(spr_typeMag,0,mov+xx+370,yy+10,1,1,0,c_white,1)
					}

					var srpite = moves_type[|i];
					if(srpite > -1){
						//element
						draw_sprite_extended_ratio(srpite,0,mov+xx+285,yy+10,1,1,0,c_white,1)
					}
		
					draw_set_halign(fa_center)
					//draw_text_transformed_ratio(mov+xx+290,yy,string(moves_targets[|i]),text_size,text_size,0)
					menuBattleMovesDrawBarColor(xx+mov+150,yy+5,75,8,moves_damage[|i],coll);
					
					draw_set_halign(fa_left)
					//draw_text_transformed_ratio(xx,yy,stringer,text_size,text_size,0)

				}
			
				with(braille_display[i]){
					gpu_set_colorwriteenable(true,true,true,true)
					brailleCharacterDrawNotStupid();	
					draw_surface(surf,
						global.display_x+current_shift*global.scale_factor+x*global.scale_factor,
						17*global.scale_factor+global.display_y+y*global.scale_factor);
					gpu_set_colorwriteenable(true,true,true,false)
				}
			}
			counter++;
		}
	

		draw_set_color(global.textColor);
		draw_set_halign(fa_left)
		var xxx = 25;
		var yyy = 450;
	
		var newsize = 3;
		draw_text_transformed_ratio(xxx,yyy,"Input:",newsize,newsize,0)
		draw_set_halign(fa_right)
		draw_text_transformed_ratio(xxx+250,yyy,selected_string,newsize,newsize,0)
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(xxx+340,yyy,"Energy Cost:",newsize,newsize,0)
		menuBattleMovesDrawBarColor(xxx+640,yyy+5,120,15,total_mpcost,global.textColor)

	}
