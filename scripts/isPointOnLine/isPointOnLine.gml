//argument 0-1 = first line point xy
// argument 2-3 = second line point xy
// argument 4-5 = test point

var dxl = argument[2] - argument[0];
var dyl = argument[3] - argument[1];

if (abs(dxl) >= abs(dyl))
{
	return dxl > 0 ?
	  (argument[0] <= argument[4] && argument[4] <= argument[2]) :
	  (argument[2] <= argument[4] && argument[4] <= argument[0]);
}
else
{
	return dyl > 0 ?
	  (argument[1] <= argument[5] && argument[5] <= argument[3]) :
	  (argument[3] <= argument[5] && argument[5] <= argument[1]);
}
