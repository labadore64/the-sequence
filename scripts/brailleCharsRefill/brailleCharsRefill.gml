
if(ds_queue_empty(braille_insert_queue)){
	
	ds_list_shuffle(all_braillecells);
	
	ds_queue_clear(braille_inserted_queue);
	
	// if it allows for repeats, use braille_insert_queue
	// as a temp holder for the next queue item
	if(global.brailleRepeat){
		ds_queue_enqueue(braille_insert_queue,all_braillecells[|0]);
	} else {
	// if it doesn't allow for repeats use braille_insert_queue
	// as the whole list of characters
		var len = ds_list_size(all_braillecells);
		for(var i = 0; i < len; i++){
			ds_queue_enqueue(braille_insert_queue,all_braillecells[|i]);
		}
	}
	
}