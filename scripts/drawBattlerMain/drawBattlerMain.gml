//draw_self();

drawBattlerShadow();

if(global.battleParticle){
	if(!part_closer){
		part_system_drawit(damage_part_system)
	}
}

if(!defeat_animation_complete){
	if(sprite_index >= 0){
		if(flash || flash_once){
			drawBattlerDropShadow()
			drawBattlerFlash();

		} else if(defeat_animation_trigger){
			//defeat_animation_alpha	
			drawBattlerDefeat();
		} else if (fleeing){
			drawBattlerFlee();	
		
		} else if (flee_complete){
		
		}else {
			//no shaders applied
			drawBattlerDropShadow()
			drawBattlerDefault();
		}
	}
}



	if(active_filter_sprite > 0){
		if (!fleeing){
			draw_sprite_ext(active_filter_sprite,active_filter_frame,x,y-jump_y*image_yscale-128*image_yscale,image_xscale*sprite_scale,image_yscale*sprite_scale,0,image_blend,image_alpha*hide_alpha);
		}
	}
	
//draw attack sprite

//end draw attack sprite
	
if(global.battleParticle){
	if(part_closer){
	
		part_system_drawit(damage_part_system)
	}
	part_system_drawit(main_part_system);
}

	if(!sprite_attack_complete){
		if(sprite_attack != -1){
			draw_sprite_ext(sprite_attack,sprite_attack_current_frame,x,y-jump_y*image_yscale-128*image_yscale,image_xscale*sprite_scale,image_yscale*sprite_scale,0,image_blend,1);
		}
	
	}