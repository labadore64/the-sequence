if(ds_list_empty(numbers)){
	if(obj_to_create != -1){
		var obj = instance_create(0,0,obj_to_create);
		menuControlStage(obj);	
		if(obj_to_create == brailleGameTimeAttack){
			obj.seconds = quantity;
		} else if(obj_to_create == brailleGameElimination){
			obj.base_rounds = quantity;
		} else if (obj_to_create == brailleGamePracticeMode){
			ds_list_add(obj.all_braillecells,braille_char);
		}
		obj.braille_data = braille_data
	}
	instance_destroy();	

} else {
	complete_animation_text = numbers[|0];
	ds_list_delete(numbers,0);
	if(ds_list_empty(numbers)){
		soundfxPlay(sound_ax_countdown_go)	
	} else {
		soundfxPlay(sound_ax_countdown)	
	}
	result_fade_counter = 1;
	result_start_counter = 0;
}