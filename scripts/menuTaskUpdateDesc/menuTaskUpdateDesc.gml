var obj = task_list[|menupos];

if(!is_undefined(obj)){
	if(obj > -1){
		var text = objdata_task.desc[obj];
		var display_linesize = 15;

		current_desc = textForceCharsPerLine(text,display_linesize);
	
		current_sprite = objdata_task.sprite[obj]
		objdata_task.unread[obj] = false;
	}
}