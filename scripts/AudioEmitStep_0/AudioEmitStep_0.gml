var update_sound = false;
audio_emitter_position(emit, x, y, 0);


if(play_this_turn){
	emit_sound = audio_play_sound_on(emit, audio_sound, loop, 1);
	play_this_turn = false;
}

if(mod_pitch){
    if(instance_exists(obj_you)){
        if(obj_you.y > y){
            pitch_mod = 1
            
        } else {
            pitch_mod = 1.025
        }
        
        if(pitch_mod != pitch_mod_previous){
            update_sound = true
        }
        
        pitch_mod_previous = pitch_mod
        
        if(emit_sound != -1){
            audio_sound_pitch(emit_sound,pitch_base*pitch_mod)
        }
    }
}

countdown--;

if(countdown < 0){
	if(!loop){
		if(!audio_is_playing(emit_sound)){
			instance_destroy();	
		}
	}
}