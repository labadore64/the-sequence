with(menuParent){
	menuParentSetTitleColor(global.textColor)	
	menuParentSetSubtitleColor(global.textColor)
	menuParentSetSelectColor(global.textColor)
	menuParentSetInfoboxSubtitleColor(global.textColor)	
	menuParentSetOptionTextColor(global.textColor)
	
	menuParentSetGradient(merge_color(gradient1_original,global.textColor,.1),merge_color(gradient2_original,global.textColor,.3))
}

with(menuControl){
	bg_color = merge_color(c_black,global.textColor,.2);	
}