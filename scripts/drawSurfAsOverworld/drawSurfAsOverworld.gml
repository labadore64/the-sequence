/// @description Insert description here
// You can write your code in this editor
if(global.overworld_on){
	with(shd_overworld_obj){
		
		
		
		if(!surface_exists(surf)){
			surf = surface_create_access(u_uv_x , u_uv_y)	
		} else {
			if(ScaleManager.updated){
				surface_resize(surf,u_uv_x , u_uv_y)	
			}
		}
		if(!surface_exists(final_surface)){
			final_surface = surface_create_access(u_uv_x , u_uv_y)
		} else {
			if(ScaleManager.updated){
				surface_resize(final_surface,u_uv_x , u_uv_y)	
			}
		}
		surface_set_target(surf);
		draw_clear_alpha(c_black, 1);
		
		/*if shader_enabled */shader_set(shd_gaussian_horizontal1);
		    shader_set_uniform_f(uni_resolution_hoz, var_resolution_x, var_resolution_y);
		    shader_set_uniform_f(uni_blur_amount_hoz, var_blur_amount);
		    draw_surface(argument[0],0,0);
		shader_reset();
		surface_reset_target();

		surface_set_target(final_surface){
			//Do vertical blur last
			draw_clear_alpha(c_black, 1);
			if shader_enabled shader_set(shd_gaussian_vertical1);
			    shader_set_uniform_f(uni_resolution_vert, var_resolution_x, var_resolution_y);
			    shader_set_uniform_f(uni_blur_amount_vert, var_blur_amount);
			    draw_surface(surf,0,0);
			shader_reset();
		}
		surface_reset_target()
		shader_set(shd_overworld)

		shader_set_uniform_f(shd_vcr_counter,counter);
		shader_set_uniform_f(shader_get_uniform(shd_overworld,"u_settings"),size1,size2,.25,1)
		draw_surface(final_surface,0,0)
		shader_reset()
	}
} else {
	draw_surface(argument[0],0,0);
}