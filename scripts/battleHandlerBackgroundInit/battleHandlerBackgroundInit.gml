var encounters = objdataToIndex(objdata_encountermap,room_get_name(room_last));

if(encounters > -1){
	background = objdata_encountermap.bg_sprite[encounters]; //background of battle
} else {
	background = spr_battlebg_conif_forest
}
bg_spacer = 1; //how many pixels correlates to orientation.
bg_width = 1; //width of the bg animation

bg_pos = 0; //location of battle image relative to the orientation