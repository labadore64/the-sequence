
// input attack as "defend"
battleAttackInputCreate(BattleHandler.char_input_character,noone,objdataToIndex(objdata_moves,"defend"));
soundfxPlay(soundBattleSelectTarget)

with(menuBattleFight){
	menuBattleFightSetHowmany();

	character = BattleHandler.char_input_character;
	character_load = BattleHandler.char_input_counter;

	battleHandlerSelectAllyClear()
	battleHandlerSelectAlly(ds_list_find_index(BattleHandler.allies,character),true);

	battleHandlerClearFlash();
	battleHandlerFlashSelected();	
	menupos = 0;

	if(character > 0){
		tts_say(character.name + " " + menu_name_array[0])
	}
}

with(BattleHandler){
	var sizer = ds_list_size(select_list);
	if(char_input_counter >= sizer){
		instance_create(0,0,battleStartAttack)
		with(menuParent){
			active=false;
			battleHandlerClearFlash();
			battleHandlerSelectAllyClear()
			battleTextDisplayClearText();
			instance_destroy()	
		}
	}
}