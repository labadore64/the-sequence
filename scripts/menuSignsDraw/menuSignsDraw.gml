
		//menuParentDrawBox();

		draw_clear(c_black)
		if(drawGradient){
			draw_rectangle_color_ratio(x1,0,x2,0+gradient_height,gradient_color1,gradient_color2,gradient_color2,gradient_color1,false)
		}

		menuParentDrawText();

		menuParentDrawOptions();
		draw_set_halign(fa_left);
	
		gpu_set_colorwriteenable(true,true,true,true)

		var xxx = 600;
		var yyy = 350;

		if(drawInfoText){
			if(ds_exists(menu_name,ds_type_list)){
			if(ds_exists(menu_description,ds_type_list)){
			
			draw_set_halign(fa_center)

			var stringer = menu_name_array[ menupos];

			if(!is_undefined(stringer)){
				draw_set_color(info_title_color);
				draw_text_transformed_ratio(xxx,yyy+info_title_y_offset,stringer,info_title_size,info_title_size,0)
			}

			stringer = menu_desc_array[ menupos];
			if(!is_undefined(stringer)){
				draw_set_color(info_subtitle_color)
				draw_text_transformed_ratio(xxx,yyy+info_subtitle_y_offset,stringer,info_subtitle_size,info_subtitle_size,0)
			}

			}
			}
		}
	
	
		draw_set_halign(fa_left);
	
		with(display_obj){
			brailleDisplayDraw(id);		
		}
	
		if(sign_sprite > -1){
			draw_sprite_extended_ratio(sign_sprite,0,350,410,1,1,0,global.textColor,1);
		}
	
		var xx = 700
		var yy = 560
	
		draw_set_halign(fa_right)
		draw_text_transformed_ratio(xx-95,yy,"Seen:",3,3,0)
		draw_text_transformed_ratio(xx-7,yy,string(sign_seen),3,3,0)
		draw_set_halign(fa_center)
		draw_text_transformed_ratio(xx,yy,"/",3,3,0)
		draw_set_halign(fa_left)
		draw_text_transformed_ratio(xx+7,yy,string(sign_total),3,3,0)
