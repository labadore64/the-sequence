/// @description - sets growth param based on level
/// @param level

//this script will return a value 0-1 that represents the growth factor relative
//to the current level.

var tester = argument[0]/99;

return (sqrt(tester)+tester)*.5;