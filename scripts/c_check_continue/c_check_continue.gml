//checks if processing can continue.
with(CutsceneParser){
	if(ds_list_empty(execution_stack)){
		return true;	
	} 
	return false;
}