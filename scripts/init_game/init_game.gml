
init_macro();
randomize();
tts_init();

init_particle();

init_global(); //initializes global vars


global.keybind_map = ds_map_create();

global.gamepad_map = ds_map_create();

keybind_init(); //initializes keybinds
gamepadbind_init();

//inits necessary objs
instance_create_depth(0,0,0,menuControl);
instance_create_depth(0,0,0,ttsControl);
instance_create_depth(0,0,0,obj_data);
instance_create_depth(0,0,0,MusicPlayer);
instance_create_depth(0,0,0,ScaleManager);
instance_create_depth(0,0,0,HUDController);
instance_create_depth(0,0,0,MouseHandler)
//ax stuff
instance_create_depth(0,0,0,AXBookmarkHandler);

draw_set_font(global.textFont)

initCreateDirectories();

newalarm[0] = 2