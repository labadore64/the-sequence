ds_list_clear(call_list);
menuParentClearOptions();

var call_size = ds_list_size(obj_data.characters);

ds_list_copy(call_list,obj_data.party)

for(var i = 0; i < call_size; i++){
	if(ds_list_find_index(obj_data.party,obj_data.characters[|i]) == -1){
		if(obj_data.characters[|i].recruited){
			ds_list_add(call_list,obj_data.characters[|i]);	
		}
	}
}

if(ds_list_empty(call_list)){
	menuAddOption("No friends :(","",-1)
} else {
	var sizer = ds_list_size(call_list);
	var obj;
	for(var i = 0; i < sizer; i++){
		obj = call_list[|i];
		if(!is_undefined(obj)){
			menuAddOption(obj.name,"",menuCallOpen)
		}
	}
}

menu_size = menuGetSize();