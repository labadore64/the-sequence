giveTablet(character,insert_index,braille_id);
if(braille_id > -1){
	tts_say("Gave " 
			+ "\"" + objdata_tablet.char[braille_id] + "\" " 
			+ objdata_tablet.name[braille_id] +
			" to " + character.name);
}
instance_destroy();
with(menuTabletSub){
	instance_destroy();	
}