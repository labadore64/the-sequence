
if(animation_len == 0){
	if(!surface_exists(surface)){
		update_surface = true;	
	}
	
	if(update_surface){
		surface_set_target(surface);
		draw_clear(c_black)
		menuCharacterSwitchPartyRecruitParty();
	
		surface_reset_target();
		update_surface = false;
	}
	
	draw_clear(c_black)
	if(global.menuParticles){
		draw_rectangle_color(0,480,800,600,c_black,c_black,aura_draw,aura_draw,false);


		part_system_drawit(Sname);

	}
	draw_set_color(c_black)
	draw_rectangle(0,550,800,600,false)
	
	//start data
	
	gpu_set_colorwriteenable(true,true,true,true)
	
	with(portrait){
		draw_surface(surface,400,180)	
	}
	
	var movexx = -150;
	var moveyy = 20;
	
	var statsxoff = -25+movexx;
	var statsyoff = 25+3+moveyy;

	with(drawstat){
		draw_surface(surface_hex,450+statsxoff,250+statsyoff);	
	}

	//draw stat icons
	var statspritexoff = 500-50+movexx;
	var statspriteyoff = 300-10+moveyy;

	draw_sprite_ext(spr_magpower_icon,0,10+statspritexoff-75+22,35+statspriteyoff+62,1,1,0,$FF7FFF,1)
	draw_sprite_ext(spr_physguard_icon,0,170+statspritexoff-7,35+statspriteyoff+185-22,1,1,0,$FFFF7F,1)
	draw_sprite_ext(spr_magguard_icon,0,170+statspritexoff-165+7,35+statspriteyoff+185-22,1,1,0,$7FFF7F,1)
	draw_sprite_ext(spr_physpower_icon,0,170+statspritexoff+65-22,35+statspriteyoff+62,1,1,0,$7F7FFF,1)	
	draw_sprite_ext(spr_speguard_icon,0,170+statspritexoff-165+7,35+statspriteyoff-65+22,1,1,0,$FF7F7F,1)
	draw_sprite_ext(spr_spepower_icon,0,170+statspritexoff-7,35+statspriteyoff-65+22,1,1,0,$7FFFFF,1)	

	//draw color bars

	var color_spacer = 30; //y spacer
	var color_move = 250;
	var color_move_y = 50
	var color_height = 12*.5;
	var color_width = 120*.5;
	var i = 0;

	var adjusttt = -20

	var xxxx = -90
	var yyyy = 510-40+adjusttt
	draw_set_font(global.textFont)
	draw_set_color(global.textColor)
	draw_set_halign(fa_right)
	draw_text_transformed_ratio(xxxx+180,-55+yyyy-color_spacer*1.5,"Color",1,1,0)
	draw_text_transformed_ratio(xxxx+180,-55+yyyy,"Red",1,1,0)
	draw_text_transformed_ratio(xxxx+180,-55+yyyy+color_spacer,"Green",1,1,0)
	draw_text_transformed_ratio(xxxx+180,-55+yyyy+color_spacer*2,"Blue",1,1,0)
	draw_set_halign(fa_left)
	
	draw_set_color($111111);
	for(var i = 0; i < 3; i++){
		draw_rectangle(color_move + xxxx-color_width-2,yyyy-color_height-2+color_spacer*i-color_move_y,color_move + xxxx+color_width+2,yyyy+color_height+2+color_spacer*(i)-color_move_y,false)	
	}
	draw_set_color(c_black)
	for(var i = 0; i < 3; i++){
		draw_rectangle(color_move + xxxx-color_width,yyyy-color_height+color_spacer*i-color_move_y,color_move + xxxx+color_width,yyyy+color_height+color_spacer*(i)-color_move_y,false)	
	}

	if(abs(red_ratio) > .1){
		draw_set_color(red_color)
		i = 0;
		var ratio = red_ratio;
		draw_rectangle(color_move + xxxx,yyyy-color_height+color_spacer*i-color_move_y,color_move + xxxx+(color_width*ratio),yyyy+color_height+color_spacer*(i)-color_move_y,false)	
	}
	
	if(abs(green_ratio) > .1){
		draw_set_color(green_color)
		i = 1;
		var ratio = green_ratio;
		draw_rectangle(color_move + xxxx,yyyy-color_height+color_spacer*i-color_move_y,color_move + xxxx+(color_width*ratio),yyyy+color_height+color_spacer*(i)-color_move_y,false)	
	}
	
	if(abs(blue_ratio) > .1){
		draw_set_color(blue_color)
		i = 2;
		var ratio = blue_ratio;
		draw_rectangle(color_move + xxxx,yyyy-color_height+color_spacer*i-color_move_y,color_move + xxxx+(color_width*ratio),yyyy+color_height+color_spacer*(i)-color_move_y,false)	
	}

	menuOverworldPartyDrawInfo();

	menuCharacterSwitchPartyDrawLevel(adjusttt)
	
	drawBar(160,375+adjusttt,60,16,1,char_color)
	
	//end data 
	
	draw_set_color(c_black)
	var ggg = 160+130;
	draw_rectangle(0,0,800,ggg,false);

	draw_set_color($7F7F7F)
	var toppp = 151
	draw_rectangle(0,toppp,800,toppp+2,false);
	draw_rectangle(0,ggg,800,ggg+2,false);

	if(surface_exists(surface)){
		draw_surface(surface,-267+animation_adjust,0)
	}
	menuCharacterSwitchPartyRecruitSelect();

	//menuOverworldPartyDrawParty();
	
	//menuOverworldPartyDrawSelect();

	draw_set_font(global.textFont);
	draw_set_color(global.textColor);
	draw_set_halign(fa_center);
	
	draw_text_transformed_ratio(400,170,character_string,2,2,0)
	draw_set_halign(fa_left);

	obj_data.aura_ability = 5;

	//menuOverworldPartyDrawAura(aura_width, aura_height, xxx, yyy, textxxx, textyyy, boosttextxxx, boosttextyyy,obj_data.phy_boost, obj_data.mag_boost, obj_data.spe_boost, obj_data.aura_ability,false,80)

	//menuOverworldPartyDrawAuraBasic(xxx,yyy,aura_width,aura_height,textxxx,textyyy+5,obj_data.aura_ability)

}
//menuParentDrawInfo();