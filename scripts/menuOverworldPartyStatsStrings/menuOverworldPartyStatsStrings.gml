event_inherited();

var sizer = ds_list_size(obj_data.party);

for(var i = 0; i < sizer; i++){
	menuAddOption("",global.langini[3,8],menuOverworldStatsSubScript);
}

menuParentSetTitle(global.langini[2,0])
menuParentSetSubtitle(global.langini[3,0])

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menu_draw = menuOverworldPartyStatsDraw

menuParentSetDrawGradient(false)

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

menuParentUpdateBoxDimension();

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menu_tableft = menuOverworldPartyStatsMoveUp;
menu_tabright = menuOverworldPartyStatsMoveDown;

menu_left = -1;
menu_right = -1;

menu_down = menu_tabright;
menu_up = menu_tableft;

instance_create_depth(0,0,0,dataDrawStat);

tts_clear();

newalarm[2] = 1;

title_x_offset = 10

draw_black = c_black;
draw_chara = merge_color(c_black,global.textColor,.3);
