// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from


	if(obj > -1 && obj < objdata_tablet.data_size){
		// name and character
		hudControllerAddData(entryname + "tablet_name",objdata_tablet.name[obj]);
		hudControllerAddData(entryname + "tablet_desc",objdata_tablet.desc[obj]);
		hudControllerAddData(entryname + "tablet_char","\"" + objdata_tablet.char[obj] + "\"" );
		
		hudControllerAddData(entryname + "phy_power",string(objdata_tablet.phy_power[obj]));
		hudControllerAddData(entryname + "phy_guard",string(objdata_tablet.phy_guard[obj]));
		hudControllerAddData(entryname + "mag_power",string(objdata_tablet.mag_power[obj]));
		hudControllerAddData(entryname + "mag_guard",string(objdata_tablet.mag_guard[obj]));
		hudControllerAddData(entryname + "spe_power",string(objdata_tablet.spe_power[obj]));
		hudControllerAddData(entryname + "spe_guard",string(objdata_tablet.spe_guard[obj]));
		
		hudPopulateMove(argument[0],objdata_tablet.move_id[obj],0);
	}

//move_id[itemcount]