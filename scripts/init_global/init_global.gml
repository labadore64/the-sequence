
		init_font();

	global.playerName = "Leon";
	global.inputEnabled = true;
	
	// determines the current sort mode for the dex
	global.dex_sort_mode = SORT_MODE_ALPHA

	var blindmode = "";
	var forcegraph = ""

	file_delete("config_backup.ini");

	file_copy("config.ini","config_backup.ini");

	ini_open("config.ini")

	var music_vola = ini_read_real("config","music_vol",.3)
	var sfx_vola = ini_read_real("config","sfx_vol",.5);
	var env_vol = ini_read_real("config","env_vol",.5);
	var ax_vol = ini_read_real("config","access_vol",1);
	var fullscre = stringToBool(ini_read_string("config","fullscreen","false"));

	blindmode = stringToBool(ini_read_string("access","disable_graphics","false"));
	var AXCornerSound = stringToBool(ini_read_string("access","corner_sound","true"));
	var AXVisionEnabled = stringToBool(ini_read_string("access","vision","true"));
	var sight_dist = ini_read_real("access","vision_distance",AX_SIGHT_DISTANCE);
	var collision_distance =  ini_read_real("access","proximity_distance",AX_COLLISION_DISTANCE);
	var tts  = stringToBool(ini_read_string("access","tts","false"));
	var shader = stringToBool(ini_read_string("config","disable_shader","false"))

	ini_close()

	// phone signal strength
	
	global.signal_strength = 5;

	// represents the default aspect ratio of the game
	// will redraw graphics when window is resized
	global.window_width = window_get_width();
	global.window_height = window_get_height();
	global.display_x = 0;
	global.display_y = 0;
	global.letterbox_w = 0;
	global.letterbox_h = 0;
	global.scale_factor = 1;
	global.aspect_ratio = global.window_width/global.window_height;

	global.tts = tts; // if tts is enabled
	global.blindMode = blindmode
	global.blindForceGraphics = false
	global.cantSeeMode = false; //for dark rooms

	//if(global.blindMode){
	audio_group_load(audiogroup_ax);	
	//}

	//debug
	global.debugMenu = false;
	global.debugPlayerMovement = false;
	global.debugSkipIntro = false

	//options

	global.textSpeed = 1; //speed of text
	global.text_auto_speed = 10;
	global.textColor = c_white;
	global.textFont = global.normalFont;
	global.voice = !blindmode; //voice for text boxes plays?
	global.metric = false; //metric mode or US mode?
	global.tutorial = true; //do the tutorials?

	//drawing options
	global.disableShader = shader // disable all shaders
	global.drawBrightnessContrast = false; //draw vignette?
	global.drawVignette = true; //draw vignette?
	global.menuParticles = true; //draw menu particle graphics?
	global.brightness = 0.05; //how much to add onto brightness
	global.contrast = 0.05; //how much to add onto contrast
	global.vcr_on = true; //VCR drawing?
	global.overworld_on = true; //overworld drawing?
	global.fullscreen = fullscre; //fullscreen?
	global.menuAnimation = true; //Do menu animations?

	window_set_fullscreen(fullscre)

	//audio options
	global.sfxVolume = sfx_vola; //volume for sfx
	global.musicVolume = music_vola; //volume for music
	global.envVolume = env_vol; //volume for 3D sounds in enviorment

	//AX
	global.AXVolume = ax_vol; //volume for AX utils
	global.AXCollisionDistance = collision_distance
	global.AXSightDistance = sight_dist
	global.AXCornerSound = tts
	global.AXVisionEnabled = tts
	global.AXCollisionEnabled = tts
	
	// whether or not notify sounds are enabled.
	global.AXNotify = tts

	// to remove
	global.AXLayout = 0; //determine layout used for infokeys

	//battle
	global.battleRotate = true; //whether or not the battle characters will rotate
	global.battleParticle = true; //should battle particles be drawn
	global.battleAnimate = true; //should battle animations be drawn.
	global.battleUIGradient = false; //whether or not gradients should be drawn
	global.battleHPSound = true; //Plays the HP sound effect when attacking enemies

	// not yet implemented
	global.battlePercent = true; //whether battle damage is in percent or numbers
	global.battlePortrait = true; //whether character portraits should be drawn
	global.attackSound = true; //whether or not attacks make sounds

	//button
	global.gamepadRightAxis = false;
	global.gamepadVibration = false;

	// braille practice mode
	global.braille_set = "alphabeten"; // the main braille set used for the game.
	global.braille_practice = "alphabeten"; // which braille set to use for practice mode.
	global.BrailleSound = true; //if the braille sounds are used for braille input
	global.BrailleTimerSpeed = 4; // if set to 0, timer is disabled. otherwise its a multiplier.
	global.brailleRepeat = true; //whether or not to repeat braille characters in practice mode
	global.brailleBackground = true; //whether or not to show background for braille minigame. is black otherwise
	global.brailleHint = false; //whether or not to give the braille hint near the end of the timer
	global.countdownSound = true; //whether or not to play sound for timer

	// to implement
	global.readBrailleCell = true; //Reads the braille cell data when pressing the infokeys

	// menu positions
	global.overworldMenuPosition = 0;
	global.overworldPartyPosition = 0;
	global.overworldItemPosition = 0;
	global.overworldItemBag = 0;
	
	// control
	global.mouse_active = true
	global.mouse_snap = true;

	langiniLoad();

