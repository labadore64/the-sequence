
//select enemy attacks
var enemy_size = 0;
var getauto = objdataToIndex(objdata_moves,"attack");
var gettarget = -1;

var is_fleeing = false;

with(battleAttackInput){
	if(attack == objdataToIndex(objdata_moves,"flee")){
		is_fleeing = true;	
	}
}

if(!is_fleeing){
	with(BattleHandler){
		enemy_size = ds_list_size(enemy_list);	
		var chara = -1;
	
		var targ_list = -1;
	


		var ha =-1;
		for(var i = 0; i < enemy_size; i++){
			
			chara = enemy_list[|i];
			
			if(battlerIsAlive(chara)){
				if(battlerHasFilter(chara,"Talk")){
					getauto = objdataToIndex(objdata_moves,"talking")
				} else {
					getauto = script_execute(chara.ai_script,chara)
				}
				moveData(getauto);
				if(target_ally){
					targ_list = BattleHandler.enemy_list;	
				} else {
					targ_list = BattleHandler.party_list	
				}
		
				gettarget = script_execute(chara.ai_target_script,getauto,chara);
				var targ = -1;
				if(target_ally){
					targ = enemy_list[|floor(clamp(gettarget,0,2))]
			
				} else {
					targ = party_list[|floor(clamp(gettarget,0,2))];
				}
		
				ha = battleAttackInputCreate(chara,targ,getauto);
		
				var add_target = ha.additional_targets;
		
				if(target_number == 2){
					var next_index = ds_list_find_index(targ_list,targ);
					if(next_index != -1){
						next_index = (next_index+1) mod ds_list_size(targ_list);
					}
					var obj = targ_list[|next_index];
					if(!is_undefined(obj)){
						ds_list_add(add_target,obj);	
					}
				} else if (target_number == 3){
					var sizer = ds_list_size(targ_list);
					var obj = -1;
					for(var j =0; j < sizer; j++){
						obj = targ_list[|j];
						if(!is_undefined(obj)){
							ds_list_add(add_target,obj);	
						}
					}
			
					ds_list_delete(add_target,ds_list_find_index(add_target,targ))
				}
			}
		}
	}
}