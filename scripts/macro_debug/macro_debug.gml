
#macro DEBUG_SHOW_COLLISION false
#macro DEBUG_SHOW_SPRAY false
#macro DEBUG_SHOW_PUSH_AREA false
#macro DEBUG_NO_MONSTER_ENCOUNTER false
#macro DEBUG_DEFAULT_MONSTER_ENCOUNTER false

//change this value if there is a compatibility issue with save files and if it should reject save files
#macro GAME_COMPATIBILITY_VERSION 11