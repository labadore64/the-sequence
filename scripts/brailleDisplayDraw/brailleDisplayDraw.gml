with(argument[0]){
	if(!surface_exists(surf)){
		var width = x_spacing*row_length*cell_scale
		var height =  y_spacing*(column_height)*cell_scale
	
		surf = surface_create(width*global.scale_factor,height*global.scale_factor);	
		surf_update = true;
	} else {
		if(ScaleManager.updated){
			var width = x_spacing*row_length*cell_scale
			var height =  y_spacing*(column_height)*cell_scale
		
			surface_resize(surf,width*global.scale_factor,height*global.scale_factor);
			surf_update = true;
		}
	}

	surface_set_target(surf)
	draw_clear_alpha(c_black,0);
	draw_set_color(c_white);
	var sizer = array_length_1d(braille_display);
	
	for(var i = 0; i < sizer; i++){
		with(braille_display[i]){
			brailleCharacterDraw();	
		}
	}
	surface_reset_target();

	draw_surface_ext(surf,
					global.display_x+x*global.scale_factor,
					global.display_y+y*global.scale_factor,
					1,1,0,
					display_color,
					display_alpha);
	
}