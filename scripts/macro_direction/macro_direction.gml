#macro DIR_NORTH 0
#macro DIR_SOUTH 1
#macro DIR_EAST 2
#macro DIR_WEST 3
#macro DIR_NORTHEAST 4
#macro DIR_NORTHWEST 5
#macro DIR_SOUTHEAST 6
#macro DIR_SOUTHWEST 7

#macro OVERWORLD_MONSTER_MIN 15
#macro OVERWORLD_MONSTER_MAX 27

#macro PLAYER_START_X 480
#macro PLAYER_START_Y 225

#macro FOOTSTEP_TYPE_GRASS soundFootstep01
#macro FOOTSTEP_TYPE_INSIDE soundFootstep02
#macro FOOTSTEP_TYPE_WOOD soundFootstep03
#macro FOOTSTEP_TYPE_DIRT soundFootstep04

#macro AXIS_LEFT 0
#macro AXIS_RIGHT 1
#macro AXIS_UP 2
#macro AXIS_DOWN 3