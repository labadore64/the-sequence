
var chara = argument[0];
var targe = argument[1];

var bat = -1;
var lister = -1;

with(BattleHandler){
	lister = party_list;	
}

if(lister != -1){
	var sizer = ds_list_size(lister);
	
	for(var i =0; i < sizer; i++){
		bat = lister[|i];
		var spacer = 4;
		with(bat){
			fleeing = true;
			flee_delay = 1 + spacer*i;
		}
		with(BattleHandler){
			newalarm[1] = 3
		}
	}

}

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];
battleTextDisplayClearText();
battleAttackInputAddString(stringer);
incrementGameStat(GAMESTAT_FLEE)

ds_list_destroy(battletext);