//note returns list of battle text...

var indexx = argument[0]

var returner = ds_list_create();

// battle_text[itemcount,j]

var len = array_length_2d(objdata_moves.battle_text,indexx);

for(var i = 0; i < len; i++){
	ds_list_add(returner,objdata_moves.battle_text[indexx,i]);
}

if(ds_list_empty(returner)){
	ds_list_add(returner,"");	
}

return returner;