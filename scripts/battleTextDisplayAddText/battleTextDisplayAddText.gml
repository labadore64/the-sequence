if(argument[0] != " "){


	with(Battler){
		if(damage_calc > 0){
			var ouch = hp.current-damage_calc;
			if(ouch < 0){
				ouch = 0;
			}
		
			battlerHPChange(id,ouch)
			damage_this_turn+=damage_calc;
			damage_calc = 0;
		}
		
		if(filter_add_later != -1){
			//filter_add_later;	
			ds_list_add(status_effects,filter_add_later);
			newalarm[1] = 3;
		}
		filter_add_later = -1;
	}

	with(battleTextDisplay){
	signal = false;
	var stringer = textForceCharsPerLine(argument[0],display_linesize);

	var adjusterr = string_replace_all(stringer,"\n","@");

	var string_queue = split_string(adjusterr,"@");
	var obj = -1;

	while(!ds_queue_empty(string_queue)){
		obj = ds_queue_dequeue(string_queue);
		if(!is_undefined(obj)){
			ds_list_add(display_list,obj)
			ds_list_add(text_list,obj);
			battleHandlerAddHistory(obj);
		}
	}

	battleTextDisplaySetEnd();

	display_list_size = ds_list_size(display_list);

	ds_queue_destroy(string_queue)

	}
}