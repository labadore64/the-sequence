
if(instance_exists(AXBookmarkHandler)){
	var checkDist = 100000000;

	var mydist = checkDist;

	with(AXBookmark){
		mydist = point_distance(x,y,Player.x,Player.y)
	
		if(mydist < checkDist){
			checkDist = mydist;	
		}
	}

	if(checkDist > AX_BOOKMARK_MIN_DIST){
		return instance_create_depth(argument[0],argument[1],0,AXBookmark);
	}

	return -1
}
return -1;