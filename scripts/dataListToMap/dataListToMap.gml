//note: should only be used with lists where order doesn't matter such as events

var list = argument[0];

var returner = -1;

if(ds_exists(list,ds_type_list)){
	returner = ds_map_create();
	
	var sizer = ds_list_size(list);
	
	for(var i = 0; i < sizer; i++){
		ds_map_add(returner,i,list[|i]);	
	}
	
}

return returner;