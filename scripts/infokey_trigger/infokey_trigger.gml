var infokey_value = argument[0];
with(HUDController){
	if(!infokey_thisframe){
		var tri = infokey_strings[|infokey_value];

		if(!is_undefined(tri)){
			tts_say(string(tri))
			keypress_this_frame = true;
			infokey_thisframe = true;
		}
	}
}