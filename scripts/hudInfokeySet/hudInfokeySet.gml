//infokeys_set
with(HUDController){
	if(infokey_id != -1){
		var lol = ds_list_create();
		
		var sizer = array_length_2d(objdata_UI.infokey,infokey_id);
		
		for(var i = 0; i < sizer; i++){
			if(objdata_UI.infokey_req[infokey_id,i] == "" || hudControllerContainsData(objdata_UI.infokey_req[infokey_id,i])){
				ds_list_add(lol,hudControllerReplaceData(objdata_UI.infokey[infokey_id,i],active_object));
			} else {
				ds_list_add(lol,"");
			}
		}
		
		infokeys_set(lol)
		
		ds_list_destroy(lol);
	}
}