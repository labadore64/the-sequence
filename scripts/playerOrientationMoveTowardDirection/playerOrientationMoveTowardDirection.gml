/// @description - move the orientation towards the direction of the movement
/// @param angle

var ori = argument[0];

	//move towards rotation direction
	
	//adjust vars so that system is oriented at 180 degrees relative to destination direction

if(orientation != ori){

	var test_orientation = 360-((orientation+360-(360-ori)) mod 360);
	
	if(test_orientation > 180){
		orientation -= rotation_speed;
	} else {
		orientation += rotation_speed;
	}
	
	
	rotation_speed+= rotation_delta;
	
	if(rotation_speed > rotation_max){
		rotation_speed = rotation_max;	
	}
}

update_surface = true;

move_this_frame = true;

draw_action = PLAYER_ACTION_WALK