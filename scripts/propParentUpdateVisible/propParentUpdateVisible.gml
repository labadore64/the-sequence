gml_pragma("forceinline");
var getvisible = true;
if(obj_data.camera_exists){
	if( CameraOverworld.draw_cornerx1 > bounding_box[1] || CameraOverworld.draw_cornery1 > bounding_box[3] || CameraOverworld.draw_cornerx2 < bounding_box[0] || CameraOverworld.draw_cornery2 < bounding_box[2])//view_yview,,view_yview+view_hview, self, false, true))
	{
		getvisible = false
	} else {
		getvisible = true
		if(requires_binoculars){
			if(!CameraOverworld.binoculars){
				getvisible = false;	
			}
		}
	}
}

visible = getvisible;