with(BattleHandler){
	draw_black_bg = true;	
	background = background_Black
}

with(battleStat){
	instance_destroy();	
}

with(battleTextDisplay){
	instance_destroy();	
}

with(battleStartAttack){
	instance_destroy();	
}

with(battleAttackInput){
	instance_destroy();	
}

with(battleEndVictoryStats){
	instance_destroy();	
}
with(dataDrawStat){
	instance_destroy();
}

with(battleEndPhaseHolder){
	instance_destroy();	
}

with(Battler){
	visible = false;
	instance_destroy();	
}


with(battleEndPhase){
	instance_destroy();	
}

with(menuBattleFight){
	instance_destroy();	
}