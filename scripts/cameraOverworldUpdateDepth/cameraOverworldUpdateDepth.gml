var prio = list;

ds_priority_clear(prio);

with(Player){
	ds_priority_add(prio,id,depth);
}

if(DEBUG_SHOW_COLLISION){
	with(Collision){
		ds_priority_add(prio,id,500000);	
	}
	with(overworld_scalable){
		if(object_index == overworld_scalable){
			ds_priority_add(prio,id,500000);	
		}
	}
}

if(DEBUG_SHOW_PUSH_AREA){
	with(pushArea){
		ds_priority_add(prio,id,500000);	
	}
}


with(PropParent){
	if(visible){
		ds_priority_add(prio,id,depth);
	}
}
