/// @description - test collision for object
/// @function collisionTest(collision_id,x,y)
/// @param collision_id
/// @param x
/// @param y


//var argument[0] = argument[0];
//var argument[1] = argument[1];
//var argument[2] = argument[2];

var returner = false;

//first determine what side the collision would be on


//test the top side.
returner = point_in_triangle(argument[1], argument[2], argument[0].topleftx, argument[0].toplefty, argument[0].toprightx, argument[0].toprighty, argument[0].bottomrightx, argument[0].bottomrighty);

if(returner){
	
	return returner;	
}

returner = point_in_triangle(argument[1], argument[2], argument[0].topleftx, argument[0].toplefty, argument[0].bottomleftx, argument[0].bottomlefty, argument[0].bottomrightx, argument[0].bottomrighty);

return returner;
