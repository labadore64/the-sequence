
		with(parent){
			with(parent){
				var ob = textboxOverworld("")
				ob.unlock_player = true;
				textboxScriptSet(ob,textboxTakePlacedItemScriptYes,-1)
				textboxScriptAddArgument(ob,"item_id",item_id)
				textboxScriptAddArgument(ob,"obj_id",parent)
			
				itemData(item_id);
				textboxAfterTextAddLine(ob,global.playerName + " took the " + name + ".");
			}
		}
		
		with(Player){
			lock = true;
		}
		
		instance_destroy();