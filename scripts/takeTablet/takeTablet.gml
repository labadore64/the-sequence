var chara = argument[0]
var tablet_id = argument[1]

if(tablet_id > -1){
	obj_data.tablet_character[tablet_id] = -1;

	with(chara){
		for(var i = 0; i < 3; i++){
			if(tablet[i] == tablet_id){
				tablet[i] = -1;
			}
		}
	
		newalarm[0] = 1;
	}
}