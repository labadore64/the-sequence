var lay_id = layer_background_get_id(layer_get_id("Background"));
layer_background_visible(lay_id, false)

binoculars = false;


binoculars_u_vRatio = shader_get_uniform(shd_binoculars, "u_vRatio");
binoculars_sampler0  = shader_get_sampler_index(shd_binoculars, "sampler0")
binoculars_texture0  = sprite_get_texture(spr_binoculars_shader,0);
binoculars_u_threshold = shader_get_uniform(shd_binoculars, "u_threshold");
binoculars_amount = -.75

prop_place_surface = -1;
prop_place = false;
prop_place_obj = -1;