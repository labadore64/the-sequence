takeTablet(character,braille_id);
if(braille_id > -1){
	tts_say("Took " 
			+ "\"" + objdata_tablet.char[braille_id] + "\" " 
			+ objdata_tablet.name[braille_id] +
			" from " + character.name);
}
instance_destroy();
with(menuTabletSub){
	instance_destroy();	
}