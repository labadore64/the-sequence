if(ds_exists(text,ds_type_list)){
	ds_list_destroy(text)	
}

if(ds_exists(add_beforetext,ds_type_list)){
	ds_list_destroy(add_beforetext)	
}

if(ds_exists(add_aftertext,ds_type_list)){
	ds_list_destroy(add_aftertext)	
}

with(display_object){
	instance_destroy();	
}

if(ds_exists(scripts,ds_type_map)){
	ds_map_destroy(scripts);	
}

if(ds_exists(arguments,ds_type_map)){
	ds_map_destroy(arguments);	
}

if(triggered_by_player){
	with(Player){
		active = true;
	}
}

if(unlock_player){
	with(Player){
		lock = false;
	}
}

with(parent){
	lock = false;	
}

audio_stop_sound(text_sound1);
audio_stop_sound(text_sound2);
audio_stop_sound(text_sound3);
audio_stop_sound(text_sound4);

if(end_script != -1){
	script_execute(end_script);	
}

with(portrait){
	instance_destroy();	
}

with(DialogHandler){
	dontdrawportrait = false;	
}
tts_stop();

with(BattleHandler){
	with(ObjGeneric){
		lock = false;	
	}
	
}

if(braille_thing){
	with(battleStartAttack){
		lock = true;	
	}
} else {
	with(battleStartAttack){
		current_attack.done = true;
		battleStartAttackConfirm();
	}
}

if(event_id != ""){
	eventNormalSet(event_id);
}

