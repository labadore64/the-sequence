var chara = argument[0];

var level = chara.level;
var move_list = chara.moves;
var character = chara.character;

ds_list_clear(move_list);

//add lists for info from ini file
var moves = ds_list_create();
var moves_level = ds_list_create();
var moves_event = ds_list_create();

//note returns list of battle text...

var stringer = "move_id"

var returner = ds_list_create();

var filename = working_directory + "resources\\stats\\move_learn\\character"+string(character)+".ini";

ini_open(filename)

var i = 0;

while(true){
	if(!ini_key_exists("moves", stringer+ string(i))){
		break;
    }
	i++
}

var strrr = "";

for(var j = 0; i > j; j++){
	
    strrr = ini_read_real("moves", "move_id"+ string(j),-1)
	ds_list_add(moves,strrr);
    strrr = ini_read_real("moves", "move_level"+ string(j) ,-1)
	ds_list_add(moves_level,strrr);
    strrr = ini_read_string("moves", "move_event"+ string(j),"none")
	ds_list_add(moves_event,strrr);
}

ini_close();

//now, parse through the lists and add moves based on the event/level combo

var sizer = ds_list_size(moves);

var move_id = 0;
var move_level = -1;
var move_event = "none"

for(var i = 0; i < sizer; i++){
	move_id = moves[|i];
	move_level = moves_level[|i];
	move_event = moves_event[|i];
	movesLearnedLoadInsert(move_list,move_id,move_level,move_event,chara);
}

ds_list_destroy(moves);
ds_list_destroy(moves_level);
ds_list_destroy(moves_event);