// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from

if(obj < 1000){
	if(obj > -1 && obj < objdata_character.data_size){
		obj = obj_data.characters[|obj];	
	}
}

if(instance_exists(obj)){
	if(obj.object_index == dataCharacter){
		hudControllerAddData(entryname + "element",string(obj.element));
		
		// name and character
		hudControllerAddData(entryname + "name",string(obj.name));
		hudControllerAddData(entryname + "desc",objdata_character.battle_desc[obj.character]);
		hudControllerAddData(entryname + "axdesc",objdata_character.ax_desc[obj.character]);
		hudControllerAddData(entryname + "character",string(obj.character));
		
		// stats
		hudControllerAddData(entryname + "phy_power",string(argument[2]));
		hudControllerAddData(entryname + "phy_guard",string(argument[3]));
		hudControllerAddData(entryname + "mag_power",string(argument[4]));
		hudControllerAddData(entryname + "mag_guard",string(argument[5]));
		hudControllerAddData(entryname + "spe_power",string(argument[6]));
		hudControllerAddData(entryname + "spe_guard",string(argument[7]));
		hudControllerAddData(entryname + "hp",string(obj.hp));
		hudControllerAddData(entryname + "mp",string(obj.mp));
		
		// exp
		hudControllerAddData(entryname + "exp",string(obj.experience));
		hudControllerAddData(entryname + "level",string(obj.level));
		
		var expnext = "none";
		var expperc = "100";
		
		if(obj.level != 100){
			expnext = string(obj.exp_to_next);
			expperc = string(floor(obj.exp_percent*100));
		} 
		
		hudControllerAddData(entryname + "exp_next",expnext);
		hudControllerAddData(entryname + "exp_percent",expperc);

		// items and tablets
		for(var i = 0; i < 5; i++){
			hudControllerAddData(entryname + "item"+string(i),string(obj.item[i]));
		}
		
		hudControllerAddData(entryname + "tablet0",string(argument[8]));
		hudControllerAddData(entryname + "tablet1",string(argument[9]));
		hudControllerAddData(entryname + "tablet2",string(argument[10]));
	}
}