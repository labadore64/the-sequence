#macro SORT_MODE_INDEX 1
#macro SORT_MODE_ALPHA 2

var sort_mode = argument[0];

// clears le menus
ds_list_clear(display_list);
ds_list_clear(menu_name);
ds_list_clear(menu_description);
ds_list_clear(menu_script);

var len = ds_list_size(bird_list);

if(sort_mode == SORT_MODE_INDEX){
	ds_list_copy(display_list,bird_list);
	ds_list_sort(display_list,true);
} else if (sort_mode == SORT_MODE_ALPHA){
	var listnames = ds_list_create();
	for(var i = 0; i < len; i++){
		ds_list_add(listnames,objdata_enemy.name[bird_list[|i]]);	
	}
	
	ds_list_sort(listnames,true);
	
	for(var i = 0; i < len; i++){
		ds_list_add(display_list,objdataToIndex(objdata_enemy,listnames[|i]));
	}
	
	ds_list_destroy(listnames);
} else {
	ds_list_copy(display_list,bird_list);
}

// add menu options

for(var i = 0; i < len; i++){
	menuAddOption(objdata_enemy.name[display_list[|i]],"",menuBestiaryMainSelect)	
}

if(menuGetSize() == 0){
	no_creatures = true
	menuAddOption("","",-1);	
}

menu_size = menuGetSize();