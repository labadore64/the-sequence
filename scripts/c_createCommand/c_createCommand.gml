var array = argument[0];

//do stuff

var checkbytes;

checkbytes[0] = C_ARG1;
checkbytes[1] = C_ARG2;
checkbytes[2] = C_ARG3;
checkbytes[3] = C_ARG4;

var inst = instance_create(0,0,CutsceneCommand);

inst.command = array[0]; //command is always the first value.

//skip the command
array = c_array_subarray(array,0,0);

//if the next 4 bytes is the same as the checkbytes, then
//skip them too
if(array_length_1d(array) >= array_length_1d(checkbytes)){
	if(array[0] == checkbytes[0] &&	
	array[1] == checkbytes[1] &&	
	array[2] == checkbytes[2] &&	
	array[3] == checkbytes[3]){
		array = c_array_subarray(array,0,3)
	}
}

var secArray;

while(array != -1){
	secArray = c_array_readuntil(checkbytes,array);
	
	with(inst){
		ds_list_add(args,secArray);
	}
	
	array = c_array_subarray(array,0,array_length_1d(secArray)+3);
}