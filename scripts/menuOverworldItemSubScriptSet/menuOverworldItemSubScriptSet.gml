var item_selected = -1;
var cando = -1;

with(menuOverworldItems){
	surface_update = true;
	item_selected = item_id [| menupos];	
	cando = overworld_effect;
}

if(cando != -1){
	if(obj_data.set_item == item_selected){
		obj_data.set_item = -1;	
	} else {
		obj_data.set_item = item_selected;		
	}
} else {
	soundfxPlay(soundMenuInvalid)
}

menuControlForceDrawMenuBackground();