var time = argument[0];
var colorz = argument[1];
var fade_type = argument[2]

var returner = instance_create(0,0,screenFade);

returner.calling_obj = id;
returner.fade_type = fade_type;
returner.fade_time = time;
returner.fade_color = colorz;

with(returner){
	event_perform(ev_alarm,0)	
}

return returner;