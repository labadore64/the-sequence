
	draw_set_color(global.textColor)

	draw_set_halign(fa_left)

	var stat_pox = 440;
	var stat_poy = 140;



	draw_text_transformed_ratio(stat_pox,stat_poy,"Damage:",3,3,0)

	var bar_width = 160;
	var bar_height = 20;
	var bar_xx = 175;
	var bar_yy = 5-2;

	draw_set_color($111111)

	draw_rectangle_ratio(bar_xx+stat_pox-2,bar_yy+stat_poy-2,bar_xx+stat_pox+bar_width+2,bar_yy+stat_poy+bar_height+2,false)

	draw_set_color($000000)

	draw_rectangle_ratio(bar_xx+stat_pox,bar_yy+stat_poy,bar_xx+stat_pox+bar_width,bar_yy+stat_poy+bar_height,false)

	draw_set_color(global.textColor);

	if(multtt != 0){
		draw_rectangle_ratio(bar_xx+stat_pox,bar_yy+stat_poy,bar_xx+stat_pox+bar_width*multtt,bar_yy+stat_poy+bar_height,false)
	}

	var stat_poy_target = 120+60;

	var sprite_xx = 210
	var sprite_yy = 10;

	draw_text_transformed_ratio(stat_pox,stat_poy+stat_poy_target,"Element:",3,3,0)
	if(element_sprite >= 0){
		draw_sprite_extended_ratio(element_sprite,0,stat_pox+sprite_xx,stat_poy+stat_poy_target+sprite_yy,1.5,1.5,0,c_white,1)
	}
	
	var stat_poy_target = 120+120;

	sprite_xx = 130
	sprite_yy = -10;

	draw_text_transformed_ratio(stat_pox,stat_poy+stat_poy_target,"Type:",3,3,0)
	if(move_type == MOVE_TYPE_PHYSICAL){
		draw_sprite_extended_ratio(spr_physpower_icon,0,stat_pox+sprite_xx,stat_poy+stat_poy_target+sprite_yy,1.5,1.5,0,c_white,1)
	} else {
		draw_sprite_extended_ratio(spr_magpower_icon,0,stat_pox+sprite_xx,stat_poy+stat_poy_target+sprite_yy,1.5,1.5,0,c_white,1)
	}

	var stat_poy_target = 60+60;

	draw_text_transformed_ratio(stat_pox,stat_poy+stat_poy_target,"Target: " + string(target_number),3,3,0)

	//add MP cost

	var stat_poy_mpcost = 60;


	draw_text_transformed_ratio(stat_pox,stat_poy+stat_poy_mpcost,"Cost:",3,3,0)

	draw_set_color($111111)

	draw_rectangle_ratio(bar_xx+stat_pox-2,bar_yy+stat_poy-2+stat_poy_mpcost,bar_xx+stat_pox+bar_width+2,bar_yy+stat_poy+bar_height+2+stat_poy_mpcost,false)

	draw_set_color($000000)

	draw_rectangle_ratio(bar_xx+stat_pox,bar_yy+stat_poy+stat_poy_mpcost,bar_xx+stat_pox+bar_width,bar_yy+stat_poy+bar_height+stat_poy_mpcost,false)

	draw_set_color(global.textColor);


	if(multttz != 0){
		draw_rectangle_ratio(bar_xx+stat_pox,bar_yy+stat_poy+stat_poy_mpcost,bar_xx+stat_pox+bar_width*multttz,bar_yy+stat_poy+bar_height+stat_poy_mpcost,false)
	}
