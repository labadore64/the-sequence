/// @description - add new menu option
/// @function keybind_get(name,description,script)
/// @param name
/// @param description
/// @param script

var sizer = ds_list_size(menu_name);

ds_list_add(menu_name,argument[0]); //name of all menu items
ds_list_add(menu_description,argument[1]); //descriptions of all menu items
ds_list_add(menu_script,argument[2]); //scripts for all menu items

menu_name_array[sizer] = argument[0];
menu_desc_array[sizer] = argument[1];