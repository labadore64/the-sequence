var filename = working_directory + "save\\";

var map = ds_map_secure_load(filename +"option.sav");

if(ds_exists(map,ds_type_map)){
	
	if(!is_undefined(map[? "compatibility_version"])){
		var compat = map[? "compatibility_version"];
		if(!is_real(compat)){
			compat = real(compat);
		}
		
		return compat == GAME_COMPATIBILITY_VERSION
	}
}
return false;