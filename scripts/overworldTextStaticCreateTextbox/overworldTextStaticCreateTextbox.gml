if(instance_exists(Player)){
	if(!menuControl.help_open){
		if(!instance_exists(TextBox)){
			if(Player.active){
				with(Player){
					active = false;	
				}
		
				var ob = textboxOverworld(text_box_id)
				ob.triggered_by_player = true;
			}
		}
	}
}