var stringer = string_lower(argument[0]);

if(stringer == "battle_effect"){
	return ITEM_TYPE_BATTLE_EFFECT;	
} else if (stringer == "equip"){
	return ITEM_TYPE_EQUIP;	
} else if (stringer == "heal"){
	return ITEM_TYPE_HEAL;	
} else if (stringer == "other"){
	return ITEM_TYPE_OTHER;	
} else if (stringer == "overworld"){
	return ITEM_TYPE_OVERWORLD_EFFECT;	
} else if (stringer == "book"){
	return ITEM_TYPE_BOOK	
} else if (stringer == "food"){
	return ITEM_TYPE_FOOD	
} else if (stringer == "craft"){
	return ITEM_TYPE_CRAFT	
} else if (stringer == "decor"){
	return ITEM_TYPE_DECOR
}



return -1;