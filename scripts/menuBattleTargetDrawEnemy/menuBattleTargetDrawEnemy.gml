if(character > 0){
	draw_set_color(global.textColor);

	draw_set_font(global.largeFont);
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(400,500,character.name,2,2,0);

	draw_set_color(global.textColor);
	draw_set_font(global.textFont)
	draw_text_transformed_ratio(400,540,character.flavor_text[flavor_which],2,2,0);

	draw_set_halign(fa_left)

	if(light_aura){
		menuBattleTargetDrawLight();	
	}
}