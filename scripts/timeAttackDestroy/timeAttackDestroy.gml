
	var obj = instance_create(0,0,menuBrailleComplete);
	obj.myscore = floor(current_score*score_multiplier);
	obj.highscore_id = "timeattack_"+string(seconds);
	obj.title = "Time Attack"
	obj.subtitle = string(seconds) + " seconds"
	obj.game_type = brailleGameTimeAttack;