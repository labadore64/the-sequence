//updates the option based on direction.
//-1 = left, 1 = right
if(menupos == 1 || menupos == 2){
	var list_to_check = -1;
	var curpos = 0;

	if(menupos == 1){
		//speed
		list_to_check=axis_strings;
		curpos = global.gamepadRightAxis;
	} else if(menupos == 2){
		//color	
		list_to_check=vib_strings;
		curpos = global.gamepadVibration;
	}

	var directionz = argument[0];
	var list_size = ds_list_size(list_to_check);

	if(directionz == -1){
		//if moving left	
		curpos--;
		if(curpos < 0){
			if(wrapz[menupos-1]){
				curpos = list_size-1;
			} else {
				curpos = 0;	
			}
		}
	} else if (directionz == 1){
		//if moving right	
		curpos++;
		if(curpos > list_size-1){
			if(wrapz[menupos-1]){
				curpos = 0;	
			} else {
				curpos = list_size - 1;	
			}
		}
	}
	//finally set values accordingly
	if(menupos == 1){
		global.gamepadRightAxis = curpos;
	} else if (menupos == 2){
		global.gamepadVibration = curpos;
		if(global.gamepadVibration){
			gamepad_rumble(1,13);
		}
	}

	curposs[0] = global.gamepadRightAxis
	curposs[1] = global.gamepadVibration

	menuParentUpdateLookAndFeel()

	//menuOverworldOptionInfokeyStrings();

	soundfxPlay(sound_select)
}