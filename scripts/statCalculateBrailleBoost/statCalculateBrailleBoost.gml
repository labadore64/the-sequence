// arg0 = tablet power
// arg1 = level multiplier

return ceil(argument[1] * argument[0] * BATTLE_STAT_MULTIPLIER_TABLET);