
with(argument[1]){
	last_character = cell_character;
	cell_character = string_lower(argument[0]);

	var braillecell = ds_map_find_value(argument[2],cell_character)

	if(!is_undefined(braillecell) && is_array(braillecell)){
		for(i = 0; i < 2; i++){
			for(j = 0; j < 3; j++){
				cell[i,j] = braillecell[i,j]
			}
		}
	} else {
		for(i = 0; i < 2; i++){
			for(j = 0; j < 3; j++){
				cell[i,j] = false
			}
		}
	}

	cells_changed = true;

	if(string_length(cell_character) == 1){
		draw_character = true;	
	} else {
		draw_character = false;	
	}
}