
if(cango){
	mouseHandler_clear_main()
	dataLoad();

	var roomy = instance_create(0,0,roomTransition);
	roomy.room_id = obj_data.current_room;
	roomy.x_pos = obj_data.pos_x;
	roomy.y_pos = obj_data.pos_y;
	roomy.play_sound = false;

	menu_select_hold = -1;
	cango = false;
}