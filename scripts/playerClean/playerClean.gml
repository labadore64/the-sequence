gml_pragma("forceinline");

if(surface_exists(surface)){
	surface_free(surface);	
}

with(CameraOverworld){
	instance_destroy();	
}

if(ds_exists(dir_queue,ds_type_queue)){
	ds_queue_destroy(dir_queue);	
}

if(ds_exists(calc_queue,ds_type_queue)){
	ds_queue_destroy(calc_queue);	
}

with(CollisionDetector){
	instance_destroy();	
}

part_emitter_destroy_all(sname)
part_system_destroy(sname)

if(surface_exists(spray_surface)){
	surface_free(spray_surface)	
}