

if(draw_centered){
	x1 = x-draw_width*.5;
	y1 = y-draw_height*.5;
	x2 = x+draw_width*.5;
	y2 = y+draw_height*.5;
} else {
	x1 = x;
	y1 = y;
	x2 = x+draw_width;
	y2 = y+draw_height;
}

menuParent_titlex = x1+title_x_offset;
menuParent_titley = y1+title_y_offset;
menuParent_subtitlex = x1+subtitle_x_offset;
menuParent_subtitley = y1+subtitle_y_offset;

menuParent_xx = x1+option_x_offset;
menuParent_yy = y1+option_y_offset