/// @description - test collision for object
/// @function collisionTest(collision_id,x,y)
/// @param collision_id
/// @param x
/// @param y


//var argument[0] = argument[0];
//var argument[1] = argument[1];
//var argument[2] = argument[2];

var returner = false;

if(!argument[0].active){
	return false;	
}

//first determine what side the collision would be on


//first line = center of collision to player
//second line = line of each

var plx = argument[3]
var ply = argument[4]

line12_test = playerCheckIntersect(plx,ply,x,y,topleftx,toplefty,toprightx,toprighty);
line23_test = playerCheckIntersect(plx,ply,x,y,toprightx,toprighty,bottomrightx,bottomrighty)
line34_test = playerCheckIntersect(plx,ply,x,y,bottomrightx,bottomrighty,bottomleftx,bottomlefty)
line41_test = playerCheckIntersect(plx,ply,x,y,bottomleftx,bottomlefty,topleftx,toplefty)

//test the top side.
returner = point_in_triangle(argument[1], argument[2], argument[0].topleftx, argument[0].toplefty, argument[0].toprightx, argument[0].toprighty, argument[0].bottomrightx, argument[0].bottomrighty);

if(returner){
	
	var intersect = 0;
	
	if(line12_test){
		intersect = line12_angle
	} else if(line23_test){
		intersect = line23_angle
	} else if(line34_test){
		intersect = line34_angle
	} else if(line41_test){
		intersect = line41_angle
	}
	
	with(Player){
		collision_interacting = intersect;
	}
	
	return returner;	
}

returner = point_in_triangle(argument[1], argument[2], argument[0].topleftx, argument[0].toplefty, argument[0].bottomleftx, argument[0].bottomlefty, argument[0].bottomrightx, argument[0].bottomrighty);

if(returner){
	
	var intersect = -1;
	
	if(line12_test){
		intersect = line12_angle
	} else if(line23_test){
		intersect = line23_angle
	} else if(line34_test){
		intersect = line34_angle
	} else if(line41_test){
		intersect = line41_angle
	}
	
	with(Player){
		collision_interacting = intersect;
	}
		
}

return returner;
