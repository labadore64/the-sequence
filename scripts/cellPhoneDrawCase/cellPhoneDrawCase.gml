with(menuCellPhone){
	gpu_set_colorwriteenable(true,true,true,true)
	draw_set_color($333333);

	draw_rectangle_ratio(x1,y1,x2,y1+option_y_offset - option_space*0.25,false)

	draw_sprite_extended_ratio(spr_cellphone_signal,cell_signal,x2-45,y1+35,1,1,0,c_white,1)

	for(var i = 0; i < 3; i++){
		if(menu_icon[i] > -1){
			draw_sprite_extended_ratio(menu_icon[i],0,x1+45+70*i,y1+35,1,1,0,c_white,1)	
		}
	}
	
	draw_set_color(c_black)
	
	draw_sprite_extended_ratio(spr_cell_phone_menu,0,0,translation,1,1,0,c_white,1)
}

draw_letterbox();