
if(animation_len == 0){
	//menuParentDrawBox();

	draw_clear(c_black)
	
	if(drawGradient){
		draw_rectangle_color_ratio(x1,0,x2,0+gradient_height,gradient_color1,gradient_color2,gradient_color2,gradient_color1,false)
	}

	menuParentDrawText();
	
	menuBirdGuideDrawBird();
	
	draw_set_color(global.textColor)
	draw_set_font(global.textFont)
	draw_set_halign(fa_right)
	draw_text_transformed_ratio(65-5,570,string(menupos+1),2,2,0)
	
	draw_set_halign(fa_center)
	draw_text_transformed_ratio(65,570,"/",2,2,0)
	
	draw_set_halign(fa_left)
	draw_text_transformed_ratio(65+5,570,string(menu_size),2,2,0)
	
	
	gpu_set_colorwriteenable(true,true,true,true)

} else {
	//draw_clear(c_black)
	//menuParentDrawBoxOpen();
}
