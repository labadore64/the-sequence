//parse through the commands.
var blockToRun = argument[0]

//gets the actual block to run as an object instead of an index.
with(CutsceneParser){
	blockToRun = ds_list_find_value(block_list,blockToRun);
}

with(blockToRun){
	var command_temp = -1;

	for(var i = 0; i < array_length_1d(commands); i++){
		command_temp[i] = commands[i];	
	}

	var checkbytes;

	checkbytes[0] = C_START_COMMAND1;
	checkbytes[1] = C_START_COMMAND2;
	checkbytes[2] = C_START_COMMAND3;
	checkbytes[3] = C_START_COMMAND4;


	var arrayer;

	while(command_temp != -1){
		arrayer = c_array_readuntil(checkbytes,command_temp)
	
		c_createCommand(arrayer);
	
		command_temp = c_array_subarray(command_temp,0,array_length_1d(arrayer)+3);
	}
}