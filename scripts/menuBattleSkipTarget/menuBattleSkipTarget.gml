
	var returner = false;
	var obj = argument[0];
	moveData(obj);
	
	if(target_self || target_skip){
		return true;	
	}

	if(!target_ally){
		if(character != -1){
			if(character.ability == "blind" || battlerHasFilter(character,"blind")){
				returner = true;	
			}
		}
	}

	return returner;
