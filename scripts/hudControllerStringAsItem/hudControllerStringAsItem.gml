var stringer = argument[0];

var nothing = global.langini[4,7]
var stringcheck = ":as_item"
var stringchecklen = string_length(stringcheck);

var position = string_pos(stringcheck,stringer);
var initpos = position;
if(position > -1){
	while(position >= 1){
		while(!(
				string_char_at(stringer,position) == "[" &&
				string_char_at(stringer,position-1) == "["
			) && position >= 1){
			position--;
		}
		position++;
		var val = string_replace_all(string_copy(stringer,position,initpos-position-2)," ","");
	
		if(val != "-1"){
			val = string_digits(val);
		
			if(val != ""){
				val = objdata_items.name[real(val)];
			} else {
				val = nothing
			}
		} else {
			val = nothing
		}
	
		stringer=string_delete(stringer,position-2,((initpos-position)+stringchecklen+2));
		stringer=string_insert(val,stringer,position-2);
		position = string_pos(stringcheck,stringer);
		initpos = position;
	}
}

return stringer;