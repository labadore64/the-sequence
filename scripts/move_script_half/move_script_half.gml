//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|0];

stringer = string_replace_all(stringer,"@0",chara.name);

battleAttackInputAddString(stringer);

var stringer = battletext[|1];

stringer = string_replace_all(stringer,"@0",chara.name);
stringer = string_replace_all(stringer,"@1",targe.name);

battleAttackInputAddString(stringer);


var hp_set = ceil(targe.hp.base*.5)

targe.damage_calc = targe.hp.current - hp_set;

ds_list_destroy(battletext);