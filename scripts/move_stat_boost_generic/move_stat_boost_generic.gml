
	var chara = argument[0];
	var targe = argument[1];

	var map = argument[2]; //map of stat boosts
	var turns = argument[3];



	var size, key, i;
	size = ds_map_size(map);
	key = ds_map_find_first(map);
	var amount;
	var basestring;
	for (i = 0; i < size; i++;)
	{
		amount = ds_map_find_value(map,key)
		battleStatChangeAdd(targe,key,amount,turns);
		if(amount > 1){
			basestring = global.langini[5,0];
		} else {
			basestring = global.langini[5,1];
		}
		
		battleAttackInputAddString(replaceStringWithVars(basestring,chara.name,global.langini[5,key+2]))
		
		
		key = ds_map_find_next(map, key);
	}

	battleAttackInputAddString(" ");

	ds_map_destroy(map);

