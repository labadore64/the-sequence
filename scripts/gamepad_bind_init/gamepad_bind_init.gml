/// @function gamepad_bind_init()
/// @description Initializes the game's default gamepad binding state.

//gamepad binds

gamepad_bind_set("left",gp_padl);
gamepad_bind_set("right",gp_padr);
gamepad_bind_set("up",gp_padu);
gamepad_bind_set("down",gp_padd);

gamepad_bind_set("lefttab",gp_shoulderrb);
gamepad_bind_set("righttab",gp_shoulderr);

gamepad_bind_set("control",gp_face4);

gamepad_bind_set("cancel",gp_face2);
gamepad_bind_set("select",gp_face1);
gamepad_bind_set("select2",-1);

//overworld gamepadbinds

gamepad_bind_set("open_menu",gp_start);
gamepad_bind_set("run",gp_face3);

gamepad_bind_set("scopeFunc1",gp_padu);
gamepad_bind_set("scopeFunc2",gp_padd);

gamepad_bind_set("accessMenu", gp_stickr);
gamepad_bind_set("bookmark",gp_stickl);

gamepad_bind_set("sight",gp_shoulderrb)
gamepad_bind_set("coordinates",gp_shoulderr);

// additional keys for accessibility
gamepad_bind_set("access1",-1);
gamepad_bind_set("access2",-1);
gamepad_bind_set("access3",-1);
gamepad_bind_set("access4",-1);
gamepad_bind_set("access5",-1);
gamepad_bind_set("access6",-1);
gamepad_bind_set("access7",-1);
gamepad_bind_set("access8",-1);
gamepad_bind_set("access9",-1);
gamepad_bind_set("access0",-1);

// new keys
gamepad_bind_set("exit",gp_select);
gamepad_bind_set("help",gp_shoulderl);
gamepad_bind_set("hud",gp_shoulderlb);
gamepad_bind_set("screenshot",gp_shoulderrb)

for(var i = 0; i < global.gamepad_size; i++){
	global.gamepad_state_array[i] = KEY_STATE_NONE;
}

//axis stuff
axis = 0;

axisv = 0;
axish = 0;
axis_angle = 0;
axis_amount = 0;

axis_angle_last = 0;
axis_amount_last = 0;

axis_changed = false;

right_axis = global.gamepadRightAxis;

axis_start = false;

axis_min = .5

axis_direction = AXIS_LEFT;

axis_dead_angle = 0.2;

axis_left_pressed = false;
axis_right_pressed = false;
axis_up_pressed = false;
axis_down_pressed = false;