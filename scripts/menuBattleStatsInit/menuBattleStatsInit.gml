
party = ds_list_create();

status_turns = "";

var partyy = party;

with(BattleHandler){
	var sizer = ds_list_size(party_list);
	for(var i =0; i < sizer; i++){
		ds_list_add(partyy,party_list[|i])	
	}
}

var sizer = ds_list_size(party);

var obj = -1;

for(var i = 0; i < sizer; i++){
	obj = party[|i];
	if(!is_undefined(obj)){
		menuAddOption(obj.name,"",-1)
	}
}

menu_size = menuGetSize();

menuParentSetTitle("Stats")
menuParentSetSubtitle("");

menu_draw = menuBattleStatsDraw

draw_bg = false;

//menu_cancel = -1;

menu_left = menuBattleStatsLeft;
menu_right = menuBattleStatsRight;

menu_up = menuBattleStatsUp;
menu_down = menuBattleStatsDown;

//menu_control = menuBattleStatsControl;

var drawstat = instance_create_depth(0,0,0,dataDrawStat);

var obj = party[|menupos];

chaa = obj;
with(dataDrawStat){
drawHexStatSetBoosts(obj.phy_power.braille_stat,obj.phy_guard.braille_stat,obj.mag_power.braille_stat,obj.mag_guard.braille_stat,
							obj.spe_power.braille_stat,obj.spe_guard.braille_stat)
}

menuBattleStatsUpdateStats(obj,min(obj.phy_power.base,obj.phy_power.braille_stat),
								min(obj.phy_guard.base,obj.phy_guard.braille_stat),
								min(obj.mag_power.base,obj.mag_power.braille_stat),
								min(obj.mag_guard.base,obj.mag_guard.braille_stat),
								min(obj.spe_power.base,obj.spe_power.braille_stat),
								min(obj.spe_guard.base,obj.spe_guard.braille_stat));
	
status_count = ds_list_size(chaa.status_effects);
status_pos = 0;
current_status = noone;

if(status_count > 0){
	current_status = obj.status_effects[|0];		
	if(current_status > 0){
		status_turns = string_replace_all(string(current_status.turns),"-1","@");
	}
}
							
portrait = -1;
draw_portrait = true;

if(draw_portrait){
	portrait = instance_create_depth(0,0,0,drawPortrait)
	portrait.character = obj.character
}
