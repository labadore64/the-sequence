
if(surface_exists(menuControlSurface)){
	var stack = -1;
	gpu_set_colorwriteenable(true,true,true,true)
	with(menuControl){
		surface_set_target(menuControlSurface);
	
		draw_clear_alpha(c_black,0);
	
		stack = ds_stack_create();
	
		ds_stack_copy(stack,menu_stack);
	

	}

	if(stack != -1){

		var stacksize = ds_stack_size(stack);

		var holder = -1;
		if(!draw_active_menu_uwu){
			ds_stack_pop(stack);
		}
		
		var newstack = ds_stack_create();
		
		for(var i = 1; i < stacksize; i++){
			holder = ds_stack_pop(stack);
			if(!is_undefined(holder)){
				ds_stack_push(newstack,holder);
			}
		}

	
		stacksize = ds_stack_size(newstack);
		//if(!menuControl.delay){
		for(var i = 0; i < stacksize; i++){
			holder = ds_stack_pop(newstack);
			if(!is_undefined(holder)){
				with(holder){
					if(menu_draw != -1){
						if(visible){
							if(!do_not_draw_me){
								gpu_set_colorwriteenable(true,true,true,false)

								script_execute(menu_draw);	
							}
						}
					}
				}
			}
		}
		//}
	
		ds_stack_destroy(stack);
		ds_stack_destroy(newstack);
	
		surface_reset_target();
	
	}
}