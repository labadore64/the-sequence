if(!name_input){

	var sizerx = box_width*.5;
	var sizery = box_height*.5;

	if(active){
		enterTextDrawBox();	
		draw_set_valign(fa_middle)
		var stringsize = string_length(current_text);
	
		for(var i = 1; i < stringsize+1; i++){
			draw_text_transformed_ratio(x-sizerx+box_x_offset + ((i-1)*character_space),y+box_y_offset,string_char_at(current_text,i),3,3,0)
		}
		draw_set_valign(fa_top)
	
		var basex = x-sizerx+box_x_offset;
		var pos = position;
		if(pos > max_character+1){
			pos = max_character+1;	
		}
	
		if(blink && state == 0){
			draw_rectangle(basex+(pos-1)*character_space-2.5,y-20,basex+(pos)*character_space-2.5,y+20,false)
		}
	
		if((state == 1)){
			enterTextDrawVerify()
		}
	}
} else {

}