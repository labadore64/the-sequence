//rotation
if(!ds_list_empty(rotation_amounts)){
	orientation_last = orientation;
	orientation = rotation_amounts[|0];
	var test_delta = (orientation+720)-(orientation_last+720);
	if(abs(test_delta) < 20){
		orientation_delta = test_delta;
	}
	if(abs(test_delta) < min_delta){
		orientation_delta = 0
	}
	ds_list_delete(rotation_amounts,0);
	
} else {
	orientation_delta = 0;	
}