if(menu_select != -1){
	if(global.inputEnabled){

		if(double_select){
			if(global.key_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
				global.key_state_array[KEYBOARD_KEY_SELECT2] == KEY_STATE_PRESS ||
				global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

				script_execute(menu_select);
				return true;
			}
		} else {
			if(global.key_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
				global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS) {

				script_execute(menu_select);
				return true;
			}
		}
	}
} 

return false;