/// @description - returns adjusted MAGGUARD stat

var minvalue = 10;

var multiplier = getMultiplierFromLevel(objdata_character.mag_guard_grow[character],level);

var returner = ceil((minvalue + multiplier * (mag_guard_base+(boost_mag_guard)*BATTLE_STAT_MULTIPLIER_ITEM)));

returner += statCalculateBrailleBoost(multiplier,argument[0]);

return returner;