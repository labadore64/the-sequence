var ssound = -1;

with(AXBookmark){
	ssound = sound_emitter.emit_sound;
	audio_stop_sound(ssound);
}