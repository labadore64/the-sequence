var strong = 1.5;
var weak = .66;

if(argument[0] == TYPE_GLITCH){
	if(argument[1] == TYPE_MIND ||
		argument[1] == TYPE_SPIRIT ||
		argument[1] == TYPE_NATURE){
			return strong;
		}
	return 1;
} else if (argument[0] == TYPE_MIND){
	if(argument[1] == TYPE_NATURE){
		return strong;	
	} else if (argument[1] == TYPE_SPIRIT){
		return weak;	
	}
	return 1;
} else if (argument[0] == TYPE_NATURE){
	if(argument[1] == TYPE_SPIRIT){
		return strong;	
	} else if (argument[1] == TYPE_MIND){
		return weak;	
	}
	return 1;
} else if (argument[0] == TYPE_SPIRIT){
	if(argument[1] == TYPE_MIND){
		return strong;	
	} else if (argument[1] == TYPE_NATURE){
		return weak;	
	}
	return 1;
}

return 1;