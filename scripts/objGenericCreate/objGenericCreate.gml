initTimingAlarm();
objCanDoState = false;
active = true;
lock = false;
destroy = -1;
transition = false;
cancelled = false;
double_select = true; //allow both select1 and select2 to be used as select

menu_update_values = -1
menu_up = -1;
menu_down = -1;
menu_left = -1;
menu_right = -1;
menu_select = -1;
menu_select_hold = -1;
menu_cancel = -1;
menu_help = "";
menu_ax_HUD = "";

textbox_hold_counter = 0;
textbox_hold_max = global.text_auto_speed*5;

menu_control = -1; //used for special functions...

menu_hold_up = -1;
menu_hold_down = -1;
menu_hold_left = -1;
menu_hold_right = -1;
menu_hold_zoom_in = -1;
menu_hold_zoom_out = -1;

menu_release_up = -1;
menu_release_down = -1;
menu_release_left = -1;
menu_release_right = -1;

menu_tableft = -1;
menu_tabright = -1;

menu_input_text = -1;

keypress_this_frame = false;

fade_in_script =-1
fade_out_script =-1