with(HUDController){
	
	var size, key, i, lastkey;

	size = ds_map_size(data);
	key = ds_map_find_first(data);

	for (i = 0; i < size; i++;)
	{
		lastkey = key;
		key = ds_map_find_next(data, key);
		
		if(string_pos(argument[0],lastkey) > 0){
			return true;
		}
	}
}
return false;