//draw stat icons
var statspritexoff = argument[0]
var statspriteyoff = argument[1]

draw_sprite_extended_ratio(spr_magpower_icon,0,10+statspritexoff-75,35+statspriteyoff+62,1,1,0,$FF7FFF,1)
draw_sprite_extended_ratio(spr_physguard_icon,0,170+statspritexoff,35+statspriteyoff+185,1,1,0,$FFFF7F,1)
draw_sprite_extended_ratio(spr_magguard_icon,0,170+statspritexoff-165,35+statspriteyoff+185,1,1,0,$7FFF7F,1)
draw_sprite_extended_ratio(spr_physpower_icon,0,170+statspritexoff+65,35+statspriteyoff+62,1,1,0,$7F7FFF,1)	
draw_sprite_extended_ratio(spr_speguard_icon,0,170+statspritexoff-165,35+statspriteyoff-65,1,1,0,$FF7F7F,1)
draw_sprite_extended_ratio(spr_spepower_icon,0,170+statspritexoff,35+statspriteyoff-65,1,1,0,$7FFFFF,1)	

with(dataDrawStat){
	draw_surface(surface_hex,
				global.display_x+(statspritexoff-45)*global.scale_factor,
				global.display_y+(statspriteyoff-40)*global.scale_factor);	
}