with(Player){
	draw_action = PLAYER_ACTION_STAND; 	
	playerStandSprite();
	active = false;
}

var newchara = instance_create(0,0,dataCharacter);

newchara.temporary = true;
newchara.character = character.character
newchara.newalarm[0] = 1;

var dia = instance_create(0,0,DialogHandler);

dia.character = newchara;
dia.portrait.character = character.character

dia.dialogRef = "call"

lock = true;