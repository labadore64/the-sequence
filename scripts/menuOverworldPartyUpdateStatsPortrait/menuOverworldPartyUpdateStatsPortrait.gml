chaa = argument[0];
var radd = argument[1];
var port = argument[2]

get_colorz = chaa.color;

var myred = color_get_red(get_colorz);
var mygreen = color_get_green(get_colorz);
var myblue = color_get_blue(get_colorz)

red_ratio = (myred-127)/127;
green_ratio = (mygreen-127)/127;
blue_ratio = (myblue-127)/127;

red_color = make_color_rgb(myred,255-myred,255-myred);
green_color = make_color_rgb(255-mygreen,mygreen,255-mygreen);
blue_color = make_color_rgb(255-myblue,255-myblue,myblue);

with(port){
	character = argument[0];
	draw_hex_init(character,radd,character.phy_power,
				character.phy_guard ,
				character.mag_power ,
				character.mag_guard ,
				character.spe_agility ,
				character.spe_mpr);
	radius = radd;
}