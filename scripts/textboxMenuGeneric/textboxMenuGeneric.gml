var choices = argument[1];
var choices_scripts = argument[2];

var teee = instance_create_depth(0,30,0,TextBoxChoice);
teee.text_id = argument[0]
teee.triggered_by_player = true;
teee.char_per_line = 50
teee.parent = id;

with(Player){
	active=false;
	draw_action = PLAYER_ACTION_STAND; 
	playerUpdateSprite();
}

with(teee){
	ds_list_copy(textbox_choices,choices);
	ds_list_copy(textbox_choices_script,choices_scripts);
}

return teee;