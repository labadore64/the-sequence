//draw_set_color(title_color)

//draw title

gpu_set_colorwriteenable(true,true,true,false)
if(drawTitle){
	drawOutlineText(menuParent_titlex,menuParent_titley,title,titleFontSize,titleFontSize,0,title_color)
}
//draw_set_color(subtitle_color)

//draw subtitle
if(drawSubtitle){
	drawOutlineText(menuParent_subtitlex,menuParent_subtitley,subtitle,subtitleFontSize,subtitleFontSize,0,subtitle_color)
}
gpu_set_colorwriteenable(true,true,true,true)