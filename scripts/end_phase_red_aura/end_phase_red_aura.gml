var nope = argument[0];
var perc_damage = .125

battleTextDisplayClearText();

with(Battler){
	
	if(!battlerHasFilter(id,"Talk")){
		if(is_enemy){
			if(battlerIsAlive(id)){
				if(hp.current/hp.base > .5){
					var damg = ceil(hp.base * perc_damage * .5);
				} else {
					var damg = ceil(hp.current * perc_damage);
				}
				damg = clamp(damg,1,99);
			
				var perc_damg = clamp(floor(100*damg/hp.base),1,100);
		
				var changed = hp.current - damg;
		
				battlerHPChange(id,changed)
				battleAnimHit(id);
		
				var stringer = "@0 took @1% damage from the searing flames!"
		
				stringer = string_replace_all(stringer,"@0",name);
				stringer = string_replace_all(stringer,"@1",string(perc_damg));
				battleTextDisplayAddText(stringer);
		
				if(!battlerIsAlive(id)){
					stringer = "@0 was defeated!"
					stringer = string_replace_all(stringer,"@0",name);
					battleTextDisplayAddText(stringer);
				}
			}
		}
	}
	
}

battleTextDisplayAddText(" ");