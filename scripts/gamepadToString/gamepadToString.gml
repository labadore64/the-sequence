var button = argument[0]

switch(button){
	case gp_face1:
	return "Button 1"
	
	case gp_face2:
	return "Button 2"
	
	case gp_face3:
	return "Button 3"
	
	case gp_face4:
	return "Button 4"
	
	case gp_padu:
	return "DPad Up"
	
	case gp_padd:
	return "DPad Down"
	
	case gp_padl:
	return "DPad Left"
	
	case gp_padr:
	return "DPad Right"
	
	case gp_start:
	return "Start"
	
	case gp_select:
	return "Select"
	
	case gp_stickl:
	return "Left Stick"
	
	case gp_stickr:
	return "Right Stick"
	
	case gp_shoulderl:
	return "Left 1"
	
	case gp_shoulderr:
	return "Right 1"
	
	case gp_shoulderlb:
	return "Left 2"
	
	case gp_shoulderrb:
	return "Right 2"
}