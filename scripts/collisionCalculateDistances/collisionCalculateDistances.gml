for(var i = 0; i < 4; i++){
	player_coll[i] = 100000000; //distance from coll.	
}
sight_dist = 100000000;

x_outofrange = false;
y_outofrange = false;

leftcol = false;
rightcol = false;
upcol = false;
downcol = false;

sightcol = false;

if(instance_exists(Player)){
	if(Player.collision[0] != noone){
		leftcol =  collisionTestDistance(Player.collision[AX_COLLISION_DIRECTION_LEFT].orientation,global.AXCollisionDistance)

		rightcol =  collisionTestDistance(Player.collision[AX_COLLISION_DIRECTION_RIGHT].orientation,global.AXCollisionDistance)

		upcol =  collisionTestDistance(Player.collision[AX_COLLISION_DIRECTION_UP].orientation,global.AXCollisionDistance)

		downcol =  collisionTestDistance(Player.collision[AX_COLLISION_DIRECTION_DOWN].orientation,global.AXCollisionDistance)
	
		sightcol =  collisionTestDistance(Player.sight_detector.orientation,global.AXSightDistance)
			
		for(var i = 0; i < 4; i++){
			player_coll[i] = player_coll_last[i];
		}	
			
		if(upcol != 0 &&
			downcol != 0 &&
			leftcol != 0 &&
			rightcol != 0){
				if(leftcol != 0){
					player_coll[AX_COLLISION_DIRECTION_LEFT] =leftcol
				}
				if(rightcol != 0){
					player_coll[AX_COLLISION_DIRECTION_RIGHT] =rightcol
				}
				if(upcol != 0){
					player_coll[AX_COLLISION_DIRECTION_UP] =upcol
				}
				if(downcol != 0){
					player_coll[AX_COLLISION_DIRECTION_DOWN] = downcol
				}
					
				for(var i = 0; i < 4; i++){
					player_coll_last[i] = player_coll[i];
				}
			}
	}
}