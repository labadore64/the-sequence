var chara = argument[0];
var mov = argument[1];
var status_max = argument[2]; //how many characters maximum will have status applied.
var status_aura = argument[3]; //is the character aware of the aura, if an enemy?
var status_filter = argument[4]; //is the character aware of statuses that would make this status unable to be applied?

moveData(mov);

//returns true if cando, false if no
var returner = true;

if(move_filter[0] != ""){
	var fil = move_filter[0];
	
	if(target_self){
		if(battlerHasFilterIgnoreNull(chara,fil)){
			returner = false;	
		}
	} else {
	
		if(target_ally){
			var filtercount = battlerCountFilter(chara,fil);
	
			if(filtercount <= status_max){
				returner = false;
			}
	
		} else {
			var testobj = BattleHandler.party_list[|0];
			
			if(!chara.is_enemy){
				testobj = BattleHandler.enemy_list[|0];
			}
			
			var filtercount = battlerCountFilter(testobj,fil);
	
			if(filtercount <= status_max){
				returner = false;
			}
		}
	
		if(status_aura){
			var cando = false;
			
			if(target_ally && !chara.is_enemy){
				cando = true;	
			}
			
			if(!target_ally && chara.is_enemy){
				cando = true;	
			}
			
			if(cando){
				if(BattleHandler.protect_status){
					returner = false;	
				}
			}
		}
		
		if(status_filter){
			if(!battlerFilterCanDo(chara,fil)){
				returner = false;
			}
		}
	}
}

return returner;