var task = objdataToIndex(objdata_task,argument[0])

if(task > -1){
	if(eventNormalCheckIndex(objdata_task.event_id[task])
		&& !eventNormalCheckIndex(objdata_task.completed_event_id[task])){
		with(obj_data){
			newalarm[2] = argument[1];	
			task_buffer = task
		}
	}
}