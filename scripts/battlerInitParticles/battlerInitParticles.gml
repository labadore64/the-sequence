
//particles
//the following is for tracking how the particles should move for this character.
part_closer = false;//grows if true. shrinks if false.
part_direction = 45; //direction of particles.
part_base_speed = .01
part_number = 2
part_time = 0;

part_damage_trigger = false

damage_part_system = part_system_create()
    
part_system_automatic_draw(damage_part_system, false);
part_system_automatic_update(damage_part_system,false)

damage_part_emitter = part_emitter_create(damage_part_system);
part_emitter_region(damage_part_system,damage_part_emitter,0,800,0,600,ps_shape_ellipse,ps_distr_gaussian)
