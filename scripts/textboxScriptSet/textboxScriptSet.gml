/// @description - sets a script for a specific offset
/// @param textbox
/// @param script_id
/// @param script_index


//Note:
//Scripts that have an index >= 0 will be executed on the index-th dialog
//scripts that have an index < 0 will be executed on the (n-index)-th dialog
//index >=0 will have priority.

with(argument[0]){
	ds_map_replace(scripts,argument[2],argument[1]);
}