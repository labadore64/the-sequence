if(status_count > 0){
	status_pos--;
	if(status_pos < 0){
		status_pos = status_count-1;	
	}
	
	var obj = party[|menupos];
	current_status = obj.status_effects[|status_pos];	
	if(current_status > 0){
		status_turns = string_replace_all(string(current_status.turns),"-1","@");
	}
	hudPopulateBattleStatus("status",current_status);
}

