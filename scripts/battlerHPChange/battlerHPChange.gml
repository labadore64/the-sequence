with(argument[0]){
	if(!defeat_animation_trigger){
		var mpp = argument[1];
	
		if(mpp > hp.base){
			mpp = hp.base;	
		} else if (mpp <= 0){
			mpp = 0;	
			
			
			// do cry
			with(battleStartAttack){
				lock = true;
			}
			if(character > -1){
				// if ignore cry isn't set, treat the creature as registered!
				// creatures that have their cries ignored must have their
				// cries triggered in other ways, such as with moves.
				if(!objdata_enemy.ignore_cry[character]){
					setEnemyOwnBattle(id);
				}
			}
			if(death_cry != -1){
				var sound = soundPlayCry(death_cry)	
				audio_sound_pitch(sound,random_range(.9,1.1));
			}
			
			newalarm[4] = 2;
			
		}
		var multi = 1;
		if(global.battleHPSound){
			multi = 2	
		}
		
		battleStatSliderSet(hp,mpp,10*multi)
		if(global.battleHPSound){
			if(!is_enemy){
				// start sound
				hp.sound = soundfxPlay(BattleHandler.hp_sound[load_id])
			}
		}
		draw_hp_val = hp.current/hp.base;
		hp_size = hp.current * .1
	}
}