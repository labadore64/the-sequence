
draw_set_color(c_black)
draw_rectangle_ratio(0,550,800,600,false)

gpu_set_colorwriteenable(true,true,true,false);
draw_set_alpha(.75)
draw_set_color(c_black)
draw_rectangle_ratio(0,0,800,600,false);
draw_set_alpha(1)
draw_set_color(global.textColor)

draw_set_halign(fa_left)


if(animation_len == 0){
	menuParentDrawBox();
	
	//menuParentDrawText();
	
	
	menuTabletTakeDrawOptions();
	

	gpu_set_colorwriteenable(true,true,true,true)

	menuParentDrawInfo();

	// draw move stuff
	if(move_id > -1 && character > -1){
		draw_set_color(global.textColor)
		var xx = 500;
		var yy = 75;
	
		with(braille_obj){
			brailleCharacterDrawAgain();
		}
	
		draw_set_halign(fa_center);
		draw_text_transformed_ratio(xx-290,yy,b_bcharacter,4,4,0)
		draw_text_transformed_ratio(xx+25,yy,objdata_moves.name[move_id],4,4,0)
		draw_text_transformed_ratio(xx,yy+50,objdata_moves.flavor[move_id],1,1,0)
	
		draw_text_transformed_ratio(xx-290,yy+185,title,3,3,0)
	
		draw_set_halign(fa_left);

	

		// draw current tablets
	
		var moxx = 400+175;
		var moyy = 400;
		var spacey = 30;
	
		var stat_spacerx = -90;
		var stat_spacery = -235;
	
		with(old_stat){
			draw_surface(surface_hex,global.display_x+(moxx+stat_spacerx)*global.scale_factor,global.display_y+(moyy+stat_spacery)*global.scale_factor);	
		}
	
	
		for(var i =0; i < 3; i++){
			draw_circle_ratio(moxx-10,6+moyy + spacey*i,3,false)
			if(character.tablet[i] != -1){
				draw_text_transformed_ratio(moxx,moyy + spacey*i,objdata_moves.name[objdata_tablet.move_id[character.tablet[i]]],2,2,0)
			} else {
				draw_text_transformed_ratio(moxx,moyy + spacey*i,"--",2,2,0)	
			}
		}
	
		var charaa = character.character;
	
		var mozxx = 350;
		var mozyy = 200;
		var textmv = 140;
		var textmvx = 60;
	
		if(charaa > -1){
			draw_sprite_extended_ratio(objdata_character.face_sprite[charaa],0,mozxx,mozyy,1,1,0,c_white,1);
			draw_set_halign(fa_center);
			draw_text_transformed_ratio(mozxx+textmvx,mozyy+textmv,objdata_character.name[charaa],2,2,0);
			draw_set_halign(fa_left)
		}
	}
}
draw_letterbox()