var batt = argument[0]
var chara = argument[1];

batt.character = chara.character;
batt.name = chara.name;
batt.ax_desc = objdata_character.ax_desc[chara.character];
batt.phy_power.base = chara.phy_power;
batt.phy_guard.base = chara.phy_guard;
batt.mag_power.base = chara.mag_power;
batt.mag_guard.base = chara.mag_guard;
batt.spe_power.base = chara.spe_agility;
batt.spe_guard.base = chara.spe_mpr;
batt.hp.base = chara.hp;
batt.mp.base = chara.mp;

batt.sprite_idle_front=objdata_character.sprite_idle_front[chara.character]
batt.sprite_idle_back=objdata_character.sprite_idle_back[chara.character]
batt.sprite_hit_front=objdata_character.sprite_hit_front[chara.character]
batt.sprite_hit_back=objdata_character.sprite_hit_back[chara.character]
batt.sprite_jump0_front=objdata_character.sprite_jump0_front[chara.character]
batt.sprite_jump0_back=objdata_character.sprite_jump0_back[chara.character]
batt.sprite_jump1_front=objdata_character.sprite_jump1_front[chara.character]
batt.sprite_jump1_back=objdata_character.sprite_jump1_back[chara.character]
batt.sprite_cast0_front=objdata_character.sprite_cast0_front[chara.character]
batt.sprite_cast0_back=objdata_character.sprite_cast0_back[chara.character]
batt.sprite_cast1_front=objdata_character.sprite_cast1_front[chara.character]
batt.sprite_cast1_back=objdata_character.sprite_cast1_back[chara.character]

batt.sprite_default_speed = objdata_character.battle_sprite_default_speed[chara.character]
batt.color = objdata_character.color[chara.character]

batt.ability = objdata_character.ability[chara.character]
batt.battle_desc = objdata_character.battle_desc[chara.character]

for(var i = 0; i < 5; i++){
	batt.item[i] = chara.item[i];	
}

batt.level = chara.level;
batt.experience = chara.experience;
batt.element = objdata_character.element[chara.character]

var sizer = ds_list_size(chara.moves);

for(var i = 0; i < sizer; i++){
	ds_list_add(batt.moves,chara.moves[|i]);	
}
with(batt){
	battlerLoadDeathParticles(batt);
}