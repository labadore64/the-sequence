event_inherited();

statUpdate();

menuOverworldSetStringsAsParty(menuOverworldSubScript) 

menuParentSetTitle(global.langini[2,0])
menuParentSetSubtitle(global.langini[3,0])

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

text_bottomline = global.langini[4,3]

menu_draw = menuOverworldPartyDrawMain

menuParentSetDrawGradient(false)

//menuParentTTSTitleRead();

//menuParentTTSLabelRead();

menuParentUpdateBoxDimension();

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menu_left = menuOverworldPartyLeft;
menu_right = menuOverworldPartyRight;

menu_up = menu_left;
menu_down = menu_right;

aura_draw = merge_color(c_black,obj_data.aura,.4)

draw_red =  merge_color($0000FF,global.textColor,.25)
draw_green = merge_color($00FF00,global.textColor,.25)
draw_blue = merge_color($FF0000,global.textColor,.25)

menuControlForceDrawMenuBackground();