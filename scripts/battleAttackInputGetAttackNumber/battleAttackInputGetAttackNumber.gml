//returns how many attacks a character has.
var char = argument[0]

var lister = -1;

with(BattleHandler){
	lister = attack_input;	
}

var sizer = ds_list_size(lister);

var obj = -1;

var counter = 0;
for(var i = 0; i < sizer; i++){
	obj = lister[|i];
	if(!is_undefined(obj)){
		if(obj.character == char){
			counter++;
		}
	}
}

return counter;