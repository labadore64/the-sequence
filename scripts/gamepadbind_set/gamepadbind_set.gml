/// @description - set keybind value
/// @function keybind_set(bind_name, bind_value)
/// @param bind_name
/// @param bind_value

ds_map_replace(global.gamepad_map,argument[0],argument[1]);