character = party[|menupos];

battleHandlerClearFlash();

character.flash = true;
//battleHandlerFlashSelected();

with(Battler){
	draw_hp = false;
	draw_mp = false;
}
var liiight = light_aura;
with(character){
	draw_hp = true;	
	if(liiight || !is_enemy){
		draw_mp = true;
	}
}

hp_amount = character.hp.current/character.hp.base;

if(hp_amount > .66){
	flavor_which = 0;	
} else if (hp_amount < .33){
	flavor_which = 2;
} else {
	flavor_which = 1;
}

var obj = character;
				
var raddd = 50;

if(character != -1){	
with(dataDrawStat){
	drawHexStatSetBoosts(obj.phy_power.current,obj.phy_guard.current,obj.mag_power.current,obj.mag_guard.current,
							obj.spe_power.current,obj.spe_guard.current)
	
	
	character = obj;
	draw_hex_init(character,raddd,obj.phy_power.base,obj.phy_guard.base,
	obj.mag_power.base,obj.mag_guard.base,
	obj.spe_power.base,obj.spe_guard.base);
	radius = raddd;
	

}

type = character.element
type_weak = getTypeWeakness(character.element);

	type_sprite = getTypeSprite(type);
	type_weak_sprite = getTypeSprite(type_weak);
	
	var moves = character.moves;
	
	var sizer = ds_list_size(moves);
	
	var moveid = 0;
	ds_list_clear(moves_names);
	for(var i =0; i < sizer; i++){
		moveid = moves[|i];
		if(!is_undefined(moveid)){
			moveData(moveid);
			ds_list_add(moves_names,name);
		}
	}
	moves_names_size = ds_list_size(moves_names);
}	
//gotta do this or else targeting messes up
moveData(move)