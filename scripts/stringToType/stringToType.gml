var typename = string_lower(argument[0]);

switch(typename){
	case "nature":
	return  TYPE_NATURE;
	
	case "none":
	return  TYPE_NONE;
	
	
	case "mind":
	return  TYPE_MIND;
	
	
	case "spirit":
	return  TYPE_SPIRIT;
	
	
	case "glitch":
	return  TYPE_GLITCH;
	
}

return -1;