var teee = instance_create_depth(0,30,0,TextBox);
teee.text_id = argument[0]
teee.triggered_by_player = true;
teee.char_per_line = 50
teee.parent = id;

with(Player){
	active=false;
	draw_action = PLAYER_ACTION_STAND; 
	playerUpdateSprite();
}

return teee;