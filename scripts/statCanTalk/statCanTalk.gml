chara = argument[0]

if(string_lower(chara.next_dialog) != "none"){
	
	var dialogFolder = "resources\\dialog\\character" + string(chara.character) + "\\";
	var dialogFile = string_lower(chara.next_dialog) +".ini";
	var filename = dialogFolder+dialogFile;
		
	if(file_exists(filename)){
		return true;
	}
}

return false;