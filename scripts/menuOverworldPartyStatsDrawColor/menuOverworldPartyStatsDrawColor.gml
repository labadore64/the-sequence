draw_set_font(global.textFont)
var coloradjusty = 15;
var colorboxy = coloradjusty+10;

draw_text_transformed_ratio(360,125-coloradjusty,"Color" + ":",2,2,0)
draw_set_halign(fa_right)
draw_text_transformed_ratio(560,120,"Red",1,1,0)
draw_text_transformed_ratio(560,120+30,"Green",1,1,0)
draw_text_transformed_ratio(560,120+60,"Blue",1,1,0)
draw_set_halign(fa_left)
var aura_width = 128*.5;
var aura_height = 32*.5;
var xxx = 230+68+100;
var yyy = 275-100
var textxxx = -200;
var textyyy = -10
var boosttextxxx = 25;
var boosttextyyy = 50

var color_spacer = 30; //y spacer
var color_move = 250;
var color_move_y = 50
var color_height = 12*.5;
var color_width = 160*.5;

draw_set_color($111111);
draw_rectangle(xxx-aura_width-2,yyy-aura_height-2-colorboxy,xxx+aura_width+2,yyy+aura_height+2-colorboxy,false)
for(var i = 0; i < 3; i++){
	draw_rectangle(color_move + xxx-color_width-2,yyy-color_height-2+color_spacer*i-color_move_y,color_move + xxx+color_width+2,yyy+color_height+2+color_spacer*(i)-color_move_y,false)	
}
draw_set_color(c_black)
for(var i = 0; i < 3; i++){
	draw_rectangle(color_move + xxx-color_width,yyy-color_height+color_spacer*i-color_move_y,color_move + xxx+color_width,yyy+color_height+color_spacer*(i)-color_move_y,false)	
}
draw_set_color(chaa.color);
draw_rectangle(xxx-aura_width,yyy-aura_height-colorboxy,xxx+aura_width,yyy+aura_height-colorboxy,false)
draw_set_color(global.textColor);
draw_set_halign(fa_left)

//if(chaa.color != $7F7F7F){
	if(abs(red_ratio) > .1){
		draw_set_color(red_color)
		i = 0;
		var ratio = red_ratio;
		draw_rectangle(color_move + xxx,yyy-color_height+color_spacer*i-color_move_y,color_move + xxx+(color_width*ratio),yyy+color_height+color_spacer*(i)-color_move_y,false)	
	}
	
	if(abs(green_ratio) > .1){
		draw_set_color(green_color)
		i = 1;
		var ratio = green_ratio;
		draw_rectangle(color_move + xxx,yyy-color_height+color_spacer*i-color_move_y,color_move + xxx+(color_width*ratio),yyy+color_height+color_spacer*(i)-color_move_y,false)	
	}
	
	if(abs(blue_ratio) > .1){
		draw_set_color(blue_color)
		i = 2;
		var ratio = blue_ratio;
		draw_rectangle(color_move + xxx,yyy-color_height+color_spacer*i-color_move_y,color_move + xxx+(color_width*ratio),yyy+color_height+color_spacer*(i)-color_move_y,false)	
	}
