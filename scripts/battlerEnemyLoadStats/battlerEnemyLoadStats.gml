var batt = argument[0];
var chara = argument[1];

with(batt){
	character = chara;
	level = BattleHandler.partyLevel;
	enemyGetStats(character);
	moveEnemyLoad(id,level);
	battlerLoadDeathParticles(id);
	
}

