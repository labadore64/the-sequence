
	var howmany = BattleHandler.char_input_counter-1//instance_number(menuBattleFight);

	battleHandlerSetCharacter(howmany);
	battleAttackingInputDeleteLast()

	if(howmany > -1){

		character = BattleHandler.char_input_character;
		character_load = BattleHandler.char_input_counter;

		battleHandlerSelectAllyClear()
		battleHandlerSelectAlly(ds_list_find_index(BattleHandler.allies,character),true);

		battleHandlerClearFlash();
		battleHandlerFlashSelected();
		
		ds_list_clear(menu_name); //name of all menu items
		ds_list_clear(menu_description); //descriptions of all menu items
		ds_list_clear(menu_script); //scripts for all menu items
		
		menuBattleFightPopulateItems()
		
		menu_size = menuGetSize();	

		menupos = 0;
		
		tts_say(character.name + " " + menu_name_array[0])
	} else {
		cancelled = true;
		instance_destroy();
	}

	soundfxPlay(sound_cancel)

	//menuParentCancel();

