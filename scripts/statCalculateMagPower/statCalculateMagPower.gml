/// @description - returns adjusted MAGPOWER stat

var minvalue = 10;

var multiplier = getMultiplierFromLevel(objdata_character.mag_power_grow[character],level);

var returner = ceil((minvalue + multiplier * (mag_power_base+(boost_mag_power)*BATTLE_STAT_MULTIPLIER_ITEM)));

returner += statCalculateBrailleBoost(multiplier,argument[0]);

return returner;