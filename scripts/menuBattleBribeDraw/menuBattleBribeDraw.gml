draw_set_color(c_black)
draw_rectangle_ratio(0,485,800,600,false)

draw_set_font(global.textFont);
draw_set_color(global.textColor);

var height = 505
var move = 500;

draw_set_halign(fa_right)

draw_text_transformed_ratio(move,height,"Current Money: $",3,3,0)

draw_text_transformed_ratio(move,height+40,"Bribe: $",3,3,0)

draw_set_halign(fa_left);

draw_text_transformed_ratio(move,height,string(money),3,3,0)

draw_text_transformed_ratio(move,height+40,string(bribe_amount),3,3,0)