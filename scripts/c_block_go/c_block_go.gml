//this is a generic starter command
//index = index of the header (integer)
//parent = parent of the header (object)
//startpointer = where execution starts (integer)

var index = argument[0];
var parent = argument[1];
var startpointer = argument[2];

c_block_startprocessing(index);
c_block_run(index,parent,startpointer);