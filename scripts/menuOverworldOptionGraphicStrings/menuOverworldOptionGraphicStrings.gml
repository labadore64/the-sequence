mouseHandler_clear_main()
	//menuAddOption("Vignette","Enable the Vignette on the Field.",-1)
	menuAddOption(global.langini[0,36],global.langini[1,36],menuOverworldOptionsGraphicLeft)
	menuAddOption(global.langini[0,37],global.langini[1,37],menuOverworldOptionsGraphicLeft)
	menuAddOption(global.langini[0,38],global.langini[1,38],menuOverworldOptionsGraphicLeft)
	menuAddOption(global.langini[0,56],global.langini[1,56],menuOverworldOptionsGraphicLeft)
	menuAddOption("Animate Menus","Toggle menu animations",menuOverworldOptionsGraphicLeft)
	

	text_speed_strings = ds_list_create();
	ds_list_add(text_speed_strings,LANG_SELECT_ON,LANG_SELECT_OFF);

	text_color_strings = ds_list_create();
	text_color_values = ds_list_create()

	ds_list_add(text_color_strings,LANG_SELECT_OFF,LANG_SELECT_ON);
	text_font_strings = ds_list_create();
	text_font_values = ds_list_create();

	text_sound_strings = ds_list_create();

	for(var i = 0; i < 11; i++){
		ds_list_add(text_font_strings,"Lv. " + string(i));
		ds_list_add(text_sound_strings,"Lv. " + string(i));
	}

	listsss = ds_list_create()

	ds_list_add(listsss,text_speed_strings,text_color_strings,text_font_strings,text_sound_strings,text_speed_strings)

	//whether or not the value should wrap around when adjusted.
	wrapz[0] = true;
	wrapz[3] = true;
	wrapz[1] = false;
	wrapz[2] = false;
	wrapz[4] = true

	menu_size = menuGetSize();

	animation_start = 1 //animation length
	animation_len = animation_start; //animation counter

	menuParentSetTitle(global.langini[2,2])
	menuParentSetSubtitle(global.langini[3,2])

	menuParentUpdateBoxDimension();


	menuParentSetInfoboxHeight(550);
	menuParentSetInfoboxTitleYOffset(250)
	menuParentSetInfoboxSubtitleYOffset(10)
	menuParentSetInfoboxSubtitleColor(global.textColor);
	menuParentSetSelectWidth(.5)

	menu_left = menuOverworldOptionsGraphicLeft
	menu_right = menuOverworldOptionsGraphicRight

	menu_up = menuOverworldOptionUp
	menu_down = menuOverworldOptionDown

	menu_draw = menuOverworldOptionGraphicDraw;

	curposs[0] = global.disableShader;
	curposs[3] = global.fullscreen;
	curposs[1] = round(global.brightness*100)
	curposs[2] = round(global.contrast*100);
	curposs[4] = global.menuAnimation

	menuControl.transparent = true;

