var chara = argument[0];
var fil = string_lower(argument[1]);

var returner = true;

with(chara){
	if(ds_list_size(status_effects) > BATTLE_MAX_STATUS){
		return false;
	}
}

if(battlerHasAbility(chara,"fleet")){
	//if its any of these filters, cancel.
	if(fil == "stone" ||
		fil == "sleep"){
		returner = false;	
	}
}

if(battlerHasAbility(chara,"blind")){
	//if its any of these filters, cancel.
	if(fil == "blind"){
		returner = false;	
	}
}

//can't have both lock and mute.

if(battlerHasFilter(chara,"lock")){
	if(fil == "mute"){
		returner = false;	
	}
}

if(battlerHasFilter(chara,"mute")){
	if(fil == "lock"){
		returner = false;	
	}
}

if(battlerHasFilterIgnoreNull(chara,"null")){
	returner = false;	
}

return returner;