/// @description Insert description here
// You can write your code in this editor

// gets the count of the items
var filename = working_directory + "resources\\event\\item.ini";

ini_open(filename)

var i = 0;

while(true){
	if(!ini_key_exists("data", "name"+ string(i))){
		break;
	}
	i++
}


item_event_id[0] = "";
item_event_quantity[0] = 0;
item_event_item[0] = 0;
item_event_set[0] = false;
for(var j = 0; i > j; j++){
	item_event_id[j] = ini_read_string_length("data", "name"+ string(j),"",IMPORT_MAX_SIZE_EVENT);
	item_event_quantity[j] = ini_read_real("data", "quantity"+ string(j),1);
	item_event_item[j] = ini_read_real("data", "item_id"+ string(j),1);
	item_event_set[j] = false;
}

ini_close()

item_event_total = array_length_1d(item_event_id);