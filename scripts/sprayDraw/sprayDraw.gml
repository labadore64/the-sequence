if(DEBUG_SHOW_SPRAY ){
if(active){
		var _w = 400;
	var _h = 300;

	var getValx = 1;
	var getValy = 1;

	var player_x = 0;
	var player_y = 0;

	with(Player){
		getValx = zoom_x;
		getValy = zoom_y;
		player_x = x;
		player_y = y;
	}
		
	var diffx = getValx
	var diffy = getValy
    

	
	var curviewx = getValx*800//camera_get_view_x(curview);
	var curviewy = getValy*600//camera_get_view_y(curview);

	//represents x and y position of the view

	var viewx = curviewx*.5 -player_x
	var viewy = curviewy*.5 - player_y

	var temp_xx = x
	var temp_yy = y
            
	x = ((viewx+temp_xx)/diffx);
	y = ((viewy+temp_yy)/diffy);
	
	image_xscale = draw_scalex/diffx
	image_yscale = draw_scaley/diffy
	
	
	gpu_set_blendmode(bm_add);
	draw_self();
	gpu_set_blendmode(bm_normal)
	
	x = temp_xx;
	y = temp_yy;
}
}