// returns the index in the bank of the new emitter
// argument0 = type of particle

for(var i = 0; i < BATTLER_PARTICLE_MAX_EMITTER; i++){
	if(particle_bank[i] == -1){
		particle_bank[i] = part_emitter_create(main_part_system)
		particle_type[i] = argument[0];
		particle_time[i] = argument[1];
		return i;
	}
}