var ori = 0;

with(BattleHandler){
	ori = orientation;	
}

var display_orientation = (start_orientation + ori+360) mod 360

if(display_orientation < 180){
	if(sprite_state == BATTLER_SPRITE_IDLE){
		sprite_index = sprite_idle_back;	
	} else if (sprite_state == BATTLER_SPRITE_HIT){
		sprite_index = sprite_hit_back;	
	} else if (sprite_state == BATTLER_SPRITE_JUMP0){
		sprite_index = sprite_jump0_back;	
	} else if (sprite_state == BATTLER_SPRITE_JUMP1){
		sprite_index = sprite_jump1_back;	
	} else if (sprite_state == BATTLER_SPRITE_CAST0){
		sprite_index = sprite_cast0_back;	
	} else if (sprite_state == BATTLER_SPRITE_CAST1){
		sprite_index = sprite_cast1_back;	
	}
} else {
	if(sprite_state == BATTLER_SPRITE_IDLE){
		sprite_index = sprite_idle_front;	
	} else if (sprite_state == BATTLER_SPRITE_HIT){
		sprite_index = sprite_hit_front;	
	} else if (sprite_state == BATTLER_SPRITE_JUMP0){
		sprite_index = sprite_jump0_front;	
	} else if (sprite_state == BATTLER_SPRITE_JUMP1){
		sprite_index = sprite_jump1_front;	
	} else if (sprite_state == BATTLER_SPRITE_CAST0){
		sprite_index = sprite_cast0_front;	
	} else if (sprite_state == BATTLER_SPRITE_CAST1){
		sprite_index = sprite_cast1_front;	
	}
}

if(display_orientation < 90 ||
	display_orientation > 270){
	flip = true	
} else {
	flip = false;	
}