//collision should update distance for each side

var up_min = 10000000;
var down_min = 10000000;
var left_min = 10000000;
var right_min = 10000000;

var sight = 1000000;

with(Collision){
	if(collInFrame){
		collisionCalculateDistances();	
		
		if(player_coll[AX_COLLISION_DIRECTION_UP] != 100000000){
			
			up_min = min(up_min,player_coll[AX_COLLISION_DIRECTION_UP])
			
		}
		if(player_coll[AX_COLLISION_DIRECTION_DOWN] != 100000000){
			down_min = min(down_min,player_coll[AX_COLLISION_DIRECTION_DOWN])
		}
		if(player_coll[AX_COLLISION_DIRECTION_LEFT] != 100000000){
			left_min = min(left_min,player_coll[AX_COLLISION_DIRECTION_LEFT])
		}
		if(player_coll[AX_COLLISION_DIRECTION_RIGHT] != 100000000){
			right_min = min(right_min,player_coll[AX_COLLISION_DIRECTION_RIGHT])
		}
	}
}
var sitedis = 100000000;
var sight_obj = noone;
if(global.AXVisionEnabled){
	with(AXOverworldLabel){
		sitedis = collisionTestDistance(Player.sight_detector.orientation,global.AXSightDistance);

		if(sight > sitedis){
			sight = sitedis;
			sight_obj = id;
		}
	}
}

var xxx = 0;
var yyy = 0;

with(Player){
	
	var counter = 0;
	var obj = noone;
	var poss = 0;
	
	if(global.AXCollisionEnabled){
		xxx = x;
		yyy = y + collision_y_adjust
	
		collision_detect_distance[AX_COLLISION_DIRECTION_UP] = up_min;
		collision_detect_distance[AX_COLLISION_DIRECTION_DOWN] = down_min;
		collision_detect_distance[AX_COLLISION_DIRECTION_LEFT] = left_min;
		collision_detect_distance[AX_COLLISION_DIRECTION_RIGHT] = right_min;
		sight_detector.current_dist = sight;
	
		for(var i = 0; i < 4; i++){
			collision[i].x = xxx + collision_detect_distance[i]*cos(degtorad(collision[i].orientation))
			collision[i].y = yyy + collision_detect_distance[i]*sin(degtorad(collision[i].orientation))
			collision[i].current_dist = collision_detect_distance[i];
			if(collisionDetectorCornerDetect(collision[i])){
				counter++;
				obj = collision[i];
				poss = i;
			}
		}
		
		if(global.AXCornerSound){
			if(corner_counter == -1){
				if(counter == 1){
					var obja = instance_create(collision[poss].xprevious,collision[poss].yprevious,AudioEmitter)
			
					obja.falloff_min = 16 * (global.AXCollisionDistance/AX_COLLISION_DISTANCE);
					obja.falloff_max = global.AXCollisionDistance*1.25
					obja.falloff_factor = 2
					obja.loop = false;
					obja.audio_sound = obj.corner_detect_sound;
			
					corner_counter = 10;
				}
			}
		}
	} else {
		playerUpdateSoundNoBlind()	
	}
	
	if(global.AXVisionEnabled){
		sight_detector.x = 	xxx
		sight_detector.y = 	yyy

		if(sight_detector.looking_obj != sight_obj &&
			sight_obj != noone){
			//soundfxPlay(sound_seeSomething)		
		} else if (sight_detector.looking_obj != sight_obj &&
			sight_obj == noone){
			//soundfxPlay(sound_stopSeeing)		
		}

		sight_detector.looking_obj = sight_obj;
	}

}