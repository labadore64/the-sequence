if(!destroy_executed){
	destroy_executed = true;

	if(destroy_script != -1){
		script_execute(destroy_script);
	} else {
		instance_destroy();	
	}
	complete_animation = 0;
	
	// current score
	current_score = 0;
	display_score = "0";
	current_success = 0;
	// results

	result_value = 0;

	result_start_counter = 0;

	result_fade_counter = 1;
	
	result_score = 0;
}