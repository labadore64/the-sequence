//note, returns DS map, make sure you kill it!

var character1 = argument[0];
var character2 = argument[1];
var character3 = argument[2];

var sizer = 0;

if(!is_undefined(character1)){
	if(instance_exists(character1)){
		sizer++;	
	}
}
if(!is_undefined(character2)){
	if(instance_exists(character2)){
		sizer++;	
	}
}
if(!is_undefined(character3)){
	if(instance_exists(character3)){
		sizer++;	
	}
}

var returner = ds_map_create();


var colorz = 0;

	
	if(sizer == 3){
		var ob1 = character1
		var ob2 = character2
		var ob3 = character3
		
		colorz = make_color_rgb(color_get_red(ob1.color),color_get_green(ob2.color),color_get_blue(ob3.color));
	} else {
		colorz = $7F7F7F;	
	}

	ds_map_add(returner,"aura",colorz);
	
	//calculate stat boosts
	
	ds_map_add(returner,"phy_boost",0);
	ds_map_add(returner,"mag_boost",0);
	ds_map_add(returner,"spe_boost",0);

	ds_map_add(returner,"aura_ability",-1);


return returner;