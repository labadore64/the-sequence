//fill later with character names
gml_pragma("forceinline");
menuOverworldSetStringsAsParty(menuOverworldItemGiveScript);

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

menuParentSetTitle(global.langini[4,2]);
menuParentSetSubtitle("");

menuParentSetGradientHeight(0)
menuParentSetOptionXOffset(150);
menuParentSetOptionYOffset(20);
menuParentSetOptionSpacing(200)
menuParentSetOptionSize(3)
draw_height = 300
draw_width = 790

x = 400
y = 300

list_of_names = ds_list_create();


menu_size = menuGetSize();

var mylist = 0;

for(var i = 0; i < menu_size; i++){
	mylist = ds_list_create()
	ds_list_add(list_of_names,mylist);
}

/*
for(var i = 0; i < menu_size; i++){
	mylist = list_of_names[|i];
	chara = obj_data.party[|i];
	for(var j = 0; j < 5; j++){
		iteme = chara.item[i];
		itemData(iteme);
		ds_list_add(mylist,name);
	}
}
*/

menuOverworldItemGiveSetItems();


menu_draw = menuOverworldItemGiveDraw;

menu_up = menuOverworldItemGiveUp
menu_down = menuOverworldItemGiveDown

menu_left = menu_up
menu_right = menu_down

menuParentTTSTitleRead();

//menuParentTTSLabelRead();

menuParentUpdateBoxDimension();

if(menu_size == 0){
	instance_destroy();	
}
