
	updateBattleMainHUD();
	hudPopulateTablet("tablet",character.tablet[menupos])
	hudPopulateMoveBattle("move",moves[|menupos],0);

	var fixstring = "";
	var len = string_length(selected_string);
	for(var i = 0; i < len; i++){
		fixstring += "\"" + string_char_at(selected_string,i+1) + "\" ";	
	}

	hudControllerAddData("tablet." + "input",fixstring);

	var orderstring = "not selected"

	if(numbering[menupos] > -1){
		if(numbering[menupos] == 0){
			orderstring = "first"	
		} else if(numbering[menupos] == 1){
			orderstring = "second"	
		} else if(numbering[menupos] == 2){
			orderstring = "third"	
		}
	}

	hudControllerAddData("tablet." + "order",orderstring);
	hudControllerAddData("tablet." + "mp_cost",string(floor(total_mpcost*100)));
