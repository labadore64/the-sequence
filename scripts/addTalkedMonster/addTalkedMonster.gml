with(obj_data){
		if(!hasTalkedMonster(argument[0])){
			ds_map_add(monster_talk_list,argument[0],1);	
		} else {
			var obj = ds_map_find_value(monster_talk_list,argument[0])
			ds_map_replace(monster_talk_list,argument[0],clamp(real(obj)+1,0,4));	
		}
}