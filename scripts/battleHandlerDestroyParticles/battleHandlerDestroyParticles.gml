part_type_destroy(damage_particle)

var count = array_length_1d(part_type);

for(var i = 0; i < count; i++){
	if(part_type_exists(part_type[i])){
		part_type_destroy(part_type[i]);	
	}
}