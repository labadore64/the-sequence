
	    px = argument[0];
		py = argument[1];
		qx = argument[2];
		qy = argument[3];
		rx = argument[4];
		ry = argument[5];
	
		if (qx <= max(px, rx) && qx >= min(px, rx) &&
	        qy <= max(py, ry) && qy >= min(py, ry))
	       return true;
 
	    return false;
