if(check_for_collision){
	//if(instance_exists(Player)){
	if(distance_to_object(Player) < falloff_max){
		if(collision_line(x,y,Player.x, Player.y+Player.collision_y_adjust, Collision, false, true) != noone){
			audio_pause_sound(emit_sound)
			stop_playing = true;
		} else if(stop_playing){
			play_this_turn = true;
			stop_playing = false;
			}
		}
	}