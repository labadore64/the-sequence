
// common selection text
#macro LANG_SELECT_ON global.langini[12,0]
#macro LANG_SELECT_OFF global.langini[12,1]
#macro LANG_SELECT_LEFT global.langini[12,2]
#macro LANG_SELECT_RIGHT global.langini[12,3]
#macro LANG_SELECT_YES global.langini[12,4]
#macro LANG_SELECT_NO global.langini[12,5]

#macro LANG_PHONE_DEFAULT_AREA_CODE global.langini[11,0]
#macro LANG_PHONE_STYLE global.langini[11,1]