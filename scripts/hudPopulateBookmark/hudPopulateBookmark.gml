// populates with a data character

var entryname = argument[0] + "."; // entry name of the character for referencing in the string replacements
var obj = argument[1]; //the object to copy from

if(instance_exists(obj)){
	// name and character
	hudControllerAddData(entryname + "name",string(obj.name));
	hudControllerAddData(entryname + "desc",string(obj.desc));
	hudControllerAddData(entryname + "x",string(coordConvert(obj.x)));
	hudControllerAddData(entryname + "y",string(coordConvert(obj.y)));
}