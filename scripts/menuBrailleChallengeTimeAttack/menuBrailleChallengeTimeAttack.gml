var obj = instance_create(0,0,menuBrailleChallengeQuantity);
obj.game_obj = brailleGameTimeAttack; //which object type to use for this game mode
obj.min_value = 30;
obj.max_value = 300;
obj.value_spacer = 30;
obj.braille_obj = brailleDataObj
obj.menu_help = "brailletime"

obj.unit_string = "Seconds"
obj.title = "Time Attack"