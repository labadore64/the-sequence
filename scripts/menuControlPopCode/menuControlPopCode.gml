with(menuControl){
	active_menu = noone;
	while(!instance_exists(active_menu) && ds_stack_size(menu_stack) > 0){
		var getval = ds_stack_pop(menu_stack);	
	
		if(!is_undefined(getval)){
		
			var newactive = ds_stack_top(menu_stack);
			if(!is_undefined(active_menu)){
				active_menu = newactive;
			} else {
				active_menu = noone;	
			}
		}
		
		if(is_undefined(active_menu)){
			active_menu = noone	
		}
	}
	
	if(ds_stack_size(menu_stack)== 0){
		bookmarkStart();
	}
	
	while(!ds_list_empty(push_stage_list)){
		active_menu =push_stage_list[|0];
		ds_stack_push(menu_stack,active_menu);
		ds_list_delete(push_stage_list,0);
	}
	
	please_draw_this_frame = false;

	please_draw_all_visible = true
}
menuControlUpdate();
