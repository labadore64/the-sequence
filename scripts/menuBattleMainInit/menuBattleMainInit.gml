if(ds_exists(BattleHandler.party_list,ds_type_list)){
	menuAddOption("Fight","Select moves to fight.",menuBattleFightScript)
	menuAddOption("Auto","Automatically starts fighting.",menuBattleAutoScript)
	menuAddOption("Stats","View character stats.",menuBattleStatsScript)
	menuAddOption("Flee","Flee from battle.",menuBattleFleeScript)

	menu_size = menuGetSize();

	menuParentSetTitle("Main Battle Menu")
	menuParentSetSubtitle("");

	menu_draw = menuBattleMainDraw;

	draw_bg = false;

	menu_cancel = -1;

	menu_left = menu_up;
	menu_right = menu_down;
	battleHandlerAddHistory(" ")
	battleHandlerAddHistory("---------------------------------------")
	battleHandlerAddHistory(" ");


	if(obj_data.battle_exists){
		if(battlerCountFilter(BattleHandler.party_list[|0],"listen") > 0){
			menupos = 1;	
		}
	}
}