
if(!surface_exists(surface)){
	surface = surface_create_access(512*global.scale_factor,512*global.scale_factor);	
}
surface_set_target(surface)

draw_clear_alpha(c_black,0);
gpu_set_colorwriteenable(true,true,true,true)

draw_set_color(c_black)
draw_set_alpha(.7)
draw_ellipse(draw_shadx1,draw_shady1,draw_shadx2,draw_shady2,false)
draw_set_alpha(1)
draw_sprite_ext(sprite_index,spr_counter,draw_the_xx,draw_the_yy,draw_the_x_holder,draw_the_y_holder,0,c_white,1);

surface_reset_target()
