var ori = 0;

with(BattleHandler){
	ori = orientation;	
}

orientation = base_orientation + ori

battleHandlerFixOrientation();

var chara_ori = degtorad(orientation);
	
if(lunge_target != noone){
	var targe_ori = degtorad(lunge_target.orientation);
	/* orientation of the target on the battle circle (you
		* do not ever have to change this with this model)
		* calculates the normalized position where he stands */
	norm_target_x = cos(targe_ori);
	norm_target_y = sin(targe_ori);
	
}

/* orientation of the current guy being controlled on
	* the battle circle (you do not ever have to change this
	* with this model) calculates the normalized position
	* where he stands */
norm_original_x = cos(chara_ori);
norm_original_y = sin(chara_ori);

/* linear interpolation factor, equal to 1 when in the
	* original position and 0 when at the target position,
	* and it's inverse. */
lerp_factor = (1.0 + distance) * 0.5;
inv_lerp_factor = 1.0 - lerp_factor;

/* interpolated normalized position */
norm_x = lerp_factor*norm_original_x + inv_lerp_factor*norm_target_x;
norm_y = lerp_factor*norm_original_y + inv_lerp_factor*norm_target_y;

/* scale to the battle circle's screen space coordinates */
x = norm_x*BattleHandler.h_size + BattleHandler.origin_x;
y = norm_y*BattleHandler.v_size + BattleHandler.origin_y;	

base_x = x;
base_y = y;

