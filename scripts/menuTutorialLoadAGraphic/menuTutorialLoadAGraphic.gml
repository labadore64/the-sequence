var map = argument[0];

if(ds_exists(map,ds_type_map)){
	var xx = 0;
	var yy = 0;
	var xscale = 0;
	var yscale = 0;
	var rotation = 0;
	var sprite = -1;
	var blend = c_white;
	var alpha = 1;
	
	var test = map[? "x"];
	if(!is_undefined(test)){
		xx = test;	
	}
	
	test = map[? "y"];
	if(!is_undefined(test)){
		yy = test;	
	}
	
	test = map[? "xscale"];
	if(!is_undefined(test)){
		xscale = test;	
	}
	
	test = map[? "yscale"];
	if(!is_undefined(test)){
		yscale = test;	
	}
	
	test = map[? "angle"];
	if(!is_undefined(test)){
		rotation = test;	
	}
	
	test = map[? "sprite"];
	if(!is_undefined(test)){
		sprite = asset_get_index(test);	
	}
	
	test = map[? "r"];
	if(!is_undefined(test)){
		var g = map[? "g"];
		var b = map[? "b"];
		
		if(!is_undefined(g) && !is_undefined(b)){
			blend = make_color_rgb(test,g,b);
		}
	}
	
	test = map[? "alpha"];
	if(!is_undefined(test)){
		alpha = test;	
	}
	
	// now that the object vars are defined create the object
	draw_array[draw_count] = instance_create(xx,yy,tutorialGraphic);
	
	draw_array[draw_count].image_xscale = xscale;
	draw_array[draw_count].image_yscale = yscale;
	draw_array[draw_count].image_angle = rotation;
	draw_array[draw_count].image_alpha = alpha;
	draw_array[draw_count].image_blend = blend;
	draw_array[draw_count].sprite_index = sprite;
	
	draw_count++;
}