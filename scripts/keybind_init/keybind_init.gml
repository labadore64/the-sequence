/// @description - initialize all keybinding


//the following adds keybind entries to the map

//general keybinds

keybind_set("left",vk_left);
keybind_set("right",vk_right);
keybind_set("up",vk_up);
keybind_set("down",vk_down);

keybind_set("control",vk_backspace);

keybind_set("cancel",ord("X"));
keybind_set("select",ord("Z"));
keybind_set("select2",vk_enter);

//overworld keybinds

keybind_set("open_menu",vk_enter);
keybind_set("run",ord("X"));

keybind_set("zoomIn",ord("N"));
keybind_set("zoomOut",ord("M"));

keybind_set("accessMenu", ord("Q"));

keybind_set("bookmark", vk_control);
keybind_set("exit", vk_escape);

// help

keybind_set("help", vk_f1);
keybind_set("hud", vk_f2);

// additional keys for accessibility
keybind_set("access1",ord("1"));
keybind_set("access2",ord("2"));
keybind_set("access3",ord("3"));
keybind_set("access4",ord("4"));
keybind_set("access5",ord("5"));
keybind_set("access6",ord("6"));
keybind_set("access7",ord("7"));
keybind_set("access8",ord("8"));
keybind_set("access9",ord("9"));
keybind_set("access0",ord("0"));