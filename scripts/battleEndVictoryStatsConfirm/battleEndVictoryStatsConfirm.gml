
if(!exp_count_complete){
	exp_count_complete = true;	
	
	current_exp = exp_end;
	current_exp = clamp(current_exp,0,BATTLE_STAT_EXP_TOTAL);
	character.experience = current_exp;
	with(character){
		statLoadMain(id)
	}
} else {
	//end
	var endie = false;
	with(battleEndVictory){
		if(active){
			do_windows = true;
			newalarm[4] = 2;
			newalarm[5] = 1;
			endie = true;
		}
	}
	
	if(endie){
		lock = true;
	}
}