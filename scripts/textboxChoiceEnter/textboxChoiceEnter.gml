if(!wait){
	if(ds_exists(text,ds_type_list)){
		if(display_object != -1){
			var size = ds_list_size(text);
			if(line < size){
				var cando = false;
	
				var lockdown = false;
	
				with(display_object){
					if(cutoff != string_lenger){
						cutoff = string_lenger;
						force_draw = true;
						audio_stop_sound(text_sound1);
						audio_stop_sound(text_sound2);
						audio_stop_sound(text_sound3);
						audio_stop_sound(text_sound4);

					} else {
						if(array_length_1d(message)-1 > message_current){
							message_current++;
							cutoff = 0;
							cando = true;
						} else {
							lockdown = true;
						}
					}
				}
				
				if(lockdown){
					lock = true;	
				}
	
				if(cando){
					line++;
					if(line < size){
						textboxExecuteScript();
						textboxSetCurrentText();
						if(tts_speak){
							tts_say(current_text);
						}	
					} else {
						with(display_object){
							cutoff = string_lenger;
							force_draw = true;
							audio_stop_sound(text_sound1);
							audio_stop_sound(text_sound2);
							audio_stop_sound(text_sound3);
							audio_stop_sound(text_sound4);
						}
						if(complete){
							instance_destroy();	
						}
					}
				}
	
			} else {
				with(display_object){
					cutoff = string_lenger;
					force_draw = true;
					audio_stop_sound(text_sound1);
					audio_stop_sound(text_sound2);
					audio_stop_sound(text_sound3);
					audio_stop_sound(text_sound4);
				}
				
				if(complete){
					instance_destroy();	
				}
			}
		}
	}
} else {
	wait = false;	
}