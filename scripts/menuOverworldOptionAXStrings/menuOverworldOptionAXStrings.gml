

//menuAddOption("Vignette","Enable the Vignette on the Field.",-1)
/*
global.sfxVolume = 1; //volume for sfx
global.musicVolume = 1; //volume for music
global.AXVolume = 1; //volume for AX utils
*/

//AX

menuAddOption(global.langini[0,43],global.langini[1,43],menuOverworldOptionAXLeft)
menuAddOption(global.langini[0,44],global.langini[1,44],menuOverworldOptionAXLeft)
menuAddOption(global.langini[0,45],global.langini[1,45],menuOverworldOptionAXLeft)
menuAddOption("Notify Sound","Play sound near important objects.",menuOverworldOptionAXLeft)

text_speed_strings = ds_list_create();

ds_list_add(text_speed_strings,LANG_SELECT_ON,LANG_SELECT_OFF);

text_color_strings = ds_list_create();
text_color_values = ds_list_create()

for(var i =0; i < 11; i++){
	ds_list_add(text_color_strings,"Lv. " + string(i));
}

text_font_strings = ds_list_create();
text_font_values = ds_list_create();

ds_list_add(text_font_strings,global.langini[12,18],global.langini[12,18]);

text_sound_strings = ds_list_create();

listsss = ds_list_create()

ds_list_add(listsss,text_color_strings,text_speed_strings,text_speed_strings,text_speed_strings)

//whether or not the value should wrap around when adjusted.
wrapz[0] = true;
wrapz[1] = true;
wrapz[2] = true;
wrapz[3] = true;

menu_size = 4;

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);
menuParentSetSelectWidth(.5)

menu_left = menuOverworldOptionAXLeft
menu_right = menuOverworldOptionAXRight

menu_up = menuOverworldOptionUp
menu_down = menuOverworldOptionDown

menu_draw = menuOverworldOptionAXDraw;

curposs[2] = global.AXCornerSound;
curposs[0] = round(global.AXVolume*10);
curposs[1] = global.AXVisionEnabled
curposs[3] = !global.AXNotify;

with(menuControl){
	transparent = true;
}
