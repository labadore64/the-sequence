var chara = argument[0];
var filtername = "null";

var returner = false;

with(chara){
	if(ds_exists(status_effects,ds_type_list)){
		var sizer = ds_list_size(status_effects);

		var fill = -1;

		for(var i = 0; i < sizer; i++){
			fill = status_effects[|i];
			if(!is_undefined(fill)){
				if(instance_exists(fill)){
					if(string_lower(fill.name) == filtername){
						returner = true;	
					}
				}
			}
		}
	}

}

return returner;