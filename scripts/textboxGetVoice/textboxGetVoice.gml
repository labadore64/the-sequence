if(is_monster){

	var soun1 = asset_get_index("speech_" + string_lower(argument[0].name) + "1");
	var soun2 = asset_get_index("speech_" + string_lower(argument[0].name)+ "2")
	var soun3 = asset_get_index("speech_" + string_lower(argument[0].name)+ "3") 
	var soun4 = asset_get_index("speech_" + string_lower(argument[0].name) + "4")

	if(soun1 != -1){
		text_sound1 = soun1
	}
	if(soun2 != -1){
		text_sound2 = soun2
	}
	if(soun3 != -1){
		text_sound3 = soun3
	}
	if(soun4 != -1){
		text_sound4 = soun4
	}
} else {
	switch(argument[0]){

		default:
		text_sound1 = textbox_sound_default
		text_sound2 = textbox_sound_default
		text_sound3 = textbox_sound_default
		text_sound4 = textbox_sound_default
		break;
		
		case 0:
		text_sound1 = speech_leon01
		text_sound2 = speech_leon02
		text_sound3 = speech_leon03
		text_sound4 = speech_leon04
		pitch_base=1.25
		break;
		
		case 1:
		text_sound1 = speech_pierre01
		text_sound2 = speech_pierre02
		text_sound3 = speech_pierre03
		text_sound4 = speech_pierre04
		pitch_base=1.25
		break;
	}
}