
//blind mode update
//updates TTS reader and the sound effect for position

if(access_mode() || global.tts){
	menuParentTTSLabelRead();
}

if(menu_use_generic){
	mouseHandler_center_cursor(menupos)
} else {
	mouseHandler_center_menu_cursor(menupos)
}

with(MouseHandler){
	ignore_menu = false	
}

soundfxPlay(sound_move)

MouseHandler.active = true