var obj_id = argument[0];
var item_id = argument[1];

with(Player){
	prop_place_id = obj_id;
	prop_placing = true;
	prop_place_item = item_id;
	overworld_item_active = true;
}

with(CameraOverworld){
	prop_place = true;
	prop_place_obj = obj_id;
}

return true