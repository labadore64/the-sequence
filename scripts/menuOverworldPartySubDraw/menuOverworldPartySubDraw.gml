
with(menuOverworldParty.portrait){
	if(!surface_exists(surface)){
		script_execute(draw_script)
	}
	
	draw_surface(surface,global.display_x,global.display_y+130*global.scale_factor)
}

draw_set_color(c_black)
draw_rectangle_ratio(0,550,800,600,false)

draw_set_color(global.textColor)

draw_set_halign(fa_left)

if(animation_len == 0){
	menuParentDrawBox();
	
	//menuParentDrawText();
	
	
	menuParentDrawOptionsNoGradient();
	
}

gpu_set_colorwriteenable(true,true,true,true)

menuParentDrawInfo();

draw_letterbox()