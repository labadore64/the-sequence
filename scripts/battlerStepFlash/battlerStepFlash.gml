//is sprite flashing
if(flash){
	flash_counter+=menuControl.timer_diff;
	flash_intensity = .5+cos((flash_counter*pi/flash_time))*.5;
}

if(flash_once){
	flash_once_counter++
	flash_intensity = .5+cos((flash_once_counter*pi/flash_once_time))*.5;
	if(flash_once_counter == flash_once_time){
		flash_once = false;
		flash_once_counter = 0;
	}
}