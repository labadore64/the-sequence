//returns a string that contains the user data

var chara = argument[0]
var filename = argument[1]

/// @description Insert description here
// You can write your code in this editor
//represents the character stats.

var map = ds_map_create();
var lister = ds_list_create();

var braille_list = ds_list_create();

//var newlist = ds_list_create();

with(chara){
	//displayed/used stats
	ds_map_add(map,"experience",experience);
	//ds_map_add(map,"level",level);
	ds_map_add(map,"recruited",recruited);

	//inventory
	for(var i = 0; i < 5; i++){
		ds_list_add(lister,item[i]);
	}
	
	ds_map_add_list(map,"items",lister);
	
	//braille
	for(var i = 0; i < 3; i++){
		ds_list_add(braille_list,tablet[i]);
	}
	
	ds_map_add_list(map,"tablets",braille_list);

	//moves
	ds_map_secure_save(map,filename+"character"+string(character)+".sav")
}

//var returner = json_encode(map)

ds_map_destroy(map);
//ds_list_destroy(newlist)
ds_list_destroy(lister)
ds_list_destroy(braille_list);

//return returner;