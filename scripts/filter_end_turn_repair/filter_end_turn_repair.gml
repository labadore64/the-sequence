var val = argument[0];

var texts = filterGetText("repair")

with(val.target){
	var damg = floor(hp.base * .1)
	
	var changed = hp.current + damg;
		
	battlerHPChange(id,changed)
		
	var stringer = texts[|0];
	
	stringer = string_replace_all(stringer,"@0",name);
	stringer = string_replace_all(stringer,"@1",string(10));
	battleTextDisplayAddText(stringer);
	
	var stringer = texts[|1];
		
	stringer = string_replace_all(stringer,"@0",name);
	stringer = string_replace_all(stringer,"@1",string(10));
	battleTextDisplayAddText(stringer);
		
}

ds_list_destroy(texts);