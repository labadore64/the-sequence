var checkbytes;

checkbytes[0] = C_END_COMMAND1;
checkbytes[1] = C_END_COMMAND2;
checkbytes[2] = C_END_COMMAND3;
checkbytes[3] = C_END_COMMAND4;

var arrayOfStuff = c_readuntil(checkbytes);
var inst = instance_create(0,0,CutsceneBlock);
inst.commands = arrayOfStuff;
if(array_length_1d(arrayOfStuff) > 1){
	with(inst){
		c_blockInitialize();
	}
}

return inst;