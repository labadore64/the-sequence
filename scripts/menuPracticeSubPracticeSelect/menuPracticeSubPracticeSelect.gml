var obj = instance_create(0,0,brailleCountDown)
obj.obj_to_create = brailleGamePracticeMode
obj.braille_char = braille_char
obj.braille_obj = braille_obj
obj.braille_data = braillePracticeList.braille_data.braille_map
with(menuPartyTablet){
	visible = false;	
}
with(menuPartyTabletSub){
	visible = false;	
}