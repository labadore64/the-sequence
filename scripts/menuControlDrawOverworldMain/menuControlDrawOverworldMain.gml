	var test = false;
	
	//gpu_set_colorwriteenable(true,true,true,false)
	with(menuParentFullBG){
		if(animation_len == 0 && !force_draw_bg && visible){
			test = true;	
		}
	}
	
	with(CameraOverworld){
		if(!test){
			if(draw_script != -1){
				script_execute(draw_script);
			}
		} else {
			draw_clear(c_black)	
		}
	}
	
	menuControlDrawDialog();
	
	if(wait == 0){
		menuControlDrawMenus();
	}