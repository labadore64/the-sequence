ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items

var list_to_use = physical_moves;

if(menu_type == MOVE_TYPE_MAGIC){
	list_to_use = magic_moves;
}

var sizer = ds_list_size(list_to_use);
var movez = -1;

for(var i = 0; i < sizer; i++){
	movez = list_to_use[|i];
	if(!is_undefined(movez)){
		moveData(movez);
		menuAddOption(name,flavor,-1)
	}
}

menu_size = menuGetSize();