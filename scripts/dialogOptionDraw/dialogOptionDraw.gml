var xx = 400;
var yy = 487;
var yspacer = 28;
var select_adjust = -4;

var scale = 2;

draw_set_halign(fa_center);
draw_set_colour(global.textColor);
draw_set_font(global.textFont)
for(var i =0; i < stringsize; i++){
	draw_text_transformed_ratio(xx,yy+yspacer*i,option_strings[|i],scale,scale,0);
}

draw_set_halign(fa_left);

//draw select
draw_set_alpha(.45)
draw_rectangle_ratio(0,yy+yspacer*menupos+select_adjust,800,yy+(yspacer*(menupos+1))+select_adjust,false)
draw_set_alpha(1)

//draw intro animation
if(!active){
	draw_set_color(c_black)
	//spacer
	draw_rectangle_ratio(0,480,800,540-spacer*animation_counter,false)
	draw_rectangle_ratio(0,540+spacer*(animation_counter),800,600,false)
}