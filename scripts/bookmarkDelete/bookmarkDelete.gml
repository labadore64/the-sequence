//var checkDist = 100000000;
if(instance_exists(AXBookmarkHandler)){
	var mydist = 0

	with(AXBookmark){
		if(object_index == AXBookmark){
			mydist = distance_to_object(Player)
	
			if(mydist < AX_BOOKMARK_DELETE_DIST){
				instance_destroy();	
			}
		}
	}
}