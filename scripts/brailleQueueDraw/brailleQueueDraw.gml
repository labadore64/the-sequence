if(display_ready_message){
	gpu_set_colorwriteenable(true,true,true,false)
	draw_set_alpha(.45);
	draw_set_color(c_black);
	draw_rectangle_ratio(0,0,800,600,false);
	
	draw_set_alpha(1);
	draw_set_color(global.textColor);
	
	draw_set_halign(fa_center)
	draw_set_valign(fa_middle);
	draw_text_transformed_ratio(400,300,"Go,\n\nBraille!",3,3,0);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
}