//arg0 = character
if(instance_exists(objdata_character)){
	if(argument[0].overworld_character != -1){
		var multiplier_phys_power = getMultiplierFromLevel(objdata_character.phy_power_grow[argument[0].overworld_character.character],argument[0].level);
		var multiplier_phys_guard = getMultiplierFromLevel(objdata_character.phy_guard_grow[argument[0].overworld_character.character],argument[0].level);
		var multiplier_mag_power = getMultiplierFromLevel(objdata_character.mag_power_grow[argument[0].overworld_character.character],argument[0].level);
		var multiplier_mag_guard = getMultiplierFromLevel(objdata_character.mag_guard_grow[argument[0].overworld_character.character],argument[0].level);
		var multiplier_spe_power = getMultiplierFromLevel(objdata_character.spe_power_grow[argument[0].overworld_character.character],argument[0].level);
		var multiplier_spe_guard = getMultiplierFromLevel(objdata_character.spe_guard_grow[argument[0].overworld_character.character],argument[0].level);
	} else {
		var levelvalue = level/100;
		var multiplier_phys_power = levelvalue
		var multiplier_phys_guard = levelvalue
		var multiplier_mag_power = levelvalue
		var multiplier_mag_guard = levelvalue
		var multiplier_spe_power = levelvalue
		var multiplier_spe_guard = levelvalue
	}

	argument[0].phy_power.braille_grow = 0
	argument[0].phy_guard.braille_grow = 0
	argument[0].mag_power.braille_grow = 0
	argument[0].mag_guard.braille_grow = 0
	argument[0].spe_power.braille_grow = 0
	argument[0].spe_guard.braille_grow = 0

	for(var i = 0; i < 3; i++){
		if(argument[0].tablet[i] > -1){
			if(tablet_recover[i] == -1){
				argument[0].phy_power.braille_grow += statCalculateBrailleBoost(multiplier_phys_power,objdata_tablet.phy_power[argument[0].tablet[i]]);
				argument[0].phy_guard.braille_grow += statCalculateBrailleBoost(multiplier_phys_guard,objdata_tablet.phy_guard[argument[0].tablet[i]]);
				argument[0].mag_power.braille_grow += statCalculateBrailleBoost(multiplier_mag_power,objdata_tablet.mag_power[argument[0].tablet[i]]);
				argument[0].mag_guard.braille_grow += statCalculateBrailleBoost(multiplier_mag_guard,objdata_tablet.mag_guard[argument[0].tablet[i]]);
				argument[0].spe_power.braille_grow += statCalculateBrailleBoost(multiplier_spe_power,objdata_tablet.spe_power[argument[0].tablet[i]]);
				argument[0].spe_guard.braille_grow += statCalculateBrailleMPBoost(multiplier_spe_guard,objdata_tablet.spe_guard[argument[0].tablet[i]]);
			}
		}
	}

	argument[0].phy_power.braille_stat = argument[0].phy_power.braille_grow + argument[0].phy_power.current;
	argument[0].phy_guard.braille_stat = argument[0].phy_guard.braille_grow + argument[0].phy_guard.current;
	argument[0].mag_power.braille_stat = argument[0].mag_power.braille_grow + argument[0].mag_power.current;
	argument[0].mag_guard.braille_stat = argument[0].mag_guard.braille_grow + argument[0].mag_guard.current;
	argument[0].spe_power.braille_stat = argument[0].spe_power.braille_grow + argument[0].spe_power.current;
	argument[0].spe_guard.braille_stat = argument[0].spe_guard.braille_grow + argument[0].spe_guard.current;
	
	argument[0].phy_power.current = argument[0].phy_power.braille_stat;
	argument[0].phy_guard.current = argument[0].phy_guard.braille_stat;
	argument[0].mag_power.current = argument[0].mag_power.braille_stat;
	argument[0].mag_guard.current = argument[0].mag_guard.braille_stat;
	argument[0].spe_power.current = argument[0].spe_power.braille_stat;
	argument[0].spe_guard.current = argument[0].spe_guard.braille_stat;
}