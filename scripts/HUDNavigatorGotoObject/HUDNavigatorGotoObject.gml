var arg = argument[0];

// contains remap list
var remapstring = "";
if(argument_count > 1){
	var remapstring = argument[1];
}

// convert arg to obj

with(HUDController){
	if(is_string(arg)){
		if(arg == "parent"){
			arg = active_object.parent;
		} else if (string_pos("parent.",arg) > 0){
			arg = string_replace(arg,"parent.","");
			var len = array_length_1d(active_object.parent.children);
			
			for(var i = 0; i < len; i++){
				if(instance_exists(active_object.parent.children[i])){
					if(active_object.parent.children[i].name == arg){
						arg = active_object.parent.children[i];
						break;
					}
				}
			}
			
		}else {
			arg = HUDObjects[?arg];
	
			if(is_undefined(arg)){
				arg = noone;	
			}
		}
	} else {
		if(!instance_exists(arg)){
			arg = noone;	
		}
	}
	
	if(is_string(arg)){
		arg = noone;	
	}
	
	if(arg != noone){
		active_object = arg	
	}
}

if(arg != noone){
	current_object = arg
}

if(instance_exists(current_object)){
	if(remapstring != ""){
		// do object remap	
		HUDObjectRemap(current_object,remapstring);
	}
	tts_say(hudControllerReplaceData(current_object.read_text,current_object));
} else {
	show_debug_message("there appears to be a problem");	
}