menuParentCreate();
animation_start = 1 //animation length
animation_len = animation_start; //animation counter


menu_draw = menuParentNoBGDraw;
x = 400
y = 265

draw_width = 800; //width of box in pixels
draw_height = 500//400; //height of box in pixels

menuParentSetGradientHeight(105)
menuParentSetOptionYOffset(120)

menuParentSetGradient(c_black,$7F7F7F)
draw_bg = false; //whether to draw transparent bg

menuParentUpdateBoxDimension();