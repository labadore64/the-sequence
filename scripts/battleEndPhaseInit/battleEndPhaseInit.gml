var me = id;
if(instance_exists(objdata_endphase)){
	//aura red
	if(obj_data.battle_exists){
	
		var count = objdata_endphase.data_size;
		var cando = false;
		var condition_script_has = false;
		var charaid;
	
		// cycle through the end phase effects that target a user.
		for(var i = 0; i < count; i ++){
			if(objdata_endphase.script[i] != -1 && objdata_endphase.target[i]){
				condition_script_has = objdata_endphase.condition_script[i] == -1;
		
				with(Battler){
					charaid = id;
					if(battlerIsAlive(charaid)){
						cando = false;
		
						if(condition_script_has){
							cando = true;	
						} else {
							if(script_execute(objdata_endphase.condition_script[i],charaid)){
								cando = true;	
							}
						}
		
						// add the script to the list
						if(cando){
							with(me){
								battleEndPhaseAdd(objdata_endphase.script[i],charaid,i);
							}
						}
					}
				}
			}
		}	
		
		// entire party effects
		for(var i = 0; i < count; i ++){
			if(objdata_endphase.script[i] != -1 && !objdata_endphase.target[i]){
				condition_script_has = objdata_endphase.condition_script[i] == -1;
		

				cando = false;
		
				if(condition_script_has){
					cando = true;	
				} else {
					if(script_execute(objdata_endphase.condition_script[i],-1)){
						cando = true;	
					}
				}
		
				// add the script to the list
				if(cando){
					battleEndPhaseAdd(objdata_endphase.script[i],noone,i);
				}

			}
		}
	}
}