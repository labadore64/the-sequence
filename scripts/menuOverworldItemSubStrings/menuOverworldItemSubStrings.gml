
	gml_pragma("forceinline");

	draw_height = 200
	draw_width = 200

	x = 600
	y = 300-100

	var item_selected = -1;
	
	with(menuOverworldItems){
		item_selected = item_id [| menupos];	
	}

	menuAddOption(global.langini[0,13],global.langini[1,13],menuOverworldItemSubScriptUse)
	menuAddOption(global.langini[0,14],global.langini[1,14],menuOverworldItemSubScriptGive)
	menuAddOption(global.langini[0,15],global.langini[1,15],menuOverworldItemSubScriptToss)
	menuAddOption(global.langini[0,16],global.langini[1,16],menuOverworldItemSubScriptSet)
	if(item_selected > -1){
		if(objdata_items.help[item_selected] != ""){
			menuAddOption(global.langini[0,55],global.langini[1,55],menuOverworldItemSubScriptHelp)
			draw_height = 240
			y = 300-70
		}
	}
	
	SHORTHAND_CANCEL_OPTION
	var namerr = "";
	with(menuOverworldItems){
		namerr = name;	
	}

	if(namerr != ""){
		menuParentSetTitle(replaceStringWithVars(global.langini[4,0],namerr)); // do what with ?
	} else {
		menuParentSetTitle(global.langini[4,1]); // no item, do what?
	}
	menuParentSetSubtitle("");

	menuParentSetDrawTitle(false);
	menuParentSetDrawSubtitle(false);
	menuParentSetDrawGradient(false);

	menuParentSetGradientHeight(0)
	menuParentSetOptionYOffset(10);
	menuParentSetOptionSpacing(40)

	menu_size = menuGetSize();

	menu_draw = menuOverworldItemSubDraw;

	menu_left = menuOverworldPartySubLeft;
	menu_right = menuOverworldPartySubRight;

	menu_up = menu_left;
	menu_down = menu_right;

	menuParentUpdateBoxDimension();

	tts_say(global.langini[0,1] + " " + menu_name_array[menupos])
