var move_list = argument[0];
var move = argument[1];
var move_level = argument[2];
var move_event = argument[3];
var chara = argument[4]

var level = chara.level;

var move_cando = false;
var event_cando = false;

if(!is_undefined(move)){
	if(!is_undefined(move_level)){
		if(!is_undefined(move_event)){
			//do level calc
			
			if(move_level == -1){
				move_cando = true;	
			} else if (move_level <= level) {
				move_cando = true;
			}
			
			if(string_lower(move_event) == "none" || string_lower(move_event) == ""){
				event_cando = true;	
			} else if (eventDialogCheck(move_event)){
				event_cando = true;	
			}
			
			if(move_cando && event_cando){
				ds_list_add(move_list,move);	
			}
		}
	}
}