
		p1x = argument[0];
		p1y = argument[1];
		q1x = argument[2];
		q1y = argument[3];
		p2x = argument[4];
		p2y = argument[5];
		q2x = argument[6];
		q2y = argument[7];
	    // Find the four orientations needed for general and
	    // special cases
	    o1 = playerCheckIntersectOrientation(p1x, p1y, q1x,q1y, p2x,p2y);
	    o2 = playerCheckIntersectOrientation(p1x, p1y, q1x,q1y, q2x,q2y);
	    o3 = playerCheckIntersectOrientation(p2x, p2y, q2x,q2y, p1x,p1y);
	    o4 = playerCheckIntersectOrientation(p2x, p2y, q2x,q2y, q1x,q1y);
 
	    // General case
	    if (o1 != o2 && o3 != o4)
	        return true;
 
	    // Special Cases
	    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
	    if (o1 == 0 && playerCheckIntersectSegment(p1x,p1y, p2x,p2y, q1x,q1y)) return true;
 
	    // p1, q1 and q2 are colinear and q2 lies on segment p1q1
	    if (o2 == 0 && playerCheckIntersectSegment(p1x,p1y, q2x,q2y, q1x,q1y)) return true;
 
	    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
	    if (o3 == 0 && playerCheckIntersectSegment(p2x,p2y, p1x,p1y, q2x,q2y)) return true;
 
	     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
	    if (o4 == 0 && playerCheckIntersectSegment(p2x,p2y, q1x,q1y, q2x,q2y)) return true;
 
	    return false; // Doesn't fall in any of the above cases

