/// @description Insert description here
// You can write your code in this editor
if(global.vcr_on && !global.disableShader){
	with(shd_vcr_obj){
		
		if(!surface_exists(surf)){
			surf = surface_create_access(global.window_width,global.window_height)	
		} else {
			if(ScaleManager.updated){
				surface_resize(surf,global.window_width,global.window_height);
			}
		}
	
		if(!surface_exists(final_surface)){
			final_surface = surface_create_access(global.window_width,global.window_height)	
		} else {
			if(ScaleManager.updated){
				surface_resize(final_surface,global.window_width,global.window_height);
			}
		}
		surface_set_target(surf);
		gpu_set_colorwriteenable(true,true,true,true)
		draw_clear_alpha(c_black, 1);
		gpu_set_colorwriteenable(true,true,true,false)
		/*if shader_enabled */shader_set(shd_gaussian_horizontal1);
		    shader_set_uniform_f(uni_resolution_hoz, var_resolution_x, var_resolution_y);
		    shader_set_uniform_f(uni_blur_amount_hoz, var_blur_amount);
		    draw_surface(argument[0],argument[1],argument[2]);
		shader_reset();
		surface_reset_target();

		surface_set_target(final_surface){
		gpu_set_colorwriteenable(true,true,true,true)
		draw_clear_alpha(c_black, 1);
		gpu_set_colorwriteenable(true,true,true,false)
			//Do vertical blur last
			if shader_enabled shader_set(shd_gaussian_vertical1);
			    shader_set_uniform_f(uni_resolution_vert, var_resolution_x, var_resolution_y);
			    shader_set_uniform_f(uni_blur_amount_vert, var_blur_amount);
			    draw_surface(surf,0,0);
			shader_reset();
		}
		surface_reset_target()

		shader_set(shd_vcr)
		shader_set_uniform_f(shd_vcr_glitch_properties,glitch_bars,glitch_speed);
		shader_set_uniform_f(shd_vcr_glitch_wave_properties,wave_speed,wave_amount,wave_jitter)

		shader_set_uniform_f(shd_vcr_counter,counter)
		shader_set_uniform_f(shd_vcr_jitter_properties,jitter_amount,jitter_speed)


		shader_set_uniform_f(shd_vcr_noise_strength,noise_strength)

		shader_set_uniform_f(shd_vcr_vignette_properties,vignette_inner_circle,vignette_outer_circle)

		shader_set_uniform_f(shd_vcr_scan_properties,scan_linenumber,scan_amount)

		shader_set_uniform_f(shd_vcr_u_uv,u_uv_x,u_uv_y)

		draw_surface(final_surface,0,0)

		shader_reset()
		gpu_set_colorwriteenable(true,true,true,true)
	}
} else {
	draw_surface(argument[0],argument[1],argument[2]);
}