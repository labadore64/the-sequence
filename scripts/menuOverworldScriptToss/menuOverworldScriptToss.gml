removeItem(menuOverworldItems.item_val,menupos)

tts_say(string(menupos) + " tossed. ");

instance_destroy();

with(menuOverworldItemSub){
	instance_destroy();	
}

with(menuOverworldItems){
	menuOverworldItemsUpdateSublists();
	itemData(item_id [| menupos])
	if(ds_map_empty(obj_data.inventory)){
		instance_destroy();	
	}
}