update_image_only_active = false;
var script_for_update = menuOverworldKeyboardKeybindStart

menuAddOption(global.langini[9,14],global.langini[10,14],menuOverworldKeyboardKeybindSetDefault)
menuAddOption("","",-1);
menuAddOption("Global Controls","Controls used through the whole game.",-1);
menuAddOption(global.langini[9,0],global.langini[10,0],script_for_update) // select
menuAddOption(global.langini[9,1],global.langini[10,1],script_for_update) // select 2
menuAddOption(global.langini[9,2],global.langini[10,2],script_for_update) // cancel
menuAddOption(global.langini[9,3],global.langini[10,3],script_for_update) // up
menuAddOption(global.langini[9,4],global.langini[10,4],script_for_update) // down
menuAddOption(global.langini[9,5],global.langini[10,5],script_for_update) // left
menuAddOption(global.langini[9,6],global.langini[10,6],script_for_update) // right
menuAddOption(global.langini[9,20],global.langini[10,20],script_for_update) // left Tab
menuAddOption(global.langini[9,21],global.langini[10,21],script_for_update) // right Tab
menuAddOption(global.langini[9,7],global.langini[10,7],script_for_update) // open menu
menuAddOption(global.langini[9,8],global.langini[10,8],script_for_update) // info
menuAddOption("","",-1);
menuAddOption("Function Controls","Controls used for specific global functions.",-1);
menuAddOption(global.langini[9,11],global.langini[10,11],script_for_update) // help
menuAddOption("Screenshot","Takes a screenshot!",script_for_update) // Screenshot
menuAddOption(global.langini[9,13],global.langini[10,13],script_for_update) // exit
menuAddOption("","",-1);
menuAddOption("Field Controls","Controls used for the field.",-1);
menuAddOption(global.langini[9,9],global.langini[10,9],script_for_update) // run
menuAddOption("","",-1);
menuAddOption("AX Controls","Controls used for accessibility.",-1);
menuAddOption(global.langini[9,10],global.langini[10,10],script_for_update) // bookmark
menuAddOption(global.langini[9,12],global.langini[10,12],script_for_update) // HUD
for(var i = 0; i < 10; i++){
	menuAddOption(global.langini[9,17]+string(i),global.langini[10,17],script_for_update);
}

keybind_text = ds_list_create();

key_ids = ds_list_create();

ds_list_add(key_ids,"","","","select","select2","cancel","up","down","left","right","lefttab","righttab","open_menu","control")
ds_list_add(key_ids,"","","help","screenshot","exit")
ds_list_add(key_ids,"","","run")
ds_list_add(key_ids,"","","bookmark","hud")

for(var i = 0; i < 10; i++){
	ds_list_add(key_ids,"access"+string(i));
}

menuOverworldKeyboardSetKeybindText(keybind_text);

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menuParentSetTitle(global.langini[2,2])
menuParentSetSubtitle(global.langini[3,2])

menuParentUpdateBoxDimension();

menu_draw = menuOverworldOptionKeyboardDraw

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

//menu_up = menuOverworldOptionRealUp;
//menu_down = menuOverworldOptionRealDown;

//menu_left = menu_up;
//menu_right = menu_down;