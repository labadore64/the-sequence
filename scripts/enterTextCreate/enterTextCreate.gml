gml_pragma("forceinline");

/// @description Insert description here
// You can write your code in this editor
event_inherited();

double_select = false;

name_input = false;

//return_text = ""; //text to return after completed
current_text = ""; //current display text
max_character = 28; //maximum number of characters allowed for this input
obj_to_activate = -1; //what object should be activated when completed
position = 1;

box_height = 100;
box_width = 750;

box_x_offset = 10;
box_y_offset = 0;

character_space = 25

blink_rate = 20
blink = true;

with(ObjGeneric){
	active = false;	
}

newalarm[0] = 1
newalarm[1] = blink_rate

menu_draw = enterTextDraw;

x = 400
y = 300

state = 0;

menupos = 0;

sound_move = soundMenuDefaultMove;
sound_cancel = soundMenuDefaultCancel;

script_menu_open = enterTextEnter;
menu_left = enterTextLeft;
menu_right = enterTextRight;
menu_up = enterTextUp;
menu_down = enterTextDown;
menu_select = enterTextSelect
menu_cancel = enterTextCancel

calling_obj = noone;

letters = ds_list_create();

menu_size_name = 60;
menu_per_line = 10;

char_menupos = 0;
counter = 0;
ds_list_add(letters,"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",".","!","?","$","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","'","\"","/"," ")

//keypresses for name input