if(binocular_item){
	binoculars = true;
	with(CameraOverworld){
		binoculars = true;	
	}
} else if (prop_placing){
	var xx = x + cos(degtorad(orientation))*prop_place_dist;
	var yy = y + sin(degtorad(orientation))*prop_place_dist;
	
	var cando = true;
	
	//add a check to make sure the item doesn't have a collision.
	var dummy = instance_create(xx,yy,prop_place_id)
	var xx1 = 0;
	var yy1 = 0;
	var xx2 = 0;
	var yy2 = 0;
	
	//do the check here.
	with(dummy){
		var width = abs(sprite_bbox_right - sprite_bbox_left)*.5
		var height = abs(sprite_bbox_bottom - sprite_bbox_top)*.5	
		
		var size_const = 64

		var scalex = draw_scalex*(width*2)/size_const;
		var scaley = draw_scaley*(height*2)/size_const;
	
		//offset x and y coords based on where the center of the collision box lies
	
		var collide_centerx = sprite_get_bbox_left(sprite_index) + width-size_const;
		var collide_centery = sprite_get_bbox_top(sprite_index) + height-size_const;
	
		var xxxx = x+collide_centerx;
		var yyyy = y+collide_centery;
		
		xx1 = xxxx-width;
		xx2 = xxxx+width;
		yy1 = yyyy-height;
		yy2 = yyyy+height;
	}
	
	//now that we have the boundaries do a collision test between the collisions
	
	var check = false;
	
	with(Collision){
		check = collisionTestRectangle(id,xx1,yy1,xx2,yy2)	
		if(check){
			cando = false;	
		}
	}
	
	//destroy the dummy object used in the test.
	
	with(dummy){
		instance_destroy();	
	}
	
	if(cando){
	
		placeProp(prop_place_id,xx,yy);
		removeItem(prop_place_item,1);
	
		prop_place_id = -1;
		prop_placing = false;
		prop_place_item = -1;
		overworld_item_active = false;
		
		with(CameraOverworld){
			prop_place = false;
			prop_place_obj = -1;
		}
	} else {
		soundfxPlay(soundMenuInvalid);	
	}
} else if(spray_release_type >= 0){
	spray_lock = !spray_lock;

	if(!spray_lock){
		spray_trigger_counter= 0
		
	}

}