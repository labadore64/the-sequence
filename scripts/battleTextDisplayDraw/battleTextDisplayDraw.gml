/// @description Insert description here
// You can write your code in this editor
gml_pragma("forceinline");

with(battleTextDisplay){
	if(draw_me){
		if(!surface_exists(text_surface)){
			text_surface = surface_create_access(800*global.scale_factor,(600-485+50)*global.scale_factor);
		}
		
		if(!surface_exists(display_surface)){
			display_surface = surface_create_access(800*global.scale_factor,(600-485)*global.scale_factor);	
		}
		
		surface_set_target(text_surface);
		draw_clear(c_black);
		
		//draw text.
		draw_set_color(global.textColor);
		draw_set_font(global.textFont);
		
			for(var i = 0; i < show_up_to +1; i++){
				obj = display_list[|i];
				if(!is_undefined(obj)){
					draw_text_transformed_textbox_ratio(20,10+display_spacer*i,obj,2,2,0)
				}
			}
		surface_reset_target();

		surface_set_target(display_surface)
		draw_surface(text_surface,0,0- display_animation_spacer*display_animation_counter)
		draw_set_color(c_black)
		draw_rectangle(0,100*global.scale_factor,800*global.scale_factor,120*global.scale_factor,false)
		/*
		draw_clear(c_black);

		var obj = -1;
		
		draw_set_color(global.textColor);
		if(display_animation){
			for(var i = 0; i < display_number+1; i++){
				obj = display_list[|i];
				if(!is_undefined(obj)){
					draw_text_transformed_ratio(20,10+display_spacer*i - display_animation_spacer*display_animation_counter,obj,2,2,0)
				}
			}
		} else {
			for(var i = 0; i < display_number; i++){
				obj = display_list[|i];
				if(!is_undefined(obj)){
					draw_text_transformed_ratio(20,10+display_spacer*i,obj,2,2,0)
				}
			}
		}

		draw_set_color(c_black)
		draw_rectangle(0,0,800,5,false)
		draw_rectangle(0,100,800,125,false)
		*/
		
		surface_reset_target();

		draw_surface(display_surface,global.display_x,global.display_y+485*global.scale_factor)
		
		if(!text_finished){
				//draw_sprite_ext(spr_talk_continue,image_index,765,580,.5,.5,0,c_white,1);
		}
		
	}
}