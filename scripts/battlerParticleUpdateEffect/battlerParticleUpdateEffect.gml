if(particle_bank[argument[0]] != -1){

	var part_id = particle_type[argument[0]]	
	var emittere = particle_bank[argument[0]];

	if(orientation < 180){
		part_closer = false;	
	} else {
		part_closer = true;	
	}
	
    distancex = ori_cos*BattleHandler.part_distance[part_id]
    distancey = ori_sin*BattleHandler.part_distance[part_id]
    
    var part_center_x = x+distancex + BattleHandler.part_x[part_id]*image_yscale
    var part_center_y = y+distancey + BattleHandler.part_y[part_id]*image_yscale - jump_y*image_yscale +35
    var part_size_xscale = image_xscale
    var part_size_yscale = image_yscale
    
    var part_x_len = BattleHandler.part_width[part_id] * part_size_xscale*.5
    var part_y_len = BattleHandler.part_height[part_id] * part_size_yscale*.5
    
    //draw_rectangle(true)
	//part_system_clear(damage_part_system);
    
    part_emitter_region(main_part_system,emittere,part_center_x-part_x_len*.5,part_center_x+part_x_len*.5,part_center_y-part_y_len*.5,part_center_y+part_y_len*.5,ps_shape_ellipse,ps_distr_linear)
            
	part_emitter_burst(main_part_system, emittere, BattleHandler.part_type[part_id], BattleHandler.part_quantity[part_id]);
}