// returns true if the data was validated succesfully.
// this is a several step process
// it can strip/trim strings that might be too long (such as with names)
// and it can reject imports entirely if the stats are ridiculous

var result = true;

var sizer = ds_list_size(obj_data.characters);

var cando = true;

for(var i = 0; i < sizer; i++){
	dataLoadValidateCharacter(obj_data.characters[|i]);
}

// validate inventory

var size, key, i;
size = ds_map_size(obj_data.inventory);
key = ds_map_find_first(obj_data.inventory);
for (i = 0; i < size; i++;)
{
	if(!is_undefined(key)){
		// if its an invalid item, remove it from inventory!
		if(key < 0 || key >= objdata_items.data_size){
			ds_map_delete(obj_data.inventory,key);
		}
		
		key = ds_map_find_next(obj_data.inventory, key)
	} else {
		break;	
	}
}

return true;