
if(battlerHasAbility(target,"smoke")){
	if(!battlerHasFilter(character,"smoke")){
		if(battlerIsAlive(target) && battlerIsAlive(character)){
			if(random(1) < .2){
		
				var did = battlerAddFilter(character,"smoke");
		
				if(did == 1){
					var stringerr = "@0 is surrounded by smoke!";
			
					stringerr = string_replace_all(stringerr,"@0",character.name)
			
					battleAttackInputAddString(stringerr);
				}
			}
		}
	}
}

if(battlerHasAbility(character,"venom")){
	if(!battlerHasFilter(target,"poison")){
		if(battlerIsAlive(target) && battlerIsAlive(character)){
			if(random(1) < .2){
		
				var did = battlerAddFilter(character,"poison");
		
				if(did == 1){
					var stringerr = "@0 is poisoned!";
			
					stringerr = string_replace_all(stringerr,"@0",target.name)
			
					battleAttackInputAddString(stringerr);
				}
			}
		}
	}
}

if(battlerHasAbility(target,"thorn")){
	if(battlerIsAlive(target) && battlerIsAlive(character)){
		var damage_percent = 5;
		character.damage_calc += ceil(character.hp.base*(damage_percent*.01))
		var stringerr = "@0 takes @1% damage from thorns!";
			
		stringerr = string_replace_all(stringerr,"@0",character.name)
		stringerr = string_replace_all(stringerr,"@1",string(damage_percent))
			
		battleAttackInputAddString(stringerr);	
	}
}