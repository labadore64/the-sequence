var sprite_list = textboxArgumentGet("sprite_list");
var emotion_list = textboxArgumentGet("emotion_list");
var pitch_list = textboxArgumentGet("pitch_list");
var bg_list = textboxArgumentGet("background_list");
if(ds_exists(sprite_list,ds_type_list)){
	dialogScriptUpdateSprite(sprite_list);
	dialogScriptUpdateEmotion(emotion_list);
	dialogScriptUpdatePitch(pitch_list);
	dialogScriptUpdateBackground(bg_list);

	dialogGetSpritexy()
}