var filename = argument[0]
var map = ds_map_secure_load(filename +"option.sav");

if(ds_exists(map,ds_type_map)){
	

	global.textSpeed = map[? "textSpeed"];
	global.textColor = map[? "textColor"];
	global.textFont = map[? "textFont"];
	global.voice = map[? "textVoice"];
	global.drawVignette = map[? "drawVignette"];
	global.menuParticles = map[? "menuParticles"];
	global.brightness = map[? "brightness"];
	global.contrast = map[? "contrast"];
	
	if(!is_undefined(map[? "battleRotate"])){
		global.battleRotate = map[? "battleRotate"];
	}
	if(!is_undefined(map[? "overworld_on"])){
		global.overworld_on = map[? "overworld_on"];	
	}
	
	if(!is_undefined(map[? "battlePortrait"])){
		global.battlePortrait = map[? "battlePortrait"];
	}
	if(!is_undefined(map[? "attackSound"])){
		global.attackSound = map[? "attackSound"];
	}
	
	if(!is_undefined(map[? "menuAnimation"])){
		global.menuAnimation = map[? "menuAnimation"]	
	}
	
	if(!is_undefined(map[? "braille_practice"])){
		global.braille_practice = map[? "braille_practice"];
	}
	if(!is_undefined(map[? "BrailleSound"])){
		global.BrailleSound = map[? "BrailleSound"];
	}
	if(!is_undefined(map[? "BrailleTimerSpeed"])){
		global.BrailleTimerSpeed = map[? "BrailleTimerSpeed"];
	}
	if(!is_undefined(map[? "brailleRepeat"])){
		global.brailleRepeat = map[? "brailleRepeat"];
	}
	if(!is_undefined(map[? "brailleBackground"])){
		global.brailleBackground = map[? "brailleBackground"];
	}
	if(!is_undefined(map[? "countdownSound"])){
		global.countdownSound = map[? "countdownSound"];
	}
	if(!is_undefined(map[? "brailleHint"])){
		global.brailleHint = map[? "brailleHint"];	
	}
	
	ds_map_destroy(map);
	return true;
} else {
	return false;	
}

