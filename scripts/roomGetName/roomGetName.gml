switch(argument[0]){
	case room0:
	return "Boot"
	
	case intro:
	return "Intro room"
	
	case room1:
	return "Debug1"
	
	case room2:
	return "Debug2"
	
	case battle_room:
	return "Battle"
	
	case room11:
	return "Debug3"
	
	case gamepad_test:
	return "Gamepad Test"
	
	case name_room:
	return "Name"
	
	case gameload_room:
	return "Game Load"
	
	//dummies
	
	case RoomDummy:
	return "Room Dummy"
	
	//actual rooms
	case CinderForestYourHouseRoom2:
	case CinderForestYourHouseRoom1:
	return "Your House"
	
	case CinderForestMarkRoom1:
	return "Mark's House"
	
	case CinderForestOutside1:
	return "Cinder Town"
	
	case newDemoRoom2:
	return "Outside";
	
	case newDemoRoom1:
	return "House";
	
	default:
	return "No room name"
}