if(bird_sprite != -1){
	
	gpu_set_blendmode(bm_add)
	
	draw_rectangle_color_ratio(500,400,800,700,c_black,c_black,$333300,$333300,false)
	
			draw_set_font(global.normalFont)
			draw_set_halign(fa_right);
			draw_text_transformed_ratio(690,140,"Element:",3,3,0)
			draw_sprite_extended_ratio(element_sprite,0,740,150,2.5,2.5,0,c_white,1);
			draw_set_halign(fa_left);
	
	gpu_set_blendmode(bm_normal)
	if(bird_state == DEX_STATE_OWN){
		draw_sprite_extended_ratio(bird_sprite,img,650,545-jump_y,-1,1,0,c_white,1)
	
		draw_set_color(global.textColor)
		if(element_sprite != -1){
			draw_text_transformed_ratio(30,190,bird_desc,2,2,0)
		}
	} else {
		if(bird_sprite != -1){
			draw_sprite_extended_ratio(bird_sprite,0,650,545-jump_y,-1,1,0,c_black,.75)	
		}
	}
	
	draw_set_font(global.largeFont)
	draw_text_transformed_ratio(30,130,bird_name,2,2,0)
	draw_set_halign(fa_right);
	draw_text_transformed_ratio(480,130,"#"+string(bird_list[|menupos]+1),2,2,0)
	draw_set_halign(fa_left);
	draw_set_font(global.normalFont)
	
}