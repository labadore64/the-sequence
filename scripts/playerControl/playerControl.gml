//checks if you have the item

if(!CameraOverworld.binoculars){
	var findvalue = ds_map_find_value(obj_data.inventory,string(obj_data.set_item));

	if(is_undefined(findvalue)){
		obj_data.set_item = -1;	
	}

	if(obj_data.set_item == -1){
		textboxOverworld("item_set")
	} else {
		itemData(obj_data.set_item);
		if(overworld_effect != -1){
			active = false;
			script_execute(overworld_effect);
		}
	}
} else {
	script_execute(menu_cancel)
}