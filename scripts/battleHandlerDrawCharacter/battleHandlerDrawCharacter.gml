gml_pragma("forceinline");


var chara = -1;
gpu_set_colorwriteenable(true,true,true,false);
while(!ds_priority_empty(character_draw_depth)){
	chara = ds_priority_delete_min(character_draw_depth);
	if(!is_undefined(chara)){
		if(instance_exists(chara)){
			with(chara){
				if(!dont_draw){
					drawBattlerMain();
				}
			}
		}
	}
}
gpu_set_colorwriteenable(true,true,true,true);
gpu_set_tex_filter(false)
with(Battler){
	drawBattlerHPMP();	
}