var compare = argument[0];

with(obj_data){
	for(var i = 0; i < item_event_total; i++){
		if(item_event_id[i] == compare){
			return i;
		}
	}
}

return -1;