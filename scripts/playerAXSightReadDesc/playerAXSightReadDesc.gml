if(objGenericCanDo()){
	if(access_mode() || global.tts){
		if(global.AXVisionEnabled){
			if(sight_detector.looking_obj > 0){
				tts_say(sight_detector.looking_obj.ax_desc);	
			} else {
				tts_say("Not looking at anything")	
			}
		}
	}
}