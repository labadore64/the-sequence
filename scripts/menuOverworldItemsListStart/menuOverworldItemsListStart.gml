if(ds_exists(menu_name,ds_type_list)){

ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items

menuOverworldItemsInitLists();

menuOverworldItemsCalculateSublists(argument[0]);


menu_option = global.overworldItemBag
menupos = global.overworldItemPosition 

if(ds_list_empty(list_of_lists[|menu_option])){
	menuOverworldItemsSetItStraight(argument[0]);
}
menuOverworldItemSetList(argument[0]);

hudPopulateItem("item",item_id[|menupos]);

itemData(item_id [| menupos])

}