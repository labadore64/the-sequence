if(battlerHasAbility(target,"wolf") && battlerIsAlive(target) && battlerIsAlive(character)){
	if(random(1) < .4){
		//add attack
		var taa = target;
		var chaa = character;
		
		with(battleStartAttack){
			var priority_add = priority[0];
	
			var ha = instance_create(0,0,battleAttackInput);

			ha.character = taa;
			ha.target = chaa
			ha.attack = objdataToIndex(objdata_moves,"attack");
			ha.priority = -2
			
			ds_list_add(priority[1],ha)

		}
		
		var stringerr = "@0's is enraged!";
			
		stringerr = string_replace_all(stringerr,"@0",target.name)
			
		battleAttackInputAddString(stringerr);
	}
}

if(battlerHasAbility(target,"harden") && battlerIsAlive(target) && battlerIsAlive(character)){
	if(random(1) < .1){
		battleStatChangeAdd(target,BATTLE_STAT_DEFENSE,2,2);
		
		var stringerr = "@0 hardens its skin! Its physical guard is boosted temporarily!";
			
		stringerr = string_replace_all(stringerr,"@0",target.name)
			
		battleAttackInputAddString(stringerr);
	}
}

if(battlerHasAbility(target,"meditate") && battlerIsAlive(target) && battlerIsAlive(character)){
	if(random(1) < .1){
		battleStatChangeAdd(target,BATTLE_STAT_RESISTANCE,2,2);
		
		var stringerr = "@0 concentrates its mind! Its magic guard is boosted temporarily!";
			
		stringerr = string_replace_all(stringerr,"@0",target.name)
			
		battleAttackInputAddString(stringerr);
	}
}