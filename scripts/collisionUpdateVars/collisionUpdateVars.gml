flip_edge = false;


var test_angle = (image_angle+360*50) mod 360;

if(test_angle > 90 && test_angle < 270){
	//if the test angle is upside down
	image_angle -=180;
}

spritewidth = draw_scalex*sprite_get_width(sprite_index)*.5
spriteheight = draw_scaley*sprite_get_height(sprite_index)*.5

var lean_left = true; //is it leaning with the left side lower?

var test_im = ((image_angle+360*5000) mod 360);

if(test_im > 270){
	lean_left = false;	
}


var x1 = -spritewidth;
var y1 = -spriteheight;

var x2 = +spritewidth;
var y2 = -spriteheight;

var x5 = -spritewidth;
var y5 = +spriteheight;

var x6 = +spritewidth;
var y6 = +spriteheight;

var a = -degtorad(image_angle-90);

var x3 = y1*cos(a) - x1*sin(a);
var y3 = y1*sin(a) + x1*cos(a);

var x4 = y2*cos(a) - x2*sin(a);
var y4 = y2*sin(a) + x2*cos(a);

var x7 = y5*cos(a) - x5*sin(a);
var y7 = y5*sin(a) + x5*cos(a);

var x8 = y6*cos(a) - x6*sin(a);
var y8 = y6*sin(a) + x6*cos(a);

pointsx[0]=x3+x
pointsy[0]=y3+y
pointsx[1]=x4+x
pointsy[1]=y4+y
pointsx[3]=x7+x
pointsy[3]=y7+y
pointsx[2]=x8+x
pointsy[2]=y8+y


topleftx = floor(x3+x);
toplefty = floor(y3+y);
toprightx = floor(x4+x);
toprighty = floor(y4+y);

bottomleftx = floor(x7+x)
bottomlefty = floor(y7+y);
bottomrightx = floor(x8+x);
bottomrighty = floor(y8+y);

xxleft = min(topleftx,bottomleftx,toprightx,bottomrightx)
xxright = max(topleftx,bottomleftx,toprightx,bottomrightx)

ytoptop = min(toplefty,toprighty,bottomlefty,bottomrighty);
ybotbottom = max(bottomlefty,bottomrighty,toplefty,toprighty)

collInFrame = collisionInFrame();

//calculatate proximity from player

for(var i = 0; i < 4; i++){
	player_coll_last[i] = 100000000; //distance from coll.	
}
collisionCalculateDistances();

//calculate angles

colliding = false;

line12_angle = point_direction(topleftx,toplefty,toprightx,toprighty);
line23_angle = point_direction(toprightx,toprighty,bottomrightx,bottomrighty);
line34_angle = point_direction(bottomrightx,bottomrighty,bottomleftx,bottomlefty);
line41_angle = point_direction(bottomleftx,bottomlefty,topleftx,toplefty);

//which side would the collision be on?

line12_test = false;
line23_test = false;
line34_test = false;
line41_test = false;