var chara = argument[0];

var texts = filterGetText("spore")

with(chara){
	var damg = floor(hp.base * .1)
	
	var changed = hp.current - damg;
		
	battlerHPChange(id,changed)
		
	var stringer = texts[|0];
	
	battleAnimHit(id);
		
	stringer = string_replace_all(stringer,"@0",name);
	stringer = string_replace_all(stringer,"@1",string(10));
	battleTextDisplayAddText(stringer);
	
	var stringer = texts[|1];
		
	stringer = string_replace_all(stringer,"@0",name);
	stringer = string_replace_all(stringer,"@1",string(10));
	battleTextDisplayAddText(stringer);
		
	if(!battlerIsAlive(id)){
		stringer = "@0 was defeated!"
		stringer = string_replace_all(stringer,"@0",name);
		battleTextDisplayAddText(stringer);
	}
}

ds_list_destroy(texts);