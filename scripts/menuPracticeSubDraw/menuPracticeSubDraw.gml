
if(active){
	draw_set_color(c_black)
	draw_rectangle_ratio(0,550,800,600,false)

	gpu_set_colorwriteenable(true,true,true,false);
	draw_set_alpha(.75)
	draw_set_color(c_black)
	draw_rectangle_ratio(0,0,800,600,false);
	draw_set_alpha(1)
	draw_set_color(global.textColor)

	draw_set_halign(fa_left)


	if(animation_len == 0){
		menuParentDrawBox();
	
		//menuParentDrawText();
	
	
		menuParentDrawOptionsNoGradient();
	


		gpu_set_colorwriteenable(true,true,true,true)

		menuParentDrawInfo();

		with(braille_obj){
			brailleCharacterDrawAgain();
		}
	
		draw_set_color(global.textColor)
		draw_set_alpha(1)
		draw_set_halign(fa_center)
		draw_text_transformed_ratio(490,105,title,4,4,0)
		draw_text_transformed_ratio(490,340,desc,2,2,0)
		draw_set_halign(fa_left)
	}
}
draw_letterbox()