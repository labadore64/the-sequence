gml_pragma("forceinline");

var partyy = obj_data.party;
var partyy_size = ds_list_size(partyy);
for(i = 0; i < partyy_size; i++){
	if(!is_undefined(partyy[|i])){
		with(partyy[|i]){
			statLoadMain(id);
		}
	}
}

with(dataDrawStat){
	draw_hex_init(character,120,character.phy_power,
				character.phy_guard ,
				character.mag_power ,
				character.mag_guard ,
				character.spe_agility ,
				character.spe_mpr);
	radius = 120;
}