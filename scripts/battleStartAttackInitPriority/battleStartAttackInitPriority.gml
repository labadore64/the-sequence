//priority levels:
//0 = -3
//1 = -2
//2 = -1
//3 = 0
//4 = 1
//5 = 2
//6 = 3

var middle = 3; //represents where priority 0 is

for(var i =0; i < 7; i++){
	priority[i] = ds_list_create();
}

var attacks = -1;

with(BattleHandler){
	var partysize = ds_list_size(party_list);
	var oo = -1;
	
	//adds moves for repeat.
	for(var i = 0; i < partysize; i++){
		oo = party_list[|i];
		if(!is_undefined(oo)){
			if(instance_exists(oo)){
				if(battlerHasFilter(oo,"repeat")){
					battleAttackInputCreate(oo,oo.last_target,oo.last_move);	
				}
			}
		}
	}
	attacks = attack_input
}

var sizer = ds_list_size(attacks);
var obj = -1;
var test_priority = 0;
var test_lister = 0;
//insert each attack into the proper priority bracket
for(var i = 0; i < sizer; i++){
	obj = attacks[|i];
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			
			if(obj.attack != objdataToIndex(objdata_moves,"flee")){
				if(battlerHasFilter(obj.character,"repeat")){
					if(obj.character.last_move != -1){
						obj.target = obj.character.last_target;
						obj.attack = obj.character.last_move;
						with(obj){
							moveData(attack);	
							priority = speed_priority;
						}
					}
				}
			}

			
			test_priority = obj.priority;
			
			test_priority = battleStartChangePriorityEffect(test_priority,obj.character);
			
			test_lister = priority[test_priority+middle];
			ds_list_add(test_lister,obj);
		}
	}
}


