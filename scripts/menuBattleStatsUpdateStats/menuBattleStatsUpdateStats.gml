chaa = argument[0];

var stat1 = argument[1];
var stat2 = argument[2];
var stat3 = argument[3];
var stat4 = argument[4];
var stat5 = argument[5];
var stat6 = argument[6];

battleHandlerSelectAllyClear()
battleHandlerSelectAlly(menupos,true)

get_colorz = chaa.color;

var myred = color_get_red(get_colorz);
var mygreen = color_get_green(get_colorz);
var myblue = color_get_blue(get_colorz)

red_ratio = (myred-127)/127;
green_ratio = (mygreen-127)/127;
blue_ratio = (myblue-127)/127;

red_color = make_color_rgb(myred,255-myred,255-myred);
green_color = make_color_rgb(255-mygreen,mygreen,255-mygreen);
blue_color = make_color_rgb(255-myblue,255-myblue,myblue);

with(dataDrawStat){
	character = argument[0];
	draw_hex_init(character,120,stat1,stat2,stat3,stat4,stat5,stat6);
	radius = 120;
}