//counts how many characters have a certain filter on the character's side

var chara = argument[0];
var filter = argument[1];

var returner = 0;

if(instance_exists(chara)){
	var side = chara.is_enemy;

	with(Battler){
		if(is_enemy == side){
			if(battlerHasFilterIgnoreNull(id,filter)){
				returner++;	
			}
		}
	}
}

return returner;