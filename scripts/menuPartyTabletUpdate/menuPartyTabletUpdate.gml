val_phy_power = 0;
val_phy_guard = 0
val_mag_power = 0;
val_mag_guard = 0
val_spe_power = 0;
val_spe_guard = 0
multtt = 0
move_id = -1;

move_element_sprite = -1
move_type_sprite = -1
the_char = "";
brailleDataSetCharacter("",braille_obj);	

if(character_select.tablet[menupos] > -1){
	move_id = objdata_tablet.move_id[character_select.tablet[menupos]];
	the_char = string_upper(objdata_tablet.char[character_select.tablet[menupos]])
	brailleDataSetCharacter(objdata_tablet.char[character_select.tablet[menupos]],braille_obj);	
	
	val_phy_power = objdata_tablet.phy_power[tablet[menupos]]
	val_phy_guard = objdata_tablet.phy_guard[tablet[menupos]]
	val_mag_power = objdata_tablet.mag_power[tablet[menupos]]
	val_mag_guard = objdata_tablet.mag_guard[tablet[menupos]]
	val_spe_power = objdata_tablet.spe_power[tablet[menupos]]
	val_spe_guard = objdata_tablet.spe_guard[tablet[menupos]]
	
	if(move_id > -1){
		multtt = objdata_moves.damage[move_id]/maxdamage;

		move_element_sprite = getTypeSprite(objdata_moves.element[move_id])
	
		if(objdata_moves.move_type[move_id] == MOVE_TYPE_PHYSICAL){
			move_type_sprite = spr_typePhys
		} else {
			move_type_sprite = spr_typeMag
		}	
	}
}


hudControllerClearData();
hudControllerClearObjects();
if(tablet[menupos] > -1){
	menu_ax_HUD = "tabletDetails"
	hudPopulateTablet("tablet",tablet[menupos]);
	hudPopulateOverworldStats("character",character_select.character);
} else {
	menu_ax_HUD = "empty"
}
	
	
// Inherit the parent event
active = true;

with(menuControl){
	please_draw_black = false;	
}

if(menu_ax_HUD != ""){
	infokeys_assign(menu_ax_HUD);
}

