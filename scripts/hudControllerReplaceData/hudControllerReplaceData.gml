// this will remove all variables from strings in the HUD controller.
// the way you reference strings is by doing the following: {{[stringname]}}
// argument1 is the reference object. i

var stringer = argument[0];

with(HUDController){
	var size, key, i;

	size = ds_map_size(data);
	key = ds_map_find_first(data);

	for (i = 0; i < size; i++;)
	{
		stringer = hudControllerReplaceString(key,stringer);
		key = ds_map_find_next(data, key);
	}
}

// get variables relative to object
if(argument_count > 1){
	stringer = hudControllerReplaceObjVariables(stringer,argument[1]);	
}

// convert any lingering chains into [[ ]]
stringer = string_replace_all(stringer,"{{","[[");
stringer = string_replace_all(stringer,"}}","]]");

// do the as conversions.
stringer = hudControllerStringAsItem(stringer);
stringer = hudControllerStringAsMove(stringer);
stringer = hudControllerStringAsTabletMove(stringer);
stringer = hudControllerStringAsTabletCharacter(stringer);
stringer = hudControllerStringAsElement(stringer);
stringer = hudControllerStringAsType(stringer);

// finally remove braces

stringer = string_replace_all(stringer,"[[","");
stringer = string_replace_all(stringer,"]]","");

return stringer;