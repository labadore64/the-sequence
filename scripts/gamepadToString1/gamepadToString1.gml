/// @function gamepadToString(gp_button)
/// @description Turns a gamepad button into a string.
/// @param {real} gp_button The gamepad button.

	var key = argument[0];
	if(key != 0){

		switch(key){
			case gp_face1:
			return "Button1"
		
			case gp_face2:
			return "Button2";
	
			case gp_face3:
			return "Button3";
	
			case gp_face4:
			return "Button4";
	
			case gp_padd:
			return "Down";
	
			case gp_padl:
			return "Left";
	
			case gp_padr:
			return "Right";
	
			case gp_padu:
			return "Up";
	
			case gp_start:
			return "Start";
	
			case gp_select:
			return "Select"
	
			case gp_shoulderl:
			return "Left Shoulder 1"
	
			case gp_shoulderlb:
			return "Left Shoulder 2"
	
			case gp_shoulderr:
			return "Right Shoulder 1"
	
			case gp_shoulderrb:
			return "Right Shoulder 2"
	
			case gp_stickl:
			return "Left Stick"
	
			case gp_stickr:
			return "Right Stick"
		}
	}
	
	return "-1"