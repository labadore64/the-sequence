var returner = argument[0]
var chara = argument[1];

if(battlerHasFilter(chara,"chain")){
	returner = -3;	
}

if(battlerHasFilter(chara,"rush")){
	returner++;
	if(returner > 3){
		returner = 3;	
	}
}

return returner;