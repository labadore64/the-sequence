var minn = 1;
var maxx = 1.5;

var diff = maxx-minn;

var amount = argument[0];

if(amount < 0){
	amount = abs(amount);	
} else {
	amount--;	
}

if(amount >= 7){
	return maxx;	
} else if (amount >= 6){
	return 	minn + diff*.66;
} else if (amount >= 5){
	return minn + diff*.33;	
}

return minn;