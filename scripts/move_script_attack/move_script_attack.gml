//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var sizer = ds_list_size(battletext);

var stringer = "";

for(var i = 0; i < sizer; i++){

	stringer = battletext[|i];

	stringer = string_replace_all(stringer,"@0",chara.name);
	stringer = string_replace_all(stringer,"@1",targe.name);

	battleAttackInputAddString(stringer);
}

ds_list_destroy(battletext);