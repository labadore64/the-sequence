var instanceee = -1;

if(!instance_exists(menuCharacterSwitchPartyMoves)){
	instanceee = instance_create(0,0,menuCharacterSwitchPartyMoves)	
}
menuControlForceDrawMenuBackground();

if(instanceee != -1){
	if(object_index == menuCharacterSwitchRecruitSub){
		with(menuCharacterSwitchRecruit){
			instanceee.chaa = recruit_list[|menupos]
		}
	} else {
		with(menuCharacterSwitchParty){
			instanceee.chaa = obj_data.party[|menupos]
		}
	}
}