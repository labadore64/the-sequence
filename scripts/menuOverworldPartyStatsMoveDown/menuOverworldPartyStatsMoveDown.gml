
menupos++;

if(menupos > menu_size-1){
	if(wrap){
		menupos = 0;	
	} else {
		menupos = menu_size-1;
	}
}

menuParentUpdate();

character = obj_data.party[|menupos];

portrait.character = character.character

with(portrait){
	event_perform(ev_alarm,0)
	x_scalefactor = .75;
	y_scalefactor = .75
	update_surf = true;
	image_xscale = global.scale_factor*x_scalefactor;
	image_yscale = global.scale_factor*y_scalefactor;
	
	x = 256*global.scale_factor*x_scalefactor;
	y = 256*global.scale_factor*y_scalefactor;
}

with(menuOverworldPartySub){
	ds_list_clear(menu_name)
	ds_list_clear(menu_description)
	ds_list_clear(menu_script)	
	
	var namerr = "";
	with(menuOverworldPartyStats){
		namerr = character;	
	}

	character_name = namerr.name
	character = namerr;

	menuAddOption("Stats","View " + character.name + "'s stats.",menuOverworldStatsScript)
	menuAddOption("Switch","Change " + character.name + "'s party position.",menuOverworldSwitchScript)
	menuAddOption("Talk","See what " + character.name +" has to say.",-1)
	SHORTHAND_CANCEL_OPTION;
	with(menuOverworldParty){
	
		with(portrait){
			instance_destroy();	
		}
	
		portrait = instance_create_depth(0,0,0,drawPortrait)
		portrait.character = namerr.character

	
		with(portrait){
			x_scalefactor = .75
			y_scalefactor = .75
			update_surf = true;
			image_xscale = global.scale_factor*x_scalefactor;
			image_yscale = global.scale_factor*y_scalefactor;
	
			x = 256*global.scale_factor*x_scalefactor;
			y = 256*global.scale_factor*y_scalefactor;
		}
	}
}

if(menu_size == 2){
	var poss = (menupos+1) % 2
	var chh = obj_data.party[|poss];
	
	othername[0] = chh.character
	othername[1] = chh.character
} else {
	var poss = (menupos+1) % 3
	var chh = obj_data.party[|poss];
	
	othername[1] = chh.character
	
	var poss = (menupos+menu_size-1) % 3
	var chh = obj_data.party[|poss];
	
	othername[0] = chh.character
}

menuOverworldPartyUpdateStats(character,100);
tts_clear();

mouseHandler_update_text(1,objdata_character.name[othername[0]])
mouseHandler_update_text(2,objdata_character.name[othername[1]])
mouseHandler_update_text(0,character.name + " Details")
tts_say(character.name);
hudPopulateOverworldStats("character",character)
surface_update = true;