// arg0 = tablet power
// arg1 = level multiplier

return ceil(argument[1] * argument[0]);