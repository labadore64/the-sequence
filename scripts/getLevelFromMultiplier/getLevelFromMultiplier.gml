var grow_id = argument[0];
var current_exp = argument[1];
var max_exp = argument[2];

// gets the array of key point exp curves.
// the level is calculated between these.
for(var i = 0; i < 8; i++){
	if(current_exp < floor(objdata_exp.growth[grow_id,i]*max_exp)){
		break;
	}
}

if(i >=8){
	return 100;	
} else if(i <= 0){
	return 1;	
}

// now that you know the level difference, calculate
var minlev = objdata_exp.EXPERIENCE[i-1];
var maxlev = objdata_exp.EXPERIENCE[i];

var minexp = floor(objdata_exp.growth[grow_id,i-1]*max_exp);
var maxexp = floor(objdata_exp.growth[grow_id,i]*max_exp);

var diff = (maxexp-minexp)/(maxlev - minlev);

while(minexp < current_exp){
	minlev++;
	minexp+=diff;
}

return minlev-1;