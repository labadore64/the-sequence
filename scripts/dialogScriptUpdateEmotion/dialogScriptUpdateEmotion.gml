var list = argument[0];

var emotionz = string_lower(list[|line]);

var sprite_set = -1;

if(instance_exists(portrait)){
	if(emotionz == "neutral"){
		sprite_set = EMOTION_NEUTRAL
	} else if (emotionz == "happy"){
		sprite_set = EMOTION_HAPPY
	} else if (emotionz == "sad"){
		sprite_set = EMOTION_SAD
	} else if (emotionz == "annoyed"){
		sprite_set = EMOTION_ANNOYED
	} else if (emotionz == "frustrated"){
		sprite_set = EMOTION_FRUSTRATED
	} else if (emotionz == "angry"){
		sprite_set = EMOTION_ANGRY
	} else if (emotionz == "shy"){
		sprite_set = EMOTION_SHY
	} else if (emotionz == "nervous"){
		sprite_set = EMOTION_NERVOUS
	} else if (emotionz == "joy"){
		sprite_set = EMOTION_JOY
	}

}


if(sprite_set != -1){
	with(portrait){
		emotion = sprite_set;	
	}
	
	with(DialogHandler.portrait){
		emotion = sprite_set;	
	}
}