var list_to_use = -1;
if(menupos == 0){
	list_to_use = text_speed_strings;
} else if (menupos == 1){
	list_to_use = text_color_strings;
} else if (menupos == 2){
	list_to_use = text_font_strings;
} else if (menupos == 3){
	list_to_use = text_sound_strings;
}

if(object_index == menuOverworldTextOption){
	if(menupos == 4){
		list_to_use = text_metric_strings;	
	} else if (menupos == 5 || menupos == 6){
		list_to_use = text_tts_strings;	
	}
}
var stringer = list_to_use[|curposs[menupos]] + " " + menu_name_array[menupos] + ": " + menu_desc_array[menupos];
tts_say(string_replace_all(stringer,"Lv. ",""));
