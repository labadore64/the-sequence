//jump
if(is_jumping){
	var jump_last = jump_y;
	jump_counter+=menuControl.timer_diff;
	jump_y = sin(jump_counter*pi/jump_time)*jump_height;
	jump_percent = 1-(jump_y/jump_height)*.5;
	jump_delta = jump_y - jump_last;
	
	if(!is_hurting){
		if(sprite_state == BATTLER_SPRITE_IDLE ||
			sprite_state == BATTLER_SPRITE_JUMP0 ||
			sprite_state == BATTLER_SPRITE_JUMP1){
			if(jump_delta > 0){
				sprite_state = BATTLER_SPRITE_JUMP0	
			} else {
				sprite_state = BATTLER_SPRITE_JUMP1	
			}
		}
	}
	
	//if you land you stop
	if(jump_y < 0){
		jump_y = 0;
		jump_counter = 0;
		is_jumping = false;
		jump_delta = 0;
	}
}
