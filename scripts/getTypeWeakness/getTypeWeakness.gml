var type = argument[0];

switch(type){
	case TYPE_MIND:
	return TYPE_SPIRIT;
	
	case TYPE_SPIRIT:
	return TYPE_NATURE;
	
	case TYPE_NATURE:
	return TYPE_MIND;
}

return -1;