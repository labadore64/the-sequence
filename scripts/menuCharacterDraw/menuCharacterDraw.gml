
if(animation_len == 0){
	//menuParentDrawBox();

	draw_clear(c_black)
	
	var xx = character_sprite_x;
	var yy = character_sprite_y;
	with(character_sprite){
		draw_surface(surface,
					global.display_x+xx*global.scale_factor,
					global.display_y+yy*global.scale_factor);
	}
	draw_set_color(c_black)
	draw_rectangle_ratio(0,0,800,100,false)
	
	draw_set_color(global.textColor)
	draw_set_alpha(1)
	draw_set_font(global.largeFont)
	draw_text_transformed_ratio(40,30,title,3,3,0)
	
	draw_set_font(global.textFont)
	
	draw_set_halign(fa_left);
	menuCharacterDrawOptions();

	//menuParentDrawText();

	//menuParentDrawOptions();
	
	//gpu_set_colorwriteenable(true,true,true,true)

	draw_sprite_extended_ratio(spr_character_talk_bubble,spr_counter,talk_sprite_x,talk_sprite_y,3.25,2.5,0,c_white,1);

	if(active){
		with(obj_dialogue){
			objdialogDraw();	
		}
	}

	menuParentDrawInfo();
	
	draw_letterbox();
} else {
	//draw_clear(c_black)
	//menuParentDrawBoxOpen();
}