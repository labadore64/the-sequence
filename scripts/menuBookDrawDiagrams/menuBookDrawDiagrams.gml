var obj;
var menup = toppos;

var spacer = 40;

for(var i = 0; i < diagram_length; i++){
	obj = diagrams[|i];
	if(!is_undefined(obj)){
		with(obj){
			var space = y+(min_index-menup)*spacer;
			if(space >= 0 && space <= 540){
				if(draw){
					if(sprite_index > -1){
						draw_sprite_extended_ratio(sprite_index,0,x,space,image_xscale,image_yscale,image_angle,image_blend,1)
					}
				}
			}
		}
	}
}