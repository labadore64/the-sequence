var battler = argument[0];

var jumpheight = argument[1];
var jumptime = argument[2];

with(battler){
	if(!defeat_animation_trigger){
		if(!is_jumping){
			jump_height = jumpheight;
			is_jumping = true;
			jump_counter = 0;
			jump_time = jumptime;	
		}
	}
}