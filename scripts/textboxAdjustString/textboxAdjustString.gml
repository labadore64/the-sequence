	var i = 0;
   var string_lenger = string_length(argument[0]);
   var stringerr = argument[0];
   var lineEnd = argument[1] //line length
   var space = 0;
   var length = 0;
   var char_list = ds_list_create();
   
   for(var z = 1; z < string_lenger+1; z++){
		ds_list_add(char_list,string_char_at(stringerr,z))   
   }
   
   var returner = "";
   
   while(i < string_lenger)
    {   
        
        //Go to next line
        //Check if the current word fits inside the text box, and if not we go to the next line
        length = 0;
        while (char_list[|i] != " " && i <= string_lenger)
        {
            i++;
            length++;
        }
        
        if (space+length > lineEnd)
        {
            space = 0;
			returner += "\n"
        }
        i -= length;
        
		returner+=char_list[|i];
		
        //Move to next character
        space++;
        i++;
		
    }
	
	ds_list_destroy(char_list);
	
	return returner;