//must be in every move script

var chara = argument[0];
var targe = argument[1];

var battletext = moveGetBattleText(attack);

var stringer = battletext[|irandom_range(0,4)];

stringer = string_replace_all(stringer,"@0",chara.name);

battleAttackInputAddString(stringer);

var character_list = ds_list_create();

ds_list_copy(character_list,additional_targets);

ds_list_insert(character_list,0,targe);

var sizer = ds_list_size(character_list);
var obj = -1;

for(var i = 0; i < sizer; i++){
	obj = character_list[|i];
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			if(random(1) < .333){
			var cando = battlerAddFilter(obj,"blind");
			if(cando){
				stringer = battletext[|5];
				stringer = string_replace_all(stringer,"@1",obj.name);
				battleAttackInputAddString(stringer);
				}
			}
		}
	}
}

ds_list_destroy(battletext);
ds_list_destroy(character_list);