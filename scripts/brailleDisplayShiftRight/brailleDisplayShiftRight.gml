with(argument[0]){
	var replacechar;
	display_string = "";
	for(var i = count-1; i >= 0; i--){
		if(i > 0){
			replacechar = braille_display[i-1].cell_character;
		} else {
			replacechar = " ";	
		}
		brailleDataSetCharacter(replacechar,braille_display[i]);
		
		display_string = braille_display[i].cell_character+display_string;
	}	
}