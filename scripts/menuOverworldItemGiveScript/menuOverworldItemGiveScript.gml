var brea = giveItemToCharacter(menuOverworldItems.item_val,obj_data.party[|menupos])
if(menuOverworldItems.item_val > -1){
	tts_say(objdata_items.name[menuOverworldItems.item_val] + " given to " + menu_name_array[menupos]);
}
menuOverworldItemGiveSetItems();

with(menuOverworldItems){
	menuOverworldItemsUpdateSublists();
	itemData(item_id [| menupos])
}

if(!brea){
	with(menuOverworldItemSub){
		instance_destroy();	
	}
	if(ds_map_empty(obj_data.inventory)){
		with(menuOverworldItems){
			instance_destroy();	
		}
	}
	
	instance_destroy();
}

//if quantity is 0, close the menus