var list_to_use = -1;
if(menupos == 0){
	list_to_use = rotation_strings;
} else if (menupos == 1){
	list_to_use = animation_strings;
} else if (menupos == 2){
	list_to_use = particle_strings;
} else if (menupos == 3){
	list_to_use = crt_strings;	
} else if (menupos == 4 || menupos == 5){
	list_to_use = sfx_strings;	
}

tts_say(list_to_use[|curposs[menupos]] + " " + menu_name_array[menupos] + ": " + menu_desc_array[menupos]);
