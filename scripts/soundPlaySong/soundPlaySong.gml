/// @description - play a new song
/// @param song
/// @param time

with(MusicPlayer){
	if(current_audio != argument[0]){
		current_audio = argument[0]
		last_song = current_song;
		
		current_song = audio_play_sound(argument[0],1,true);
		
		if(last_song != -1){
			audio_sound_gain(current_song,0,0);
			fade_time = argument[1];
			fade_counter = fade_time;
		}
	}
}