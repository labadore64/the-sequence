var xxx = 0;
var yyy = 0;

with(Player){
	xxx = x;
	yyy = y + collision_y_adjust
}

var tester = argument[1];

var oriez = argument[0];
var xmulti = cos(degtorad(oriez));
var ymulti = sin(degtorad(oriez));

//var dist_to_test = collisionBoxTestLine(id,xxx,yyy,xxx+xmulti*tester,yyy+ymulti*tester);
return doesLineCrossBoundary(id,xxx,yyy,xxx+xmulti*tester,yyy+ymulti*tester);
/*
while(tester > 0 && dist_to_test){
	tester-= AX_COLLISION_TEST_SPACE
	dist_to_test = collisionBoxTestLine(id,xxx,yyy,xxx+xmulti*tester,yyy+ymulti*tester);
}

return tester;
*/