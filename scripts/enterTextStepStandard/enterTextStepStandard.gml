// do keypress stuff.
event_inherited();
	if(global.key_state_array[KEYBOARD_KEY_OPEN_MENU] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_OPEN_MENU] == KEY_STATE_PRESS){
		if(objGenericCanDo()){
			if(script_menu_open!= -1){
				script_execute(script_menu_open);
			}
		}
	}