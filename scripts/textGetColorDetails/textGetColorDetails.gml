var colo = argument[0]
var col_r = color_get_red(colo);
var col_g = color_get_green(colo);
var col_b = color_get_blue(colo);

returner = "Red: " + string(col_r) + " Green: " + string(col_g) + " Blue: " + string(col_b);

return returner;