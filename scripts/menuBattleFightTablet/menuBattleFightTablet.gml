
if((battleTabletIsUsable(character,0) ||
	battleTabletIsUsable(character,1) ||
	battleTabletIsUsable(character,2))
	&& character.mp.base*TABLET_MPCOST_1 < character.mp.current){
		
		var moves_instance = menuCreateInstance(menuBattleTablet)
	
		moves_instance.character = character;
	
		var tablet_moves = ds_list_create();

		var moves_list = tablet_moves;
		var tmpa;
		var brailletab;
		for(var i =0; i < 3; i++){
			if(character.tablet[i] < 0){
				// if theres no tablet set the move to none
				ds_list_add(moves_list,0);
			} else {
				// get the tablet move
				tmpa = instance_create(0,0,tmp);
				brailletab = character.tablet[i];
				with(tmp){
					loadTablet(brailletab);
				}
				// if the move is -1 set it to none
				// otherwise set it to the appropriate move
				if(tmp.move_id  < 0){
					ds_list_add(moves_list,0);
				} else {
					ds_list_add(moves_list,tmp.move_id);
				}
				with(tmp){
					instance_destroy();	
				}
			}
		}

		var moves_len = ds_list_size(moves_list);

		moves_instance.character = character;

		var obj = -1;

		for(var i = 0; i < moves_len; i++){
			obj = moves_list[|i]

			if(!is_undefined(obj)){
				moveData(obj);	
					ds_list_add(moves_instance.moves,obj);	
			}
		}
	
	ds_list_destroy(tablet_moves);
} else {
	if(character.mp.base*TABLET_MPCOST_1 >= character.mp.current){
		textboxBattleMain("invalid_lowmp")
	} else {
		textboxBattleMain("invalid_tabletunavail")
	}
}