if(active){
	if(global.key_state_array[KEYBOARD_KEY_CANCEL] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_CANCEL] == KEY_STATE_PRESS){
		//cancel	
		menupos = 0;
		state = 0;
		soundfxPlay(sound_cancel)
	} else if (global.key_state_array[KEYBOARD_KEY_LEFT] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_LEFT] == KEY_STATE_PRESS){
		menupos = 0;
		soundfxPlay(sound_move)
		tts_say(LANG_SELECT_YES)
	} else if (global.key_state_array[KEYBOARD_KEY_RIGHT] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_RIGHT] == KEY_STATE_PRESS){
		menupos = 1;
		soundfxPlay(sound_move)
		tts_say(LANG_SELECT_NO);
	} else if(global.key_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
		global.key_state_array[KEYBOARD_KEY_SELECT2] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS){
		if(menupos == 0){
			soundfxPlay(sound_move);
			instance_destroy();
		} else {
			menupos = 0;
			state = 0;
			soundfxPlay(sound_cancel)
		}
	}
}