var index = argument[1] 
var chara = argument[0]
var tablet_id = argument[2]

if(obj_data.tablet_obtained[tablet_id]){
	obj_data.tablet_character[tablet_id] = chara;
	with(chara){
		tablet[index] = tablet_id;
		newalarm[0] = 1;
	}
}