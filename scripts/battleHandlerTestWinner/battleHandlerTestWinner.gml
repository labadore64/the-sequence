with(BattleHandler){
	if(ds_exists(attack_input,ds_type_list)){
		ds_list_clear(attack_input)	
	
		var obj = -1;
		var sizer = ds_list_size(party_list);
		var test_list = ds_list_create();
		for(var i =  0; i < sizer; i++){
			obj = party_list[|i];
			if(!is_undefined(obj)){
				if(battlerIsAlive(obj)){
					ds_list_add(test_list,obj);
				}
			}
		}
	
		ds_list_copy(party_list,test_list);
		ds_list_clear(test_list);
		sizer = ds_list_size(enemy_list);
		var test_list = ds_list_create();
		for(var i =  0; i < sizer; i++){
			obj = enemy_list[|i];
			if(!is_undefined(obj)){
				if(battlerIsAlive(obj)){
					ds_list_add(test_list,obj);
				}
			}
		}
	
		ds_list_copy(enemy_list,test_list);
	
		ds_list_destroy(test_list);
	}
}

var returner = "nothing";
if(instance_exists(obj_data)){
	if(obj_data.battle_exists){
		if(ds_exists(BattleHandler.enemy_list,ds_type_list)){
			if(ds_list_empty(BattleHandler.enemy_list)){
				returner = "win";
			} else if (ds_list_empty(BattleHandler.party_list)) {
				returner = "lose";
			}
		}
		if(BattleHandler.fleeing){
			returner = "flee";
		}
	}
}
return returner;