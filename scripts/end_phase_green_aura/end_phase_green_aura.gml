var nope = argument[0];
var perc_damage = .125

battleTextDisplayClearText();

with(Battler){

	if(!is_enemy){
		if(battlerIsAlive(id)){
			if(hp.current/hp.base > .5){
				var damg = ceil(hp.base * perc_damage * .5);
			} else {
				var damg = ceil(hp.current * perc_damage);
			}
			var perc_damg = floor(100*damg/hp.base);
		
			var changed = hp.current + damg;
			battlerHPChange(id,changed)
		
			var stringer = "@0 gained @1% health from the healing winds!"
		
			stringer = string_replace_all(stringer,"@0",name);
			stringer = string_replace_all(stringer,"@1",string(perc_damg));
			battleTextDisplayAddText(stringer);
		}
	}
	
}

battleTextDisplayAddText(" ");