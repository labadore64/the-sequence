

var chara = argument[0];

if(ds_exists(chara.moves, ds_type_list)){
	var sizer = ds_list_size(chara.moves)-1;
	
	if(sizer > -1){
		var val = chara.moves[| irandom_range(0,sizer)];
		if(!is_undefined(val)){
			return val;	
		}
	}
}

return objdataToIndex(objdata_moves,"attack");
