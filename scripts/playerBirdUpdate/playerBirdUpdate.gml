var run = is_running;
var x_check = argument[0];
var y_check = argument[1];

var is_millet = bird_millet;
var is_suet = bird_suet;
var is_carrion = bird_carrion;

with(prop_bird_parent){
	var multiplier = 1.5;
	
	if(is_millet && millet){
		multiplier = .005;	
	}
	
	if(is_suet && suet){
		multiplier = .005;	
	}
	
	if(is_carrion && carrion){
		multiplier = .005	
	}
	
	
	var dist = point_distance(x_check,y_check,x,y)
	
	var multi = 1;
	
	if(run){
		multi = 1.5	
	}
	

	var randy = random(1);
	if(randy < multiplier){
		if(dist < fly_distance*multi){
			fly_away = true
			
			var dir = random_range(45,75)
		
			if(face_left){
				dir = random_range(135,135+30)
			}
		
			fly_direction = dir;
		
			sprite_index = sprite_fly;
		
			fly_move_x = cos(degtorad(dir)) * fly_speed;
			fly_move_y = sin(degtorad(dir)) * fly_speed;
		
			var myid = id;
			flaptimer_time = menuControl.timer_frame_count;
			
			with(sound_emitter){
				instance_destroy();	
			}

			sound_emitter = instance_create_depth(x,y,0,AudioEmitter);
			//sound_emitter.check_for_collision = true;
			sound_emitter.audio_sound = sound3D_bird_flap;
			sound_emitter.falloff_max = 600
			sound_emitter.falloff_min = 25
			sound_emitter.pitch_base = random_range(.85,1.15);
			
			with(obj_data){
				ds_list_add(bird_flown_away,myid);	
			}
		}
	}
}