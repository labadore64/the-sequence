
{
    "name": "CinderForestYourHouseRoom2",
    "id": "d0feca44-ef3c-4f53-942a-1be3ee535565",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "ccbc6031-53b5-456a-ae8b-06118864ccd3",
        "2de7999e-dcec-4da9-a259-5a02513155bc",
        "ecc84095-58ab-4205-8680-c96732a41045",
        "20bc93d1-c5c2-456b-a86f-b53225a93c22",
        "73446007-8829-416e-9577-0f360b1ad627",
        "bf4cae69-794d-46e9-ac2a-0fbbc9e14ee8",
        "976c50bc-478d-41c5-8651-6a36faddb44d",
        "c6d8da46-6e26-4993-95d8-51de960a136e",
        "ab110b3d-c55a-4cbf-9516-e80dd5d01aa4",
        "8d683af0-89e4-4682-9e55-d9ab95134cf4",
        "4003d032-3a0d-4e69-9269-0187484e0e5c",
        "ee5d698c-a3b8-4f96-9574-dbdc31deb926",
        "6e1be4a3-a480-4ee4-bae3-401050e096b5",
        "d86a7dac-af88-4fd4-8767-dee4d6d1a670",
        "64ab5686-8adf-4e06-bab8-9ff80e7a08a0",
        "5574ece7-a73c-46ca-9581-b4ccff825e83",
        "9d75b03f-d713-4e52-a2ae-744a29d79ac7",
        "7870af40-7a03-464a-8adc-f1bb82ec5065",
        "f99341c6-d69e-4e4e-b965-7846d4b7487d",
        "16e12a63-fe4a-4396-a358-932e74cd6545",
        "f59b0f0b-0f43-4528-abf2-939cd39f4c3e",
        "fca11dbe-661a-4242-9ce8-1a774f5e1ec1",
        "56702800-617f-4c65-a271-7c7f4d7d97aa",
        "652ab2dc-2a61-4e1a-a5eb-13890ac5746c",
        "caabf14f-08a8-414c-a442-d1e45fed60e7",
        "dd924adb-a7f8-4022-9ddc-f1bbace8f24f",
        "e5dc8799-d0e1-4d52-81ff-3098706e443d",
        "1578a00e-7575-46cf-a0db-e1bb91749a4a",
        "a3f09d0f-fdf2-4dc4-a068-39efea9c6318"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "scripts",
            "id": "6aeee279-b96f-4586-85ec-efc81f0f564f",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_71518CB9","id": "a3f09d0f-fdf2-4dc4-a068-39efea9c6318","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_71518CB9.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_71518CB9","objId": "bf4d59ef-a965-4611-9751-9fe9b07c96cb","properties": null,"rotation": 0,"scaleX": 4,"scaleY": 2.625,"mvc": "1.0","x": 207,"y": 107}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "ItemInteract",
            "id": "ea4a3b03-0ba2-4cde-99da-5f19a2fa3d1a",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7FC54493","id": "1578a00e-7575-46cf-a0db-e1bb91749a4a","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7FC54493.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7FC54493","objId": "3a4c24c5-8171-4ac7-b0f2-44dae58c9bc4","properties": null,"rotation": 0,"scaleX": 0.9374999,"scaleY": 1.46875,"mvc": "1.0","x": 140,"y": 27}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "TextInteracting",
            "id": "086816fe-5632-4931-ae98-9c1ea2ea3f1a",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7C248376","id": "16e12a63-fe4a-4396-a358-932e74cd6545","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7C248376.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7C248376","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 0.9843749,"scaleY": 1.203125,"mvc": "1.0","x": 241,"y": 193.5},
{"name": "inst_7415EF02","id": "f59b0f0b-0f43-4528-abf2-939cd39f4c3e","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7415EF02.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7415EF02","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.515625,"scaleY": 1.171875,"mvc": "1.0","x": 53,"y": 192.5},
{"name": "inst_311AA148","id": "fca11dbe-661a-4242-9ce8-1a774f5e1ec1","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_311AA148.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_311AA148","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.046875,"scaleY": 1.484375,"mvc": "1.0","x": -15,"y": 120},
{"name": "inst_3CE758A8","id": "56702800-617f-4c65-a271-7c7f4d7d97aa","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3CE758A8.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3CE758A8","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.703125,"scaleY": 1.1875,"mvc": "1.0","x": 155,"y": 193},
{"name": "inst_38C3AEDF","id": "652ab2dc-2a61-4e1a-a5eb-13890ac5746c","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_38C3AEDF.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_38C3AEDF","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.03125,"scaleY": 1.828125,"mvc": "1.0","x": 291.5,"y": 99},
{"name": "inst_43056BF8","id": "caabf14f-08a8-414c-a442-d1e45fed60e7","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_43056BF8.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_43056BF8","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 0.8281249,"scaleY": 0.8281251,"mvc": "1.0","x": 35,"y": 46},
{"name": "inst_355E4F1D","id": "dd924adb-a7f8-4022-9ddc-f1bbace8f24f","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_355E4F1D.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_355E4F1D","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 0.8437501,"scaleY": 0.5625,"mvc": "1.0","x": 82,"y": 90},
{"name": "inst_3B1AD3B0","id": "e5dc8799-d0e1-4d52-81ff-3098706e443d","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3B1AD3B0.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3B1AD3B0","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 2.015625,"scaleY": 0.734375,"mvc": "1.0","x": 275.5,"y": 12.5}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "AXLocator",
            "id": "fc411a3d-eb23-4563-bf84-1f0d94f10e5b",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_2ECB88AF","id": "7870af40-7a03-464a-8adc-f1bb82ec5065","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_2ECB88AF.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2ECB88AF","objId": "38e9391e-d1e3-4240-b1c1-e3d9880b4800","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 262,"y": 8}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Footstep",
            "id": "11b9d6fc-9949-4c6b-aaec-ae0f898c5c1f",
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_75DD7103","id": "9d75b03f-d713-4e52-a2ae-744a29d79ac7","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_75DD7103.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_75DD7103","objId": "bf4d59ef-a965-4611-9751-9fe9b07c96cb","properties": null,"rotation": 0,"scaleX": 5.875,"scaleY": 4.3125,"mvc": "1.0","x": 169,"y": 82}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Sound3D",
            "id": "23fad3b3-38b9-4c03-b9fc-a3d64fe63eac",
            "depth": 500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_47E01385","id": "5574ece7-a73c-46ca-9581-b4ccff825e83","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_47E01385.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_47E01385","objId": "b38106b3-a405-4a53-8874-b5845310d549","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 43,"y": 44}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "AXDescribe",
            "id": "cb3ed76c-abb8-4758-9747-0b6276ea5f37",
            "depth": 600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_F4B10A0","id": "ee5d698c-a3b8-4f96-9574-dbdc31deb926","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_F4B10A0.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_F4B10A0","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1.90625,"mvc": "1.0","x": 280,"y": 107},
{"name": "inst_35798BEA","id": "6e1be4a3-a480-4ee4-bae3-401050e096b5","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_35798BEA.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_35798BEA","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 0.78125,"scaleY": 1,"mvc": "1.0","x": 35,"y": 51},
{"name": "inst_E61AF07","id": "d86a7dac-af88-4fd4-8767-dee4d6d1a670","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_E61AF07.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_E61AF07","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 0.9375,"scaleY": 0.6875,"mvc": "1.0","x": 82,"y": 98},
{"name": "inst_45D866C7","id": "64ab5686-8adf-4e06-bab8-9ff80e7a08a0","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_45D866C7.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_45D866C7","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 0.90625,"scaleY": 0.828125,"mvc": "1.0","x": 140,"y": 53},
{"name": "inst_775FD255","id": "f99341c6-d69e-4e4e-b965-7846d4b7487d","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_775FD255.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_775FD255","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 1.703125,"scaleY": 0.71875,"mvc": "1.0","x": 159,"y": 185}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": false
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Collision",
            "id": "66a5e545-5962-4a88-915f-7a218502ef33",
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_400B0AE4","id": "73446007-8829-416e-9577-0f360b1ad627","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_400B0AE4","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1.15625,"scaleY": 2.4375,"mvc": "1.0","x": 298,"y": 107},
{"name": "inst_81B9656","id": "bf4cae69-794d-46e9-ac2a-0fbbc9e14ee8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_81B9656","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 7.625,"scaleY": 1.125,"mvc": "1.0","x": 245,"y": 196},
{"name": "inst_7D70D290","id": "976c50bc-478d-41c5-8651-6a36faddb44d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7D70D290","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1.078125,"scaleY": 2.640625,"mvc": "1.0","x": -20.5,"y": 115},
{"name": "inst_23FCBEC8","id": "c6d8da46-6e26-4993-95d8-51de960a136e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_23FCBEC8","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 3.140625,"scaleY": 1,"mvc": "1.0","x": 85.49999,"y": 36},
{"name": "inst_3DDF92AB","id": "ab110b3d-c55a-4cbf-9516-e80dd5d01aa4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3DDF92AB","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1.09375,"scaleY": 1,"mvc": "1.0","x": 195,"y": 7},
{"name": "inst_7ED44F54","id": "8d683af0-89e4-4682-9e55-d9ab95134cf4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7ED44F54","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 2.6875,"scaleY": 1,"mvc": "1.0","x": 249,"y": -24},
{"name": "inst_6763E3EE","id": "4003d032-3a0d-4e69-9269-0187484e0e5c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6763E3EE","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 0.703125,"scaleY": 1,"mvc": "1.0","x": 82,"y": 71}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Warps",
            "id": "8be32f1b-b1d4-4f32-b4fb-5d61d618ad50",
            "depth": 800,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_580740A6","id": "20bc93d1-c5c2-456b-a86f-b53225a93c22","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_580740A6.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_580740A6","objId": "a0935c63-5359-467e-81ff-8734cb9f1732","properties": null,"rotation": 0,"scaleX": 0.59375,"scaleY": 1,"mvc": "1.0","x": 302,"y": 3}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Props",
            "id": "ad84e6a5-b197-4786-a333-862d93dbb096",
            "depth": 900,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "cinderForestYourRoom2_propDeskDresser","id": "ccbc6031-53b5-456a-ae8b-06118864ccd3","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_cinderForestYourRoom2_propDeskDresser.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "cinderForestYourRoom2_propDeskDresser","objId": "4ea4fa8a-3fed-4b9b-8524-c48db09e0d85","properties": null,"rotation": -0.04845664,"scaleX": 1.343751,"scaleY": 1.265625,"mvc": "1.0","x": 86,"y": 1},
{"name": "cinderForestYourRoom2_propChair","id": "2de7999e-dcec-4da9-a259-5a02513155bc","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_cinderForestYourRoom2_propChair.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "cinderForestYourRoom2_propChair","objId": "bd97c2c7-6ab9-4bb0-9363-5d01fb6de8bc","properties": null,"rotation": 0,"scaleX": 0.6640625,"scaleY": 0.671875,"mvc": "1.0","x": 82.5,"y": 70},
{"name": "cinderForestYourRoom2_propBed","id": "ecc84095-58ab-4205-8680-c96732a41045","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_cinderForestYourRoom2_propBed.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "cinderForestYourRoom2_propBed","objId": "566984b6-0318-4688-af1a-0d98a9debb20","properties": null,"rotation": 0,"scaleX": 0.671875,"scaleY": 0.875,"mvc": "1.0","x": 299,"y": 103}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "e4bb831d-90a4-40d0-9898-f07beadee066",
            "depth": 1000,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 64,
            "prev_tilewidth": 64,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 7,
                "SerialiseWidth": 11,
                "TileSerialiseData": [
                    94,94,94,12,12,76,2147483648,2147483648,2147483648,2147483648,2147483648,
                    12,12,12,12,12,76,2147483648,2147483648,2147483648,2147483648,2147483648,
                    12,12,12,12,12,76,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                    2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648
                ]
            },
            "tilesetId": "72e8566a-f543-490e-9983-d972256ae6ad",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "6449628d-6dc8-467c-8814-a463ba9463f1",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "7d6b9695-4921-45e0-b2d2-4f0825801dd6",
        "Height": 200,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 350
    },
    "mvc": "1.0",
    "views": [
{"id": "fe7ca974-5cc2-4857-8c73-5da074a85937","hborder": 32,"hport": 768,"hspeed": -1,"hview": 300,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1024,"wview": 400,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a5278efb-04fc-45b7-8a05-889d790b5299","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "87f6535b-c744-45a8-91c9-3ea51da07360","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0adbc8c6-b61c-4d3e-9509-eae752e10bfa","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "86f97f03-5a9f-4c4b-85d4-56be049c0ab5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "33eee4db-593b-49b2-8fc1-86e1e6c10d09","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6297dad6-07f4-435a-a4c4-c77b12d06f51","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b260c281-93e4-4773-86c1-c24ed4074648","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "3afbfef4-880c-43c9-825a-4000d29ad4f3",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}