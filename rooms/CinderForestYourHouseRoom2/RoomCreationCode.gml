init_first_room();

mapSetHasMonsters(false)

defeatRoomSet(room,PLAYER_START_X,PLAYER_START_Y);

soundPlaySong(music005,30)

cameraZoomSet(.4)

setMapRoom(CinderForestOutside1)

with(Player){
	active = true;
	visible = true;
}

taskAssign("taskStart");
emailReceive("msg1");

global.signal_strength = 3;