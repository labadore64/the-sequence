//item data
item_text_id = "CinderLake_Map4_BirdFeederItem"; //id for item dialog
item_event_id = "cl4_millet2"; //event id for item being obtained
item_id = objdataToIndex(objdata_items,"millet");//-1; //item to receive
item_quantity = 5; //number of item to receive

//after item has been obtained
text_box_id = "CinderLake_Map4_BirdFeeder"; //textbox to look up