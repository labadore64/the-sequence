//item data
item_text_id = "CinderLake_MapWildlife_BookshelfItem"; //id for item dialog
item_event_id = "clw_bird"; //event id for item being obtained
item_id = objdataToIndex(objdata_items,"bird book");//-1; //item to receive
item_quantity = 1; //number of item to receive

//after item has been obtained
text_box_id = "CinderLake_MapWildlife_Bookshelf"; //textbox to look up