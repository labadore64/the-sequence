//item data
item_text_id = "CinderLake_MapWildlife_TableItem"; //id for item dialog
item_event_id = "clw_paper"; //event id for item being obtained
item_id = objdataToIndex(objdata_items,"paper");//-1; //item to receive
item_quantity = 4; //number of item to receive

//after item has been obtained
text_box_id = "CinderLake_MapWildlife_Table"; //textbox to look up