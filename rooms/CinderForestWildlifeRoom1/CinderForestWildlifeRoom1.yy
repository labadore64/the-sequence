
{
    "name": "CinderForestWildlifeRoom1",
    "id": "81c67364-8f0e-49a2-bd51-f283f11846f1",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "83994059-99d4-466b-945a-465a18b85ce3",
        "ff1f68b3-13fc-4e83-9741-5e1cb878651e",
        "f80f9865-4ff4-4909-98d5-27bac7333539",
        "ba34d6d5-d52a-4948-86f3-8c05fd303328",
        "2f8d7a79-8061-40b5-8fb5-e2415d70d1e2",
        "0bc3b55c-c3e2-49b8-8a97-7af0cec08d91",
        "5e64c3ac-7365-425d-8df7-d250d5664a74",
        "fede5e2c-65f2-4c53-80e5-25217800d96f",
        "ca08ccae-9232-409c-b995-2c9a41bf260b",
        "48730bbf-b35d-44c2-9d25-7cb5ca3a231c",
        "0d1c8583-f8e5-403e-b777-c5f0d5264e44",
        "1c499e02-74ef-4a09-8ec8-51cf0888ec89",
        "d0d32c1a-7798-4731-a0b2-4cc35ae513e6",
        "cd52bcdf-0d79-47b1-a397-9f948fa46824",
        "013ee37b-8d57-411d-9599-44fdb16dc8e0",
        "906ebef7-fa1a-4ce0-a74f-c0a1a946fd99",
        "bae433d7-4b29-4eb2-8f8c-0f1500e19567",
        "5a461824-f42c-4da9-883f-a4691f7ec765",
        "f35b7862-89b1-4973-b6ac-06823a964518",
        "b7f70ad9-3143-404e-9259-4b9721bad559",
        "02a5a540-4522-47fe-a793-cb49ba3fbdc1",
        "6b5d55cc-8b0e-4a20-9992-74e15361ee4a",
        "67179d65-c5f2-4b64-9715-918dcfc171b8",
        "a0890bb5-69ca-40cf-86b0-bf8cbf831277",
        "4dca6a74-ff28-46ca-a2ab-2ce431aa6c96",
        "13fbc464-afa7-46cb-b7da-97f91e782fba"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "scriptInteract",
            "id": "6ab2b620-e62a-4b20-8e2f-c9a08447d6a3",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7A7FAF50","id": "f35b7862-89b1-4973-b6ac-06823a964518","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7A7FAF50.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7A7FAF50","objId": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3","properties": null,"rotation": 0,"scaleX": 1.828125,"scaleY": 1.28125,"mvc": "1.0","x": 198,"y": 23}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "ItemInteract",
            "id": "d4d37512-19f5-4bcc-80c8-a4fee8d3e904",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_44A4DDEC","id": "a0890bb5-69ca-40cf-86b0-bf8cbf831277","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_44A4DDEC.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_44A4DDEC","objId": "3a4c24c5-8171-4ac7-b0f2-44dae58c9bc4","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1.1875,"mvc": "1.0","x": 108,"y": 25},
{"name": "inst_548E2D9","id": "4dca6a74-ff28-46ca-a2ab-2ce431aa6c96","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_548E2D9.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_548E2D9","objId": "3a4c24c5-8171-4ac7-b0f2-44dae58c9bc4","properties": null,"rotation": 0,"scaleX": 1.359375,"scaleY": 1.703125,"mvc": "1.0","x": 129.5,"y": 164.5}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "TextInteract",
            "id": "9269cee0-1a9b-4790-8d7c-196afec23ab8",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_33BB07AB","id": "b7f70ad9-3143-404e-9259-4b9721bad559","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_33BB07AB.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_33BB07AB","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 2.90625,"scaleY": 2.421875,"mvc": "1.0","x": 350,"y": 33.5},
{"name": "inst_21D70D33","id": "02a5a540-4522-47fe-a793-cb49ba3fbdc1","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_21D70D33.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_21D70D33","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.265625,"scaleY": 1.1875,"mvc": "1.0","x": 402,"y": 281},
{"name": "inst_191B8A10","id": "6b5d55cc-8b0e-4a20-9992-74e15361ee4a","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_191B8A10.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_191B8A10","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.625,"scaleY": 1.640625,"mvc": "1.0","x": 224.5,"y": 163.5},
{"name": "inst_674262C1","id": "67179d65-c5f2-4b64-9715-918dcfc171b8","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_674262C1.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_674262C1","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.203125,"scaleY": 1.640625,"mvc": "1.0","x": 313.5,"y": 164},
{"name": "inst_284467F2","id": "13fbc464-afa7-46cb-b7da-97f91e782fba","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_284467F2.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_284467F2","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.09375,"scaleY": 1.203125,"mvc": "1.0","x": 40,"y": 25}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "AXLocator",
            "id": "4056cf71-cce4-49db-a430-7289ced4941f",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_35CAFCE8","id": "5e64c3ac-7365-425d-8df7-d250d5664a74","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_35CAFCE8.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_35CAFCE8","objId": "38e9391e-d1e3-4240-b1c1-e3d9880b4800","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 299,"y": 295}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Footstep",
            "id": "ba2ad028-0201-4a76-8288-c203d94bb390",
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_F285DD2","id": "fede5e2c-65f2-4c53-80e5-25217800d96f","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_F285DD2.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_F285DD2","objId": "bf4d59ef-a965-4611-9751-9fe9b07c96cb","properties": null,"rotation": 0,"scaleX": 7.250005,"scaleY": 4.765625,"mvc": "1.0","x": 210,"y": 188}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Sound3D",
            "id": "fc52ebb3-fa6b-4f0f-8a3e-6aa8f65c2b79",
            "depth": 500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "AXDescribe",
            "id": "06f66973-54b0-4472-9318-25be594ce43b",
            "depth": 600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "collision",
            "id": "412cce28-3cd1-47d9-a292-233a4d273923",
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_57E48E2E","id": "83994059-99d4-466b-945a-465a18b85ce3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_57E48E2E","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 2.109375,"scaleY": 1,"mvc": "1.0","x": 419.5,"y": 319},
{"name": "inst_1F079334","id": "ff1f68b3-13fc-4e83-9741-5e1cb878651e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1F079334","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 5.046875,"scaleY": 1,"mvc": "1.0","x": 102,"y": 320},
{"name": "inst_38BDE782","id": "f80f9865-4ff4-4909-98d5-27bac7333539","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_38BDE782","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 4.75,"mvc": "1.0","x": 441,"y": 154},
{"name": "inst_4646BA77","id": "ba34d6d5-d52a-4948-86f3-8c05fd303328","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4646BA77","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 4.75,"mvc": "1.0","x": -16,"y": 152},
{"name": "inst_55224277","id": "2f8d7a79-8061-40b5-8fb5-e2415d70d1e2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_55224277","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 8.484375,"scaleY": 1.859375,"mvc": "1.0","x": 200,"y": -14},
{"name": "inst_8F4AE6B","id": "013ee37b-8d57-411d-9599-44fdb16dc8e0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_8F4AE6B","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 2.9375,"scaleY": 1,"mvc": "1.0","x": 351,"y": 63},
{"name": "inst_710AC972","id": "906ebef7-fa1a-4ce0-a74f-c0a1a946fd99","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_710AC972","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 3.921875,"scaleY": 1.28125,"mvc": "1.0","x": 215.5,"y": 164},
{"name": "inst_18B9C463","id": "5a461824-f42c-4da9-883f-a4691f7ec765","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_18B9C463","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 407,"y": 290}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "warps",
            "id": "92eff583-4807-4448-8e4b-7dcb81bcc409",
            "depth": 800,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7618F7CF","id": "0bc3b55c-c3e2-49b8-8a97-7af0cec08d91","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7618F7CF.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7618F7CF","objId": "a0935c63-5359-467e-81ff-8734cb9f1732","properties": null,"rotation": 0,"scaleX": 1.90625,"scaleY": 0.46875,"mvc": "1.0","x": 309,"y": 316}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Props",
            "id": "43a8cd4e-e15b-431d-bb3c-c852c6905d4b",
            "depth": 900,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_1EC458E0","id": "ca08ccae-9232-409c-b995-2c9a41bf260b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1EC458E0","objId": "18db916d-f6a1-477d-983a-93e8568923d9","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 200,"y": -27},
{"name": "inst_7BF24A3E","id": "48730bbf-b35d-44c2-9d25-7cb5ca3a231c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7BF24A3E","objId": "f3a28abb-bfec-4164-b9b6-0ccf0c21c917","properties": null,"rotation": 0,"scaleX": 1.148438,"scaleY": 1.125,"mvc": "1.0","x": 344,"y": -21},
{"name": "inst_3BA0AF26","id": "0d1c8583-f8e5-403e-b777-c5f0d5264e44","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3BA0AF26.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3BA0AF26","objId": "f44d0bcf-8446-4673-b58d-9f6ab65e9b12","properties": null,"rotation": 0,"scaleX": 1.90625,"scaleY": 1.632813,"mvc": "1.0","x": 212,"y": 179},
{"name": "inst_6BAC726D","id": "1c499e02-74ef-4a09-8ec8-51cf0888ec89","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6BAC726D","objId": "47368022-8732-4ba7-85dd-f2b0267b2b1d","properties": null,"rotation": 0,"scaleX": 1.554687,"scaleY": 0.703125,"mvc": "1.0","x": 344,"y": 52},
{"name": "inst_5DC57A48","id": "d0d32c1a-7798-4731-a0b2-4cc35ae513e6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5DC57A48","objId": "a25a8eaf-c31e-452f-bff6-41c410f725d0","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 47,"y": -7},
{"name": "inst_1F16A2F5","id": "cd52bcdf-0d79-47b1-a397-9f948fa46824","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1F16A2F5","objId": "a25a8eaf-c31e-452f-bff6-41c410f725d0","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 106,"y": -7},
{"name": "inst_35CD7C04","id": "bae433d7-4b29-4eb2-8f8c-0f1500e19567","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_35CD7C04","objId": "39a39c46-e07a-472a-92b6-8db700a1e91f","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 378,"y": 248}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "f9134d68-9bbf-48f0-87d1-f6d86f2e24fa",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 1000,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "33cf858b-c16d-4254-a149-798db8b6a803",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "ae38566a-23df-41e3-bbbe-886e994d880e",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "e00920d7-5847-410e-b0df-d99a45cf216a",
        "Height": 400,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 500
    },
    "mvc": "1.0",
    "views": [
{"id": "d2c777c7-5b84-4f89-885a-11060a11f092","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "27fcb83f-184f-407a-a60c-bbe7263e672e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5a714184-f7a2-402a-ba39-32e2a6e5799d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f3f2786c-968f-42e3-91a2-245a496bb79a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e9e57b53-8fe7-4efb-8515-e1edbbc94e11","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0b23baf3-4f25-4979-984b-8f8e46b65ca3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5f7c3a92-5d31-4292-90bf-132836121d61","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "93a05234-d2de-4e07-8840-ce11cef05d53","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "a6989bfe-7652-435e-bd1a-b34e1a1b2417",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}