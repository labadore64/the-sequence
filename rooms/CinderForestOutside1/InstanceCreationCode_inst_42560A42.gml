//item data
item_text_id = "CinderLake_Map0_Boulder3Item"; //id for item dialog
item_event_id = "cl1_boulder"; //event id for item being obtained
item_id = objdataToIndex(objdata_items,"stick");//-1; //item to receive
item_quantity = 2; //number of item to receive

//after item has been obtained
text_box_id = "CinderLake_Map0_Boulder3"; //textbox to look up

