
{
    "name": "map_room",
    "id": "76a3540e-94ed-4d24-bd84-043b2fe54f79",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "5514f834-d7e3-4494-afa4-5b4134d686f3",
        "fcba069b-a6a1-4cc6-b65f-e001b1eab02e",
        "53def5fd-7034-4180-a31a-af7eb45547e0",
        "56c455c9-8ab2-4990-95e3-c6d293ea9413",
        "36517dc6-e43c-4d45-b32f-dabe2adb48c4"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "points",
            "id": "e8ba3432-9a39-44a4-981d-a047db1ba2c5",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_6999AD02","id": "fcba069b-a6a1-4cc6-b65f-e001b1eab02e","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_6999AD02.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6999AD02","objId": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 296,"y": 172},
{"name": "inst_10DA3229","id": "53def5fd-7034-4180-a31a-af7eb45547e0","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_10DA3229.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_10DA3229","objId": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1","properties": null,"rotation": 0,"scaleX": 0.625,"scaleY": 0.71875,"mvc": "1.0","x": 322,"y": 182},
{"name": "inst_5AACD7CC","id": "56c455c9-8ab2-4990-95e3-c6d293ea9413","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_5AACD7CC.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5AACD7CC","objId": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1","properties": null,"rotation": 0,"scaleX": 0.625,"scaleY": 0.71875,"mvc": "1.0","x": 342,"y": 182},
{"name": "inst_679B7F7A","id": "36517dc6-e43c-4d45-b32f-dabe2adb48c4","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_679B7F7A.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_679B7F7A","objId": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1","properties": null,"rotation": 0,"scaleX": 0.625,"scaleY": 0.71875,"mvc": "1.0","x": 362,"y": 182}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "72fa0754-50f3-4543-966d-3331688f654c",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_B770FFA","id": "5514f834-d7e3-4494-afa4-5b4134d686f3","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_B770FFA.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_B770FFA","objId": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 416,"y": 320}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "17298d50-12d7-4627-8b87-76ce334669cb",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "5a808f22-d78b-432b-a31c-81cfde4e71b9",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "4cf87d51-e856-413a-ba19-e48c700cd755",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "e158da99-a168-4a14-8950-1342fe090d2f",
        "Height": 600,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 800
    },
    "mvc": "1.0",
    "views": [
{"id": "d17b446b-8c5e-4bf6-8c87-59aee00f58c9","hborder": 200,"hport": 600,"hspeed": -1,"hview": 300,"inherit": false,"modelName": "GMRView","objId": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121","mvc": "1.0","vborder": 150,"visible": true,"vspeed": -1,"wport": 800,"wview": 400,"xport": 0,"xview": 200,"yport": 0,"yview": 150},
{"id": "702c87c8-e63d-43b7-bfa9-1c1ac52875d3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "01008526-3a63-444f-9bb6-9583de760b25","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fc40f58a-85ab-475b-96b4-e906d8e48d6f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d3c4b088-26d4-4fc4-b568-81b6ba75d918","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "310d55d6-eef3-44e9-8e19-9027dee6a1ff","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "7015e06f-be1d-4a42-a6d4-707f2ce6f15e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "dcc044aa-acaa-4f0c-a269-4cfef92a30ea","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "a50f1182-1a7d-4f4d-a2fb-155549f4ef5a",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}