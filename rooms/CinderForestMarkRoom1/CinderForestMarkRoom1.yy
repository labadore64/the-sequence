
{
    "name": "CinderForestMarkRoom1",
    "id": "45f6c644-6fb3-480c-834f-2e6995113f09",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "7e25da03-6333-4b50-8b6b-b8a9d1296cbe",
        "bcfefe69-9c85-40d9-b7a3-be9a5ed2ec51",
        "a02b94fd-28e6-4da9-9029-3d6e4ffdfe62",
        "51fd84bb-2da1-4b18-a5d9-286db227e2ad",
        "26b2026f-5cdc-4577-b215-eb1690957820",
        "b509559d-34be-478c-ab47-8ced42c590e1",
        "14fe4859-46d4-4eb2-a9c9-4cdc6bddf6a0",
        "8092b3f5-9957-448b-94ac-68be9690c767",
        "8cfae6a5-1c99-4dcd-a1bc-1dc9e8d30129",
        "910385cb-e60f-478a-80ed-ae64c525617f",
        "ea94d1de-4c1b-4760-bffb-4da7fdb24937",
        "c0ff6a67-5462-4d23-b999-ec9fa58b7857",
        "1f712114-66b1-4d30-90a2-0216df354a6c",
        "89f70350-b951-4ce0-abf5-33ce8bf298f7",
        "20e26e1d-81a9-4607-ba21-4f1eb326de4e",
        "81c60ffb-66d1-46e7-a1d0-54a6467815e6",
        "93d4d6c1-37f8-4e8b-a02c-3acb5f3bee4e",
        "797e7458-0bb2-41f7-998a-43cabe017248",
        "50e486e9-86ab-44cf-9f13-60094226f6a4",
        "3dd68d10-6962-4f1a-8a44-d9ef82834e97",
        "7c3a3a65-4f78-4c71-ab97-c3c6cf166d97",
        "4d2e8824-047f-4a23-8aa6-5923e7caac4a",
        "d0777745-b992-4c90-b9b3-b8255f1029ac",
        "469f6e7a-fe28-4ccc-a756-003151435ce2",
        "7160bcb5-bb2f-4d01-93db-64cd9d64f53c",
        "2eaa4ab4-a946-4f22-a5c2-98222c6cba40",
        "42c9d629-da44-414a-86ae-34e9dc120fab",
        "71362f19-9a4d-4442-8a68-9610b3cea164",
        "60acaa11-0cfb-44ad-8449-9eea2d52f044"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "ItemInteract",
            "id": "dad8dae3-ea33-40d8-8d72-b8b027e1e6d9",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "TextInteract",
            "id": "35d95de6-8a63-4a05-8aeb-d356d935dad9",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_6DD79C09","id": "7160bcb5-bb2f-4d01-93db-64cd9d64f53c","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_6DD79C09.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6DD79C09","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.28125,"scaleY": 1.28125,"mvc": "1.0","x": 316.5,"y": 50},
{"name": "inst_1E813C65","id": "2eaa4ab4-a946-4f22-a5c2-98222c6cba40","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_1E813C65.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1E813C65","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 0.734375,"scaleY": 0.8125,"mvc": "1.0","x": 384,"y": 92},
{"name": "inst_424B23DA","id": "42c9d629-da44-414a-86ae-34e9dc120fab","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_424B23DA.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_424B23DA","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 4.390625,"scaleY": 0.734375,"mvc": "1.0","x": 113,"y": 63.5},
{"name": "inst_663A6C82","id": "71362f19-9a4d-4442-8a68-9610b3cea164","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_663A6C82.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_663A6C82","objId": "382b61d9-b7ef-464b-9788-5f1bea9842a7","properties": null,"rotation": 0,"scaleX": 1.6875,"scaleY": 1.359375,"mvc": "1.0","x": 136.5,"y": 176}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "AXLocator",
            "id": "984ce9dc-089c-47a9-9879-204c57722ab8",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_341EE20B","id": "14fe4859-46d4-4eb2-a9c9-4cdc6bddf6a0","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_341EE20B.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_341EE20B","objId": "38e9391e-d1e3-4240-b1c1-e3d9880b4800","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 299,"y": 295}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Footstep",
            "id": "4ce8beec-e017-4556-93c2-4c52bf9cc304",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_3EADF6F","id": "60acaa11-0cfb-44ad-8449-9eea2d52f044","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3EADF6F.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3EADF6F","objId": "bf4d59ef-a965-4611-9751-9fe9b07c96cb","properties": null,"rotation": 0,"scaleX": 7.250005,"scaleY": 4.765625,"mvc": "1.0","x": 210,"y": 188}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Sound3D",
            "id": "a2a3423b-ec22-4fa3-8c61-f849961cb3d8",
            "depth": 400,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_12C6D752","id": "469f6e7a-fe28-4ccc-a756-003151435ce2","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_12C6D752.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_12C6D752","objId": "b38106b3-a405-4a53-8874-b5845310d549","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 299,"y": 58}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "AXDescribe",
            "id": "06b97474-042e-4a0c-8548-133fdaf6f1ca",
            "depth": 500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_21346DA0","id": "50e486e9-86ab-44cf-9f13-60094226f6a4","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_21346DA0.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_21346DA0","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 2.25,"scaleY": 1.65625,"mvc": "1.0","x": 140.5,"y": 176},
{"name": "inst_669F363D","id": "3dd68d10-6962-4f1a-8a44-d9ef82834e97","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_669F363D.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_669F363D","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 4.03125,"scaleY": 1.53125,"mvc": "1.0","x": 126.5,"y": 53},
{"name": "inst_69A7ADF2","id": "7c3a3a65-4f78-4c71-ab97-c3c6cf166d97","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_69A7ADF2.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_69A7ADF2","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 2.328125,"scaleY": 1.53125,"mvc": "1.0","x": 352,"y": 55},
{"name": "inst_3660F600","id": "4d2e8824-047f-4a23-8aa6-5923e7caac4a","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3660F600.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3660F600","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 1.390625,"scaleY": 0.734375,"mvc": "1.0","x": 394,"y": 105},
{"name": "inst_3D9C400F","id": "d0777745-b992-4c90-b9b3-b8255f1029ac","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3D9C400F.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3D9C400F","objId": "059cb24d-9904-4e51-bf42-963f99956265","properties": null,"rotation": 0,"scaleX": 1.390625,"scaleY": 1.203125,"mvc": "1.0","x": 210,"y": 188}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "collision",
            "id": "bbe0d6fc-9b5b-4abc-a3ce-52fb276369cd",
            "depth": 600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_373B97F5","id": "7e25da03-6333-4b50-8b6b-b8a9d1296cbe","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_373B97F5","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 2.109375,"scaleY": 1,"mvc": "1.0","x": 419.5,"y": 319},
{"name": "inst_2CBD10FD","id": "bcfefe69-9c85-40d9-b7a3-be9a5ed2ec51","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2CBD10FD","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 5.046875,"scaleY": 1,"mvc": "1.0","x": 102,"y": 320},
{"name": "inst_32E0CC4B","id": "a02b94fd-28e6-4da9-9029-3d6e4ffdfe62","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_32E0CC4B","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 4.75,"mvc": "1.0","x": 441,"y": 154},
{"name": "inst_75CF5964","id": "51fd84bb-2da1-4b18-a5d9-286db227e2ad","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_75CF5964","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 4.75,"mvc": "1.0","x": -16,"y": 152},
{"name": "inst_3997F8DD","id": "26b2026f-5cdc-4577-b215-eb1690957820","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3997F8DD","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 8.484375,"scaleY": 1.859375,"mvc": "1.0","x": 218.5,"y": 30.5},
{"name": "inst_5A6C3E21","id": "81c60ffb-66d1-46e7-a1d0-54a6467815e6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5A6C3E21","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1.84375,"scaleY": 1.34375,"mvc": "1.0","x": 141.5,"y": 175.5},
{"name": "inst_42F81919","id": "93d4d6c1-37f8-4e8b-a02c-3acb5f3bee4e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_42F81919","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 0.8906416,"mvc": "1.0","x": 213,"y": 190},
{"name": "inst_115E4FBB","id": "797e7458-0bb2-41f7-998a-43cabe017248","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_115E4FBB","objId": "e63340af-28f1-495f-9657-b46bb416151a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 392,"y": 83}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "warps",
            "id": "96efd1c4-485e-4c5c-9c54-4edc4cf7e00d",
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_721315B","id": "b509559d-34be-478c-ab47-8ced42c590e1","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_721315B.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_721315B","objId": "a0935c63-5359-467e-81ff-8734cb9f1732","properties": null,"rotation": 0,"scaleX": 1.90625,"scaleY": 0.46875,"mvc": "1.0","x": 309,"y": 316}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Props",
            "id": "bbb2372f-8672-4fe4-a79f-ba499434132c",
            "depth": 800,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_3FDF676E","id": "8092b3f5-9957-448b-94ac-68be9690c767","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3FDF676E","objId": "a25a8eaf-c31e-452f-bff6-41c410f725d0","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1.09375,"mvc": "1.0","x": 49,"y": 28},
{"name": "inst_52EBE270","id": "8cfae6a5-1c99-4dcd-a1bc-1dc9e8d30129","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_52EBE270","objId": "a25a8eaf-c31e-452f-bff6-41c410f725d0","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1.09375,"mvc": "1.0","x": 106,"y": 28},
{"name": "inst_20107668","id": "910385cb-e60f-478a-80ed-ae64c525617f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_20107668","objId": "a25a8eaf-c31e-452f-bff6-41c410f725d0","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1.09375,"mvc": "1.0","x": 165,"y": 28},
{"name": "inst_1D1D981","id": "ea94d1de-4c1b-4760-bffb-4da7fdb24937","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1D1D981","objId": "a25a8eaf-c31e-452f-bff6-41c410f725d0","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1.09375,"mvc": "1.0","x": 224,"y": 28},
{"name": "inst_1F69B75F","id": "c0ff6a67-5462-4d23-b999-ec9fa58b7857","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1F69B75F","objId": "2dff5bdd-5a54-40a0-9d25-538c872b61af","properties": null,"rotation": 0,"scaleX": 1.460938,"scaleY": 1.359375,"mvc": "1.0","x": 339,"y": 11},
{"name": "inst_32C5999C","id": "1f712114-66b1-4d30-90a2-0216df354a6c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_32C5999C","objId": "47368022-8732-4ba7-85dd-f2b0267b2b1d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 139,"y": 166},
{"name": "inst_3D282599","id": "89f70350-b951-4ce0-abf5-33ce8bf298f7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3D282599","objId": "4210a2c7-33b2-4ba3-a878-ca6f12630ed1","properties": null,"rotation": 0,"scaleX": 1.015625,"scaleY": 1,"mvc": "1.0","x": 208,"y": 168},
{"name": "inst_3DF6C57F","id": "20e26e1d-81a9-4607-ba21-4f1eb326de4e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3DF6C57F","objId": "bd97c2c7-6ab9-4bb0-9363-5d01fb6de8bc","properties": null,"rotation": 0,"scaleX": 0.8515625,"scaleY": 0.8671875,"mvc": "1.0","x": 385,"y": 73}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "0a6ecfb9-99e8-4dd4-ab0a-e074a57c1017",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 900,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "33cf858b-c16d-4254-a149-798db8b6a803",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "8a20b38b-2aa5-4268-9c66-3fbad2996b01",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "17cf65e1-8d8e-4a47-8818-63a57482e9ca",
        "Height": 400,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 500
    },
    "mvc": "1.0",
    "views": [
{"id": "48e898fe-78f5-4cba-af88-1345f36ac52f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "8ac759c0-7795-41ac-988d-bd80b78a0821","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "37ec33b4-22ce-4699-b77c-4e7a9de3f619","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "c17bab43-4466-4b99-b7d8-e6befaeebb17","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e1d15abe-11ed-4b89-8e91-a063bc8e5af8","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "5291d400-a2bf-4939-881b-cdf35a9d332a","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "839b6471-96ba-41a1-96a1-2daf8ef1d59c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "c687ccf7-ec6e-4f5b-9cac-e4ea5b912717","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "777edea5-f5c1-4196-b411-fd151bc75e98",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}