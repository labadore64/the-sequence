{
    "id": "09ea28e9-18d2-4775-b6f7-61be923fc5c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldMenus",
    "eventList": [
        {
            "id": "45776c89-cede-4409-8605-7c1f6d92edd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09ea28e9-18d2-4775-b6f7-61be923fc5c5"
        },
        {
            "id": "e77e8103-ed36-4f23-be44-c14578f21d57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "09ea28e9-18d2-4775-b6f7-61be923fc5c5"
        },
        {
            "id": "84b3c371-6607-43d6-9dce-f7a5612f2137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09ea28e9-18d2-4775-b6f7-61be923fc5c5"
        },
        {
            "id": "f99a4cac-024b-4549-9e24-e16764ef0c6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "09ea28e9-18d2-4775-b6f7-61be923fc5c5"
        },
        {
            "id": "ab041742-b5ed-4d69-a6fa-3c7957ac3f32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "09ea28e9-18d2-4775-b6f7-61be923fc5c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}