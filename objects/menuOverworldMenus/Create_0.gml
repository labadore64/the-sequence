/// @description Insert description here
// You can write your code in this editor
#macro OVERWORLD_MENU_MOVING_TIME 15

event_inherited();

menu_type[0] = menuOverworldPartyStats;
menu_type[1] = menuOverworldItems;
menu_type[2] = menuCellPhone;
menu_type[3] = menuBrailleTabletsMain;
menu_type[4] = menuOverworldOption;

menu_draw = -1;

menu_size = 5;

menu_left = -1;
menu_right = -1;
menu_up = -1;
menu_down = -1;

menu_left = menuOverworldMenuTabLeft;
menu_right = menuOverworldMenuTabRight;

menus = ds_list_create();

running = true;

background_obj = instance_create(0,0,brailleGameBackground);

running = false;
newalarm[2] = 4

menupos = global.overworldMenuPosition

with(PropParent){
	prop_menu = true
}

moving = -1;

with(CameraOverworld){
	active = false;	
}
with(ScriptInteract){
	active = false	
}
with(ScriptInteractStepOver){
	active = false	
}


soundfxPlay(soundMenuOpenMenu)