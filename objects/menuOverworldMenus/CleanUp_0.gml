/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuOverworldMenuClearMenus()

if(ds_exists(menus,ds_type_list)){
	ds_list_destroy(menus);
}	

global.overworldMenuPosition = menupos;

with(Player){
	active=true;	
	click_delay = 5;	
}

with(brailleGameBackground){
	instance_destroy();	
}

with(menuControl){
	please_draw_black = false;	
}

fadeOut(10,c_black)

with(screenFade){
	script_run = false;	
}

with(PropParent){
	prop_menu = false
}


with(CameraOverworld){
	active = true	
}

with(ScriptInteract){
	active = true	
}
with(ScriptInteractStepOver){
	active = true	
}

with(AudioEmitter){
	if(audio_sound > -1){
		if(!(!global.AXNotify && access_only) &&
			!(global.AXNotify && not_access)){
			audio_resume_sound(audio_sound);	
		}
	}
}


mouseHandler_clear()
