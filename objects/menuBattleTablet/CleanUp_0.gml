/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


//battleHandlerSelectAllyClear()
ds_list_destroy(moves_element);
ds_list_destroy(moves);

ds_list_destroy(moves_targets);
ds_list_destroy(moves_type);
ds_list_destroy(moves_damage);
ds_list_destroy(moves_mp_cost);
ds_list_destroy(move_cando);
ds_list_destroy(move_skip);
ds_list_destroy(move_list_type);
battleHandlerClearFlash();
battleHandlerFlashSelected();
ds_list_destroy(braille_select);

for(var i = 0; i < 3; i++){
	with(braille_display[i]){
		instance_destroy();	
	}
}