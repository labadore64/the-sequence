{
    "id": "10589ba5-afd1-45c6-878f-e738e8e36aad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleTablet",
    "eventList": [
        {
            "id": "7b385af6-206a-4878-a596-2aba9eada6d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10589ba5-afd1-45c6-878f-e738e8e36aad"
        },
        {
            "id": "f4cd9964-6650-49a4-aa58-89d34035d7dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "10589ba5-afd1-45c6-878f-e738e8e36aad"
        },
        {
            "id": "24e1de4a-64e6-42d5-9d30-b21cc51d52db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "10589ba5-afd1-45c6-878f-e738e8e36aad"
        },
        {
            "id": "1fa6e7b0-3c00-430e-821c-938eb2153ef5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "10589ba5-afd1-45c6-878f-e738e8e36aad"
        },
        {
            "id": "749a5c24-36de-478e-beb2-65249e3f23fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "10589ba5-afd1-45c6-878f-e738e8e36aad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}