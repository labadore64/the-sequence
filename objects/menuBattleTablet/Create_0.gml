/// @description Insert description here
// You can write your code in this editor
event_inherited();
moves_element = ds_list_create();
menuBattleMovesInit();

// max number of tablets you can insert
max_tablet_count = 3;

gradient_height = 1;
drawGradient = false;

drawme = true;
draw_bg = false;

// how much the display shifts when a tablet is selected
shift_value = 80;

option_space = 90;

menu_draw = menuBattleTabletDraw;

menu_left = menuBattleTabletLeft;
menu_right = menuBattleTabletRight;
menu_up = menuBattleTabletUp;
menu_down = menuBattleTabletDown;
menu_cancel = menuBattleTabletCancel;

menu_select = menuBattleTabletSelect;

active = false;
is_bribe = false; //just here to prevent crashes.

character = -1;

move_list_type = ds_list_create();

newalarm[11] = 2

for(var i = 0; i < 3; i++){
	braille_display[i] = noone;
	numbering[i] = -1;
}

braille_select = ds_list_create();

main_tablet = noone;
main_index = -1;

is_bribe =false;

selected_string = "";

menu_ax_HUD = "battleTabletSelect"

menu_help = "battleTablet"

total_mpcost = 0;