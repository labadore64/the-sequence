/// @description Insert description here
// You can write your code in this editor

/*
moves_targets
moves_type
moves_damage
moves_mp_cost
*/

var obj = -1;
var maxdam = 50;
var tablet_count = 0;
for(var i = 0; i < 3; i++){
	obj = moves[|i];
	moveData(obj);
	if(character.tablet[i] > -1 && character.tablet_recover[i] == -1){
		tablet_count++;	
	}
	
	ds_list_add(moves_targets,target_number);
	ds_list_add(moves_type,menuBattleMovesGetElementSprite(element));
	ds_list_add(moves_element,element);
	ds_list_add(moves_damage,clamp(damage/maxdam,0,1));
	ds_list_add(moves_mp_cost,menuBattleMovesCalcMPCost(mp_cost,character));
	menuAddOption(name,flavor,selectScript)
	var cando = true;
	if(character.tablet_recover[i] > -1){
		cando = false
	}
	ds_list_add(move_cando,cando)
	ds_list_add(move_skip,target_skip)
	ds_list_add(move_list_type,move_type);
	//}
	//}

	menu_size = menuGetSize();
	braille_display[i] = instance_create(200,110+option_space*i,brailleCharacter);
	braille_display[i].display_character = false;
	braille_display[i].image_blend =global.textColor;
	braille_display[i].cell_trans_time = 1;
	if(cando){
		brailleDataSetCharacter(char,braille_display[i])
	}
}

for(var i = 0; i < 3; i++){
	if(floor(menuBattleTabletCalculateMPCostMultiplier(i+1)*character.mp.base) > character.mp.current){
		max_tablet_count = i-1;
		break;
	}	
}

max_tablet_count = min(max_tablet_count,tablet_count)

tts_title_string = "";

if(!move_cando[|menupos]){
	tts_title_string += "Recovery" + " "
}

if(numbering[menupos] > -1){
	tts_title_string += string(numbering[menupos]+1) + " ";	
}
total_mpcost = menuBattleTabletCalculateMPCostMultiplier(string_length(selected_string));
tts_title_string += "\"" + string(braille_display[menupos].cell_character) + "\""  + " " + menu_name_array[menupos] + " " + menu_desc_array[menupos]

tts_clear()
tts_say(tts_title_string)

menuBattleTabletUpdateHUD();

infokeys_assign(menu_ax_HUD);

textboxBattleMain("tutorial_battleB");

mouseHandler_clear();
mouseHandler_menu_default()