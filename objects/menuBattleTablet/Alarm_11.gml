/// @description Insert description here
// You can write your code in this editor

tts_title_string = "";

if(numbering[menupos] > -1){
	tts_title_string = string(numbering[menupos]+1) + " ";	
}
menuBattleTabletUpdateHUD();
tts_title_string += "\"" + string(braille_display[menupos].cell_character) + "\""  + " " + menu_name_array[menupos] + " " + menu_desc_array[menupos]
// Inherit the parent event
event_inherited();

with(menuControl){
	please_draw_all_visible = true;	
}

mouseHandler_clear();
mouseHandler_menu_default()
with(MouseHandler){
	active = true;
}