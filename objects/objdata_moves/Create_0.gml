/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\move\\move";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)

	name[itemcount] = ini_read_string_length("stats","name"," ",IMPORT_MAX_SIZE_MOVENAME);

	flavor[itemcount] = newline(ini_read_string_length("stats","desc"," ",IMPORT_MAX_FLAVOR_TEXT));

	damage[itemcount] = ini_read_real("stats","damage",0)
	mp_cost[itemcount] = ini_read_real("stats","mp_cost",0)

	element[itemcount] = stringToType(ini_read_string_length("stats","element"," ",IMPORT_MAX_SIZE_TYPE)); //type of attack
	move_type[itemcount] = stringToMoveType(ini_read_string_length("stats","move_type"," ",IMPORT_MAX_SIZE_MOVETYPE)) //physical or magical

	target_number[itemcount] = ini_read_real("stats","target_number",1); //amount of enemies affected.

	target_skip[itemcount] = stringToBool(ini_read_string_length("stats","target_skip","false",IMPORT_MAX_BOOLEAN)); //skips targeting on target screen.
	target_ally[itemcount] = stringToBool(ini_read_string_length("stats","target_ally","false",IMPORT_MAX_BOOLEAN)); //does this move target an ally?
	target_self[itemcount] = stringToBool(ini_read_string_length("stats","target_self","false",IMPORT_MAX_BOOLEAN));
	script_battle[itemcount] = asset_get_index(ini_read_string_length("stats","script_battle","",IMPORT_MAX_SIZE_SCRIPT)); //note: all battle scripts must accept character and target
	rotate_camera[itemcount] = stringToBool(ini_read_string_length("stats","camera_rotate","true",IMPORT_MAX_BOOLEAN));

	sprite_show[itemcount] = ini_read_string_length("stats","show_battler","",IMPORT_MAX_SIZE_SCRIPT)

	if(script_battle[itemcount] == -1){
		script_battle[itemcount] = move_script_attack;	
	}

	speed_modifier[itemcount] = ini_read_real("stats","speed_modifier",0) //how much your speed is affected by this move
	speed_priority[itemcount] = ini_read_real("stats","speed_priority",0) //move priority bracket

	char[itemcount] = ini_read_string_length("stats","char","",IMPORT_MAX_SIZE_CHAR)

	//animation stuff
	bg_animation[itemcount] = asset_get_index("spr_move_bg_" +ini_read_string_length("stats","bg_animation","",IMPORT_MAX_SIZE_SPRITE)); //animation for background
	
	var anima = ini_read_string_length("stats","sprite_animation","",IMPORT_MAX_SIZE_SPRITE);
	
	sprite_animation[itemcount] = asset_get_index("spr_move_self_" + anima); //animation on user

	if(sprite_animation[itemcount] == -1){
		sprite_animation[itemcount] = asset_get_index("spr_move_sprite_" + anima); //animation on user
	}
	
	sprite_animation[itemcount] = asset_get_index("spr_move_sprite_" + ini_read_string_length("stats","sprite_animation","",IMPORT_MAX_SIZE_SPRITE)); //animation on target
	fg_animation[itemcount] = asset_get_index("spr_move_fg_" + ini_read_string_length("stats","fg_animation","",IMPORT_MAX_SIZE_SPRITE)); //aniation on foreground
	var anima = ini_read_string_length("stats","self_animation","",IMPORT_MAX_SIZE_SPRITE);
	
	self_animation[itemcount] = asset_get_index("spr_move_self_" + anima); //animation on user

	if(self_animation[itemcount] == -1){
		self_animation[itemcount] = asset_get_index("spr_move_sprite_" + anima); //animation on user
	}

	//lunge_animation[itemcount] = asset_get_index("lungeAnimation_" + ini_read_string_length("stats","lunge_animation",""));//script for lunge animation to use
	animation_length[itemcount] = ini_read_real("stats","animation_length",0) //length of animation
	animation_knockback[itemcount] = stringToBool(ini_read_string_length("stats","animation_knockback","true",IMPORT_MAX_BOOLEAN))

	//item stuff
	item_id[itemcount] = ini_read_real("stats","item_id",0)  //what item correlates with this obj
	effect_percent[itemcount] = ini_read_real("stats","effect_percent",0) 
	
	// to do, fix the filters...

	var i = 0;

	while(true){
		if(!ini_key_exists("stats", "filter"+ string(i))){
			break;
	    }
		i++
	}

	var strrr = "";
	filter[itemcount,0] = -1;
	for(var j = 0; i > j; j++){
	
	    strrr = newline(ini_read_string_length("stats", "filter"+ string(j),"",IMPORT_MAX_SIZE_STATUSNAME));
		filter[itemcount,j] = strrr;
	}
	
	i = 0;

	while(true){
		if(!ini_key_exists("stats", "battle_text"+ string(i))){
			break;
	    }
		i++
	}

	
	battle_text[itemcount,0] = "";
	for(var j = 0; i > j; j++){
	
	    strrr = newline(ini_read_string_length("stats", "battle_text"+ string(j),"",IMPORT_MAX_SIZE_BATTLETEXT));
		battle_text[itemcount,j] = strrr;
	}
	
	i = 0;

	while(true){
		if(!ini_key_exists("stats", "anim_def"+ string(i))){
			break;
	    }
		i++
	}

	
	anim_def[itemcount,0] = "";
	for(var j = 0; i > j; j++){
	
	    strrr = newline(ini_read_string_length("stats", "anim_def"+ string(j),"",IMPORT_MAX_SIZE_ANIMDEF));
		anim_def[itemcount,j] = strrr;
	}

	ini_close()
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}
data_size = itemcount;