/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
gml_pragma("forceinline");
visible = false;
if(!ignore_clean_script){
	if(clean_script != -1){
		script_execute(clean_script);	
	}
}

//menuControlPop();
ds_list_destroy(menu_name);
ds_list_destroy(menu_description);
ds_list_destroy(menu_script);


tts_stop()
mouseHandler_clear()


//kill the menu control obj id

with(menuControl){
	end_game_obj = noone;
	with(active_menu){
		event_perform(ev_alarm,11)	
	}
}