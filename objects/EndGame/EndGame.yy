{
    "id": "9be951e0-1c32-4c07-b25e-6afe1d194340",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "EndGame",
    "eventList": [
        {
            "id": "a41ee7fc-2c3b-4bf8-82f2-81fa2ab999e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9be951e0-1c32-4c07-b25e-6afe1d194340"
        },
        {
            "id": "22ea18aa-c929-4d09-94b3-76b2df6a039b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9be951e0-1c32-4c07-b25e-6afe1d194340"
        },
        {
            "id": "30f74408-7ec9-4083-9557-545c58488046",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9be951e0-1c32-4c07-b25e-6afe1d194340"
        },
        {
            "id": "8b4a51b8-9144-413f-9841-d66ca6954b20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "9be951e0-1c32-4c07-b25e-6afe1d194340"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}