/// @description Insert description here
// You can write your code in this editor
//control
initTimingAlarm();
drawInfoText = true
do_not_draw_me = false;
infokey_script = -1;

bookmarkPause();
animation_complete = false;
image_speed = 0;
update_image_only_active = true;
drawOtherMenus = true;
objGenericCreate();

menupos = 0; //represents the position of the menu
toppos = 0; //represents the top displayed item in the menu
menu_size = 1; //represents the full size of the menu
wrap = true; //whether or not if you get to the end of the list, if it wraps around.
skip = 1; //how many points in the list to skip if you're skipping through the list
debug = global.debugMenu; //if in debug mode
draw_bg = true; //whether to draw transparent bg

//text
menu_name = ds_list_create(); //name of all menu items
menu_description = ds_list_create(); //descriptions of all menu items
menu_script = ds_list_create(); //scripts for all menu items

title = "Exit Game?"; //title of the menu
subtitle = "Game is saved before exiting."; //subtitle of the menu

//other scripts

menu_draw = EndGameDraw;

//keybind scripts

menu_up = menuParentMoveLeft;
menu_down = menuParentMoveRight;
menu_left = menuParentMoveLeft;
menu_right = menuParentMoveRight;
menu_select = menuParentSelect;
menu_select_hold = menuParentSelectHold;
menu_cancel = menuParentCancel;

cancelled = false;

menuAddOption(LANG_SELECT_YES,"Saves and exits.",EndGameSelect)
menuAddOption(LANG_SELECT_NO,"Return to game.",menuParentCancel)

menu_size = ds_list_size(menu_name);
//drawing variables
x = 400;
y = 250;

selected_scale_counter = 0;
selected_scale_amount = 1;

draw_centered = true; //Is the origin in the center of the box, or on the top left? 
draw_width = 380; //width of box in pixels
draw_height = 140; //height of box in pixels

gradient_height = 80; //height of the title
gradient_color1 = c_black; //gradient colour for the title left
gradient_color2 = c_white; //gradient colour for the title right

title_color = global.textColor; //text color for the title
subtitle_color = global.textColor; //text color for the subtitle
text_color = global.textColor; //text color for most of the menu

titleFontSize = 4; //title font size
subtitleFontSize = 2; //subtitle font size
text_size = 2; //size of option text

drawTitle = true; //whether or not to draw title
drawSubtitle = false; //whether or not to draw subtitle
drawGradient = false; //whether or not to draw gradient

title_x_offset = draw_width*.5; //xoffset for title
title_y_offset = 20; //yoffset for title
subtitle_x_offset = 20 //xoffset for subtitle
subtitle_y_offset = 80; //yoffset for subtitle
option_space = 100; //how many pixels between each option
option_x_offset = draw_width*.5 - option_space*.5 //xoffset for options
option_y_offset = 80; //yoffset for options

select_alpha = .25; //alpha of current selection
select_color = global.textColor; //selection color
select_percent = 1; //how much of the menu does the selection graphic occupy

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = global.textColor; //color of the title of the info box
info_subtitle_color = global.textColor; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

//following are specifically for the pop up animation
animation_start = 4 //animation length
animation_len = animation_start; //animation counter
anim_width = 0; //animation width size
anim_height = 0; //animation height size
anim_grad = 0; //animation gradient size



//accessibility
menuParentAccessibility();

//following represent the corners of the box
x1 = 0;
x2 = 0;
y1 = 0;
y2 = 0;

//following represents sound effects for the box
sound_move = soundMenuDefaultMove;
sound_cancel = soundMenuDefaultCancel;
sound_select = soundMenuDefaultMove;

menuParentUpdateBoxDimension();

//if in debug mode, add these objects to allow stretching the box
stretch[0] = noone; //top left corner
stretch[1] = noone; //bottom right corner
stretch[2] = noone; //gradient height

/*
if(debug){
	stretch[0] = instance_create_depth(x1,y1,-2,menuTransformationPoint);
	stretch[1] = instance_create_depth(x2,y2,-2,menuTransformationPoint);
	stretch[2] = instance_create_depth(x1+(x2-x1)*.5,y1+gradient_height,-2,menuTransformationPoint);
	
	for(var i = 0; i < 3; i++){
		stretch[i].parent_menu = id;	
	}
}*/

if(!instance_exists(BattleHandler)){
	//menuControlDrawBG();
}

newalarm[11] = 1;
newalarm[1] = 1;
active = false;

gradient1_original = gradient_color1;
gradient2_original = gradient_color2;

destroy_time = 1;

destroy_script = -1;

clean_script = -1;

ignore_clean_script = false;

//makes it so the player stops moving animation
with(Player){
	draw_action = PLAYER_ACTION_STAND		
	playerStandSprite();
}

tts_title_string = "End game?"
//makes it so the player stops moving animation
with(Player){
	draw_action = PLAYER_ACTION_STAND		
	playerStandSprite();
}

// the string to use to read the title
infokeys_loaded = false;

with(CollisionDetector){
	if(object_index == CollisionDetector){
		audio_pause_sound(sound_emit.audio_sound)
	}
}

ignore_tab = true;

enable_scrollwheel = false
scrollwheel_skip = 1; // how many entries to skip with the scroll wheel

mouseHandler_clear()
var xpos = 400;
var ypos = 267
mouseHandler_add(xpos-80-25,ypos-20,xpos-80+50,ypos+20,EndGameSelect,"Yes",true)
mouseHandler_add(xpos+80-50,ypos-20,xpos+80+25,ypos+20,menuParentCancel,"No",true)
menu_use_generic = true;