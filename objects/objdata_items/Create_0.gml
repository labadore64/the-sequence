/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\item\\item";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)

	name[itemcount] = ini_read_string_length("stats","name"," ",IMPORT_MAX_SIZE_ITEMNAME);
	flavor[itemcount] = newline(ini_read_string_length("stats","desc"," ",IMPORT_MAX_FLAVOR_TEXT));
	help[itemcount] = ini_read_string_length("stats","help","",IMPORT_MAX_SIZE_ITEMHELP);

	item_sprite[itemcount] = asset_get_index("item_" + ini_read_string_length("stats","sprite","",IMPORT_MAX_SIZE_SPRITE));
	//

	book_name[itemcount] = ini_read_string_length("stats","book_name","test_book",IMPORT_MAX_SIZE_ITEMBOOKNAME); //book name for item effects

	obj_id[itemcount] = asset_get_index(ini_read_string_length("stats","obj_id","",IMPORT_MAX_SIZE_ITEMOBJID));

	overworld_effect[itemcount] = asset_get_index("script_itemOverworld_" + ini_read_string_length("stats","overworld_effect","",IMPORT_MAX_SIZE_SCRIPT)); //if not -1 executes a script.

	battle_effect[itemcount] = objdataToIndex(objdata_moves,ini_read_string("stats","battle_effect","none")); //if not -1 executes a script.

	price[itemcount] = ini_read_real("stats","price",1)
	can_toss[itemcount] = stringToBool(ini_read_string_length("stats","can_toss","true",IMPORT_MAX_BOOLEAN)); //can be tossed.
	can_give[itemcount] = stringToBool(ini_read_string_length("stats","can_give","true",IMPORT_MAX_BOOLEAN)); //can be given to a character.
	can_sell[itemcount] = stringToBool(ini_read_string_length("stats","can_sell","true",IMPORT_MAX_BOOLEAN)); //can be sold/tossed. 

	single_use[itemcount] = stringToBool(ini_read_string_length("stats","single_use","true",IMPORT_MAX_BOOLEAN));//determines if item is single use on the overworld.
	
	item_type_string[itemcount]=ini_read_string_length("stats","item_type","other",IMPORT_MAX_SIZE_ITEMTYPE)
	item_type[itemcount] = stringToItemType(item_type_string[itemcount]); //what kind of item it is for sorting

	//following are for stat boosts from items.
	stat_attack[itemcount] = ini_read_real("stats","stat_attack",0)
	stat_defense[itemcount] = ini_read_real("stats","stat_defense",0)
	stat_magic[itemcount] = ini_read_real("stats","stat_magic",0)
	stat_resistance[itemcount] = ini_read_real("stats","stat_resistance",0)
	stat_agility[itemcount] = ini_read_real("stats","stat_agility",0)
	stat_mpr[itemcount] = ini_read_real("stats","stat_mpr",0)

	ini_close();
	
	// updates counter
	itemcount++;
	testname = filename+string(itemcount)+ext;
	
}

data_size = itemcount;

