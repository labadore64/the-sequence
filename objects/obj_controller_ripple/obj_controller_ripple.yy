{
    "id": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_ripple",
    "eventList": [
        {
            "id": "71a64d59-c9f4-40c8-b4e7-be017ff915cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5"
        },
        {
            "id": "dd6d7867-69c3-4a44-91b5-1fb2db4c15e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5"
        },
        {
            "id": "38621a08-449b-4839-9888-5c663d3fb36d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5"
        },
        {
            "id": "b2d04f0b-6070-4657-84c2-ab8e6bea6963",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5"
        },
        {
            "id": "6600bd52-b744-466f-a88a-bf87e6efb616",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5"
        },
        {
            "id": "91ab51ca-5c5c-4c97-94ae-7b8657a8e5e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "e8fec8d2-b01d-4878-ba39-dee6d4324ac5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}