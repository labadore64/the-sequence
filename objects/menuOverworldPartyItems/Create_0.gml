/// @description Insert description here
// You can write your code in this editor
event_inherited();
initTimingAlarm();

character = -1;

item_sprite = -1;
menuOverworldPartyItemsStrings();

animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = 5;

draw_bg = false;

//active = false;
menu_update_values = menuOverworldItemsPartyUpdateValues
fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;

menu_ax_HUD = "characterItemMain"

menu_help = "items_equip"


jump_y = 0; // the actual jump display value
jump_speed = 0; // current jump speed
jump_start_speed = 5;
jump_change = .85// rate of jump change

textboxMenuTutorial("tutorial_menuC")

ignore_tab = true;

	mousepos_x = -230