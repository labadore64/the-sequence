{
    "id": "9cf153c4-3de1-4ae1-ae21-7639931c13a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartyItems",
    "eventList": [
        {
            "id": "43f82e4a-1c03-412c-bc65-039c1a272b79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9cf153c4-3de1-4ae1-ae21-7639931c13a7"
        },
        {
            "id": "a947346e-b337-45f9-9e9c-47fe8db78420",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9cf153c4-3de1-4ae1-ae21-7639931c13a7"
        },
        {
            "id": "f843f0e4-6a21-4432-bfa2-4ea78ad84abc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9cf153c4-3de1-4ae1-ae21-7639931c13a7"
        },
        {
            "id": "cb1b295b-5b14-471a-9d07-d1403e1123d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "9cf153c4-3de1-4ae1-ae21-7639931c13a7"
        },
        {
            "id": "93e29c22-629b-49e4-8a3a-0a1e7324bdea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "9cf153c4-3de1-4ae1-ae21-7639931c13a7"
        },
        {
            "id": "db6608be-2725-4d58-892b-e36f9487891e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "9cf153c4-3de1-4ae1-ae21-7639931c13a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}