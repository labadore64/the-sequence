/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuControlForceDrawMenuBackground();
	
	with(Player){
		item_use_lock = false;	
	}
	
var sizer = ds_list_size(diagrams);

for(var i = 0; i < sizer; i++){
	if(!is_undefined(diagrams[|i])){
		with(diagrams[|i]){
			instance_destroy();	
		}
	}
}
	
ds_list_destroy(diagrams);