/// @description Insert description here
// You can write your code in this editor
event_inherited();
initTimingAlarm();
filename = "test_book";

text_string = "";

//for(var i = 0; i < 40; i++){
	menuAddOption("Test","A test string.",-1)
//}
display_lines = 11

skip_amount = display_lines;

menu_size = menuGetSize();

menuParentSetDrawGradient(false)
menuParentSetDrawSubtitle(false)

menu_draw = menuBookDraw;

menu_down = menuBookMoveRight
menu_up = menuBookMoveLeft

menu_left = menuBookMoveLeft
menu_right = menuBookMoveRight

newalarm[0] = 2;
newalarm[2] = 1;

	with(Player){
		item_use_lock = true	
	}
	
animation_start = 1 //animation length

animation_len = animation_start;

draw_bg = false;

//active = false;

if(!menuControl.help_open){
	fadeIn(5,c_black)

	with(screenFade){
		script_run = false;	
	}
	newalarm[3] = 5
	animation_start = 5 //animation length
	animation_len = animation_start;
}

menu_control = menu_cancel

menu_select = menuBookSelect;

diagrams = ds_list_create()
diagram_length = 0;

ignore_tab = true

enable_scrollwheel = true

//menu_update_values = menuBookUpdate

mouseHandler_clear();