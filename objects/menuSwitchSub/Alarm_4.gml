/// @description Insert description here
// You can write your code in this editor
var partychar = noone;
var switchchar = noone;

with(menuCallSub){
	partychar = character;	
}
with(menuSwitchSub){
	switchchar = character;	
}

if(partychar > -1 && switchchar > -1){
	var party_index = ds_list_find_index(obj_data.party,partychar);
	if(party_index > -1){
		obj_data.party[|party_index] = switchchar;	
	}
	
	// close everything but the main call menu
	with(menuCallSub){
		instance_destroy();	
	}
	with(menuCallSwitchList){
		instance_destroy();	
	}
	
	// refresh the call list
	with(menuCallMain){
		menuCallSetCalls();	
	}
	
	// kill yourself
	instance_destroy();
}