/// @description Insert description here
// You can write your code in this editor
dialing = false;
minimum_range = 4;

in_party = false;

// Inherit the parent event
event_inherited();

// menu options

x= 400;
y = 325+200

menu_background_color = c_black
gradient_height = 0 //height of the title
menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430+60)
option_y_offset = 85
title_y_offset = 0
option_x_offset = 20;
select_percent = 1
animation_len = 1
option_space = 60
text_size = 3;

menuParentUpdateBoxDimension()

menu_draw = menuCallSubDraw

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

draw_bg = false;
newalarm[2] = 1;
skip = 5

wrap = false;

selected_call = 0;

character = -1;

menu_up = menuCallSupMoveUp;
menu_down = menuCallSupMoveDown;

menu_left = menuCallSupMoveUp;
menu_right = menuCallSupMoveDown;

menu_select = menuCallSupSelect

menu_cancel = menuCallSupCancel

menu_help = "phonecall"

ignore_tab = true