{
    "id": "762dd0da-39d2-449a-81c1-a354c308d4ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuSwitchSub",
    "eventList": [
        {
            "id": "d721693d-dcee-4f1a-b0ce-9065fe3d5ec6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        },
        {
            "id": "35c29170-9f4a-4463-a6aa-6367b0521157",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        },
        {
            "id": "eb246b0e-ef4f-4403-87a8-da7f224b691f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        },
        {
            "id": "ec84370d-6300-4b72-82fe-a31c9d66d23f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        },
        {
            "id": "d6ab4b69-10a9-4a9f-a9e7-2caf6b647178",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        },
        {
            "id": "a88a9207-91e6-4a6e-9f20-bdd73a46df10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        },
        {
            "id": "a9b48f75-93ee-45ab-a8ba-d66b53669ba0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "762dd0da-39d2-449a-81c1-a354c308d4ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}