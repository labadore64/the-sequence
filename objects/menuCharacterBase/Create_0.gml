/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_select = menuCharacterSelect;

character_id = 0;
character = noone;
can_talk = false

character_name = "";
character_sprite = noone

character_sprite_x = 385;
character_sprite_y = 100;

character_sprite_xscale = .75
character_sprite_yscale = .75

text_start = "Hey there... don't be a stranger.";
text_return = "Thanks for coming around. You need anything else?";

text_to_use = text_start;

text_box = noone;

text_counter = 0;
text_push = 2;


title = "Convenience Store"

animation_start = 10 //animation length

animation_len = animation_start;

newalarm[3] = 10;
newalarm[4] = 1

draw_bg = false;

//active = false;

fadeIn(11,c_black)

with(screenFade){
	script_run = false;	
}
image_speed=2


menu_draw = menuCharacterDraw;

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;

active = false;

select_percent = .25
menuParentSetInfoboxHeight(515)

talk_sprite_x = 275;
talk_sprite_y = 375;
text_y_start = talk_sprite_y-38

transition = false;

audio_group_load(audiogroup_character_voice)