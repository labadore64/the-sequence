{
    "id": "35653b71-cb48-431a-a97c-4f6608da2d9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterBase",
    "eventList": [
        {
            "id": "27f64ae6-b43c-4a5d-9d1a-0e5b70077ac9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "35653b71-cb48-431a-a97c-4f6608da2d9b"
        },
        {
            "id": "f506dd91-f094-4e1e-914c-14b12544366a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "35653b71-cb48-431a-a97c-4f6608da2d9b"
        },
        {
            "id": "f95dd6a0-eb42-4b32-b419-42c74a957bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "35653b71-cb48-431a-a97c-4f6608da2d9b"
        },
        {
            "id": "2ae0aae2-cc04-415e-9616-0b48837fe3cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "35653b71-cb48-431a-a97c-4f6608da2d9b"
        },
        {
            "id": "fbeca45f-fe33-4a0d-922d-b9e810a7bdb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "35653b71-cb48-431a-a97c-4f6608da2d9b"
        },
        {
            "id": "4f7195ad-05ea-4f03-89f0-ff0d9863c728",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "35653b71-cb48-431a-a97c-4f6608da2d9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cbe81332-02ee-4a31-85f8-b80725d92c6c",
    "visible": true
}