/// @description Insert description here
// You can write your code in this editor
ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items
var chara = obj_data.characters[|character_id];
character_name = chara.name;
active = true;

can_talk = statCanTalk(chara)