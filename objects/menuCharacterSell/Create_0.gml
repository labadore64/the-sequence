/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

enter_script = menuCharacterScriptSellSub
menuOverworldItemsListStart(enter_script);

var chara = noone;

var happy_boost = 0
var sell_multiplier = 1;

with(menuCharacterShop){
	chara = character;
	active = false;
}

if(chara != noone){
	if(chara.sell_multiplier_script != -1){
		sell_multiplier = script_execute(chara.sell_multiplier_script)
	}
}

multiplier = sell_multiplier+happy_boost;
draw_money = true;
menuParentSetTitle("Sell")
menuParentSetSubtitle("Sell items in your inventory for cash.")