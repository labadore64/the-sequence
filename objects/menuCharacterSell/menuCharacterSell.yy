{
    "id": "e96b2adb-9e2a-40bf-95a7-6e28c3dcf2a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSell",
    "eventList": [
        {
            "id": "52b013f1-efad-40ab-ad39-18853de9cefd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e96b2adb-9e2a-40bf-95a7-6e28c3dcf2a0"
        },
        {
            "id": "971fdc1c-2fb2-44ab-9805-8037fe01d5a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e96b2adb-9e2a-40bf-95a7-6e28c3dcf2a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}