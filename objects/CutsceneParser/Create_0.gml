/// @description Insert description here
// You can write your code in this editor

//tests

//lock player first 
cutsceneLockPlayer()

//stuff needed for execution.
execution_pointer = 0;
header_address = 0; //address for all the headers.
header_list = ds_list_create();
execution_stack = ds_stack_create(); //represents all the executions that can run at once.
block_list = ds_list_create();

//if initialized, starts processing
initialized = false;

//read the file into a byte list.
filename = "test.bin"

newalarm[0] = 1;

//whether or not the stack should be checked this frame
check_stack = false;

continue_cutscene = false