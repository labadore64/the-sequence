{
    "id": "59e3c708-af5d-4761-8cbb-41da105a631a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CutsceneParser",
    "eventList": [
        {
            "id": "4304814a-ffaf-4613-9392-41ef309cc4c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "59e3c708-af5d-4761-8cbb-41da105a631a"
        },
        {
            "id": "078d9299-072d-457f-b6fe-0dc2f3b0e0b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "59e3c708-af5d-4761-8cbb-41da105a631a"
        },
        {
            "id": "968053c9-40c7-4e43-8b10-feeba4474ed0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "59e3c708-af5d-4761-8cbb-41da105a631a"
        },
        {
            "id": "19ab79e4-d773-42f7-b0e2-cd4ebf419ef0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "59e3c708-af5d-4761-8cbb-41da105a631a"
        },
        {
            "id": "441c606f-7820-4b64-bb76-d707d436a41a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "59e3c708-af5d-4761-8cbb-41da105a631a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}