{
    "id": "5d69a131-cd5d-47f7-927f-05e5f256bc91",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleFight",
    "eventList": [
        {
            "id": "12bbc546-81ca-402d-85ba-b6b1187ba536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5d69a131-cd5d-47f7-927f-05e5f256bc91"
        },
        {
            "id": "d0fd151e-6390-40a5-b40b-339ca9290459",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5d69a131-cd5d-47f7-927f-05e5f256bc91"
        },
        {
            "id": "68bf7b71-6326-4171-8fa8-b62dd1c519b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5d69a131-cd5d-47f7-927f-05e5f256bc91"
        },
        {
            "id": "2044a7fe-d419-482d-ab89-98cb7b96c4f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "5d69a131-cd5d-47f7-927f-05e5f256bc91"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}