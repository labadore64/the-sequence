/// @description Insert description here
// You can write your code in this editor
mouseHandler_clear();

event_inherited();

var cando = true

with(BattleHandler){
	ds_list_copy(select_list,party_list);
	
	//remove any characters whos input should be skipped.
	var sizer = ds_list_size(select_list);
	var obj = -1;
	
	for(var i = sizer-1; i >= 0; i--){
		obj = select_list[|i];
		if(!is_undefined(obj)){
			if(instance_exists(obj)){
				if(!menuBattleMainCanInput(obj)){
					ds_list_delete(select_list,i);	
				}
			}
		}
	}	
	if(ds_list_empty(select_list)){
		cando = false;
	}
}

if(cando){
	menuBattleFightInit();

	active = false;

	battleHandlerClearFlash();
	battleHandlerFlashSelected();

	newalarm[11] = 1

	menuControlUpdate()

	
} else {
	instance_create(0,0,battleStartAttack)
	with(menuParent){
		active=false;
		battleHandlerClearFlash();
		battleHandlerSelectAllyClear()
		battleTextDisplayClearText();
		instance_destroy()	
	}	
}

menu_ax_HUD = "battleMain";

menu_help = "battleFight"
