{
    "id": "86429a93-cc5a-417e-9b41-2836f4217931",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player",
    "eventList": [
        {
            "id": "c90fd7ba-0486-4d41-9f41-26c70abfde3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "ae3a5ddf-30e8-4e42-8f30-cf3624732453",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "df65a022-5ac3-4513-ae59-5821bdf1d740",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "10afb9fe-5784-4f7c-ae2c-c1a4496e05b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "ece29455-660b-4752-b06e-d94c30af0696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "f409dfde-a101-4a25-937a-7462488cba63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "a98fbaf2-cbb5-4a4d-ba0d-0b75a0234780",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "52eb9132-ab44-4acf-82fe-42c9474a47e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "49ce139d-ce7c-4912-91f7-fc8ec26ff924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 9,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "9daf49c8-c64f-4f13-b25d-020ef65c56f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 71,
            "eventtype": 9,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "d76ab0af-10e5-4206-b493-0dd925b4e65f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "911ea4fe-97a7-49a2-8bf7-a5da47993e1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "08c0f401-1157-4c32-bac2-93c0d981ba4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 5,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "0f1978d9-7e38-4e57-bb53-b405dbe3b37a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 78,
            "eventtype": 5,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "f98ced1e-2d82-48f6-80b4-63e6d9f26c78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "07b220dc-1be2-4bb0-b5f8-1cfc2289a20c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        },
        {
            "id": "04df8408-265a-4c43-9a0d-cc6357f05d4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "86429a93-cc5a-417e-9b41-2836f4217931"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a314b28c-1027-400d-ade0-93ba910f74fa",
    "visible": true
}