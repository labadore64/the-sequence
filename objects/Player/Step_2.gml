/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(zoom_x_previous != zoom_x){
	zoom_factor_x = (1/zoom_x);
	zoom_factor_y = (1/zoom_y);	
}
zoom_x_previous = zoom_x;

last_orientation = orientation;

last_dir = dir;

// other draw stuff

draw_the_x_holder = 2*sign(image_xscale)*global.scale_factor;
draw_the_y_holder = 2*global.scale_factor;

var shad_offset = 210*global.scale_factor
var xset = 120*global.scale_factor;
var yset = 40*global.scale_factor;

draw_shadx1 = draw_the_xx-xset
draw_shadx2 = draw_the_xx+xset
draw_shady1 = draw_the_yy+shad_offset-yset
draw_shady2 = draw_the_yy+shad_offset+yset

// draw stuff
var diffx = zoom_x
var diffy = zoom_y
	
if(diffx == 0){
	diffx = 1;	
}
	
if(diffy == 0){
	diffy = 1;	
}
	
var scaler = draw_scale
	
var strex = (1/diffx);
var strey = (1/diffy);
	
var xxxx = x*strex+400;
var yyyy = y*strey+300;
	
with(CameraOverworld){
	xxxx -= x*strex;
	yyyy -= y*strey;
}
	
draw_posx = xxxx*global.scale_factor-256*strex*scaler*global.scale_factor;
draw_posy = yyyy*global.scale_factor-256*strey*scaler*global.scale_factor;
	
var viewx = 0;
var viewy = 0;
	
with(CameraOverworld){
	var viewx = draw_cornerx1
	var viewy = draw_cornery1
}
	
var offsett = 400;
	
if(orientation > 180){
	offsett = 200;	
}
	
draw_strex = strex*scaler;
draw_strey = strey*scaler;
	
// end draw stuff