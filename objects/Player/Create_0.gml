/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
playerCreate();

click_delay = -1;

sight_detector = instance_create(x,y,SightDetector);

gamepad_see_name = gamepadbind_get("sight");
gamepad_coordinates = gamepadbind_get("coordinates")
bookmark_key = keybind_get("bookmark");
bookmark_gamepad = gamepadbind_get("bookmark");
seeing_name_key = keybind_get("access1");
seeing_desc_key = keybind_get("access2");
axx_key = keybind_get("access3");
axy_key = keybind_get("access4");

corner_counter = -1;

spray_surface = -1
double_select = false;

overworld_item_active = false;
overworld_item_wait_trigger = true;

binocular_item = false;
binoculars = false
binoculars_distance = 200; //distance from player that binoculars see.
binoculars_zoom = .2

binocular_rotate = 1.5;

menu_help = "player"

camera_x = 0;
camera_y = 0;

camera_zoom_x = 1;
camera_zoom_y = 1;

prop_place_id = -1; //object id of placed prop
prop_placing = false; //is in process of placing prop?
prop_place_item = -1; //item id of placed prop
prop_place_dist = 50; //distance from player to place
prop_place_max = 25; //how many items can be placed on a map

bird_millet = false;
bird_suet = false;
bird_carrion = false;

draw_the_xx = 256*global.scale_factor;
draw_the_yy = 256*global.scale_factor;

audio_group_load(audiogroup_birds)
