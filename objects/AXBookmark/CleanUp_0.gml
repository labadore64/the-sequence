/// @description Insert description here
// You can write your code in this editor
with(sound_emitter){
	instance_destroy();	
}

var myself = id;

with(AXBookmarkHandler){
	var index = ds_list_find_index(bookmarks,myself);
	if(index != -1){
		ds_list_delete(bookmarks,index);	
	}
}