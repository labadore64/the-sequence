{
    "id": "76975396-4ce6-4073-abff-bc73b6961754",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AXBookmark",
    "eventList": [
        {
            "id": "817194d3-b72f-4a60-9ca6-013a7abcef5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76975396-4ce6-4073-abff-bc73b6961754"
        },
        {
            "id": "2bb483b8-44aa-4d50-b168-e3b197e25cbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "76975396-4ce6-4073-abff-bc73b6961754"
        },
        {
            "id": "e16d4506-107d-450e-aa7e-78e13fd1553b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "76975396-4ce6-4073-abff-bc73b6961754"
        },
        {
            "id": "4128b7dd-690e-48f4-a785-40bc2509dc62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76975396-4ce6-4073-abff-bc73b6961754"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}