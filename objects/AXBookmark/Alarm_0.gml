/// @description Insert description here
// You can write your code in this editor
sound_emitter = instance_create_depth(x,y,0,AudioEmitter);
//sound_emitter.check_for_collision = true;
sound_emitter.audio_sound = audio_sound;
sound_emitter.access_only = true;

sound_emitter.falloff_max = 128
sound_emitter.falloff_min = 25
sound_emitter.is_bookmark = true;
sound_emitter.pitch_base = pitch