{
    "id": "b0a169e6-be8f-40d6-86a7-9517f18607ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DialogHandlerPhone",
    "eventList": [
        {
            "id": "e968aa05-5e64-4a8a-bcd8-48a3bd3a6f50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0a169e6-be8f-40d6-86a7-9517f18607ac"
        },
        {
            "id": "408e6e60-c5ad-4f75-a0cd-4dedca3349df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b0a169e6-be8f-40d6-86a7-9517f18607ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02e46696-1196-432d-b96a-6773c46a9b54",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}