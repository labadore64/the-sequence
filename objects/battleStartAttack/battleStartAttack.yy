{
    "id": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleStartAttack",
    "eventList": [
        {
            "id": "02edbcbc-1ef0-42fa-a06a-4546e749faa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        },
        {
            "id": "f68b8bea-06a4-413e-b515-f486baf74994",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        },
        {
            "id": "fe824924-a450-4942-9a2a-7a3bc9948de2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        },
        {
            "id": "305f821b-f8e5-4601-9ebd-9bc62f3d2cee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        },
        {
            "id": "73e57168-eae7-406c-86c0-3182ba3eb396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        },
        {
            "id": "c9cc9fdf-f9b6-4acd-81cf-83f5375b6880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        },
        {
            "id": "82b99742-84d2-44e3-b671-25f49d8c6eaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 2,
            "m_owner": "ded5d6e0-050d-4952-8250-79ed7e3ac7eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}