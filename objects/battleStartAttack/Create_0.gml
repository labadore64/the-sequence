/// @description Insert description here
// You can write your code in this editor
//this obj loads all the attacks in order
//and handles the battle stuff until the end phase
//or end of battle display
randomize();
event_inherited();
initTimingAlarm();
tts_title_string = "";
infokeys_loaded = false;
infokey_script = -1
with(Battler){
	battlerUpdateStats();	
}

with(battleEndPhase){
	instance_destroy();	
}

battleHandlerClearFlash();
delay = 20;
newalarm[1] = 1
newalarm[0] = delay
newalarm[2]=delay
if(!BattleHandler.fleeing){
	battleStartAttackInitEnemyMove();
}
battleStartAttackInitPriority();

battleStartAttackPriorityUpdate();

textbox_hold_counter = 0;
textbox_hold_max = global.text_auto_speed;

menu_select = battleStartAttackConfirm
menu_select_hold = battleStartAttackConfirmHold;

current_attack = -1;
active=false;
//current_attack = battleStartAttackPriorityNext();

infokeys_init();
menu_ax_HUD = "battleMain";
menu_help = "battleMain"
infokeys_assign(menu_ax_HUD)
updateBattleMainHUD();

//battleStartBegin();

// sets tutorial flags
eventNormalSet("tut004");
eventNormalSet("tut005");
eventNormalSet("tut006");

mouseHandler_clear()

// update this later to update the text...
mouseHandler_add(0,0,800,600,battleStartAttackConfirmMouse,"")
mouseHandler_center_cursor(0)