/// @description Insert description here
// You can write your code in this editor

for(var i = 0; i < 26; i++){
	var movename = "";
	if(objdata_tablet.move_id[i] > -1){
		movename = objdata_moves.name[objdata_tablet.move_id[i]]
	}
	
	menuAddOption(objdata_tablet.alpha[i],movename,select_script)
}
menu_size = menuGetSize();

var counter = 0;

for(var i = 0; i < braille_rows; i++){
	for(var j = 0; j < braille_columns; j++){
		if(counter < 26){
			display_cell[i,j] = instance_create(
							start_x + space_x*j,
							start_y + space_y*i,
							brailleCharacter);
			display_cell[i,j].cell_trans_time =0;
			if(!obj_data.tablet_obtained[counter] || obj_data.tablet_character[counter] > -1){
				display_cell[i,j].image_blend = $333333
				active_cell[counter] = false;
			} else {
				active_cell[counter] = true;	
			}
			brailleDataSetCharacter(objdata_tablet.alpha[counter],display_cell[i,j]);	
		} else {
			display_cell[i,j] = noone	
		}
		counter++;
	}
}

drawOtherMenus = false

menuTabletsMainAdjustRight();

menuTabletsMainSelectedCellUpdate();

/// @description Insert description here
// You can write your code in this editor
active = true;

with(menuControl){
	please_draw_black = false;	
}

hudControllerClearData();
hudControllerClearObjects();

	menu_ax_HUD = "tabletDetails"
	hudPopulateTablet("tablet",menupos);
	with(menuPartyTablet){
		hudPopulateOverworldStats("character",character_select.character);
	}


if(menu_ax_HUD != ""){
	infokeys_assign(menu_ax_HUD);
}
tts_title_string = "Select a tablet."
tts_clear();
if(tts_title_string != ""){
	tts_say(tts_title_string)	
}
mouseHandler_clear()
// do braille columns
var spacee_x = space_x
var spacee_y = space_y

for(var i = 0; i < braille_rows; i++){
	for(var j = 0; j < braille_columns; j++){
			//draw the boundary box

			var x_pos = start_x + space_x*j + space_x*.22;
			var y_pos = start_y + space_y*(i) +spacee_y*.5;
			var v_size = spacee_y*.5
			var h_size = spacee_x*.5

			if(instance_exists(display_cell[i,j])){
				mouseHandler_add(
							x_pos-h_size,y_pos-v_size,
							x_pos+h_size,y_pos+v_size,
							select_script, 
							display_cell[i,j].cell_character,
							true);
			}
	}
}

menu_update_values = menuTabletsMainSelectedCellUpdate

mouseHandler_menu_setCursor();