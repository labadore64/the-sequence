{
    "id": "8f8b7605-0627-4504-93da-1d5738c71611",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuPartyTabletSelect",
    "eventList": [
        {
            "id": "4e48f54d-acd3-45e1-babe-1e014ba71112",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f8b7605-0627-4504-93da-1d5738c71611"
        },
        {
            "id": "9f2ec091-8d15-42a2-b0e9-bcb0ddc681a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "8f8b7605-0627-4504-93da-1d5738c71611"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}