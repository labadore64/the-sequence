{
    "id": "92e17e8e-7b48-4c00-af0c-4c2d93e00c77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_refraction",
    "eventList": [
        {
            "id": "36b82ca8-0b47-4823-a4a6-c26b8130876c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92e17e8e-7b48-4c00-af0c-4c2d93e00c77"
        },
        {
            "id": "e8449a20-eb05-4de4-bc10-da4bc597a13b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92e17e8e-7b48-4c00-af0c-4c2d93e00c77"
        },
        {
            "id": "4713b53d-51e6-4ffc-a26d-48923a7f8290",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "92e17e8e-7b48-4c00-af0c-4c2d93e00c77"
        },
        {
            "id": "3dfb2ab6-32c9-41ef-8279-5721812a8da6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "92e17e8e-7b48-4c00-af0c-4c2d93e00c77"
        },
        {
            "id": "793f1d75-5aa3-490e-8e73-1812aaeea8ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "92e17e8e-7b48-4c00-af0c-4c2d93e00c77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}