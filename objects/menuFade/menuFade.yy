{
    "id": "ca412c9b-59f1-4355-b1e3-21d13728a143",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuFade",
    "eventList": [
        {
            "id": "ad156331-987e-4d01-9a8f-a9754a059447",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca412c9b-59f1-4355-b1e3-21d13728a143"
        },
        {
            "id": "90277db6-1585-4b6a-adc0-b453b4288f5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ca412c9b-59f1-4355-b1e3-21d13728a143"
        },
        {
            "id": "1f7662f9-b01d-413b-a823-aadc4d7b9344",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ca412c9b-59f1-4355-b1e3-21d13728a143"
        },
        {
            "id": "31527fee-5e59-4ef9-919b-1fa5a4789482",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ca412c9b-59f1-4355-b1e3-21d13728a143"
        },
        {
            "id": "35c50602-973d-4675-876d-9dac2b3d1520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca412c9b-59f1-4355-b1e3-21d13728a143"
        },
        {
            "id": "e35bd86a-9372-4240-b40e-2854ba8b1ede",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "ca412c9b-59f1-4355-b1e3-21d13728a143"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}