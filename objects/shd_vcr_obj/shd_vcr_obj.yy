{
    "id": "dd5f3948-99f3-4b70-9155-06f5a3f91757",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shd_vcr_obj",
    "eventList": [
        {
            "id": "4f8e9c99-2b6a-4bdf-8ac1-c37f722767f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd5f3948-99f3-4b70-9155-06f5a3f91757"
        },
        {
            "id": "43208cf1-bede-4d79-939d-93c530f2c054",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd5f3948-99f3-4b70-9155-06f5a3f91757"
        },
        {
            "id": "747819bb-504b-49fc-a1e8-39b6dcf3a12f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "dd5f3948-99f3-4b70-9155-06f5a3f91757"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}