/// @description Insert description here
// You can write your code in this editor

//blur

uni_resolution_hoz = shader_get_uniform(shd_gaussian_horizontal1,"resolution");
uni_resolution_vert = shader_get_uniform(shd_gaussian_vertical1,"resolution");
var_resolution_x = global.window_width;
var_resolution_y = global.window_height;

uni_blur_amount_hoz = shader_get_uniform(shd_gaussian_vertical1,"blur_amount");
uni_blur_amount_vert = shader_get_uniform(shd_gaussian_horizontal1,"blur_amount");
var_blur_amount = .65;

shader_enabled = true;
final_surface = -1

surf = -1

//uniforms
shd_vcr_glitch_properties = shader_get_uniform(shd_vcr,"glitch_properties");
shd_vcr_glitch_wave_properties = shader_get_uniform(shd_vcr,"glitch_wave_properties");
shd_vcr_jitter_properties = shader_get_uniform(shd_vcr,"jitter_properties");
shd_vcr_vignette_properties = shader_get_uniform(shd_vcr,"vignette_properties");
shd_vcr_scan_properties = shader_get_uniform(shd_vcr,"scan_properties");

shd_vcr_counter = shader_get_uniform(shd_vcr,"counter");
shd_vcr_noise_strength = shader_get_uniform(shd_vcr,"noise_strength")
/*
shd_vcr_jitter_amount = shader_get_uniform(shd_vcr,"jitter_amount");
shd_vcr_jitter_speed = shader_get_uniform(shd_vcr,"jitter_speed");



shd_vcr_vignette_inner_circle = shader_get_uniform(shd_vcr,"vignette_inner_circle")
shd_vcr_vignette_outer_circle = shader_get_uniform(shd_vcr,"vignette_outer_circle")

shd_vcr_scan_linenumber = shader_get_uniform(shd_vcr,"scan_linenumber")
shd_vcr_scan_amount = shader_get_uniform(shd_vcr,"scan_amount")
*/

shd_vcr_u_uv = shader_get_uniform(shd_vcr,"u_uv")

//values

glitch_bars = 12
glitch_speed = .025
wave_amount = .0044
wave_speed = .05
wave_jitter = .008

jitter_amount = .0035
jitter_speed = 1.94

noise_strength = .16

vignette_inner_circle = 1000
vignette_outer_circle = 600

scan_linenumber = .015
scan_amount = .05

u_uv_x = 800;
u_uv_y = 600;

counter = 0