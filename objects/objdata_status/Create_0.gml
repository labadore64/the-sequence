/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\battle\\status\\status";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data
	ini_open(testname)
	
	name[itemcount] = ini_read_string_length("data","name","a",IMPORT_MAX_SIZE_STATUSNAME);
	desc[itemcount] = newline(ini_read_string_length("data","desc","a",IMPORT_MAX_SIZE_DESC));
	final_text[itemcount] = ini_read_string_length("data","final_text","a",IMPORT_MAX_SIZE_STATUSFINALTEXT);
	
	sprite[itemcount] = asset_get_index("spr_filter_" + string_lower(name[itemcount]));
	shader[itemcount] = asset_get_index("shdr_filter_" + string_lower(name[itemcount]));

	turns_min[itemcount] = ini_read_real("data","turns_min",1);
	turns_max[itemcount] = ini_read_real("data","turns_max",1);

	harmful[itemcount] = stringToBool(ini_read_string_length("stats","harmful","false",IMPORT_MAX_BOOLEAN));

	script_destroy[itemcount] = asset_get_index("filter_destroy_" + string_lower(name[itemcount]));
	script_end_turn[itemcount] = asset_get_index("filter_end_turn_" + string_lower(name[itemcount]));

	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;