/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\battle\\map\\";
var testname = "";

testname = filename+file_find_first(filename+"*.ini", 0);

name[0] = "";

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] =ini_read_string_length("data","name","test",IMPORT_MAX_SIZE_ENCOUNTERMAP_NAME);

	encounter_a[itemcount,0] = "";
	encounter_b[itemcount,0] = "";
	encounter_c[itemcount,0] = "";
	
	rate_a[itemcount,0] = 1;
	rate_b[itemcount,0] = 1;
	rate_c[itemcount,0] = 1;
	
	var encountercount = 0;
	
	var i = 0;

	while(true){
		if(!ini_key_exists("data", "enc0_"+ string(i))){
			encountercount++;
			break;
	    }
		i++
	}

	for(var j = 0; i > j; j++){
	
		encounter_a[itemcount,j] = ini_read_string_length("data", "enc0_"+ string(j),"",IMPORT_MAX_SIZE_ENCOUNTERNAME);
		rate_a[itemcount,j] = ini_read_real("data", "rate0_"+ string(j),0);
	}
	
	i = 0;

	while(true){
		if(!ini_key_exists("data", "enc1_"+ string(i))){
			encountercount++;
			break;
	    }
		i++
	}

	for(var j = 0; i > j; j++){
	
		encounter_b[itemcount,j] = ini_read_string_length("data", "enc1_"+ string(j),"",IMPORT_MAX_SIZE_ENCOUNTERNAME);
		rate_b[itemcount,j] = ini_read_real("data", "rate1_"+ string(j),0);
	}
	
	
	i = 0;

	while(true){
		if(!ini_key_exists("data", "enc2_"+ string(i))){
			encountercount++;
			break;
	    }
		i++
	}


	for(var j = 0; i > j; j++){
	
		encounter_c[itemcount,j] = ini_read_string_length("data", "enc2_"+ string(j),"",IMPORT_MAX_SIZE_ENCOUNTERNAME);
		rate_c[itemcount,j] = ini_read_real("data", "rate2_"+ string(j),0);
	}

	encounter_type_count[itemcount] = encountercount;
	
	bg_sprite[itemcount] = asset_get_index("spr_battlebg_" + ini_read_string_length("data", "enc2_"+ string(j),"conif_forest",40));

	if(bg_sprite[itemcount] < -1){
		bg_sprite[itemcount] = 	spr_battlebg_conif_forest;
	}

	ini_close();
	
	itemcount++;
	testname = filename+file_find_next();
}

data_size = itemcount;

file_find_close();
