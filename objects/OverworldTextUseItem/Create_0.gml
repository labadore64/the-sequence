/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_interact = overworldTextUseItemCreateTextbox; //script to create text box
text_box_id = "test"; //textbox to look up

//item data
item_text_id = "test"; //id for dialog before using item
item_text_id_after = "item_set"; //id for dialog after using item
event_id = "teststring"; //event id for item being obtained
item_id = objdataToIndex(objdata_items,"fur pelt")//-1; //item to use
item_consume = true; //whether or not item is consumed

//after item has been obtained
text_box_id = "test"; //textbox to look up
