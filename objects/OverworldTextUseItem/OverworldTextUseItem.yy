{
    "id": "3d6a04dd-8376-44d6-89b8-10ee1efc7532",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OverworldTextUseItem",
    "eventList": [
        {
            "id": "4b08ad37-2e08-4be1-87eb-2feed45d9131",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d6a04dd-8376-44d6-89b8-10ee1efc7532"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "18265d66-17c8-4b0a-8d7f-bfbd843e46ef",
    "visible": false
}