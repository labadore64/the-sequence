/// @description Insert description here
// You can write your code in this editor
if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	if(obj_data.tablet_character[braille_id] <= -1){
		
		//if there is a character with an opening, add give
		var counter = 0;
		var store = 0;
		
		var len = ds_list_size(obj_data.party)
		for(var i = 0; i < len; i++){
			for(var j = 0; j < 3; j++){
				if(obj_data.party[|i].tablet[j] <= -1){
					store++;
				}
				if(obj_data.party[|i].tablet[j] == braille_id){
					store = 0;
					break;
				}
			}
			counter+=store;
		}
		
		if(counter > 0){
			menuAddOption("Give","Give this tablet to someone.",menuTabletsSubGiveScript)
		}
		
		menuAddOption("Swap","Swap this tablet with someone's tablets.",menuTabletSubSwitchScript)
	} else {
		menuAddOption("Take","Take this tablet back.",menuTabletsSubTakeScript)
	}
	menuAddOption("Practice","Practice this tablet.",menuTabletSubPracticeSelect)
	SHORTHAND_CANCEL_OPTION
	
	menupos = 0;
	menu_size = menuGetSize();
	
	if(menu_size == 3){
		option_y_offset = 290+30; //yoffset for options	
	}

}
braille_obj = instance_create(
					170,
					65,
					brailleCharacter);
braille_obj.cell_trans_time =0;
braille_obj.display_character = false;
braille_obj.cell_scale = 2;
brailleDataSetCharacter(braille_character,braille_obj);	

multtt = objdata_moves.damage[move_id]/maxdamage;

move_element_sprite = getTypeSprite(objdata_moves.element[move_id])

menu_ax_HUD = "tabletDetailsNoCharacter"
if(obj_data.tablet_character[braille_id] > -1){
	menu_ax_HUD = "tabletDetails"
	hudPopulateOverworldStats("character",obj_data.tablet_character[braille_id].character);
}
	
if(objdata_moves.move_type[move_id] == MOVE_TYPE_PHYSICAL){
	move_type_sprite = spr_typePhys
} else {
	move_type_sprite = spr_typeMag
}
menuParentSetTitle(string_upper(braille_character));
menuParentSetSubtitle("");

menuParentUpdateBoxDimension();

tts_title_string = title;
// Inherit the parent event
event_inherited();

menu_help = "tabletsSub"

mouseHandler_clear();
mouseHandler_menu_default()
mouseHandler_menu_setCursor();