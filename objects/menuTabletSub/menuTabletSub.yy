{
    "id": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTabletSub",
    "eventList": [
        {
            "id": "dc8ea2b4-b58c-44a9-bcb6-791c4c5f7586",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6"
        },
        {
            "id": "c4ace0ba-76ac-4546-b40c-ebd5d0cdfc39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6"
        },
        {
            "id": "6d63f7a6-befc-48ce-87b4-9bf495df13bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6"
        },
        {
            "id": "6a69ed60-5cb0-46b3-85a7-3551140aedf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6"
        },
        {
            "id": "482f5b04-a56b-4795-a541-b0185754b6e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}