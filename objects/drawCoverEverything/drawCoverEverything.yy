{
    "id": "7be87473-3521-4d4e-9412-6ef3b8346f26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "drawCoverEverything",
    "eventList": [
        {
            "id": "8233e2d1-377e-4c85-87fa-ab2a32e54bde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7be87473-3521-4d4e-9412-6ef3b8346f26"
        },
        {
            "id": "f93d6837-c32a-4976-ac9e-2cf14e3cfb44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7be87473-3521-4d4e-9412-6ef3b8346f26"
        },
        {
            "id": "b60bbd21-165d-4d90-9a07-a7ecd3957e7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7be87473-3521-4d4e-9412-6ef3b8346f26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}