/// @description Insert description here
// You can write your code in this editor
//zoom variables
initTimingAlarm();
draw_script = drawPropParentMain;

zoom_x = .25
zoom_y = .25
draw_scalex = image_xscale;
draw_scaley = image_yscale;

init_x = x;
init_y = y;

depth = y;

bounding_box[0] = x - (sprite_width*.5)
bounding_box[1] = x + (sprite_width*.5)
bounding_box[2] = y - (sprite_height*.5)
bounding_box[3] = y + (sprite_height*.5)

//does it require binoculars to be visible/to be detected
requires_binoculars = false;
prop_menu = false;