{
    "id": "8dae0454-3116-4f27-a2c1-ac11d9390c02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_water_shine",
    "eventList": [
        {
            "id": "e901a352-431b-481b-8c53-79a6787d1528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8dae0454-3116-4f27-a2c1-ac11d9390c02"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d8e32d5-4230-432e-8e5b-103b8eb255e6",
    "visible": true
}