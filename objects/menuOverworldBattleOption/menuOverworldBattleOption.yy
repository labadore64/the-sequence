{
    "id": "bef8bf46-9ff9-4ad2-a9f5-cd5bea61a71d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldBattleOption",
    "eventList": [
        {
            "id": "d1a3e53a-bdf2-4aa0-9cc0-7ba01e386d17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bef8bf46-9ff9-4ad2-a9f5-cd5bea61a71d"
        },
        {
            "id": "01553ec3-1804-4432-842a-68e6f5d9f4b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "bef8bf46-9ff9-4ad2-a9f5-cd5bea61a71d"
        },
        {
            "id": "3bb64ddf-fd8d-4db9-8f3b-7412541269db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "bef8bf46-9ff9-4ad2-a9f5-cd5bea61a71d"
        },
        {
            "id": "dd249fa0-47e1-4dab-a2d0-c12c15717445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bef8bf46-9ff9-4ad2-a9f5-cd5bea61a71d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}