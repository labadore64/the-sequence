/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(ds_exists(rotation_strings,ds_type_list)){
	ds_list_destroy(rotation_strings)	
}

if(ds_exists(rotation_values,ds_type_list)){
	ds_list_destroy(rotation_values);	
}

if(ds_exists(animation_strings,ds_type_list)){
	ds_list_destroy(animation_strings)	
}

if(ds_exists(animation_values,ds_type_list)){
	ds_list_destroy(animation_values)	
}

if(ds_exists(particle_strings,ds_type_list)){
	ds_list_destroy(particle_strings)	
}

if(ds_exists(particle_values,ds_type_list)){
	ds_list_destroy(particle_values);	
}

if(ds_exists(sfx_strings,ds_type_list)){
	ds_list_destroy(sfx_strings);	
}

if(ds_exists(sfx_values,ds_type_list)){
	ds_list_destroy(sfx_values);	
}



if(ds_exists(listsss,ds_type_list)){
	ds_list_destroy(listsss)	
}

with(menuControl){
	transparent = false;
}

with(menuControl){
	please_draw_this_frame= true
}