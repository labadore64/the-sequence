{
    "id": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mapPoint",
    "eventList": [
        {
            "id": "f7f7c2ca-b1e1-4bde-a90d-9403f84c7d8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1"
        },
        {
            "id": "76c25fd7-3f6c-4f87-871e-507d09b5e943",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1"
        },
        {
            "id": "48d24448-6acd-421f-8d02-9970d6a03c9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "edc301a4-f4d8-4a7e-a22c-6dcfe8e7ceb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "09f9e2ff-0025-46f8-85a7-eccd20b7a83f",
    "visible": false
}