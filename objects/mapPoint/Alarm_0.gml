/// @description Insert description here
// You can write your code in this editor
if(room_id == obj_data.room_for_map){
	var xx = x;
	var yy = y;
	var idd = id;
	
	with(mapController){
		x = xx;
		y = yy;
		map_you_are_here = idd;
	}
	visible = true;
	
	image_xscale = .5;
	image_yscale = .5;
	sprite_index = spr_map_current_position;
}

name = roomGetName(room_id);