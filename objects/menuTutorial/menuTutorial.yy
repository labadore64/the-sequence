{
    "id": "df5e33f1-ff77-45a5-aa56-e74dccf76540",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTutorial",
    "eventList": [
        {
            "id": "f5818a0f-a74f-478c-bc08-58726c334ec7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df5e33f1-ff77-45a5-aa56-e74dccf76540"
        },
        {
            "id": "0d39f6b0-908c-44c0-96aa-8e3ca1f57f35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "df5e33f1-ff77-45a5-aa56-e74dccf76540"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}