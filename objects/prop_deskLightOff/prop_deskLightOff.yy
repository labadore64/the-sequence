{
    "id": "a0d621ef-26db-47b6-8be8-25039b5c16fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_deskLightOff",
    "eventList": [
        {
            "id": "e0e7f24d-87d8-4c8b-9d39-d2cc137ed231",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a0d621ef-26db-47b6-8be8-25039b5c16fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0bf40f04-0038-4124-a921-2bc0b8077da7",
    "visible": true
}