{
    "id": "d3535960-1a1f-4cdc-bcf7-e03833a49b7f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleBribe",
    "eventList": [
        {
            "id": "c1052124-b79c-4a50-beba-1923574c2785",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3535960-1a1f-4cdc-bcf7-e03833a49b7f"
        },
        {
            "id": "7de5011f-b14f-4c25-876d-c72f10198778",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d3535960-1a1f-4cdc-bcf7-e03833a49b7f"
        },
        {
            "id": "22951613-e013-4c2a-b17a-0f999ac2ee96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "d3535960-1a1f-4cdc-bcf7-e03833a49b7f"
        },
        {
            "id": "7e3b0da6-7e23-49df-bafc-054889d6c41d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3535960-1a1f-4cdc-bcf7-e03833a49b7f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}