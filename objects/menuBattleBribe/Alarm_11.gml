/// @description Insert description here
// You can write your code in this editor
tts_title_string = "$" + string(bribe_amount)
// Inherit the parent event
event_inherited();

hudControllerClearData();
var tess = noone;
var noooo = "";

with(menuBattleTarget){
	tess = party[|menupos];	
	noooo = character.flavor_text[flavor_which];
}

hudPopulateBattle("enemy",tess,false);
hudControllerAddData("bribe." + "amount",string(bribe_amount));
hudControllerAddData("bribe." + "money",string(money));
hudControllerAddData("enemy.desc",noooo);


with(menuControl){
	please_draw_all_visible = true;	
}