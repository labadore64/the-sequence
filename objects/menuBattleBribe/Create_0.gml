/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_draw = menuBattleBribeDraw;

transparent = true;

//drawOtherMenus = false;
draw_bg = false;

money = BattleHandler.money

var bribe_totalz = 0;
var bribe_amountz = BattleHandler.money
		//add all bribes of inputs together.
with(battleAttackInput){
	bribe_totalz+= bribe;	
}
		
bribe_amountz -= bribe_totalz;

bribe_max = bribe_amountz

bribe_amount = (floor((bribe_max*.2)*.1))*10;

if(bribe_amount == 0 && bribe_max >= 10){
	bribe_amount = 10;
}

menu_up = menuBattleBribeDown
menu_down = menuBattleBribeUp
menu_left = menuBattleBribeLeft
menu_right = menuBattleBribeRight

menu_select = menuBattleBribeSelect;

with(menuParent){
	visible = false;	
}

with(menuBattleMoves){
	do_not_draw_me = true
}

with(menuControl){
	please_draw_all_visible = true;	
}


visible = true;

menu_ax_HUD = "battleTargetBribe";