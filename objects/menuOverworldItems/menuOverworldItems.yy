{
    "id": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldItems",
    "eventList": [
        {
            "id": "60e738e4-3476-44a6-bf01-372debaaf7bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        },
        {
            "id": "64e0bc01-157f-49cb-8c42-b4dcfa22d6b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        },
        {
            "id": "8c5f65e0-2737-4234-b1dc-afd34a0c6102",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        },
        {
            "id": "e376bc03-5892-4ac8-a6bb-ce2ce401f472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        },
        {
            "id": "2a435fa9-9153-453c-b178-9f4158cf33a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        },
        {
            "id": "f8ca79bb-d84b-4ed8-b2d7-05d6b865a6ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        },
        {
            "id": "8fa163ab-6160-444b-8c60-a19807421be9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}