/// @description Insert description here
// You can write your code in this editor

//blur
shader_enabled = true;
uni_resolution_hoz = shader_get_uniform(shd_gaussian_horizontal1,"resolution");
uni_resolution_vert = shader_get_uniform(shd_gaussian_vertical1,"resolution");
var_resolution_x = 900;
var_resolution_y = 900;

uni_blur_amount_hoz = shader_get_uniform(shd_gaussian_vertical1,"blur_amount");
uni_blur_amount_vert = shader_get_uniform(shd_gaussian_horizontal1,"blur_amount");
var_blur_amount = 2

u_uv_x = global.letterbox_w;
u_uv_y = global.letterbox_h;
var_resolution_x = 900*global.scale_factor;
var_resolution_y = 900*global.scale_factor;

counter = 0

shd_vcr_counter = shader_get_uniform(shd_overworld,"counter");

size1 = 174
size2 = 327

final_surface = -1

surf = -1
