/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

with(menuCharacterSwitchParty){
	var chara = obj_data.party[|menupos].character
	char_color = obj_data.party[|menupos].color
	portrait.character = chara;
	menuOverworldPartyUpdateStatsPortrait(obj_data.party[|menupos],100,drawstat);
	with(portrait){
		surface_free(surface)	
		event_perform(ev_alarm,0)
	}	
}
newalarm[2] = 1;
with(menuCharacterSwitchRecruit){
	//recruit_list 
	var chara = recruit_list[|menupos].character
	char_color = recruit_list[|menupos].color
	portrait.character = chara;
	menuOverworldPartyUpdateStatsPortrait(recruit_list[|menupos],100,drawstat);
	with(portrait){
		surface_free(surface)	
		event_perform(ev_alarm,0)
	}	
}

chaa = -1;