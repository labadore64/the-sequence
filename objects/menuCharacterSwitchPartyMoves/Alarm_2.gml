/// @description Insert description here
// You can write your code in this editor

ds_list_clear(physical_moves);
ds_list_clear(magic_moves);

var sizer = ds_list_size(chaa.moves);

var mymove = -1;

for(var i = 0; i < sizer; i++){
	mymove = chaa.moves[|i];
	
	if(!is_undefined(mymove)){
		if(mymove > -1){
			moveData(mymove);
			if(move_type == MOVE_TYPE_PHYSICAL){
				ds_list_add(physical_moves,mymove);	
			} else {
				ds_list_add(magic_moves,mymove);	
			}
		}
	}
}

menuOverworldPartyMovesSetMoveList();


menuOverworldPartyMovesGetMoveData();
