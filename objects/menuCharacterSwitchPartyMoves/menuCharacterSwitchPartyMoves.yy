{
    "id": "66233d23-990e-4675-8a21-42c232a2afd4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSwitchPartyMoves",
    "eventList": [
        {
            "id": "f9eca61d-ed13-4ae4-886f-c4e6e6fbd383",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "66233d23-990e-4675-8a21-42c232a2afd4"
        },
        {
            "id": "9fdac235-c3d2-438c-a1f3-e9ef0018a948",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "66233d23-990e-4675-8a21-42c232a2afd4"
        },
        {
            "id": "b6084565-69c7-42c7-93fb-7f36501e38bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "66233d23-990e-4675-8a21-42c232a2afd4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "28429074-a47a-4172-a1ba-d340aa45251b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}