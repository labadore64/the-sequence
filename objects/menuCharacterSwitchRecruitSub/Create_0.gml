/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items

var namer = "";

with(menuCharacterSwitchRecruit){
	namer = recruit_list[|menupos].name
}

menuAddOption("Switch","Switch " + namer+" with someone else.",menuCharacterSwitchPartyConfirmScript);
//menuAddOption("Sell","View and sell your inventory.",-1)
menuAddOption("Moves","View "+namer+"'s moves.",menuCharacterSwitchPartySubMovesScript)
menuAddOption("Items","View "+namer+"'s items.",menuCharacterSwitchPartySubItemsScript)
menuAddOption(global.langini[0,7],global.langini[1,7],menu_cancel)