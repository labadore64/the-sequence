/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
// how many seconds to count down
seconds = 10;
newalarm[0] = 1;

time_start = 0;
time_end = 0;
time_counter = 0;
last_time = 0;
text_size = 3;

timer_ratio = 0;
timer_start_ratio = 0;

time_sound[0] = sound_ax_clock_tick;
time_sound[1] = sound_ax_clock_tock;
time_sound_index = 0;

time_text[0] = "";
time_text[1] = "";
time_text[2] = "";
time_text[3] = "";
time_text[4] = "";

active = false;
minutes = 0;