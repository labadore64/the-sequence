/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();

if(active){
	var curtime = current_time;
	
	if(!instance_exists(BrailleQuitGame) && !menuControl.help_open){
		time_counter-= curtime - last_time;
		
		timer_ratio = 1 - (time_counter/timer_start_ratio);
		
		if(time_counter < 0){
			instance_destroy();	
		} else {
			var timelord = time_counter;
			time_text[0] = string_format(floor(timelord / 60000),2,0);
			time_text[1] = ":";
			time_text[2] = string_format((floor(timelord / 1000) % 60),2,0);
			time_text[3] = ":";
			time_text[4] = string_format((timelord % 1000),3,0);
		
			for(var i = 0 ;i < 5; i ++){
				time_text[i] = string_replace_all(time_text[i], " ","0");	
			}
		}
	}
	last_time = curtime;
	
	if(global.countdownSound){
		minutes = floor(time_counter*0.001);
		if(minutes != lastminutes){
			time_sound_index++;
		
			if(time_sound_index == 5){
				time_sound_index = 0;	
				var sound = soundfxPlay(time_sound[1]);
				audio_sound_pitch(sound,1+timer_ratio*.5)
			} else {
				var sound = soundfxPlay(time_sound[0]);
				audio_sound_pitch(sound,1+timer_ratio*.5)
			}
		}
	
		lastminutes = minutes;
	}
}