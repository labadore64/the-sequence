/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();
blink_counter--;
if(blink_counter == 0){
	blink_counter = irandom_range(blink_min,blink_max);	
}

drawPortraitUpdateSprite();

drawPortraitSpriteEmotion();

if(ScaleManager.updated || update_surf){
	
	image_xscale = global.scale_factor*x_scalefactor;
	image_yscale = global.scale_factor*y_scalefactor;
	
	x = 256*global.scale_factor*x_scalefactor;
	y = 256*global.scale_factor*y_scalefactor;
}