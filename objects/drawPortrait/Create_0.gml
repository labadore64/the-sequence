/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
character = 0; //what character is represented in the portrait
emotion = EMOTION_NEUTRAL //what emotion

//sprites for emotions
sprite_neutral = spr_portrait_leon;

state = PORTRAIT_WAIT_NO_BLINK;

blink_min = 300;
blink_max = 2000;
blink_counter = irandom_range(blink_min,blink_max);

surface = -1;

draw_script = drawPortraitDrawMain;

text_box = -1; //if the portrait is connected to a text box.

x = 256*global.scale_factor;
y = 256*global.scale_factor;
sprite_index = sprite_neutral;

draw_this_frame = false;

newalarm[0] = 1;

image_xscale = global.scale_factor;
image_yscale = global.scale_factor;

x_scalefactor = 1;
y_scalefactor = 1;

update_surf = false;