{
    "id": "543db632-e4a8-492c-907f-68bd8f1d032a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "drawPortrait",
    "eventList": [
        {
            "id": "7e03cbb4-a18f-4653-855c-1582839d9937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "543db632-e4a8-492c-907f-68bd8f1d032a"
        },
        {
            "id": "f1bf22f4-a21f-467c-9078-cac628c27579",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "543db632-e4a8-492c-907f-68bd8f1d032a"
        },
        {
            "id": "38169758-6023-4ed3-9c31-55b759ea6ef5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "543db632-e4a8-492c-907f-68bd8f1d032a"
        },
        {
            "id": "06a06821-0899-482f-91bc-ade332d71cb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "543db632-e4a8-492c-907f-68bd8f1d032a"
        },
        {
            "id": "d3875e08-05b1-4dec-9fca-bd8f88375e73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "543db632-e4a8-492c-907f-68bd8f1d032a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}