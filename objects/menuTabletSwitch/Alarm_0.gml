/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event

event_inherited();

menu_ax_HUD = "tabletStatInfo"
infokeys_assign(menu_ax_HUD);

ds_list_clear(cando_list);

repeat(menu_size){
	ds_list_add(cando_list,true);
}

for(var i = 0; i < menu_size; i++){
	menu_script[|i] = menuTabletsSwitchSelectScript;	
}

menuParentSetTitle("Give " + string_upper(braille_character) + "\nto whom?");
menuParentSetSubtitle("");

tts_title_string = title;

tts_clear();
if(tts_title_string != ""){
	tts_say(tts_title_string)	
}