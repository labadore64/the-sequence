{
    "id": "d50f37d4-9c32-454a-b4b8-9ca14917abaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTabletSwitch",
    "eventList": [
        {
            "id": "ec28b66c-b9cf-4530-84d7-63ce4b947212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d50f37d4-9c32-454a-b4b8-9ca14917abaf"
        },
        {
            "id": "a81f2e05-336c-42a2-9e81-cb9047835ffe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d50f37d4-9c32-454a-b4b8-9ca14917abaf"
        },
        {
            "id": "edf9af43-4346-441f-becd-c39121a8e4c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d50f37d4-9c32-454a-b4b8-9ca14917abaf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00e8f880-ea72-49e5-8a60-2de5525111a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}