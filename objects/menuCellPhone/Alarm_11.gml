/// @description Insert description here
// You can write your code in this editor
tts_title_string = "Cell Phone"
// Inherit the parent event
event_inherited();

menu_icon[0] = -1
menu_icon[1] = -1
menu_icon[2] = -1

voice_new = false;
task_new = false;
email_new = false;

// calculate menu icons
var cando = false;

for(var i = 0; i < objdata_voicemail.data_size; i++){
	if(objdata_voicemail.state[i] == EMAIL_STATE_UNREAD){
		cando = true;
		break;
	}
}

if(cando){
	for(var i = 0; i < 3; i++){
		if(menu_icon[i] == -1){
			menu_icon[i] = spr_cellphone_comment;
			voice_new = true;
			break;
		}
	}
}

cando = false;
for(var i = 0; i < objdata_task.data_size; i++){
	if(objdata_task.unread[i]){
		cando = true;
		break;
	}
}

if(cando){
	for(var i = 0; i < 3; i++){
		if(menu_icon[i] == -1){
			menu_icon[i] = spr_cellphone_task;
			task_new = true
			break;
		}
	}
}

cando = false;
for(var i = 0; i < objdata_email.data_size; i++){
	if(objdata_email.state[i] == EMAIL_STATE_UNREAD){
		cando = true;
		break;
	}
}

if(cando){
	for(var i = 0; i < 3; i++){
		if(menu_icon[i] == -1){
			menu_icon[i] = spr_cellphone_email;
			email_new = true;
			break;
		}
	}
}

if(translation > 0){
	active = false;	
}

mouseHandler_menu_default()

with(menuOverworldMenus){
	mouseHandler_clear_main()
	mouseHandler_add(0,550,200,600,menuOverworldMenuTabLeft,"Items")
	mouseHandler_add(600,550,800,600,menuOverworldMenuTabRight,"Braille")
}