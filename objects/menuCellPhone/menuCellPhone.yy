{
    "id": "046bd35c-a84c-41f1-9013-23b895d00065",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCellPhone",
    "eventList": [
        {
            "id": "182f469e-9a61-46bd-a2ac-3b60b01e0467",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "046bd35c-a84c-41f1-9013-23b895d00065"
        },
        {
            "id": "55d9772c-2387-4270-bd6f-cc52b0e39892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "046bd35c-a84c-41f1-9013-23b895d00065"
        },
        {
            "id": "fe55b045-414b-49cb-b82b-35d1a7c19eb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "046bd35c-a84c-41f1-9013-23b895d00065"
        },
        {
            "id": "f26e0b35-7b8f-4df7-b1e7-18f86e7289ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "046bd35c-a84c-41f1-9013-23b895d00065"
        },
        {
            "id": "57b09c1b-8d7f-4435-81b2-55458370697d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "046bd35c-a84c-41f1-9013-23b895d00065"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}