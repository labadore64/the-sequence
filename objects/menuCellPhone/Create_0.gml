/// @description Insert description here
// You can write your code in this editor

//debugging stuff. remove later

menu_slideInit();

translation = 150
translation_rate_start = 75;
translation_decay_rate = .65

alpha_translation = 1;
alpha_translation_rate = .22;
alpha_delay = 3;

// Inherit the parent event
event_inherited();

// menu icons

menu_icon[0] = -1
menu_icon[1] = -1
menu_icon[2] = -1

voice_new = false;
task_new = false;
email_new = false;

// calculate menu icons
var cando = false;

for(var i = 0; i < objdata_voicemail.data_size; i++){
	if(objdata_voicemail.state[i] == EMAIL_STATE_UNREAD){
		cando = true;
		break;
	}
}

if(cando){
	for(var i = 0; i < 3; i++){
		if(menu_icon[i] == -1){
			menu_icon[i] = spr_cellphone_comment;
			voice_new = true;
			break;
		}
	}
}

cando = false;
for(var i = 0; i < objdata_task.data_size; i++){
	if(objdata_task.unread[i]){
		cando = true;
		break;
	}
}

if(cando){
	for(var i = 0; i < 3; i++){
		if(menu_icon[i] == -1){
			menu_icon[i] = spr_cellphone_task;
			task_new = true
			break;
		}
	}
}

cando = false;
for(var i = 0; i < objdata_email.data_size; i++){
	if(objdata_email.state[i] == EMAIL_STATE_UNREAD){
		cando = true;
		break;
	}
}


if(cando){
	for(var i = 0; i < 3; i++){
		if(menu_icon[i] == -1){
			menu_icon[i] = spr_cellphone_email;
			email_new = true;
			break;
		}
	}
}

// how many bars
cell_signal = global.signal_strength

// menu options

x= 400;
y = 325;

menu_background_color = c_black
gradient_height = 0; //height of the title
menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430)
option_y_offset = 90
title_y_offset = 0
option_x_offset = 20;
select_percent = .5

option_space = 60;
text_size =3

menuParentUpdateBoxDimension()

menu_draw = cellPhoneDrawMenu

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

draw_bg = false;
with(menuControl){
	please_draw_black = true;	
}

// options

menu_img[0] = spr_cellphone_phone
menu_img[1] = spr_cellphone_task
menu_img[2] = spr_cellphone_email
menu_img[3] = -1;
menu_img[4] = -1;

menuAddOption("Call","Call a friend.",cellPhoneCallMenu)
menuAddOption("Tasks","View tasks.",cellPhoneTaskMenu)
menuAddOption("Email","View Email.",cellPhoneEmailMenu)
menuAddOption("Stats","View game stats.",cellPhoneStatsMenu)

menu_size = menuGetSize();

menu_cancel = menuOverworldMenusCancel;

newalarm[3] = 1;

menu_left = -1;
menu_right = -1;

textboxMenuTutorial("tutorial_menuE")
menu_help = "phone"

mousepos_x = -130