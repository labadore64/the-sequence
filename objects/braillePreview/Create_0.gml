/// @description Insert description here
// You can write your code in this editor
event_inherited();
count = 5
row_length = 5

display_character = true
skip_spaces = true;

init_string = "";
add_string = "";

right_insert_string = "Right Insert";
left_insert_string = "Left Insert";

display_string = init_string;
preview_string = "";
display_color = c_white;
display_alpha = 1;

// how many cycles to wait until transitioning to the preview
wait_to_transition = 90;

// how many cycles to wait until resetting
wait_to_reset = 300;

// whether or not to detect words in the string.
detect_words = true
word_move = -1;
detected_word = "";
detected_length = 0;
for(var i = 0; i < 5; i++){
	preview_word[i] = false;
}

// whether inserting from the right side
directional = true;

sprite_counter =0;