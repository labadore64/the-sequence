{
    "id": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "braillePreview",
    "eventList": [
        {
            "id": "e530ee1d-5fe7-45b0-b778-c3aa4122b23f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        },
        {
            "id": "da67ba8a-e41f-4a05-a617-8573d57ba770",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        },
        {
            "id": "a00861d1-d0e5-4467-8e99-c7f475019101",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        },
        {
            "id": "0d6ff2b5-8eb0-463c-99d8-777cd61e2156",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        },
        {
            "id": "3fe0eb5d-8a6b-4173-95d1-e24688187986",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        },
        {
            "id": "03bed712-e8d0-47c6-af16-dfd330472640",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        },
        {
            "id": "9a8a8475-6716-40c3-9a25-8fb6a2dc0d8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5cab9cdf-2b4c-44f2-824a-029d73cc09fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "19943076-b550-4d21-9f00-14427af681b4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}