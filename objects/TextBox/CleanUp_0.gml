/// @description Insert description here
// You can write your code in this editor

visible = false;
if(!ignore_clean_script){
	if(clean_script != -1){
		script_execute(clean_script);	
	}
}
if(!destroyed){
	menuControlPop();
	mouseHandler_clear_main()
	audio_group_unload(audiogroup_character_voice)
}
ds_list_destroy(menu_name);
ds_list_destroy(menu_description);
ds_list_destroy(menu_script);

with(Player){
	 click_delay = 5;	
}

tts_stop()

/*
if(instance_number(menuParent) == 1){
	with(CollisionDetector){
		if(object_index == CollisionDetector){
			audio_resume_sound(sound_emit.audio_sound)
		}
	}
}
*/

if(!instance_exists(menuParent)){
	with(AudioEmitter){
		if(audio_sound > -1){
			audio_resume_sound(audio_sound);	
		}
	}
}

// put textbox shit here

// executes any scripts in the text box
if(ds_exists(scripts,ds_type_map)){
	if(!ds_map_empty(scripts)){
		
		var size, i;
		size = ds_list_size(text);
		for (i = 0; i < size; i++;)
		{
			//if the scriptval is positive
			var scriptval = scripts[? i];
			if(!is_undefined(scriptval)){
				scriptval = real(scriptval);
			
				if(script_exists(scriptval)){
					script_execute(scriptval,arguments);	
				}
			}
		
			//if the scriptval is negative
			var scriptval = scripts[? (i-size)];
			if(!is_undefined(scriptval)){
				scriptval = real(scriptval);
			
				if(script_exists(scriptval)){
					script_execute(scriptval,arguments);	
				}
			}
	
		}	
		
	}
}

if(event_id != ""){
	with(parent){
		instance_destroy();	
	}
}

//if(event_id != ""){
	with(parent_lock){
		lock = false;
	}
//}

textboxClean();

if(ds_exists(draw_map,ds_type_map)){
	ds_map_destroy(draw_map);
}


