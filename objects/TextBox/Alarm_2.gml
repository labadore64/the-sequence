/// @description Insert description here
// You can write your code in this editor

with(AudioEmitter){
	if(audio_sound > -1){
		audio_pause_sound(audio_sound);	
	}
}


if(draw_portrait){
	portrait = instance_create_depth(0,0,0,drawPortrait)
	portrait.character = character	
}

textboxGetVoice(character);

if(string_pos(".ini",filename_id) == 0){
	textboxStringFromFile(filename_id + text_id + ".ini")
} else {
	textboxStringFromFile(filename_id)
}
if(!destroyed){
	active = true;
	audio_group_load(audiogroup_character_voice)
	textboxSetCurrentText();
	if(tts_speak){
		tts_clear();
		tts_say(current_text);
	}
	if(delay_display = 0){
		display_object = textboxDialogCreate(text,portrait,x_align)
		display_object.lineEnd = char_per_line
		display_object.text_sound1 = text_sound1;
		display_object.text_sound2 = text_sound2;
		display_object.text_sound3 = text_sound3;
		display_object.text_sound4 = text_sound4;
		display_object.text_mod_sound = text_mod_sound;
		display_object.pitch_base = pitch_base;
		textboxExecuteScript();
		with(DialogHandler){
			dontdrawportrait = true;	
		}
	
		var tester = draw_map[? display_object.message_current];
		if(!is_undefined(tester)){
			menuTutorialLoadGraphics(tester);
		}
	} else {
		newalarm[3] = delay_display;	
	}

	if(!is_monster){
		wait = false	
	}
	menuParentPushToStack();

	with(menuControl){
		please_draw_all_visible = true;	
	}
}