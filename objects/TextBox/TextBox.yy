{
    "id": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TextBox",
    "eventList": [
        {
            "id": "82191808-704b-4f2a-b9be-bbe18c405d69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5"
        },
        {
            "id": "f9746d15-027a-4d02-8a6e-68dd4541b8aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5"
        },
        {
            "id": "cf3df244-0939-43c3-bebc-6a6738c67c34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5"
        },
        {
            "id": "68cbb80a-ce05-436b-beec-4aa901c23de6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5"
        },
        {
            "id": "a98926e3-2f25-44cc-85c6-8e27e97b02e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5"
        },
        {
            "id": "3be76e7c-2df6-46b2-8493-73fd298be36a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}