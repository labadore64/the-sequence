{
    "id": "7d9861d4-e033-4757-80ac-d328ac424c37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_LED_effect",
    "eventList": [
        {
            "id": "71b3287d-118c-47c8-97eb-7647a63b2ac7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d9861d4-e033-4757-80ac-d328ac424c37"
        },
        {
            "id": "8e893327-12f8-4a9c-8986-a0eea299f6b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d9861d4-e033-4757-80ac-d328ac424c37"
        },
        {
            "id": "f0f87ce2-16f2-4e40-b454-4be0e46693a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7d9861d4-e033-4757-80ac-d328ac424c37"
        },
        {
            "id": "587f3271-1dbe-4395-8521-e01e7ea0ca94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "7d9861d4-e033-4757-80ac-d328ac424c37"
        },
        {
            "id": "433b685b-598b-489e-8b70-2cd22ba85df4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "7d9861d4-e033-4757-80ac-d328ac424c37"
        },
        {
            "id": "fbfa8fa6-b149-46dd-9608-3b1e1c8dc736",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "7d9861d4-e033-4757-80ac-d328ac424c37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}