/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// menu options

x= 400;
y = 325-60;

menu_background_color = c_black
gradient_height = 0 //height of the title
menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430-120)
option_y_offset = 85
title_y_offset = 0
option_x_offset = 20;
select_percent = 1
animation_len = 1

menuParentUpdateBoxDimension()

menu_draw = menuTaskDraw

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

draw_bg = false;

current_sprite = -1
current_desc = "";

// create the list of tasks
task_list = ds_list_create();

for(var i = 0; i < objdata_task.data_size; i++){
	if(taskActive(i)){
		ds_list_add(task_list,i);	
	}
}

menu_select = menuTaskReadDesc;

if(ds_list_empty(task_list)){
	menuAddOption("No tasks...","",-1)
} else {
	var sizer = ds_list_size(task_list);
	var obj;
	for(var i = 0; i < sizer; i++){
		obj = task_list[|i];
		if(!is_undefined(obj)){
			menuAddOption(objdata_task.title[obj],"",-1)
		}
	}
}

menu_size = menuGetSize();
skip = 5

menu_up = menuTaskMoveUp
menu_down = menuTaskMoveDown

menu_left = menuParentMoveLeft
menu_right = menuParentMoveRight
wrap = false;

menuTaskUpdateDesc();

ignore_tab = true