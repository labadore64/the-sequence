{
    "id": "e930f8a8-d300-42d5-b749-293dfe88aaf6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTasks",
    "eventList": [
        {
            "id": "4fb341c0-1033-4f57-8526-8017eac2fe39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e930f8a8-d300-42d5-b749-293dfe88aaf6"
        },
        {
            "id": "fa24b574-a9ea-4ca0-a066-699a955b5db1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e930f8a8-d300-42d5-b749-293dfe88aaf6"
        },
        {
            "id": "92d03fa7-a76e-46db-982b-ec2f4d8f33ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e930f8a8-d300-42d5-b749-293dfe88aaf6"
        },
        {
            "id": "a0119015-5a15-4d77-b77b-a61d9aabe137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "e930f8a8-d300-42d5-b749-293dfe88aaf6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}