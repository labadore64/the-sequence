/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
party_size = ds_list_size(obj_data.party)

// menu options

x= 400;
y = 325+50

menu_background_color = c_black
gradient_height = 0 //height of the title
menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430+60)
option_y_offset = 85
title_y_offset = 0
option_x_offset = 20;
select_percent = 1
animation_len = 1
option_space = 120
text_size = 3;

menuParentUpdateBoxDimension()

menu_draw = menuCallDraw

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

draw_bg = false;

current_desc = "";

// create the list of Calls
call_list = ds_list_create();

menuCallSetCalls();

skip = 5

menu_up = menuCallMoveUp
menu_down = menuCallMoveDown

menu_left = menuParentMoveLeft
menu_right = menuParentMoveRight
wrap = false;

selected_call = 0;

menuCallUpdateDesc();

menu_help = "phonecall"

ignore_tab = true