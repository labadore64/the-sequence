{
    "id": "8bc959fb-2b84-469e-857b-795e8e2a5bed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCallMain",
    "eventList": [
        {
            "id": "a6bbe574-44bc-43b2-b478-d1bdac2c1112",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bc959fb-2b84-469e-857b-795e8e2a5bed"
        },
        {
            "id": "90cbfa97-cfe3-4839-92ca-a9bbe7385766",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8bc959fb-2b84-469e-857b-795e8e2a5bed"
        },
        {
            "id": "668ac8e1-9aab-4c89-9f84-ef2f8238520c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bc959fb-2b84-469e-857b-795e8e2a5bed"
        },
        {
            "id": "1605c141-2715-4ab6-83d0-ba810488ac7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "8bc959fb-2b84-469e-857b-795e8e2a5bed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}