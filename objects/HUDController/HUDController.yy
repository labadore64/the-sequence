{
    "id": "8a666f96-ec65-4df7-93b7-145dfa53c068",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "HUDController",
    "eventList": [
        {
            "id": "fbf61d4d-0c6b-4ec0-bdde-4eb436420461",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a666f96-ec65-4df7-93b7-145dfa53c068"
        },
        {
            "id": "3edcee15-6a6f-4389-841d-1a2968558373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8a666f96-ec65-4df7-93b7-145dfa53c068"
        },
        {
            "id": "9175065d-067c-49cd-b62d-646f448e73c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8a666f96-ec65-4df7-93b7-145dfa53c068"
        },
        {
            "id": "7e4ff2ae-528a-4367-adf2-dd71247bc470",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8a666f96-ec65-4df7-93b7-145dfa53c068"
        },
        {
            "id": "05636cb0-7069-43da-b718-3363104c660a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a666f96-ec65-4df7-93b7-145dfa53c068"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}