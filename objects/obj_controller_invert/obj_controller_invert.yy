{
    "id": "2753d0c0-260b-4130-a208-8db818de346c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_invert",
    "eventList": [
        {
            "id": "109516ad-0514-4477-8c61-6b02596f8211",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2753d0c0-260b-4130-a208-8db818de346c"
        },
        {
            "id": "da8cb895-e47d-4c02-8213-7b9c45b83819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2753d0c0-260b-4130-a208-8db818de346c"
        },
        {
            "id": "3eabaa4c-41df-4ea6-a737-5d53965e9869",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "2753d0c0-260b-4130-a208-8db818de346c"
        },
        {
            "id": "292d3ca4-1b62-412d-82c7-bf8b5319d628",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "2753d0c0-260b-4130-a208-8db818de346c"
        },
        {
            "id": "7813a8ea-0a4b-46e7-ad1e-5b2f87d8eb23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "2753d0c0-260b-4130-a208-8db818de346c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}