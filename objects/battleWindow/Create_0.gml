/// @description Insert description here
// You can write your code in this editor
event_inherited();

menuControlForceDrawMenuBackground();
windowText = "With Kisara captured, Aknadin heard of the Dragon's power and hoped to transfer the ka to his son Seto, unconcerned that this would mean killing its current host.";
windowCharPerLine = 20;

x = 400;
y = 300;

menuParentSetBoxHeight(300)
menuParentSetBoxWidth(400);

menu_select = menuParentCancel;

sound_cancel = soundMenuDefaultMove;

menuParentUpdateBoxDimension();

menu_draw = battleWindowDraw;

drawGradient = false

newalarm[2] = 1

menu_up = -1;
menu_down = -1;
menu_left = -1;
menu_right = -1;

locked_player = false

mouseHandler_clear()