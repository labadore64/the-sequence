{
    "id": "25e7d0b9-5b73-44e9-8b7f-f4c10a05f9de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleWindow",
    "eventList": [
        {
            "id": "08dd2f71-eaff-4ea1-8569-2c25feacb64e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25e7d0b9-5b73-44e9-8b7f-f4c10a05f9de"
        },
        {
            "id": "13f67b10-7236-4423-bf1b-9bb14175ac2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "25e7d0b9-5b73-44e9-8b7f-f4c10a05f9de"
        },
        {
            "id": "6224eee6-2746-4b4f-8c9a-22c36b80573a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "25e7d0b9-5b73-44e9-8b7f-f4c10a05f9de"
        },
        {
            "id": "95b49dd0-7a07-4d32-80ec-7366efdd62c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "25e7d0b9-5b73-44e9-8b7f-f4c10a05f9de"
        },
        {
            "id": "471a2322-82fd-4ef8-881f-391a6e68ec90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "25e7d0b9-5b73-44e9-8b7f-f4c10a05f9de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}