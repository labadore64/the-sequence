{
    "id": "1f495660-41c7-4508-bfb2-0d697717acf0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_bird_coot",
    "eventList": [
        {
            "id": "63288b97-73c7-452a-88fd-3a882b73e5ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f495660-41c7-4508-bfb2-0d697717acf0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f668bad7-213f-4b60-8d95-626e6b699bdc",
    "visible": true
}