/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

name = "American Coot"

if(choose(true,false)){
	sprite_idle = spr_bird_coot_idle;
	sprite_fly = spr_bird_coot_idle;
} else {
	sprite_idle = spr_bird_coot_idle;
	sprite_fly = spr_bird_coot_idle;
}


fly_distance = -1 //distance that bird will fly away. -1 means it won't fly away.

guide_distance = 200; //distance you need to be to be able to register in bird guide.

fly_speed = 7;

audio_sound = -1

destroy_chance = .95;

