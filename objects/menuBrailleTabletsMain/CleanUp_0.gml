/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(!instance_exists(menuOverworldMenus)){
	with(brailleGameBackground){
		instance_destroy();	
	}
}

with(menuOverworldMain){
	visible = true
}

with(Player){
	overworld_item_wait_trigger = false;
	newalarm[3] = 1;
}