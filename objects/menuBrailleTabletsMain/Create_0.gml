/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

info_height = 485-20; //height of the info box

if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	menuParentSetTitle(global.langini[2,3])
	menuParentSetSubtitle(global.langini[3,3])

	menuAddOption(global.langini[0,4],global.langini[1,4],menuBrailleTabletsMainTablets)
	menuAddOption(global.langini[0,5],global.langini[1,5],menuBrailleTabletsMainSigns)
	menuAddOption(global.langini[0,6],global.langini[1,6],menuBrailleTabletsMainBraille)

}
title_x_offset = 300+25; //xoffset for title
draw_width = 650; //width of box in pixels

menu_size = menuGetSize();
menuParentUpdateBoxDimension();

menu_help=""

spr_img[0] = spr_mode_tablets
spr_img[1] = -1
spr_img[2] = spr_mode_review;
spr_img[3] = -1;

menu_help = "brailleandtablets"


// do fade in shit
animation_start = 1 //animation length

animation_len = animation_start;

newalarm[3] = 1
newalarm[11] = animation_len

draw_bg = false;

active = false;

image_speed = 1.5

menu_cancel = menuOverworldMenusCancel;

textboxMenuTutorial("tutorial_menuF")

with(menuControl){
	please_draw_black = true;	
}

menu_left = -1;
menu_right = -1;

mousepos_x = -220