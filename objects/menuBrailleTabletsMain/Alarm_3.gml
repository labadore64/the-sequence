/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
fadeOut(10,c_black)

with(screenFade){
	script_run = false;	
}

if(!instance_exists(brailleGameBackground)){
	background_obj = instance_create(0,0,brailleGameBackground);
} else {
	var idd = noone;
	with(brailleGameBackground){
		idd = id;	
	}
	background_obj = idd;
}