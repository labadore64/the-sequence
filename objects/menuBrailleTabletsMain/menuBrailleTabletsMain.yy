{
    "id": "e79895fe-996c-4112-9220-a86d23d4402c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBrailleTabletsMain",
    "eventList": [
        {
            "id": "f3598034-651f-40c1-bdc9-6ed9e0f84435",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e79895fe-996c-4112-9220-a86d23d4402c"
        },
        {
            "id": "0f79d987-22b5-48cc-bfcc-44b849266f99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e79895fe-996c-4112-9220-a86d23d4402c"
        },
        {
            "id": "de960804-8b51-4c7a-97a5-cc6d3a3ead80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "e79895fe-996c-4112-9220-a86d23d4402c"
        },
        {
            "id": "9bfc3908-6555-4a95-a317-072579cbf963",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e79895fe-996c-4112-9220-a86d23d4402c"
        },
        {
            "id": "acb94394-8a50-498a-8bfa-be1b739cc678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "e79895fe-996c-4112-9220-a86d23d4402c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b0ea70d2-d9e2-4417-8483-c36a3f1713d9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}