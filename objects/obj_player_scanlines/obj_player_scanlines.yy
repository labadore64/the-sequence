{
    "id": "b8ade4d4-7139-4cb5-9d5e-8b18d89bab29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_scanlines",
    "eventList": [
        {
            "id": "bd9113ff-d6b6-4205-8991-7269b11ea39a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8ade4d4-7139-4cb5-9d5e-8b18d89bab29"
        },
        {
            "id": "f0342c07-4af1-40c6-b906-e587dd9d4615",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b8ade4d4-7139-4cb5-9d5e-8b18d89bab29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "57f4b287-eb64-468d-807e-5d5458d12ea2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "03b0b29b-33bb-40d9-8286-a4688ec095e6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}