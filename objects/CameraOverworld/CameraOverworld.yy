{
    "id": "2ee9faf8-9044-436d-9cb8-6acd30784a8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CameraOverworld",
    "eventList": [
        {
            "id": "3166871f-8a11-4011-b76c-db64448d9fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "bf2e29d5-5d51-4d97-af0d-f2fc63e94095",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "5ff94022-1396-42bc-a738-2d9e304c450d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "adcd91ca-fc30-480c-8134-a8528c42dd39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "561ec431-4bea-400a-bba0-112f879d6559",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "b2011778-279d-46f5-b60e-afa72d22e6b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 100,
            "eventtype": 5,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "42733ddb-f741-43ce-b1ba-266f5f119775",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 102,
            "eventtype": 5,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "2e95dddb-7c3a-496d-8f0c-458f195e27ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 103,
            "eventtype": 5,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "f6942ad7-712c-4912-8460-d9a1720ddd26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 105,
            "eventtype": 5,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "a5a35dde-9c10-4585-bd63-42dcf549d043",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        },
        {
            "id": "1f7de966-4fcd-41ef-aed0-2519df19a31a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "2ee9faf8-9044-436d-9cb8-6acd30784a8a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}