/// @description Insert description here
// You can write your code in this editor
tiles_stepsound = -1;	
tiles_encounter = -1;

// sounds
stepsound[0] = soundMenuDefaultMove
stepsound[1] = soundFootstep03
stepsound[2] = soundFootstep04
stepsound[3] = soundMenuDefaultMove
stepsound[4] = soundMenuDefaultMove
stepsound[5] = soundFootstep01
stepsound[6] = soundFootstep02
stepsound[7] = soundMenuDefaultMove

follow_obj = Player
set_obj = -1; //object to be set to after transitioning
path = -1

//convert this to scripts later
zoom_x = .25
zoom_y = .25

cameraOverworldCreate();

binoculars = false;

binoculars_u_vRatio = shader_get_uniform(shd_binoculars, "u_vRatio");
binoculars_sampler0  = shader_get_sampler_index(shd_binoculars, "sampler0")
binoculars_texture0  = sprite_get_texture(spr_binoculars_shader,0);
binoculars_u_threshold = shader_get_uniform(shd_binoculars, "u_threshold");
binoculars_amount = -.75

prop_place_surface = -1;
prop_place = false;
prop_place_obj = -1;

surface_view = -1;

bg_camera = view_get_camera(0);

active = true;

event_perform(ev_room_start,0);