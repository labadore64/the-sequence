{
    "id": "6c299296-44d4-492b-82db-049ae9fd29c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_vcrOn",
    "eventList": [
        {
            "id": "22282ddf-1d71-43c4-a6ba-67852e9819b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6c299296-44d4-492b-82db-049ae9fd29c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8391da6-b468-40dc-98eb-dcb432a5fdfe",
    "visible": true
}