{
    "id": "bf4d59ef-a965-4611-9751-9fe9b07c96cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ScriptInteractStepOver",
    "eventList": [
        {
            "id": "5f6e9535-f2cf-4599-88fa-eb6696c76aa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf4d59ef-a965-4611-9751-9fe9b07c96cb"
        },
        {
            "id": "2efb466c-fe3c-4d08-a92c-1303b2dec44e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bf4d59ef-a965-4611-9751-9fe9b07c96cb"
        },
        {
            "id": "24e45c43-2860-4661-ab56-5835a9b135b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "bf4d59ef-a965-4611-9751-9fe9b07c96cb"
        },
        {
            "id": "44d959d4-aca9-48eb-af80-62fe51592629",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf4d59ef-a965-4611-9751-9fe9b07c96cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8b41fb1-1288-4cb8-b6a5-9b65b831bc8f",
    "visible": false
}