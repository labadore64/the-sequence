{
    "id": "fe749908-3d78-41bb-9b23-cc34a54c688d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_data",
    "eventList": [
        {
            "id": "1fe27b84-356f-4512-86da-7c200e28c2e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "f3c29bf2-62cf-4110-9fc4-d72f0e841a22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "8a113f7d-7c0d-49b1-96f9-24cf2cff4b17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "f66220e9-59a3-4505-891b-b301d93a27c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "a55d6212-86e2-471e-9573-ad34d1ce47b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "7a24dda2-b2dc-4c5c-bacc-dcde0ed49bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "5d25e762-f87d-424a-9fc2-761ec03364eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "bc2ec2a0-6f27-425d-8853-20ec5395ce24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "c1ce1790-9f37-4772-9398-8f9d0c72b4d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "b179afc7-83b7-4601-8672-e6b8d0dfac8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        },
        {
            "id": "8c5109b1-45fa-48fb-9862-8f4e525db91e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "fe749908-3d78-41bb-9b23-cc34a54c688d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}