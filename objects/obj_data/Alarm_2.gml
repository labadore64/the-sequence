/// @description Insert description here
// You can write your code in this editor

// completed task
if(task_buffer != -1){
	if(!instance_exists(menuParent) && 
		!instance_exists(TextBox) && 
		!instance_exists(BattleHandler)){
			
		var obj = instance_create(0,0,battleWindow);
		obj.windowText = "Completed task: " +
						objdata_task.title[task_buffer];
		obj.locked_player = true;
		with(obj){
			menuParentSetBoxHeight(200)	
		}
		with(Player){
			lock = true;	
		}
		eventNormalSetIndex(objdata_task.completed_event_id[task_buffer])	
	} else {
		newalarm[2] = 5;	
	}
}