/// @description Insert description here
// You can write your code in this editor
if(mapIsOverworld()){
	last_room = current_room;
	current_room = room;
	
	if(instance_exists(Player)){
		pos_x = Player.x;
		pos_y = Player.y;
	}
	
	dataSave();
	loadProps();
	
	var sizer = ds_list_size(bird_flown_away);
	var obj = -1;
	for(var i = 0; i < sizer; i++){
		obj = bird_flown_away[|i];
		if(!is_undefined(obj)){
			if(instance_exists(obj)){
				with(obj){
					instance_destroy();	
				}
			}
		}
	}
	
	if(last_room != room){
		ds_list_clear(bird_flown_away)
	}
}

if(reposition){
	var xx = current_x;
	var yy = current_y;
	
	if(!instance_exists(Player)){
		instance_create(0,0,Player);	
	}
	
	with(Player){
		x = xx;
		y = yy;
	}
	
	cameraZoomSet(screen_scale)
}

reposition = false;

if(push_obj_position != -1){
	
	
	dataPushOjbectPlace();
	ds_map_destroy(push_obj_position);
	push_obj_position = -1;
}