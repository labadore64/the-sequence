/// @description Insert description here
// You can write your code in this editor
//turn to scripts later
// date/time
initTimingAlarm();
time_cando = false;
time_start_timer = 0;

// if certain important objects exist
camera_exists = false;
battle_exists = false;

// data

// start events
loadEvents();
loadItemEvents();
// end events

textbox_data = instance_create(0,0,objdata_textbox);
dia_data = instance_create(0,0,objdata_diagram);
bird_data = instance_create(0,0,objdata_bird);
enemy_data = instance_create(0,0,objdata_enemy);
character_data = instance_create(0,0,objdata_character);
move_data = instance_create(0,0,objdata_moves);
item_data = instance_create(0,0,objdata_items);
tablet_data = instance_create(0,0,objdata_tablet);
word_data = instance_create(0,0,objdata_words);
exp_data = instance_create(0,0,objdata_exp);
status_data = instance_create(0,0,objdata_status);
character_movelearn_data = instance_create(0,0,objdata_character_movelearn)
enemy_movelearn_data = instance_create(0,0,objdata_enemy_movelearn)
end_phase_data = instance_create(0,0,objdata_endphase);
UI_data = instance_create(0,0,objdata_UI);
encounter_data = instance_create(0,0,objdata_encounter);
encountermap_data = instance_create(0,0,objdata_encountermap);
task_data = instance_create(0,0,objdata_task);
email_data = instance_create(0,0,objdata_email);
voice_data = instance_create(0,0,objdata_voicemail);

ax_manager = instance_create(0,0,AXManager);

task_buffer = -1;

braille_data = instance_create(0,0,brailleData);

inventory = ds_map_create(); //key = item, value = quantity
money = 250;
time_seconds = floor(get_timer()*0.000001)
time_start = 0;
aura = c_red;
time_current = 0;

//stat boosts
phy_boost = 0;
mag_boost = 0;
spe_boost = 0;

aura_ability = -1;

//events
//items_obtained = ds_list_create(); //whether or not you obtained an overworld item
//events = ds_list_create(); //whether or not a specific event was triggered
dialog_events = ds_list_create(); //whether or not a specific dialog event was triggered

pos_x = 0;
pos_y = 0;

obj_dataCharactersInit()

//debug items
//for(var i = 1 ; i < 17; i++){
	giveItem(objdataToIndex(objdata_items,"map"),1);
	giveItem(objdataToIndex(objdata_items,"bird guide"),1);
	giveItem(objdataToIndex(objdata_items,"binoculars"),1);
	giveItem(objdataToIndex(objdata_items,"creature scope"),1);
//}

newalarm[0] = 2

text_input_buffer = "";

current_room = room;
last_room = room;

room_for_map = room;

current_x = 0;
current_y = 0;

defeat_room = room1;
defeat_x = 300;
defeat_y = 300;

has_monsters = false;

bird_seen = ds_map_create();
prop_place = ds_map_create();

bird_seen_count = 0;

set_item = -1;

reposition = false;

bird_flown_away = ds_list_create();

screen_scale = 1;

push_obj_position = -1;

monster_talk_list = ds_map_create();

newalarm[1] = 1

monster_spawn[0] = objdataToIndex(objdata_enemy,"vecdor")
monster_spawn[1] = objdataToIndex(objdata_enemy,"snailcat")
monster_spawn[2] = objdataToIndex(objdata_enemy,"phantern")

for(var i = 0; i< 3; i++){
	default_spawn[i] = monster_spawn[i];	
}

braille_highscores = ds_map_create();

initTablets();

getTablet(1);
getTablet(2);
// collected signs
for(var i =0; i < objdata_words.data_size; i++){
	if(i mod 2 == 0){
		sign_seen[i] = true	
	} else {
		sign_seen[i] = false	
	}
}

// phone things

game_stats = ds_map_create();

encounter_id = 0;