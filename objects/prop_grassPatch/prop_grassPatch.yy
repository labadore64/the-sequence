{
    "id": "2bb3cfc0-8d84-49f6-845b-e5f5291537e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_grassPatch",
    "eventList": [
        {
            "id": "80e2b154-c464-4135-a7c3-1a8fbce7811e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "2bb3cfc0-8d84-49f6-845b-e5f5291537e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5b1402b7-048a-429e-936a-504937bb4890",
    "visible": true
}