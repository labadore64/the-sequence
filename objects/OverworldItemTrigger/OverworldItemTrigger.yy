{
    "id": "9a554a30-83c5-4d99-8254-81907bcbd747",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OverworldItemTrigger",
    "eventList": [
        {
            "id": "d22e2f2a-60de-4f70-bf19-ba9dd7535078",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a554a30-83c5-4d99-8254-81907bcbd747"
        },
        {
            "id": "dea43242-2afd-4f1f-bfdd-d0961cd34d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9a554a30-83c5-4d99-8254-81907bcbd747"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ed7965c-678d-4847-87e0-8e4edc504b92",
    "visible": false
}