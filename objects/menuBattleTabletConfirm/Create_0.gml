/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

mp_total_cost = 0;

menu_draw = menuBattleTabletConfirmDraw;

braille_preview = noone

menu_left = menuBattleTabletConfirmLeft
menu_right = menuBattleTabletConfirmRight

menu_up = menu_left;
menu_down = menu_right;

menuAddOption(LANG_SELECT_LEFT,"",menuBattleTabletConfirmSelect);
menuAddOption(LANG_SELECT_RIGHT,"",menuBattleTabletConfirmSelect);

menu_size =2
drawSubtitle = false;
wrap = false;

character = -1;

is_bribe = false;

title = "Insert Where?"
title_x_offset = 315
option_space = 250
option_x_offset = 175
option_y_offset = 235 
text_size = 3;
drawGradient = false;
draw_bg_alpha = .75
draw_bg = true;

base_string = BattleHandler.braille_display.display_string;
add_string = "";

x = 400;
y = 300;

draw_width = 600;
draw_height = 300; 

selected_scale_counter = 0;
selected_scale_amount = 1;

move_type_sprite = -1

tablet_boost = 0;

menuParentUpdateBoxDimension();

rotate_color_value = 0;
rotate_color = 0;

menu_ax_HUD = "battleTabletConfirm"

menu_help = "battleTabletConfirm"

newalarm[2] = 5;