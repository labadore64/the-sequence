/// @description Insert description here
// You can write your code in this editor
tts_title_string = add_string + " " + title
// Inherit the parent event
event_inherited();

braille_preview = instance_create(130,250,braillePreview);
braille_preview.visible = false;
braille_preview.directional = false;
braille_preview.init_string = base_string;
braille_preview.add_string = add_string;

tablet_boost = (string_length(add_string)-1)*.5;

menuBattleTabletConfirmUpdateHUD();

braille_preview.preview_string = preview_string


with(menuControl){
	please_draw_all_visible = true;	
}

