{
    "id": "1f24e664-f210-40cd-a1e7-7584b7f42fc4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleTabletConfirm",
    "eventList": [
        {
            "id": "c5341309-3483-4f36-aa63-38103ec34df2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f24e664-f210-40cd-a1e7-7584b7f42fc4"
        },
        {
            "id": "e92e33fb-2f66-4d93-931f-69848e2a7df0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f24e664-f210-40cd-a1e7-7584b7f42fc4"
        },
        {
            "id": "b573292f-1cca-4cad-9cbc-2e654bf96ef4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "1f24e664-f210-40cd-a1e7-7584b7f42fc4"
        },
        {
            "id": "19c10049-ac9c-4311-8b10-78e42fb4723d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1f24e664-f210-40cd-a1e7-7584b7f42fc4"
        },
        {
            "id": "2c182cc3-51cb-490b-ac10-03bc81e99bd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "1f24e664-f210-40cd-a1e7-7584b7f42fc4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}