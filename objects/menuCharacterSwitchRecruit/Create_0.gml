/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

recruit_list = ds_list_create();

var list = recruit_list;

with(dataCharacter){
	if(recruited){
		ds_list_add(list,id);	
	}
}

var newlist = obj_data.party;

var sizer = ds_list_size(newlist);
var obj = -1;
var index = 0;
for(var i = 0; i < sizer; i++){
	obj = newlist[|i];
	
	if(!is_undefined(obj)){
		index = ds_list_find_index(list,obj);
		
		if(index != -1){
			ds_list_delete(list,index);	
		}
	}
}

menu_size = ds_list_size(list);

character_string = textForceCharsPerLine(string_replace_all(recruit_list[|menupos].battle_desc,"\n"," "),40)

release_the_penguins = false;

surface = surface_create_access(267*5,160)

animation_adjust = 0;
animation_move = false;
animation_time = 7;
animation_direction = 1;
update_surface = true;

menu_select = menuCharacterSwitchPartyRecruitSubScript;

menu_draw = menuCharacterSwitchPartyRecruitDraw//menuCharacterSwitchPartyDraw

menu_left = menuCharacterSwitchPartyRecruitLeft;
menu_right = menuCharacterSwitchPartyRecruitRight;
menu_up = menu_left;
menu_down = menu_right;

newalarm[4] = 1;

with(menuCharacterSwitchParty){
	var chara = obj_data.party[|menupos].character
	char_color = obj_data.party[|menupos].color
	portrait.character = chara;
	menuOverworldPartyUpdateStatsPortrait(obj_data.party[|menupos],100,drawstat);
	with(portrait){
		surface_free(surface)	
		event_perform(ev_alarm,0)
	}	
}