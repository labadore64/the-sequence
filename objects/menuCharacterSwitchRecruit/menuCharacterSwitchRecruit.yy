{
    "id": "420addde-0863-4209-9a28-76876b020a20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSwitchRecruit",
    "eventList": [
        {
            "id": "1102c9f9-67c9-4987-9c73-4ab365ed2731",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "420addde-0863-4209-9a28-76876b020a20"
        },
        {
            "id": "a27cd9be-af94-441b-a73e-e0f00f5dfddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "420addde-0863-4209-9a28-76876b020a20"
        },
        {
            "id": "2a0a1a0c-9276-41c5-be74-fc14f84b2a43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "420addde-0863-4209-9a28-76876b020a20"
        },
        {
            "id": "b1bb3924-f4a9-4b62-a2b2-bdb91a5116ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "420addde-0863-4209-9a28-76876b020a20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fe3deea6-05d1-4137-9a29-fbe06b4f5dae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}