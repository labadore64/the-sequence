/// @description Insert description here
// You can write your code in this editor
if(start ){
	
	if(!surface_exists(stats_surf)){
		stats_surf = surface_create_access(275*global.scale_factor,600*global.scale_factor);
	} else {
		if(ScaleManager.updated){
			surface_resize(stats_surf,275*global.scale_factor,600*global.scale_factor)
		}
	}
	
	surface_set_target(stats_surf);
	draw_clear_alpha(c_white,0)
	draw_set_color(global.textColor)

	draw_set_font(global.largeFont);

	draw_set_halign(fa_center)

	draw_text_transformed_textbox_ratio(120,0,character.name,1,1,0)

	draw_set_halign(fa_left)

	draw_set_font(global.textFont)

	if(char_sprite != -1){
		draw_sprite_extended_nopos_ratio(char_sprite,0,30,20,1.4,1.4,0,c_white,1)
	}

	var adjust = 155;

	draw_text_transformed_textbox_ratio(10,52+adjust+10,"Lv." + string(character.level),1,1,0)
	gpu_set_texfilter(true);
	drawBarNoScale(
				125*global.scale_factor,
				(55+adjust+10)*global.scale_factor,
				60*global.scale_factor,
				5*global.scale_factor,
				character.exp_percent,global.textColor)
	if(stats != -1){
		with(stats){
		
			draw_surface_ext(
							surface_hex,
							(20+10)*global.scale_factor,
							(60+adjust)*global.scale_factor,
							.6,.6,0,
							c_white,1);
		
		}
	}
	gpu_set_texfilter(false);
	surface_reset_target();
}