/// @description Insert description here
// You can write your code in this editor
event_inherited();
initTimingAlarm();
gpu_set_colorwriteenable(true,true,true,true)
menu_select = battleEndVictoryStatsConfirm;

last_level = 0;
current_level = 0;

character = -1;
stats = -1;

current_exp  = 0;

exp_reward = 0;

exp_spacer = 0; //to make sure it always takes the same amount of time to fill the exp

exp_time = 90; //how much time it takes to fill the EXP bar

exp_counter = 0;

exp_end = 0; //final exp

exp_count_complete = false;

wait_counter = 10;

active = false;

newalarm[0] = 1;

stats_surf = -1

char_sprite = -1;

start = false;

textbox_hold_counter = 0;
textbox_hold_max = global.text_auto_speed;

mouseHandler_clear();
mouseHandler_add(0,0,800,600,battleEndVictoryStatsConfirm,"")