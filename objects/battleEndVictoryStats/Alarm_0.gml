/// @description Insert description here
// You can write your code in this editor
active = true;
current_exp = character.experience;
character = character.overworld_character;
char_sprite = character.face_sprite;
if(character != -1){
	stats = instance_create(0,0,dataDrawStat);
	var chara = character;
	
	with(stats){
		character = chara;
		draw_hex_init(chara,120,chara.phy_power,
					chara.phy_guard ,
					chara.mag_power ,
					chara.mag_guard ,
					chara.spe_agility ,
					chara.spe_mpr);
		radius = 120;
	}
}

exp_spacer = exp_reward/exp_time

exp_end = current_exp + exp_reward

start = true;