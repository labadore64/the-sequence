/// @description Insert description here
// You can write your code in this editor
event_inherited();
updateTimingAlarm()
if(active){
	if(wait_counter >= 0){
		wait_counter--;	
	} else {
		if(!exp_count_complete){
			
			current_level = character.level;
			
			//keep adding exp to the level	
			current_exp+=exp_spacer;
			current_exp = clamp(current_exp,0,BATTLE_STAT_EXP_TOTAL);
			exp_counter++;
			if(exp_counter == exp_time){
				exp_count_complete = true;
				current_exp = exp_end;
				current_exp = clamp(current_exp,0,BATTLE_STAT_EXP_TOTAL);
			}
			character.experience = current_exp;
			with(character){
				statExperience(id)
			}
			
			if(last_level != current_level){
				battleEndVictoryStatsUpdateLevel();
			}
			
			last_level = current_level;
		}
		
	}
	
}
