{
    "id": "4d8545bd-dc37-4820-a104-74f817da3b89",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleEndVictoryStats",
    "eventList": [
        {
            "id": "79fcda0e-0e6d-4e36-b1d0-36521b7f27cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d8545bd-dc37-4820-a104-74f817da3b89"
        },
        {
            "id": "54ec787f-678e-4f74-a78f-34e7429f57bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4d8545bd-dc37-4820-a104-74f817da3b89"
        },
        {
            "id": "802832e9-b9a3-4e67-bc3b-1cd80cbeb510",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d8545bd-dc37-4820-a104-74f817da3b89"
        },
        {
            "id": "3353fdbb-4ad3-4040-8dfa-a5e2b60b291c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4d8545bd-dc37-4820-a104-74f817da3b89"
        },
        {
            "id": "6a405274-d901-4cff-ad6f-e059025c032b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4d8545bd-dc37-4820-a104-74f817da3b89"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}