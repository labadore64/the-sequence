{
    "id": "06585025-1490-4605-b918-a3495f5d5189",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "roomTransition",
    "eventList": [
        {
            "id": "e772e815-dd88-4338-b18c-a2becbc73c8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "e02ecd72-f549-4b45-a8bf-9a34424bb69e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "3d6c6d2a-8890-478d-a97c-2c7553aaf468",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "aedde525-6d9d-4f38-ab9a-df05921d3527",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "07f5301b-15a1-4b2c-8f19-69e934d3dec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "4fc8eee9-4e92-4de4-a3d0-90afe1c4aff0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "48cfc641-6bc7-4500-b36d-92ff36fde495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "72d57456-ffbe-4d28-a85a-f35e38ef9f7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        },
        {
            "id": "b4fa8d6a-eaaf-4bfb-bf91-b148744f7762",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "06585025-1490-4605-b918-a3495f5d5189"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65b2907f-c371-4ea3-9bdd-d3887c7f45f2",
    "visible": false
}