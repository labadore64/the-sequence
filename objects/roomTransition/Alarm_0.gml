/// @description Insert description here
// You can write your code in this editor
if(room_id != -1){
	room_goto(room_id);
	var xx = x_pos;
	var yy = y_pos;
	clearHUD();

	with(Player){
		x = xx;
		y = yy;
		draw_action = PLAYER_ACTION_STAND
		playerUpdateSprite();
		audio_listener_position(x, y+collision_y_adjust, 0);
		depth = y;
	}
}