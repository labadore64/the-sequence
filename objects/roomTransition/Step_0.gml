/// @description Insert description here
// You can write your code in this editor
if(ScaleManager.updated){
	x = global.display_x
	y = global.display_y

	image_xscale = global.scale_factor;
	image_yscale = global.scale_factor;	
}

for(var i = 0; i < 4; i++){
	if(newalarm[i] > -10000){
		newalarm[i] -= menuControl.timer_diff;	
		if(newalarm[i] < 0){
			event_perform(ev_alarm,i);
			newalarm[i] = -10000;
		}
	}
}
spr_counter += menuControl.timer_diff*.5;

if(spr_counter > 47){
	instance_destroy()
}