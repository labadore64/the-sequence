/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\task\\task";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] =ini_read_string_length("data","name","",IMPORT_MAX_SIZE_TASK_NAME);
	title[itemcount] =ini_read_string_length("data","title","",IMPORT_MAX_SIZE_TASK_TITLE);
	desc[itemcount] = ini_read_string_length("data","desc","",IMPORT_MAX_SIZE_TASK_DESC);
	sprite[itemcount] = asset_get_index("spr_task_" + ini_read_string_length("data","sprite","",IMPORT_MAX_SIZE_SPRITE));
	event_id[itemcount] = eventFind(ini_read_string_length("data","event_id","",IMPORT_MAX_SIZE_EVENT));
	completed_event_id[itemcount] =eventFind(ini_read_string_length("data","completed_event_id","",IMPORT_MAX_SIZE_EVENT));
	
	ini_close();
	
	// data that will be saved in files
	unread[itemcount] = false;
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;

