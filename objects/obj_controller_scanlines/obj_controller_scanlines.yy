{
    "id": "626dc21c-dd6c-47f2-b17e-be724553971c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_scanlines",
    "eventList": [
        {
            "id": "e6fa437e-2108-4e03-a2fb-147383d70819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "626dc21c-dd6c-47f2-b17e-be724553971c"
        },
        {
            "id": "67ba0bf7-fd88-4b8b-9f2f-6f13c087bd69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "626dc21c-dd6c-47f2-b17e-be724553971c"
        },
        {
            "id": "8a82f777-7f4e-42a0-b300-3159a8a69d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "626dc21c-dd6c-47f2-b17e-be724553971c"
        },
        {
            "id": "ce873aec-6684-4066-a979-2c38ec54b833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "626dc21c-dd6c-47f2-b17e-be724553971c"
        },
        {
            "id": "1499e2da-117d-4f23-af6b-3035f7deca83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "626dc21c-dd6c-47f2-b17e-be724553971c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}