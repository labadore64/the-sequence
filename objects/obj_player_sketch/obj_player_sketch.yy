{
    "id": "3db06440-282a-4dd3-8c90-feb99da557d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_sketch",
    "eventList": [
        {
            "id": "58449969-7f65-46d5-b55d-a771090061f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3db06440-282a-4dd3-8c90-feb99da557d7"
        },
        {
            "id": "d4f5f42c-b382-485e-a61b-3523dc018093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3db06440-282a-4dd3-8c90-feb99da557d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "9efd7e18-07b0-48d6-8d35-d4f5818d6758",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "1ed6c0ff-cfe1-49f3-9aaf-a77548abd596",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}