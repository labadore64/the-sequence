{
    "id": "aac8cb8e-dc5f-46d2-8423-44877150caf9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CutsceneBlock",
    "eventList": [
        {
            "id": "f0849393-7302-48f8-8f61-cfc91c40bd83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aac8cb8e-dc5f-46d2-8423-44877150caf9"
        },
        {
            "id": "429abcc0-4c11-437f-8342-0bb38b5ee6d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "aac8cb8e-dc5f-46d2-8423-44877150caf9"
        },
        {
            "id": "f2a83a4b-e01e-42ce-936c-feb454f7ca20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aac8cb8e-dc5f-46d2-8423-44877150caf9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}