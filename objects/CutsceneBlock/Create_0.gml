/// @description Insert description here
// You can write your code in this editor
//represents a single block.
//loaded at the beginning of a cutscene.
commands = -1;
active = false; //whether or not its executing code.
pointer = 0; //indicates where we are reading.
return_block = noone; //determines which block should be returned to after dead.

return_header = noone

//flags
cocurrent = false; //if cocurrent, is part of the current executing block, and will only
					//continue processing once all concurrent threads are completed
background = false; //if background, it is not part of the current executing block, but
					//will continue to execute. Processing will continue even as it is
					//still processing.
				
command_list = ds_list_create(); //a list that is filled when the block starts running.