{
    "id": "16e49a02-f443-452b-b82e-588e052cf9a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBestiaryMain",
    "eventList": [
        {
            "id": "bf662a04-64e7-4109-93bf-a4ef9388b96c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16e49a02-f443-452b-b82e-588e052cf9a4"
        },
        {
            "id": "4c557c24-dab6-411c-aa66-642b4d7c0170",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "16e49a02-f443-452b-b82e-588e052cf9a4"
        },
        {
            "id": "2493489e-5f25-417f-8e69-75b3730a7a5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16e49a02-f443-452b-b82e-588e052cf9a4"
        },
        {
            "id": "0624957c-4e00-4665-8e1f-8498e187ee55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "16e49a02-f443-452b-b82e-588e052cf9a4"
        },
        {
            "id": "be93c607-743c-40d6-bda3-448c65033714",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "16e49a02-f443-452b-b82e-588e052cf9a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}