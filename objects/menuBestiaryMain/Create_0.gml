/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
no_creatures = false

double_select = false;
sort_name[0] = ""
sort_name[SORT_MODE_ALPHA] = "ABC"
sort_name[SORT_MODE_INDEX] = "123"

bird_list = ds_list_create();

display_list = ds_list_create();

recorded_size = 0;
   
for(var i = 0; i < objdata_enemy.data_size; i++){
	if(objdata_enemy.state[i] != DEX_STATE_UNSEEN &&
		!objdata_enemy.ignore_dex[i]){
		ds_list_add(bird_list,i);	
		
		if(objdata_enemy.state[i] == DEX_STATE_OWN){
			recorded_size++;	
		}
	}
}
   
menuBestiaryMainSort(global.dex_sort_mode)

select_percent = .5

menuParentSetTitle("Creature Scope");
menuParentSetDrawSubtitle(false)
title_y_offset = 20

menu_draw = menuBestiaryMainDraw;

menu_up = menuBestiaryMainUp
menu_down = menuBestiaryMainDown

script_menu_open = menuBestiaryMainUpdateSort

monster_sprite = -1;
monster_state = DEX_STATE_SEEN;

menu_slideInit();
translation_start = 150;



// fade shit

animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = animation_start

draw_bg = false;

active = false;

fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;

surf = -1;

var obj = instance_create(0,0,brailleGameBackground);
obj.draw_me = false;

menuParentSetGradient($7f7f00,c_black)

menu_ax_HUD = "bestiaryMain"

tts_title_string = title

ignore_tab = true