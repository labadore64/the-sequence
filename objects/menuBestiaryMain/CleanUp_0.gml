/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(ds_exists(bird_list,ds_type_list)){
	ds_list_destroy(bird_list);	
}

if(ds_exists(display_list,ds_type_list)){
	ds_list_destroy(display_list);	
}

draw_texture_flush();

if(!instance_exists(menuOverworldItems)){
	with(Player){
		active=true;	
	}	
}

if(surface_exists(surf)){
	surface_free(surf)	
}