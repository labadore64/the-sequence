/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_interact = overworldTextStaticCreateItemTextbox; //script to create text box

//item data
item_text_id = "test"; //id for item dialog
item_event_id = "teststring"; //event id for item being obtained
item_event = -1;
item_id = objdataToIndex(objdata_items,"red feather")//-1; //item to receive
item_quantity = 1; //number of item to receive

//after item has been obtained
text_box_id = "test"; //textbox to look up

