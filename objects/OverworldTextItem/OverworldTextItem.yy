{
    "id": "3a4c24c5-8171-4ac7-b0f2-44dae58c9bc4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OverworldTextItem",
    "eventList": [
        {
            "id": "4fb127dc-f0a4-4db9-9fc3-a6f3553432e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a4c24c5-8171-4ac7-b0f2-44dae58c9bc4"
        },
        {
            "id": "60e70a8d-0305-455c-8f06-65c35a44c2ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3a4c24c5-8171-4ac7-b0f2-44dae58c9bc4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f791b599-0793-407d-a584-5783653aeb53",
    "visible": false
}