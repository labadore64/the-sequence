{
    "id": "7d3e6398-ff57-4a88-8de3-9d63c509cf5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldItemSub",
    "eventList": [
        {
            "id": "4b434f97-92ba-425e-ace6-4b3c2bebf73f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d3e6398-ff57-4a88-8de3-9d63c509cf5b"
        },
        {
            "id": "a32683d3-3d4f-4c7f-9ad2-f52509df43b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "7d3e6398-ff57-4a88-8de3-9d63c509cf5b"
        },
        {
            "id": "773beb19-9cce-48ed-81f1-861ff80bbf91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d3e6398-ff57-4a88-8de3-9d63c509cf5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}