{
    "id": "f6c2be22-136e-4bd3-8655-a32b1a11b020",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_brightness_contrast",
    "eventList": [
        {
            "id": "091643d7-2d61-41b2-b528-b417e1e8c968",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6c2be22-136e-4bd3-8655-a32b1a11b020"
        },
        {
            "id": "0cb8a040-c8cf-4c92-87ea-f410d0258e88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6c2be22-136e-4bd3-8655-a32b1a11b020"
        },
        {
            "id": "d25ce5e8-a93f-4071-bf0c-3f2caa975a56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f6c2be22-136e-4bd3-8655-a32b1a11b020"
        },
        {
            "id": "423f0c75-b302-4403-aaa8-5e6a0a201c24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "f6c2be22-136e-4bd3-8655-a32b1a11b020"
        },
        {
            "id": "83d45500-959c-457a-8072-c4f2460364a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "f6c2be22-136e-4bd3-8655-a32b1a11b020"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}