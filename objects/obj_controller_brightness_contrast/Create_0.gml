draw_set_color(c_white);

uni_time = shader_get_uniform(shd_bright_contrast,"time");
var_time_var = 0;

uni_mouse_pos = shader_get_uniform(shd_bright_contrast,"mouse_pos");
var_mouse_pos_x = 400
var_mouse_pos_y = 300;

uni_resolution = shader_get_uniform(shd_bright_contrast,"resolution");
var_resolution_x = 800;
var_resolution_y = 600;

uni_brightness_amount = shader_get_uniform(shd_bright_contrast,"brightness_amount");
var_brightness_amount = 0;

uni_contrast_amount = shader_get_uniform(shd_bright_contrast,"contrast_amount");
var_contrast_amount = 0;

shader_enabled = shader_is_compiled(shd_bright_contrast);

shader_enabled = true;
full_screen_effect = true;

surf = -1
view_set_surface_id(0, surf);