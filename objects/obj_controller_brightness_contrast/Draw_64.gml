
if(!surface_exists(surf)){
	surf = surface_create_access(view_wview, view_hview);	
}

var_time_var+=0.04;

var_mouse_pos_x = mouse_x - camera_get_view_x(0);
var_mouse_pos_y = mouse_y - camera_get_view_y(0);



draw_text(0,12,string_hash_to_newline("Brightness amount (Q & A to adjust): "+string(var_brightness_amount)));
draw_text(0,24,string_hash_to_newline("Contrast amount (W & S to adjust): "+string(var_contrast_amount)));

draw_text(0,60,string_hash_to_newline("Spacebar to toggle shader"));
draw_text(0,84,string_hash_to_newline("Escape key to exit"));