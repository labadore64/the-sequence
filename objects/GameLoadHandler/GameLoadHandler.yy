{
    "id": "caadc18c-b31d-443e-9c8c-e0a1680805da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GameLoadHandler",
    "eventList": [
        {
            "id": "40c983ed-7f84-43df-9cea-d89057afc0f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "caadc18c-b31d-443e-9c8c-e0a1680805da"
        },
        {
            "id": "69f9b256-dd31-4718-856f-f5c2910d060e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "caadc18c-b31d-443e-9c8c-e0a1680805da"
        },
        {
            "id": "d2ec7d1f-ca4c-4233-8660-e33a52f3504d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "caadc18c-b31d-443e-9c8c-e0a1680805da"
        },
        {
            "id": "b05067af-d292-416e-8d8f-fe9dee88a779",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "caadc18c-b31d-443e-9c8c-e0a1680805da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}