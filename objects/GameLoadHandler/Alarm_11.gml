/// @description Insert description here
// You can write your code in this editor
tts_title_string = "Load Game"//menu_name_array[menupos]
// Inherit the parent event
event_inherited();


mouseHandler_clear_main()
mouseHandler_add(120,500-15-10,400,530-10,gameLoadHandlerLoadGame,menu_name[|0],true)
mouseHandler_add(120,550-15-10,400,580-10,menuOverworldMainScriptOption,menu_name[|1],true)
mouseHandler_add(430,500-15-10,650,530-10,gameLoadHandlerNewGame,menu_name[|2],true)
mouseHandler_add(450,550-15-10,650,580-10,gameLoadHandlerBraillePractice,menu_name[|3],true)

mouseHandler_menu_setCursor();