/// @description Insert description here
// You can write your code in this editor
event_inherited()

cango = true;
menu_use_generic = true;

name = "";
time_seconds = 0;
time_min = 0;
time_min_draw = "00";
time_hour = 0;
recruited = 0;
location_name = "";
party = ds_list_create();

menuAddOption("Load Game","Load the current game.",gameLoadHandlerLoadGame)
menuAddOption(global.langini[0,3],global.langini[1,3],menuOverworldMainScriptOption)
menuAddOption("New Game","Start a new game.",gameLoadHandlerNewGame)
menuAddOption("Practice","Practice Braille.",gameLoadHandlerBraillePractice)

enable_scrollwheel = false;

animation_len = 0
menu_draw = gameLoadHandlerDraw;
menu_cancel = -1;

fade_in_script = gameLoadHandlerStartNewGame;
fade_out_script = -1;

menu_size = menuGetSize();

menu_up = gameLoadHandlerUpDown;
menu_down = gameLoadHandlerUpDown;

normal_scale = 3;
select_scale = 3;
select_counter = 0;
select_expand = .15;
select_time = .15;

skip = 2

var filename = working_directory + "save\\base.ini";

var fileexists = file_exists(filename);

if(fileexists && saveFileValid()){
	soundPlaySong(music003,30)
	fadeOut(30,c_black);
	ini_open(filename)
	
	tablet_count = ini_read_real("main","tablets_total",0);
	time_seconds = ini_read_real("main","time_seconds",0);
	recruited = ini_read_real("main","recruited",0);
	location_name = roomGetName(ini_read_real("main","room",0));
	var val = 0;
	for(var i = 0; i < 3; i++){
		val = ini_read_real("main","party" + string(i),0);
		if(!is_undefined(val)){
			val = floor(val);
			if(val >= 0){
				ds_list_add(party,obj_data.characters[|val]);
			}
		}
	}
	
	time_hour = clamp(floor(time_seconds/60/60),0,999);
	time_minute = (floor(time_seconds/60) mod 60);
	
	if(time_minute < 10){
		time_min_draw = "0" + string(time_minute);	
	} else {
		time_min_draw = string(time_minute);	
	}
	
	ini_close();
	menu_ax_HUD = "gameState"
	hudPopulateGameData();
	// fix game time
	hudControllerAddData("game.hours",string(time_hour));
	hudControllerAddData("game.minutes",string(time_minute));
	hudControllerAddData("game.location",location_name);
	
	tts_say(menu_name_array[0])
	
	menu_help = "loadgame"
} else {
	//delete all files in folder
	var filedel = file_find_first(working_directory + "save\\*.*", 0);
	file_delete(working_directory + "save\\"+filedel);
	while(filedel != ""){
		filedel = file_find_next();
		file_delete(working_directory + "save\\"+filedel);
	}
	
	//reset keybinds
	with(AXManager){
		keyboard_bind_init();
	}

	file_find_close();
	room_goto(name_room);	
}