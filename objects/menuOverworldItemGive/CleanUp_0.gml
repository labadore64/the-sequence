/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var sizer = ds_list_size(list_of_names);

for(var i = 0; i < sizer; i++){
	ds_list_destroy(list_of_names[|i]);	
}

ds_list_destroy(list_of_names);

menuControlForceDrawMenuBackground();