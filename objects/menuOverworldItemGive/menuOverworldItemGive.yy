{
    "id": "b98d78b4-1074-4e76-9585-bea3f14b31fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldItemGive",
    "eventList": [
        {
            "id": "6236a591-8720-496f-ac3d-6702585166e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b98d78b4-1074-4e76-9585-bea3f14b31fd"
        },
        {
            "id": "dff825c8-aa88-43da-8077-65bae5b43acf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b98d78b4-1074-4e76-9585-bea3f14b31fd"
        },
        {
            "id": "595adf11-57ba-4e0a-b76a-0ce5670734ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "b98d78b4-1074-4e76-9585-bea3f14b31fd"
        },
        {
            "id": "6f80771c-8e21-4ecc-8131-dabe60ecddd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b98d78b4-1074-4e76-9585-bea3f14b31fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}