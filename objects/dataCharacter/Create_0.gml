/// @description Insert description here
// You can write your code in this editor
//represents the character stats.
initTimingAlarm()
//whether or not the data already has been loaded once
loaded = false;

can_switch = true;

// element
element = -1;

//displayed/used stats
name = "";
character = 0;
phy_power = 1; //attack
phy_guard = 1; //defense
mag_power = 1; //magic
mag_guard = 1; //resistance
spe_agility = 1; //agility
spe_mpr = 1; //mpr
hp = 1; //hp
mp = 1; //energy points

phy_power_base = 1; //attack base
phy_guard_base = 1; //defense base
mag_power_base = 1; //magic base
mag_guard_base = 1; //resistance base
spe_agility_base = 1; //agility base
spe_mpr_base = 1; //mpr
hp = 1;
mp = 1;

sell_multiplier_script = -1;
buy_multiplier_script = -1;
buy_item_script = -1;

//item boosts
boost_phy_power = 0; //attack
boost_phy_guard = 0; //defense
boost_mag_power = 0; //magic
boost_mag_guard = 0; //resistance
boost_spe_agility = 0; //agility
boost_spe_mpr = 0; //mpr

// stats with tablets
tablet_phy_power = 0
tablet_phy_guard = 0
tablet_mag_power = 0
tablet_mag_guard = 0
tablet_spe_power = 0
tablet_spe_guard = 0

// used for temp operations
temp_phy_power = 0
temp_phy_guard = 0
temp_mag_power = 0
temp_mag_guard = 0
temp_spe_power = 0
temp_spe_guard = 0

//growth curves

//levelling
experience = 2500 //0; //experience points
exp_to_next = 0; //required exp to next level
exp_of_level = 0; //exp of current level
exp_percent = 0; //0-1 value representing how much exp is filled
level = 99; //1 to 99

//colour
color = c_white;

//inventory
for(var i = 0; i < 5; i++){
	item[i] = -1
}

//moves
moves = ds_list_create();

//dialog stuff
next_dialog = "none"; //name of text file for next dialog.

primary_color = false;
color_battle = c_black;

newalarm[1] = 1;

recruited = false;

temporary = false;

//tablets
tablet[0] = -1;
tablet[1] = -1;
tablet[2] = -1;

newalarm[2] = 5;