{
    "id": "d315da07-af61-4950-9aae-ec9dd53ea7db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "dataCharacter",
    "eventList": [
        {
            "id": "a34fa13a-8297-4407-9e04-a97e18818dcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d315da07-af61-4950-9aae-ec9dd53ea7db"
        },
        {
            "id": "25a3964f-87ed-4ed5-837d-434c33a65405",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d315da07-af61-4950-9aae-ec9dd53ea7db"
        },
        {
            "id": "e6892fa4-f831-4816-a792-6306f4f24128",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d315da07-af61-4950-9aae-ec9dd53ea7db"
        },
        {
            "id": "3647db10-134d-433f-a3f6-424e2a485dba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d315da07-af61-4950-9aae-ec9dd53ea7db"
        },
        {
            "id": "d88c0699-e2f4-4c31-a253-400735a56706",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d315da07-af61-4950-9aae-ec9dd53ea7db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}