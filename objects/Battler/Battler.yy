{
    "id": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Battler",
    "eventList": [
        {
            "id": "23213d2b-50be-4b2d-9068-acdf028a92f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "af9cdbe8-cbc8-4dfa-a90e-ea09aed6c215",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "cc5e49de-da59-4ece-87fe-fed0c07e5ea8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "8778b786-8c9b-44d4-81fa-ea732c9b44ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "d5c6b2dc-0b40-478a-bf4e-8340df361010",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "400a9298-815f-4e10-a5fe-970a61646426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "9d180ff0-24e4-426e-9b7c-d0e685a8dce8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        },
        {
            "id": "621bd1e5-ed8a-4c1f-90e6-2e4242f65b65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "7c7311b6-362a-4cf5-bacb-63dcafb44a2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
    "visible": false
}