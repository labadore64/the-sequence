/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();

battlerUpdateTrig();

battlerStepFlash()

battlerUpdateSpriteAnim();

battlerStepJumpAnimate()

battlerParticleUpdate();
battlerStepDamageParticle();

battlerStepLungeAnimate();

battlerStepDamageAnimate();

battlerStepJump();

battlerStepDefeatAnimate();

battlerStepFleeAnimate();

//attack animation sprite

if(!sprite_attack_complete){
	sprite_attack_current_frame+=menuControl.timer_diff;
	if(sprite_attack_current_frame >= sprite_attack_frames ||
		!global.battleAnimate){
		sprite_attack_complete = true;	
		anim_delay = 1
	}
}

//end attack animation sprite

//start hide animation

if(hide){
	if(hide_alpha != 0){
		hide_alpha -= hide_time*menuControl.timer_diff
		if(hide_alpha < 0){
			hide_alpha = 0;	
		}
	}
} else {
	if(hide_alpha != 1){
		hide_alpha += hide_time*menuControl.timer_diff
		if(hide_alpha > 1){
			hide_alpha = 1;	
		}
	}	
}

if(anim_delay > -1){
	anim_delay--;	
}

//end hide animation

image_speed = sprite_default_speed * sprite_speed_multiplier;
active_filter_frame+=menuControl.timer_diff;

sprite_scale = character_scale * modifier_scale 
