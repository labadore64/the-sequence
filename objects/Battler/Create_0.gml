/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
// the enemy being targeted
targeting_enemy = noone;
defended_this_turn = false;
can_move = true;

sprite_anim_map = ds_map_create();
sprite_anim_counter = -1;

//inits important values
sprite_scale = .5;

character_scale = 1;
modifier_scale = 1;

is_hurting = false;

fleeing = false; //fleeing animation
flee_counter = 0;
flee_dist = 0;
flee_speed = 8;
flee_time = 60
flee_spacer = 1/flee_time;
flee_alpha = 1;
flee_complete = false;
flee_delay = 0;

shadow_y = 0;
dont_draw = false;


battleInitSpriteVars()
ax_desc = "";
primary_color = false; //if true, adds an extra turn to color changing


overworld_character = -1;
load_id = -1; //which order is it loaded in
for(var i = 0; i < 3; i++){
	flavor_text[i] = ""	
}


ai_script_id= ai_script_attack;
ai_script_target_id = ai_target_script_default;
bribe = 0
ability = ""

damage_this_turn = 0;
damage_calc = 0;

base_orientation = 0; //original orientation
start_orientation = 0;
orientation = 0;
is_enemy = false;
character = 0;
is_symmetric = false;
jump_attack = true; //does it jump for attacks?
jump_hit = true; //does it jump when hit?

//drawing vars
backside = false; //drawing the backside of the sprite
flip = false; //whether or not to flip the sprite
curr_scale = 1; //current scale of drawing
y_shadow = 0; //adjust for shadow.
draw_shadow = true; //whether or not shadow should be drawn.

//constants
var base_scale = .75

maxxsize = 1.1*base_scale //maximum size of the sprite
minsize = .5*base_scale //minimum size of the sprite

max_y_pos = sin(degtorad(90))*BattleHandler.v_size + BattleHandler.origin_y;
min_y_pos = sin(degtorad(270))*BattleHandler.v_size + BattleHandler.origin_y;

diff_y_pos = max_y_pos - min_y_pos;

distance = 1;

battlerInitSprite()
battlerInitFlash();
battlerInitJump();
battlerInitParticles();
battlerInitLunge();
battlerInitStats();

//animation variables
damage_time = -10000; //how much time to wait until damage animation is triggered
anim_knockback = false; //should there be knockback?
in_base_position = true

jump_time_anim = -1; //how much time to wait until jump animation is triggered
jump_time_duration = 0; //how much time the jump takes.
jump_time_height = 0;

//load stuff
newalarm[0] = 1;

moves = ds_list_create();

//do this later. but put it in for now so its easier to implement
attacks_per_turn = 1;
attacks_counter = 0;

ai_script = ai_script_attack;
ai_target_script = ai_target_script_default

defeat_animation_counter = 0;
defeat_animation_time = 20
defeat_animation_trigger = false;
defeat_animation_complete = false;
defeat_animation_alpha = 1;

sprite_default_speed = 1;
sprite_speed_multiplier = 1;

ability = "";
battle_desc = "";

draw_hp = false;
draw_hp_val = 1;

draw_mp = false;
draw_mp_val = 1;
hp_size = 0;

active_filter = ""; //determines which filter to display on the character
active_filter_index = 0; //which filter index to display
active_filter_sprite = -1; //determines which filter sprite to draw over the character
active_filter_time = 120; //how long it takes to change the displaying filter

active_filter_frame = 0;

//item drops
drop_normal = -1
drop_talk = -1
drop_betray = -1
drop_bribe = -1
drop_megabribe = -1
drop_special = -1

//item drop states
trigger_drop_talk = false;
trigger_drop_betray = false;
trigger_drop_bribe = false;
trigger_drop_megabribe = false;
trigger_drop_special = false;

//money/bribe
money = 0;
bribe = 0; //max number of money for it to be bribed away. Only goes away if bribed, not just if holding money. If -1, it will never be bribed away.
bribed = false;

exp_multiplier = 1;
trigger_disappear_anim = false;

newalarm[1] = 1;
newalarm[3] = 1;

flash_once = false;
flash_once_counter = 0;
flash_once_time = 30

hide = false;
hide_alpha = 1;
hide_time = 1/15;

talk_counter = 0;
talk_max = 0;

talk_x = 0;
talk_y = 0;

last_move = -1;
last_target = -1;

shader_to_use = -1;
shader_uniform_alpha = -1;

sprite_change_later = -1;
flinch = false;

moves_used = ds_list_create();
filter_add_later = -1

talk_cancel = false;

// tablets
tablet[0] = -1;
tablet[1] = -1;
tablet[2] = -1;

for(var i = 0; i < 3; i++){
	tablet_char[i] = ""
}

// if -1, tablet is available to use.
// otherwise indicates the number of turns before the tablet recovers.
tablet_recover[0] = -1;
tablet_recover[1] = -1;
tablet_recover[2] = -1;

damage_type_mulitplier = 1;

death_cry = -1;

death_zoom = -1;

battlerParticleInitBank();

anim_delay = -1;

//battlerParticleAddEffect(1,200)