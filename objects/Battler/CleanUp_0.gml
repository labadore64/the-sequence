/// @description Insert description here
// You can write your code in this editor
part_system_destroy(damage_part_system);

ds_list_destroy(stat_boosts );
ds_list_destroy(status_effects);

var myid = id;

with(BattleHandler){
	if(ds_exists(party_list,ds_type_list)){
	ds_list_delete(party_list,ds_list_find_index(party_list,myid));
	}
	if(ds_exists(enemy_list,ds_type_list)){
	ds_list_delete(enemy_list,ds_list_find_index(enemy_list,myid));
	}
}

ds_list_destroy(moves);

part_type_destroy(part_death1);
part_type_destroy(part_death2);

ds_list_destroy(moves_used);

ds_map_destroy(sprite_anim_map);

soundFreeCry(death_cry);

battlerParticleReleaseAll();