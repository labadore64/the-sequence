/// @description Insert description here
// You can write your code in this editor
newalarm[1] = active_filter_time;
active_filter_index++;
if(ds_list_size(status_effects)-1 < active_filter_index){
	active_filter_index = 0;	
}

shader_to_use = -1;
shader_uniform_alpha = -1

if(ds_list_empty(status_effects)){
	active_filter = "";
	active_filter_sprite = -1;
	sprite_speed_multiplier = 1
} else {
	
	if(active_filter_index >= ds_list_size(status_effects)){
		active_filter_index = 0;	
	}
	var filter = status_effects[|active_filter_index];
	var original_index = active_filter_index;
	var sizer = ds_list_size(status_effects);
	var cando = true;
	
	if(instance_exists(filter)){
		active_filter = filter.name;
		active_filter_sprite = filter.sprite;
	}
	
	if(battlerHasFilter(id,"damp")){
		shader_to_use = shd_filter_damp;	
		
	}
	
	if(battlerHasFilter(id,"dry")){
		shader_to_use = shd_filter_dry;	
		
	}
	
	if(battlerHasFilter(id,"poison")){
		shader_to_use = shd_filter_poison;	
		
	}
	if(battlerHasFilter(id,"stone")){
		sprite_speed_multiplier = 0;
		shader_to_use = shd_greyscale;
	}
	
	if(shader_to_use != -1){
		shader_uniform_alpha = shader_get_uniform(shader_to_use,"alpha");
	}
	
}
