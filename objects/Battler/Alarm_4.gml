/// @description Insert description here
// You can write your code in this editor
if(audio_is_playing(death_cry)){
	newalarm[4] = 3;	
} else {
	battlerDeathParticleTrigger();	
	defeat_animation_trigger = true;
	var sound = soundfxPlay(soundBattleDefeatBattler);
	audio_sound_pitch(sound,random_range(.9,1.1));
	
	with(battleStartAttack){
		lock = true;
		newalarm[8] = 30;
	}
}