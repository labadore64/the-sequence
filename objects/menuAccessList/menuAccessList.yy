{
    "id": "db84dfa8-06d8-4a41-be51-8ef8a7b06b90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuAccessList",
    "eventList": [
        {
            "id": "9beb444d-6d9c-4636-9b83-fad2947655e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db84dfa8-06d8-4a41-be51-8ef8a7b06b90"
        },
        {
            "id": "0f89d8b0-2b10-4c83-aeef-ebd7e8389965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "db84dfa8-06d8-4a41-be51-8ef8a7b06b90"
        },
        {
            "id": "f86a1b81-6840-41b7-8810-a7b3087743ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "db84dfa8-06d8-4a41-be51-8ef8a7b06b90"
        },
        {
            "id": "114db19a-fcfa-4d57-9f5b-b629eb96ed87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "db84dfa8-06d8-4a41-be51-8ef8a7b06b90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}