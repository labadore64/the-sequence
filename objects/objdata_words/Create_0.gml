/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\word\\word";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] = string_lower(ini_read_string_length("data","name","a",IMPORT_MAX_SIZE_BRAILLEWORD));
	desc[itemcount] = newline(ini_read_string_length("data","desc","",IMPORT_MAX_FLAVOR_TEXT));
	move[itemcount] = ini_read_real("data","move_id",0);

	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;
