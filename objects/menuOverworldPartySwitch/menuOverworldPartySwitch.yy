{
    "id": "f7165250-6823-4c86-abd9-82694ee7f134",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartySwitch",
    "eventList": [
        {
            "id": "ca6077f3-93f5-4fb5-9e7e-382fba2c08ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7165250-6823-4c86-abd9-82694ee7f134"
        },
        {
            "id": "2f11e85d-2cd2-4433-8635-46612ff5e294",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f7165250-6823-4c86-abd9-82694ee7f134"
        },
        {
            "id": "b6c82d4d-c06b-4f08-b7af-b0c1fc0e1835",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7165250-6823-4c86-abd9-82694ee7f134"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f126f65-0956-4831-9210-5223e85c1baf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}