{
    "id": "355423a4-2c8b-4196-b138-de8832b0f587",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartyStatsSub",
    "eventList": [
        {
            "id": "64071785-f6b8-4810-826a-9444702cff63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "355423a4-2c8b-4196-b138-de8832b0f587"
        },
        {
            "id": "82e1fdd1-a247-4547-a78b-a941f6719e8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "355423a4-2c8b-4196-b138-de8832b0f587"
        },
        {
            "id": "9b840ea1-8d3e-46b5-a277-da2983790c71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "355423a4-2c8b-4196-b138-de8832b0f587"
        },
        {
            "id": "e3e06031-85fc-4c53-9dd1-881b77ccff3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "355423a4-2c8b-4196-b138-de8832b0f587"
        },
        {
            "id": "110d58c9-28f7-4463-8f4d-681931bb2d2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "355423a4-2c8b-4196-b138-de8832b0f587"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}