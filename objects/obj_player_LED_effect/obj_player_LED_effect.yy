{
    "id": "a2144661-7411-4e38-b5bc-46f03d755a43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_LED_effect",
    "eventList": [
        {
            "id": "4cde4b0f-8037-486f-9385-e57bc85cdaf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2144661-7411-4e38-b5bc-46f03d755a43"
        },
        {
            "id": "bc7fb0b7-6ba7-435a-8833-d1299541baf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a2144661-7411-4e38-b5bc-46f03d755a43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "c9c22ead-bad6-4feb-abf7-e4a34e4d60a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "57929f1b-c79a-472b-858f-6653a6aa0fff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}