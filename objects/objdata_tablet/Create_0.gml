/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\tablet\\tablet";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data


	ini_open(testname)
	
	name[itemcount] = ini_read_string_length("tablet","name","a",IMPORT_MAX_SIZE_NAME);
	desc[itemcount] = newline(ini_read_string_length("tablet","desc","a",IMPORT_MAX_FLAVOR_TEXT));
	char[itemcount] = ini_read_string_length("tablet","char","a",IMPORT_MAX_SIZE_CHAR);
	move_id[itemcount] = ini_read_real("tablet","move_id",1);
	phy_power[itemcount]  = ini_read_real("tablet","stat_attack",1);
	phy_guard[itemcount] = ini_read_real("tablet","stat_defense",1);
	mag_power[itemcount] = ini_read_real("tablet","stat_magic",1);
	mag_guard[itemcount] = ini_read_real("tablet","stat_resistance",1);
	spe_power[itemcount] = ini_read_real("tablet","stat_agility",1);
	spe_guard[itemcount] = ini_read_real("tablet","stat_mpr",1);

	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

brailleInitAlphaList();

data_size = itemcount;

// also load the braille file info
var filename = working_directory + "braille\\brailleinfo";
var ext = ".ini";
var testname = "";

testname = filename+ext;

ini_open(testname)

var i = 0;

while(true){
	if(!ini_key_exists("files", "bfile_file"+ string(i))){
		break;
	}
	i++
}

brailletype_file[0] = "";
brailletype_title[0] = ""; 
var counter = 0;

for(var j = 0; i > j; j++){
	counter++;
	brailletype_file[j] = ini_read_string_length("files","bfile_file"+string(j),"",IMPORT_MAX_SIZE_BRAILLEFILENAME);
	brailletype_title[j] = ini_read_string_length("files","bfile_name"+string(j),"",IMPORT_MAX_SIZE_BRAILLEFILENAME);
	brailletype_columns[j] = ini_read_real("files","bfile_columns"+string(j),8)
}

braille_types_size = counter;

ini_close();