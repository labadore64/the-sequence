{
    "id": "a831dcc0-5fdc-4b4a-8efe-eec66c50e099",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_house1_closed",
    "eventList": [
        {
            "id": "97846c7a-78b2-472e-a1d1-12e983f995ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a831dcc0-5fdc-4b4a-8efe-eec66c50e099"
        },
        {
            "id": "8797c2a0-ac0a-42fe-bead-a34c78bc05c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "a831dcc0-5fdc-4b4a-8efe-eec66c50e099"
        },
        {
            "id": "de386301-f57d-4706-883b-77c3c3098216",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a831dcc0-5fdc-4b4a-8efe-eec66c50e099"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6df9a92f-b158-4801-9eb0-4ccadf8ccf48",
    "visible": true
}