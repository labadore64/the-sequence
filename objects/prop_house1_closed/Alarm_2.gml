/// @description Insert description here
// You can write your code in this editor
if(eventNormalCheck(event_id)){
	with(door_collision){
		active = false;	
	}
	with(door_open_collision){
		active = true;	
	}
	with(text_object){
		instance_destroy();	
	}
	sprite_index = spr_prop_yourHouse
} else {
	with(door_collision){
		active = true;	
	}
	with(door_open_collision){
		active = false;	
	}	
}