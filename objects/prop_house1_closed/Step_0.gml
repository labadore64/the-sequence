/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(!triggered){
	if(eventNormalCheck(event_id)){
		triggered = true;
		with(door_collision){
			active = false;	
		}
		with(door_open_collision){
			active = true;	
		}
		with(text_object){
			instance_destroy();	
		}
		sprite_index = spr_prop_yourHouse
	}
}