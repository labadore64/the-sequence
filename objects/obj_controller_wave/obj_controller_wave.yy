{
    "id": "bf416d46-9efd-4eb0-b9fb-94c753e8647c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_wave",
    "eventList": [
        {
            "id": "0607a4e6-a5cf-4eda-8e23-f45772d3aee8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf416d46-9efd-4eb0-b9fb-94c753e8647c"
        },
        {
            "id": "bd9b546b-74e1-43f8-9143-83f1dbad2183",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf416d46-9efd-4eb0-b9fb-94c753e8647c"
        },
        {
            "id": "72d8926a-deca-45e6-bdea-b1fd8c675bc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bf416d46-9efd-4eb0-b9fb-94c753e8647c"
        },
        {
            "id": "a9f65025-d3a6-4fd2-a6ee-a10524911fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "bf416d46-9efd-4eb0-b9fb-94c753e8647c"
        },
        {
            "id": "ad2ccbf3-bf65-4fd9-b519-284596e5fe65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "bf416d46-9efd-4eb0-b9fb-94c753e8647c"
        },
        {
            "id": "cca74d3d-194c-4098-ae24-ee70c7cd2722",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "bf416d46-9efd-4eb0-b9fb-94c753e8647c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}