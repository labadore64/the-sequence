{
    "id": "3444cdc2-0acd-4efa-a40b-8aa347d755a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleZoom",
    "eventList": [
        {
            "id": "25ef7a21-c5a3-419c-8e94-481c6fc0aaca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3444cdc2-0acd-4efa-a40b-8aa347d755a5"
        },
        {
            "id": "c99bf2fa-d4e9-4e85-946d-f7c75792373b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3444cdc2-0acd-4efa-a40b-8aa347d755a5"
        },
        {
            "id": "bafbc004-ba00-446e-80ce-bdfd3890e365",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3444cdc2-0acd-4efa-a40b-8aa347d755a5"
        },
        {
            "id": "11962409-44ce-46f4-a96b-567d4e89bcee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3444cdc2-0acd-4efa-a40b-8aa347d755a5"
        },
        {
            "id": "9864885a-2904-488f-a148-5488b884a654",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3444cdc2-0acd-4efa-a40b-8aa347d755a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}