/// @description Insert description here
// You can write your code in this editor
initTimingAlarm()
camera = view_get_camera(0);

var zoomstartx = 0;
var zoomstarty = 0;
var zoomstartw = 1;
var zoomstarth = 1;

with(BattleHandler){
	zoomstartx = zoomstart_x
	zoomstarty = zoomstart_y
	zoomstartw = zoomstart_w
	zoomstarth = zoomstart_h
}


zoomstart_x = zoomstartx
zoomstart_y = zoomstarty
zoomstart_w = zoomstartw
zoomstart_h = zoomstarth
	
zoomend_x = 0;
zoomend_y = 0;
zoomend_w = 100;
zoomend_h = 100;

time = 1;
newalarm[0] = 1;
active = false;

zoom_space_x = 0;
zoom_space_y = 0;
zoom_space_w = 0;
zoom_space_h = 0;