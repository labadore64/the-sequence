/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

y-=30;

ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items

// tablets to choose from

for(var i = 0; i < 3; i++){
	tablet_choice[i] = -1;
}

menu_left = menuTabletSwitchSelectUp
menu_right = menuTabletSwitchSelectDown

menu_up = menu_left;
menu_down = menu_right;

menu_draw = menuTabletSwitchSelectDraw;

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = global.textColor; //color of the title of the info box
info_subtitle_color = global.textColor; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

menuParentUpdateBoxDimension();

menu_help = "tabletsSub"

ignore_tab = true;