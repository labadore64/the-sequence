{
    "id": "bf45a1d8-362b-4d9c-9d19-0ba01f1433b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTabletSwitchSelect",
    "eventList": [
        {
            "id": "0ec47eb7-76e0-4a1b-bdac-92964ccfb10f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf45a1d8-362b-4d9c-9d19-0ba01f1433b5"
        },
        {
            "id": "3d6a4d9d-5abe-4958-b9c9-40dab689dcee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bf45a1d8-362b-4d9c-9d19-0ba01f1433b5"
        },
        {
            "id": "c968cf71-8825-428d-823b-ca82d936e09b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "bf45a1d8-362b-4d9c-9d19-0ba01f1433b5"
        },
        {
            "id": "b144ae8b-a1ff-4b66-8b51-ffb556a62897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf45a1d8-362b-4d9c-9d19-0ba01f1433b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00e8f880-ea72-49e5-8a60-2de5525111a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}