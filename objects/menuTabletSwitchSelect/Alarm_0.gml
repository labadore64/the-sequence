/// @description Insert description here
// You can write your code in this editor


braille_obj = instance_create(
					170,
					70,
					brailleCharacter);
braille_obj.display_character = false;
braille_obj.cell_scale = 2;
braille_obj.cell_trans_time =0;

b_bcharacter = string_upper(braille_character);

brailleDataSetCharacter(braille_character,braille_obj);	

for(var i = 0; i < 3; i++){
	tablet_choice[i] = character.tablet[i];
}

if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	ds_list_clear(cando_list);

	var len = ds_list_size(obj_data.party)

	var cando = false;
	var namera = "";
	var desccc = "";
	var charaaa = -1;
	
	for(var i = 0; i < 3; i++){
		cando = false;
		namera = "--"
		desccc = "";
		charaaa = obj_data.party[|i];
		if(tablet_choice[i] != -1){
			namera = objdata_moves.name[objdata_tablet.move_id[tablet_choice[i]]]
			desccc = objdata_moves.flavor[objdata_tablet.move_id[tablet_choice[i]]]
		}
		menuAddOption(namera,desccc,menuTabletSwitchSelectSelect)

		ds_list_add(cando_list,true);
	}
	
	menupos = 0;
	menu_size = menuGetSize();
	character_id = -1;
	receive_id = character.character;
}

menuParentUpdateBoxDimension();

menuTabletSwitchSelectUpdate();
// Inherit the parent event
active = true;

menuParentSetTitle("Swap " + string_upper(braille_character) + "\nwith what?");
menuParentSetSubtitle("");

tts_title_string = title;

tts_clear();
if(tts_title_string != ""){
	tts_say(tts_title_string)	
}