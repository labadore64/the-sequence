{
    "id": "ad68764d-d7ae-4b31-b0f5-7a9e15c57cc1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartySub",
    "eventList": [
        {
            "id": "30358f81-c867-4644-9cca-4a548ddd7558",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad68764d-d7ae-4b31-b0f5-7a9e15c57cc1"
        },
        {
            "id": "de70f0ea-3e8f-4604-987a-e8bc1f91dbe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ad68764d-d7ae-4b31-b0f5-7a9e15c57cc1"
        },
        {
            "id": "9f026dd9-fee1-4313-891a-bd5f71a1a82c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "ad68764d-d7ae-4b31-b0f5-7a9e15c57cc1"
        },
        {
            "id": "21710c20-7e2f-45fb-840c-b0a08a7cc010",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "ad68764d-d7ae-4b31-b0f5-7a9e15c57cc1"
        },
        {
            "id": "b346a533-dc69-4140-9654-c6962f4b5495",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad68764d-d7ae-4b31-b0f5-7a9e15c57cc1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}