{
    "id": "f3a28abb-bfec-4164-b9b6-0ccf0c21c917",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_pawsitivityPoster",
    "eventList": [
        {
            "id": "3c806a7b-f10e-4ec2-99aa-f193a16739a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3a28abb-bfec-4164-b9b6-0ccf0c21c917"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd82fead-7f2b-4e18-bcec-4c0e065080d7",
    "visible": true
}