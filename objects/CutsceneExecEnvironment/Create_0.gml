/// @description Insert description here
// You can write your code in this editor
//this object represents the execution environment.

//note: the execs and pointers will only have 1 of each in most situations.
//they will only have more if there are headers running cocurrently.
execs = ds_list_create(); //a list of headers that are being executed.
pointers = ds_list_create(); //a list of pointers that tells where the current execution is.

parent = noone; //the parent execution environment