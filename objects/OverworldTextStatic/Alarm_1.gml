/// @description Insert description here
// You can write your code in this editor
/*
p_x1 = x3+center_x
p_y1 = y3+center_y

p_x2 = x4+center_x
p_y2 = y4+center_y

p_x3 = x7+center_x
p_y3 = y7+center_y

p_x4 = x8+center_x
p_y4 = y8+center_y
*/

var instancetype = prop_bone;

instance_create(p_x1,p_y1,instancetype)
instance_create(p_x2,p_y2,instancetype)
instance_create(p_x3,p_y3,instancetype)
instance_create(p_x4,p_y4,instancetype)

with(instancetype){
	depth += 1000
	image_xscale = .1
	image_yscale = .1
	draw_scalex = image_xscale
	draw_scaley = image_yscale
	sprite_index = spr_prop_test;
}