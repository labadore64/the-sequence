{
    "id": "382b61d9-b7ef-464b-9788-5f1bea9842a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OverworldTextStatic",
    "eventList": [
        {
            "id": "c44672ca-1474-4189-ae43-c8a576832ce2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "382b61d9-b7ef-464b-9788-5f1bea9842a7"
        },
        {
            "id": "6f243deb-4763-46b1-8fd2-775bdbc5a81e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "382b61d9-b7ef-464b-9788-5f1bea9842a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c265d5f-787e-42eb-a640-da8f9fd6855f",
    "visible": false
}