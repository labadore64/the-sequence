{
    "id": "94613b94-e865-4405-8ab0-c2b93eba39c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartyItemsSub",
    "eventList": [
        {
            "id": "39b1db22-0ff1-4e40-9d3d-def2e0cf08bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94613b94-e865-4405-8ab0-c2b93eba39c6"
        },
        {
            "id": "2928c528-3435-4a51-8f3e-bbb507a63846",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "94613b94-e865-4405-8ab0-c2b93eba39c6"
        },
        {
            "id": "5bda5cca-13f2-437f-87e0-49b05f033506",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94613b94-e865-4405-8ab0-c2b93eba39c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}