{
    "id": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "introGraphicMain",
    "eventList": [
        {
            "id": "f87396ba-691a-4a87-9eb5-bc4d8b689c9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "ef4281a4-d8f2-4593-8fd3-f0b44fb79441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "e675890f-3a19-439a-bbfd-06cbe3074844",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "81342057-4869-4f58-a29a-b1f25f073c56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "28cb225d-629b-448b-8e77-70b65b71fc21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "2904b1bb-651b-44cc-b7a9-0369b5cf5f7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "20062dbf-5fef-4f40-8f26-44efb5f82f70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "b487174b-0a74-4745-be2b-b87fd4d5ded1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "5c0f2007-91fa-4135-b98d-408138b67b8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "fdd8b2be-f159-4c9b-984d-e23958331550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "70071b18-774e-4991-b3dd-3a08cb629866",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        },
        {
            "id": "6190ab43-7697-46ab-8f93-81f4417459c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "0b0131a9-ce28-44c9-b759-2a0a0fda4e34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}