/// @description Insert description here
// You can write your code in this editor
if(visible){
	if(!surface_exists(surf)){
		surf = surface_create_access(global.window_width,global.window_height)	
	} else {
		if(ScaleManager.updated){	
			surface_resize(surf,global.window_width,global.window_height);
		}
	}
	surface_set_target(surf)
	draw_clear(c_black)

	with(brailleGameBackground){
		draw_surface(surf,0,0);
	}

	brailleDisplayDraw(brailledisplay);
	brailleDisplayDraw(brailledisplay);
	draw_set_color(c_white)
	if(anim_complete){

		draw_set_font(global.largeFont)
		draw_set_halign(fa_center)
		draw_set_color(c_white)
		draw_text_transformed_ratio(400,300-25,"The Sequence",3,3,0)
	}

	if(display_subtitle){
		draw_set_font(global.normalFont);
		draw_text_transformed_ratio(400,300+50,"Press any key",2,2,0)
	}

	if(do_subtitle){
		draw_set_font(global.normalFont);
		draw_set_halign(fa_right)
		draw_text_transformed_ratio(780,580,"Created by PunishedFelix",1,1,0)
		draw_set_halign(fa_left)
	}

	surface_reset_target();

	drawSurfAsVCR(surf)

	draw_letterbox();
}