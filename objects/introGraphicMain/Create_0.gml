/// @description Insert description here
// You can write your code in this editor
initTimingAlarm()
soundPlaySong(music006,30)
surf = -1
event_inherited()
depth = 0;
image_speed = 5
active = false;
display_subtitle = false;
display_flash_time = 20

do_subtitle = false;

room_end = gameload_room//CinderForestYourHouseRoom2; //what room to jump to after title
/*
initPlayer();
with(Player){
	active = false;
	visible = false;
}*/

anim_text_leng = 30;

dest_pos = 275;
start_pos = -250;

current_pos = 0;
pos_spacer = 1

anim_trigger = false;
anim_complete = false;

pos_spacer_multi = .95
pos_spacer_min = 1;

fade_in_script = introFadeInScript;
//fade_out_script = -1;

newalarm[4] = 30;

brailledisplay = instance_create(0,240,brailleDisplayText);

with(AXManager){
	keyboard_keybind_load("keyboard.json");
	gamepad_keybind_load("gamepad.json")
}
	
var obj = instance_create(0,0,brailleGameBackground);
obj.draw_me = false;

mouseHandler_add(0,0,800,600,intro_startscreen_fadeout,"")