{
    "id": "69e4cdca-0d87-41d9-b29d-a8307a6a0b6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "HUDNavigator",
    "eventList": [
        {
            "id": "02055cfa-26c2-43e0-8da7-96713ab12b1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "69e4cdca-0d87-41d9-b29d-a8307a6a0b6b"
        },
        {
            "id": "fd336f5f-9c72-4386-9bf7-5387032f30b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69e4cdca-0d87-41d9-b29d-a8307a6a0b6b"
        },
        {
            "id": "273fbf7c-7dd6-4453-bd6d-ef5288e5dbcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "69e4cdca-0d87-41d9-b29d-a8307a6a0b6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}