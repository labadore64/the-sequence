/// @description Insert description here
// You can write your code in this editor

updateTimingAlarm();
gml_pragma("forceinline");

if(destroy > 0){
	destroy--;
} else if(destroy == 0){
	instance_destroy();
}

if(objGenericCanDo()){

	//get axis data
	//objGenericDoGamepad()

	// pass through the keys to see if any have been pressed.
	if(current_object > -1){
		var len = array_length_1d(current_object.key_id);
		for(var i =0; i < len; i++){
			if(!keypress_this_frame){
				var keybindvar = keyboard_bind_get(current_object.key_id[i])
				if(keybindvar > -1){
					var gamepadvar = gamepad_bind_get(current_object.key_id[i])
					if(gamepad_button_check_pressed(global.gamepad_map,gamepadvar) || 
						keyboard_check_pressed(keybindvar)){
							HUDObjectDoFunc(current_object.key_func[i],current_object.key_arg[i],current_object.key_req[i])

							keypress_this_frame = true;
					}
				}
			}
		}
	}
	
	if(HUDController.infokey_id > -1){
		// debug info keys
		//if(!keypress_this_frame){
			//infokey_keycheck();
		//}	
	}

	if(menu_help != ""){
		if(global.inputEnabled){
			if(!keypress_this_frame){
				if(global.key_state_array[KEYBOARD_KEY_HELP] == KEY_STATE_PRESS ||
				global.gamepad_state_array[KEYBOARD_KEY_HELP] == KEY_STATE_PRESS){
						createHelpMenu(menu_help)
						keypress_this_frame = true;
				}
			}
		}
	} 
	
	keypress_this_frame = false;
}