{
    "id": "cf31167b-22c1-4a6e-833e-585694929681",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleStartAnimation",
    "eventList": [
        {
            "id": "d57b8a05-9fce-41b3-8b05-6d8a08de1227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf31167b-22c1-4a6e-833e-585694929681"
        },
        {
            "id": "720c8590-bc48-4dd1-92bf-df618b627663",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cf31167b-22c1-4a6e-833e-585694929681"
        },
        {
            "id": "c5b5d621-17f8-44f4-a58f-c8485827d1e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "cf31167b-22c1-4a6e-833e-585694929681"
        },
        {
            "id": "ef6ccbc0-9e8e-4f44-b78f-b2415a5b55c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cf31167b-22c1-4a6e-833e-585694929681"
        },
        {
            "id": "8cbaf835-f468-4aed-a312-9552e08642de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "cf31167b-22c1-4a6e-833e-585694929681"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}