/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();

with(obj_data){
	dataSave();
}

time = 55;

newalarm[0] = time;
newalarm[1] = 1

drawit = true

var mylist = ds_list_create();

with(BattleHandler){
	ds_list_copy(mylist,enemy_list);
}

entry_text = battleStartGetText(mylist);

ds_list_destroy(mylist)

tts_say(entry_text);
battleTextDisplaySetText(entry_text);

if(global.battleRotate){
	battleHandlerSpin(-(285+90),time,.94)
} else {
	with(BattleHandler){
		orientation = 40;	
	}
}

top_point = 300;
bottom_point = 300;

spacer = 300/time
fadeOut(time,c_white)

fade_out_script = -1;
fade_in_script = -1;

battleZoomDefault(time)

