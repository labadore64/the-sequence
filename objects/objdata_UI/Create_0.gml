/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\ui\\hud";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] =ini_read_string_length("object","name","main",IMPORT_MAX_SIZE_UI_NAME);
	read_text[itemcount] = ini_read_string_length("object","text","main",IMPORT_MAX_SIZE_UI_READ_TEXT);
	ui_id[itemcount] = ini_read_string_length("object","id","id",IMPORT_MAX_SIZE_UI_ID);
	
	// get children data
	var i = 0;

	while(true){
		if(!ini_key_exists("children", "child_name"+ string(i))){
			break;
		}
		i++
	}
		
	children_strings[itemcount,0] = "";
	children_templates[itemcount,0] = "";
	children_variable[itemcount,0] = "";
		
	for(var j = 0; i > j; j++){
	
		children_strings[itemcount,j]=ini_read_string_length("children", "child_name"+ string(j),"",IMPORT_MAX_SIZE_UI_ID);
		children_templates[itemcount,j]=ini_read_string_length("children", "child_template"+ string(j),"",IMPORT_MAX_SIZE_UI_ID);
		children_variable[itemcount,j]=ini_read_string_length("children", "child_var"+ string(j),"",IMPORT_MAX_SIZE_UI_VARNAME);
		children_customhud[itemcount,j]=ini_read_string_length("children", "custom"+ string(j),"",IMPORT_MAX_SIZE_UI_JSON);
	}
	
	// get key data
		
	key_id[itemcount,0] = -1
	key_func[itemcount,0] = -1
	key_arg[itemcount,0] = ""
	key_req[itemcount,0] = "";

		
	var i = 0;

	while(true){
		if(!ini_key_exists("definition", "key_def"+ string(i))){
			break;
		}
		i++
	}
		
	for(var j = 0; i > j; j++){
	
		key_id[itemcount,j] = ini_read_string_length("definition", "key_def"+ string(j),"",IMPORT_MAX_SIZE_UI_KEY); // id used for the key input
		key_func[itemcount,j] = HUDObjectGetFunc(ini_read_string_length("definition", "key_func"+ string(j),"",IMPORT_MAX_SIZE_UI_KEYFUNC)); // which function is used for the key input (text/move/exit/origin)
		key_arg[itemcount,j] = ini_read_string_length("definition", "key_arg"+ string(j),"",IMPORT_MAX_SIZE_UI_JSON); // argument used for the key function
		key_req[itemcount,j] = ini_read_string_length("definition", "key_req"+ string(j),"",IMPORT_MAX_SIZE_UI_REQ); // argument used for the key function
	}
	
	var j = 0;

	while(true){
		if(!ini_key_exists("infokey", "info"+ string(j))){
			break;
		}
		j++
	}
		
		
	for(var i = 0; i < j; i++){
		infokey[itemcount,i] = ini_read_string_length("infokey", "info"+ string(i),"",IMPORT_MAX_SIZE_UI_READ_TEXT); // argument used for the key function
		infokey_req[itemcount,i] = ini_read_string_length("infokey", "req"+ string(i),"",IMPORT_MAX_SIZE_UI_REQ); // argument used for the key function
	}
		
	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;

