/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
game_obj = brailleGameTimeAttack; //which object type to use for this game mode
braille_obj = noone; //which instance to use for braille
min_value = 30;
max_value = 300;
value_spacer = 30;

unit_string = "Seconds"
title = "Time Attack"

newalarm[2] = 1;

x = 400;
y = 300;

draw_width = 400; //width of box in pixels
draw_height = 250; //height of box in pixels

current_value = 0

menu_left = menuBrailleChallengeQuantityLeft
menu_right = menuBrailleChallengeQuantityRight
menu_up = menuBrailleChallengeQuantityUp
menu_down = menuBrailleChallengeQuantityDown

menu_select = menuBrailleChallengeQuantitySelect

menu_draw = menuBrailleChallengeQuantityDraw

menuParentUpdateBoxDimension();

sprite_counter = 0;

mouseHandler_clear();

var sizezr = 25;

mouseHandler_add(x+80-sizezr,y+32-sizezr,x+80+sizezr,y+32+sizezr,menuBrailleChallengeQuantityRight,"Increase")
mouseHandler_add(x-90-sizezr,y+32-sizezr,x-90+sizezr,y+32+sizezr,menuBrailleChallengeQuantityLeft,"Decrease")
		draw_sprite_extended_ratio(spr_scroll_right,sprite_counter,x-90,y+32,-.5,.5,0,global.textColor,1);
		
var xxpo = 395;
var yypo = 395

mouseHandler_add(xxpo-40-3,yypo-22-3,xxpo+40+3,yypo+22+3,menuBrailleChallengeQuantitySelect,"Go")
