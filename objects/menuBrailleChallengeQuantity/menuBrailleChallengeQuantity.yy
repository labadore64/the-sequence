{
    "id": "47640eb4-2a08-47b2-9c6f-985436b7fcf0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBrailleChallengeQuantity",
    "eventList": [
        {
            "id": "f487f315-564d-44a2-b2db-fc9ced703fba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47640eb4-2a08-47b2-9c6f-985436b7fcf0"
        },
        {
            "id": "232c7945-b247-41b6-988b-ff03cb335b76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "47640eb4-2a08-47b2-9c6f-985436b7fcf0"
        },
        {
            "id": "3987be7a-7a6d-4320-8918-dd32ea0ac316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "47640eb4-2a08-47b2-9c6f-985436b7fcf0"
        },
        {
            "id": "833634ac-2563-4919-bf9b-1e118e296a6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "47640eb4-2a08-47b2-9c6f-985436b7fcf0"
        },
        {
            "id": "99db77ec-a7ff-4883-9559-b1f5ee5fccc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "47640eb4-2a08-47b2-9c6f-985436b7fcf0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}