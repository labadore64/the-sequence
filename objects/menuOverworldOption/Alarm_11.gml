/// @description Insert description here
// You can write your code in this editor
tts_title_string = "Options"

// Inherit the parent event
event_inherited();

mouseHandler_menu_default()

if(object_index == menuOverworldOption){
	with(menuOverworldMenus){
		mouseHandler_clear_main()
		mouseHandler_add(0,550,200,600,menuOverworldMenuTabLeft,"Braille")
		mouseHandler_add(600,550,800,600,menuOverworldMenuTabRight,"Options")
	}
}