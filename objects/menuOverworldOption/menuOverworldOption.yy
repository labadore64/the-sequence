{
    "id": "54d47214-be92-472f-bc26-a9201d2ea2d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldOption",
    "eventList": [
        {
            "id": "7eeed2b1-3efd-4a92-8fa0-6dbb3187b24c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54d47214-be92-472f-bc26-a9201d2ea2d3"
        },
        {
            "id": "80b321de-ad59-42ae-870e-ce403d61e653",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "54d47214-be92-472f-bc26-a9201d2ea2d3"
        },
        {
            "id": "01f958d0-0ef1-48ba-97a0-57081c3a3e0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "54d47214-be92-472f-bc26-a9201d2ea2d3"
        },
        {
            "id": "ac06d08e-81f1-485c-94ad-df2d12b62441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "54d47214-be92-472f-bc26-a9201d2ea2d3"
        },
        {
            "id": "b9f6eb34-39d1-444c-b123-3fad8481aa78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "54d47214-be92-472f-bc26-a9201d2ea2d3"
        },
        {
            "id": "c9230b4a-a4de-428a-81e6-bf0fa5cc9cb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54d47214-be92-472f-bc26-a9201d2ea2d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}