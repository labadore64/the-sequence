{
    "id": "47368022-8732-4ba7-85dd-f2b0267b2b1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_table",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "91b30d59-2ec4-40b3-9b65-0501cc9669a9",
    "visible": true
}