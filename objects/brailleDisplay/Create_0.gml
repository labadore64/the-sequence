/// @description Insert description here
// You can write your code in this editor
// how many cells does this display have
initTimingAlarm();

count = 14;

detect_words = false;

// how many characters are displayed per row
row_length = 7;

column_height = ceil(count/row_length);

// spacing for display characters
x_spacing = 50;
y_spacing = 100;

// array for display
braille_display[0] = noone;

// display string
display_string = "";

// how long it takes a cell to update
update_time = 9;

// newalarm for cells
newalarm[0] = 1;

// skips over spaces when inserting,
// so spaces between words are removed
skip_spaces = false;

display_color = c_aqua;
display_alpha = .5;

display_character = true

cell_scale = 1;

surf = -1;

// whether or not to detect words in the string.
detect_words = false

detected_word = "";
detected_length = 0;
preview_word[0] = false;

image_blend = global.textColor