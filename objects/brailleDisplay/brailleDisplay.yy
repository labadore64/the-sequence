{
    "id": "19943076-b550-4d21-9f00-14427af681b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleDisplay",
    "eventList": [
        {
            "id": "d925bd68-b6bc-472b-a430-ff9eaaa5494c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "19943076-b550-4d21-9f00-14427af681b4"
        },
        {
            "id": "eb60e8d6-f0bf-4065-92c3-82883ddb6a09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "19943076-b550-4d21-9f00-14427af681b4"
        },
        {
            "id": "84962a8b-6bf3-450e-9d04-2265909a9b3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "19943076-b550-4d21-9f00-14427af681b4"
        },
        {
            "id": "7aa5b1fc-95fe-4480-af7f-a290fe835679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "19943076-b550-4d21-9f00-14427af681b4"
        },
        {
            "id": "9a21ca3c-91ce-45e8-b927-1ca5f46974a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "19943076-b550-4d21-9f00-14427af681b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}