/// @description Insert description here
// You can write your code in this editor

var row_counter = 0;
var column_counter = 0;

for(var i = 0; i < count; i++){
	var move = 0;
	
	if(!display_character){
		move = -30;	
	}
	
	braille_display[i] = instance_create(
						x_spacing*row_counter*cell_scale,
						move+y_spacing*column_counter*cell_scale,
						brailleCharacter);
	braille_display[i].cell_scale = cell_scale;
	braille_display[i].display_character = display_character;
	braille_display[i].highlight_move_counter = i;
	//braille_display[i].text_color=image_blend;
	braille_display[i].image_blend=image_blend;
	row_counter++;
	if(row_counter >= row_length){
		column_counter++;
		row_counter = 0;
	}
}

brailleDisplayUpdate(id);