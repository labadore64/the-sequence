{
    "id": "d1e9cecb-20e9-491a-abb8-90b73e49cbe1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleAttackInput",
    "eventList": [
        {
            "id": "00b3274a-6bfe-4efb-9ca8-fb03b72d3a26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1e9cecb-20e9-491a-abb8-90b73e49cbe1"
        },
        {
            "id": "4f76d7a8-a973-4393-8210-9b1a5f525184",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d1e9cecb-20e9-491a-abb8-90b73e49cbe1"
        },
        {
            "id": "080ff2f9-0c18-4b6c-b27c-1ba4611b161e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d1e9cecb-20e9-491a-abb8-90b73e49cbe1"
        },
        {
            "id": "7901605f-868e-4570-853f-458d0c242b12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d1e9cecb-20e9-491a-abb8-90b73e49cbe1"
        },
        {
            "id": "5cc1f6ae-b2fc-423e-8f4e-84819b6c6944",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d1e9cecb-20e9-491a-abb8-90b73e49cbe1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}