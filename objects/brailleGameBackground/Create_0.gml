/// @description Insert description here
// You can write your code in this editor
initTimingAlarm()
surf = -1

shader_seed = 0;
shader = shd_background_rainbow;
shader_enabled = shader_is_compiled(shader);
shader_sprite_index = 0;
shader_counter = 0;

//uni_blur_amount_hoz = shader_get_uniform(shd_gaussian_vertical1,"blur_amount");
//uni_blur_amount_vert = shader_get_uniform(shd_gaussian_horizontal1,"blur_amount");
//var_blur_amount = .65;

shd_vcr_seed = shader_get_uniform(shader,"seed");
shd_vcr_counter = shader_get_uniform(shader,"counter");
shd_vcr_u_uv = shader_get_uniform(shader,"u_uv")

// particles

Sname = part_system_create();

var scale = .05;

part2_scale = .25;

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_square);
part_type_scale(particle1,scale,scale);
if(global.disableShader){
	part_type_color1(particle1,$7F7F00);
} else {
	part_type_color1(particle1,16776960);	
}
part_type_alpha3(particle1,0,1,0);
part_type_life(particle1,15,60);
part_type_blend(particle1,true)

emitter1 = part_emitter_create(Sname);
part_emitter_region(Sname,emitter1,
					global.display_x,
					global.window_width,
					global.display_y,
					global.window_height,
					ps_shape_rectangle,
					0);

part_system_automatic_draw(Sname, false)
part_system_automatic_update(Sname,false)

repeat(500){
	part_system_update(Sname)
}

quantity = 40;
draw_me = true;

newalarm[0] = 1