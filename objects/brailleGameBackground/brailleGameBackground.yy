{
    "id": "8453c897-16be-45da-9d1a-be7485726592",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleGameBackground",
    "eventList": [
        {
            "id": "58786409-7fb5-4b8c-8669-bc9577c91d12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8453c897-16be-45da-9d1a-be7485726592"
        },
        {
            "id": "8d383731-86d8-4d8e-b48b-bc086208df18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8453c897-16be-45da-9d1a-be7485726592"
        },
        {
            "id": "4bbf254a-1a88-4a4b-8a88-9761c2591f88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "8453c897-16be-45da-9d1a-be7485726592"
        },
        {
            "id": "84922dc2-fabe-49bc-a660-4cc31a8e981e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8453c897-16be-45da-9d1a-be7485726592"
        },
        {
            "id": "120e37f9-9247-4562-bdbc-826d2b848e34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8453c897-16be-45da-9d1a-be7485726592"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}