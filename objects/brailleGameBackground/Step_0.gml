/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
repeat(menuControl.timer_diff_round){
	part_system_update(Sname)
}

if(ScaleManager.updated){	
	part_emitter_region(Sname,emitter1,
						global.display_x,
						global.display_x+global.letterbox_w,
						global.display_y,
						global.display_y+global.letterbox_h,
						ps_shape_rectangle,
						ps_distr_invgaussian);

				
	part_emitter_stream(Sname,emitter1,particle1,quantity*global.scale_factor);
}

shader_sprite_index+= menuControl.timer_diff
shader_counter+= menuControl.timer_diff