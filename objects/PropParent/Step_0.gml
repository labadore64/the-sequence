/// @description Insert description here
// You can write your code in this editor

// alarms
if(!prop_menu){
	if(newalarm[0] > TIMER_INACTIVE){
		newalarm[0] -= menuControl.timer_diff;	
		if(newalarm[0] < 0){
			event_perform(ev_alarm,0);
			if(newalarm[0] < 0){
				newalarm[0] = TIMER_INACTIVE;
			}
		}
	}

	spr_counter += menuControl.timer_diff*image_speed*.5;
	alarm_done = true;

	// Inherit the parent event
	event_inherited();

	// end update drawing prop details

	if(disappear){
		if(image_alpha > 0){
			image_alpha -= disappear_counter;
		} else {
			instance_destroy();	

		}
	}
}