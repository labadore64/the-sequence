/// @description Insert description here
// You can write your code in this editor
if(event_id != "none"){
	if(eventItemCheck(event_id)){
		instance_destroy();
	}
}

	var width = abs(sprite_bbox_right - sprite_bbox_left)*.5
	var height = abs(sprite_bbox_bottom - sprite_bbox_top)*.5
	
	var size_const = 64

	var scalex = draw_scalex*(width*2)/size_const;
	var scaley = draw_scaley*(height*2)/size_const;
	
	//offset x and y coords based on where the center of the collision box lies
	
	var collide_centerx = sprite_get_bbox_left(sprite_index) + width-size_const;
	var collide_centery = sprite_get_bbox_top(sprite_index) + height-size_const;
	
	var xx = x+collide_centerx;
	var yy = y+collide_centery;	
	
	bounding_box[0] = x - (sprite_width*.5)
	bounding_box[1] = x + (sprite_width*.5)
	bounding_box[2] = y - (sprite_height*.5)
	bounding_box[3] = y + (sprite_height*.5)
	
	if(make_collision){
		child_collision = instance_create(xx,yy,Collision);
		child_collision.image_xscale = scalex;
		child_collision.image_yscale = scaley;
		child_collision.parent = id;
		child_collision.can_push = can_push;
	
		with(child_collision){
			draw_scalex = image_xscale;
			draw_scaley = image_yscale;	
			collisionUpdateVars();
		}
		
		var objid = object_index;
		var check = -1;
		var coll = child_collision;
		//checks if prop is placed 
		with(OverworldItemTrigger){
			check = ds_list_find_value(prop_list,objid)
			if(check != -1){
				//does a collision check with the obj.	
				var cando = collisionTestRectangle(coll,x_left,y_up,x_right,y_down);
				
				if(cando){
					eventNormalSet("event_id")
				}
			}
		}
		
	}
	if(make_ax_label){
		child_ax = instance_create(xx,yy,AXOverworldLabel);
		child_ax.text = ax_label;
		child_ax.ax_name = ax_label;
		child_ax.ax_desc = ax_desc;
		with(child_ax){
		
			image_xscale = scalex*1.1;
			image_yscale = scaley*1.1;	
			scriptInteractGetPoints();
		}
	}
	
	if(make_interact_script){
		if(make_interact_type > 0){
			child_make_interact = instance_create(xx,yy,make_interact_type);
			child_make_interact.text = ax_label;
			child_make_interact.parent = id;
			child_make_interact.item_id = item_id;
			if(make_interact_script_id != -1){
				child_make_interact.menu_interact = make_interact_script_id;	
			}
			with(child_make_interact){
		
				image_xscale = scalex*1.1;
				image_yscale = scaley*1.1;	
				scriptInteractGetPoints();
			}
		}
	}