/// @description Insert description here
// You can write your code in this editor
with(interact_object){
	instance_destroy();	
}

var sizer = ds_list_size(child_sprayPropInteract);
	
var obj = -1

for(var i = 0; i < sizer; i++){
	obj = child_sprayPropInteract[|i];
	if(!is_undefined(obj)){
		if(instance_exists(obj)){
			obj.newalarm[3] = 120;	
		}
	}
}

if(ds_exists(child_sprayPropInteract,ds_type_list)){
	ds_list_destroy(child_sprayPropInteract);	
}

var objid = object_index;

// something to do with deleting props. idk wont let me compile with YYC with it
/*
if(instance_exists(child_collision)){
	with(OverworldItemTrigger){
		if(ds_exists(prop_list,ds_type_list)){
			check = ds_list_find_value(prop_list,objid)
			if(check != -1){
				//does a collision check with the obj.	
				var cando = collisionTestRectangle(coll,x_left,y_up,x_right,y_down);
				
				if(cando){
					//delete event
					eventNormalDelete("event_id")
				}
			}
		}
	}
}
*/

with(child_collision){
	instance_destroy();	
}

with(child_ax){
	instance_destroy();	
}

with(child_make_interact){
	instance_destroy();	
}