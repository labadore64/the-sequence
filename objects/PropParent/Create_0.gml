/// @description Insert description here
// You can write your code in this editor

createPropParent();

child_sprayPropInteract = ds_list_create(); //if there's a connected spray prop interact object.
child_collision = noone; //collision attached to this obj
make_collision = false;
disappear = false;
disappear_time = 20
disappear_counter = 1/disappear_time;
can_push = false;

prop_id = 0;//unique id for prop placement.

prop_push_id = ""; //required to save prop location in save files.

burn_sprite = -1;

sprite_bbox_left = sprite_get_bbox_left(sprite_index);
sprite_bbox_right = sprite_get_bbox_right(sprite_index);
sprite_bbox_top = sprite_get_bbox_top(sprite_index);
sprite_bbox_bottom = sprite_get_bbox_bottom(sprite_index);

make_ax_label = false;
ax_label = "";
ax_desc = "";
child_ax = noone;

make_interact_script = false; //does it allow you to interact with object?
make_interact_type = -1; //object to make
child_make_interact = noone; //child interact object;
make_interact_script_id = -1;

item_id = -1; //item associated with object.

newalarm[1] = 1