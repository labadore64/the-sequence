{
    "id": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "PropParent",
    "eventList": [
        {
            "id": "11e6ad6c-921b-4da1-807d-a44bd4a5f2e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27e55429-e6ad-475a-a1e6-6ac91fe41c69"
        },
        {
            "id": "96ed479a-9bad-40ca-a30c-03c8a70fffdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "27e55429-e6ad-475a-a1e6-6ac91fe41c69"
        },
        {
            "id": "d163dd17-4bd8-41a9-a918-288830f1c28e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "27e55429-e6ad-475a-a1e6-6ac91fe41c69"
        },
        {
            "id": "3c70a0fd-0068-43a1-95d7-1861759a70ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "27e55429-e6ad-475a-a1e6-6ac91fe41c69"
        },
        {
            "id": "76bc6fa7-46f7-4eab-96f3-2dfbf2c055ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27e55429-e6ad-475a-a1e6-6ac91fe41c69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f643c4f4-3b8e-4b62-8fe0-78dc4855e219",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "06366385-c7ed-4853-adf3-b03f0dc8a508",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4fa5b002-ce7f-41fb-9fd5-acd37af00170",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "b0b6603b-35d4-4d4b-b3df-821374d0e503",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 128
        },
        {
            "id": "08596f17-cc8d-4dd6-8da8-3ca9bf1e16cf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 128
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca521c32-5848-4ff7-87f9-dd823bc44496",
    "visible": true
}