{
    "id": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_greyscale",
    "eventList": [
        {
            "id": "da56876e-75e7-4fa9-8c1f-5f8bf313e3cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e"
        },
        {
            "id": "e51ce530-22fe-4acf-b8bf-b12ffe7d3e64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e"
        },
        {
            "id": "413f6b4a-ea53-4d41-886a-104454a62fa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e"
        },
        {
            "id": "fc099126-f679-4f66-9ab0-74d7f00c7267",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e"
        },
        {
            "id": "16f59458-d73a-430b-ab87-d4722537dbc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e"
        },
        {
            "id": "897959d7-b9de-405d-8ba6-32d349a22cfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "eed784dd-f7e3-4d83-9d81-5bdc826d5f0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}