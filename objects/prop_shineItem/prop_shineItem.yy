{
    "id": "3e6e05b4-8f1f-4ee6-b422-8fafce4ce7a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_shineItem",
    "eventList": [
        {
            "id": "9544c197-af65-4ff3-ac53-597d940ca7af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e6e05b4-8f1f-4ee6-b422-8fafce4ce7a7"
        },
        {
            "id": "674c3e9b-7a0a-4720-bdab-67f178b12de9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3e6e05b4-8f1f-4ee6-b422-8fafce4ce7a7"
        },
        {
            "id": "6de506cd-9991-4d72-aec2-5c072d65a6fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "3e6e05b4-8f1f-4ee6-b422-8fafce4ce7a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3a9474f5-58fe-47c9-9a80-5c903da9f420",
    "visible": true
}