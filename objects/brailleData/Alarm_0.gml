/// @description Insert description here
// You can write your code in this editor
var filename = working_directory + "braille\\" + braille_source + ".txt";
var file = file_text_open_read(filename)
var counter = 0;
var val = 0;

var row = 0;

var character = "";
var desc = "";
var row_parse = "";

var characterss = -1;

for(var i = 0; i < 2; i++){
	for(var j = 0; j < 3; j++){
		characterss[i,j] = false;
	}
}

while (!file_text_eof(file))
{
	
	if(counter == 0){
		character =  string_lower(string_replace(file_text_readln(file),"\n",""));
	} 
	else if(counter == 4){
		desc = string_replace(file_text_readln(file),"\n","")
	} else {
		row = counter-1;
		row_parse = string_replace(file_text_readln(file),"\n","");
		if(string_char_at(row_parse,1) == "●"){
			characterss[0,row] = true;
		}
		if(string_char_at(row_parse,3) == "●"){
			characterss[1,row] = true;
		}
	}
	
	counter++;
	val++;
	if(counter == 5){
		ds_map_add(braille_map,character,characterss);
		counter = 0;	
		
		characterss = -1;
		
		for(var i = 0; i < 2; i++){
			for(var j = 0; j < 3; j++){
				characterss[i,j] = false;
			}
		}
		
		if(read_desc){
			ds_map_add(braille_desc,character,desc);	
		}
	}
}
file_text_close(file)