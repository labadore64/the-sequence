{
    "id": "e63d62b3-8223-447e-8836-e11103ff3f54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleData",
    "eventList": [
        {
            "id": "faa2acb5-c257-47bf-b131-e62de26f59ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e63d62b3-8223-447e-8836-e11103ff3f54"
        },
        {
            "id": "ea62bd47-dfcb-419f-9496-812fe021bd19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e63d62b3-8223-447e-8836-e11103ff3f54"
        },
        {
            "id": "92e21cbc-3866-419f-8a2a-a9d112ddc18a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e63d62b3-8223-447e-8836-e11103ff3f54"
        },
        {
            "id": "17a2896d-7e59-4774-97c4-04d7d2be4621",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e63d62b3-8223-447e-8836-e11103ff3f54"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}