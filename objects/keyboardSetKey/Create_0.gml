/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
map = global.keybind_map;

keybind = ""; //key set

active = false;

newalarm[0] = 1;
with(menuOverworldKeyboardOption){
	lock = true;	
}

depth = -1000000

draw_msg = global.langini[4,9]

var center_x = 400;
var center_y = 300;
var sizex = 300*.5;
var sizey = 50*.5;

x1 = center_x - sizex;
x2 = center_x + sizex;
y1 = center_y - sizey;
y2 = center_y + sizey;

keyboard_lastkey = 0;

bg_color = merge_color(global.textColor,c_black,.5)

tts_say(draw_msg);