/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();
if(active){
	if(keyboard_lastkey != 0){
		var oldbinding = keyboard_bind_get(keybind);
		keyboard_bind_set(keybind,keyboard_lastkey);
		
		if(keybind_legal_change(key_get_index(keybind))){
			instance_destroy();
			tts_say(replaceStringWithVars(global.langini[4,10],keybind,keyboardToString(keyboard_lastkey)));
		} else {
			// reset the binding
			keyboard_bind_set(keybind,oldbinding);
		}
	}
}