{
    "id": "f176f39e-7ede-4159-a511-6c7e1ee61edd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_magnify",
    "eventList": [
        {
            "id": "413468d9-9cd9-4d6c-acbf-ec47ae8eb60b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f176f39e-7ede-4159-a511-6c7e1ee61edd"
        },
        {
            "id": "c2d216d1-f616-47b5-b8f7-2dcc3d9a8df9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f176f39e-7ede-4159-a511-6c7e1ee61edd"
        },
        {
            "id": "427c60d2-a012-4631-b708-bf7be073003a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f176f39e-7ede-4159-a511-6c7e1ee61edd"
        },
        {
            "id": "9cdbfd8e-de53-410f-a1ff-fd4f5fd3bfe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "f176f39e-7ede-4159-a511-6c7e1ee61edd"
        },
        {
            "id": "f5debd36-ba23-4278-9a61-458392a00ca6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "f176f39e-7ede-4159-a511-6c7e1ee61edd"
        },
        {
            "id": "853baec6-6b25-40bf-af0e-21e967687702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "f176f39e-7ede-4159-a511-6c7e1ee61edd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}