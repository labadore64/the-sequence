{
    "id": "3c69d1c4-4369-4b17-9068-ecbb47d6563a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_mailboxItem",
    "eventList": [
        {
            "id": "380ee026-def4-4e39-af17-77b63c7ac33d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3c69d1c4-4369-4b17-9068-ecbb47d6563a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dea790a2-2270-46c9-ba5a-399a63e85ac9",
    "visible": true
}