{
    "id": "558eb7b2-a214-4649-aa72-526789cecdef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldItemToss",
    "eventList": [
        {
            "id": "924cfce4-f936-4bf9-b0ed-71ed2829d98d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "558eb7b2-a214-4649-aa72-526789cecdef"
        },
        {
            "id": "92cd048b-f758-4310-8640-55ed617e615e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "558eb7b2-a214-4649-aa72-526789cecdef"
        },
        {
            "id": "bb05074b-ebec-4073-a90c-596cd4376cc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "558eb7b2-a214-4649-aa72-526789cecdef"
        },
        {
            "id": "d96ab94b-87a8-4ab9-ab9a-dc86b09b93c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "558eb7b2-a214-4649-aa72-526789cecdef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}