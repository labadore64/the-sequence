/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

sound_select = soundMenuDefaultMove;
menu_select = menuParentCancel;
menu_cancel = menuParentCancel;
display_text = "Test text";

menu_draw = menuDialogDraw;

x = 400;
y = 300;

menuParentUpdateBoxDimension()

menuParentSetDrawGradient(false);
menuParentSetDrawSubtitle(false);
menuParentSetDrawTitle(false);

destroyWithMe = -1; //if a menu should be destroyed with the dialog