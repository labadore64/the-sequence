{
    "id": "de9dd88e-d28f-42f2-a038-7ea3209cd3db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCallSub",
    "eventList": [
        {
            "id": "dca97795-edda-4df2-8d78-cf9e88fff8f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de9dd88e-d28f-42f2-a038-7ea3209cd3db"
        },
        {
            "id": "a2d6303e-5cc0-4f77-b15d-05f608b867d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "de9dd88e-d28f-42f2-a038-7ea3209cd3db"
        },
        {
            "id": "7736cbaf-2c0a-47cd-87e9-ec938147ed2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de9dd88e-d28f-42f2-a038-7ea3209cd3db"
        },
        {
            "id": "ef32ddf8-4a35-4316-b387-78e2e8fb3ef6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "de9dd88e-d28f-42f2-a038-7ea3209cd3db"
        },
        {
            "id": "e875b526-ad41-4ee5-9f90-eed0bdfab8d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "de9dd88e-d28f-42f2-a038-7ea3209cd3db"
        },
        {
            "id": "e979ad03-7f8a-496d-9de3-87b9c3516fc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "de9dd88e-d28f-42f2-a038-7ea3209cd3db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}