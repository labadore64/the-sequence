/// @description Insert description here
// You can write your code in this editor
var call_counter = 0;
var call_size = ds_list_size(obj_data.characters); 



for(var i = 0; i < call_size; i++){
	if(obj_data.characters[|i].recruited){
		if(ds_list_find_index(obj_data.party,obj_data.characters[|i]) == -1){
			call_counter++
		}
	}
}
menuParentClearOptions();
lock = false;
if(character.character > -1){
	if(ds_list_find_index(obj_data.party,character) != -1){
		if(call_counter > 0){
			menuAddOption("Switch","Switch this character for someone else.",menuCallSubScriptSwitch)
		}
		in_party = true;
	}
	if(character.character != 0  && ds_list_find_index(obj_data.party,character) == -1){
		menuAddOption("Call","Call this character.",menuCallSubScriptCall)
	}
	menuAddOption("Profile","View details.",menuCallSubScriptStats)
}
SHORTHAND_CANCEL_OPTION

menu_size = menuGetSize();