{
    "id": "d44c48a3-6278-4868-b4cb-96906e11ab1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleTextDisplay",
    "eventList": [
        {
            "id": "fc603f61-7b86-4d90-9bc2-f30630ed4058",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        },
        {
            "id": "a6aea62c-4be9-4b25-94da-fab75e92325b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        },
        {
            "id": "b90b455d-2c87-42d3-8607-7560064303d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        },
        {
            "id": "1fabe27c-0d15-44a6-ac5e-440d88bb6347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        },
        {
            "id": "039ddda3-b2c5-417e-9262-6ddcf3cbd335",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        },
        {
            "id": "7e65aa1e-8f72-4b25-be26-1be89bd55642",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        },
        {
            "id": "1e6c3138-d19f-4af6-b17c-c494910f384e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d44c48a3-6278-4868-b4cb-96906e11ab1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}