/// @description Insert description here
// You can write your code in this editor
event_inherited()
initTimingAlarm();
text_list = ds_list_create();
display_list = ds_list_create();
display_orig = 3;
display_number = display_orig; //how many lines of text to show
display_list_size = 0;
display_linesize = 45; //characters per line to draw
display_spacer = 30; //space between lines
display_animation = false;
display_animation_counter = 0;
display_animation_time = 7;
display_animation_spacer = display_spacer/display_animation_time;
display_surface = -1
text_surface = -1;

wait = false;

bottom_of_list = false;

newalarm[1] = 2
newalarm[2] = 1;

textbox_hold_counter = 0;
textbox_hold_max = global.text_auto_speed;

menu_select = battleTextDisplaySelect;
menu_select_hold = menuParentSelectHold;

menu_key_hold = false;

draw_me = true;
signal = false;

counter = display_orig;

access_read_counter = 0;
access_read_complete = false;
last_text = "";

draw_next_icon = false;
draw_icon_counter = 0;
draw_icon_set = 0;

text_finished = false;

show_up_to = 3

no_text = false;

mouseHandler_clear()

// update this later to update the text...
mouseHandler_add(0,0,800,600,battleTextDisplaySelect,"")
mouseHandler_center_cursor(0)