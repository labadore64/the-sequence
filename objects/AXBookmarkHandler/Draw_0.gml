/// @description Insert description here
// You can write your code in this editor
//DEBUGGING ONLY
if(debug){

	if(triggered){
		
		if(!surface_exists(debug_surface)){
			debug_surface = surface_create_access(2000,2000)
			//draw_clear(c_black)
		}
		surface_set_target(debug_surface)
		mp_grid_draw(grid)
		with(AXBookmark){
			draw_set_color(c_black);
			draw_circle(x,y,5,false)	
		}
		
		if(path_exists(path_nav)){
			draw_set_color(c_red)
			draw_path(path_nav,0,0,true);
		}
		
		surface_save(debug_surface,"lol.png");
		surface_reset_target();
		surface_free(debug_surface)
		triggered= false;
	}
}