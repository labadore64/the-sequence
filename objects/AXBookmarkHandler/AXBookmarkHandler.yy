{
    "id": "7aed56f9-5eb8-449b-ba08-e532598aa88b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AXBookmarkHandler",
    "eventList": [
        {
            "id": "4b5f14e4-b5ea-4379-ba52-f0e1bf2a6d81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7aed56f9-5eb8-449b-ba08-e532598aa88b"
        },
        {
            "id": "cb39f053-5de4-4381-b082-c722a62b7eb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7aed56f9-5eb8-449b-ba08-e532598aa88b"
        },
        {
            "id": "a569d256-b60a-4cf5-b2fa-2ca8f0982ca6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "7aed56f9-5eb8-449b-ba08-e532598aa88b"
        },
        {
            "id": "a219e714-7708-43c2-9ab2-1702e1313d1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "7aed56f9-5eb8-449b-ba08-e532598aa88b"
        },
        {
            "id": "dd1bd1f5-468c-45fa-b92e-ee2c6938fc40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "7aed56f9-5eb8-449b-ba08-e532598aa88b"
        },
        {
            "id": "2f44e290-0ad9-4200-a088-60e4c4b54917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7aed56f9-5eb8-449b-ba08-e532598aa88b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}