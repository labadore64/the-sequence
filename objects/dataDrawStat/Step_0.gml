/// @description Insert description here
// You can write your code in this editor
hex_Step_0();

if(ScaleManager.updated){
	if(!surface_exists(surface_hex)){
		surface_hex = surface_create_access(radius*2.5*global.scale_factor,radius*2.5*global.scale_factor);	
	} else {
		surface_resize(surface_hex,radius*2.5*global.scale_factor,radius*2.5*global.scale_factor);	
	}
	
	force_draw = true;
}