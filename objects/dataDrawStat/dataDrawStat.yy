{
    "id": "687f3945-a547-4283-85ce-317e912c5de6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "dataDrawStat",
    "eventList": [
        {
            "id": "7d95e452-a7cc-4e3e-8489-593d7bde894b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "687f3945-a547-4283-85ce-317e912c5de6"
        },
        {
            "id": "828b9a46-2371-4022-bd76-bd9c06a7dde4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "687f3945-a547-4283-85ce-317e912c5de6"
        },
        {
            "id": "78173156-d72b-4fbe-a608-8f0935cac90c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "687f3945-a547-4283-85ce-317e912c5de6"
        },
        {
            "id": "ee98481a-94f0-4b6d-99e9-8e7fde680a87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "687f3945-a547-4283-85ce-317e912c5de6"
        },
        {
            "id": "09adca66-e53f-4bb7-8525-c261570d0fb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "687f3945-a547-4283-85ce-317e912c5de6"
        },
        {
            "id": "1e2846bf-6728-423b-8be0-bd4355dd60b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "687f3945-a547-4283-85ce-317e912c5de6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}