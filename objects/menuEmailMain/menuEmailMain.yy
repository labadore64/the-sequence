{
    "id": "e0d17890-3cb6-4004-8998-30fd70bbe6f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuEmailMain",
    "eventList": [
        {
            "id": "ba750e43-e82a-4d4e-8aa7-18db1b3f4e21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0d17890-3cb6-4004-8998-30fd70bbe6f7"
        },
        {
            "id": "c3dd13cc-44e3-4ee1-a803-661f083e4c3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e0d17890-3cb6-4004-8998-30fd70bbe6f7"
        },
        {
            "id": "3864bc76-af5e-493a-9f2d-6a0e7922b03f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0d17890-3cb6-4004-8998-30fd70bbe6f7"
        },
        {
            "id": "45554dca-0c63-4f20-8f36-45284daf7a26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "e0d17890-3cb6-4004-8998-30fd70bbe6f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}