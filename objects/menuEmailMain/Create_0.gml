/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// menu options

x= 400;
y = 325+30

menu_background_color = c_black
gradient_height = 0 //height of the title
menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430+60)
option_y_offset = 85
title_y_offset = 0
option_x_offset = 20;
select_percent = 1
animation_len = 1
option_space = 80

menuParentUpdateBoxDimension()

menu_draw = menuEmailDraw

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

draw_bg = false;

current_desc = "";

// create the list of Emails
email_list = ds_list_create();

for(var i = 0; i < objdata_email.data_size; i++){
	if(objdata_email.state[i] != EMAIL_STATE_NONE){
		ds_list_add(email_list,i);	
	}
}

if(ds_list_empty(email_list)){
	menuAddOption("Inbox Empty","",-1)
} else {
	var sizer = ds_list_size(email_list);
	var obj;
	for(var i = 0; i < sizer; i++){
		obj = email_list[|i];
		if(!is_undefined(obj)){
			menuAddOption(objdata_email.title[obj],"",menuEmailOpenEmail)
		}
	}
}

menu_size = menuGetSize();
skip = 5

menu_up = menuEmailMoveUp
menu_down = menuEmailMoveDown

menu_left = menuParentMoveLeft
menu_right = menuParentMoveRight
wrap = false;

selected_email = 0;

menuEmailUpdateDesc();

menu_help = "phoneemail"

ignore_tab = true