/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuAddOption("Switch","Switch this tablet with a different one.",menuPartyTabletScriptSwitch)
menuAddOption("Take","Take this tablet.",menuPartyTabletSubTake);
menuAddOption("Practice","Practice this tablet.",menuTabletSubPracticeSelect)
SHORTHAND_CANCEL_OPTION

menu_size = menuGetSize();

character = noone;

menu_draw = menuOverworldPartyStatsSubDraw;

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentSetTitle("");
menuParentSetSubtitle("");

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

menuParentSetGradientHeight(0)
menuParentSetOptionYOffset(10);
menuParentSetOptionSpacing(40)
draw_width = 160

x = 550
y = 200
draw_height = 40 * menu_size

menuParentUpdateBoxDimension()

visible = true;
with(menuPartyTablet){
	visible = true;	
}

ignore_tab = true;

//mousepos_x = -240