{
    "id": "0a7935c0-37b9-4082-983d-db63ebe23e3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuPartyTabletSub",
    "eventList": [
        {
            "id": "f403dbbc-cf58-4654-b8c7-b719bdd40ba9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a7935c0-37b9-4082-983d-db63ebe23e3c"
        },
        {
            "id": "aaa48891-886e-4f65-bf5e-5fcf42fbc1fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0a7935c0-37b9-4082-983d-db63ebe23e3c"
        },
        {
            "id": "fa2b5631-2eef-4f8d-b313-cba0bd0893c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "0a7935c0-37b9-4082-983d-db63ebe23e3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}