{
    "id": "2f08f9ca-d7ee-43e4-8bb6-fe3d1b903e37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_bird_cardinal",
    "eventList": [
        {
            "id": "61a4983d-bc4b-41e9-a5e2-90f2ad9ea39b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f08f9ca-d7ee-43e4-8bb6-fe3d1b903e37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dfecf79a-2560-4ecb-b29b-fdf2bad352af",
    "visible": true
}