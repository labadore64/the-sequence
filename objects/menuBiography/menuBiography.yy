{
    "id": "c90d53cf-36ac-4542-872c-c40f3f380a83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBiography",
    "eventList": [
        {
            "id": "f6eb1f16-2da9-4453-b2ac-7ba176212063",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c90d53cf-36ac-4542-872c-c40f3f380a83"
        },
        {
            "id": "89c97c28-8318-4606-a910-511b0bc778a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "c90d53cf-36ac-4542-872c-c40f3f380a83"
        },
        {
            "id": "c3ac0fee-04c3-42d7-af24-05419a891f99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "c90d53cf-36ac-4542-872c-c40f3f380a83"
        },
        {
            "id": "b8b57d08-f572-458c-871b-db9cbc80992d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c90d53cf-36ac-4542-872c-c40f3f380a83"
        },
        {
            "id": "de5dac18-7c45-4f6e-bb7e-73bf19ebffef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "c90d53cf-36ac-4542-872c-c40f3f380a83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}