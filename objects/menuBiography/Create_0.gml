/// @description Insert description here
// You can write your code in this editor
event_inherited();
windowText = "With Kisara captured, Aknadin heard of the Dragon's power and hoped to transfer the ka to his son Seto, unconcerned that this would mean killing its current host.";
windowCharPerLine = 27;


sprite_face = -1;

x = 400;
y = 300;

menuParentSetBoxHeight(250)
menuParentSetBoxWidth(700);

menu_select = menuParentCancel;

sound_cancel = soundMenuDefaultMove;

menuParentUpdateBoxDimension();

menu_draw = menuBiographyDraw

menu_up = -1
menu_down = -1
menu_left = -1;
menu_right = -1;

drawGradient = false

newalarm[2] = 1

menu_ax_HUD = "bio"

ignore_tab = true;