/// @description Handles dialog trees.
// You can write your code in this editor
initTimingAlarm()

character = obj_data.characters[|1];
dialogFolder = "";
dialogFile = "";
dialogRef = character.next_dialog;
audio_group_load(audiogroup_character_voice)

dialog_strings = ds_list_create();
dialog_emotionList = ds_list_create();
current_emotion = 0;
dialog_spriteList = ds_list_create();
current_sprite = 0;
dialog_pitchList = ds_list_create();
current_pitch = 1;
dialog_background = ds_list_create();

option_stringList = ds_list_create(); //list of string options.
option_gotoList = ds_list_create(); //list representing the next dialog to call. -1 if end of tree.
//option_eventTrueList = ds_list_create(); //list representing 1 event for each option that must be true to display. usually -1.
//option_eventFalseList = ds_list_create(); //list representing 1 event for each option that must be false to display. usually -1.
option_eventSetList = ds_list_create(); //list representing the event that is set if you select a certain option. usually -1.
//option_displayList = ds_list_create(); //list representing whether or not options are displayed based on true/false lists.
//option_nextTree = ds_list_create(); //list representing the next tree to go to after conversation is finished. Usually -1 unless end of tree.
//option_endScript = ds_list_create(); // list representing end scripts

textbox = -1;

filename = "";

newalarm[0] = 2;
newalarm[2] = 1;
newalarm[4] = 1

background = -1;

image_speed = 2;

fade_in_script = -1
fade_out_script = -1

portrait = instance_create_depth(0,0,0,drawPortrait)
portrait.character = character.character	

dontdrawportrait = false;

background = -1;
background_len = 0;
bg_index = 0;
bg_speed = 1;
sprite = -1;
sprite_in = 0;
sprite_len = 0;
sprite_stop = false;

sprite_x = 400;
sprite_y = 300;

surface = -1
surface_alpha = 1;
active = false;

update_next = true;
show_chara_name = true;

finish_script = -1;

menu_draw = dialogHandlerDraw;

with(menuParent){
	visible = false;	
}