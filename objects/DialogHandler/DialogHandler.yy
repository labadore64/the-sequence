{
    "id": "02e46696-1196-432d-b96a-6773c46a9b54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DialogHandler",
    "eventList": [
        {
            "id": "f12dff26-d297-4a2f-a367-174d383a7213",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "b85656f4-0311-4fe5-8b95-b2e07a6d5a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "c69b758f-4e7a-4b98-b0d6-aa0596eb91ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "bf5a952d-bfe2-4ec3-8c89-3c61cda06089",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "feaafb51-63e9-4563-b488-459dc51ae550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "d548a159-13c7-4308-89da-a657b33ec360",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "5127536c-f43a-46ae-9799-c49b4dceb73c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        },
        {
            "id": "ccf25769-f233-4798-b428-8c021d8297f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "02e46696-1196-432d-b96a-6773c46a9b54"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}