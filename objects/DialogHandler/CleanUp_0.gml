
ds_list_destroy(dialog_strings);

ds_list_destroy(dialog_emotionList)

ds_list_destroy(dialog_spriteList)

ds_list_destroy(dialog_pitchList)
ds_list_destroy(dialog_background);

ds_list_destroy(option_stringList) //list of string options.
ds_list_destroy(option_gotoList)//list representing the next dialog to call. -1 if end of tree.
//ds_list_destroy(option_eventTrueList)//list representing 1 event for each option that must be true to display. usually -1.
//ds_list_destroy(option_eventFalseList) //list representing 1 event for each option that must be false to display. usually -1.
ds_list_destroy(option_eventSetList)//list representing the event that is set if you select a certain option. usually -1.
//ds_list_destroy(option_displayList) //list representing whether or not options are displayed based on true/false lists.
//ds_list_destroy(option_nextTree)//list representing the next tree to go to after conversation is finished. Usually -1 unless end of tree.
//ds_list_destroy(option_endScript);

with(portrait){
	instance_destroy();	
}

with(menuCharacterBase){
	newalarm[5] = 20;
	newalarm[4] = 1;
	menuCharacterDoText();
}

if(surface_exists(surface)){
	surface_free(surface);	
}

if(finish_script != -1){
	script_execute(finish_script);	
}

with(dataCharacter){
	if(temporary ){
		instance_destroy();	
	}
}

audio_group_unload(audiogroup_character_voice)

with(menuParent){
	visible = true;	
}

with(menuOverworldPartyStatsSub){
	newalarm[2] = 20;
}
menuControlForceDrawMenuBackground();