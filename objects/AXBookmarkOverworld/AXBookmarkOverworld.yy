{
    "id": "38e9391e-d1e3-4240-b1c1-e3d9880b4800",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AXBookmarkOverworld",
    "eventList": [
        {
            "id": "e1895279-cdec-457f-9cf4-db29e0fbb77b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38e9391e-d1e3-4240-b1c1-e3d9880b4800"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "76975396-4ce6-4073-abff-bc73b6961754",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}