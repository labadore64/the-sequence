{
    "id": "917beefb-10ac-498a-b572-7b9747af09ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleStatusEffect",
    "eventList": [
        {
            "id": "545c95e1-f235-4a05-aa1d-c7cfd063a3e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "917beefb-10ac-498a-b572-7b9747af09ab"
        },
        {
            "id": "550aca7e-1ca7-4d26-9b4e-4486b4cf0af2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "917beefb-10ac-498a-b572-7b9747af09ab"
        },
        {
            "id": "69a2daf3-db4f-4cc6-83b6-8cb4c01a7cb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "917beefb-10ac-498a-b572-7b9747af09ab"
        },
        {
            "id": "b5f903b5-4ffc-4af5-8a9d-66ab4ed39535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "917beefb-10ac-498a-b572-7b9747af09ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}