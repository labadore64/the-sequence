/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\stats\\character";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data
	ini_open(testname)

	phy_power[itemcount] = ini_read_real("stats","phy_power",1);
	phy_guard[itemcount] = ini_read_real("stats","phy_guard",1);
	mag_power[itemcount] = ini_read_real("stats","mag_power",1);
	mag_guard[itemcount] = ini_read_real("stats","mag_guard",1);
	spe_agility[itemcount] = ini_read_real("stats","spe_power",1);
	spe_mpr[itemcount] = ini_read_real("stats","spe_guard",1);
	hp[itemcount] = ini_read_real("stats","hp",1);
	mp[itemcount] = ini_read_real("stats","mp",1);
	
	element[itemcount] = stringToType(ini_read_string_length("stats","type","",IMPORT_MAX_SIZE_TYPE));

	color[itemcount] 
				= make_color_rgb(
					ini_read_real("stats","red",0),
					ini_read_real("stats","green",0),
					ini_read_real("stats","blue",0)
				);

	phone[itemcount] = phoneString(ini_read_string_length("stats","phone","123456789",IMPORT_MAX_SIZE_PHONE));

	name[itemcount] = ini_read_string_length("stats","name","",IMPORT_MAX_SIZE_NAME);

	face_sprite[itemcount] = asset_get_index(ini_read_string_length("stats","face_sprite","spr_faceSpriteMain",IMPORT_MAX_SIZE_SPRITE));
	portrait[itemcount] = asset_get_index("spr_portrait_" + string_lower(name[itemcount]));

	total_exp[itemcount] = ini_read_real("stats","total_exp",2500);
	exp_grow[itemcount] = ini_read_real("stats","exp_grow",0);
	
	hp_grow[itemcount] = ini_read_real("stats","hp_grow",0);
	mp_grow[itemcount] = ini_read_real("stats","mp_grow",0);
	
	phy_power_grow[itemcount] = ini_read_real("stats","phy_power_grow",0);
	phy_guard_grow[itemcount] = ini_read_real("stats","phy_guard_grow",0);
	mag_power_grow[itemcount] = ini_read_real("stats","mag_power_grow",0);
	mag_guard_grow[itemcount] = ini_read_real("stats","mag_guard_grow",0);
	spe_power_grow[itemcount] = ini_read_real("stats","spe_power_grow",0);
	spe_guard_grow[itemcount] = ini_read_real("stats","spe_guard_grow",0);

	ability[itemcount] = ini_read_string_length("stats","ability","",IMPORT_MAX_SIZE_ABILITY);
	battle_desc[itemcount] = newline(ini_read_string_length("stats","battle_description","",IMPORT_MAX_SIZE_DESC));
	ax_desc[itemcount] = ini_read_string_length("stats","ax_description","",IMPORT_MAX_SIZE_DESC);

	battle_sprite_default_speed[itemcount] = ini_read_real("stats","sprite_default_speed",1);

	var tempname = string_lower(name[itemcount]);

	sprite_idle_front[itemcount] = asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_idle_front","idle_front",IMPORT_MAX_SIZE_SPRITE));
	sprite_idle_back[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_idle_back","idle_back",IMPORT_MAX_SIZE_SPRITE));
	sprite_hit_front[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_hit_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_hit_back[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_hit_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump0_front[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_jump0_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump0_back[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_jump0_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump1_front[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_jump1_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump1_back[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_jump1_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast0_front[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_cast0_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast0_back[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_cast0_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast1_front[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_cast1_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast1_back[itemcount] =  asset_get_index("spr_battle_party_" + tempname + "_" + ini_read_string_length("stats","sprite_cast1_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));

	battleFixSpriteVars(itemcount);

	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;