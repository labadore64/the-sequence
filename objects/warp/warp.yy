{
    "id": "a0935c63-5359-467e-81ff-8734cb9f1732",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "warp",
    "eventList": [
        {
            "id": "5d71f3fe-93f0-4386-bf98-7bb51398fbf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0935c63-5359-467e-81ff-8734cb9f1732"
        },
        {
            "id": "2a84382c-d199-4138-b86a-8ab88d2b6d5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a0935c63-5359-467e-81ff-8734cb9f1732"
        },
        {
            "id": "69c3624d-a860-447a-8d68-b445ffd8ffd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a0935c63-5359-467e-81ff-8734cb9f1732"
        },
        {
            "id": "ab4817b6-6aaa-4aab-b4f6-bc52e1208755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a0935c63-5359-467e-81ff-8734cb9f1732"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6179a617-253d-4158-9176-856156f2c744",
    "visible": false
}