{
    "id": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ScriptInteract",
    "eventList": [
        {
            "id": "cd9f5c8c-1c75-490a-8152-450e5c83f10e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3"
        },
        {
            "id": "120d3142-673b-468a-9ed9-072a91a8319e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3"
        },
        {
            "id": "622d0f3e-3f30-4188-89bc-079a2bbd6857",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3"
        },
        {
            "id": "31475704-2aef-45b0-bf41-244b12aafbea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c265d5f-787e-42eb-a640-da8f9fd6855f",
    "visible": false
}