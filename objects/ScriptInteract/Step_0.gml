/// @description Insert description here
// You can write your code in this editor
// Inherit the parent event
updateTimingAlarm();
if(objGenericCanDo()){

	if(menu_select != -1){
	if(global.key_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
		global.key_state_array[KEYBOARD_KEY_SELECT2] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_SELECT] == KEY_STATE_PRESS ||
		mouse_check_button_pressed(mb_left)){
			var playerInBounds = script_execute(menu_select);
			
			if(playerInBounds){
				if(menu_interact != -1){
					script_execute(menu_interact);	
				}
			}
		}
	
	}
}