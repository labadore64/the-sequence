/// @description Insert description here
// You can write your code in this editor
//this object represents the parent object for anything that you can interact with
//on the overworld with a specific area that triggers the script when you press
//the select key.

event_inherited();

menu_select = scriptInteractSelect;
menu_interact = -1; //script to be executed when script is interacted with

parent = noone; //parent object connected to it...
item_id = -1; //item id associated with the script...

scriptInteractGetPoints()
newalarm[0]=1

debug_test_trigger = false;

// text for AX

// name of object
ax_name = "Test";
// visual description of object
ax_desc = "Test test";