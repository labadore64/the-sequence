/// @description Insert description here
// You can write your code in this editor
EXPERIENCE[0] = 0;
EXPERIENCE[1] = 15;
EXPERIENCE[2] = 25;
EXPERIENCE[3] = 50;
EXPERIENCE[4] = 66;
EXPERIENCE[5] = 75;
EXPERIENCE[6] = 83;
EXPERIENCE[7] = 100;

var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\exp\\exp";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data
	
	ini_open(testname)
	
	name[itemcount] = ini_read_string_length("data","name","a",IMPORT_MAX_SIZE_EXPNAME);

	for(var i = 0; i < 10; i++){
		growth[itemcount,i] = ini_read_real("data","growth"+string(i),0);
	}

	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;
