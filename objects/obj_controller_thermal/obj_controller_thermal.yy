{
    "id": "83c6a438-960e-40a5-9b96-3c3f42a43680",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_thermal",
    "eventList": [
        {
            "id": "93e2aa2d-3ac1-45fa-a282-baa9a31c8ec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83c6a438-960e-40a5-9b96-3c3f42a43680"
        },
        {
            "id": "afbc8025-3edc-4c90-aef5-aea7e3f7a6d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "83c6a438-960e-40a5-9b96-3c3f42a43680"
        },
        {
            "id": "a116e8ef-aa01-43b9-81c4-1f32f1e935a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "83c6a438-960e-40a5-9b96-3c3f42a43680"
        },
        {
            "id": "4b5894b6-ae55-4567-9063-0d5dd14234a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "83c6a438-960e-40a5-9b96-3c3f42a43680"
        },
        {
            "id": "1246268a-cc93-4fba-9fc3-2a8e73b5b3ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "83c6a438-960e-40a5-9b96-3c3f42a43680"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}