{
    "id": "d9a71d50-1da3-42d7-b9e3-f5b4430e3805",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_place_debug_box",
    "eventList": [
        {
            "id": "b435599e-410d-4a46-a4ad-f46479b09261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9a71d50-1da3-42d7-b9e3-f5b4430e3805"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68cc3a71-5f10-4df3-b90a-75048f7ff35c",
    "visible": true
}