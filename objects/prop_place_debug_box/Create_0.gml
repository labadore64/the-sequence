/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

make_collision = true;
make_ax_label = true;
ax_label = "Debug Box"

item_id = objdataToIndex(objdata_items,"debug box");

make_interact_script = true; //does it allow you to interact with object?
make_interact_type = OverworldItemTake; //object to make
