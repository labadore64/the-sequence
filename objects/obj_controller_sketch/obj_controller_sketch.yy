{
    "id": "1444f2e4-d7ec-4954-be11-95bd373a8dd7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_sketch",
    "eventList": [
        {
            "id": "dd9aee3e-73bb-4820-94be-6f8c4b22764d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1444f2e4-d7ec-4954-be11-95bd373a8dd7"
        },
        {
            "id": "c54786d3-4f0d-4283-a727-9790c5f9bafd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1444f2e4-d7ec-4954-be11-95bd373a8dd7"
        },
        {
            "id": "f36f093c-c4c8-48f1-9d61-f0dd999660d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1444f2e4-d7ec-4954-be11-95bd373a8dd7"
        },
        {
            "id": "3fc07e3e-8162-489a-aab3-bca15171b49f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "1444f2e4-d7ec-4954-be11-95bd373a8dd7"
        },
        {
            "id": "14b497cc-5412-4015-afe0-8c1ff236748e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "1444f2e4-d7ec-4954-be11-95bd373a8dd7"
        },
        {
            "id": "9a980583-65e6-4325-aa1a-d1b1c26197df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "1444f2e4-d7ec-4954-be11-95bd373a8dd7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}