{
    "id": "f6892751-6cbf-48c8-9a29-127e4e0d8d40",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBookmark",
    "eventList": [
        {
            "id": "65d3c044-30f3-4d75-93da-1076bc75b68e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6892751-6cbf-48c8-9a29-127e4e0d8d40"
        },
        {
            "id": "1364da94-f30c-4f96-81b7-cc54c17734f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f6892751-6cbf-48c8-9a29-127e4e0d8d40"
        },
        {
            "id": "9840d93d-a40d-4bc9-8a18-8dbf99e83e9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f6892751-6cbf-48c8-9a29-127e4e0d8d40"
        },
        {
            "id": "9cfcb2b8-292a-4831-b8f8-8088b790af15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "f6892751-6cbf-48c8-9a29-127e4e0d8d40"
        },
        {
            "id": "3c46b63a-3e2c-4542-83f1-d4e6de0d0c51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6892751-6cbf-48c8-9a29-127e4e0d8d40"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}