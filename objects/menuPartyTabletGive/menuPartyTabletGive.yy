{
    "id": "a4ce8930-7695-4226-a0d6-b1d08c96cc71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuPartyTabletGive",
    "eventList": [
        {
            "id": "6eff3720-9467-455a-a6ed-960e9684ddf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a4ce8930-7695-4226-a0d6-b1d08c96cc71"
        },
        {
            "id": "0c168675-01a1-486e-942b-86225fcc57e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "a4ce8930-7695-4226-a0d6-b1d08c96cc71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00e8f880-ea72-49e5-8a60-2de5525111a9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}