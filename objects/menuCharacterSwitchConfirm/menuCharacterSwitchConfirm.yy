{
    "id": "55183774-f330-42eb-8e54-fb60fe03b157",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSwitchConfirm",
    "eventList": [
        {
            "id": "1390491b-9eeb-4636-add3-1d34b18cd245",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55183774-f330-42eb-8e54-fb60fe03b157"
        },
        {
            "id": "2448ee1c-382a-4f29-b410-abc557800e9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "55183774-f330-42eb-8e54-fb60fe03b157"
        },
        {
            "id": "130afa42-6271-48f3-bec7-65da16fd82df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "55183774-f330-42eb-8e54-fb60fe03b157"
        },
        {
            "id": "fa3a66d0-dfb3-4638-815b-e3184560ab21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "55183774-f330-42eb-8e54-fb60fe03b157"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}