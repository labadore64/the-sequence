/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

party_char = "";
recruit_char = "";

var nameee = "";

party_pos = 0;
switch_char = -1;

var paaasss = -1;
var swittttt = -1;

with(menuCharacterSwitchParty){
	if(object_index == menuCharacterSwitchParty){
		nameee = obj_data.party[|menupos].name
		paaasss = menupos;
	}
}

party_pos = paaasss

party_char = nameee;

with(menuCharacterSwitchRecruit){
	nameee = recruit_list[|menupos].name
	swittttt = recruit_list[|menupos]
}

switch_char = swittttt;

recruit_char = nameee;

menuAddOption(LANG_SELECT_YES,"",menuCharacterSwitchPartyConfirmSelect);
//menuAddOption("Sell","View and sell your inventory.",-1)
menuAddOption(LANG_SELECT_NO,"",menu_cancel)

menu_draw = menuCharacterSwitchPartyConfirmDraw;

menu_size = 2;

normal_scale = 3;
select_scale = 3;
select_counter = 0;
select_expand = .15;
select_time = .15;

menu_left = menu_up;
menu_right = menu_down;

//aura stuff
predict_list = ds_list_create();
ds_list_copy(predict_list,obj_data.party);

ds_list_replace(predict_list,party_pos,switch_char);

//fade stuff

animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = 5

draw_bg = false;

//active = false;

fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;
