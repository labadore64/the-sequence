{
    "id": "95cf0c1e-e779-4940-87aa-9d5d4fd51b93",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "BrailleQuitGame",
    "eventList": [
        {
            "id": "d8ba285e-b709-49e8-aa97-745f7170709a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95cf0c1e-e779-4940-87aa-9d5d4fd51b93"
        },
        {
            "id": "e1007e47-3956-4048-908b-5f02283c5229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95cf0c1e-e779-4940-87aa-9d5d4fd51b93"
        },
        {
            "id": "e6573d60-e2fa-4fbe-9440-6981c4f6f5bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "95cf0c1e-e779-4940-87aa-9d5d4fd51b93"
        },
        {
            "id": "7d17bef0-89e2-44df-95fb-8f64fdf9070b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "95cf0c1e-e779-4940-87aa-9d5d4fd51b93"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}