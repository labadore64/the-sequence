/// @description Insert description here
// You can write your code in this editor
//control
event_inherited();

skip = 1; //how many points in the list to skip if you're skipping through the list

//text

title = "Quit Minigame?"; //title of the menu

//other scripts

menu_draw = EndGameDraw;

//keybind scripts

menu_up = menuParentMoveLeft;
menu_down = menuParentMoveRight;

menuAddOption(LANG_SELECT_YES,"Exits the Braille Game.",brailleEndGameSelect)
menuAddOption(LANG_SELECT_NO,"Return to game.",menuParentCancel)

menu_size = ds_list_size(menu_name);
//drawing variables
x = 400;
y = 300;

selected_scale_counter = 0;
selected_scale_amount = 1;

draw_centered = true; //Is the origin in the center of the box, or on the top left? 

draw_width = 500; //width of box in pixels
draw_height = 170; //height of box in pixels

title_color = global.textColor; //text color for the title
subtitle_color = global.textColor; //text color for the subtitle
text_color = global.textColor; //text color for most of the menu

titleFontSize = 4; //title font size
subtitleFontSize = 2; //subtitle font size
text_size = 2; //size of option text

drawTitle = true; //whether or not to draw title
drawSubtitle = false; //whether or not to draw subtitle
drawGradient = false; //whether or not to draw gradient

title_x_offset = draw_width*.5; //xoffset for title
title_y_offset = 20; //yoffset for title
subtitle_x_offset = 20 //xoffset for subtitle
subtitle_y_offset = 80; //yoffset for subtitle
option_space = 100; //how many pixels between each option
option_x_offset = draw_width*.5 - option_space*.5 //xoffset for options
option_y_offset = 100; //yoffset for options

select_alpha = .25; //alpha of current selection
select_color = global.textColor; //selection color
select_percent = 1; //how much of the menu does the selection graphic occupy

info_height = 485; //height of the info box
info_bg_color = c_black; //bg color of info box
info_title_color = global.textColor; //color of the title of the info box
info_subtitle_color = global.textColor; //color of the subtitle of the info box
info_title_size = 3; //size of text for title
info_subtitle_size = 2; //size of subtitle text
info_title_y_offset = 10; //y offset of title text
info_subtitle_y_offset = info_title_y_offset+40; //y offset of subtitle text

menuParentUpdateBoxDimension();

ignore_tab = true;

enable_scrollwheel = false
scrollwheel_skip = 1; // how many entries to skip with the scroll wheel

mouseHandler_clear()
var xpos = 400;
var ypos = 322;
mouseHandler_add(xpos-80-25,ypos-20,xpos-80+50,ypos+20,brailleEndGameSelect,"Yes",true)
mouseHandler_add(xpos+80-50,ypos-20,xpos+80+25,ypos+20,menuParentCancel,"No",true)
menu_use_generic = true;