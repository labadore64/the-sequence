{
    "id": "374eb5cf-d360-4cde-921e-4d90c446f123",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DialogOption",
    "eventList": [
        {
            "id": "045d4d42-f753-496f-8b7d-33c13065f57c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "374eb5cf-d360-4cde-921e-4d90c446f123"
        },
        {
            "id": "95429140-2cf0-4296-8850-06a662fcdafc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "374eb5cf-d360-4cde-921e-4d90c446f123"
        },
        {
            "id": "034ff195-3153-4e3c-a1db-67d60d3876ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "374eb5cf-d360-4cde-921e-4d90c446f123"
        },
        {
            "id": "1a209796-0b3f-43f1-8987-71d4ec78f218",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "374eb5cf-d360-4cde-921e-4d90c446f123"
        },
        {
            "id": "b49de4c1-b3d9-4837-bd03-fc82cb740653",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "374eb5cf-d360-4cde-921e-4d90c446f123"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}