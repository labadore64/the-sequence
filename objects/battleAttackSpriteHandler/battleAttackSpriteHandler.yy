{
    "id": "088cefe9-39a8-4de6-90de-aa143f4439c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleAttackSpriteHandler",
    "eventList": [
        {
            "id": "ce33b829-fff0-43f5-abf5-2fa27b751754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "088cefe9-39a8-4de6-90de-aa143f4439c1"
        },
        {
            "id": "9bce9826-4967-4d1d-b8a5-9fa0c82a370e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "088cefe9-39a8-4de6-90de-aa143f4439c1"
        },
        {
            "id": "5a067933-6606-4832-a010-d17594e10c44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "088cefe9-39a8-4de6-90de-aa143f4439c1"
        },
        {
            "id": "78d6557d-f681-49e3-8280-7898d98d4da4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "088cefe9-39a8-4de6-90de-aa143f4439c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}