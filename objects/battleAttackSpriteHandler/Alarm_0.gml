/// @description Insert description here
// You can write your code in this editor
var sprite_len1 = 0;
var sprite_len2 = 0;
var sprite_len3 = 0;
var sprite_len4 = 0;

if(sprite_exists(bg_sprite)){
	sprite_len1 = sprite_get_number(bg_sprite)	
	bg_count = sprite_len1
}

if(sprite_exists(fg_sprite)){
	sprite_len2 = sprite_get_number(fg_sprite)	
	fg_count = sprite_len2
}

if(sprite_exists(self_sprite)){
	sprite_len3 = sprite_get_number(self_sprite)	
}

if(sprite_exists(targ_sprite)){
	sprite_len4 = sprite_get_number(targ_sprite)	
}

time = max(sprite_len1,sprite_len2,sprite_len3,sprite_len4,anim_len);
active = true;

battleAttackSpriteDoHide(hide_chars);