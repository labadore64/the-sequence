{
    "id": "010faee2-dc00-42f8-aa0f-d7988e76c875",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldAXOption",
    "eventList": [
        {
            "id": "f26f8303-0bad-4d02-8cc6-983ded4aa4f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "010faee2-dc00-42f8-aa0f-d7988e76c875"
        },
        {
            "id": "906c8709-1577-45e8-88fd-ee9ed7593bb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "010faee2-dc00-42f8-aa0f-d7988e76c875"
        },
        {
            "id": "5560320c-4cb8-4e3e-8457-1f5f74b9dd6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "010faee2-dc00-42f8-aa0f-d7988e76c875"
        },
        {
            "id": "5a563da2-2b6d-4d6a-bb2a-15158c1702b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "010faee2-dc00-42f8-aa0f-d7988e76c875"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}