/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(ds_exists(text_speed_strings,ds_type_list)){
	ds_list_destroy(text_speed_strings);	
}

if(ds_exists(text_color_strings,ds_type_list)){
	ds_list_destroy(text_color_strings);	
}

if(ds_exists(text_font_strings,ds_type_list)){
	ds_list_destroy(text_font_strings);	
}

if(ds_exists(text_sound_strings,ds_type_list)){
	ds_list_destroy(text_sound_strings);	
}

if(ds_exists(text_color_values,ds_type_list)){
	ds_list_destroy(text_color_values);	
}

if(ds_exists(text_font_values,ds_type_list)){
	ds_list_destroy(text_font_values);	
}

if(ds_exists(listsss,ds_type_list)){
	ds_list_destroy(listsss)	
}

with(menuControl){
	transparent = false;
}

with(menuControl){
	please_draw_this_frame= true
}