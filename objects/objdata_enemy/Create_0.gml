/// @description Insert description here
// You can write your code in this editor

var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\stats\\monster";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

dex_size = 0;

while(file_exists(testname)){
	//loads data

	ini_open(testname)

	shadow_y[itemcount] = ini_read_real("stats","shadow_y",0);

	ai_script_id[itemcount] = asset_get_index("ai_script_" +ini_read_string_length("stats","ai_script","default",IMPORT_MAX_SIZE_SCRIPT));
	ai_script_target_id[itemcount] = asset_get_index("ai_target_script_" + ini_read_string_length("stats","ai_target_script","default",IMPORT_MAX_SIZE_SCRIPT));

	if(ai_script_id[itemcount] == -1){
		ai_script_id[itemcount] = ai_script_default
	}
	if(ai_script_target_id[itemcount] == -1){
		ai_script_target_id[itemcount] = ai_target_script_default;
	}

	stat_phy_power[itemcount] = ini_read_real("stats","phy_power",1);
	stat_phy_guard[itemcount] = ini_read_real("stats","phy_guard",1);
	stat_mag_power[itemcount] = ini_read_real("stats","mag_power",1);
	stat_mag_guard[itemcount] = ini_read_real("stats","mag_guard",1);
	stat_spe_power[itemcount] = ini_read_real("stats","spe_power",1);
	stat_spe_guard[itemcount] = ini_read_real("stats","spe_guard",1);
	stat_hp[itemcount] = ini_read_real("stats","hp",1);
	stat_mp[itemcount] = ini_read_real("stats","mp",1);
	sprite_scale_factor[itemcount] = ini_read_real("stats","sprite_scale",1);

	type[itemcount] = string_lower(ini_read_string_length("stats","type","",IMPORT_MAX_SIZE_TYPE));

	var r = ini_read_real("stats","red",0);
	var g = ini_read_real("stats","green",0);
	var b = ini_read_real("stats","blue",0);
	color[itemcount] = make_color_rgb(r,g,b);
	name[itemcount] = ini_read_string_length("stats","name","",IMPORT_MAX_SIZE_NAME);
	ax_desc[itemcount] = ini_read_string_length("stats","ax_description","",IMPORT_MAX_SIZE_DESC);
	desc[itemcount] = ini_read_string_length("stats","description","",IMPORT_MAX_SIZE_DESC);
	for(var i = 0; i < 3; i++){
		flavor_text[itemcount,i] = newline(ini_read_string_length("stats","flavor"+string(i),"",IMPORT_MAX_FLAVOR_TEXT));
	}
	experience[itemcount] = ini_read_real("stats","exp",1);

	sprite_default_speed[itemcount] = ini_read_real("stats","sprite_default_speed",1);

	jumper[itemcount] = !stringToBool(ini_read_string_length("stats","cant_jump","false",IMPORT_MAX_BOOLEAN));
	can_move[itemcount] = !stringToBool(ini_read_string_length("stats","cant_move","false",IMPORT_MAX_BOOLEAN));
	can_flee[itemcount] = !stringToBool(ini_read_string_length("stats","cant_flee","false",IMPORT_MAX_BOOLEAN));
	
	ignore_dex[itemcount] = stringToBool(ini_read_string_length("stats","ignore_dex","false",IMPORT_MAX_BOOLEAN));
	ignore_cry[itemcount] = stringToBool(ini_read_string_length("stats","ignore_cry","false",IMPORT_MAX_BOOLEAN));

	if(!ignore_dex[itemcount]){
		dex_size++;	
	}

	//item drops
	drop_normal[itemcount] = ini_read_real("stats","drop_normal",-1)
	drop_bribe[itemcount] = ini_read_real("stats","drop_bribe",-1)
	drop_megabribe[itemcount] = ini_read_real("stats","drop_megabribe",-1)
	drop_special[itemcount] = ini_read_real("stats","drop_special",-1)

	bribe[itemcount] = ini_read_real("stats","bribe",-1);
	ability[itemcount] = ini_read_string_length("stats","ability","",IMPORT_MAX_SIZE_ABILITY);

	min_money[itemcount] = ini_read_real("stats","min_money",0);
	max_money[itemcount] = ini_read_real("stats","max_money",0);

	talk_x[itemcount] = ini_read_real("stats","zoom_x",0);
	talk_y[itemcount] = ini_read_real("stats","zoom_y",0);

	var tempname = string_lower(name[itemcount]);

	sprite_idle_front[itemcount] = asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_idle_front","idle_front",IMPORT_MAX_SIZE_SPRITE));
	sprite_idle_back[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_idle_back","idle_back",IMPORT_MAX_SIZE_SPRITE));
	sprite_hit_front[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_hit_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_hit_back[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_hit_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump0_front[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_jump0_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump0_back[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_jump0_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump1_front[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_jump1_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_jump1_back[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_jump1_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast0_front[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_cast0_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast0_back[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_cast0_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast1_front[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_cast1_front",sprite_idle_front[itemcount],IMPORT_MAX_SIZE_SPRITE));
	sprite_cast1_back[itemcount] =  asset_get_index("spr_battle_enemy_" + tempname + "_"+ ini_read_string_length("stats","sprite_cast1_back",sprite_idle_back[itemcount],IMPORT_MAX_SIZE_SPRITE));

	battleFixSpriteVars(itemcount);

	ini_close()
	
	// values saved through the game
	
	state[itemcount] = DEX_STATE_UNSEEN;
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;