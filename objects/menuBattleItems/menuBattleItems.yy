{
    "id": "2a721858-aff6-4d8a-b2f4-fe506df8061d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleItems",
    "eventList": [
        {
            "id": "9a71a015-6a41-4c41-822c-aeb85b10dc5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a721858-aff6-4d8a-b2f4-fe506df8061d"
        },
        {
            "id": "4aa264ad-1184-4e85-a762-a1105a1f51b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2a721858-aff6-4d8a-b2f4-fe506df8061d"
        },
        {
            "id": "27d08123-0d58-4a96-8508-be68dcb27d0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "2a721858-aff6-4d8a-b2f4-fe506df8061d"
        },
        {
            "id": "bba80bdd-7298-4be4-bf25-077596eaeee9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "2a721858-aff6-4d8a-b2f4-fe506df8061d"
        },
        {
            "id": "4ada3895-d49c-4eb3-8f19-6315ac2c746d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a721858-aff6-4d8a-b2f4-fe506df8061d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}