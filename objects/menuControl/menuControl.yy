{
    "id": "94fa2498-9665-440b-adb1-ed2325e432a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuControl",
    "eventList": [
        {
            "id": "68960f7a-cc39-4fc7-8b52-72e339c144ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "b62fa43f-b0b5-4274-8447-eaa0fccbb253",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "e7a7c85a-74e1-4f5d-ba26-69c720fdcd6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "82d7470e-f528-479b-8f0c-6f488de44026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "fe838877-f8cf-4300-b708-7528b5570bbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "6ec79b19-ec47-4a07-9608-87935e58c8a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "24531ec9-0686-4716-a09e-85e8d3684a27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "8b17f9bf-bae0-4757-b467-f09b0633f3ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "f4df245a-ce38-4fa6-9baa-5383b874f7aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        },
        {
            "id": "7c1101a7-952a-4237-a526-ae3b872f291a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "94fa2498-9665-440b-adb1-ed2325e432a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}