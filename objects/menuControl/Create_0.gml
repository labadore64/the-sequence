/// @description Insert description here
// You can write your code in this editor
// tick
debug_blind_text = "";

initTimingAlarm();
timer_estimated_fps = 0;
timer_frame_count = 0;
timer_dt = 0;
timer_diff = 0;
timer_diff_round = 1
popped = false;
ignore_tab = false;

save_screen = -1;
save_screen_max = 30;

active_menu = noone; //current active menu
delay = false;
menu_stack = ds_stack_create();
menu_stack_size = 0;
menuControlSurface = -1;
draw_active_menu_uwu = false;

bg_alpha = .5;
bg_color = merge_color(c_black,global.textColor,.1);

push_stage_list = ds_list_create();

wait = 0;

transparent = false;

depth = -10000;

please_draw_menu_owo = false;
please_draw_all_visible = false;
please_draw_all = false
please_draw_this_frame = false;
please_draw_black = false;

end_game_obj = noone;

pad = 0;

for(var i = 0; i < 12; i++){
	if(gamepad_is_connected(i)){
		pad = i;
		break;
	}
}

key_end = keybind_get("exit");
gamepad_end = gamepadbind_get("exit");

help_open = false;

please_clear = false;