/// @description Insert description here
// You can write your code in this editor
if(access_mode()){
	draw_clear(c_black)
	draw_set_color(c_white)
	draw_set_font(global.normalFont)
	draw_set_halign(fa_center)
	draw_set_valign(fa_middle)
	draw_text_transformed_ratio(400,300,debug_blind_text,2,2,0)
} else {
	if(!surface_exists(menuControlSurface)){
		menuControlSurface = surface_create_access(global.window_width,global.window_height);	
		please_draw_menu_owo = true;
	} else {
		if(ScaleManager.updated){
			surface_resize(menuControlSurface,global.window_width,global.window_height);
			please_draw_menu_owo = true;
		}
	}

	if(please_draw_all_visible || please_draw_this_frame){
	
		menuControlDrawAll();	
		please_draw_all_visible = false;
	}
	if(please_draw_menu_owo){
		menuControlDrawBG();	
		please_draw_menu_owo = false;
		draw_active_menu_uwu = false;
	}

	if(please_clear){
		gpu_set_colorwriteenable(true,true,true,true)
		surface_set_target(menuControlSurface);
	
		draw_clear_alpha(c_black,0);
		please_clear = false;
		surface_reset_target();
	}

	if(please_draw_black){
		draw_clear_alpha(c_black,1);
	} else {
		if(!dark_mode()){
			menuControlDrawMain();
		}
	}

	with(EndGame){
		EndGameDraw();	
	}
}