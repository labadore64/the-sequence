/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();

if(save_screen > -1){
	save_screen++;
	if(save_screen == save_screen_max){
		save_screen = -1;
	}
}

// you can make the end game menu whenever you want!
if(global.inputEnabled){
	if(global.key_state_array[KEYBOARD_KEY_EXIT] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_EXIT] == KEY_STATE_PRESS){
		if(!instance_exists(controllerSetKey) &&
			!instance_exists(keyboardSetKey)){
			if(end_game_obj == noone){
				end_game_obj = instance_create(0,0,EndGame);	
			}
		}
	}
}

// you can take a screenshot whenever you want!
if(global.inputEnabled){
	if(global.key_state_array[KEYBOARD_KEY_SCREENSHOT] == KEY_STATE_PRESS ||
		global.gamepad_state_array[KEYBOARD_KEY_SCREENSHOT] == KEY_STATE_PRESS){
		if(save_screen == -1){
			save_screen = 0;
		}
	}
}

if(wait >0){
	wait--;	
}

menu_stack_size = ds_stack_size(menu_stack)

// Executes mouse stuff here
mouseHandler_process();