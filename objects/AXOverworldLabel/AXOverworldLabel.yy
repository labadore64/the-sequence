{
    "id": "059cb24d-9904-4e51-bf42-963f99956265",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AXOverworldLabel",
    "eventList": [
        {
            "id": "150d5f9f-5f4a-489c-b8fb-62111e7dd973",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "059cb24d-9904-4e51-bf42-963f99956265"
        },
        {
            "id": "f215fc46-ad27-4e32-9a82-c8ed1f9396a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "059cb24d-9904-4e51-bf42-963f99956265"
        },
        {
            "id": "78f3f05e-3d04-436b-8cb1-d1267f0f2aba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "059cb24d-9904-4e51-bf42-963f99956265"
        },
        {
            "id": "579c03d9-aa20-4218-8db3-f2c76af67603",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "059cb24d-9904-4e51-bf42-963f99956265"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "33309ed6-eac1-48f7-9a1e-a8a8546b5bc3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52eeaa41-8d84-4ca4-b83a-b1fe04aa1be9",
    "visible": false
}