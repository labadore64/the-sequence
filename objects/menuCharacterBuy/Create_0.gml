/// @description Insert description here
// You can write your code in this editor
event_inherited();

menu_left = -1;
menu_right = -1;

menu_select = menuCharacterBuyQuantityScript;

menuParentSetTitle("Buy")
menuParentSetSubtitle("Buy items with cash.")

menu_draw = menuCharacterBuyDraw

draw_money = true;

var chara = noone;

var happy_boost = 0
var sell_multiplier = 1;

with(menuCharacterShop){
	chara = character;
	active = false;
}


if(chara != noone){
	if(chara.buy_multiplier_script != -1){
		sell_multiplier = script_execute(chara.buy_multiplier_script)
	}
}

multiplier = sell_multiplier+happy_boost;

menuCharacterBuyListStart(menuOverworldItemScriptSub);
