{
    "id": "20547330-6ef3-45c2-9a2c-cdf79b51cbf2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterBuy",
    "eventList": [
        {
            "id": "98da3689-f826-4f86-a6ce-9b5b2f74beca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20547330-6ef3-45c2-9a2c-cdf79b51cbf2"
        },
        {
            "id": "400eff26-842e-47e9-a24e-35493272ab23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "20547330-6ef3-45c2-9a2c-cdf79b51cbf2"
        },
        {
            "id": "ee981bbd-2a39-4803-b864-3c1024ca580c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "20547330-6ef3-45c2-9a2c-cdf79b51cbf2"
        },
        {
            "id": "336b2221-262b-4dd6-a499-d784f6e7ef57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20547330-6ef3-45c2-9a2c-cdf79b51cbf2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f6c2682-acbd-4eaa-a031-105f7f2ebb37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}