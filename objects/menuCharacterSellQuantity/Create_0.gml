/// @description Insert description here
// You can write your code in this editor
event_inherited();

menuControlForceDrawMenuBackground();

menuOverworldItemTossStrings();

price_item = 0;
price_total = 0;
price_multiplier = 1;

draw_height = 220
draw_width = 400

var mut = 1;

with(menuCharacterSell){
	mut = multiplier;	
}

price_multiplier = mut;

var itemz = 0;

with(menuOverworldItems){
	itemData(item_id[|menupos]);
	itemz = price
}

price_item = itemz;

menu_up = menuCharacterSellQuantityLeft//menuTossMoveRight;//menuParentMoveUp;
menu_down = menuCharacterSellQuantityRight//menuTossMoveLeft;//menuParentMoveDown;
menu_left = menuCharacterSellQuantityUp//menuTossMoveUp;//menuParentMoveLeft;
menu_right = menuCharacterSellQuantityDown//menuTossMoveDown;//menuParentMoveRight;

menuParentSetTitle("Sell how many?");

menuCharacterSellQuantityUpdate();

menu_select = menuCharacterSellConfirm;

menu_draw = menuCharacterSellQuantityDraw;


menuParentUpdateBoxDimension();

item_idd = -1;
