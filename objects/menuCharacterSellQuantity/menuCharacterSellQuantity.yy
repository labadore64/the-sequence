{
    "id": "1b036b0a-81c5-40ba-8d63-4ee67dd216c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSellQuantity",
    "eventList": [
        {
            "id": "0993a436-7035-433c-98e0-5ee3d7504517",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1b036b0a-81c5-40ba-8d63-4ee67dd216c0"
        },
        {
            "id": "d4740d4f-c3d5-4b5d-9097-cedf99fc7ae8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1b036b0a-81c5-40ba-8d63-4ee67dd216c0"
        },
        {
            "id": "2eaab528-eab8-4262-80b8-94a6bfb9bd65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1b036b0a-81c5-40ba-8d63-4ee67dd216c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}