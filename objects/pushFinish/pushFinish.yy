{
    "id": "91892b82-5dd3-4bed-911e-10b9ec19d29a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "pushFinish",
    "eventList": [
        {
            "id": "f2fd1919-8652-4eaf-9eeb-24cc974a1fc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91892b82-5dd3-4bed-911e-10b9ec19d29a"
        },
        {
            "id": "c0fdaf39-4ebe-469f-9c2b-5fc4e796e325",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "91892b82-5dd3-4bed-911e-10b9ec19d29a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d2670df-ab69-4d01-a6ac-88df40dfef49",
    "visible": false
}