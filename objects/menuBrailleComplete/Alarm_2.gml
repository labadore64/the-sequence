/// @description Insert description here
// You can write your code in this editor
if(ds_map_exists(obj_data.braille_highscores,highscore_id)){
	var val = ds_map_find_value(obj_data.braille_highscores,highscore_id);
	
	if(!is_undefined(val)){
		if(val < myscore){
			ds_map_replace(obj_data.braille_highscores,highscore_id,myscore);	
			highscore = myscore;
			new_highscore = true;
		} else {
			highscore = val;	
		}
	} else {
		ds_map_add(obj_data.braille_highscores,highscore_id,myscore);
		highscore = myscore;
		new_highscore = true;
	}
}  else {
	ds_map_add(obj_data.braille_highscores,highscore_id,myscore);
	highscore = myscore;
	new_highscore = true;
}

if(highscore == 0){
	new_highscore = false;	
}

if(new_highscore){
	tts_say(" new high score! ");	
}
tts_say(string(myscore) + " score");
tts_say(menu_name_array[0])

brailleGameCompleteInfokeyString();