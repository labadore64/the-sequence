/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();
// Inherit the parent event
event_inherited();

selected_scale_counter+=menuControl.timer_diff
selected_scale_amount = sin(.07*selected_scale_counter*.5*pi)*.15+1;	

if(new_highscore){
	highscore_color = make_color_hsv(color_value,255,220);
	color_value+=menuControl.timer_diff;
	if(color_value > 255){
		color_value = color_value % 256;	
	}
}