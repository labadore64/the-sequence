{
    "id": "a898a87b-8018-430a-ab9f-4b36fc054019",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBrailleComplete",
    "eventList": [
        {
            "id": "bd095cda-47af-4f44-85fc-3463f20ab1a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a898a87b-8018-430a-ab9f-4b36fc054019"
        },
        {
            "id": "e770cc6a-1ecb-452f-8a94-5cabab74593e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "a898a87b-8018-430a-ab9f-4b36fc054019"
        },
        {
            "id": "0bde2b00-e8ea-49b0-85b2-c5271072f46d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a898a87b-8018-430a-ab9f-4b36fc054019"
        },
        {
            "id": "9eb6d94b-bb4c-469e-b868-ba58f86e0d5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "a898a87b-8018-430a-ab9f-4b36fc054019"
        },
        {
            "id": "dc6885bd-35f5-4ab7-9c45-b71e82f4b230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a898a87b-8018-430a-ab9f-4b36fc054019"
        },
        {
            "id": "9682e46b-700c-48e7-88e0-51443e2558af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "a898a87b-8018-430a-ab9f-4b36fc054019"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}