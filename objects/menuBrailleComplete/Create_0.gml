/// @description Insert description here
// You can write your code in this editor
event_inherited();

menu_left = menu_up
menu_right = menu_down
menu_select = brailleGameCompleteSelect
menu_select_hold = -1
menu_cancel = -1

menu_draw = brailleGameCompleteDraw

highscore_id = "null"

new_highscore = false;

myscore = 0;
highscore = 0;

newalarm[2] = 1;

//display strings

title = "Braille Game"
subtitle = "Minigame"
drawGradient = false

game_type = -1;

menuAddOption("Try Again","",-1)
menuAddOption(global.langini[0,7],"",menu_cancel)

x = 400;
y = 300;

draw_width = 600; //width of box in pixels
draw_height = 400; //height of box in pixels
title_x_offset = 300; //xoffset for title
title_y_offset = 30; //yoffset for title
subtitle_x_offset = 300; //xoffset for title
subtitle_y_offset = 80; //yoffset for subtitle
option_space = 300;
option_y_offset = 350;
option_x_offset = 165;

selected_scale_counter = 0;
selected_scale_amount = 1;
draw_bg = false;

menu_size = menuGetSize();

menuParentUpdateBoxDimension();

color_value = 0;
highscore_color = global.textColor

with(brailleGameGeneric){
	visible = false;
}

menu_use_generic = true;