{
    "id": "e63340af-28f1-495f-9657-b46bb416151a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Collision",
    "eventList": [
        {
            "id": "1eda6c77-5a24-486e-a644-2de3ffef6ce8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e63340af-28f1-495f-9657-b46bb416151a"
        },
        {
            "id": "d406a103-4bb9-492e-9597-f7249a45619f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e63340af-28f1-495f-9657-b46bb416151a"
        },
        {
            "id": "2b177c76-3917-4616-87c5-247ff8dec4ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e63340af-28f1-495f-9657-b46bb416151a"
        },
        {
            "id": "5ce6d292-5cce-471f-80ae-c29ea9a439ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e63340af-28f1-495f-9657-b46bb416151a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f643c4f4-3b8e-4b62-8fe0-78dc4855e219",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f637de93-58ac-413f-b212-ed702a6d8c0a",
    "visible": false
}