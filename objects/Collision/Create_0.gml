/// @description Insert description here
// You can write your code in this editor
//put code in scripts later!!

event_inherited()

zoom_x = .25
zoom_y = .25

draw_scalex = image_xscale;
draw_scaley = image_yscale;

createCollision();

draw_script = drawPropParentMain;

parent = noone;
//draw_script = drawScriptDefault

can_push = false;

push_this_frame = false;
pushFinish_trigger = false;

//in process of pushing
player_pushing = false; //did player cause pushing?
is_pushing = false; //is it in pushing animation?
push_dist = 30; //distance that 0 weight will push.
push_diff_x = 0; //difference for each frame of pushing
push_diff_y = 0 
push_time = 30; //how long push animation is.
push_counter = 0;
push_dest_x = 0; //destination of push
push_dest_y = 0; //destination of push
push_caused_by_player = false;
push_direction = 0; //push direction in degrees.

active = true;