/// @description Insert description here
// You can write your code in this editor


image_blend = c_white	

//do pushing
if(can_push){
	

	if(push_this_frame){
		image_blend = c_lime	
	}
	
	if(is_pushing){
		push_counter++
		
		x+=push_diff_x
		y+=push_diff_y;
		
		var xx = x;
		var yy = y;

		with(parent){
			x = xx
			y = yy
		}
		
		//test if collision with pushFinish is detected.
		
		
		var newxx = 0;
		var newyy = 0;
		var triggerNew = false;
		
		var myid = id;
		if(!pushFinish_trigger){
			with(pushFinish){
				if(myid == obj_id.child_collision){
					if(xx > xx1 && xx < xx2){
						if(yy > yy1 && yy < yy2){
							//set new coordinates.
							triggerNew = true;
							newxx = x;
							newyy = y;
							eventNormalSet(event_id);
						}
					}
				}
			}
		
			if(triggerNew){
				push_counter = push_time*.5;
				push_dest_x = newxx;
				push_dest_y = newyy;
				push_diff_x = (newxx - x)/(push_time*.5);
				push_diff_y = (newyy - y)/(push_time*.5);
				pushFinish_trigger = true;
			}
		}
	
		
		if(push_counter > push_time){
			is_pushing = false;
			push_counter = 0;
			x = push_dest_x;
			y = push_dest_y
			
			var xx = x;
			var yy = y;

			bounding_box[0] = x - (sprite_width*.5)
			bounding_box[1] = x + (sprite_width*.5)
			bounding_box[2] = y - (sprite_height*.5)
			bounding_box[3] = y + (sprite_height*.5)

			with(parent){
				x = xx
				y = yy
				depth = y;

				bounding_box[0] = x - (sprite_width*.5)
				bounding_box[1] = x + (sprite_width*.5)
				bounding_box[2] = y - (sprite_height*.5)
				bounding_box[3] = y + (sprite_height*.5)

			}
			
			if(pushFinish_trigger){
				can_push = false;
			}
			
			if(player_pushing){
				with(Player){
					push_lock = false;	
				}
				push_caused_by_player = false;
				player_pushing = false;
			}
		}
		collisionUpdateVars();
	}
}

var scalingx = draw_scalex;
var scalingy = draw_scaley

image_xscale = scalingx;
image_yscale = scalingy;

image_xscale = image_xscale * (1/zoom_x);
image_yscale = image_yscale * (1/zoom_y);


event_inherited();
//end pushing

//debug