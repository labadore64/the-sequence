{
    "id": "5661bae4-c66d-4153-b178-3252518b3132",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CollisionDetector",
    "eventList": [
        {
            "id": "b8a75c40-bc2f-4958-b81d-c3a531bbbb2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5661bae4-c66d-4153-b178-3252518b3132"
        },
        {
            "id": "6f718eca-60b3-4305-b1be-4e671a01c01b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5661bae4-c66d-4153-b178-3252518b3132"
        },
        {
            "id": "7249bebd-da68-4614-ae30-c415f7d4c4d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5661bae4-c66d-4153-b178-3252518b3132"
        },
        {
            "id": "6f2318fd-6905-48b7-8ad1-c7b809318d51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5661bae4-c66d-4153-b178-3252518b3132"
        },
        {
            "id": "ee1d35e1-01e8-4563-9004-fb60fdf35caa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5661bae4-c66d-4153-b178-3252518b3132"
        },
        {
            "id": "400a5be5-2518-4f72-b8c8-b94f0463662e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "5661bae4-c66d-4153-b178-3252518b3132"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}