/// @description Insert description here
// You can write your code in this editor
active = true;


	if(directional == AX_COLLISION_DIRECTION_UP){
		sound_effect = sound3D_wallUp;
		orientation = 90
	} else if(directional == AX_COLLISION_DIRECTION_DOWN){
		sound_effect = sound3D_wallDown;
		orientation = 270 
	} else if(directional == AX_COLLISION_DIRECTION_LEFT){
		sound_effect = sound3D_wallLeft;
		orientation = 180 
	} else if(directional == AX_COLLISION_DIRECTION_RIGHT){
		sound_effect = sound3D_wallRight;
		orientation = 0 
	} 

color = make_color_hsv(((orientation%360)/360)*255,255,127)	

sound_emit = instance_create(x,y,AudioEmitter)
sound_emit.persistent = true;
sound_emit.delay = 2;
sound_emit.falloff_min = 1;
sound_emit.falloff_max = global.AXCollisionDistance
sound_emit.audio_sound = sound_effect;