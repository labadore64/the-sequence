{
    "id": "9eac30f8-6733-4785-a746-0105443ad1e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBrailleOption",
    "eventList": [
        {
            "id": "a0235ce1-736d-4fdc-b603-baca4705e344",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9eac30f8-6733-4785-a746-0105443ad1e7"
        },
        {
            "id": "f3bccdd1-1105-463a-97f2-fd96327ea760",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9eac30f8-6733-4785-a746-0105443ad1e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54d47214-be92-472f-bc26-a9201d2ea2d3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}