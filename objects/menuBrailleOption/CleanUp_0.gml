/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(ds_exists(text_color_strings,ds_type_list)){
	ds_list_destroy(text_color_strings);	
}

if(ds_exists(text_color_values,ds_type_list)){
	ds_list_destroy(text_color_values);	
}
