/// @description Insert description here
// You can write your code in this editor
mouseHandler_clear()
// Parent stuff
draw_my_bg = true;

menuParentNoBGCreate();

animation_start = 1 //animation length

animation_len = animation_start;

//newalarm[3] = 5

draw_bg = false;

//active = false;

//fadeIn(5,c_black)

destroy_time = 10

//destroy_script = menuFadeOut;

//clean_script = menuFadeIn;

menu_help = "braillegame2"

with(screenFade){
	script_run = false;	
}
// end parent stuff

menuParentSetTitle("Braille Options")
menuParentSetSubtitle("Configure Braille options.")

menu_draw = menuBraillePracticeOptionDraw;

menu_up = menuOverworldOptionRealUp;
menu_down = menuOverworldOptionRealDown;

menu_left = menuBraillePracticeOptionsLeft;
menu_right = menuBraillePracticeOptionsRight;

// clears menu real quick
ds_list_clear(menu_name); //name of all menu items
ds_list_clear(menu_description); //descriptions of all menu items
ds_list_clear(menu_script); //scripts for all menu items

menuAddOption("Timer Speed","Change Braille timer speed.",menuBraillePracticeOptionsLeft)
menuAddOption("Practice Charset","Which charset to use in practice mode.",menuBraillePracticeOptionsLeft)
menuAddOption("Sound FX","Enable/Disabled Braille input Sound FX.",menuBraillePracticeOptionsLeft)
menuAddOption("Repeat","Allows repeat characters in practice mode.",menuBraillePracticeOptionsLeft)
menuAddOption("Background","Displays background in practice mode.",menuBraillePracticeOptionsLeft)
menuAddOption("Hint","Shows a hint if you're running low on time.",menuBraillePracticeOptionsLeft);
text_color_strings = ds_list_create();
text_color_values = ds_list_create()
var objdata_len = array_length_1d(objdata_tablet.brailletype_file)
for(var i = 0; i < objdata_len; i++){
	ds_list_add(text_color_values,objdata_tablet.brailletype_file[i]);
	ds_list_add(text_color_strings,objdata_tablet.brailletype_title[i]);
}

menu_size = menuGetSize();

menuParentTTSTitleRead();

menuParentTTSLabelRead(); 

menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentUpdateBoxDimension();

curposs[0] =  global.BrailleTimerSpeed;
curposs[1] = ds_list_find_index(text_color_values,global.braille_practice);
if(curposs[1] == -1){
	curposs[1] = 0;	
}
curposs[2] = global.BrailleSound
curposs[3] = global.brailleRepeat;
curposs[4] = global.brailleBackground;
curposs[5] =global.brailleHint;

surfa = -1;

wrapz[0] = false;
wrapz[1] = true;
wrapz[2] = true;
wrapz[3] = true;
wrapz[4] = true;
wrapz[5] = true;