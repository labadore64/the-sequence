/// @description Insert description here
// You can write your code in this editor
if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	menuAddOption("Practice","Practice this tablet.",menuPracticeSubPracticeSelect)
	SHORTHAND_CANCEL_OPTION
	
	menupos = 0;
	menu_size = menuGetSize();
	
	if(menu_size == 3){
		option_y_offset = 290+30; //yoffset for options	
	}

}
braille_obj = instance_create(
					425,
					130,
					brailleCharacter);
braille_obj.cell_trans_time =0;
braille_obj.display_character = false;
braille_obj.cell_scale = 3;
brailleDataSetCharacterSpecific(braille_character,braille_obj,braillePracticeList.braille_data.braille_map);	

menuParentSetTitle(string_upper(braille_character));
menuParentSetSubtitle("");

menuParentUpdateBoxDimension();

tts_title_string = title;
// Inherit the parent event
event_inherited();

mouseHandler_clear();
mouseHandler_menu_default()

mousepos_x = -205
mouseHandler_menu_setCursor();