/// @description Insert description here
// You can write your code in this editor
event_inherited();
braille_character = "";
braille_id = -1;
braille_cell = noone;

braille_obj = noone;

move_element_sprite = -1;
move_type_sprite = -1;

multtt = 0;
multttz = 0;
maxdamage = 50;


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentSetDrawTitle(true);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

x = 400;
y = 250;
desc = ""
draw_width = 550; //width of box in pixels
draw_height = 350; //height of box in pixels
title_x_offset = 300; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 300; //xoffset for title
subtitle_y_offset = 60; //yoffset for subtitle

option_y_offset = 145; //yoffset for options
option_x_offset = 25; //yoffset for options

select_percent = .33; //how much of the menu does the selection graphic occupy

menuParentUpdateBoxDimension();

menu_draw = menuPracticeSubDraw

menu_left = menuOverworldPartySubLeft;
menu_right = menuOverworldPartySubRight;

menu_up = menu_left;
menu_down = menu_right;

//menuParentTTSLabelRead();

portrait = noone

move_id = -1;

menuControlForceDrawMenuBackground();
menu_help = "tabletsSub"

drawOtherMenus = false

ignore_tab = true;

infokey_script = menuPracticeInfokeyScript;
playing_sound = -1;

// braille sounds

cells_sound[0,0] = braille_cell1;
cells_sound[0,1] = braille_cell5;
cells_sound[0,2] = braille_cell3;
cells_sound[1,0] = braille_cell4;
cells_sound[1,1] = braille_cell2;
cells_sound[1,2] = braille_cell6;