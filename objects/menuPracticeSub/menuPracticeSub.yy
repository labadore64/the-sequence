{
    "id": "af4a273d-1dcc-4b58-a77b-1da6dc6f0d8d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuPracticeSub",
    "eventList": [
        {
            "id": "5e299a4c-be23-4a9b-add0-f6b12e7447ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af4a273d-1dcc-4b58-a77b-1da6dc6f0d8d"
        },
        {
            "id": "73df1e50-5c11-471a-a7c5-91bf387fc7f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "af4a273d-1dcc-4b58-a77b-1da6dc6f0d8d"
        },
        {
            "id": "fadf1911-54bc-45a0-b437-b5ce4948bdee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "af4a273d-1dcc-4b58-a77b-1da6dc6f0d8d"
        },
        {
            "id": "1c36bd0f-fe46-4cf6-9e2d-300294e20f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "af4a273d-1dcc-4b58-a77b-1da6dc6f0d8d"
        },
        {
            "id": "990781bd-2e6d-4e47-bdb1-4220b7c8a1c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "af4a273d-1dcc-4b58-a77b-1da6dc6f0d8d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}