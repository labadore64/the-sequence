/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuControlForceDrawMenuBackground();

	with(Player){
		item_use_lock = true	
	}

//first, check if any birds are close enough. If there 
//is a new bird, add it to the new bird list!

menuParentSetTitle("Creature Scope");
menuParentSetDrawSubtitle(false)
title_y_offset = 20

bird_list = ds_list_create();

var lizz = bird_list;
   
with(menuBestiaryMain){
	ds_list_copy(lizz,display_list);
}

//ds_list_sort(bird_list,true);

newalarm[2] = 2

menu_size = ds_list_size(bird_list);

menu_draw = menuBestiaryDraw;

menu_left = menuBestiaryLeft;
menu_right = menuBestiaryRight

menu_up = menuBestiaryUp
menu_down = menuBestiaryDown

menu_select = menuBestiarySelect

animation_frame[0] = -1;
animation_frame[1] = -1;

//bird draw stuff

char_per_line = 25

bird_name = "";
bird_desc = "";
bird_state = DEX_STATE_SEEN

element_sprite = -1;

bird_sprite = -1;

bird_adjust = 0;
bird_scale = 2;

animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = 5

draw_bg = false;

//active = false;

img = 0;
img_increment = 1;


menu_control = menu_cancel

waveform = noone

jump_y = 0; // the actual jump display value
jump_speed = 0; // current jump speed
jump_start_speed = 7;
jump_change = .45// rate of jump change

// fade shit

animation_start = 5 //animation length

animation_len = animation_start;

draw_bg = false;

active = false;

fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;

menu_ax_HUD = "bestiaryEntry"

tts_title_string = "Creature Details"
drawOtherMenus = false

ignore_tab = true