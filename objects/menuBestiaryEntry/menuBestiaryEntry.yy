{
    "id": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBestiaryEntry",
    "eventList": [
        {
            "id": "b4c6e3a7-23a4-44eb-83bf-c0909e448a48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "3f8140e7-2371-4d20-82e5-a7ac4c06982c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "4708a269-91db-4a0f-b0bf-ef36a42152e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "a3bd7710-9ab4-4f78-909e-8f9b35e80e79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "b33434a5-904e-4cb9-a6dd-7ec4e8de0582",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "ab97ca3a-ad8a-4b79-ae8a-5876c03fcb4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "e1a44968-8d9f-4495-a03e-9eaf1ae5ab98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "d1cf0b8f-cb3b-40df-96dc-3b1bfd973f6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        },
        {
            "id": "7be45184-48ba-4ebf-9a0c-4e7770b4d735",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "dc906c19-90ba-415a-bb06-b74f9eb6a6f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}