/// @description Insert description here
// You can write your code in this editor

//this object is a pseudo-singleton object that makes it easy to have objects
//that you must only have ONE of in a game. 
//use it for things like camera, config, player instance, music players ect.

//just have any object you want inherit this one and bam, that object acts as
//a pseudo-singleton. Keep in mind that since GameMaker doesn't have static
//variables, that you can still destroy the only instance of a pseudo-singleton.

//The application will log when the last instance of a pseudo-singleton has been
//destroyed for this reason.

//we don't want our pseudo-singleton to be destroyed by changing rooms
persistent = true;


if(instance_number(object_index) > 1){
	show_debug_message("instance destroyed because more than one instance detected")
	instance_destroy();
}