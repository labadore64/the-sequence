/// @description Insert description here
// You can write your code in this editor

if(dial_count >= array_length_1d(dial_array)){
	instance_destroy();	
} else {
	var rsound = row_sounds[0]
	var csound = column_sounds[0]
	var done = false; 
	
	for(var i = 0; i < 4; i ++){
		for(var j = 0; j < 4; j++){
			if(dial_numbers[i,j] == dial_array[dial_count]){
				done = true;
				rsound = row_sounds[i];
				csound = column_sounds[j];
				break;
			}
		}
		
		if(done){
			break;	
		}
	}
	
	row_sound = audio_play_sound(rsound,1,true)
	column_sound = audio_play_sound(csound,1,true)
	
	audio_sound_gain(row_sound,global.sfxVolume,0)
	audio_sound_gain(column_sound,global.sfxVolume,0)
}

newalarm[1] = dial_timer
dial_count++;