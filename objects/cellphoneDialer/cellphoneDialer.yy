{
    "id": "29513c01-bb37-41db-94f3-e241f541a61c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "cellphoneDialer",
    "eventList": [
        {
            "id": "014cffbf-6853-4c6b-84bd-d2bf36edfa05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29513c01-bb37-41db-94f3-e241f541a61c"
        },
        {
            "id": "e2f491e3-dfbe-4497-9ba6-a117a21d86cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29513c01-bb37-41db-94f3-e241f541a61c"
        },
        {
            "id": "81bf57a9-3087-469c-a7d3-a8860945a4b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "29513c01-bb37-41db-94f3-e241f541a61c"
        },
        {
            "id": "65e90d79-fbad-4605-8d8c-021cd050017a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "29513c01-bb37-41db-94f3-e241f541a61c"
        },
        {
            "id": "a796dd7a-b84b-47db-bad9-24e1cd81b3fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "29513c01-bb37-41db-94f3-e241f541a61c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}