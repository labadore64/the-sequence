{
    "id": "bde24027-3d3d-4413-9bb3-794afcf4fec1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_emboss",
    "eventList": [
        {
            "id": "ec265b61-e322-4e56-89da-ae98124ccec3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bde24027-3d3d-4413-9bb3-794afcf4fec1"
        },
        {
            "id": "64d9ff03-e499-4d47-a20f-d3675a5c00a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bde24027-3d3d-4413-9bb3-794afcf4fec1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "65111db1-dfab-45ea-9fa1-a5f43e12c77a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c9d1adbb-e31d-49c0-a1ff-b31c59e990c5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}