/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

character_select = noone

tablet[0] = -1;
tablet[1] = -1;
tablet[2] = -1;
the_char = "";
menu_left = menuPartyTabletMoveLeft
menu_right = menuPartyTabletMoveRight

menu_draw = menuPartyTabletDraw//menuTabletSubDraw

menu_update_values = menuPartyTabletUpdate

menu_up = menu_left;
menu_down = menu_right;

val_phy_power = 0;
val_phy_guard = 0
val_mag_power = 0;
val_mag_guard = 0
val_spe_power = 0;
val_spe_guard = 0

background_obj = instance_create(0,0,brailleGameBackground);

with(menuOverworldPartyStats){
	visible = false;	
}

draw_bg = false;

ignore_tab = true;
