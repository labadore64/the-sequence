{
    "id": "143a9760-a553-41ef-b72f-1924a4827fa7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuPartyTablet",
    "eventList": [
        {
            "id": "c183f39f-5634-44e4-a12a-c3898b440799",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "143a9760-a553-41ef-b72f-1924a4827fa7"
        },
        {
            "id": "4d75a418-681e-4e79-98cc-d2fd852ea937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "143a9760-a553-41ef-b72f-1924a4827fa7"
        },
        {
            "id": "a2b7b0fa-ba8f-496a-bbe7-f029e03a7098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "143a9760-a553-41ef-b72f-1924a4827fa7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a8f50ff-d548-4c96-9178-fe0b2fd9fff6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}