/// @description Insert description here
// You can write your code in this editor
if(character_select > -1){
	if(ds_exists(menu_name,ds_type_list)){

		ds_list_clear(menu_name); //name of all menu items
		ds_list_clear(menu_description); //descriptions of all menu items
		ds_list_clear(menu_script); //scripts for all menu items
		for(var i = 0; i < 3; i++){
			tablet[i] = character_select.tablet[i];
			if(tablet[i] != -1){
				menuAddOption(objdata_tablet.name[tablet[i]],
								"Switch or take this tablet.",
								menuPartyTabletSubScript);
			} else {
				menuAddOption("--","Equip a tablet.",menuPartyTabletScriptGive);	
			}
		}
	
		//menupos = 0;
		menu_size = menuGetSize();

		option_y_offset = 290+30; //yoffset for options	

	}
	
	
	braille_obj = instance_create(
						150,
						65,
						brailleCharacter);
	braille_obj.cell_trans_time =0;
	braille_obj.display_character = false;
	braille_obj.cell_scale = 3;
	
	menuPartyTabletUpdate();
	
	menuParentSetTitle(string_upper(braille_character));
	menuParentSetSubtitle("");

	menuParentUpdateBoxDimension();
	title = replaceStringWithVars("@1's Tablets",character_select.name)
	tts_title_string = title;
	
	menu_ax_HUD = "tabletDetailsNoCharacter"
	

	hudControllerClearData();
	hudControllerClearObjects();
	if(tablet[menupos] > -1){
		menu_ax_HUD = "tabletDetails"
		hudPopulateTablet("tablet",tablet[menupos]);
		hudPopulateOverworldStats("character",character_select.character);
	} else {
		menu_ax_HUD = "empty"
	}
	
	// Inherit the parent event
	active = true;

	with(menuControl){
		please_draw_black = false;	
	}

	if(menu_ax_HUD != ""){
		infokeys_assign(menu_ax_HUD);
	}

	tts_clear();
	if(tts_title_string != ""){
		tts_say(tts_title_string)	
	}
	visible = true;
	mouseHandler_clear()
	mouseHandler_menu_default()
	
	mouseHandler_menu_setCursor();
}