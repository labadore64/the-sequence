{
    "id": "29b62744-1310-46a9-87a8-b2a35cc18f06",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "EnterText",
    "eventList": [
        {
            "id": "dc6115ee-7554-4f0c-8831-da8adaca7fea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29b62744-1310-46a9-87a8-b2a35cc18f06"
        },
        {
            "id": "586f236d-2939-4338-af42-e475db75c347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29b62744-1310-46a9-87a8-b2a35cc18f06"
        },
        {
            "id": "e673dbf3-961d-417a-865a-eb9eaad5a2ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "29b62744-1310-46a9-87a8-b2a35cc18f06"
        },
        {
            "id": "d6cb59ff-c33d-48c8-ab6b-f7dd9db65dc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "29b62744-1310-46a9-87a8-b2a35cc18f06"
        },
        {
            "id": "96f746db-6382-417e-827e-f2e6a73554bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "29b62744-1310-46a9-87a8-b2a35cc18f06"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}