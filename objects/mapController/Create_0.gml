/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
// Inherit the parent event
event_inherited();

map_list = ds_list_create();

selected_pos = -1; //position of currently selected map data.
last_selected = -1;
selected_text_name = "";
last_selected_text_name = "";
selected_text_desc = "";

active = false;

current_location = noone; //location of current map
current_map = -1; //map of current.

map_you_are_here = noone;

move_speed = 2.5;

image_speed = 2;
var scaler = .6

check_dist = 8;

image_xscale = scaler;
image_yscale = scaler;

newalarm[0] = 30;


menu_hold_left=mapMoveLeft;
menu_hold_right=mapMoveRight;
menu_hold_up=mapMoveUp;
menu_hold_down=mapMoveDown;
menu_select = mapSelect;
menu_cancel = mapCancel;
menu_control = menu_cancel

fadeOut(30,c_black)

with(screenFade){
	script_run = false;	
}

fade_in_script = mapFadeIn;

depth = -10000

path = -1;

pathspeed = 60;

menu_help = "map"