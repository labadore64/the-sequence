{
    "id": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mapController",
    "eventList": [
        {
            "id": "ca74fe79-c5f1-4cd0-a662-9be43da42009",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121"
        },
        {
            "id": "91a8c5ea-d19e-43bc-b834-9182407edc41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121"
        },
        {
            "id": "4aafda9f-94e1-4467-85c6-4679874281f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121"
        },
        {
            "id": "1f5710e8-e62c-42f2-9d3a-d4d933127daf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121"
        },
        {
            "id": "58a0cc31-1d7f-4c44-bb12-eb11a09c42a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121"
        },
        {
            "id": "1e2ba58f-d558-45af-abaf-67a5d076be02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab526e9f-2ea5-47ec-a5de-a53f26fd6121"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d695fc8c-6e7b-43ce-9ad9-04cd831ea3e2",
    "visible": true
}