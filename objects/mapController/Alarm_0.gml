/// @description Insert description here
// You can write your code in this editor
var prio = ds_priority_create();

var curmap = current_map
var current_loc = -1;

with(mapPoint){
	ds_priority_add(prio,id,number);	
	if(room_id == curmap){
		current_loc = id;	
	}
}

current_location = current_loc;

var obj = -1;

while(!ds_priority_empty(prio)){
	obj = ds_priority_delete_min(prio)
	
	if(!is_undefined(obj)){
		ds_list_add(map_list,obj);
	}
}

ds_priority_destroy(prio);

active = true;
mapGetCheckPos();