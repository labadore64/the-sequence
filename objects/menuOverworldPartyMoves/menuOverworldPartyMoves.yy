{
    "id": "28429074-a47a-4172-a1ba-d340aa45251b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartyMoves",
    "eventList": [
        {
            "id": "3f807a63-c80a-4be9-98d5-8093edfa701a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "28429074-a47a-4172-a1ba-d340aa45251b"
        },
        {
            "id": "68949443-cd21-4b91-8fdf-5b8122034d19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "28429074-a47a-4172-a1ba-d340aa45251b"
        },
        {
            "id": "f62d3971-36c3-4183-a816-82cafbb7b12f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "28429074-a47a-4172-a1ba-d340aa45251b"
        },
        {
            "id": "f007c983-7287-4ea0-8e2c-d757e30761fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "28429074-a47a-4172-a1ba-d340aa45251b"
        },
        {
            "id": "5975f501-5733-4cbc-8ed5-b49e10c7e005",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "28429074-a47a-4172-a1ba-d340aa45251b"
        },
        {
            "id": "7fcf2e56-39a3-4083-a2dc-0f6db61971e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "28429074-a47a-4172-a1ba-d340aa45251b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}