/// @description Insert description here
// You can write your code in this editor
event_inherited();

character = -1;

element_sprite = -1;

menuOverworldPartyMovesStrings();
damage = 0;

animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = 5;

draw_bg = false;

//active = false;

fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;

menu_ax_HUD = "characterMoveMain"

menu_help="party_moves"

menu_update_values = menuOverworldPartyMovesGetMoveData

textboxMenuTutorial("tutorial_menuB")

//menu_tableft = menuOverworldMenuTabLeft;
//menu_tabright = menuOverworldMenuTabRight;

ignore_tab = true;

mousepos_x = -240