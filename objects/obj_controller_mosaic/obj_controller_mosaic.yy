{
    "id": "abc9a8a7-697f-447f-80df-5b2149389000",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_mosaic",
    "eventList": [
        {
            "id": "c5012102-873f-4fcd-80d4-c09cbeb5abda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "abc9a8a7-697f-447f-80df-5b2149389000"
        },
        {
            "id": "04b0136b-e7c9-45c6-b8e9-c807b7c3ee96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "abc9a8a7-697f-447f-80df-5b2149389000"
        },
        {
            "id": "603afd03-d0d5-4fee-9640-cfd1603513ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "abc9a8a7-697f-447f-80df-5b2149389000"
        },
        {
            "id": "6641e5ba-e226-472f-8093-082eaf34271d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "abc9a8a7-697f-447f-80df-5b2149389000"
        },
        {
            "id": "dd4ef4a2-dbb7-4a6b-985d-f4d328b5ebba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "abc9a8a7-697f-447f-80df-5b2149389000"
        },
        {
            "id": "4c9fa4d0-086b-43e5-a920-669e44058662",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "abc9a8a7-697f-447f-80df-5b2149389000"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}