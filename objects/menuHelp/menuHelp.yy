{
    "id": "65de32f4-ddf3-4a3e-8b7b-9ddb833dacc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuHelp",
    "eventList": [
        {
            "id": "b928e20e-4fde-40e4-817b-8b43dff81c40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "65de32f4-ddf3-4a3e-8b7b-9ddb833dacc8"
        },
        {
            "id": "1792847f-eb88-41e7-8b86-b1f8c50fc0f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "65de32f4-ddf3-4a3e-8b7b-9ddb833dacc8"
        },
        {
            "id": "efda49ca-76e5-4749-9240-a98b742d77c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "65de32f4-ddf3-4a3e-8b7b-9ddb833dacc8"
        },
        {
            "id": "d481ebee-6dfb-443b-b65c-a7d8f6964a2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "65de32f4-ddf3-4a3e-8b7b-9ddb833dacc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}