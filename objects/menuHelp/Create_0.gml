/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
menu_draw = menuHelpDraw

title = global.langini[2,7]
drawSubtitle = false; 
x = 400;
y = 250;


draw_width =500; //width of box in pixels
draw_height = 400; //height of box in pixels

title_x_offset = 85 //xoffset for title
title_y_offset = 20; //yoffset for title

drawInfoText = false;

help_file = "testhelp";

title_text[0] = "";
text_text[0] = "";

skip = 8;

menuParentUpdateBoxDimension();

newalarm[2] = 1;

tts_title_string = title;

with(menuControl){
	please_draw_all_visible = true;	
}