/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuAddOption(global.langini[0,11],global.langini[1,11],menuBrailleChallengeTimeAttack)
menuAddOption(global.langini[0,12],global.langini[1,12],menuBrailleChallengeElimination)
SHORTHAND_CANCEL_OPTION

menu_size = menuGetSize();

menu_help="braillegame1"

x = 400;
y = 250;

title = global.langini[2,6]
subtitle = global.langini[3,6]

draw_width = 600; //width of box in pixels
draw_height = 300; //height of box in pixels
title_x_offset = 300; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 300; //xoffset for title
subtitle_y_offset = 60; //yoffset for subtitle

option_y_offset = 120; //yoffset for options
option_x_offset = 20; //yoffset for options
option_space = 50; //how many pixels between each option

select_percent = .5; //how much of the menu does the selection graphic occupy

draw_gradient = false;

draw_bg = false; //whether to draw transparent bg

menu_draw = menuBraillePracticeMainDraw;

sprite_counter = 0;

spr_img[0] = spr_mode_time;
spr_img[1] = spr_mode_heart;
spr_img[2] = -1;

brailleDataObj = noone;

newalarm[2] = 1;
image_speed = 1.5
tts_title_string = title

menuParentUpdateBoxDimension();

drawOtherMenus = false

mousepos_x = -150