{
    "id": "273cfd32-c2f8-4a53-9bd8-c6893b84648e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBrailleChallengeSelect",
    "eventList": [
        {
            "id": "0e934c72-8ee8-4a79-8753-a50299d236f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "273cfd32-c2f8-4a53-9bd8-c6893b84648e"
        },
        {
            "id": "9f976e0c-80a0-49b3-ad87-dc94647a4ee2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "273cfd32-c2f8-4a53-9bd8-c6893b84648e"
        },
        {
            "id": "58e6c406-f973-47da-8f0c-1050f83d033e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "273cfd32-c2f8-4a53-9bd8-c6893b84648e"
        },
        {
            "id": "1cd73a01-1559-423c-a294-106bb7bdfdb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "273cfd32-c2f8-4a53-9bd8-c6893b84648e"
        },
        {
            "id": "f96513c4-e687-4c61-b9a2-439a92eca6ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "273cfd32-c2f8-4a53-9bd8-c6893b84648e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}