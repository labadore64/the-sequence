/// @description Insert description here
// You can write your code in this editor
event_inherited();
numbers = ds_list_create();

braille_data = noone;

braille_obj = noone;
obj_to_create = -1;
quantity = 0;
braille_char = ""

newalarm[3] = 1;
newalarm[2] = 1;

ds_list_add(numbers,"3","2","1","Go!");

menu_up = -1
menu_down = -1
menu_left = -1
menu_right = -1
menu_select = brailleCountdownSelect
menu_select_hold = -1
menu_cancel = -1

menu_draw = brailleGameGenericDraw

completed = true
complete_animation = 0;
complete_animation_length = 120;

// animation
complete_animation_text = "Complete!"

surface_fade_time = 6;

result_value = 0;

result_mid_x = 200;

result_start_counter = 0;
result_start_speed = result_mid_x/surface_fade_time;

result_move_after = 3;

result_surf = -1;

result_fade_time = 90;
result_fade_counter = 1;
result_fade_amount = 1/result_fade_time;

result_score = 0;

with(menuBrailleChallengeQuantity){
	visible = false;	
}
with(menuBrailleChallengeSelect){
	visible = false;	
}
with(menuBraillePracticeMain){
	visible = false
}


ignore_tab = true;
with(menuTabletsMain){
	force_draw_bg = true;	
}

audio_group_load(audiogroup_braille)
menuControlForceDrawMenus()

with(menuBraillePracticeMain){
	with(background_obj){
		visible = true;	
	}
}

drawOtherMenus = false;

mouseHandler_clear()
mouseHandler_add(0,0,800,600,brailleCountdownSelect,"")