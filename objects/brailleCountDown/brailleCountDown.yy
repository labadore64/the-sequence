{
    "id": "04e07725-236f-4b26-8982-d3c8cc5bc282",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleCountDown",
    "eventList": [
        {
            "id": "cf0bc79d-c6bb-42c2-abf6-cab7cf638765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04e07725-236f-4b26-8982-d3c8cc5bc282"
        },
        {
            "id": "7ce3ab87-10da-43b8-8724-9d2231d6f2c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "04e07725-236f-4b26-8982-d3c8cc5bc282"
        },
        {
            "id": "8246e56d-8e39-48a7-81d9-4baa1b415815",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04e07725-236f-4b26-8982-d3c8cc5bc282"
        },
        {
            "id": "b03d80af-d5d0-4613-a248-7d2cec6b0702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "04e07725-236f-4b26-8982-d3c8cc5bc282"
        },
        {
            "id": "cd2e9c1a-7d41-4730-bb12-3c9da9b8f3f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "04e07725-236f-4b26-8982-d3c8cc5bc282"
        },
        {
            "id": "2bbed4e8-42e7-4368-8f41-e70e667260d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "04e07725-236f-4b26-8982-d3c8cc5bc282"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}