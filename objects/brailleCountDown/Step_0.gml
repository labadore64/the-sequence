/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
event_inherited();

if(active){
	// first slide counter
	if(result_start_counter < result_mid_x){
		result_start_counter+=result_start_speed*menuControl.timer_diff;
	
		if(result_start_counter > result_mid_x){
			result_start_counter = result_mid_x;	
		}
	} else {
		result_start_counter+=result_move_after*menuControl.timer_diff;	
		
		if(result_fade_counter > 0){
			result_fade_counter-= result_fade_amount*menuControl.timer_diff;
		} else {
			brailleCountdownSelect();
		}
	}
}