{
    "id": "f27e97dc-39ec-4e6c-a70e-83718715e5b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldOptionGraphics",
    "eventList": [
        {
            "id": "c0b31a7f-1c96-4808-b83f-b525cfc6c273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f27e97dc-39ec-4e6c-a70e-83718715e5b6"
        },
        {
            "id": "66def577-aa38-4171-a0d1-9f0639921a53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f27e97dc-39ec-4e6c-a70e-83718715e5b6"
        },
        {
            "id": "8bf0f099-91ed-453a-abde-5ab780391c48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "f27e97dc-39ec-4e6c-a70e-83718715e5b6"
        },
        {
            "id": "fa0ed9d4-65a3-4f37-b08e-7e10574081d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f27e97dc-39ec-4e6c-a70e-83718715e5b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}