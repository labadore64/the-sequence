{
    "id": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleGameGeneric",
    "eventList": [
        {
            "id": "f461fb39-496a-44d2-8eb9-8c7172afb3bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c"
        },
        {
            "id": "85125268-1b08-4338-a7b1-2e2c75f1e735",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c"
        },
        {
            "id": "c685198d-faec-43ab-8111-c066a17cb1b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c"
        },
        {
            "id": "ab49afba-55e6-4728-b792-0b222bf86c55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}