/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
// Inherit the parent event
event_inherited();
if(!instance_exists(BrailleQuitGame)  && !menuControl.help_open){
	if(initialized){
		if(completed){
			// first slide counter
			if(result_start_counter < result_mid_x){
				result_start_counter+=result_start_speed*menuControl.timer_diff;
	
				if(result_start_counter > result_mid_x){
					result_start_counter = result_mid_x;	
				}
			} else {
				result_start_counter+=result_move_after*menuControl.timer_diff;	
		
				if(result_fade_counter > 0){
					result_fade_counter-= result_fade_amount*menuControl.timer_diff;
				} else {
					brailleCharsDestroy();
				}
			}
		} else {
			if(ds_queue_empty(input_queue.queue)){
				brailleCharsRefill();

				addToBrailleQueue(ds_queue_dequeue(braille_insert_queue),input_queue);
			}

			if(complete_script != -1){
				if(script_execute(complete_script)){
					completed = true;
					audio_stop_sound(sound_braille_miss)
					audio_stop_sound(sound_braille_ok)
					audio_stop_sound(sound_braille_good)
					audio_stop_sound(sound_braille_great)
					audio_stop_sound(sound_braille_perfect)
					
					soundfxPlay(sound_braille_finished)
					tts_say(complete_animation_text);
					with(input_queue){
						instance_destroy();	
					}
				}
			}	
		}
	}
}