/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

brailleCharsInit();

menu_up = -1
menu_down = -1
menu_left = -1
menu_right = -1
menu_select = brailleCharsSelect
menu_select_hold = -1
menu_cancel = -1

menu_draw = brailleGameGenericDraw

complete_script = -1;
destroy_script = -1;
destroy_executed = false;
completed = false;
complete_animation = 0;
complete_animation_length = 120;

complete_animation_text = "Complete!"

initialized = false;

// which minigame for high scores
braillegame_id = 0;

// current score
current_score = 0;
display_score = "0";
current_success = 0;
score_multiplier = 100;
// results

surface_fade_time = 6;

result_value = 0;

result_mid_x = 200;

result_start_counter = 0;
result_start_speed = result_mid_x/surface_fade_time;

result_move_after = 3;

result_surf = -1;

result_fade_time = 90;
result_fade_counter = 1;
result_fade_amount = 1/result_fade_time;

result_score = 0;

ignore_tab = true;

with(menuBrailleChallengeQuantity){
	instance_destroy();	
}