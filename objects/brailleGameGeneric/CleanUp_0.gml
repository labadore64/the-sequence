/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

brailleCharsCleanup();

if(surface_exists(result_surf)){
	surface_free(result_surf);	
}

with(menuBrailleChallengeSelect){
	visible = true		
}
with(menuBraillePracticeMain){
	visible = true	
}
with(menuControl){
	please_draw_all_visible = true;	
}