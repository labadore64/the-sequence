/// @description Insert description here
// You can write your code in this editor
#macro BRAILLE_LOWEST_SCORE .1

if(!lock){
	updateTimingAlarm();
	// Inherit the parent event
	if(!instance_exists(BrailleQuitGame) && !menuControl.help_open){
		if(global.key_state_array[KEYBOARD_KEY_OPEN_MENU] == KEY_STATE_PRESS ||
			global.gamepad_state_array[KEYBOARD_KEY_OPEN_MENU] == KEY_STATE_PRESS){
			if(objGenericCanDo()){
				if(script_menu_open!= -1){
					script_execute(script_menu_open);
				}
			}
		}else {
			event_inherited();
		}
	
		if(braille_sound != -1){
			audio_sound_pitch(braille_sound,(.5+(1-timer_percent)*.5));
		}

		if(completed){
			// fade surface counter
			if(surface_fade > 0){
				surface_fade-=surface_fade_amount*menuControl.timer_diff;
			}
	
			// first slide counter
			if(result_start_counter < result_mid_x){
				result_start_counter+=result_start_speed*menuControl.timer_diff;
	
				if(result_start_counter > result_mid_x){
					result_start_counter = result_mid_x;	
				}
			} else {
				result_start_counter+=result_move_after*menuControl.timer_diff;	
		
				if(result_fade_counter > 0){
					result_fade_counter-= result_fade_amount*menuControl.timer_diff;
				} else {
					instance_destroy();	
				}
			}
		} else {
			if(start_going){
				if(active){
					if(timer_active){
						timer_counter+=menuControl.timer_diff;
						timer_percent = 1- (timer_counter/timer_max);
						timer_color = merge_color(c_red,c_lime,timer_percent);
						if(timer_max < timer_counter){
							if(braille_sound != -1){
								audio_stop_sound(braille_sound)	
							}
							completed = true;
							// Test it. If its correct, treat as lowest score.
							if(brailleInputCellTest()){
								result_value = 3;
								soundfxPlay(sound_braille_ok);
								result_score = BRAILLE_LOWEST_SCORE;	
							} else {
								soundfxPlay(sound_braille_miss);
								result_value = 4;
							}
							tts_say(result_text[result_value]);
						}
						

					}
				}
			}
		}
	}
	
	
	// if nearing the end of the thing, show hint
	if(global.brailleHint){
		if(timer_percent < .5){
			// start adding the hint
			hint_counter++;
			hint_color = make_color_hsv(0,0,clamp(51+hint_counter*.5,40,180));
		}
	}
}