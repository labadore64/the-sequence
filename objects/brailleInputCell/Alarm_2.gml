/// @description Insert description here
// You can write your code in this editor
	// uses the default list if can't find
	if(!ds_exists(braille_data,ds_type_map)){
		var obj = instance_find(brailleData,0);
		braille_data = obj.braille_map;
	}


	var braillecell = ds_map_find_value(braille_data,string_lower(cell_character))

	if(!is_undefined(braillecell) && is_array(braillecell)){
		for(i = 0; i < 2; i++){
			for(j = 0; j < 3; j++){
				cells_correct[i,j] = braillecell[i,j]
			}
		}
	} else {
		for(i = 0; i < 2; i++){
			for(j = 0; j < 3; j++){
				cells_correct[i,j] = false
			}
		}
	}
	
	
	cell_character_display = string_replace_all(string_upper(cell_character)," ","\n");
	cell_character_length = string_length(cell_character_display);
	tts_say(cell_character_display);
	
	if(global.BrailleSound && timer_active){
		
		braille_sound = soundfxPlayLoop(braille_countdown_timer);
	}
	
	start_going = true
	
	soundfxPlay(cells_sound[0,0])