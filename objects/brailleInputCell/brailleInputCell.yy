{
    "id": "be6e70b7-af34-4303-9240-f3665a84bfc6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleInputCell",
    "eventList": [
        {
            "id": "b693658d-8084-48b0-bbd0-186ba932b2b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be6e70b7-af34-4303-9240-f3665a84bfc6"
        },
        {
            "id": "d8a9aea0-93fe-4e8b-9cd1-809e093409be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "be6e70b7-af34-4303-9240-f3665a84bfc6"
        },
        {
            "id": "0144b226-7759-4215-b84c-49c4e4a78586",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be6e70b7-af34-4303-9240-f3665a84bfc6"
        },
        {
            "id": "a356cf6b-59d5-450d-95c2-2e7db45c67e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "be6e70b7-af34-4303-9240-f3665a84bfc6"
        },
        {
            "id": "67283abb-2b39-4ab9-80bd-0d282e342c9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "be6e70b7-af34-4303-9240-f3665a84bfc6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}