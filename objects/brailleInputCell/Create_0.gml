/// @description Insert description here
// You can write your code in this editor

braille_data = instance_find(brailleData,0);
menu_pos = 0;
// Inherit the parent event
event_inherited();
draw_bg = false;
double_select = false;

menu_left = brailleInputCellLeft
menu_right = brailleInputCellRight
menu_down = brailleInputCellDown
menu_up = brailleInputCellUp
menu_select = brailleInputCellSelect
menu_cancel = -1;

menu_help = "brailleinput"

script_menu_open = brailleInputCellSubmit

menu_draw = brailleInputDraw

column_pos = 0;

hint_color = $333333
hint_counter = 0;

cell_character = "a";
cell_character_length = 1;
cell_character_display = "";

confirm_string = "Confirm"

// braille sounds
cells_sound[0,0] = braille_cell1;
cells_sound[1,1] = braille_cell2;
cells_sound[0,2] = braille_cell3;
cells_sound[1,0] = braille_cell4;
cells_sound[0,1] = braille_cell5;
cells_sound[1,2] = braille_cell6;

playing_sound = -1;

in_cell = true;

// input cells
for(var i = 0; i < 2; i++){
	for(var j = 0; j < 3; j++){
		cells[i,j] = false;	
	}
}

// correct cells
for(var i = 0; i < 2; i++){
	for(var j = 0; j < 3; j++){
		cells_correct[i,j] = false;	
	}
}

// surface stuff
surf = -1;

start_going = false;

surface_fade = 1;
surface_fade_time = 6;
surface_fade_amount = 1/surface_fade_time;

drawOtherMenus = false;

// timer stuff
timer_active = true;
if(global.BrailleTimerSpeed == 0){
	timer_active = false;
}
var timer_multiplier = 120;

timer_max = global.BrailleTimerSpeed*timer_multiplier;
timer_counter = 0;
timer_percent = 1;

timer_color = $00FF00
var timee = 15
newalarm[2] = 1;

newalarm[0] +=timee;
newalarm[2] +=timee;

// result display
completed = false;

result_text[0] = "Perfect!"
result_text[1] = "Great!"
result_text[2] = "Good!"
result_text[3] = "OK!" 
result_text[4] = "Miss!"

result_value = 0;

result_mid_x = 200;

result_start_counter = 0;
result_start_speed = result_mid_x/surface_fade_time;

result_move_after = 3;

result_surf = -1;

result_fade_time = 90;
result_fade_counter = 1;
result_fade_amount = 1/result_fade_time;

result_score = 0;

parent_queue = noone

braille_sound = -1;

// only triggers in battle
if(instance_exists(BattleHandler)){
	var obj = textboxMenuTutorial("tutorial_battleF");
	obj.braille_thing=true;
}

ignore_tab = true;


enable_scrollwheel = false
menu_use_generic = true
