/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(surface_exists(surf)){
	surface_free(surf);	
}

if(surface_exists(result_surf)){
	surface_free(result_surf);
}
var rvale = result_value;
var myscore = result_score;

with(parent_queue){
	result_score +=myscore;
	if(rvale != 4){
		result_success++;	
	}
	
	if(instance_exists(parent_obj)){
		parent_obj.current_score+= myscore;	
		if(rvale != 4){
			parent_obj.current_success++;
		}
		parent_obj.display_score = floor(parent_obj.current_score*parent_obj.score_multiplier);		
	}
}

with(brailleGameElimination){
	if(rvale == 4){
		rounds--;	
	}
}

if(braille_sound != -1){
	audio_stop_sound(braille_sound)	
}

mouseHandler_clear()