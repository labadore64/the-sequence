{
    "id": "4f2a6798-6450-454a-b511-8276d95ff116",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartyStats",
    "eventList": [
        {
            "id": "b2c43fd6-6218-4422-a937-a85fd12ebef4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f2a6798-6450-454a-b511-8276d95ff116"
        },
        {
            "id": "27a1e587-be6d-4f78-a091-e6db68db88fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4f2a6798-6450-454a-b511-8276d95ff116"
        },
        {
            "id": "ae1da8dc-f1f3-43f9-8672-501fe7f3f0f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "4f2a6798-6450-454a-b511-8276d95ff116"
        },
        {
            "id": "34e6bda4-6f2c-483a-bebe-01432897e62d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "4f2a6798-6450-454a-b511-8276d95ff116"
        },
        {
            "id": "e2a1171d-4add-46b8-8ec7-447cc46c4923",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f2a6798-6450-454a-b511-8276d95ff116"
        },
        {
            "id": "32322d7f-437a-414f-9fc6-18ef742b9c4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "4f2a6798-6450-454a-b511-8276d95ff116"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}