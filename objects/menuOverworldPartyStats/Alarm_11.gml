/// @description Insert description here
// You can write your code in this editor
tts_title_string = replaceStringWithVars(global.langini[4,5],character.name)
portrait.character = character.character

	with(portrait){
		x_scalefactor = .75;
		y_scalefactor = .75
		update_surf = true;
		image_xscale = global.scale_factor*x_scalefactor;
		image_yscale = global.scale_factor*y_scalefactor;
	
		x = 256*global.scale_factor*x_scalefactor;
		y = 256*global.scale_factor*y_scalefactor;
	}
menuOverworldPartyUpdateStats(character,100);

clearHUD();
hudPopulateOverworldStats("character",character)

translation = 0;

if(!noItems){
	menu_cancel = menuOverworldMenusCancel;
}
surface_update = true;

// Inherit the parent event
event_inherited();

if(menu_size == 2){
	var poss = (menupos+1) % 2
	var chh = obj_data.party[|poss];
	
	othername[0] = chh.character
	othername[1] = chh.character
} else {
	var poss = (menupos+1) % 3
	var chh = obj_data.party[|poss];
	
	othername[1] = chh.character
	
	var poss = (menupos+menu_size-1) % 3
	var chh = obj_data.party[|poss];
	
	othername[0] = chh.character
}

mouseHandler_clear()
mouseHandler_add(0,100,550,549,menu_select,character.name + " Details")
mouseHandler_add(250,0,400,99,menu_tableft,objdata_character.name[othername[0]])
mouseHandler_add(401,0,550,99,menu_tabright,objdata_character.name[othername[1]])

mouseHandler_add(551,100,800,275,menuOverworldPartyTablet,"Tablets")
mouseHandler_add(551,276,800,480,menuOverworldStatsSubItemsScript,"Items")

with(menuOverworldMenus){
	mouseHandler_add(0,550,200,600,menuOverworldMenuTabLeft,"Options")
	mouseHandler_add(600,550,800,600,menuOverworldMenuTabRight,"Items")
}
