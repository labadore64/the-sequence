/// @description Insert description here
// You can write your code in this editor
moveData(move);
var lister = -1;
var fulllister = -1;
is_ally = target_ally
if(is_ally){
	lister = BattleHandler.party_list;
	battleZoomParty(zoom_time);	
	battleHandlerClearFlash();
	fulllister = BattleHandler.allies;
} else {
	battleZoomEnemy(zoom_time);	
	lister = BattleHandler.enemy_list;
	fulllister = BattleHandler.opponents;

}

	if(ds_exists(lister,ds_type_list)){
		var sizer = ds_list_size(lister);
		for(var i = 0; i < sizer; i++){
			ds_list_add(party,lister[|i]);	
			menuAddOption(lister[|i].name,"",selectScript)
		}
	}
	tts_say(menu_name_array[0])
menu_size = ds_list_size(party);
character = party[|menupos];

if(character != -1){
	menuBattleTargetSetVals();
	with(character){
		draw_hp = true;	
	}
}

//if the current move is bribe, add the ability to change the bribe


if(move == objdataToIndex(objdata_moves,"bribe")){
	is_bribe = true;	
}


if(BattleHandler.see_enemy_stats){
	light_aura = true;
}

instance_create_depth(0,0,0,dataDrawStat);

var obj = character;
				
var raddd = 120
		
if(character != -1){	
with(dataDrawStat){
	drawHexStatSetBoosts(obj.phy_power.current,obj.phy_guard.current,obj.mag_power.current,obj.mag_guard.current,
							obj.spe_power.current,obj.spe_guard.current)
	
	
		character = obj;
		draw_hex_init(character,raddd,obj.phy_power.base,obj.phy_guard.base,
		obj.mag_power.base,obj.mag_guard.base,
		obj.spe_power.base,obj.spe_guard.base);
		radius = raddd;
	}
	type = character.element
	type_weak = getTypeWeakness(character.element);
	
	type_sprite = getTypeSprite(type);
	type_weak_sprite = getTypeSprite(type_weak);
	
	var moves = character.moves;
	
	var sizer = ds_list_size(moves);
	
	var moveid = 0;
	ds_list_clear(moves_names);
	for(var i =0; i < sizer; i++){
		moveid = moves[|i];
		if(!is_undefined(moveid)){
			moveData(moveid);
			ds_list_add(moves_names,name);
		}
	}
	moves_names_size = ds_list_size(moves_names);

}	

with(menuControl){
	please_draw_all_visible = true;	
}

menuBattleTargetSetVals();


hudPopulateMove("move",move,menuBattleMovesCalcMPCost(objdata_moves.mp_cost[move],character))
if(is_ally){
	menu_ax_HUD = "battleTargetAlly"
	hudPopulateBattle("character",character,true);
} else {
	menu_ax_HUD = "battleTargetEnemy";
	hudPopulateBattle("enemy",character,false);
	hudControllerAddData("enemy.desc",character.flavor_text[flavor_which]);
}

infokeys_assign(menu_ax_HUD);

with(MouseHandler){
	active = true;
}

mouseHandler_clear_main()

if(!target_ally){
	var size = ds_list_size(fulllister);
	var counta = 0;
	for(var i = 0; i < size; i++){
		if(battlerIsAlive(fulllister[|i])){
			if(i == 0){
				mouseHandler_add(100,120,310,450,selectScript,menu_name[|counta],true)
			} else if (i == 1){
				mouseHandler_add(320,120,530,450,selectScript,menu_name[|counta],true)
			} else {
				mouseHandler_add(540,120,780,450,selectScript,menu_name[|counta],true)
			}
			counta++;
		}
	}
	
} else {
	var size = ds_list_size(fulllister);
	var counta = 0;
	for(var i = 0; i < size; i++){
		if(battlerIsAlive(fulllister[|i])){
			if(i == 0){
				mouseHandler_add(75,120,320,475,selectScript,menu_name[|counta],true)
			} else if (i == 1){
				mouseHandler_add(321,120,530,475,selectScript,menu_name[|counta],true)
			} else {
				mouseHandler_add(540,120,780,475,selectScript,menu_name[|counta],true)
			}
			counta++;
		}
	}

}

mouseHandler_center_cursor(0);