/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
battleZoomDefault(zoom_time)
//battleHandlerSelectAllyClear()

battleHandlerClearFlash();

battleHandlerFlashSelected();

with(Battler){
	draw_hp = false;	
	draw_mp = false;
}

with(dataDrawStat){
	instance_destroy();	
}

if(ds_exists(moves_names,ds_type_list)){
	ds_list_destroy(moves_names);	
}

with(menuBattleMoves){
	do_not_draw_me = false;
}