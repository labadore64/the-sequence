/// @description Insert description here
// You can write your code in this editor
event_inherited();
light_aura = false; //if light aura is active?
target_ally = false
draw_me = false;
menuBattleTargetInit();
zoom_time = 6
active = false;

bribe = 0;
is_bribe = false; //is true if the current move is bribe.

move = 0;

newalarm[11] = 1
newalarm[2] = 1;
newalarm[3] = zoom_time;

drawOtherMenus = false



menu_up = -1;
menu_down = -1;
battleHandlerClearFlash();

type = -1;
type_weak = -1;
type_strong = -1;
moves_names = ds_list_create();
moves_names_size = ds_list_size(moves_names);

with(menuBattleMoves){
	do_not_draw_me = true
}


menu_ax_HUD = "battleTargetEnemy";

menu_help = "battleTarget"

is_ally = false;

menu_update_values = menuBattleTargetSetVals;

textboxBattleMain("tutorial_battleD");