{
    "id": "5aab8001-14e8-46a1-847f-091dd077f1df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleTarget",
    "eventList": [
        {
            "id": "68c40f36-d7d0-4d1d-8b1e-ad9d6a76d5ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5aab8001-14e8-46a1-847f-091dd077f1df"
        },
        {
            "id": "5a32d0fa-5901-4ec1-a296-4155a94153b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5aab8001-14e8-46a1-847f-091dd077f1df"
        },
        {
            "id": "7b6b2e6e-ee5c-49c5-a942-bf106f321f9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "5aab8001-14e8-46a1-847f-091dd077f1df"
        },
        {
            "id": "a1f8a7f9-2c97-46ea-8d06-6f079c9baa50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "5aab8001-14e8-46a1-847f-091dd077f1df"
        },
        {
            "id": "54066b53-f519-4148-bec6-7f2d2dfb7f2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "5aab8001-14e8-46a1-847f-091dd077f1df"
        },
        {
            "id": "004ffd0d-5415-4244-85d7-36965fc9d37d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5aab8001-14e8-46a1-847f-091dd077f1df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}