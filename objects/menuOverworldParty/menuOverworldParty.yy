{
    "id": "6f126f65-0956-4831-9210-5223e85c1baf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldParty",
    "eventList": [
        {
            "id": "2a0ae391-31c8-4663-bc66-5dd5585dcc5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f126f65-0956-4831-9210-5223e85c1baf"
        },
        {
            "id": "6f3e2a7a-60f5-4138-9de7-2a19964d900d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6f126f65-0956-4831-9210-5223e85c1baf"
        },
        {
            "id": "270eef49-a47d-43ce-9f0d-8d9cd95e1e33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f126f65-0956-4831-9210-5223e85c1baf"
        },
        {
            "id": "a1429b0d-9426-41cc-9e30-b8e229599794",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "6f126f65-0956-4831-9210-5223e85c1baf"
        },
        {
            "id": "a0366cbe-cc95-477b-8c75-675b0fd255b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "6f126f65-0956-4831-9210-5223e85c1baf"
        },
        {
            "id": "265b2e7d-d1f5-4d0a-a50c-458d2e700415",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "6f126f65-0956-4831-9210-5223e85c1baf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}