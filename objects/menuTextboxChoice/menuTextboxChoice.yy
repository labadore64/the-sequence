{
    "id": "f5989bfa-0831-438e-98dc-0e32b3f4716f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTextboxChoice",
    "eventList": [
        {
            "id": "8f4a22b8-ebc7-4227-965a-dc6988ef3c62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5989bfa-0831-438e-98dc-0e32b3f4716f"
        },
        {
            "id": "7035726e-d487-4392-a8fe-826e1909f0fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f5989bfa-0831-438e-98dc-0e32b3f4716f"
        },
        {
            "id": "7052d3b0-86fb-478e-9a04-971e5db1c1d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f5989bfa-0831-438e-98dc-0e32b3f4716f"
        },
        {
            "id": "d3c39abb-1597-4159-a3d3-4f90d62d8693",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "f5989bfa-0831-438e-98dc-0e32b3f4716f"
        },
        {
            "id": "a7c69b68-d305-48c0-ba23-8bb52ea9f9ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f5989bfa-0831-438e-98dc-0e32b3f4716f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}