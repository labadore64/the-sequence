/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\battle\\encounter\\enc";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] = ini_read_string_length("data","name","a",IMPORT_MAX_SIZE_ENCOUNTERNAME);
	
	event_enabled[itemcount] =  eventFind(ini_read_string_length("data","event_enabled","",IMPORT_MAX_SIZE_EVENT))
	event_disabled[itemcount] =  eventFind(ini_read_string_length("data","event_disabled","",IMPORT_MAX_SIZE_EVENT))
	
	for(var i = 0; i < 3; i++){
		enemy[itemcount,i] = objdataToIndex(objdata_enemy,ini_read_string_length("data","enemy"+string(i),"",IMPORT_MAX_SIZE_NAME))
	}

	random_order[itemcount] = stringToBool(ini_read_string_length("stats","random","true",IMPORT_MAX_BOOLEAN));
	
	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;
