{
    "id": "9d3377f3-0142-47dc-bc1c-7753e751277e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleStatSlider",
    "eventList": [
        {
            "id": "0429d8fc-4ccc-488c-b85f-978cc11d63a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d3377f3-0142-47dc-bc1c-7753e751277e"
        },
        {
            "id": "3a7de04c-cf67-4b58-a8f0-b148b2c49c50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9d3377f3-0142-47dc-bc1c-7753e751277e"
        },
        {
            "id": "d3cd58ab-3cd2-4e31-891d-49c306e3cda0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9d3377f3-0142-47dc-bc1c-7753e751277e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "52998076-0579-4441-8378-1e1dfe15940a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}