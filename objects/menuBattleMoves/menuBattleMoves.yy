{
    "id": "9973e227-7bce-4c33-b9bd-11606e15c971",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleMoves",
    "eventList": [
        {
            "id": "8a400714-672e-4482-87d0-1824c1bac295",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9973e227-7bce-4c33-b9bd-11606e15c971"
        },
        {
            "id": "764fb31a-7d91-4d18-ad41-6e41b044953e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9973e227-7bce-4c33-b9bd-11606e15c971"
        },
        {
            "id": "b033b0e7-0243-44e7-9f89-22d29cc820a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "9973e227-7bce-4c33-b9bd-11606e15c971"
        },
        {
            "id": "2e959cb1-44d7-4b6f-9e05-7848d0c3f546",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "9973e227-7bce-4c33-b9bd-11606e15c971"
        },
        {
            "id": "9b9d36c8-77f1-4f36-8c21-30f7380accc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "9973e227-7bce-4c33-b9bd-11606e15c971"
        },
        {
            "id": "36e2cd05-ecb2-49f0-9ca0-2b14c3cb7ea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9973e227-7bce-4c33-b9bd-11606e15c971"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}