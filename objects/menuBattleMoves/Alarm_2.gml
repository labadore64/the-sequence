/// @description Insert description here
// You can write your code in this editor
var move_size = ds_list_size(moves);

/*
moves_targets
moves_type
moves_damage
moves_mp_cost
*/

var obj = -1;
var maxdam = 50;

var cando = true;

for(var i = 0; i < move_size; i++){
	obj = moves[|i];
	moveData(obj);
	ds_list_add(moves_targets,target_number);
	ds_list_add(moves_type,menuBattleMovesGetElementSprite(element));
	ds_list_add(moves_damage,clamp(damage/maxdam,0,1));
	ds_list_add(moves_mp_cost,menuBattleMovesCalcMPCost(mp_cost,character));
	menuAddOption(name,flavor,selectScript)
	ds_list_add(move_cando,menuBattleMovesCando(mp_cost,character,obj))
	ds_list_add(move_skip,target_skip)
	ds_list_add(move_list_type,move_type);
	//}
	//}

}

hudPopulateBattle("character",BattleHandler.char_input_character,true);
hudPopulateMoveBattle("move",moves[|menupos],floor(moves_mp_cost[|menupos]*100));

menu_size = menuGetSize();

textboxBattleMain("tutorial_battleC");

with(menuControl){
	please_draw_all_visible = true;	
}

mouseHandler_clear();
mouseHandler_menu_default()

with(MouseHandler){
	active = true;
}