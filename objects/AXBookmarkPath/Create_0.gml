/// @description Insert description here
// You can write your code in this editor
sound_emitter = instance_create_depth(x,y,0,AudioEmitter);
sound_emitter.check_for_collision = true;
sound_emitter.audio_sound = sound3D_AXpath;
sound_emitter.falloff_min = 5;
sound_emitter.falloff_max = 64;
sound_emitter.is_bookmark = false;