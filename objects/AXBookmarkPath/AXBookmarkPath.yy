{
    "id": "eecef196-c2d6-4603-858e-8be7ab1d14c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AXBookmarkPath",
    "eventList": [
        {
            "id": "8eebef9d-6eff-4240-800d-ca9e65106160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eecef196-c2d6-4603-858e-8be7ab1d14c1"
        },
        {
            "id": "a8cbff50-ae5f-4839-a070-5a8770b40ab5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "eecef196-c2d6-4603-858e-8be7ab1d14c1"
        },
        {
            "id": "55d46256-0f80-4807-893a-8174fd2bba7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eecef196-c2d6-4603-858e-8be7ab1d14c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}