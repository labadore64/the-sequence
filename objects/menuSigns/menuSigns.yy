{
    "id": "8668aa5b-53a1-4065-b4e4-5dec65e3b12e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuSigns",
    "eventList": [
        {
            "id": "b0e0f8f8-d357-449e-8518-5c150c42b8bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8668aa5b-53a1-4065-b4e4-5dec65e3b12e"
        },
        {
            "id": "3916d7b0-6b7a-47bd-b8b7-c3b4211196b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8668aa5b-53a1-4065-b4e4-5dec65e3b12e"
        },
        {
            "id": "4009cfe5-9dfc-41e3-9bab-3af16d4caf24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "8668aa5b-53a1-4065-b4e4-5dec65e3b12e"
        },
        {
            "id": "28546ef6-3336-472a-9467-f9d726a43e2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8668aa5b-53a1-4065-b4e4-5dec65e3b12e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}