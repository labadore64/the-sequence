/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
display_obj = instance_create(295,200,brailleDisplaySignsMenu);
display_obj.newalarm[0] = TIMER_INACTIVE;
with(display_obj){
	event_perform(ev_alarm,0)
}

drawGradient = false;
select_percent = .33;
menu_draw = menuSignsDraw;

option_x_offset = 200

title = global.langini[2,4]
subtitle = global.langini[3,4]

sign_seen = 0;
sign_total = objdata_words.data_size;

sign_sprite = -1;

for(var i = 0; i < sign_total; i++){
	if(obj_data.sign_seen[i]){
		menuAddOption(objdata_words.name[i],objdata_words.desc[i],-1)
		sign_seen++;
	} else {
		menuAddOption(string_char_at(objdata_words.name[i],1)+"???","????",-1)
	}
}

menu_size = menuGetSize();

info_title_size = 4;
info_title_y_offset = 0;

menu_up = menuSignsUp;
menu_down = menuSignsDown;
menu_left = menuSignsLeft;
menu_right = menuSignsRight;

menu_help = "signs"
drawOtherMenus = true;

mousepos_x = -470