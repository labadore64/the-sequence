{
    "id": "ffe850d1-a199-4f13-bb3d-981dc9ee25ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_tree2",
    "eventList": [
        {
            "id": "83843c9e-fc7c-48db-963d-40ff67ca4eb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ffe850d1-a199-4f13-bb3d-981dc9ee25ef"
        },
        {
            "id": "32dfc8b1-1814-43cc-bed7-82c3bffe5efd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ffe850d1-a199-4f13-bb3d-981dc9ee25ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d44acc7-a31e-42e0-986a-545db73c5616",
    "visible": true
}