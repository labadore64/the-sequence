{
    "id": "d2d8d838-2412-48ae-944b-55f1f3611197",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_refraction",
    "eventList": [
        {
            "id": "ec74be2f-414d-4b6e-ba55-ebf5722b3b9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2d8d838-2412-48ae-944b-55f1f3611197"
        },
        {
            "id": "498902cf-9f9e-4e70-bf34-bcf36a3da185",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d2d8d838-2412-48ae-944b-55f1f3611197"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "1578b387-d25b-448b-9cf8-da4ee023f241",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0d85e46b-ca89-49ba-b7a4-ed3890a2171c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}