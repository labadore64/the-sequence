{
    "id": "544d5812-83e7-4fa6-867e-747e4811d614",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_gaussian_blur",
    "eventList": [
        {
            "id": "f2e15f10-7e6f-4705-ba14-5a4f132988de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "544d5812-83e7-4fa6-867e-747e4811d614"
        },
        {
            "id": "bb9c9bee-8b8e-4fef-950a-89b5b41a4b61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "544d5812-83e7-4fa6-867e-747e4811d614"
        },
        {
            "id": "0f591538-bf1f-4b22-a459-d7f2b40cbb07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "544d5812-83e7-4fa6-867e-747e4811d614"
        },
        {
            "id": "be9ec77f-5030-453a-a564-45df4be6f058",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "544d5812-83e7-4fa6-867e-747e4811d614"
        },
        {
            "id": "1fb98e10-ab73-4824-956d-371b5db24cca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "544d5812-83e7-4fa6-867e-747e4811d614"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}