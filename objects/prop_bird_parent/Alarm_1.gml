/// @description Insert description here
// You can write your code in this editor
if(audio_sound != -1){
	sound_emitter = instance_create_depth(x,y,0,AudioEmitter);
	//sound_emitter.check_for_collision = true;
	sound_emitter.audio_sound = audio_sound;
	sound_emitter.falloff_max = 300
	sound_emitter.falloff_min = 25
}

if(face_left){
	draw_scalex = -draw_scalex;	
}

var randomizer = random(1);

if(randomizer < destroy_chance){
	instance_destroy();	
}

sprite_index = sprite_idle