/// @description Insert description here
// You can write your code in this editor
//checks if another bird spawned really close to this one
//kills it if it has

var dist_of_birds = 100000000;

var tester = 0;

var me = id;

with(prop_bird_parent){
	tester = distance_to_object(me);
	
	if(tester < dist_of_birds){
		dist_of_birds = tester;	
	}
}

if(dist_of_birds < 10){
	instance_destroy();	
}