{
    "id": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_bird_parent",
    "eventList": [
        {
            "id": "dd207ad4-fdfa-459c-a08d-030f2e3353ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe"
        },
        {
            "id": "00cec97e-32b6-4de7-96ed-8ff60d38a43d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe"
        },
        {
            "id": "4c46f971-f7e5-46a9-880b-906d77db7cfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe"
        },
        {
            "id": "8b8b632a-bcf7-4697-8239-ac1787786a95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe"
        },
        {
            "id": "7b27b30a-d755-4c6f-8790-a64575acfd2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe"
        },
        {
            "id": "aba8ab78-2f29-455c-b95d-6da414b418f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93bb999a-5364-4d45-a46d-37fed305fd03",
    "visible": true
}