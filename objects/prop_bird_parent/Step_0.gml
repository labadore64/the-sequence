/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
// alarms
if(!prop_menu){
	for(var i = 0; i < 4; i++){
		if(newalarm[i] > TIMER_INACTIVE){
			newalarm[i] -= menuControl.timer_diff;	
			if(newalarm[i] < 0){
				event_perform(ev_alarm,i);
				if(newalarm[i] < 0){
					newalarm[i] = TIMER_INACTIVE;
				}
			}
		}
	}

	spr_counter += menuControl.timer_diff*image_speed*.5;
	alarm_done = true;

	if(visible){
		// update prop drawing details

		if(object_index == overworld_scalable){
			var scalingx = draw_scalex;
			var scalingy = draw_scaley

			image_xscale = scalingx;
			image_yscale = scalingy;

			image_xscale = image_xscale * (1/zoom_x)*global.scale_factor;
			image_yscale = image_yscale * (1/zoom_y)*global.scale_factor;
	
		}
	}
		stepPropParent();
	if(visible){
		var getValx = 1;
		var getValy = 1;

		var player_x = 0;
		var player_y = 0;

		with(CameraOverworld){
			getValx = zoomx;
			getValy = zoomy;
			player_x = x;
			player_y = y;
		}
		
		var diffx = getValx
		var diffy = getValy
    

	
		var curviewx = getValx*800//camera_get_view_x(curview);
		var curviewy = getValy*600//camera_get_view_y(curview);

		//represents x and y position of the view

		var viewx = curviewx*.5 -player_x
		var viewy = curviewy*.5 - player_y

		draw_temp_xx =(global.scale_factor*(viewx+x)/diffx);
		draw_temp_yy = (global.scale_factor*(viewy+y)/diffy);
	}
	// end update drawing prop details

	if(disappear){
		if(image_alpha > 0){
			image_alpha -= disappear_counter;
		} else {
			instance_destroy();	

		}
	}

	// end prop stuff

	// fly away stuff

	if(fly_away){
		x-= fly_move_x;
		y-= fly_move_y;
		if(instance_exists(sound_emitter)){
			sound_emitter.x = x;
			sound_emitter.y = y;
		}
	
		var dist = point_distance(Player.x,Player.y+Player.collision_y_adjust,x,y)
		if(dist > 600){
			instance_destroy();	
		}
		
		// do flaps
		if(menuControl.timer_frame_count - flaptimer_time > flap_timer){
			flaptimer_time = menuControl.timer_frame_count;	
			with(sound_emitter){
				instance_destroy();	
			}

				sound_emitter = instance_create_depth(x,y,0,AudioEmitter);
				//sound_emitter.check_for_collision = true;
				sound_emitter.audio_sound = sound3D_bird_flap;
				sound_emitter.falloff_max = 600
				sound_emitter.falloff_min = 25
				sound_emitter.pitch_base = random_range(.85,1.15);
		}
	}

	depth =y - depth_change
}