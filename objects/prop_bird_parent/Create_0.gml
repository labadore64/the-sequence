/// @description Insert description here
// You can write your code in this editor
event_inherited();

name = "Bird"

sprite_idle = spr_bird_robin_idle;
sprite_fly = spr_bird_robin_fly;

face_left = choose(true,false); //is bird facing left

fly_distance = 100; //distance that bird will fly away. -1 means it won't fly away.

guide_distance = 150; //distance you need to be to be able to register in bird guide.

fly_direction = 0;

fly_speed = 6;

fly_away = false;

sound_emitter = noone;

audio_sound = -1

fly_move_x = 0;
fly_move_y = 0;

destroy_chance = 0;

millet = false;
suet = false;
carrion = false;

image_index = irandom(1000)

newalarm[1] = 1
newalarm[3] = 2;

depth_change = 35;

flap_timer = 1000;
flaptimer_time = 0;