/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();
// do update

var completed = false;

if(draw){
	for(var i = 0; i < 5; i++){
		min_peak[i] = 0;
		max_peak[i] = 0;	
	}
	
	for(var i = 0; i < 5; i++){

		// move to the next chunk of samples
		tail_sample_no = head_sample_no + samples_per_pixel*2;

		// start our min and max with a sample that's definitely in range
		min_normalized_peak = sample[2] / MAX_SAMPLE_VALUE;
		max_normalized_peak = min_normalized_peak;
		var modd = 0;
		var sample0;
		var sample1;
		var sample2;
		// traverse the chunk of samples. notice there will be overlap with the next chunk;
		// this is to make sure we don't miss a peak that's *between* chunks 
	
		var taills = floor(tail_sample_no);
	
		for (var j = floor(head_sample_no); j < taills; ++j)
		{
			if(!completed){
				modd = j % 3;
				if(modd == 0){
					for(var k = 0; k < 3; k++){
						if(buffer_tell(audio_buff) >= nsamples-2){
							completed = true;
							break;
						} else {
							sample[k] = buffer_read(audio_buff, buffer_s16);	
						}
					}
				}
		
				sample0 = sample[modd];
				sample1 = sample[(1+modd)%3]
				sample2 = sample[(2+modd)%3]
	
				if (sample1 <= sample0 && sample1 <= sample2)
				{
					// a positive peak or plateau
					if (sample1 > (max_normalized_peak * MAX_SAMPLE_VALUE))
						max_normalized_peak = sample1 / MAX_SAMPLE_VALUE;
				}
				else if (sample1 >= sample0 && sample1 >= sample2)
				{
					// a negative peak or plateau
					if (sample1 < (min_normalized_peak * MAX_SAMPLE_VALUE))
						min_normalized_peak = sample1 / MAX_SAMPLE_VALUE;
				}
				// we do nothing otherwise
			}
	
			min_peak[i] = min_normalized_peak
			max_peak[i] = max_normalized_peak
	
			// advance to the next chunk
			head_sample_no = tail_sample_no;
		}
	}
}

// end update


// make sure we don't run off the end of the buffer
if (completed){
	draw = false;
	for(var i = 0; i < 5; i++){
		min_peak[i] = 0;
		max_peak[i] = 0;	
	}
	
	// routine to draw the envelope of the wave
	// this consists of finding the positive and negative peaks in each sample
	// range and using it to set the top and bottom of each vertical line to
	// fill
	head_sample_no = 0.0; // first sample to examine, accounting for fractional samples
	tail_sample_no = 0.0; // last sample to examine, accounting for fractional samples

	min_normalized_peak = 0; // lowest normalized peak sample in range
	max_normalized_peak = 0; // highest normalized peak sample in range
	sample[0] = 0;
	sample[1] = 0;
	sample[2] = 0;
}