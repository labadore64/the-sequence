/// @description Insert description here
// You can write your code in this editor
#macro MAX_SAMPLE_VALUE 65535
#macro WAV_BUFFER_START 40
initTimingAlarm();

active = false;

width = 500;
height = 100;

surface = -1;
surface_last = -1;

nsamples = 0;
draw = false;
audio_buff = -1

sound = -1;

name = "";

i = -1;

// sample array, used to optimize buffer functions
sample[0] = 0;
sample[1] = 0;
sample[2] = 0;

//min and max peaks for drawing
for(var i = 0; i < 5; i++){
	min_peak[i] = 0;
	max_peak[i] = 0;
}

// routine to draw the envelope of the wave
// this consists of finding the positive and negative peaks in each sample
// range and using it to set the top and bottom of each vertical line to
// fill
head_sample_no = 0.0; // first sample to examine, accounting for fractional samples
tail_sample_no = 0.0; // last sample to examine, accounting for fractional samples

samples_per_pixel = 0 // how many samples per pixel

min_normalized_peak = 0; // lowest normalized peak sample in range
max_normalized_peak = 0; // highest normalized peak sample in range

newalarm[0] = 1;

