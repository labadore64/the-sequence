{
    "id": "a75d5a26-9319-4e58-9809-100bfd8e5bb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "WaveDrawer",
    "eventList": [
        {
            "id": "69b326f8-ff47-4e52-8280-1e6c045ac0b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a75d5a26-9319-4e58-9809-100bfd8e5bb3"
        },
        {
            "id": "99a2ac76-6bcc-464c-8be4-723b3e570b26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a75d5a26-9319-4e58-9809-100bfd8e5bb3"
        },
        {
            "id": "76adc6f9-dd0c-4918-afbd-7d300d919366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a75d5a26-9319-4e58-9809-100bfd8e5bb3"
        },
        {
            "id": "bacbf87e-92aa-4f92-a9f5-5d9e14a19411",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a75d5a26-9319-4e58-9809-100bfd8e5bb3"
        },
        {
            "id": "576d5e62-7407-4bf8-8db6-d7fed5100d7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a75d5a26-9319-4e58-9809-100bfd8e5bb3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}