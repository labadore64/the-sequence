/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
menuControlForceDrawMenuBackground();

if(ds_exists(bird_list,ds_type_list)){
	ds_list_destroy(bird_list);	
}

	with(Player){
		item_use_lock = false;	
	}
	
	hudControllerClearData()