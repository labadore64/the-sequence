{
    "id": "f7a070ee-eab3-4a4a-a39a-3af62263e369",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBirdGuide",
    "eventList": [
        {
            "id": "35f867cb-bf7a-4a95-831b-b940b49139f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7a070ee-eab3-4a4a-a39a-3af62263e369"
        },
        {
            "id": "211c8eec-9a3a-4b40-9e4c-f8713bb34086",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f7a070ee-eab3-4a4a-a39a-3af62263e369"
        },
        {
            "id": "17c28932-e4ca-4588-adc5-336bf984332c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f7a070ee-eab3-4a4a-a39a-3af62263e369"
        },
        {
            "id": "a6563b22-b00b-419c-8bd6-97ed9cef7383",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "f7a070ee-eab3-4a4a-a39a-3af62263e369"
        },
        {
            "id": "0d79452d-a6cc-4fc0-ace8-8d938f4ad65b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7a070ee-eab3-4a4a-a39a-3af62263e369"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}