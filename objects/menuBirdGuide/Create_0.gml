/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

	menuControlForceDrawMenuBackground();
	
	with(Player){
		item_use_lock = true	
	}

//first, check if any birds are close enough. If there 
//is a new bird, add it to the new bird list!

menuParentSetTitle("Bird Guide");
menuParentSetDrawSubtitle(false)

new_bird = "";
var birdname = "";

var dist = 0;

var map = obj_data.bird_seen;
var val = "";
with(prop_bird_parent){
	if(!fly_away){
		dist = point_distance(Player.x,Player.y+Player.collision_y_adjust,x,y);
		if(dist < guide_distance){
			val = ds_map_find_value(map,name);
			if(is_undefined(val)){
				//add new bird!
				addBird(name);
				birdname = name;
			}
		}
	}
}

new_bird = birdname;

bird_list = ds_list_create();

var namelist = ds_list_create();
var timelist = ds_list_create();

var size = ds_map_size(map) ;
var key = ds_map_find_first(map);
ds_list_add(namelist,key);
ds_list_add(timelist,ds_map_find_value(map,key))
for (var i = 0; i < size; i++;)
   {

      key = ds_map_find_next(map, key);
	  if(!is_undefined(key)){
		ds_list_add(namelist,key);
		ds_list_add(timelist,ds_map_find_value(map,key))
	  }
   }
   
var prio = ds_priority_create();
var sizee = ds_list_size(namelist);
for(var i = 0; i < sizee; i++){
	ds_priority_add(prio,namelist[|i],timelist[|i]);
}

while(!ds_priority_empty(prio)){
	ds_list_add(bird_list,ds_priority_delete_min(prio))
}

ds_priority_destroy(prio);
   
//ds_list_sort(bird_list,true);

newalarm[2] = 2

menu_size = ds_list_size(bird_list);

menu_draw = menuBirdGuideDraw;

menu_left = menuBirdGuideLeft;
menu_right = menuBirdGuideRight

menu_up = menuBirdGuideUp;
menu_down = menuBirdGuideDown;

menu_select = menuBirdGuideSelect

if(ds_map_empty(obj_data.bird_seen)){
	soundfxPlay(soundMenuInvalid)
	instance_destroy();
} 

//bird draw stuff

char_per_line = 25

bird_name = "";
bird_long_name = "";
bird_science_name = "";

bird_desc = "";
bird_height = "";
bird_weight = "";
bird_time = "";

bird_male = true;

bird_male_sprite = -1;
bird_female_sprite = -1;

bird_sprite = spr_bird_robin_idle//-1;
bird_adjust = 0;
bird_scale = 2;

ds_list_destroy(namelist)
ds_list_destroy(timelist)

animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = 5

draw_bg = false;

//active = false;

fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

menu_control = menu_cancel

menu_help = "birdBook"

menu_ax_HUD = "birdBook"

ignore_tab = true