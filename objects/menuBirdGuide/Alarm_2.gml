/// @description Insert description here
// You can write your code in this editor
menuControlForceDrawMenuBackground();
if(obj_data.bird_seen_count != ds_list_size(bird_list)){
	var obj = instance_create(0,0,battleWindow);
	obj.windowText = "Found new bird!";
	obj_data.bird_seen_count = ds_list_size(bird_list);
	menupos = obj_data.bird_seen_count -1;
	bird_name = new_bird;
} else {
	bird_name = bird_list[|0];	
}

menuBirdGuideUpdate();