var key;
var state;

// test the state of all keybound keyboard values
for(var i = 0; i < global.keybind_size; i++){
	key = global.keybind_value_array[i];
	
	// updates the states of the key
	if (keyboard_check_pressed(key)){
		state = KEY_STATE_PRESS
	} else if(keyboard_check(key)){
		state = KEY_STATE_HOLD
	} else if (keyboard_check_released(key)){
		state = KEY_STATE_RELEASE
	} else {
		state = KEY_STATE_NONE
	}
		
	global.key_state_array[i] = state;
}


// if the gamepad is active
if(global.gamepad > -1){
	var key;
	var state;

	// test the state of all keybound gamepad values
	for(var i = 0; i < global.gamepad_size; i++){
		key = global.gamepad_value_array[i];

		// updates the states of the gamepad 
		if (gamepad_button_check_pressed(global.gamepad,key)){
			state = KEY_STATE_PRESS
		} else if(gamepad_button_check(global.gamepad,key)){
			state = KEY_STATE_HOLD
		} else if (gamepad_button_check_released(global.gamepad,key)){
			state = KEY_STATE_RELEASE
		} else {
			state = KEY_STATE_NONE
		}
		
		global.gamepad_state_array[i] = state;

	}
	
	objGenericDoGamepad()
}