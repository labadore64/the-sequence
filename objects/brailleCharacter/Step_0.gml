/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
if(cells_changed){
	for(i = 0; i < 2; i++){
		for(j = 0; j < 3; j++){
			if(cell[i,j] != cell_last[i,j]){
				cell_transition[i,j] = 0;
			}
			
			cell_last[i,j] = cell[i,j];
		}
	}
}

for(i = 0; i < 2; i++){
	for(j = 0; j < 3; j++){
		if(cell_transition[i,j] > -1){
			surf_update = true;
			cell_transition[i,j]+=menuControl.timer_diff;
			// reset if passes the max time
			if(cell_transition[i,j] > cell_trans_time){
				cell_transition[i,j] = -1;	
			}
		}
	}
}

if(highlighted){
	highlight_move_counter++
	highlight_move_amount_y = sin(highlight_move_time*highlight_move_counter*.5*pi);	
	surf_update = true;
	
	highlighted_text_color_counter+=3;
	if(highlighted_text_color_counter > 255){
		highlighted_text_color_counter = highlighted_text_color_counter % 255;
	}
	
	highlight_text_color = merge_color(make_color_hsv(highlighted_text_color_counter,255,255),c_white,.5);
}