/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();

for(i = 0; i < 2; i++){
	for(j = 0; j < 3; j++){
		cell[i,j] = false;	
		cell_last[i,j] = false;
		cell_transition[i,j] = -1;
	}
}

cell_character = "";
last_character = "";

// whether or not this character is a drawable one
draw_character = false;

// whether or not to draw the character
display_character = true;

cells_changed = false;

cell_trans_time = 9

cell_scale = 1;

surf = -1;
surf_update = true;
width = 40;
height = 200;

text_color = c_white;
default_text_color = global.textColor;
image_blend = global.textColor;
highlight_text_color = c_yellow;
highlighted = false;

highlighted_text_color_counter = 0;

highlight_move_counter = 0;
highlight_move_multi = 2;
highlight_move_amount_y = 0;
highlight_move_time = 1/10;
