{
    "id": "6b47835d-f402-4eba-8c8d-61e504d0337b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleCharacter",
    "eventList": [
        {
            "id": "e6e8686f-938f-4f3f-850b-c98b2039698c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b47835d-f402-4eba-8c8d-61e504d0337b"
        },
        {
            "id": "1ea56481-52d2-48ef-965f-8e3ca299bd00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6b47835d-f402-4eba-8c8d-61e504d0337b"
        },
        {
            "id": "0b6abe2f-992e-4e2c-8612-33c58eb198ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6b47835d-f402-4eba-8c8d-61e504d0337b"
        },
        {
            "id": "e3c881c2-ac0f-4a25-83f2-676d74069efe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6b47835d-f402-4eba-8c8d-61e504d0337b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}