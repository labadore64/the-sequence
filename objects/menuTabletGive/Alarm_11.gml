/// @description Insert description here
// You can write your code in this editor


braille_obj = instance_create(
					170,
					100,
					brailleCharacter);
braille_obj.display_character = false;
braille_obj.cell_scale = 2;
braille_obj.cell_trans_time =0;
b_bcharacter = string_upper(braille_character);

brailleDataSetCharacter(braille_character,braille_obj);	

if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	ds_list_clear(cando_list);

	var len = ds_list_size(obj_data.party)

	var cando = false;
	for(var i = 0; i < len; i++){
		cando = false;
		menuAddOption(obj_data.party[|i].name,"",menuTabletGiveSelect)
		for(var j = 0; j < 3; j++){
			if(obj_data.party[|i].tablet[j] == -1){
				cando = true;	
			}
			if(obj_data.party[|i].tablet[j] == braille_id){
				cando = false;
				break;
			}
		}
		ds_list_add(cando_list,cando);
	}
	
	menupos = 0;
	
	var counter = 0;

	while(!cando_list[|menupos]){
		menupos++;
		if(menupos > menu_size-1){
			if(wrap){
				menupos = 0;	
			} else {
				menupos = menu_size-1;
			}
		}
		counter++
		if(counter > 300){
			break;	
		}
	}
	
	menu_size = menuGetSize();
	character = obj_data.party[|menupos];
	character_id = -1;
	receive_id = character.character;
}

menuParentUpdateBoxDimension();

// Inherit the parent event
active = true;
menu_ax_HUD = "tabletChange"
menuTabletGiveUpdate();

hudPopulateOverworldStatsValues("character_new",receive_id,
	character.temp_phy_power,
	character.temp_phy_guard,
	character.temp_mag_power,
	character.temp_mag_guard,
	character.temp_spe_power,
	character.temp_spe_guard,
	projected_tablets[0],
	projected_tablets[1],
	projected_tablets[2]);

menuParentSetTitle("Give " + string_upper(braille_character) + "\nto whom?");
menuParentSetSubtitle("");


tts_title_string = title;

tts_clear();
if(tts_title_string != ""){
	tts_say(tts_title_string)	
}

mouseHandler_menu_default()
mouseHandler_menu_setCursor();