/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

insert_index = -1;

draw_width = 650; //width of box in pixels
draw_height = 450; //height of box in pixels
title_x_offset = 300; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 300; //xoffset for title
subtitle_y_offset = 60; //yoffset for subtitle

option_y_offset = 360; //yoffset for options
option_x_offset = 80; //yoffset for options



menu_left = menuTabletGiveUp
menu_right = menuTabletGiveDown

menu_up = menu_left;
menu_down = menu_right;

menu_help = "tabletsSub"

ignore_tab = true;