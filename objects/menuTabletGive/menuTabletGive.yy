{
    "id": "00e8f880-ea72-49e5-8a60-2de5525111a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTabletGive",
    "eventList": [
        {
            "id": "c81c87d2-5eaa-4ab3-8f32-12ec088ffc45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00e8f880-ea72-49e5-8a60-2de5525111a9"
        },
        {
            "id": "5bcbfe5d-7968-4d8c-89e6-76768916b37e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "00e8f880-ea72-49e5-8a60-2de5525111a9"
        },
        {
            "id": "acf6a369-780b-4970-93e5-27cb9821de94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "00e8f880-ea72-49e5-8a60-2de5525111a9"
        },
        {
            "id": "caddf014-62c6-4c1f-8a31-68503593ce88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "00e8f880-ea72-49e5-8a60-2de5525111a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "491ff2aa-c4b6-4d04-a746-1e7602fe233b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}