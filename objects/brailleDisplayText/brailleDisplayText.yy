{
    "id": "b440ab39-483f-4449-b193-0ea6c5575b7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleDisplayText",
    "eventList": [
        {
            "id": "835955d8-55e3-47f1-a1b8-68f6a3da6ee5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b440ab39-483f-4449-b193-0ea6c5575b7c"
        },
        {
            "id": "718db282-22c8-48c6-aafd-bf795954d644",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "b440ab39-483f-4449-b193-0ea6c5575b7c"
        },
        {
            "id": "4508d52c-ecff-4415-9e99-716304f32ca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "b440ab39-483f-4449-b193-0ea6c5575b7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "19943076-b550-4d21-9f00-14427af681b4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}