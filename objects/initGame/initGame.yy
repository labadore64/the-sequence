{
    "id": "fefab0c6-7a56-4afe-a45a-a4ae61f00a37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "initGame",
    "eventList": [
        {
            "id": "42bffdea-bfc6-4271-a4c4-8ff092a68bc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fefab0c6-7a56-4afe-a45a-a4ae61f00a37"
        },
        {
            "id": "4bbaa7c7-c5eb-48eb-96eb-343921c595da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fefab0c6-7a56-4afe-a45a-a4ae61f00a37"
        },
        {
            "id": "69128a34-a6a8-45df-8976-4f3d05028579",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fefab0c6-7a56-4afe-a45a-a4ae61f00a37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}