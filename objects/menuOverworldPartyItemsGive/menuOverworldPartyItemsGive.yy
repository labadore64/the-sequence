{
    "id": "da56244d-2b89-498b-918c-df7630e70364",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldPartyItemsGive",
    "eventList": [
        {
            "id": "8befbe3f-b999-46f6-9e07-86a050e600f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da56244d-2b89-498b-918c-df7630e70364"
        },
        {
            "id": "2396ae09-56f6-46e5-94e9-a5cb3a3be57d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "da56244d-2b89-498b-918c-df7630e70364"
        },
        {
            "id": "23a3118c-cea4-4be7-9c31-e658fd0fff7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "da56244d-2b89-498b-918c-df7630e70364"
        },
        {
            "id": "1c697a0c-5302-474b-9967-a98bbedd21b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "da56244d-2b89-498b-918c-df7630e70364"
        },
        {
            "id": "618c5f86-cef6-4df8-a469-1aca5fcdb061",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "da56244d-2b89-498b-918c-df7630e70364"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}