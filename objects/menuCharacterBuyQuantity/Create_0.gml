/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

title = "Buy how many?"
var itemz = 0;

with(menuOverworldItems){
	itemData(item_id[|menupos]);
	itemz = floor(price*multiplier)
}

price_item = itemz

price_total = price_item;

menu_size = floor(obj_data.money / price_item)+1;
quantity = menu_size;

menu_select = menuCharacterBuyConfirm;

menu_draw = menuCharacterBuyQuantityDraw
