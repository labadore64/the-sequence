{
    "id": "7eddeaa6-ac8e-426d-aca6-138e8756dca3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterBuyQuantity",
    "eventList": [
        {
            "id": "3659e2cb-d547-4765-b7b5-b87c834d66fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7eddeaa6-ac8e-426d-aca6-138e8756dca3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1b036b0a-81c5-40ba-8d63-4ee67dd216c0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}