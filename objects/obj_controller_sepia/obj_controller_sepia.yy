{
    "id": "84792c35-418e-4bbe-a9c8-6fe76d080469",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_sepia",
    "eventList": [
        {
            "id": "dd609ca3-3f6b-4062-b8c2-8d26d016483d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84792c35-418e-4bbe-a9c8-6fe76d080469"
        },
        {
            "id": "cc1b47cd-eb1f-4ca3-bf65-5a6de28d54f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "84792c35-418e-4bbe-a9c8-6fe76d080469"
        },
        {
            "id": "50cfbbe3-e3f6-40cf-a21e-0edd6f7ad936",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "84792c35-418e-4bbe-a9c8-6fe76d080469"
        },
        {
            "id": "4246f0dc-b0b7-4455-894d-fc93ae7cf00e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "84792c35-418e-4bbe-a9c8-6fe76d080469"
        },
        {
            "id": "8ce9c46e-d0b9-41f5-bf1e-d242d9e6d1ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "84792c35-418e-4bbe-a9c8-6fe76d080469"
        },
        {
            "id": "1dff5679-256c-4f14-be66-7e996176bb37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "84792c35-418e-4bbe-a9c8-6fe76d080469"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}