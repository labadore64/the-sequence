{
    "id": "cc0960a1-6e6f-4018-ab4b-c4ffbee60ab9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CutsceneCommand",
    "eventList": [
        {
            "id": "01c0c9d5-06a5-4ab5-8f1d-140ccc01db0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc0960a1-6e6f-4018-ab4b-c4ffbee60ab9"
        },
        {
            "id": "93ab0ae8-fa63-4a5c-baa6-7a9f02591170",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "cc0960a1-6e6f-4018-ab4b-c4ffbee60ab9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}