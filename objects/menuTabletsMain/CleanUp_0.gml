/// @description Insert description here
// You can write your code in this editor
event_inherited();


if(surface_exists(surf)){
	surface_free(surf);	
}

	for(var i = 0; i < braille_rows; i++){
		for(var j = 0; j < braille_columns; j++){
			with(display_cell[i,j]){
				instance_destroy()
			}
		}
	}
	
clearHUD();