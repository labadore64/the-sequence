/// @description Insert description here
// You can write your code in this editor

event_inherited();

menu_use_generic = true
tablet_seen = 0;
tablet_total = 26;

for(var i = 0; i < 26; i++){
	// whether or not you collected a tablet
	if(obj_data.tablet_obtained[i]){
		tablet_seen++;	
	}
}

title = "Tablets"
subtitle = "Select a tablet."

select_script = menuTabletsMainScript

menu_draw = menuTabletsMainDraw;

menu_up = menuTabletsMainUp
menu_down = menuTabletsMainDown
menu_left = menuTabletsMainLeft
menu_right = menuTabletsMainRight

selected_column = 0;
selected_row = 0;

// draw stuff
start_x = 65;
start_y = 120;

space_x = 90;
space_y = 100;

braille_rows = 4;
braille_columns = 8;

skip = braille_columns;
active_cell[0] = false;

surf = -1;
surface_update = false;

menupos_old = menupos

menuParentUpdateBoxDimension()

menu_ax_HUD = "tabletMain"

menu_help = "tabletsMain";