{
    "id": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTabletsMain",
    "eventList": [
        {
            "id": "52dfcc2e-af12-4701-8c58-07acd57a71d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5"
        },
        {
            "id": "5c1e6ef4-3aba-4546-a9ed-a755364f4a92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5"
        },
        {
            "id": "214c1e71-3d8b-44fc-bb72-e82430dd5ae4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5"
        },
        {
            "id": "cbd836bd-7cd4-404a-b94c-ee9e16645e57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "1941581b-8bbb-4ac9-abba-0c5901874f60",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c62c3ef1-4cf8-414b-a855-8e98fa0c90b8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "38b16d06-0467-488a-a47b-ae3608ed2754",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "cd66a192-b62e-4797-a101-91d43edc753f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}