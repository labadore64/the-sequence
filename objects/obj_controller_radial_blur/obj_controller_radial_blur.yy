{
    "id": "73d99a11-3067-411b-8b79-77f2b4a5906b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_radial_blur",
    "eventList": [
        {
            "id": "5c36fdab-20f0-4925-86a9-a74fc01bd66d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73d99a11-3067-411b-8b79-77f2b4a5906b"
        },
        {
            "id": "efc5ce90-1ddf-4c80-9eb9-c7fbe5700088",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73d99a11-3067-411b-8b79-77f2b4a5906b"
        },
        {
            "id": "84ce4275-4c93-45c0-bf1a-5c1ddb75ea50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "73d99a11-3067-411b-8b79-77f2b4a5906b"
        },
        {
            "id": "d0e2668c-4295-4120-a655-d8a7b4aee0e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "73d99a11-3067-411b-8b79-77f2b4a5906b"
        },
        {
            "id": "1134a0e0-9a63-4fd6-9c51-66d13ce20136",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "73d99a11-3067-411b-8b79-77f2b4a5906b"
        },
        {
            "id": "5e362714-a8af-49b3-9d61-daaad7864d4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "73d99a11-3067-411b-8b79-77f2b4a5906b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}