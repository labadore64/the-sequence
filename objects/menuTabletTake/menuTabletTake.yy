{
    "id": "491ff2aa-c4b6-4d04-a746-1e7602fe233b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTabletTake",
    "eventList": [
        {
            "id": "6cb1b798-cf28-47eb-96a2-56db032568ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "491ff2aa-c4b6-4d04-a746-1e7602fe233b"
        },
        {
            "id": "ea6240a9-99d5-4cf9-a6f2-8be24117df54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "491ff2aa-c4b6-4d04-a746-1e7602fe233b"
        },
        {
            "id": "e9679e03-414b-47bf-bf80-875c0c1b1e3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "491ff2aa-c4b6-4d04-a746-1e7602fe233b"
        },
        {
            "id": "333fed05-f050-4118-8b40-0edd0e5d1ccf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "491ff2aa-c4b6-4d04-a746-1e7602fe233b"
        },
        {
            "id": "c35c4153-487d-41c5-86be-60253d97645c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "491ff2aa-c4b6-4d04-a746-1e7602fe233b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}