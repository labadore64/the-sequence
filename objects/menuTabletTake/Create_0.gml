/// @description Insert description here
// You can write your code in this editor
event_inherited();

cando_list = ds_list_create()

// character id of origin character
character_id = -1;

// character id of giving character
receive_id = -1;

sprite_ind = 0;

braille_character = "";
braille_id = -1;
braille_cell = noone;

b_bcharacter = "";

braille_obj = noone;

old_stat = instance_create_depth(0,0,0,dataDrawStat);
new_stat = instance_create_depth(0,0,0,dataDrawStat);

for(var i =0; i < 3; i++){
	projected_tablets[i] = 0;
}

radius = 70;

move_element_sprite = -1;
move_type_sprite = -1;

multtt = 0;
multttz = 0;
maxdamage = 50;

character = noone;


menuParentSetInfoboxHeight(550);
menuParentSetInfoboxTitleYOffset(250)
menuParentSetInfoboxSubtitleYOffset(10)
menuParentSetInfoboxSubtitleColor(global.textColor);

menuParentSetDrawTitle(true);
menuParentSetDrawSubtitle(false);
menuParentSetDrawGradient(false);

x = 400;
y = 275;

draw_width = 650; //width of box in pixels
draw_height = 450; //height of box in pixels
title_x_offset = 300; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 300; //xoffset for title
subtitle_y_offset = 60; //yoffset for subtitle

option_y_offset = 360; //yoffset for options
option_x_offset = 80; //yoffset for options

select_percent = .37; //how much of the menu does the selection graphic occupy

if(ds_exists(menu_name,ds_type_list)){

	ds_list_clear(menu_name); //name of all menu items
	ds_list_clear(menu_description); //descriptions of all menu items
	ds_list_clear(menu_script); //scripts for all menu items

	menuAddOption(LANG_SELECT_YES,"",menuTabletTakeSelect)
	menuAddOption(LANG_SELECT_NO,"",menuParentCancel)
	
	repeat(2){
		ds_list_add(cando_list,true);
	}
	
	menupos = 0;
	menu_size = menuGetSize();

}

menuParentUpdateBoxDimension();

menu_draw = menuTabletTakeDraw

menu_left = menuOverworldPartySubLeft;
menu_right = menuOverworldPartySubRight;

menu_up = menu_left;
menu_down = menu_right;

//menuParentTTSLabelRead();

portrait = noone

move_id = -1;

menuControlForceDrawMenuBackground();
	
menu_help = "tabletsSub"
drawOtherMenus = false

ignore_tab = true;