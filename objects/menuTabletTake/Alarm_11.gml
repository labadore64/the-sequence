/// @description Insert description here
// You can write your code in this editor


braille_obj = instance_create(
					170,
					100,
					brailleCharacter);
braille_obj.display_character = false;
braille_obj.cell_scale = 2;
braille_obj.cell_trans_time =0;
b_bcharacter = string_upper(braille_character);

brailleDataSetCharacter(braille_character,braille_obj);	

menuParentSetTitle("Take " + string_upper(braille_character) + "\nfrom\n" + character.name + "?");
menuParentSetSubtitle("");

// calculate projected tablets

for(var i =0; i < 3; i++){
	if(character.tablet[i] == braille_id){
		projected_tablets[i] = -1;
	} else {
		projected_tablets[i] = character.tablet[i];
	}
}

// do stats
character_id = character.character;


var chara = character;
var radd = radius;
with(old_stat){
	character = chara
	boost_phy_power = character.tablet_phy_power
	boost_phy_guard = character.tablet_phy_guard
	boost_mag_power = character.tablet_mag_power
	boost_mag_guard	= character.tablet_mag_guard
	boost_spe_power = character.tablet_spe_power
	boost_spe_guard = character.tablet_spe_guard
	draw_hex_init(character,radd,character.phy_power,
				character.phy_guard ,
				character.mag_power ,
				character.mag_guard ,
				character.spe_agility ,
				character.spe_mpr);
	radius = radd;
}

// do newstat

var temp_pp = 0;
var temp_pg = 0;
var temp_mp = 0;
var temp_mg = 0;
var temp_sp = 0;
var temp_sg = 0;

for(var i = 0; i < 3; i++){
	if(projected_tablets[i] > -1){
		temp_pp+=	objdata_tablet.phy_power[projected_tablets[i]]
		temp_pg+=	objdata_tablet.phy_guard[projected_tablets[i]]
		temp_mp+=	objdata_tablet.mag_power[projected_tablets[i]]
		temp_mg+=	objdata_tablet.mag_guard[projected_tablets[i]]
		temp_sp+=	objdata_tablet.spe_power[projected_tablets[i]]
		temp_sg+=	objdata_tablet.spe_guard[projected_tablets[i]]
	}
}
with(chara){
	temp_phy_power = statCalculatePhysPower(temp_pp); //attack
	temp_phy_guard = statCalculatePhysGuard(temp_pg); //defense
	temp_mag_power = statCalculateMagPower(temp_mp); //magic
	temp_mag_guard = statCalculateMagGuard(temp_mg); //resistance
	temp_spe_power = statCalculateAgiPower(temp_sp); //agility
	temp_spe_guard = statCalculateAgiGuard(temp_sg); //mpr
}
with(new_stat){
	character = chara
	boost_phy_power = character.temp_phy_power
	boost_phy_guard = character.temp_phy_guard
	boost_mag_power = character.temp_mag_power
	boost_mag_guard	= character.temp_mag_guard
	boost_spe_power = character.temp_spe_power
	boost_spe_guard = character.temp_spe_guard
	draw_hex_init(character,radd,character.phy_power,
				character.phy_guard ,
				character.mag_power ,
				character.mag_guard ,
				character.spe_agility ,
				character.spe_mpr);
	radius = radd;
}

if(obj_data.tablet_character[braille_id].character > -1){
	menu_ax_HUD = "tabletChange"
	
	hudControllerAddData("tablet.action","Taking ");
	
	hudPopulateOverworldStatsValues("character_new",obj_data.tablet_character[braille_id].character,
	character.temp_phy_power,
	character.temp_phy_guard,
	character.temp_mag_power,
	character.temp_mag_guard,
	character.temp_spe_power,
	character.temp_spe_guard,
	projected_tablets[0],
	projected_tablets[1],
	projected_tablets[2]
		);
}

menuParentUpdateBoxDimension();

tts_title_string = title;

// Inherit the parent event
event_inherited();

mouseHandler_menu_default()
mouseHandler_menu_setCursor();