{
    "id": "b38106b3-a405-4a53-8874-b5845310d549",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "AudioEmitter",
    "eventList": [
        {
            "id": "b8e290c5-9b72-4708-ba42-e2dd3b80ab26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "246ca573-8dde-4d78-9e12-2f02daef0321",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "4652aca7-af57-49c3-aedb-c3b9efa0e224",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "4c55c711-49bb-4597-a1bb-0cce9ab12b9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "2b090901-e52f-4014-893f-ec2255fb48db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "a0546b8e-1ab9-4d0d-81f8-908feabc2b12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "df2faafd-bd65-471e-89df-2927393af5c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "c835d97c-9479-4355-9744-ed41cebb051c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "ac0d5837-7e66-46de-a004-c3ea44c8d46a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        },
        {
            "id": "08583baa-819c-47a9-84fe-777279bc97fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "b38106b3-a405-4a53-8874-b5845310d549"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}