{
    "id": "e8325799-e8f1-41db-b06f-d5081476dfd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_bird_housesparrow",
    "eventList": [
        {
            "id": "d6d0cf06-c354-4e93-87b0-cab25c87fa13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8325799-e8f1-41db-b06f-d5081476dfd5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca56b283-2970-4785-81ed-22b72d372248",
    "visible": true
}