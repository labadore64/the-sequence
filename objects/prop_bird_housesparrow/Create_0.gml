/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

name = "House Sparrow"

if(choose(true,false)){
	sprite_idle = spr_bird_housesparrow1_idle;
	sprite_fly = spr_bird_housesparrow1_fly;
} else {
	sprite_idle = spr_bird_housesparrow0_idle;
	sprite_fly = spr_bird_housesparrow0_fly;
}


fly_distance = 40; //distance that bird will fly away. -1 means it won't fly away.

guide_distance = 150; //distance you need to be to be able to register in bird guide.

fly_speed = 7;

audio_sound = sound3D_bird_housesparrow

destroy_chance = .9;

millet = true
suet = true;

