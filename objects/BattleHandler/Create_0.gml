/// @description Insert description here
// You can write your code in this editor

hp_sound[0] = sfx_hp_sound3;
hp_sound[1] = sfx_hp_sound2;
hp_sound[2] = sfx_hp_sound1;

event_inherited();
draw_black_bg  = false;

is_talking = false;

draw_white = true;
newalarm[2] = 2;

surf = -1;

battleHandlerCameraInit();

audio_group_load(audiogroup_braille)
audio_group_load(audiogroup_monster_voice)
audio_group_load(audiogroup_monstercry)
audio_group_load(audiogroup_battle)
camera = view_get_camera(0);
	
finish = false;

fleeing = false;
start_flee = false;

battle_start = true; //switch to false to change text box effect.

menu_select = -1

battleHandlerBackgroundInit();
active = false
orientation = 45; //orientation of the spinning effect.
orientation_last = 0; //last orientation
orientation_delta = 0; //speed of orientation change

h_size = 200; //horizontal distance
v_size = 35; //vertical distance
origin_x = 400; //origin of spinning x
origin_y = 350+70; //origin of spinning y

partyLevel = calcPartyLevel();

newalarm[0] = 1; //loads stuff into memory

rotation_amounts = ds_list_create();

character_draw_depth = ds_priority_create();

battleHandlerShaderInit()

battle_draw_surface = -1;

//these indicate the position on the circle for reference for lunging

base_x = 0;
base_y = 0;


//depth 

depth_horizon = 350;
var distertop = 25;
var disterbottom = 50;
depth_top = depth_horizon-distertop;
depth_bottom = depth_horizon+disterbottom;
horizon_color = $7f7f7f

party_list = ds_list_create();
enemy_list = ds_list_create();
opponents = ds_list_create();
allies = ds_list_create();

var dist = 23;

var obj = -1;

var getter = 0;

battleHandlerInitStats();

var base_display_orientation = ds_list_create();
var display_count = 0;
var orientation_counter = 0;

for(var i = 0; i < 3; i++){
	//base_display_orientation	
	getter = obj_data.party[|i];
	if(!is_undefined(getter)){
		display_count++;
	}
}

if(display_count == 1){
	ds_list_add(base_display_orientation,90);
} else if (display_count == 2){
	for(var i = 0; i < 2; i++){
		ds_list_add(base_display_orientation,90-15-dist*.5 + (1-(i-1))*dist);
	}
} else if (display_count == 3){
	for(var i = 0; i < 3; i++){
		ds_list_add(base_display_orientation,90-15 + (1-(i-1))*dist);
	}	
}

for(var i = 0; i < 3; i++){
	draw_char[i] = -1;
	getter = obj_data.party[|i];
	if(!is_undefined(getter)){
		if(instance_exists(getter)){
		
			obj = instance_create(0,0,Battler);
			obj.base_orientation = base_display_orientation[|orientation_counter]
			for(var j = 0 ; j < 3; j++){
				obj.tablet[j] = getter.tablet[j]
			}
			obj.is_enemy = false;
			obj.load_id = i;
			battlerAllyLoadStats(obj,getter)
			obj.overworld_character = getter 
			ds_list_add(party_list,obj);
			ds_list_add(allies,obj);
			draw_char[i] = obj;

			with(obj){
				battlerUpdateStats();
			}

			orientation_counter++;
		}
	}
}

ds_list_clear(base_display_orientation);
display_count = 0;
orientation_counter = 0;

for(var i = 0; i < 3; i++){
	//base_display_orientation	
	getter = obj_data.monster_spawn[i];
	if(!is_undefined(getter)){
		display_count++;
	}
}

if(display_count == 1){
	ds_list_add(base_display_orientation,270);
} else if (display_count == 2){
	for(var i = 0; i < 2; i++){
		ds_list_add(base_display_orientation,270-dist*.5 + (i-1)*dist);
	}
} else if (display_count == 3){
	for(var i = 0; i < 3; i++){
		ds_list_add(base_display_orientation,270 + (i-1)*dist);
	}	
}

for(var i = 0; i < 3; i++){
	
	getter = obj_data.monster_spawn[i];
	
	if(!is_undefined(getter)){
		if(getter > -1){
			setEnemySeen(getter);
			obj = instance_create(0,0,Battler);
			obj.base_orientation = base_display_orientation[|orientation_counter]
			obj.is_enemy = true;
			battlerEnemyLoadStats(obj,getter);
			

			with(obj){
				battlerUpdateStats();
			}
			
			ds_list_add(enemy_list,obj);
			ds_list_add(opponents,obj);
			
			orientation_counter++;
		}
	}
}

ds_list_destroy(base_display_orientation);

draw_texture_flush();

with(Battler){
	hp.current = hp.base;
	mp.current = mp.base;
	if(ability != "max_mp"){
		mp.current = floor(mp.base * .5)	
	}
}

money = obj_data.money;
money_this_round = 0;

instance_create(0,0,battleTextDisplay);

battle_history_text = ds_list_create();

scale_counter = 0;
scale_value = 0;
scale_time = 25

character_select[0] = false;
character_select[1] = false;
character_select[2] = false;

char_select_counter = 0;
char_select_value = 0;
char_select_time = 30

char_input_counter = -1;
char_input_character = 0;

attack_input = ds_list_create();
turn_count = 1;

color_active = false;

with(menuControl){
	visible = true;	
}

battleHandlerBattleMusicMain();
spr_counter_talk = 0;
select_list = ds_list_create();

battle_sim = false;


// these replace the aura effects
protect_status = false;
see_enemy_stats = false;
burn_enemy = false;
recover_ally = false;
reflect_attack = false;
MP_cheaper = false;

// braille stuff

braille_display = instance_create(60,10,brailleDisplayBattle);
braille_display.visible = false;

braille_load_count = -1;
braille_load_pitch = 1;

// if its 0 or 1, it goes incremental, otherwise its random
braille_insert_direction = 0;

incrementGameStat(GAMESTAT_BATTLE_TOTAL)

with(menuControl){
	please_draw_black = false;	
}

battleHandlerInitParticles();


stat_surf = -1;
stat_redraw = false;