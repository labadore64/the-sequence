/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm();

if(ScaleManager.updated){
	shd_bc_var_resolution_x = global.window_width;
	shd_bc_var_resolution_y = global.window_height;
	battleHandlerCameraUpdate();
	view_xport[0]=0;
	view_yport[0]=0;
	view_wport[0]=global.letterbox_w;
	view_hport[0]=global.letterbox_h;
	
	view_camera[0]=camera_view
}


event_inherited();

battleHandlerStepRotation();

//draw depths
ds_priority_clear(character_draw_depth);
var depth_dwellers = character_draw_depth;

var sizz = ds_list_size(opponents);

for(i = 0; i < sizz; i++){
	ds_priority_add(depth_dwellers,opponents[|i],opponents[|i].y);	
}

sizz = ds_list_size(allies);

for(i = 0; i < sizz; i++){
	ds_priority_add(depth_dwellers,allies[|i],allies[|i].y);	
}

battleHandlerFixOrientation();
battleHandlerUpdateBGPos()

//updates scale counter for selections.
scale_counter+=menuControl.timer_diff

var scale_fac = .1

scale_value = 1 + sin(degtorad((scale_counter*180/scale_time)))*scale_fac;

if(character_select[0] ||
	character_select[1] ||
	character_select[2]){
	char_select_counter++;

	var scale_fac = .5

	char_select_value = .5 + sin(degtorad((char_select_counter*180/char_select_time)))*scale_fac;

	} else {
char_select_counter = 0		
}

spr_counter_talk+=menuControl.timer_diff;

// braille load sounds

if(braille_load_count > -1){
	if(!audio_is_playing(soundBrailleInsert)){
		var sound = soundfxPlay(soundBrailleInsert);
	
		audio_sound_pitch(sound,braille_load_pitch);
	
		if(braille_load_count == 0){
			braille_load_pitch = 1;	
		} else {
			var mover = .2;
			
			if(braille_insert_direction == 0){
				braille_load_pitch += mover
			} else if (braille_insert_direction == 1){
				braille_load_pitch -= mover
			} else {
				braille_load_pitch += random_range(-mover*1.25,mover*1.25);
			}
		}
		braille_load_count--;
	}
}