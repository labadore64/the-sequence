{
    "id": "700df507-c4d1-4636-80de-d059bfc60b3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "BattleHandler",
    "eventList": [
        {
            "id": "73a73301-5b16-4c88-98ab-7d9d912f8cc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "36be4ed8-4446-47cb-b57e-7612ebac557c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "80de8c06-385e-4fe7-860f-774b71b1f797",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "87106801-1020-4120-9161-4202806ef423",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "f46311ed-d173-4e9a-9bd6-4df32a4f452e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "7655a200-e97f-4d57-b72e-f2111f957b7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "20011a5f-acf9-4f5b-b2c6-e48fba06cf8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "8363557a-004d-4651-813e-1376c180e9c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 9,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "78d98526-4a23-4e6f-8756-83b1e63fd40a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "735b2ae6-6934-4f38-b56c-7721bd316d4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "91702262-a446-45c4-a94a-e7f55f7c5efb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "06ae7a07-81e7-4450-8cbc-28e619e2dc47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "fbc855d2-dc24-47c9-a036-1901f78d7e84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "e567d3bb-8a37-4931-8fb7-29056cb85f79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "dc2e3352-34c8-4ba0-9b19-b8075d075aa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        },
        {
            "id": "60ac2e07-b3f7-451c-a61c-e831e2e6bc45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "700df507-c4d1-4636-80de-d059bfc60b3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}