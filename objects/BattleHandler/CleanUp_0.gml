/// @description Insert description here
// You can write your code in this editor
ds_list_destroy(rotation_amounts);

if(surface_exists(battle_draw_surface)){
	surface_free(battle_draw_surface);	
}

if(surface_exists(surf)){
	surface_free(surf);
	view_surface_id[0] = -1;
}

ds_list_destroy(party_list);
ds_list_destroy(enemy_list);
ds_list_destroy(opponents);
ds_list_destroy(allies)
ds_list_destroy(battle_history_text)

ds_list_destroy(attack_input);
ds_list_destroy(select_list);

if(ds_exists(character_draw_depth,ds_type_priority)){
	ds_priority_destroy(character_draw_depth );
}

if(instance_exists(obj_data)){
	obj_data.money = money;
}

with(braille_display){
	instance_destroy();	
}

audio_group_unload(audiogroup_braille)
audio_group_unload(audiogroup_monster_voice)
audio_group_unload(audiogroup_battle)
audio_group_unload(audiogroup_monstercry)
victory = false;

//update items

with(Battler){
	if(!is_enemy){
		for(var i = 0; i < 5; i++){
			if(instance_exists(overworld_character)){
				overworld_character.item[i] = item[i];	
			}
		}
	}
}

battleHandlerDestroyParticles();

draw_texture_flush();

if(surface_exists(stat_surf)){
	surface_free(stat_surf)	
}

mouseHandler_clear();