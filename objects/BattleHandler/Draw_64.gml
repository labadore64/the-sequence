/// @description Insert description here
// You can write your code in this editor
//Do horizontal blur first
if(dark_mode() || draw_black_bg){
	draw_clear_alpha(c_black,1);	
} else {
	if(draw_white){
		draw_clear_alpha(c_white,1)
	} else {
		if(!surface_exists(battle_draw_surface)){
			battle_draw_surface = surface_create_access(global.window_width,global.window_height)	
		} else {
			if(ScaleManager.updated){
				surface_resize(battle_draw_surface,global.window_width,global.window_height);
			}
		}

		if !surface_exists(surf)
		{
		   surf = surface_create(global.letterbox_w, global.letterbox_h);
		   view_surface_id[0] = surf;
		}


		draw_set_halign(fa_left)
		gpu_set_colorwriteenable(true,true,true,true)
		surface_set_target(battle_draw_surface);
		draw_clear_alpha(c_black,1)
		gpu_set_colorwriteenable(true,true,true,false)
		draw_surface(surf,global.display_x,global.display_y);
		surface_reset_target();

		battleHandlerDrawBlur()

		battleHandlerDrawDepthOfField()

		//surface_reset_target();

		gpu_set_colorwriteenable(true,true,true,true)

		drawSurfAsVCR(battle_draw_surface)

		battleHandlerDrawAttackFG();

		battleHandlerDrawStats();

		battleTextDisplayDraw();

		battleStartDraw();

		//draw braille display lol
		with(braille_display){
			brailleDisplayDraw(id);		
		}
	}

	draw_letterbox()
}