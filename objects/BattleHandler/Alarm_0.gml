/// @description Insert description here
// You can write your code in this editor

battleHandlerBackgroundSet();

//battleTextDisplayAddText("Blue-Eyes White Dragon was the monster ka spirit of Kisara, although it only emerged while she was in an unconscious state. With Kisara captured, Aknadin heard of the Dragon's power and hoped to transfer the ka to his son Seto, unconcerned that this would mean killing its current host. The imprisoned Kisara was unaware of her ka and thus unable to summon it. Seto, disgusted at the idea of using her without regard to her life, attempted to save her. He failed, and both their lives fell into danger. However, Kisara lost consciousness, causing the White Dragon to emerge and ultimately resulting in their rescue. Seto had come to care deeply for Kisara during this time. He defended her until her death at the hands of Aknadin, who proceeded to possess his son. Perhaps showing that she felt the same about him, Kisara's spirit entered Seto's mind and destroyed Aknadin, freeing the young priest. Afterwards, Seto extracted the White Dragon ka and used it to fight Zorc Necrophades. According to Kazuki Takahashi, Seto's feelings for Kisara are the reason behind Seto Kaiba's modern-day obsession with the Blue-Eyes White Dragon. Despite being always called Blue-Eyes White Dragon, or even simply just the White Dragon, Blue-Eyes always has a light blue body as well as blue eyes. Later card designs also clearly showed the dragon to be blue rather than white.")

//battleTextDisplayAddText(" ");

instance_create(0,0,battleStartAnimation);

battleHandlerFixOrientation();
battleHandlerUpdateBGPos()