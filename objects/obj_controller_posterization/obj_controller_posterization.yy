{
    "id": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_posterization",
    "eventList": [
        {
            "id": "a69e166f-a432-4def-b003-711f5f986a63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5"
        },
        {
            "id": "07252abf-2ba9-46f1-be75-5ae7c5f47a62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5"
        },
        {
            "id": "f26417f5-1a51-478f-92ea-93fda3df4bd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5"
        },
        {
            "id": "d2f8cc9c-520b-4ca7-b023-536ea87f0604",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5"
        },
        {
            "id": "8cd58d16-a33d-420d-8a3c-2740187ee91f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5"
        },
        {
            "id": "f154bdbd-b05b-43b5-985d-86aa83737524",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "fdbab360-5806-4dcb-a1a5-3dab6aea41a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}