{
    "id": "49ffea48-b997-4994-a90e-607d93aec6fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleGamePracticeMode",
    "eventList": [
        {
            "id": "3dd23802-4abe-44e5-999f-0601fc9e38e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "49ffea48-b997-4994-a90e-607d93aec6fa"
        },
        {
            "id": "02736df5-373b-4a6b-b0cb-ba3c90ff730a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "49ffea48-b997-4994-a90e-607d93aec6fa"
        },
        {
            "id": "7d99c3ac-bf6b-4f22-96b5-3bf7ab33b531",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "49ffea48-b997-4994-a90e-607d93aec6fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}