/// @description Insert description here
// You can write your code in this editor


menuParentNoBGCreate();

//menuOverworldSetStringsAsParty(menuOverworldSubScript) 

character = obj_data.party[|menuOverworldParty.menupos];

for(var i = 0; i < 5; i++){
	var itemz = character.item[i];
	itemData(itemz);
	menuAddOption(name,flavor,-1)
}

item_id = character.item[menupos]
itemData(item_id);

menuParentSetTitle(character.name + "'s items")
menuParentSetSubtitle("")

menuParentSetDrawSubtitle(false)
menuParentSetTitleXOffset(30)

menu_size = menuGetSize();

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

menu_draw = menuOverworldPartyItemsDraw

menuParentSetDrawGradient(false)


menuParentUpdateBoxDimension();

menuParentSetSelectWidth(.45)

menuParentSetOptionXOffset(10)

menuParentSetInfoboxSubtitleYOffset(45)

Sname = part_system_create();

particle1 = part_type_create();
part_type_shape(particle1,pt_shape_square);
part_type_size(particle1,0.50,0.50,0.00,0);
part_type_scale(particle1,1,1);
part_type_color3(particle1,color_value,color_value,c_black);
part_type_alpha2(particle1,1,0);;
part_type_gravity(particle1,0.10,90);
part_type_blend(particle1,1);
part_type_life(particle1,30,90);

emitter1 = part_emitter_create(Sname);
var plx = 620;
var ply = 575;

part_emitter_region(Sname,emitter1,-202+plx,202+plx,-1+ply,1+ply,ps_shape_ellipse,1);
part_emitter_stream(Sname,emitter1,particle1,2);

part_system_automatic_draw(emitter1, false);
part_system_automatic_update(emitter1, false);

repeat(100){
	part_system_update(Sname);	
}


menuParentTTSTitleRead();

if(name != ""){
	tts_say(name);	
}

menu_up = menuOverworldPartyItemsLeft;
menu_down = menuOverworldPartyItemsRight;

menu_left = menu_up;
menu_right = menu_down;

menuOverwordPartyItemInfokeyStrings();

//menuParentTTSLabelRead();


animation_start = 5 //animation length

animation_len = animation_start;

newalarm[3] = 5;

draw_bg = false;

//active = false;

fadeIn(5,c_black)

with(screenFade){
	script_run = false;	
}

destroy_time = 10

destroy_script = menuFadeOut;

clean_script = menuFadeIn;


with(menuCharacterSwitchParty){
	var chara = obj_data.party[|menupos].character
	char_color = obj_data.party[|menupos].color
	portrait.character = chara;
	menuOverworldPartyUpdateStatsPortrait(obj_data.party[|menupos],100,drawstat);
	with(portrait){
		surface_free(surface)	
		event_perform(ev_alarm,0)
	}	
}

with(menuCharacterSwitchRecruit){
	//recruit_list 
	var chara = recruit_list[|menupos].character
	char_color = recruit_list[|menupos].color
	portrait.character = chara;
	menuOverworldPartyUpdateStatsPortrait(recruit_list[|menupos],100,drawstat);
	with(portrait){
		surface_free(surface)	
		event_perform(ev_alarm,0)
	}	
}

newalarm[2] = 1;