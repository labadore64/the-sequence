{
    "id": "ff53790a-f599-4cf4-a2a4-6d9c09ff3104",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSwitchPartyItems",
    "eventList": [
        {
            "id": "a043f2b1-f199-4f00-adc2-05331cfc1b26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff53790a-f599-4cf4-a2a4-6d9c09ff3104"
        },
        {
            "id": "1d058692-c95b-4b1d-b578-538daa149c48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "ff53790a-f599-4cf4-a2a4-6d9c09ff3104"
        },
        {
            "id": "a8e284da-faf9-4edf-be9b-0304f3e3f497",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ff53790a-f599-4cf4-a2a4-6d9c09ff3104"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9cf153c4-3de1-4ae1-ae21-7639931c13a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}