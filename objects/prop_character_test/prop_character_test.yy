{
    "id": "b0864de5-0a93-4794-b6cc-88704343f7e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_character_test",
    "eventList": [
        {
            "id": "6eb3f2fe-276e-436a-9af9-6ff61d5246e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0864de5-0a93-4794-b6cc-88704343f7e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68cc3a71-5f10-4df3-b90a-75048f7ff35c",
    "visible": true
}