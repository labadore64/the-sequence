{
    "id": "fe3deea6-05d1-4137-9a29-fbe06b4f5dae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSwitchParty",
    "eventList": [
        {
            "id": "c83c7801-28bc-445a-aff3-dfeb5816369f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe3deea6-05d1-4137-9a29-fbe06b4f5dae"
        },
        {
            "id": "802d5ad3-275a-4a09-8d18-f9d85691f8fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "fe3deea6-05d1-4137-9a29-fbe06b4f5dae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f126f65-0956-4831-9210-5223e85c1baf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}