/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menu_select = menuCharacterSwitchPartySubScript;

menu_draw = menuCharacterSwitchPartyDraw

menu_left = menuCharacterSwitchPartyLeft;
menu_right = menuCharacterSwitchPartyRight;
menu_up = menu_left;
menu_down = menu_right;

portrait = instance_create(0,0,drawPortrait);

drawstat = instance_create_depth(0,0,0,dataDrawStat);

char_color = c_black;

menuOverworldPartyUpdateStatsPortrait(obj_data.party[|menupos],100,drawstat);
char_color = obj_data.party[|menupos].color
var chara = obj_data.party[|menupos].character

var scalez = .6

/*

*/
portrait.character = chara;
portrait.image_xscale = scalez
portrait.image_yscale = scalez

release_the_penguins = true;

text_bottomline = "Select a character to switch out."