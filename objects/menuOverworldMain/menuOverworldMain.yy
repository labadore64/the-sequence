{
    "id": "e58af6ce-7a26-4f91-aa66-d01d0a8bb19a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldMain",
    "eventList": [
        {
            "id": "efe9fcd2-9932-4472-9997-9fed9b57f8b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e58af6ce-7a26-4f91-aa66-d01d0a8bb19a"
        },
        {
            "id": "674f0369-2b22-48d2-971f-356233e7970c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "e58af6ce-7a26-4f91-aa66-d01d0a8bb19a"
        },
        {
            "id": "f41a78b8-57e3-4010-bec9-4667c5024549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e58af6ce-7a26-4f91-aa66-d01d0a8bb19a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}