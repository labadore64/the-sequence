{
    "id": "93302266-d228-45ef-a613-9ca8763dd002",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldControlsOption",
    "eventList": [
        {
            "id": "7cc4ef47-cad0-4438-9d83-cdb3a037ed2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93302266-d228-45ef-a613-9ca8763dd002"
        },
        {
            "id": "64de0435-7b58-4833-9e48-410049929a42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "93302266-d228-45ef-a613-9ca8763dd002"
        },
        {
            "id": "d501cc9c-a72d-424b-87a2-35e427b08f80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "93302266-d228-45ef-a613-9ca8763dd002"
        },
        {
            "id": "ea6fbbcc-1afe-4f61-9228-3d7b74b2bea6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93302266-d228-45ef-a613-9ca8763dd002"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}