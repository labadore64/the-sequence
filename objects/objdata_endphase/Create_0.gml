/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\battle\\endphase\\end";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	
	name[itemcount] = ini_read_string_length("data","name","a",IMPORT_MAX_SIZE_ENDPHASENAME);
	script[itemcount] =  asset_get_index("end_phase_"+ini_read_string_length("data","script","",IMPORT_MAX_SIZE_SCRIPT));
	condition_script[itemcount] =  asset_get_index("end_phase_cond_"+ini_read_string_length("data","condition_script","",IMPORT_MAX_SIZE_SCRIPT));
	
	sprite_animation[itemcount] = asset_get_index("spr_battle_" + ini_read_string_length("data","sprite_animation","",IMPORT_MAX_SIZE_SPRITE)); //animation on user
	bg_animation[itemcount] = asset_get_index("spr_move_bg_" +ini_read_string_length("data","bg_animation","",IMPORT_MAX_SIZE_SPRITE)); //animation for background
	fg_animation[itemcount] = asset_get_index("spr_move_fg_" + ini_read_string_length("data","fg_animation","",IMPORT_MAX_SIZE_SPRITE)); //aniation on foreground

	animation_length[itemcount] = ini_read_real("data","animation_length",1);
	
	target[itemcount] = stringToBool(ini_read_string_length("data","target","false",IMPORT_MAX_BOOLEAN));
	pause[itemcount] = stringToBool(ini_read_string_length("data","pause","false",IMPORT_MAX_BOOLEAN));
	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;
