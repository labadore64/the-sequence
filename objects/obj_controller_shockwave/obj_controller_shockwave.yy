{
    "id": "1348417a-750c-4d46-a42a-3a9e04648df4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_shockwave",
    "eventList": [
        {
            "id": "975b4575-0d4a-402b-ad11-cf89eb3c6b14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        },
        {
            "id": "8ec77fb2-26e7-4bfb-98a0-9ba42c9f165f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        },
        {
            "id": "cad55c6b-d03b-4b6c-8481-d1a8fce7b87a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        },
        {
            "id": "304f0e97-007c-4ee5-94b1-e1a5d22dfcf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        },
        {
            "id": "a9c9337a-88cc-4d3e-bd8d-ec2dd33d9dca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        },
        {
            "id": "674dc279-617f-4041-96d8-31522de94eff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        },
        {
            "id": "de016a5e-586c-4237-b870-347d8be111b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 10,
            "m_owner": "1348417a-750c-4d46-a42a-3a9e04648df4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}