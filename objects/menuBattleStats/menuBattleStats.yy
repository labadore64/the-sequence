{
    "id": "b8ea5534-8a45-412a-a1b8-705fa10a48b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleStats",
    "eventList": [
        {
            "id": "e62aa7ad-00b8-4b7b-8364-09c9d227f2b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8ea5534-8a45-412a-a1b8-705fa10a48b7"
        },
        {
            "id": "7cb83382-45b8-4d27-aa80-1bd68cf69a62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b8ea5534-8a45-412a-a1b8-705fa10a48b7"
        },
        {
            "id": "5173e2be-9b4b-4146-b26b-637f3c9922bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "b8ea5534-8a45-412a-a1b8-705fa10a48b7"
        },
        {
            "id": "5ce314ee-c81a-4f50-87b6-434d1e7f9bc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8ea5534-8a45-412a-a1b8-705fa10a48b7"
        },
        {
            "id": "7a79f65b-8d89-44e4-b35d-a4b418a96068",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "b8ea5534-8a45-412a-a1b8-705fa10a48b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}