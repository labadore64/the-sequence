/// @description Insert description here
// You can write your code in this editor
var obj = party[|menupos];

chaa = obj;

with(dataDrawStat){
drawHexStatSetBoosts(obj.phy_power.braille_stat,obj.phy_guard.braille_stat,obj.mag_power.braille_stat,obj.mag_guard.braille_stat,
							obj.spe_power.braille_stat,obj.spe_guard.braille_stat)
}

menuBattleStatsUpdateStats(obj,min(obj.phy_power.base,obj.phy_power.braille_stat),
								min(obj.phy_guard.base,obj.phy_guard.braille_stat),
								min(obj.mag_power.base,obj.mag_power.braille_stat),
								min(obj.mag_guard.base,obj.mag_guard.braille_stat),
								min(obj.spe_power.base,obj.spe_power.braille_stat),
								min(obj.spe_guard.base,obj.spe_guard.braille_stat));
								
element_sprite = menuBattleMovesGetElementSprite(obj.element)