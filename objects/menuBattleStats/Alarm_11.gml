/// @description Insert description here
// You can write your code in this editor
tts_title_string = menu_name_array[menupos] + " " + title
hudPopulateBattleStatsDetails("character",party[|menupos])
if(current_status > 0){
	hudPopulateBattleStatus("status",current_status);
}
hudControllerAddData("inbattle","");
// Inherit the parent event
event_inherited();


with(menuControl){
	please_draw_all_visible = true;	
}

with(MouseHandler){
	active = true;	
}

mouseHandler_clear();
mouseHandler_add(0,0,800,600,menu_left,"")