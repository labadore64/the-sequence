{
    "id": "379a1b08-e287-43a8-b15c-be7159b6910b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "pushArea",
    "eventList": [
        {
            "id": "37c2042a-2d52-4132-b23d-c81619aa5c44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "379a1b08-e287-43a8-b15c-be7159b6910b"
        },
        {
            "id": "e4e22118-2115-4bcc-acc3-cbd058ec1d9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "379a1b08-e287-43a8-b15c-be7159b6910b"
        },
        {
            "id": "1e76878e-bc6e-4d4c-868d-d70cfd595e33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "379a1b08-e287-43a8-b15c-be7159b6910b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f643c4f4-3b8e-4b62-8fe0-78dc4855e219",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7262d81-862f-402b-9050-48faa6e998cc",
    "visible": true
}