/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var scalingx = draw_scalex;
var scalingy = draw_scaley

image_xscale = scalingx;
image_yscale = scalingy;

image_xscale = image_xscale * (1/zoom_x);
image_yscale = image_yscale * (1/zoom_y);