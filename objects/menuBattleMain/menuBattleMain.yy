{
    "id": "6cf86ce4-efa8-4ecc-8598-0efecd017f7b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleMain",
    "eventList": [
        {
            "id": "2f21e48f-864b-42fc-9d3d-a727159190ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cf86ce4-efa8-4ecc-8598-0efecd017f7b"
        },
        {
            "id": "6c363d16-86e4-40f1-92ed-009abd7dac0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "6cf86ce4-efa8-4ecc-8598-0efecd017f7b"
        },
        {
            "id": "3fc00cab-042c-44fd-bdd9-09dfe547633c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cf86ce4-efa8-4ecc-8598-0efecd017f7b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}