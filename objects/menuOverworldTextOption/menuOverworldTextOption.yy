{
    "id": "a99d4b16-92bf-4cb8-b8c4-618f15958c25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldTextOption",
    "eventList": [
        {
            "id": "98071c5c-d029-4789-a1c8-ffc701b78bec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a99d4b16-92bf-4cb8-b8c4-618f15958c25"
        },
        {
            "id": "20dc9207-2109-4a0e-9bb1-42793cc7b65d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a99d4b16-92bf-4cb8-b8c4-618f15958c25"
        },
        {
            "id": "c256c921-9831-48e0-8feb-362de8de20ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "a99d4b16-92bf-4cb8-b8c4-618f15958c25"
        },
        {
            "id": "45597c8b-0561-4d8b-be60-eaf67b9ec869",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a99d4b16-92bf-4cb8-b8c4-618f15958c25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}