{
    "id": "5b11fd41-7b75-471b-9468-49202b9a7266",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuStats",
    "eventList": [
        {
            "id": "7c83ed93-8e0c-42ea-b7a5-1ac8219abde4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b11fd41-7b75-471b-9468-49202b9a7266"
        },
        {
            "id": "cf2fae1b-b2b3-4dee-b112-2b18616330d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5b11fd41-7b75-471b-9468-49202b9a7266"
        },
        {
            "id": "8e860531-e322-444c-b4b8-1893701aa261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b11fd41-7b75-471b-9468-49202b9a7266"
        },
        {
            "id": "acea5aeb-c46d-4494-aa29-00cd2bffec40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "5b11fd41-7b75-471b-9468-49202b9a7266"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}