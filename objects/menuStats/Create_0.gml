/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// menu options

x= 400;
y = 325+40;

menu_background_color = c_black
gradient_height = 0; //height of the title
menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430+80)
option_y_offset = 85
title_y_offset = 0
option_x_offset = 20;

menuParentUpdateBoxDimension()

menu_draw = menuStatsDraw

menuParentSetDrawTitle(false);
menuParentSetDrawSubtitle(false);

draw_bg = false;

// options
menu_select = menuStatsRead;
menuAddOption("Steps Walked","",-1)
menuAddOption("Total Battles","",-1)
menuAddOption("Battles Won","",-1)
menuAddOption("Battles Lost","",-1)
menuAddOption("Battles Fled","",-1)

menu_size = menuGetSize();

stat_list = ds_list_create();

menu_up = menuStatsMoveUp;
menu_down = menuStatsMoveDown;

menu_left = menuStatsMoveLeft;
menu_right = menuStatsMoveRight;

wrap = false;

ds_list_add(stat_list,
			getGameStat(GAMESTAT_STEPS),
			getGameStat(GAMESTAT_BATTLE_TOTAL),
			getGameStat(GAMESTAT_WIN),
			getGameStat(GAMESTAT_LOSE),
			getGameStat(GAMESTAT_FLEE)
)

menu_help = "phonestats"

ignore_tab = true