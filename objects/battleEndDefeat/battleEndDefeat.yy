{
    "id": "77338ec2-f018-4977-aa71-cf68596ddfa5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleEndDefeat",
    "eventList": [
        {
            "id": "56c2b4ee-35a3-406b-8d77-5cf81b98f120",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "77338ec2-f018-4977-aa71-cf68596ddfa5"
        },
        {
            "id": "673a8705-6250-48d7-8514-aec81b469fc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77338ec2-f018-4977-aa71-cf68596ddfa5"
        },
        {
            "id": "b16d31e6-f8e7-4645-9c7a-f2d2fc80b86d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "77338ec2-f018-4977-aa71-cf68596ddfa5"
        },
        {
            "id": "55509e3c-8cf7-4e62-97e7-8b9eefedb9e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "77338ec2-f018-4977-aa71-cf68596ddfa5"
        },
        {
            "id": "556a50f8-82e7-41cf-854d-a16cc8b33684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77338ec2-f018-4977-aa71-cf68596ddfa5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}