{
    "id": "b0ea70d2-d9e2-4417-8483-c36a3f1713d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBraillePracticeMain",
    "eventList": [
        {
            "id": "121f90db-56f5-4460-8bd4-4cf3c3a120bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0ea70d2-d9e2-4417-8483-c36a3f1713d9"
        },
        {
            "id": "dcd09c8d-ac7c-4a91-bc35-662464e7530f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b0ea70d2-d9e2-4417-8483-c36a3f1713d9"
        },
        {
            "id": "4a7eb1bb-78a7-4f86-9754-ff11da07d25f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b0ea70d2-d9e2-4417-8483-c36a3f1713d9"
        },
        {
            "id": "fb7c8d33-a425-46b9-9824-fccafa2456b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "b0ea70d2-d9e2-4417-8483-c36a3f1713d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}