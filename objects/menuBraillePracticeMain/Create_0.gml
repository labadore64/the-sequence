/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(object_index != menuBrailleTabletsMain){
	with(menuBrailleTabletsMain){
		visible = false	
	}
}

menuParentSetTitle(global.langini[2,5])
menuParentSetSubtitle(global.langini[3,5])

menuAddOption(global.langini[0,8],global.langini[1,8],menuBraillePracticeScript) // review
menuAddOption(global.langini[0,9],global.langini[1,9],menuBraillePracticeChallengeScript)
menuAddOption(global.langini[0,10],global.langini[1,10],menuBraillePracticeOptionsScript)
SHORTHAND_CANCEL_OPTION

menu_size = menuGetSize();

menu_help="braillegame1"

animation_start = 1 //animation length
animation_len = animation_start; //animation counter

draw_gradient = false;

sprite_counter = 0;

spr_img[0] = spr_mode_review;
spr_img[1] = spr_mode_challenge;
spr_img[2] = -1;
spr_img[3] = -1;

draw_bg = false; //whether to draw transparent bg

background_obj = noone;

if(!instance_exists(brailleGameBackground)){
	background_obj = instance_create(0,0,brailleGameBackground);
}

x = 400;
y = 250;

draw_width = 600; //width of box in pixels
draw_height = 300; //height of box in pixels
title_x_offset = 300; //xoffset for title
title_y_offset = 10; //yoffset for title
subtitle_x_offset = 300; //xoffset for title
subtitle_y_offset = 60; //yoffset for subtitle

option_y_offset = 120; //yoffset for options
option_x_offset = 50; //yoffset for options

select_percent = .33; //how much of the menu does the selection graphic occupy

menu_draw = menuBraillePracticeMainDraw;

menuParentUpdateBoxDimension();

tts_title_string = title

with(GameLoadHandler){
	visible = false;	
}

image_speed = 1.5

audio_group_load(audiogroup_braille)
drawOtherMenus = false
mousepos_x = -210