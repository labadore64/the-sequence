{
    "id": "70354392-61cd-4812-8828-da68c5303ddd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuParent",
    "eventList": [
        {
            "id": "43b461b1-bacd-42a2-9027-dedfe59124eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "5a09f081-38bf-46df-a7ba-ad726af89b1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "b7997f82-7b1f-48d5-8b93-16f63f893874",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "0a40d93a-22d9-441d-844d-5a5cfeba2f04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "2c7d2d14-ab3a-413e-bd8d-544ecb5c9cba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "62f8ff4c-9ee4-43a1-95b7-23699f6122f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "0f9ab7b1-2860-4a12-b968-4d2e50f252d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        },
        {
            "id": "22649384-de6c-404e-97ee-a2335840b7ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "70354392-61cd-4812-8828-da68c5303ddd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}