{
    "id": "13d1ed20-ff7a-4631-9979-0a0021651ea3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_bird_robin",
    "eventList": [
        {
            "id": "b162cecc-c734-4cba-b770-6a915e2bd114",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13d1ed20-ff7a-4631-9979-0a0021651ea3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9a64ba54-e366-4ef4-a5c7-b75eda473dbe",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93bb999a-5364-4d45-a46d-37fed305fd03",
    "visible": true
}