/// @description Insert description here
// You can write your code in this editor
menuParentClearOptions();

var list = BattleHandler.battle_history_text;

var sizer = ds_list_size(list);

for(var i = 0; i < sizer; i++){
	menuAddOption(list[|i],"",-1)
}
menu_size = menuGetSize();
menuParentSetTitle("Battle History");

with(menuControl){
	please_draw_all_visible = true;	
}