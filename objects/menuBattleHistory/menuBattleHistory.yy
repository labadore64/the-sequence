{
    "id": "04c9c5a6-e1cb-4424-847f-db9bcc3d18f0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuBattleHistory",
    "eventList": [
        {
            "id": "d92886ac-0747-45bb-ad05-5d67efcee578",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04c9c5a6-e1cb-4424-847f-db9bcc3d18f0"
        },
        {
            "id": "d3f38f36-8d58-490a-a9e9-b04444ff0551",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "04c9c5a6-e1cb-4424-847f-db9bcc3d18f0"
        },
        {
            "id": "ae2073e3-4894-4b71-8f20-10a5e9233836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04c9c5a6-e1cb-4424-847f-db9bcc3d18f0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}