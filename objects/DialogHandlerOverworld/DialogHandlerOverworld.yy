{
    "id": "2f61bf44-f1d8-422e-b24e-98afbaac2a6c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DialogHandlerOverworld",
    "eventList": [
        {
            "id": "3650b4fe-9c05-48a7-b971-71d847f2167e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f61bf44-f1d8-422e-b24e-98afbaac2a6c"
        },
        {
            "id": "c2588fdd-20e9-4df2-bbca-95a3a5dcc27b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2f61bf44-f1d8-422e-b24e-98afbaac2a6c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "02e46696-1196-432d-b96a-6773c46a9b54",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}