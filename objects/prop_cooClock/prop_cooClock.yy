{
    "id": "396756ca-8496-4fc2-949c-266c2308277b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_cooClock",
    "eventList": [
        {
            "id": "3f8664f6-b61e-494a-bdf8-b3666b2f55aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "396756ca-8496-4fc2-949c-266c2308277b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c2681f00-682a-4f09-ad8b-a04b3fab963c",
    "visible": true
}