{
    "id": "cdc6a2ef-1217-446b-8c70-d7193286a12d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleGameTimeAttack",
    "eventList": [
        {
            "id": "f8c23642-cb9e-4cde-bcc0-3d593a5c606d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cdc6a2ef-1217-446b-8c70-d7193286a12d"
        },
        {
            "id": "18443eaa-b9e9-46e3-b92e-3d39c235a69c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "cdc6a2ef-1217-446b-8c70-d7193286a12d"
        },
        {
            "id": "e4b27760-06c2-4683-bcdb-eff5a6d1adbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cdc6a2ef-1217-446b-8c70-d7193286a12d"
        },
        {
            "id": "99d15d66-692b-4fee-b0bd-35b46c2bc250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "cdc6a2ef-1217-446b-8c70-d7193286a12d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}