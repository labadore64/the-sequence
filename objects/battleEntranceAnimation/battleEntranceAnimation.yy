{
    "id": "7fd2bb6c-9b62-4844-a1d1-07e13a98b75d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleEntranceAnimation",
    "eventList": [
        {
            "id": "727ae223-ad93-4071-9bcc-e509bb4c55ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7fd2bb6c-9b62-4844-a1d1-07e13a98b75d"
        },
        {
            "id": "4d2c9d85-e530-41ad-a1dd-d828f98b5d3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7fd2bb6c-9b62-4844-a1d1-07e13a98b75d"
        },
        {
            "id": "ccc16b27-696a-4fab-880e-96778f99c0e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7fd2bb6c-9b62-4844-a1d1-07e13a98b75d"
        },
        {
            "id": "130711ea-b3e2-483c-aa61-23f14e61d1f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7fd2bb6c-9b62-4844-a1d1-07e13a98b75d"
        },
        {
            "id": "7cba3fed-6351-4923-98e7-103527bb6135",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7fd2bb6c-9b62-4844-a1d1-07e13a98b75d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}