/// @description Insert description here
// You can write your code in this editor

if(!surface_exists(surf)){
	surf = surface_create_access(global.window_width,global.window_height);
} else {
	if(ScaleManager.updated){
		surface_resize(surf,global.window_width,global.window_height);
	}
}

if(draw_this_frame){
	surface_set_target(surf);

	//draw thing
	draw_surface(application_surface,0,0);

	surface_reset_target();	
	draw_this_frame = false;
}
			if(!global.disableShader){
                if(radi_shader_enabled){
                    shader_set(shd_radial_blur)
                    
                    shader_set_uniform_f(radi_uni_time, radi_var_time_var);
                    shader_set_uniform_f(radi_uni_mouse_pos, radi_var_mouse_pos_x, radi_var_mouse_pos_y);
                    shader_set_uniform_f(radi_uni_resolution, radi_var_resolution_x, radi_var_resolution_y);
                    shader_set_uniform_f(radi_uni_radial_blur_offset,radi_var_radial_blur_offset);
                    shader_set_uniform_f(radi_uni_radial_brightness,radi_var_radial_brightness);
                    

                    draw_surface(surf,0,0)
                    //draw_surface(application_surface,0,0)
                    shader_reset()
                    
                }
			} else {
				draw_surface(surf,0,0)
			}
				draw_letterbox();