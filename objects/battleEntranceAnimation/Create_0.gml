/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
battle_choose_enemies();

battleEntranceAnimationInit();
depth = -15000
with(Player){
	lock = true;	
}

with(menuControl){
	visible = false;	
}

surf = -1;
time = 90;

draw_this_frame = true;

fadeIn(time,c_white)

fade_out_script = -1;
fade_in_script = -1;

newalarm[0] = time;

soundfxPlay(soundBattleStart)

soundPlaySong(-1,30)

with(AudioEmitter){
	if(audio_sound > -1){
		audio_pause_sound(audio_sound);	
	}
}