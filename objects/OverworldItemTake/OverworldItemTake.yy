{
    "id": "4188e8a4-1ddc-4c51-980d-289ae81a5f5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "OverworldItemTake",
    "eventList": [
        {
            "id": "e78be18c-0542-472c-afb1-74689403a60b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4188e8a4-1ddc-4c51-980d-289ae81a5f5e"
        },
        {
            "id": "8f802fb4-f688-4b84-b492-269f4b58d5fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4188e8a4-1ddc-4c51-980d-289ae81a5f5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "382b61d9-b7ef-464b-9788-5f1bea9842a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6480ebb6-837a-4179-9c6e-93f68c821409",
    "visible": false
}