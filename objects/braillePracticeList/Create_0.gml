/// @description Insert description here
// You can write your code in this editor
event_inherited();
title = "Braille"
subtitle = "Select a character to practice."

page_size = 32;

page_start = 0;

page_num = 0;
page_max = 0;

page_increment_start = 0;

menu_draw = braillePracticeDraw;

braille_file = 0;
braille_total_charset = objdata_tablet.braille_types_size;
braille_current_file = objdata_tablet.brailletype_file[braille_file]
braille_current_charset = objdata_tablet.brailletype_title[braille_file] 

braille_data = instance_create(0,0,brailleData);

braille_data.braille_source = braille_current_file;
braille_data.read_desc = true
braille_data.newalarm[0] = TIMER_INACTIVE
with(braille_data){
	event_perform(ev_alarm,0);	
}

for(var i = 0; i < braille_total_charset; i++){
	braille_menupos[i] = 0;	
}

braille_keys = ds_list_create();

menu_up = braillePracticeMoveUp
menu_down = braillePracticeMoveDown
menu_left = braillePracticeMoveLeft
menu_right = braillePracticeMoveRight

menu_tableft = menuPracticeTabRight
menu_tabright = menuPracticeTabLeft

menu_select = menuPracticeSubScript

start_x = 40

newalarm[11] = 2;
braille_columns = objdata_tablet.brailletype_columns[braille_file]

drawOtherMenus = false

menu_use_generic = true;