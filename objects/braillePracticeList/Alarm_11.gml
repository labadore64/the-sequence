/// @description Insert description here
// You can write your code in this editor

// PREPARE THE LIST

tts_title_string = title + " " + subtitle;

menuParentClearOptions();
ds_list_clear(braille_keys)

var size, key, i;
size = ds_map_size(braille_data.braille_map);
key = ds_map_find_first(braille_data.braille_map);
for (i = 0; i < size; i++;)
{
	if(!is_undefined(key)){
		ds_list_add(braille_keys,key);	
	}
	key = ds_map_find_next(braille_data.braille_map, key);
}

ds_list_sort(braille_keys,true)

// end preparing list

for(var i = 0; i < size; i++){
	menuAddOption(braille_keys[|i],"",select_script)
}

menu_size = menuGetSize();

page_max = floor(menu_size/page_size)

var counter = 0;

var maxpage = floor((menu_size+1)/8)

space_x = 720/braille_columns;
skip = braille_columns;


for(var i = 0; i < braille_rows+maxpage; i++){
	for(var j = 0; j < braille_columns; j++){
		if(counter < size){
			display_cell[i,j] = instance_create(
							start_x + space_x*j + space_x*.3,
							start_y + space_y*(i%braille_rows),
							brailleCharacter);
			display_cell[i,j].cell_trans_time =0;
			active_cell[counter] = true;	
			brailleDataSetCharacterSpecific(braille_keys[|counter],display_cell[i,j],braille_data.braille_map);	
		} else {
			display_cell[i,j] = noone	
		}
		counter++;
	}
}

drawOtherMenus = false

braillePracticeUpdateCell()

active = true;

with(menuControl){
	please_draw_black = false;	
}

if(menu_ax_HUD != ""){
	infokeys_assign(menu_ax_HUD);
}

tts_clear();
if(tts_title_string != ""){
	tts_say(tts_title_string)	
}

mouseHandler_clear()
// do braille columns
var spacee_x = space_x
var spacee_y = space_y

for(var i = 0; i < braille_rows; i++){
	for(var j = 0; j < braille_columns; j++){
			//draw the boundary box

			var x_pos = start_x + space_x*j
			var y_pos = start_y + space_y*(i-page_increment_start)

			if(instance_exists(display_cell[i,j])){
				mouseHandler_add(x_pos,y_pos,x_pos+space_x,y_pos+space_y,
							menuPracticeSubScript, display_cell[i,j].cell_character, true);
			}
	}
}

mouseHandler_add(100-50,560-20,100+50,560+20,
			menuPracticeTabLeft,"Previous Set");
mouseHandler_add(700-50,560-20,700+50,560+20,
			menuPracticeTabRight,"Next Set");

menu_update_values = braillePracticeUpdateCell

mouseHandler_menu_setCursor();