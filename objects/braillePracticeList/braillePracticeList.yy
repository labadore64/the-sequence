{
    "id": "d7004b7b-0c00-441f-960c-93b8cc294fac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "braillePracticeList",
    "eventList": [
        {
            "id": "62dce4c4-ce02-4c72-b60f-aa824f4dc0e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7004b7b-0c00-441f-960c-93b8cc294fac"
        },
        {
            "id": "cb144028-6513-4a7d-a716-f2fc8fd74b06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "d7004b7b-0c00-441f-960c-93b8cc294fac"
        },
        {
            "id": "10b3d23d-bc26-4104-b3cf-a59ccda434c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d7004b7b-0c00-441f-960c-93b8cc294fac"
        },
        {
            "id": "f077ea3c-e319-479e-bbc5-ec48b431bfa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d7004b7b-0c00-441f-960c-93b8cc294fac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e5c3aba0-03f8-472a-ac97-e0c1f54d07e5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}