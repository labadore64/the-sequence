/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

with(braille_data){
	instance_destroy();	
}

if(ds_exists(braille_keys,ds_type_list)){
	ds_list_destroy(braille_keys)	
}