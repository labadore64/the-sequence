{
    "id": "13c5df1b-f8ce-403a-89fb-7aa5d5c63755",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller_emboss",
    "eventList": [
        {
            "id": "9318a9af-1448-4db4-826a-d7a89e676b95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13c5df1b-f8ce-403a-89fb-7aa5d5c63755"
        },
        {
            "id": "28709783-976c-4e85-bfd8-f92644919aa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "13c5df1b-f8ce-403a-89fb-7aa5d5c63755"
        },
        {
            "id": "1eb4b2f9-2f89-4b5c-a9c8-0298e79308eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "13c5df1b-f8ce-403a-89fb-7aa5d5c63755"
        },
        {
            "id": "49815593-ff56-4bba-941f-a821843b213d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "13c5df1b-f8ce-403a-89fb-7aa5d5c63755"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}