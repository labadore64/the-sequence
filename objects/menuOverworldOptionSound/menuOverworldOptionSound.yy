{
    "id": "d499ebb1-8632-489c-b417-c9fe1d4c6e6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuOverworldOptionSound",
    "eventList": [
        {
            "id": "764a467f-0cda-4b9b-87d2-d5ca19dbe84e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d499ebb1-8632-489c-b417-c9fe1d4c6e6d"
        },
        {
            "id": "78593b63-3853-4cb6-a60d-56ee421b2000",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d499ebb1-8632-489c-b417-c9fe1d4c6e6d"
        },
        {
            "id": "5bf5045a-9622-480a-81fd-ac778dc28940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "d499ebb1-8632-489c-b417-c9fe1d4c6e6d"
        },
        {
            "id": "6a442314-8b84-4028-beca-1ec64e3c8309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d499ebb1-8632-489c-b417-c9fe1d4c6e6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b4b05c5-27c9-4ed4-8f64-5ee62757b1ba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}