{
    "id": "8cc40811-c432-42ce-9586-baab4b55a7ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterShop",
    "eventList": [
        {
            "id": "b72432f5-df76-4787-99d4-4043956f31de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8cc40811-c432-42ce-9586-baab4b55a7ee"
        },
        {
            "id": "4668a176-ddbf-463b-b8ec-ec713aa8d2c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "8cc40811-c432-42ce-9586-baab4b55a7ee"
        },
        {
            "id": "ccab9566-fd46-4fad-93fa-e9f6e7333597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8cc40811-c432-42ce-9586-baab4b55a7ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "35653b71-cb48-431a-a97c-4f6608da2d9b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}