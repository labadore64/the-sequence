/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_list_destroy(item_list);

with(menuCharacterSell){
	instance_destroy();	
}

with(menuCharacterBuy){
	instance_destroy();	
}

with(obj_dialogue){
	instance_destroy()	
}

with(drawPortrait){
	instance_destroy()	
}