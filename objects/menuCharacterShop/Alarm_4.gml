/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuAddOption("Buy","View and purchase items.",menuCharacterScriptBuy)
menuAddOption("Sell","View and sell your inventory.",menuCharacterScriptSell)
if(can_talk){
	menuAddOption("Talk","Talk to " + character_name + ".",menuOverworldPartySubDialogScript)
}
menuAddOption("Leave","Leave the shop.",menu_cancel)

if(!item_list_updated){
	if(chara.buy_item_script != -1){
		script_execute(chara.buy_item_script,item_list);
		item_list_updated = true;
	}
}

menu_size = menuGetSize();