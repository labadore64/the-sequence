{
    "id": "4762d54c-3202-4e00-af22-43898b03eb18",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuTransformationPoint",
    "eventList": [
        {
            "id": "1abd6641-19f1-48f3-a096-01c53f511f0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        },
        {
            "id": "55d00ec7-efa9-46a0-9cdd-95fece46ead0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        },
        {
            "id": "8f59d203-fa81-4a82-b23e-a309291f77ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        },
        {
            "id": "49f84056-d949-4d1b-a107-d375f5dfd35d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        },
        {
            "id": "fba51620-1b1b-4a50-8d32-015559369ef3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        },
        {
            "id": "859e13d0-0ddb-4ade-a53c-3989da500cce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        },
        {
            "id": "e9a4186b-cfa2-46fd-9040-24ace2cf3696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4762d54c-3202-4e00-af22-43898b03eb18"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02987c91-65d1-4df7-aa79-5429b8900874",
    "visible": true
}