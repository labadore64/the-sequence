/// @description Insert description here
// You can write your code in this editor
if(instance_exists(parent_menu)){
if(parent_menu != noone && parent_menu.active){
	if(canTransform){
		with(menuTransformationPoint){
			canTransform = false;
			image_blend = c_white;
			x_delta = 0;
			y_delta = 0;
		}
		canTransform = true;
		image_blend = c_lime;
	
		x_delta = mouse_x - mouse_last_x;
		y_delta = mouse_y - mouse_last_y;
	
		x+=x_delta;
		y+=y_delta;
	
	}
}
}