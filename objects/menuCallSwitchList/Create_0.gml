/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

// menu options

x= 400;
y = 325+50+55

menuParentSetBoxWidth(455)
menuParentSetBoxHeight(430+60)
option_y_offset = 85
title_y_offset = 0
option_x_offset = 20;
select_percent = 1
animation_len = 1
option_space = 120
text_size = 3;

menuParentUpdateBoxDimension()

ds_list_clear(call_list)
menuParentClearOptions();

var call_size = ds_list_size(obj_data.characters); 
for(var i = 0; i < call_size; i++){
	if(obj_data.characters[|i].recruited){
		if(ds_list_find_index(obj_data.party,obj_data.characters[|i]) == -1){
			ds_list_add(call_list,obj_data.characters[|i]);	
		}
	}
}

if(ds_list_empty(call_list)){
	menuAddOption("No friends :(","",-1)
} else {
	var sizer = ds_list_size(call_list);
	var obj;
	for(var i = 0; i < sizer; i++){
		obj = call_list[|i];
		if(!is_undefined(obj)){
			menuAddOption(obj.name,"",menuCallSwitchScriptSub)
		}
	}
}

menu_draw = menuCallSwitchDraw

menu_size = menuGetSize();

menuCallUpdateDesc();

menu_help = "phonecall"

ignore_tab = true