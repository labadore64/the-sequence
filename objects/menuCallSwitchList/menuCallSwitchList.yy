{
    "id": "475c983b-4364-4ad4-ace7-3085e371d862",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCallSwitchList",
    "eventList": [
        {
            "id": "1f54b4be-7fc3-48f0-b50c-4567b8447f11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "475c983b-4364-4ad4-ace7-3085e371d862"
        },
        {
            "id": "bd9b1300-c1dc-4081-8676-52b3a3d28b0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "475c983b-4364-4ad4-ace7-3085e371d862"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8bc959fb-2b84-469e-857b-795e8e2a5bed",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}