{
    "id": "f6dbb61e-aba0-4b7f-8289-cd83dffc5bcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleDisplayBattle",
    "eventList": [
        {
            "id": "acb80ece-ee87-4a27-8b7c-b2b1fa43ef85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6dbb61e-aba0-4b7f-8289-cd83dffc5bcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "19943076-b550-4d21-9f00-14427af681b4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}