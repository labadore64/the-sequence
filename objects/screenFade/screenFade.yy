{
    "id": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "screenFade",
    "eventList": [
        {
            "id": "f5b0a515-dc06-485e-83fd-be402b4e9a5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        },
        {
            "id": "1ab1f319-0a8a-47a4-8828-b6a77c722cf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        },
        {
            "id": "500245c7-8fb9-4b92-9bf4-4225aa0bb338",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        },
        {
            "id": "8e7ed437-f695-42d9-8b88-f768d6132288",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        },
        {
            "id": "d9e589ff-e3be-41f2-91a3-89ac221dc358",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        },
        {
            "id": "8f457754-2eff-4362-bc9a-c83bd321742a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        },
        {
            "id": "3657db9e-983f-461f-bae6-68e4e61cf19b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "303e6610-3b0b-4ad6-8f1e-09cdb23bd99e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}