/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
if(enter_title_counter > 0){
	enter_title_counter-=menuControl.timer_diff;
	enter_title_x += enter_title_speed*menuControl.timer_diff;
} else {
	if(enter_title_speed >= .5){
		enter_title_speed = enter_title_speed*enter_title_friction*menuControl.timer_diff;;
		enter_title_x += enter_title_speed;
	}
}

if(draw_counter > 0){
	draw_alpha = 1-(draw_counter/draw_time)
	draw_counter-=menuControl.timer_diff;	
	if(draw_counter <= 0){
		draw_alpha = 1	
	}
}

if(ScaleManager.updated){
	if(surface_exists(char_surface)){
		surface_resize(char_surface,800*global.scale_factor,300*global.scale_factor);
	} else {
		char_surface = -1;	
	}
}