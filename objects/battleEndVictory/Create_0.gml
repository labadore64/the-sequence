/// @description Insert description here
// You can write your code in this editor
initTimingAlarm()
for(var i = 0; i < 3; i++){
	old_moves_character[i] = ds_list_create();
	new_moves_character[i] = ds_list_create();
	learned_moves_character[i] = ds_list_create();
}

var allies = BattleHandler.allies;
var ally = -1;

for(var i = 0; i < 3; i++){
	ally = allies[|i];
	
	if(!is_undefined(ally)){
		if(instance_exists(ally)){
			battleEndVictoryAddMovesToList(ally.moves,old_moves_character[i]);	
		}
	}
}

battleTextDisplayClearText();
var timer = 30
active = false;

fadeIn(timer,c_black)
newalarm[0] = timer;
newalarm[1] = 1
depth = -1500000

fade_in_script = -1;
fade_out_script = -1;

enter_title_time = 9;
enter_title_counter = enter_title_time;

enter_title_speed = 10
enter_title_friction = .95;
enter_title_x = 0

//character stats
char_surface = -1

draw_char[0] = -1;
draw_char[1] = -1;
draw_char[2] = -1;

draw_time = 20;
draw_counter = draw_time;
draw_alpha = 0;

total_exp = 0;

windows = ds_queue_create();

with(menuBattleMain){
	instance_destroy();	
}

with(menuBattleFight){
	instance_destroy();	
}

do_windows = false;

soundPlaySong(music002,15)

hidebg = false;

incrementGameStat(GAMESTAT_WIN)