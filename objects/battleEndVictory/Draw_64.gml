/// @description Insert description here
// You can write your code in this editor
if(active || hidebg){
	draw_clear(c_black);	
}

if(!surface_exists(char_surface)){
	char_surface = surface_create_access(800*global.scale_factor,600*global.scale_factor)
}


	var x_spacer = 250;
	var allpp = draw_alpha;
	surface_set_target(char_surface)
	draw_clear_alpha(c_white,0)
	for(var i = 0; i < 3; i++){
		if(draw_char[i] != -1){
			with(draw_char[i]){
				if(surface_exists(stats_surf)){
					draw_surface_ext(stats_surf,
									(0+x_spacer*i)*global.scale_factor,
									0,
									1,1,
									0,c_white,
									allpp)
				}
			}
		}
	}
	
	surface_reset_target();
	
	draw_set_color(global.textColor)
	draw_set_font(global.largeFont)
	draw_text_transformed_ratio(enter_title_x,50,"Victory!",3,3,0)
	draw_set_font(global.textFont)
	
	draw_surface(char_surface,
				global.display_x + (35*global.scale_factor),
				global.display_y + (160*global.scale_factor))