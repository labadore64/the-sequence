/// @description Insert description here
// You can write your code in this editor
if(surface_exists(char_surface)){
	surface_free(char_surface);	
}

for(var i = 0; i < 3; i++){
	if(ds_exists(old_moves_character[i],ds_type_list)){
		ds_list_destroy(old_moves_character[i])
		ds_list_destroy(new_moves_character[i])
		ds_list_destroy(learned_moves_character[i])
	}
}

if(ds_exists(windows,ds_type_list)){
	ds_queue_destroy(windows);	
}

battleKillAllStuff()

battleEndVictoryReturnToOverworld();