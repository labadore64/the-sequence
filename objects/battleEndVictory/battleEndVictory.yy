{
    "id": "4b3303f1-938c-4f79-9599-bf58db081edd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleEndVictory",
    "eventList": [
        {
            "id": "5e810c21-a40b-42fb-857b-c9bd01117547",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "48cad2b0-9f7d-41d1-a6b0-f8fdd265b54b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "89412416-bf20-4454-84d7-95ba4061aded",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "91be1106-65c2-4ddb-ab0c-87502e32af0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "759e3fcc-aa16-4c0e-8ff9-4171f6672b55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "5f818910-cfb7-4e4a-b2c5-55eeeddac06d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "9e8dd6e4-7389-49f8-9669-fbb44c61c90b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "a5c5e606-962d-4cc7-a37b-24bb0d161762",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "d829b45a-3ba1-4561-80ad-90e0de2f53a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        },
        {
            "id": "28c6b97f-5723-427c-a6e0-23d97ec5438b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "4b3303f1-938c-4f79-9599-bf58db081edd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}