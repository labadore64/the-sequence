/// @description Insert description here
// You can write your code in this editor
var list_of_fighters = ds_list_create();

with(Battler){
	if(load_id != -1){
		ds_list_add(list_of_fighters,id);
	}
}

ds_list_sort(list_of_fighters,true);

var obj = -1;

//get exp multiplier, if you have an ability.

var exp_multi = 1;

with(Battler){
	if(!is_enemy){
		if(ability == "double_exp"){
			exp_multi = 2;	
		}
	}
}

for(var i = 0; i < 3; i++){
	obj = list_of_fighters[|i];
	if(!is_undefined(obj)){
		if(battlerIsAlive(obj)){
			draw_char[i] = instance_create(0,0,battleEndVictoryStats);	
			draw_char[i].character = BattleHandler.draw_char[i]
			draw_char[i].exp_reward = floor(total_exp*exp_multi);
		}
	}
}

ds_list_destroy(list_of_fighters);

