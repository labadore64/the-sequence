/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

complete_script = brailleEliminationComplete;
destroy_script = brailleEliminationDestroy;

complete_animation_text = "Game Over!"

rounds = 3;
base_rounds = 3;

braillegame_id = 0;
