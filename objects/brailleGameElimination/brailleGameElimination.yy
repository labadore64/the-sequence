{
    "id": "3cad990e-1d50-4c49-a1d9-e82261299c22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleGameElimination",
    "eventList": [
        {
            "id": "a48d52c8-b473-4d4d-a82c-7bdbfe8ce7fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3cad990e-1d50-4c49-a1d9-e82261299c22"
        },
        {
            "id": "45384373-86d8-4eaa-9316-9882e0e0496b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3cad990e-1d50-4c49-a1d9-e82261299c22"
        },
        {
            "id": "846b13ca-1772-4fa8-8830-df6f3aaeaf40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3cad990e-1d50-4c49-a1d9-e82261299c22"
        },
        {
            "id": "f4d882bf-5840-40ab-a8fc-be618869320e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "3cad990e-1d50-4c49-a1d9-e82261299c22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6bbef715-837f-43c3-8b1e-3a2d0144ec7c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}