{
    "id": "76cbd938-1f3c-4b1a-97f4-3fdcbaf19221",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TextBoxChoice",
    "eventList": [
        {
            "id": "0b033269-c8fe-467f-b037-4cd78d756694",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76cbd938-1f3c-4b1a-97f4-3fdcbaf19221"
        },
        {
            "id": "e710d9dd-ee15-4a00-b94f-1a5ee26caf94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "76cbd938-1f3c-4b1a-97f4-3fdcbaf19221"
        },
        {
            "id": "6b90e693-2356-4936-997e-a7f5a2e0b890",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76cbd938-1f3c-4b1a-97f4-3fdcbaf19221"
        },
        {
            "id": "a2714a89-2998-458f-842f-0cda61ea943a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "76cbd938-1f3c-4b1a-97f4-3fdcbaf19221"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e7d09f6b-8edd-4bec-a3bb-2d38786193c5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}