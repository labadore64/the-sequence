/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var lockdown = false;

				with(display_object){
					if(cutoff == string_lenger){
						if(array_length_1d(message)-1 <= message_current){
							lockdown = true;
						}
					}
				}
				
if(lockdown){
	if(!triggered){
		//create textbox	
		child_menu = instance_create(0,0,menuTextboxChoice);
		child_menu.parent = id;
		
		child_menu.h_start = 800-choice_width;
		
		var list_choices = textbox_choices;
		var list_choices_scripts = textbox_choices_script;
		
		var test_script = -1;
		
		with(child_menu){
			var sizer = ds_list_size(list_choices);
			
			for(var i = 0; i < sizer; i++){
				test_script = list_choices_scripts[|i];
				if(is_undefined(test_script)){
					test_script = -1;	
				}
				
				menuAddOption(list_choices[|i],"",test_script)
			}
			
			menu_size = menuGetSize();
		}
		
		child_menu.menu_cancel = set_menu_cancel;
	}
	triggered = true;
	lock = true;	
}