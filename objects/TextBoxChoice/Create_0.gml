/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

complete = false;
child_menu = noone;

textbox_choices = ds_list_create();
textbox_choices_script = ds_list_create();
menu_select = textboxChoiceEnter;

triggered = false;

choice_width = 200;

set_menu_cancel = -1; //what to set the cancel function for the menu to.

active = false;

newalarm[2] = 1;