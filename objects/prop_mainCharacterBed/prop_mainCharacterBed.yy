{
    "id": "566984b6-0318-4688-af1a-0d98a9debb20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "prop_mainCharacterBed",
    "eventList": [
        {
            "id": "fdc054de-b07b-4fdd-8160-1104cef95b21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "566984b6-0318-4688-af1a-0d98a9debb20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "27e55429-e6ad-475a-a1e6-6ac91fe41c69",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bc7f78a-461a-44e9-be69-c920d35b7afa",
    "visible": true
}