/// @description Insert description here
// You can write your code in this editor
updateTimingAlarm()
// Inherit the parent event
event_inherited();

if(!instance_exists(BrailleQuitGame)  && !menuControl.help_open){
	if(active){
		if(ds_queue_empty(queue)){
			if(destroy_when_empty){
				instance_destroy();
			}
		} else {
			current_instance = instance_create(0,0,brailleInputCell)
			current_instance.menu_cancel = menu_cancel_child;
			current_instance.braille_data = braille_data
			current_instance.cell_character = ds_queue_dequeue(queue);
			current_instance.parent_queue = id;
		}
	}
}