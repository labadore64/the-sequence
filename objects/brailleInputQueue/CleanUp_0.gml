/// @description Insert description here
// You can write your code in this editor
event_inherited();

ds_queue_destroy(queue);

if(instance_exists(move_input)){
	
	if(instance_exists(move_input.current_attack)){
		var se = result_score;
	
		var dir = move_input.current_attack.braille_left;
		var strin = move_input.current_attack.braille_string_base

		with(move_input){
	
			current_attack.state = -1
			current_attack.braille_bonus = se;
			with(current_attack.character){
				var strinlen = string_length(strin);
		
				for(var i = 0; i < strinlen; i++){
					for(var j = 0; j < 3; j++){
						if(tablet_char[j] == string_lower(string_char_at(strin,i+1))){
							tablet_recover[j] = 1;	
						}
					}
				}
			}
			battleStartAttackConfirm();
		}

		var displa = BattleHandler.braille_display;
		with(displa){
			if(dir){
				brailleDisplayInsertLeftEnd(strin,displa)
			} else {
				brailleDisplayInsertRightEnd(strin,displa)
			}
		}

	}
}

with(brailleGameGeneric){
	newalarm[2] = 1	
}

with(current_instance){
	instance_destroy();	
}

with(battleStartAttack){
	lock = false;	
	current_attack.state = -1;
	battleStartAttackConfirm();
}