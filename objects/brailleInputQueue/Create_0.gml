/// @description Insert description here
// You can write your code in this editor
event_inherited();
queue = ds_queue_create();

destroy_when_empty = true;

input_string = "";

// disable all the normal menu stuff
menu_up = -1
menu_down = -1
menu_left = -1
menu_right = -1
menu_select = -1
menu_select_hold = -1
menu_cancel = -1

menu_cancel_child = -1;

menu_draw = brailleQueueDraw

current_instance = -1;

result_score = 0;

result_success = 0;

braille_attack = false;

newalarm[2] = 1;
newalarm[11] = 2;
newalarm[8] = 1;

braille_data = noone

move_input = noone;

parent_obj = noone

display_ready_message = false;

ignore_tab = true