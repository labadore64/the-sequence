{
    "id": "2864607c-c95a-4253-81bd-87f6f0de2ff1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "brailleInputQueue",
    "eventList": [
        {
            "id": "6f9622e4-c84e-4280-ae14-49eba256b4de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2864607c-c95a-4253-81bd-87f6f0de2ff1"
        },
        {
            "id": "6fc0d55c-121e-4a1b-8d38-f16b4457cd19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2864607c-c95a-4253-81bd-87f6f0de2ff1"
        },
        {
            "id": "12e559e2-beff-4680-ba2f-1e7e7dff954a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "2864607c-c95a-4253-81bd-87f6f0de2ff1"
        },
        {
            "id": "7433a008-6f17-44c4-ba26-9e74975ce83a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2864607c-c95a-4253-81bd-87f6f0de2ff1"
        },
        {
            "id": "ae42fa64-dc99-42f2-b864-84fde753db46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 2,
            "m_owner": "2864607c-c95a-4253-81bd-87f6f0de2ff1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70354392-61cd-4812-8828-da68c5303ddd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}