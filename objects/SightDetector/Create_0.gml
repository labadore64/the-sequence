/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
directional = AX_COLLISION_DIRECTION_UP;
orientation = 0;

base_orientation = 0;

current_dist = 0;  //distance from actual collision
last_dist = 0;

falloff_min = 1;
falloff_max = global.AXSightDistance

dist_x = 0;
dist_y = 0;

color = $FFFFFF

active = false;

looking_obj = noone;