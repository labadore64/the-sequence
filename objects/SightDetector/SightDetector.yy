{
    "id": "015527fb-0172-4a60-bda1-65a6d0a87b3a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "SightDetector",
    "eventList": [
        {
            "id": "6535f21f-ebe2-4306-aa35-6dc3a56e5912",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "015527fb-0172-4a60-bda1-65a6d0a87b3a"
        },
        {
            "id": "015878e3-ffc1-431c-b6f0-46d38802ea49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "015527fb-0172-4a60-bda1-65a6d0a87b3a"
        },
        {
            "id": "a0786651-ba0f-4171-a779-955f99e1c5ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "015527fb-0172-4a60-bda1-65a6d0a87b3a"
        },
        {
            "id": "9c7b44c1-20e2-4627-b515-2189b6d8f6d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "015527fb-0172-4a60-bda1-65a6d0a87b3a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5661bae4-c66d-4153-b178-3252518b3132",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}