/// @description Insert description here
// You can write yourcode in this editor
updateTimingAlarm();
// Inherit the parent event
event_inherited();

if(state == BATTLE_PHASE_STATE_ANIMATE){
	if(battleTextDisplay.signal){
		// checks if animation is done
		
		var pause= true;
		
		if(battleAnimSpriteFinished() && instance_number(battleAttackSpriteHandler) == 0){
			pause = true;
			// executes script and destroys the end phase container.
			do {
				if(!is_undefined(current_end_phase)){
					
					if(current_end_phase.end_id != -1){
						pause = objdata_endphase.pause[current_end_phase.end_id];
					}

					if(instance_exists(current_end_phase)){
						if(current_end_phase.script != -1){
							script_execute(current_end_phase.script,current_end_phase.target);	
						}
						with(current_end_phase.obj_destroy){
							instance_destroy();	
						}
					}

					state = BATTLE_PHASE_STATE_TEXT;
					
					with(current_end_phase){
						instance_destroy();	
					}
					current_end_phase = noone;
					if(!pause){
						battleEndPhaseAnimationStage();	
					}
				} else {
					break;	
				}
			}
			until (pause);
				
			
			if(is_undefined(current_end_phase)){
				if(!complete){
					if(!instance_exists(screenFade)){
						var timer = 20
		
						newalarm[0] = 20;
						newalarm[1] = 21;
						fadeIn(timer,c_black);
					}
					complete = true;
				}
			}
		}
	}
}