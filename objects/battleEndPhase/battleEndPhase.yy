{
    "id": "d9513390-0cf9-455f-9e71-c32d34f9e9e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleEndPhase",
    "eventList": [
        {
            "id": "99e544b1-d393-4125-9d2b-a768af58d1f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        },
        {
            "id": "b1200140-0d69-4ee4-a8d7-752386e954de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        },
        {
            "id": "4034a9c0-9ac4-4666-822d-5aa22db28009",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        },
        {
            "id": "f4e62851-14d6-4f9c-aadb-bf9656d4ee1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        },
        {
            "id": "3a63780e-1542-4666-8e24-c818b6f7a471",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        },
        {
            "id": "e15da9bb-2258-495d-8275-dfe2b7d0180d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        },
        {
            "id": "02b05905-658d-490c-90f0-ccebfb16f51e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9513390-0cf9-455f-9e71-c32d34f9e9e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "40f71446-3930-4b99-90ec-0d26891a5c6b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}