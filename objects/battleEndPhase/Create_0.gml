/// @description Insert description here
// You can write your code in this editor
initTimingAlarm();
event_inherited();
end_phase_stuff = ds_queue_create();

with(battleStatChange){
	turns--;
	if(turns <= 0){
		instance_destroy();	
	}
}

with(battleColorHolder){
	turns--;
	if(turns <= 0){
		instance_destroy();	
	}
}

with(Battler){
	battlerUpdateStats();	
	flinch = false;
}

if(!BattleHandler.fleeing){
	with(battleStatusEffect){
		if(turns != -1){
			turns--;
	
			if(filterCanDoEffect(id)){
				if(turns <= 0){
					if(objdata_status.script_destroy[status_id]  != -1){
						var script = objdata_status.script_destroy[status_id] 
						var targ = target;
						var me = id;
						with(battleEndPhase){
							var endph = battleEndPhaseAdd(script,me,-1);	
							endph.obj_destroy = me;
						}
					} else {
						var script = filter_destroy_default;
						var targ = id;
						var me = id;
						with(battleEndPhase){
							var endph = battleEndPhaseAdd(script,me,-1);	
							endph.obj_destroy = me;
						}
					}
				} else {
					if(objdata_status.script_end_turn[status_id] != -1){
						var script = objdata_status.script_end_turn[status_id] 
						var targ = target;
						var me = id;
						with(battleEndPhase){
							var endph = battleEndPhaseAdd(script,me,-1);
						}
					}
				}
			}
		}
	}
}

with(BattleHandler){
	color_active = false;	
}

//adds stuff to the end phase
if(!BattleHandler.fleeing){
	battleEndPhaseInit();
}

textbox_hold_counter = 0;
textbox_hold_max = global.text_auto_speed;

menu_select = battleEndPhaseConfirm;
menu_select_hold = battleEndPhaseConfirmHold;

complete = false;

depth = -1000005
draw_this_frame = false;

battleZoomDefault(20)
battleAnimReturnAll();

current_end_phase = noone;

state = BATTLE_PHASE_STATE_TEXT;

battleEndPhaseAnimationStage();	

mouseHandler_clear();
mouseHandler_add(0,0,800,600,battleEndPhaseConfirmClick,"")