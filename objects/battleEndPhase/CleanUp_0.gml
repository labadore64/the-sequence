// @description Insert description here
// You can write your code in this editor
battleStartBegin();

with(Battler){
	battlerUpdateStats();	
	defended_this_turn = false;
}

if(ds_exists(end_phase_stuff,ds_type_queue)){
	ds_queue_destroy(end_phase_stuff);


	if(!instance_exists(battleEndFlee) &&
	!instance_exists(battleEndVictory) &&
	!instance_exists(battleEndDefeat)){
	instance_create(0,0,menuBattleMain);

		var test_win = battleHandlerTestWinner()

		if(test_win == "win"){
			battleWinScript();
		} else if (test_win == "lose"){
			battleLoseScript();
		}	else if (test_win == "flee"){
			battleFleeScript()
		}

		battleAuraCalculate()

	}
	
	with(BattleHandler){
		turn_count++;	
	}
	with(battleItemHolder){
		instance_destroy();	
	}
}