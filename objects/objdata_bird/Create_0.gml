/// @description Insert description here
// You can write your code in this editor
var itemcount = 0;

// gets the count of the items
var filename = working_directory + "resources\\bird\\bird";
var ext = ".ini";
var testname = "";

testname = filename+string(itemcount)+ext;

while(file_exists(testname)){
	//loads data

	ini_open(testname)
	name[itemcount] = ini_read_string_length("data","name","",IMPORT_MAX_SIZE_BIRDNAME);
	bird_science_name[itemcount] = ini_read_string_length("data","long_name","",IMPORT_MAX_SIZE_SCIENCEBIRDNAME);
	bird_long_name[itemcount] = ini_read_string_length("data","name","",IMPORT_MAX_SIZE_SCIENCEBIRDNAME);
	bird_desc[itemcount] = ini_read_string_length("data","desc","",IMPORT_MAX_SIZE_DESC)

	bird_height[itemcount] = ini_read_string_length("data","metric_height","",IMPORT_MAX_SIZE_BIRDSIZE);
	bird_weight[itemcount] = ini_read_string_length("data","metric_weight","",IMPORT_MAX_SIZE_BIRDSIZE);

	bird_height_metric[itemcount] = ini_read_string_length("data","metric_height","",IMPORT_MAX_SIZE_BIRDSIZE);
	bird_weight_metric[itemcount] = ini_read_string_length("data","metric_weight","",IMPORT_MAX_SIZE_BIRDSIZE);	

	
	bird_adjust[itemcount] = ini_read_real("data","offset",0);
	bird_scale[itemcount] = ini_read_real("data","scale",1)
	bird_male_sprite[itemcount] = asset_get_index("spr_bird_" + ini_read_string_length("data","male_sprite","",IMPORT_MAX_SIZE_SPRITE) + "_idle")	
	bird_female_sprite[itemcount] = asset_get_index("spr_bird_" + ini_read_string_length("data","female_sprite","",IMPORT_MAX_SIZE_SPRITE) + "_idle")	
	
	if(bird_female_sprite[itemcount] == -1){
		bird_female_sprite[itemcount]  = bird_male_sprite[itemcount]
	}
	
	ini_close();
	
	itemcount++;
	testname = filename+string(itemcount)+ext;
}

data_size = itemcount;
