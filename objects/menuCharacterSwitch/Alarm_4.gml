/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

menuAddOption("Party","Switch party members.",menuCharacterSwitchPartyScript);
//menuAddOption("Sell","View and sell your inventory.",-1)
if(can_talk){
	menuAddOption("Talk","Talk to " + character_name + ".",menuOverworldPartySubDialogScript)
}
menuAddOption("Leave","Leave the reception desk.",menu_cancel)

menu_size = ds_list_size(menu_name);