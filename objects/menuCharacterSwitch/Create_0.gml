/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

title = "Reception Desk"

var cutenames = ds_list_create();

ds_list_add(cutenames,"munchkin","sweetie","youngin","sweet cheeks","cutie pie")

var cutename = ds_list_find_value(cutenames,irandom_range(0,ds_list_size(cutenames)-1))

text_start = "Hey there, "+cutename+"! Whatcha up to today?";

cutename = ds_list_find_value(cutenames,irandom_range(0,ds_list_size(cutenames)-1))

text_return = "Need anything else, " + cutename +"?";

text_to_use = text_start;

menu_cancel = menuCharacterSwitchPartyCancel
