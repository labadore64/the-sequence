{
    "id": "97f8edf7-cf3a-401a-990b-d65cf9a9c2b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuCharacterSwitch",
    "eventList": [
        {
            "id": "553b8025-17de-4213-82bb-ed3b72e569c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97f8edf7-cf3a-401a-990b-d65cf9a9c2b5"
        },
        {
            "id": "0ecceba0-29b3-4839-9241-f6737266232c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "97f8edf7-cf3a-401a-990b-d65cf9a9c2b5"
        },
        {
            "id": "2ac45f0e-0b0d-4806-a6ad-4b4e4be13318",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "97f8edf7-cf3a-401a-990b-d65cf9a9c2b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "35653b71-cb48-431a-a97c-4f6608da2d9b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}