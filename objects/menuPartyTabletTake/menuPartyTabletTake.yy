{
    "id": "df3000f6-5d40-4adf-a4e4-6d885799407f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuPartyTabletTake",
    "eventList": [
        {
            "id": "d8fd8018-e78e-4094-b326-6247f1e28fd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df3000f6-5d40-4adf-a4e4-6d885799407f"
        },
        {
            "id": "98330d29-baf9-4a9f-b2c1-7e317b8594b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "df3000f6-5d40-4adf-a4e4-6d885799407f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a4ce8930-7695-4226-a0d6-b1d08c96cc71",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}