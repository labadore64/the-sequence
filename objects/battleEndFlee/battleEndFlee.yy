{
    "id": "613bee6f-309b-492a-be71-c8433b0551ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "battleEndFlee",
    "eventList": [
        {
            "id": "4eaa0762-3563-454e-b809-96894f5f4af1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "613bee6f-309b-492a-be71-c8433b0551ed"
        },
        {
            "id": "66f939a8-902b-4fe6-8f0e-d9263b68891f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "613bee6f-309b-492a-be71-c8433b0551ed"
        },
        {
            "id": "3883a648-8b41-474e-b013-c98852e538bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "613bee6f-309b-492a-be71-c8433b0551ed"
        },
        {
            "id": "af8c44b0-ce7e-4907-bd48-561d12cdb662",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "613bee6f-309b-492a-be71-c8433b0551ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}