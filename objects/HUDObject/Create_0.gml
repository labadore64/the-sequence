/// @description Insert description here
// You can write your code in this editor

name = ""; // name used as reference for this object
variable_name = ""; //variable name used to reference vars in the controller.
hudobj_name = ""; // name used for the filename of this object
read_text = ""; // text that is read when you move to this object
hud_id = -1; // the id value used for this HUD

parent = noone; // this object's parent object, used for the origin command and the parent keyword
children[0] = noone; // this object's child objects

key_id[0] = ""; // id used for the key input
key_func[0] = -1; // which function is used for the key input (text/move/exit/origin)
key_arg[0] = ""; // argument used for the key function
key_req[0] = ""; // required variable to allow this function to be available.
for(var i = 0; i < 10; i++){
	infokey[i] = "";
}