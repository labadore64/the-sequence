{
    "id": "638c658b-3571-4c42-9740-9b2bd1ac9197",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "titleFont",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Open Sans",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "37f3342e-edac-4a68-91ce-349683b9f854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 87,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 951,
                "y": 180
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2a27be1a-5fac-48d1-ba2c-12955b02e334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 87,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 46,
                "y": 269
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "361a82bd-afd6-4816-99b3-2a8389070d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 87,
                "offset": 4,
                "shift": 30,
                "w": 23,
                "x": 799,
                "y": 180
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "186aa768-e5a4-4ca0-b472-f893866774ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 87,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 788,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "508b5df6-c9f0-4715-9cc2-012fcd8c0b22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 33,
                "x": 703,
                "y": 91
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2cb01a5b-71f2-4497-8b77-1cf0a532ae7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 87,
                "offset": 2,
                "shift": 58,
                "w": 55,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4208d5c6-61e8-4c83-9de3-8950ddc8fa38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 87,
                "offset": 3,
                "shift": 48,
                "w": 46,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ba4f9475-4161-471e-bfa8-6867fcf4e814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 87,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 98,
                "y": 269
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c3efa7ad-f16c-464e-afd5-4823a8127874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 87,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 931,
                "y": 180
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "70dc99ae-e1f4-45ed-abb6-9a4d4162ea42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 87,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 890,
                "y": 180
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "315eb0b9-2d65-4a43-a1b2-1b2d8d9949ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 87,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 241,
                "y": 180
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1cb73760-7e5e-4300-ac37-b2a56ea489f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 139,
                "y": 180
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fb90b66f-05f6-4dc0-a314-7e920c0eb04b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 87,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 2,
                "y": 269
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9f8d60ec-7c8d-451b-9190-578c748371cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 87,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 911,
                "y": 180
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "027cb69a-2eb2-485c-9d04-5c21bd2cfa41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 87,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 18,
                "y": 269
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0e874472-0978-47e2-9250-5062414e07c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 87,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 584,
                "y": 180
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "236855fc-e8b2-4930-a25d-a46097de91a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 843,
                "y": 91
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c45a944f-7c06-47a7-aa66-1fd0b349b12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 87,
                "offset": 4,
                "shift": 37,
                "w": 24,
                "x": 698,
                "y": 180
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b26d0913-060c-442e-8454-6b373c85bc03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 913,
                "y": 91
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c67a3d27-79d3-44e1-b6bb-43f7bfffc4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 808,
                "y": 91
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b2b252de-5124-44bc-bf98-fdeb784b6fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 87,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 306,
                "y": 91
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cdabc265-93f9-4fe2-9a5e-ea6ccea77fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 308,
                "y": 180
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "846d28dd-9c0c-4f2d-8179-6e2fa4d06428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 668,
                "y": 91
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "46d9577f-fbe9-4593-8fe4-6fa7752de558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 525,
                "y": 91
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a112391b-5a8b-45f1-a068-feed7457317d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 738,
                "y": 91
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "26f9e04e-14a8-46e6-ad31-878d39d7dbc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 2,
                "y": 180
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "21e5008d-3961-4c2b-beec-4d4b2d061b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 87,
                "offset": 4,
                "shift": 18,
                "w": 12,
                "x": 32,
                "y": 269
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "96f7481c-1af6-4d1e-b59c-b4557a85cac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 87,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 1006,
                "y": 180
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2205ab0b-8703-45be-b6e4-83b33d5a11bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 105,
                "y": 180
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7b3bad4a-b8de-4c54-be2f-e188eb498b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 173,
                "y": 180
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f7891937-8fb2-442b-be5f-490b2105a110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 207,
                "y": 180
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2cea1d9a-3ab4-4cd5-b9a4-a365b90dd507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 87,
                "offset": 0,
                "shift": 31,
                "w": 29,
                "x": 341,
                "y": 180
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4444ee98-1283-43f1-ab6b-ea67e19b7cbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 87,
                "offset": 3,
                "shift": 57,
                "w": 52,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cc6e9046-37e3-4f5a-abb8-64deb23a7052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 87,
                "offset": 0,
                "shift": 44,
                "w": 45,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9a515825-dba5-423e-9364-bc893c0e1909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 87,
                "offset": 6,
                "shift": 43,
                "w": 35,
                "x": 269,
                "y": 91
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "877d790f-af13-49bb-8f6c-b7252a0076fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 87,
                "offset": 4,
                "shift": 41,
                "w": 36,
                "x": 157,
                "y": 91
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4b8b23b1-192c-425a-b2eb-c6ea0a7ba431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 87,
                "offset": 6,
                "shift": 47,
                "w": 39,
                "x": 829,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "96c53006-e3e3-44c3-8e1f-b896bf27a55f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 87,
                "offset": 6,
                "shift": 36,
                "w": 28,
                "x": 465,
                "y": 180
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8ec625b0-5e71-4b6c-bc71-bfabfcc867c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 87,
                "offset": 6,
                "shift": 35,
                "w": 27,
                "x": 613,
                "y": 180
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a9398d05-da85-42a6-8bed-d1e5e266ad58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 87,
                "offset": 4,
                "shift": 46,
                "w": 39,
                "x": 747,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "40965e16-05b1-471b-a930-904edcb6e485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 87,
                "offset": 6,
                "shift": 49,
                "w": 39,
                "x": 706,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "04ef6aa5-3bfe-4a0f-8f1e-3e5deb2cd847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 87,
                "offset": 6,
                "shift": 21,
                "w": 11,
                "x": 60,
                "y": 269
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2c3a2f6c-7b09-413b-8460-34360b08a753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 87,
                "offset": -5,
                "shift": 21,
                "w": 21,
                "x": 824,
                "y": 180
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2517e138-51fe-432b-8ab3-5fe389a763b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 87,
                "offset": 6,
                "shift": 43,
                "w": 38,
                "x": 910,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fd259f56-d319-4ea4-a537-fe137acf0a08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 87,
                "offset": 6,
                "shift": 36,
                "w": 29,
                "x": 403,
                "y": 180
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a5a3718c-4de5-49c9-b1b0-715dc5396e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 87,
                "offset": 6,
                "shift": 60,
                "w": 50,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "88a436e8-225b-4280-9ba3-939f273ec0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 87,
                "offset": 6,
                "shift": 52,
                "w": 42,
                "x": 576,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bf2fc805-7076-4264-9876-4c9dd266fef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 87,
                "offset": 4,
                "shift": 51,
                "w": 45,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "211b99f4-0f1f-4222-8a70-74a9d44317d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 87,
                "offset": 6,
                "shift": 40,
                "w": 33,
                "x": 633,
                "y": 91
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "43f8600c-c033-46e0-a78c-de1cfe0c23ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 87,
                "offset": 4,
                "shift": 51,
                "w": 45,
                "x": 484,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "40770f91-8628-4385-9cf8-d7d583d698c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 87,
                "offset": 6,
                "shift": 42,
                "w": 38,
                "x": 950,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "621a05e9-3543-45de-9415-b2bc42e9c2de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 87,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 275,
                "y": 180
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "405f4a39-9ca1-4466-8ead-3897c90e129b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 87,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 195,
                "y": 91
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9b8253ff-5afd-44f9-b6a4-a28d778bac1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 87,
                "offset": 5,
                "shift": 48,
                "w": 38,
                "x": 870,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dd6e4a16-2950-4a6e-bcd9-f35c48238d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 87,
                "offset": 0,
                "shift": 42,
                "w": 42,
                "x": 620,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "90f55a86-de2d-4d5c-9c29-1735095d0a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 87,
                "offset": 0,
                "shift": 62,
                "w": 62,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d35c09ba-b66f-481d-a384-6412f63c85b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 87,
                "offset": 0,
                "shift": 43,
                "w": 43,
                "x": 531,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ae527ffa-20a0-4dfd-a827-c48919909d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 87,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 664,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "068dc7da-fab9-4e49-9e77-19139f5fbf89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 87,
                "offset": 2,
                "shift": 37,
                "w": 35,
                "x": 232,
                "y": 91
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c311613e-7098-43b5-b433-97f3f1f3c8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 87,
                "offset": 4,
                "shift": 21,
                "w": 16,
                "x": 988,
                "y": 180
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b692e56e-9668-45c8-8c60-ef936c6e5687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 87,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 555,
                "y": 180
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "463fa3df-ae76-4452-ad8a-169029481572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 87,
                "offset": 2,
                "shift": 21,
                "w": 16,
                "x": 970,
                "y": 180
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "54de5b79-5924-4506-869c-b6dd43af2978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 87,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 489,
                "y": 91
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ac7f149e-320e-4f11-8c44-8f1af69be4d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 87,
                "offset": 0,
                "shift": 26,
                "w": 28,
                "x": 525,
                "y": 180
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e0ccc309-1667-4dcb-b1e9-40cf0c1e7a34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 87,
                "offset": 10,
                "shift": 39,
                "w": 19,
                "x": 869,
                "y": 180
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "753aa391-1dc0-4cd7-ab08-ccbc18fe0a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 87,
                "offset": 3,
                "shift": 39,
                "w": 32,
                "x": 37,
                "y": 180
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c281a918-362b-45ba-a80d-06dbb5879229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 87,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 948,
                "y": 91
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "10afb6ae-3adf-4fbe-8bcb-3bb637dae275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 87,
                "offset": 3,
                "shift": 33,
                "w": 29,
                "x": 434,
                "y": 180
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6e640364-b8aa-4d71-b2f4-2b4225cea888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 87,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 453,
                "y": 91
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0aeb2db3-ead2-4824-837f-765717f17f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 87,
                "offset": 3,
                "shift": 38,
                "w": 34,
                "x": 417,
                "y": 91
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "badb234e-2f7d-4d1d-a6ba-e8e319e2f03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 87,
                "offset": 1,
                "shift": 25,
                "w": 27,
                "x": 642,
                "y": 180
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a8477777-3798-4616-b775-505018d98b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 87,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 119,
                "y": 91
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "05b4f698-793b-42e7-9b28-9ab78d4d8339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 87,
                "offset": 5,
                "shift": 42,
                "w": 33,
                "x": 878,
                "y": 91
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8496396f-a2ef-41e2-94b0-ea15672acfc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 87,
                "offset": 5,
                "shift": 20,
                "w": 11,
                "x": 73,
                "y": 269
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "38770391-4280-48fd-9d47-93b6cba86b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 87,
                "offset": -4,
                "shift": 20,
                "w": 20,
                "x": 847,
                "y": 180
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b4adcdeb-f0ff-4049-a2de-9e31ac57d692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 87,
                "offset": 5,
                "shift": 40,
                "w": 35,
                "x": 343,
                "y": 91
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "313cccda-64ee-46a6-9b39-711b54634dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 87,
                "offset": 5,
                "shift": 20,
                "w": 10,
                "x": 86,
                "y": 269
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a5c86f83-05dc-47b6-833f-970bf6247c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 87,
                "offset": 5,
                "shift": 63,
                "w": 54,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "69462c36-701e-4977-841b-f72d220bb34f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 87,
                "offset": 5,
                "shift": 42,
                "w": 33,
                "x": 773,
                "y": 91
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8cb8d916-d988-41fb-993b-c3bc8bfe1a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 87,
                "offset": 3,
                "shift": 40,
                "w": 35,
                "x": 380,
                "y": 91
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c73b3b42-8b6b-4458-b170-b7314c3ce3d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 87,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 983,
                "y": 91
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "cf811427-291e-4568-ba00-433279c0e439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 87,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 597,
                "y": 91
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8520ca96-9f8a-4f4f-9298-23ec3b4dccd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 87,
                "offset": 5,
                "shift": 29,
                "w": 23,
                "x": 724,
                "y": 180
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c353efc6-b690-455b-806f-ca0e10121185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 87,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 495,
                "y": 180
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ea1ec7b5-e402-4ab8-b829-48960476fcc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 87,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 671,
                "y": 180
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f244a6ee-b075-4f1b-ad87-f13013844470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 87,
                "offset": 5,
                "shift": 42,
                "w": 34,
                "x": 561,
                "y": 91
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c2a65a99-34fa-478d-aeba-68316c1797d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 87,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 80,
                "y": 91
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3102c2dc-27df-42a0-8c51-ef82693a6207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 87,
                "offset": 1,
                "shift": 55,
                "w": 55,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7df73aa8-f19b-4d99-86dc-aad1314c1438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 87,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 41,
                "y": 91
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6405c916-0b31-466d-8f3b-d0e1ea93b1ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 87,
                "offset": 0,
                "shift": 36,
                "w": 37,
                "x": 2,
                "y": 91
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "89bc054a-f988-43f2-9b4d-dd51a2413854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 87,
                "offset": 2,
                "shift": 31,
                "w": 29,
                "x": 372,
                "y": 180
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "11492246-cd8e-467d-adbc-e04ae1db3a7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 87,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 774,
                "y": 180
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4c9ad98f-ce32-4519-898d-d2b268639290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 87,
                "offset": 14,
                "shift": 35,
                "w": 8,
                "x": 109,
                "y": 269
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6d950513-7594-4496-837c-5377dd6c0318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 87,
                "offset": 3,
                "shift": 25,
                "w": 23,
                "x": 749,
                "y": 180
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4c4d43ee-1671-4ed5-9df6-227c09323f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 87,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 71,
                "y": 180
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a144e68c-7a00-4d26-a20a-335a368f7d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 65
        },
        {
            "id": "ae7463aa-51fa-41ed-b870-76368a328670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 84
        },
        {
            "id": "bd87de0d-746b-47c3-aeb2-bf1c495f37e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 86
        },
        {
            "id": "f8f6b38a-e2e6-4b41-a82d-08bc210baf9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 87
        },
        {
            "id": "6e2cb220-79e5-4156-b5c7-afe32786bf64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 89
        },
        {
            "id": "89e1c10c-d660-4456-89c7-ed4964065f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 97
        },
        {
            "id": "77ea8b9c-4b4e-4df2-92e4-984c3b374384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 99
        },
        {
            "id": "4865391f-3c25-4541-9143-bcf4a413a149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 100
        },
        {
            "id": "b93fa3f9-8d9b-4c44-8202-0dace17a4ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 101
        },
        {
            "id": "2efc36ea-e645-4483-976c-be6f2ab40356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 103
        },
        {
            "id": "5c3a5c56-6964-4a67-a1e3-d1aacee359f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 109
        },
        {
            "id": "1c5555ee-6650-4976-912c-1d418757a971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 110
        },
        {
            "id": "b53c871d-dd88-4f72-8fba-d099c8198fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 111
        },
        {
            "id": "0a0fc94c-1d56-4441-807c-9b986d532fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 112
        },
        {
            "id": "f104c297-2909-47c1-9cd9-b30332919068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 113
        },
        {
            "id": "0eadaf05-571e-4b44-86be-3cd7fbd77198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 114
        },
        {
            "id": "054ded1e-98f1-48d1-a87f-c38e1e435853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 115
        },
        {
            "id": "22d05da1-9474-4438-9afb-34238a820c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 117
        },
        {
            "id": "6bf2f8ed-6deb-421f-a1a0-a36a0a7e2b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 192
        },
        {
            "id": "b152e6ed-1a47-4b3a-b7a6-8755c4978356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 193
        },
        {
            "id": "8480f707-ba7f-4089-b0a5-ce56e4efd84f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 194
        },
        {
            "id": "7f2fc8bb-4f6a-471c-9569-578da6a674b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 195
        },
        {
            "id": "45c45283-6d40-4afd-b9f1-fdc090d83259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 196
        },
        {
            "id": "9b56fb98-297a-4393-b066-f148e59de9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 197
        },
        {
            "id": "5d1f358c-ce2b-4f3a-b3bc-fedb2c770486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 221
        },
        {
            "id": "67dcea47-44e5-4a26-acfb-f5fe4a946b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 224
        },
        {
            "id": "2f8a22fd-382e-4b81-97a3-2af184fc2af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 225
        },
        {
            "id": "7fbefad4-276a-4183-bad9-6d1f835cec8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 226
        },
        {
            "id": "e7ca3255-e38e-48f7-be11-87e0aa47fadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 227
        },
        {
            "id": "55d1e869-e89a-419f-8a8f-235f93b1eba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 228
        },
        {
            "id": "31fc4d75-617e-4252-8d0f-c78e890f9a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 229
        },
        {
            "id": "805003b7-6c65-491b-bc6f-499c06fde16f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 230
        },
        {
            "id": "a3d8a0c3-673e-40a9-a576-1f1901a3655c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 231
        },
        {
            "id": "2c7dd97c-160f-40e6-b758-dce6b0288468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 232
        },
        {
            "id": "775b868c-849a-4706-966d-a82a59863f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 233
        },
        {
            "id": "8f275239-81bc-425c-8ff0-72fcf2461e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 234
        },
        {
            "id": "6d53f7bc-4a8a-48da-8c65-fe92b8b5053b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 235
        },
        {
            "id": "e5263d8c-e2a3-4077-b843-06810024af7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 242
        },
        {
            "id": "d483437f-5d51-4869-b737-69bc743f4434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 243
        },
        {
            "id": "2274ee89-6a94-4455-ab66-8201dd5a1e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 244
        },
        {
            "id": "3388298a-fd53-4bce-85ef-c03ab25bab25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 245
        },
        {
            "id": "06a70924-8ca4-49b3-8d61-1602beac7188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 246
        },
        {
            "id": "cb98420e-f58b-4c4f-9e95-725937b4fcd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 248
        },
        {
            "id": "12e566e6-bf36-425d-bd67-6eff2970b4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 249
        },
        {
            "id": "19cd8380-fe8f-4b01-ae9b-1acdcca62c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 250
        },
        {
            "id": "a03c17d1-eb3d-4bdc-bab8-7c171e003bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 251
        },
        {
            "id": "11d3b235-e35d-4617-9a9e-a61edc85281d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 252
        },
        {
            "id": "2fe8dc98-1afc-416a-8623-65def4123f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 256
        },
        {
            "id": "e527e805-7597-42d9-b4c2-3962963205d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 257
        },
        {
            "id": "ffeb2a6a-a2b7-4c3c-8710-f660ae922709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 258
        },
        {
            "id": "dcbfb025-b61d-4409-8107-47ee8c087feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 259
        },
        {
            "id": "767cc9d5-6399-40e3-b60c-275dab139772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 260
        },
        {
            "id": "c2938861-64c7-4aa2-bb27-66ba9375bd98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 261
        },
        {
            "id": "a2f326a7-3b8b-4192-9c0c-fa18b144aa1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 263
        },
        {
            "id": "7fd9aef6-eef5-4e8e-a443-e2d3b2b3dcb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 265
        },
        {
            "id": "0ecd94fb-e744-45a1-9781-09c2d177087d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 267
        },
        {
            "id": "de76dca3-0f86-440f-b5da-ab60b37cc6e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 269
        },
        {
            "id": "7a7ecade-7d4d-402e-9c89-e7009e13461a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 271
        },
        {
            "id": "14a112a2-7826-4e72-9f7e-5b33787d081c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 273
        },
        {
            "id": "d12da8a4-6761-4ed9-8b1e-0df5dc8d49d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 275
        },
        {
            "id": "b7dbc368-c018-468f-aabd-773b6741c359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 277
        },
        {
            "id": "a0d00ec1-a0ff-4108-8c8e-d69e977e4a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 279
        },
        {
            "id": "c45a4301-9e3a-4dbf-a1d6-15008b60d246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 281
        },
        {
            "id": "c099d38e-eb75-4e50-b898-a89e61ba0596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 283
        },
        {
            "id": "4e1ae446-3426-4a35-8e8a-6501111e1cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 285
        },
        {
            "id": "859de51e-a42a-4ba1-ba17-78ad3610274c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 287
        },
        {
            "id": "7ff09fb5-41d4-46ab-84a5-39dcab09617f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 289
        },
        {
            "id": "baec505d-f81d-46dc-98bd-164801aeeac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 291
        },
        {
            "id": "a00bd637-c215-41d6-85b6-2970b6486cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 312
        },
        {
            "id": "776b6967-8da4-4175-a707-c09f6c52845d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 324
        },
        {
            "id": "c881d964-cfb5-42d8-b5f1-593a21f48366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 326
        },
        {
            "id": "a2651661-4765-45e0-8f1c-afcbe0142048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 331
        },
        {
            "id": "141d8441-4bfd-4f67-8188-c88ddbc898c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 333
        },
        {
            "id": "62fedd71-a59c-4002-a8f8-0ffb954bbdae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 335
        },
        {
            "id": "bb4b709e-d231-4fc1-8cfb-f028736712c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 337
        },
        {
            "id": "ff7a52c6-d354-44a2-a08a-df9314090779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 339
        },
        {
            "id": "bba3edec-3216-4a69-8b95-b0e8e749f2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 341
        },
        {
            "id": "968849fd-5021-4d86-8c48-c657af8e9040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 343
        },
        {
            "id": "28c2ecc8-06c1-454b-a49d-64d320ec05eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 347
        },
        {
            "id": "d41ef881-df82-4fe6-968d-8768d4d98529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 351
        },
        {
            "id": "36d2a804-2d62-437f-958e-b9dab0251da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "a2141daa-4b1e-4994-aaad-d5ce824213ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "b290ed75-8b18-475d-bc92-b2801b65ff9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 361
        },
        {
            "id": "420e2a09-fa04-40e1-a2d4-69080031ed48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 363
        },
        {
            "id": "54130b6e-007d-40e7-a24d-3fed4d79ae61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 365
        },
        {
            "id": "76737ed9-b013-47d5-97c2-aced62e7e1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 367
        },
        {
            "id": "6916b604-8a49-48d5-beeb-2de6cff031a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 369
        },
        {
            "id": "2e65cd42-3ca6-4c31-b6c6-81ea30462baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 371
        },
        {
            "id": "b07b0740-23a5-4e62-a35e-cb4d7d755397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 372
        },
        {
            "id": "a5a34484-ad72-4b4f-ae2a-ad22daf73bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 374
        },
        {
            "id": "7a00ae96-069d-4883-bbb4-f9c1e432d2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 376
        },
        {
            "id": "99e440f1-36ab-4ef9-8754-2ae277f304b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 417
        },
        {
            "id": "1d231167-0713-4423-9a81-b5db1bd71b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 432
        },
        {
            "id": "54541a0c-0c3a-426b-8c37-8df3459772c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 506
        },
        {
            "id": "45cdd143-53da-4dde-baa7-7cd77b294fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 507
        },
        {
            "id": "1dbfea73-7a54-4333-8b19-31aaeaf92118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 509
        },
        {
            "id": "fb0a9bda-7c4a-4d04-8b3c-6456d309005d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 511
        },
        {
            "id": "a90d27fc-0368-4ca2-9688-1bb7fe668520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 537
        },
        {
            "id": "ead4e1da-5ec7-475c-878c-27fc45c5f564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "fa5545eb-4169-4008-be17-2cb1c78e0326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 902
        },
        {
            "id": "48c2ea85-9eae-42ae-a7ee-65217959de2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 913
        },
        {
            "id": "136ecc72-7262-49e9-b1a6-156c145e9a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 916
        },
        {
            "id": "ea55f74c-aa06-4794-a3d8-ca244dd1b57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 923
        },
        {
            "id": "b24aff57-2b7f-476c-8f10-b8a3f51fedb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 940
        },
        {
            "id": "cbf9d3a5-7dde-4587-a446-7fe5c356b798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 941
        },
        {
            "id": "7f8f4d97-86eb-4a99-917b-41ae30e41023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 942
        },
        {
            "id": "e233fab4-ef54-4baa-aa8c-0572fa3b6815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 945
        },
        {
            "id": "271b9864-93b6-4a0c-9ee1-387d0c5ce523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 948
        },
        {
            "id": "f3bf4e69-0fec-4298-bcc0-a183f234e527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 949
        },
        {
            "id": "e47d0b75-feea-41ea-963b-28d590994217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 950
        },
        {
            "id": "ce7a7829-8e51-4307-bebc-e08129f58c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 951
        },
        {
            "id": "470f7529-17e7-4a59-8892-9a8d86b95403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 954
        },
        {
            "id": "6713e4f6-f6a0-4572-be5a-aa87eb8130bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 956
        },
        {
            "id": "02dee3ae-f740-4e7c-8590-365d8f7f448f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 959
        },
        {
            "id": "7ed1e2fb-f68e-4bf7-bbfb-c4f65369d215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 961
        },
        {
            "id": "cd1db809-d291-4f81-a68d-43ab4d8138bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 962
        },
        {
            "id": "aea0e48f-0ab0-4c94-b27d-13a8a9f4abdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 963
        },
        {
            "id": "d61d75b7-1561-43e8-bb41-5e0be2b0280e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 966
        },
        {
            "id": "258bee29-5605-4c25-91e7-a4fdf388df11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 972
        },
        {
            "id": "73a56340-598a-4055-ae8a-7fb07a15a049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1033
        },
        {
            "id": "c34e6c91-1c98-490b-bbf1-443d94786357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1040
        },
        {
            "id": "3c05ef63-21f9-4df3-89f1-558a5efba770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1044
        },
        {
            "id": "0cf964ef-292a-4332-9be5-f01ccbb81e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1051
        },
        {
            "id": "bc28181d-ad7e-42dc-85a2-2348f1f9c3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1072
        },
        {
            "id": "2f20eb2e-af57-4c71-a02e-d0f07666df34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1076
        },
        {
            "id": "5fbc5cc5-b161-4435-a63a-f37e3270a005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1077
        },
        {
            "id": "21dec21a-878f-41c5-b8ea-4b04c55e5ed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1083
        },
        {
            "id": "dc2bb240-dd6b-497d-91fe-4fd62012433a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1086
        },
        {
            "id": "283e8a44-a588-4724-9bd4-799294433c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1089
        },
        {
            "id": "52a02a24-ce44-4413-b1f8-9b59de19351a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1092
        },
        {
            "id": "680f8e62-4b29-47ae-a736-cf1444db3caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1104
        },
        {
            "id": "4f933790-d7cc-41fc-978d-453b7217fdf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1105
        },
        {
            "id": "6f7eda93-4afc-4ad6-8ad3-c606cb1c6905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1108
        },
        {
            "id": "37bdfc53-6edd-4361-afdf-8c4d84fc6499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1109
        },
        {
            "id": "c882dd86-0d73-4cd0-ae39-c1b87d704c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1113
        },
        {
            "id": "9468d41c-86b7-422e-9e2c-af7b8bf185fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1126
        },
        {
            "id": "cb32bd27-10ba-4a66-8991-9b08e91b2d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1127
        },
        {
            "id": "8eb69072-5a1d-4854-9b1e-f58e0922362d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1137
        },
        {
            "id": "96b8b268-0395-4c79-bf6a-6404f6fedb35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1139
        },
        {
            "id": "0cad828d-e4eb-4599-bad6-bbee2f901054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1145
        },
        {
            "id": "57e17361-6b9b-4639-8f20-697329ffe303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1147
        },
        {
            "id": "723718b4-c346-4777-b5a2-7bc6bdc4f5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1149
        },
        {
            "id": "526c2384-44e2-468f-bcae-002fd7b63ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1153
        },
        {
            "id": "4379d086-caa4-4ea0-8b0a-4c71b6abfad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1193
        },
        {
            "id": "c259ed2e-0d4f-412e-bdc1-5b5d0d922670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1195
        },
        {
            "id": "121dd3bf-3b2d-4e3a-946f-6dfcef4b38b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1221
        },
        {
            "id": "df9b1567-e9bb-49ca-8257-8e410ce0ca70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1222
        },
        {
            "id": "22a11c29-dcf4-449c-885e-0958e51076d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1232
        },
        {
            "id": "34407f0d-961c-4045-b27f-efbc3b8b5e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1233
        },
        {
            "id": "e5b71dd9-06ad-47be-b05d-dbe694be9a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1234
        },
        {
            "id": "8b78561d-069f-4532-902c-43dce67fda84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1235
        },
        {
            "id": "0794509e-aa30-49ee-a415-a41f0ea1633b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1236
        },
        {
            "id": "701c3e60-f768-428d-91c0-a23de6714077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1237
        },
        {
            "id": "c7311b5d-017b-4efd-8176-6915eb3fd8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1239
        },
        {
            "id": "c103656d-9a5c-4f34-893e-3a3eb0ba44e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1241
        },
        {
            "id": "fbf57a47-61eb-437f-a4ca-7e77df8db7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1243
        },
        {
            "id": "2328bf28-50a9-4ba3-b008-f60d0542b695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1255
        },
        {
            "id": "03e0583f-b8a3-49f8-92aa-02b6394fa9d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1257
        },
        {
            "id": "c06a3d8c-64ff-4bb6-8c1e-1c27c62f6b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1259
        },
        {
            "id": "94299c4b-3011-4d75-b589-34e8cc4a45b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1280
        },
        {
            "id": "38e23859-9388-4e60-add1-8243b1f71dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1281
        },
        {
            "id": "263bf614-d80d-4df4-aef1-a5e05feb3fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1282
        },
        {
            "id": "31b9aabf-bba7-493c-94dc-c9dcfe2c0102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1283
        },
        {
            "id": "f432180f-4132-4018-a3ad-a832157b72fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1288
        },
        {
            "id": "ad74a2ba-cc00-4e10-bc9f-b7cf893ed449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1289
        },
        {
            "id": "b5dec646-38a2-48a7-8783-4aa1902bd640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1293
        },
        {
            "id": "336d8d92-b166-4880-8761-de98f3ed5ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1297
        },
        {
            "id": "3882f737-cf11-4973-a5f8-0d6b6a290ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1298
        },
        {
            "id": "199cd885-4557-4b78-a69a-50ca0ef6221b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 1299
        },
        {
            "id": "05e8b3b5-d777-4c5d-ba18-f347193b3110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7680
        },
        {
            "id": "54f6d38e-c21f-453a-b5b6-65dcffc772cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7681
        },
        {
            "id": "2e30a136-309f-4dc3-902a-70c0e3be7628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7743
        },
        {
            "id": "d9a8c533-d779-4354-820c-214bd7db12a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7808
        },
        {
            "id": "28c27559-8a5e-4f02-b152-2147ae0d759c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7810
        },
        {
            "id": "23e317cc-b147-4454-953d-631837975f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7812
        },
        {
            "id": "0775591f-879c-4db9-84e9-8ef7ed79fc96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7840
        },
        {
            "id": "bded632b-d5d5-41cb-99ba-72113d815911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7841
        },
        {
            "id": "829b1b00-2f84-484b-82b2-f28fc2ebb1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7842
        },
        {
            "id": "598e4443-2b4c-4be9-a64c-a481ea1ded98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7843
        },
        {
            "id": "a0212862-a788-4148-996d-83e73a6a2ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7844
        },
        {
            "id": "acbb7766-291f-4805-8a2b-090b40636583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7845
        },
        {
            "id": "661654a4-91a9-4336-b853-0201c0ee2cbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7846
        },
        {
            "id": "82d81180-4ef9-47b7-a483-b7136b12ab71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7848
        },
        {
            "id": "fd63419d-c129-4cba-9a31-1154e60b5baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7849
        },
        {
            "id": "d1d010be-b5c7-4b57-aad0-8130e2d48fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7850
        },
        {
            "id": "cf36276e-748e-4c6c-b7fb-e2370dfacd31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7851
        },
        {
            "id": "ac02284e-18c0-45b1-9605-9be2a8e7d744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7852
        },
        {
            "id": "d8d4ac3f-b878-46ef-8382-9c0d3480445e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7853
        },
        {
            "id": "1b060522-a48e-456c-bd74-b86f527f3a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7854
        },
        {
            "id": "d65141c6-3199-4f06-b117-e0cbb8205d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7855
        },
        {
            "id": "48133c0b-48a2-4f85-b135-66cdc8003295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7856
        },
        {
            "id": "a4982187-2c8d-4e91-bd1b-dd97e7121eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7857
        },
        {
            "id": "179eec1b-02e4-412c-8638-3ec53c76dceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7858
        },
        {
            "id": "70f6fa62-d0e2-4ea7-a696-0073ba510159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7859
        },
        {
            "id": "afb27c5f-628e-490f-b386-764a42f95d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7860
        },
        {
            "id": "4fbc4608-16b8-4fa6-8023-1461e495370b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7861
        },
        {
            "id": "2ddb91cb-dd94-48e5-b996-81b520c80089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7862
        },
        {
            "id": "b752ead7-5efe-420e-bcb9-dab013efe8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7863
        },
        {
            "id": "add1ffaf-3874-4d01-bbbf-8cb33c5d7cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7865
        },
        {
            "id": "53c69469-6a37-4acd-a2ee-9706c8dad820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7867
        },
        {
            "id": "b64b2159-da25-40cf-b1a8-c0b98cfb637d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7869
        },
        {
            "id": "9d81c7f6-4c18-4992-a4df-0f8f5694a5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7871
        },
        {
            "id": "f0f5a8b6-bbf2-4cde-96b8-a6e6cb8eedbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7875
        },
        {
            "id": "2f86c27b-9e76-4846-a1a5-1265620fe382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7877
        },
        {
            "id": "4ad6bebe-5a07-48ef-a1ae-4e94f4d6742a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7879
        },
        {
            "id": "63f90107-57f2-45ba-a854-07d067ac4948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7885
        },
        {
            "id": "a1fc68b1-2304-451a-a88d-6f079ef56dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7887
        },
        {
            "id": "1fb5f87d-f4b8-431d-a50f-6276b8a217b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7889
        },
        {
            "id": "2b24c9c9-d0f8-4bfa-8bba-d6aa2ab18a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7893
        },
        {
            "id": "2d6a502f-aaf7-4078-aaad-e4ad9ed84422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7895
        },
        {
            "id": "583fbadf-3883-47c5-aca5-229506a890c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7897
        },
        {
            "id": "cad98401-2061-41d1-9e38-8ceae7a10ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7899
        },
        {
            "id": "2bb0e80c-78ed-4639-9a89-8e98e65a43e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7901
        },
        {
            "id": "3cbc9dab-f123-4263-ab44-5d01c83b277c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7903
        },
        {
            "id": "fa1eb850-c530-43e5-991f-5a1eae72872a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7905
        },
        {
            "id": "28249d9a-381c-42bc-96e6-bf5ef85a9a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 7907
        },
        {
            "id": "9c4c32e2-bf2a-488f-8c93-743472d41b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7909
        },
        {
            "id": "431644d2-860f-458a-be77-d5831353d5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7911
        },
        {
            "id": "15ecbee2-8c1a-49fb-bd99-31bc4a062b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7913
        },
        {
            "id": "ca663765-7ae4-4e4c-a176-743ff86959d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7915
        },
        {
            "id": "bd6de28d-f911-4c08-827d-5fc0044ba11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7917
        },
        {
            "id": "503df6b1-7dd2-4226-830a-e30253711387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7919
        },
        {
            "id": "d441ab12-7141-42d8-b285-88a9d229833c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7921
        },
        {
            "id": "9674d146-7f5a-41ea-b772-40a5e4786219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7922
        },
        {
            "id": "3bae1b57-93e8-496b-a2eb-e14d153f4dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7924
        },
        {
            "id": "bda93355-d4e2-454c-a4f0-36be519eac87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7926
        },
        {
            "id": "c7bf3ecb-d51d-4eaf-b5e1-1ea24620247e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7928
        },
        {
            "id": "29091976-2c3e-4e97-95dd-a50349c63fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 65
        },
        {
            "id": "97c5ae57-abfd-466a-bee6-16752386cfca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "ab6c0758-9864-4135-a834-9de5b1edfe66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 86
        },
        {
            "id": "7894dda5-b936-46e7-ae90-e86e0f23f6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 87
        },
        {
            "id": "8c7a989d-dc9f-489c-9654-9a84b70f5c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 89
        },
        {
            "id": "ca674a4e-9c35-4e73-837b-72a6d5704f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 97
        },
        {
            "id": "8fab8868-85e2-4d72-bd17-c31d12f91c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 99
        },
        {
            "id": "dd2f931c-2542-4d65-a6be-d9256da5d0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 100
        },
        {
            "id": "c34b47b4-bb51-4845-b820-9eb9b34a48b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 101
        },
        {
            "id": "a8780013-6878-41cf-94ca-5cd14aefce57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 103
        },
        {
            "id": "00958718-be6c-4a16-8f20-22c40029f823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 109
        },
        {
            "id": "7a8a7ea9-a3ed-40ac-a822-35c9c8242a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 110
        },
        {
            "id": "6fb902c7-c485-4661-beea-626334e5238c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 111
        },
        {
            "id": "c7ffc8ad-904b-49fd-91be-f5e951531992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 112
        },
        {
            "id": "75828489-0f5f-40db-b08e-e553e19df7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 113
        },
        {
            "id": "f2223467-6823-4f20-9ce5-4e2966c11fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 114
        },
        {
            "id": "0dfd45ad-7b8c-4b3c-a9da-6d3b93261732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 115
        },
        {
            "id": "3e1ade19-3c8c-4d64-8684-bfde2c731924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 117
        },
        {
            "id": "bdeeeb2b-695e-486c-a7e0-035200d1d48a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 192
        },
        {
            "id": "17a241cd-8c3c-4cc0-b4e1-174056b9e41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 193
        },
        {
            "id": "f65e6724-e922-45ff-8637-fc1a5c550ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 194
        },
        {
            "id": "e95180b8-d919-41d7-922d-f3e7141e8cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 195
        },
        {
            "id": "03c4fbc1-a578-4f02-a272-ff05f8e34067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 196
        },
        {
            "id": "228472f1-d384-4fbd-80c3-df2cc8d7400d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 197
        },
        {
            "id": "ba52079d-4bac-4d0f-9787-4fcc74710fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 221
        },
        {
            "id": "adb61821-0449-440a-a81d-da788bbf6004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 224
        },
        {
            "id": "d8dfd65b-993e-466a-b875-ff521535972d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 225
        },
        {
            "id": "674da826-5265-4adb-9677-ba049b7f47c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 226
        },
        {
            "id": "acf8770e-0863-4aaa-989c-27b8b6ce6897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 227
        },
        {
            "id": "48a7e329-5878-4103-95a7-a25fa224c6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 228
        },
        {
            "id": "d7809d44-4af5-4c7f-a393-7cca17505159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 229
        },
        {
            "id": "a50761b3-cfce-45c3-9938-ec749221af3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 230
        },
        {
            "id": "0e52601e-e260-460f-a335-aa5d9cc293b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 231
        },
        {
            "id": "83953700-761b-48d3-8018-62c1de547992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 232
        },
        {
            "id": "6367a1d9-c286-44e1-aa03-41cba33ac8ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 233
        },
        {
            "id": "5ccf2b5e-d001-4055-876b-55637a3346e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 234
        },
        {
            "id": "222f9051-c006-412a-87f5-2697761f1412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 235
        },
        {
            "id": "dc2a8195-2065-45c5-beaf-720626b47b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 242
        },
        {
            "id": "ef985f75-648a-4b39-a251-73a8b726dbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 243
        },
        {
            "id": "bc137c6f-8c81-4273-99ae-4acc2578e762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 244
        },
        {
            "id": "7730ddac-6fc0-4c5f-8d89-acd7de741b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 245
        },
        {
            "id": "f3ef25e3-5341-4cfb-9ecb-e0cfff863932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 246
        },
        {
            "id": "f7ceef90-2318-45e7-ac85-fbcd16edc3b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 248
        },
        {
            "id": "d278a501-5bcd-4b5a-9a88-006ab5af9ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 249
        },
        {
            "id": "21c4551f-8b12-4434-b6a4-03a057ad0ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 250
        },
        {
            "id": "193a6524-afa4-49db-9b73-5399a510885a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 251
        },
        {
            "id": "12e9efd8-21a6-4c68-b2eb-a28fa7122252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 252
        },
        {
            "id": "0e973a6d-ec18-4b15-952c-583fe5d18647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 256
        },
        {
            "id": "a34346f6-11d3-4ecd-8870-78337a8bf6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 257
        },
        {
            "id": "b2756486-e561-48a5-83bf-1620c8a23041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 258
        },
        {
            "id": "31eb10e2-7305-466d-8979-f6276452a953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 259
        },
        {
            "id": "7d3d031a-59c7-41e8-9ffd-4f71ef65c2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 260
        },
        {
            "id": "db1644fe-fcab-4b91-bcc6-53b132018a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 261
        },
        {
            "id": "a6349431-bc1a-4bbd-951f-5c6937c47ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 263
        },
        {
            "id": "b6286717-cfbe-470e-b01e-a93828e81069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 265
        },
        {
            "id": "91181471-7787-404d-830a-59b6d20f6422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 267
        },
        {
            "id": "9e93668c-3e02-4026-80ce-4f83d1ed4c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 269
        },
        {
            "id": "fe263151-61fa-4766-a7f2-692f6ae590f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 271
        },
        {
            "id": "b3ccffe4-03cf-42b9-8278-6ba0be76e3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 273
        },
        {
            "id": "745584d3-ec25-4a4c-8749-85a672456a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 275
        },
        {
            "id": "44e4d65f-e906-4c8d-b48f-f882a307bebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 277
        },
        {
            "id": "1dc26266-b269-4b26-adfa-628aa2678247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 279
        },
        {
            "id": "804a7e99-e7cf-453f-a602-ee2f73e4e4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 281
        },
        {
            "id": "1cf3dc73-ecd5-4e3d-90c1-e16ac55697d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 283
        },
        {
            "id": "2dc8d3f3-35de-449d-8960-82c377f47a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 285
        },
        {
            "id": "8d41b2b5-d3b1-4e4a-8e3e-44dad1d39335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 287
        },
        {
            "id": "80274f91-7b1a-48d2-8a1c-5b2411fbdb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 289
        },
        {
            "id": "01cc90f7-cf2c-45d2-ae86-5a2c6ac09073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 291
        },
        {
            "id": "549ea887-017d-4aaf-8485-bae7675d7aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 312
        },
        {
            "id": "0056e886-96dd-4b62-b7cd-20ba74e3956f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 324
        },
        {
            "id": "06506b98-9004-4679-bfba-19420209d136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 326
        },
        {
            "id": "a8e06877-6847-4146-9780-e280fc140163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 331
        },
        {
            "id": "34e43365-4294-4d09-ae42-df1f796de2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 333
        },
        {
            "id": "cdea3c88-192d-4b2d-a517-a0ca718ddffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 335
        },
        {
            "id": "b2a149cf-8b0a-440a-90ee-570ad1bb7f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 337
        },
        {
            "id": "ef299b6e-511a-409f-b560-87de527f765c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 339
        },
        {
            "id": "9a996608-58a2-41b3-af15-cb21557002ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 341
        },
        {
            "id": "b45ebe90-865c-48d5-9249-b3abc928b64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 343
        },
        {
            "id": "ab0f65b0-51be-4f96-8427-dfab5973629a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 347
        },
        {
            "id": "f3c4e216-47e3-4950-9d6d-ec78f671a2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 351
        },
        {
            "id": "29954c33-9890-48c8-8382-8d2a8fa61ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 354
        },
        {
            "id": "4e86f8e7-d69c-4b52-b6b8-dfc907b68d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 356
        },
        {
            "id": "e948ab88-75bc-499d-9f36-10a74d9966fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 361
        },
        {
            "id": "c8554d52-6fb1-47bb-92e1-3a51d0a8ffc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 363
        },
        {
            "id": "4d9645e0-9dcc-4633-abcc-2aaaf4297283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 365
        },
        {
            "id": "9f3e9a14-d7f7-4348-b061-082309d50c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 367
        },
        {
            "id": "8a4a6aa9-4fb0-4ccb-9e43-ce0b81816afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 369
        },
        {
            "id": "c0d6b4f0-d289-4c5e-be87-c37598ba651f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 371
        },
        {
            "id": "b665d3e2-403d-4d5a-9f05-6615fba9ea4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 372
        },
        {
            "id": "162be9c7-6b54-429c-b599-3ac63dc2da0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 374
        },
        {
            "id": "f178186b-5b08-4c7f-a21b-ed4c4ab6441b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 376
        },
        {
            "id": "50bea009-2f25-41e4-b6ef-67c87d601326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 417
        },
        {
            "id": "2f87c449-6268-4786-8e41-318178533b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 432
        },
        {
            "id": "25cfd9c2-04c9-4953-8a9d-4a12b0df5efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 506
        },
        {
            "id": "47f36988-9799-4e3f-8a1d-cd2eb81d6153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 507
        },
        {
            "id": "8e63e367-6840-4190-823a-a395e683bf16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 509
        },
        {
            "id": "a265f25b-2600-4053-a814-2e288a30bd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 511
        },
        {
            "id": "69fa296b-5de8-43ab-9a09-374ff4a04d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 537
        },
        {
            "id": "6a6963ec-6ca6-44e6-ac91-342a2abfe9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 538
        },
        {
            "id": "39a58003-056f-455d-8646-21b2f133f7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 902
        },
        {
            "id": "8bbdbc6e-4161-424e-b77c-f79167094539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 913
        },
        {
            "id": "2feba43f-1e17-4712-bffa-136b9cd6e9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 916
        },
        {
            "id": "bfe6da69-28eb-439c-891e-c20422c33a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 923
        },
        {
            "id": "9d22c5d1-902f-4c8b-aadc-3914428c7430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 940
        },
        {
            "id": "53e81be6-d96a-4dda-9932-b07f65ee7e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 941
        },
        {
            "id": "5976a29e-34e2-4a60-94c0-c3d958161994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 942
        },
        {
            "id": "1f655970-607b-4346-b507-dab5d2a40c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 945
        },
        {
            "id": "12ccab47-1e3a-46a7-8a4c-49760fbd3540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 948
        },
        {
            "id": "f6d7bba8-17e0-427e-b6ec-4b01e55667a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 949
        },
        {
            "id": "8d4a53de-bc7b-45b0-a771-33a8988ec354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 950
        },
        {
            "id": "acbb99e3-985e-4652-b95a-0742596230ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 951
        },
        {
            "id": "c0122e9e-da02-42b0-8345-b05395f8d73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 954
        },
        {
            "id": "2889820d-40c3-496f-b50a-654a5acd05cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 956
        },
        {
            "id": "7ed72e47-7c7d-4d9a-8b0a-d8ffd2b48c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 959
        },
        {
            "id": "ecc92361-f421-4385-b552-c7bdaae2db17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 961
        },
        {
            "id": "b204eb69-d021-41ea-9085-adbdec74a4e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 962
        },
        {
            "id": "32804f92-e4a3-43fe-8617-334a18c22377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 963
        },
        {
            "id": "b514bad1-f754-4feb-907f-7d12b8110a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 966
        },
        {
            "id": "5229c05e-be72-42a5-8cf7-676b072297c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 972
        },
        {
            "id": "24dbc566-1d2e-4fd3-8a33-a8d1aa71f626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1033
        },
        {
            "id": "65bcd1a0-d09a-4cb2-b133-b9801d986c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1040
        },
        {
            "id": "a1f9923f-855f-46cc-8beb-01503ad1a2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1044
        },
        {
            "id": "04b194bb-5153-4bd0-b812-585a709b58f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1051
        },
        {
            "id": "18f2dd12-e7c3-4588-97f3-2bfeb082cff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1072
        },
        {
            "id": "3cfdf0f8-334b-4ff3-b1b6-f9829bd9ebb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1076
        },
        {
            "id": "9ec4fd19-9ebe-4d48-9b51-cc9e2b9646b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1077
        },
        {
            "id": "5d42ee7b-cee2-4ba4-950e-e0b06255e56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1083
        },
        {
            "id": "bd0f0d7c-6317-4513-8859-f268cbb111ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1086
        },
        {
            "id": "b5bed4cb-62c3-4c09-b841-3dc7b16a2681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1089
        },
        {
            "id": "359ed3cd-7a45-4760-8902-0b2ad6745259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1092
        },
        {
            "id": "1616a85c-d711-449a-baf5-49e872699e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1104
        },
        {
            "id": "70eafeab-e1d2-4824-981e-f1c46b0d1c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1105
        },
        {
            "id": "dbaffe77-9a89-4129-9318-8be73ddd915c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1108
        },
        {
            "id": "41ba1f31-5bc4-4a42-91cb-82fe629ada1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1109
        },
        {
            "id": "8946c5da-a8ac-4451-a81d-239d796f2d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1113
        },
        {
            "id": "a92c673b-46f5-4062-89a9-cc5267d25f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1126
        },
        {
            "id": "41f51aa9-d281-4921-a3c9-9a94b3e5b47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1127
        },
        {
            "id": "a83847f4-15ef-4870-b524-fde3451e098f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1137
        },
        {
            "id": "f7c7bf6f-9575-4a07-9171-a6576b97b89a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1139
        },
        {
            "id": "7223afcb-f45d-4ff6-98a4-fb3f21856d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1145
        },
        {
            "id": "fa033144-06d6-42cc-bb1d-48b8598da31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1147
        },
        {
            "id": "7a85c577-0682-4549-9603-e56e457490e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1149
        },
        {
            "id": "38f3aed3-46f1-4df5-af7b-85c3b150be77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1153
        },
        {
            "id": "b31d8211-4bce-4505-bbc2-269f687493bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1193
        },
        {
            "id": "9cf68fbb-5528-441d-a380-f68b5b2f151e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1195
        },
        {
            "id": "efc7a672-2202-4b13-920f-84072b484fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1221
        },
        {
            "id": "1625133e-bd8c-450e-9508-00f79a78c355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1222
        },
        {
            "id": "ee900cfa-4bac-4579-9171-2b2ca274a880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1232
        },
        {
            "id": "ca38a1c8-fd0c-489b-8502-3e77433d837d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1233
        },
        {
            "id": "83870812-2be8-46a2-9f83-fc62f72845c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1234
        },
        {
            "id": "90165e40-4df4-49cc-b1e6-40b5d5986788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1235
        },
        {
            "id": "b75e91e7-5d57-4f41-b02e-ef43e92ea18d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1236
        },
        {
            "id": "a51cc1db-48a5-4eea-8222-3e8af356b3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1237
        },
        {
            "id": "dac55e63-bd09-4476-9096-6063415dafb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1239
        },
        {
            "id": "c1db4f1a-7ec7-4ff7-9ead-265c88f6ea1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1241
        },
        {
            "id": "7bcbf85b-aac8-4711-99b4-5ec67fdf9721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1243
        },
        {
            "id": "1f9c507a-69c9-498b-ad13-25618d4f2795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1255
        },
        {
            "id": "ba49e6d7-d2b6-43cd-87b9-c43fcbc5b02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1257
        },
        {
            "id": "1c21ad51-0650-4bb7-8bd8-dba16e86a2b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1259
        },
        {
            "id": "0702664f-2411-4ee3-b100-c52c2445fd3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1280
        },
        {
            "id": "d2b9cbb7-8719-4946-942a-2c2524438d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1281
        },
        {
            "id": "edc5e922-9641-429e-a744-30f3a899ea7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1282
        },
        {
            "id": "3ff75e9a-2126-46e7-848a-a733b1f017f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1283
        },
        {
            "id": "9aec7142-2464-40cd-9e94-e11c21b0ce33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1288
        },
        {
            "id": "cb379a03-cd32-445c-9899-9630787fd516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1289
        },
        {
            "id": "2694f3cc-e4b7-4edf-8143-ba5db581e87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1293
        },
        {
            "id": "a73a8ec1-9109-4dc8-9f2d-a699b3afd029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1297
        },
        {
            "id": "8f8ee8f0-2c13-47aa-9a8d-b749b8c68263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1298
        },
        {
            "id": "c9d273b2-0e17-4fd4-a0a7-f1ec3bbe72da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 1299
        },
        {
            "id": "328eba07-4dcf-4a97-aef7-39aaa662b935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7680
        },
        {
            "id": "35914421-1fe1-4c6b-820b-c1056cf8d352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7681
        },
        {
            "id": "d4a3d006-f65d-4fbf-8c8c-abd2ffc50008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7743
        },
        {
            "id": "b3dcc8b7-665d-42aa-a7c6-7a6d61004281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7808
        },
        {
            "id": "f8a913b9-dc2a-4120-ad34-f0b57b10ab9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7810
        },
        {
            "id": "48e9fb42-c075-46a6-8303-c5e30f99d04d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7812
        },
        {
            "id": "cd63eb71-0144-4cc0-bcf9-d6709ab1b783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7840
        },
        {
            "id": "2114416e-f742-489b-8f8f-8f35a307bf38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7841
        },
        {
            "id": "0d669631-b4f7-4d03-8661-e00fa697bf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7842
        },
        {
            "id": "69686b5e-0d06-4db9-a42e-1a4b37d4b534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7843
        },
        {
            "id": "67e12f45-b997-4a63-bf7e-3eb893008750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7844
        },
        {
            "id": "b416248c-5b68-4b3a-a18c-c9fdfe766951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7845
        },
        {
            "id": "c850347b-73c8-4929-8990-b5b9a49a9c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7846
        },
        {
            "id": "ffd7a307-4857-4f00-ab57-982fdf800361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7848
        },
        {
            "id": "ecf0c30d-8055-489f-80cd-c591d14a42cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7849
        },
        {
            "id": "ba11dedc-b63f-4342-90c5-9f98000bb9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7850
        },
        {
            "id": "a03fde2e-2fdb-49c2-bbf0-9b60f558b3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7851
        },
        {
            "id": "549fddd5-4da0-40e4-8dd3-8aa93054a45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7852
        },
        {
            "id": "94d9f9bd-2506-4478-9bb4-abc12748e255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7853
        },
        {
            "id": "b1713021-749a-44de-bd27-048a29b669cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7854
        },
        {
            "id": "eec2e2a4-afc4-4954-9e8a-0861fab2d95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7855
        },
        {
            "id": "bbdadc4b-3536-4226-8ac6-a298600c4d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7856
        },
        {
            "id": "1c984837-30c8-4b40-9a5e-b42198e4265a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7857
        },
        {
            "id": "7b1087c4-3b71-4757-b198-17e1e213bd70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7858
        },
        {
            "id": "0f36d703-3367-4743-93dd-add77d31bf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7859
        },
        {
            "id": "a7af7fe3-81d8-4f43-b3fd-b5bce0274cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7860
        },
        {
            "id": "7851eacc-9eca-4fdd-be12-665090160873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7861
        },
        {
            "id": "76d48335-1635-4edb-a5ac-3e9c330caa5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7862
        },
        {
            "id": "490a1eae-0e61-41f1-8e62-5777f3b9e47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7863
        },
        {
            "id": "3494e866-c151-48a3-90ef-a7dfc223b2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7865
        },
        {
            "id": "69706874-e08a-4603-82a7-3cf43f23b808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7867
        },
        {
            "id": "fa4d3594-6f64-4931-ab91-53f55d4b52f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7869
        },
        {
            "id": "6c7803af-7615-487f-a0ef-62f41ca3fe45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7871
        },
        {
            "id": "3bf07a4d-b1db-487e-b8bc-453ac5b9e0f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7875
        },
        {
            "id": "ee300754-abd1-4d93-b448-4056110dd10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7877
        },
        {
            "id": "0f7f66c2-5e23-4eaf-8b06-bd4da4648a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7879
        },
        {
            "id": "fe57d1aa-eb41-42b6-9291-c5b9bed00c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7885
        },
        {
            "id": "57ea6479-b5d1-4325-9742-c8f3d190ba50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7887
        },
        {
            "id": "74ca0f11-0655-467a-9215-1fc1d636e05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7889
        },
        {
            "id": "a62da8a3-4424-48ab-b30e-8662f04603a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7893
        },
        {
            "id": "08e2d142-0228-43d7-8b3d-439864780edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7895
        },
        {
            "id": "f14c656b-563b-4c47-bdf4-4097a53fe79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7897
        },
        {
            "id": "04cc558f-de96-4703-8459-d3dcf8107046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7899
        },
        {
            "id": "e11fff2b-13d4-40d4-9260-20a866417aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7901
        },
        {
            "id": "dc63fcb6-0860-480a-88f7-d4e0e2e3a257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7903
        },
        {
            "id": "0dbae0df-dca5-4f03-9e2a-254177ea6e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7905
        },
        {
            "id": "7d45dceb-aec1-4629-8776-391f3c93e239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 7907
        },
        {
            "id": "9920a478-041f-4d5c-b212-78e4699cb4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7909
        },
        {
            "id": "71865202-2a76-4cd4-a196-3e9719da105c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7911
        },
        {
            "id": "b6f836e3-54e5-4f8a-8a1c-e6c4b6dcc7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7913
        },
        {
            "id": "5fdac912-681e-4f7f-9944-02483732c5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7915
        },
        {
            "id": "c218bb8c-2904-4cd1-bfa7-0193cedf3276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7917
        },
        {
            "id": "6bd09dee-696c-4bf6-938c-80daa46b108c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7919
        },
        {
            "id": "3ea8074f-e592-4b8c-b15b-888a412f995d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7921
        },
        {
            "id": "bd991d6f-0d76-469e-8167-d313bc644746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7922
        },
        {
            "id": "72e5d0c4-13b9-4c80-bc3d-18c9541a8e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7924
        },
        {
            "id": "46ff447c-92c3-4986-aac1-565e5a98acce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7926
        },
        {
            "id": "e4cdeb2b-fa59-4da4-b169-6f7181998876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7928
        },
        {
            "id": "fc5d5dd0-51b1-4fb2-84bd-543c6c2d9b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 40,
            "second": 74
        },
        {
            "id": "58170a61-169a-4e7d-bd98-ceee6aeefa37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 67
        },
        {
            "id": "a5c62e2c-58f1-4f11-9100-7c2b10732502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 71
        },
        {
            "id": "5a25b2e9-fc93-499d-a4f2-dcdc42f9f181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 79
        },
        {
            "id": "84e9997a-6577-41e7-88c8-1adce632d707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 81
        },
        {
            "id": "dbc1cda6-2449-4830-bcf4-66a3ae772a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 84
        },
        {
            "id": "bcd7bda0-45e7-4457-8c78-ab3b6767127f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "bb131917-3815-47f9-a459-8ef5702a2fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 86
        },
        {
            "id": "718a76fd-badf-4085-8d8f-ee9ea33a4116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 87
        },
        {
            "id": "b21df39b-32e2-47a1-969c-58d1056fc7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 89
        },
        {
            "id": "c4ed06b2-ed81-40c3-828c-9f1bb18751e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 199
        },
        {
            "id": "a447f749-1fcd-4418-ae46-1aae85fd7fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 210
        },
        {
            "id": "3c6f54a4-0dd0-4db5-abc3-d0bac4352975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 211
        },
        {
            "id": "b293919e-974c-4bfb-84bb-dade7b93c438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 212
        },
        {
            "id": "96f11857-0f50-4866-82f2-d15aa83dd64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 213
        },
        {
            "id": "f75cf6f7-f16e-4e87-8e85-812304b7a21b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 214
        },
        {
            "id": "e9523c6c-2c7f-4a9c-a8eb-8d1444ce9908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 216
        },
        {
            "id": "1e57d67b-ebf0-48b2-a0b0-7b087fd49c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 217
        },
        {
            "id": "7c79e0eb-5826-49bd-8eb3-79b09e1c244b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 218
        },
        {
            "id": "c9a8094f-836b-42d7-b889-04008ffc479d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 219
        },
        {
            "id": "b5465621-6bf3-4b94-8c26-5e30d6161a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 220
        },
        {
            "id": "1f7098c1-2f1d-40ba-9425-e6e327a39e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 221
        },
        {
            "id": "c22c07e5-6c86-4f74-9fec-dac896f10331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 262
        },
        {
            "id": "e6bde274-d13f-4c79-abd8-ae02324f53ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 264
        },
        {
            "id": "9c8e6fbb-6b62-4c65-b2dd-4b2deb7b8f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 266
        },
        {
            "id": "af499ebb-04ed-4179-a153-c423e59367d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 268
        },
        {
            "id": "c50fa43b-8a69-4fc7-9f5e-9e24268e44aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 284
        },
        {
            "id": "20a5ba43-f66f-45d9-8f64-86c6a6cf0c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 286
        },
        {
            "id": "75c6f491-6081-45a1-ba0d-1400dc65c2d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 288
        },
        {
            "id": "dc08314c-37ca-44cf-9d85-d8aa45a56e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 290
        },
        {
            "id": "be13fc23-558c-4de9-88d9-50202e07b96d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 332
        },
        {
            "id": "31c3ac43-7291-437a-9c9a-64f39e29e9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 334
        },
        {
            "id": "88475dec-3653-4f1d-865f-f864b5756571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 336
        },
        {
            "id": "35072473-5dca-46af-845d-71993d4fb917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 338
        },
        {
            "id": "5f0230e9-1aa1-4b7b-af99-23661e2fb534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 354
        },
        {
            "id": "a884ff76-4df0-4181-99e6-6ffceb947aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 356
        },
        {
            "id": "2a5ff1e3-376b-4330-902f-2ab3fce103ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 360
        },
        {
            "id": "ac8b341e-cfbb-4ebe-8ae9-9be4d092ef95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 362
        },
        {
            "id": "ecf9da06-205f-44c5-97fc-020f5c1716ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 364
        },
        {
            "id": "61b69cb4-4393-443e-9c86-843f2098a8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 366
        },
        {
            "id": "5f5f4c1f-6be6-40e0-bb6d-511f282f4e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 368
        },
        {
            "id": "9b31065e-dc1c-4106-8996-347db882f5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 370
        },
        {
            "id": "1a00ce27-9302-4b21-b993-4011930311aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 372
        },
        {
            "id": "1f730c3d-3609-4720-84ad-ed54a0d99c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 374
        },
        {
            "id": "d3ae8208-8451-40b9-938d-f500e332e5f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 376
        },
        {
            "id": "b0485574-5ca9-4714-8b01-cecd138e0f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 416
        },
        {
            "id": "37fabdd7-4840-48d8-b0a5-aa5d6eb12842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 431
        },
        {
            "id": "3eba2ec2-d1b6-44e1-bb55-cbbc378dbe6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 510
        },
        {
            "id": "5d044fb4-6080-491f-aae5-37406c4d70e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 538
        },
        {
            "id": "ed04d59b-3f7f-471b-afc8-21b865ae1e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 920
        },
        {
            "id": "bf06d27f-edee-49fa-8e8f-c79b333728cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 927
        },
        {
            "id": "d82e00ff-98d7-4836-802c-ee291ffd85b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 932
        },
        {
            "id": "303e654c-64ff-45c0-addf-9db6e050ae89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 933
        },
        {
            "id": "45bac5e3-c863-4c77-bb15-90d5e4b09802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 934
        },
        {
            "id": "8bf157a5-0ca7-4601-828f-6bd33b5f2408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 936
        },
        {
            "id": "6f82c776-ca78-4ebd-bbc1-0196cd549262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 939
        },
        {
            "id": "2439da85-a9de-479d-902c-b89b14ca58ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 952
        },
        {
            "id": "22fde281-825b-4972-a1e0-c8f06371a914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 978
        },
        {
            "id": "858edfe9-5402-439a-a327-e765d8ef472f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1026
        },
        {
            "id": "e6182b22-dd35-49e3-8fa2-f5200344bf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1028
        },
        {
            "id": "9b72bcaf-0b50-4b27-ae6c-8a0766a3d3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1035
        },
        {
            "id": "657fc0ca-234c-475a-b69c-e642a9f4344e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1054
        },
        {
            "id": "e3d5da95-d319-467e-85c9-e2884a518bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1057
        },
        {
            "id": "5a4238b7-ac56-40fc-a674-9646dbe4d443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1058
        },
        {
            "id": "3d605137-231f-4649-831b-b41d13a0a235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1060
        },
        {
            "id": "b496ad0b-ad6c-4ab7-a8ec-b73fc2e766b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 1063
        },
        {
            "id": "49152507-6ba7-4be2-a41c-d144d70b98ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1066
        },
        {
            "id": "06650dea-7da1-4dd8-9eaa-29d81acfae36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1090
        },
        {
            "id": "cb6f97e6-4d6a-4a83-9d11-0e9cf5571f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1095
        },
        {
            "id": "e66f56cb-8506-4b77-ade7-be63ebb97e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1098
        },
        {
            "id": "a6c358a9-b577-4124-8025-99fa5b1eab63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1120
        },
        {
            "id": "aa0c9d96-5f35-4590-b79f-735b867f6702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 1136
        },
        {
            "id": "c1c9f27b-ce60-4e01-81d9-7f4256500860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1138
        },
        {
            "id": "de5dd4f6-46d2-4bbe-ac6b-20bd6b58a994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1140
        },
        {
            "id": "fa159fb1-b6a3-4f1a-b74a-3793cff9f21a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1142
        },
        {
            "id": "4c689c49-e620-4b50-add8-df5b9a604b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1144
        },
        {
            "id": "bccddac6-2a60-4ee4-b23c-c95f8055d46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1146
        },
        {
            "id": "9b0a3dd4-6490-47e5-a7eb-f0c1c6a1227d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1148
        },
        {
            "id": "1e9d5161-b7c5-4cc2-b8cf-6332a1daaf4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1150
        },
        {
            "id": "33ac2ab9-d0e9-4338-afce-baa7ff76dac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1152
        },
        {
            "id": "cf052088-78bc-4e00-84a4-c7a966025563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1184
        },
        {
            "id": "6b0a98f4-93fe-41e9-8dbb-694df651db0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1185
        },
        {
            "id": "6860b58e-79a7-41b3-8b63-3eb53c7c52a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1192
        },
        {
            "id": "2a81a4bf-d332-497c-92a3-34a7aaf3a891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1194
        },
        {
            "id": "2701deae-c103-448c-9ec2-8874785cd3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1196
        },
        {
            "id": "631da21b-dd53-4382-adee-cfcb4d8e7acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1197
        },
        {
            "id": "35a402c2-6176-49eb-a55c-9d50259797f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1198
        },
        {
            "id": "bb8c31d9-03ae-44c8-b256-9232bdd2dfb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1200
        },
        {
            "id": "7a0cf2aa-576c-47e8-9350-a6bb1e42adbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1204
        },
        {
            "id": "771c2f48-4eba-420f-8dae-576f20339a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1205
        },
        {
            "id": "3e95c4d8-6a5b-4cf5-97a6-36a0cb2eb10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 1206
        },
        {
            "id": "8a130244-d097-4aaa-ba78-4f7a256f023c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1207
        },
        {
            "id": "9a06d5f0-5079-46d7-8c14-86242f773874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 1208
        },
        {
            "id": "72ed293d-3f0a-4e26-8cdd-e6b980f15cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1209
        },
        {
            "id": "990619a6-aa48-437a-87e7-26d0215610d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1212
        },
        {
            "id": "e6475300-70c4-456f-b39a-e91ae731dfbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1214
        },
        {
            "id": "41240461-7fd5-4118-98d5-83ef023fa87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 1227
        },
        {
            "id": "20108572-b055-4e4f-9d9f-2b7bb254a662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1228
        },
        {
            "id": "a3002687-b6cd-4236-8446-8c167fa0ba36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1254
        },
        {
            "id": "06fe37c4-146a-4335-9af5-4e14b77f6602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1256
        },
        {
            "id": "4095af78-d7a2-4a2b-b906-7eab29851b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1258
        },
        {
            "id": "f20a8f5d-7572-4330-80f9-a19b9071a21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 1268
        },
        {
            "id": "86174991-2628-4a72-a65e-0a6248db3cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1269
        },
        {
            "id": "552500e1-7a46-4f68-bbb3-86a3fb5955ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1284
        },
        {
            "id": "5938a62f-232a-48f2-a6ed-0574ca09aed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1285
        },
        {
            "id": "ea09ce3f-806c-420a-ad62-e7164740742b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1286
        },
        {
            "id": "e944ac53-d0fe-4e16-b02b-9409b7319375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1287
        },
        {
            "id": "5d3c2734-baa7-4615-9f77-13a238ac476a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1292
        },
        {
            "id": "94b59488-cd45-4e17-8f17-584074c5c80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1294
        },
        {
            "id": "86ba93d5-a8e9-4ced-ac84-9435e4899547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1295
        },
        {
            "id": "b5d8a85f-7bfe-4db1-805b-8ce65004ad9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7808
        },
        {
            "id": "6806ef03-dcdf-404b-91e8-4c46d4ab39a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7810
        },
        {
            "id": "344a03d1-4e2a-4b77-bbae-4e459746ee2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7812
        },
        {
            "id": "de028851-4cf4-447e-9372-758e2c60535d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7884
        },
        {
            "id": "e790bf8a-6965-4c7c-892e-e78a254daeac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7886
        },
        {
            "id": "f7fd79bb-97cf-434d-adfa-b288da6120ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7888
        },
        {
            "id": "c2849515-f8af-47c4-b8fc-18d855394ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7890
        },
        {
            "id": "d6e345b2-52f1-482d-b641-738036f945de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7892
        },
        {
            "id": "4d6f5669-c3cc-47b1-bf95-dbf4822c6b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7894
        },
        {
            "id": "8cc6d50f-8383-4be3-a87e-0a317cfdd08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7896
        },
        {
            "id": "c7e485f7-1e75-4344-885a-97b47347fcbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7898
        },
        {
            "id": "2e0eed50-3b7d-4a02-bc69-205e851bf50e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7900
        },
        {
            "id": "20d6abab-8582-4eff-8ba5-1e039d5ca747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7902
        },
        {
            "id": "91849adf-4b61-4f78-9957-344e3d33bd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7904
        },
        {
            "id": "b60f891d-8e7f-41e9-b8a6-f52e5dc75f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7906
        },
        {
            "id": "445ea619-e8a7-4128-b280-7aad0792826f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7908
        },
        {
            "id": "65014381-d96b-42ea-b61c-4a6bef52a363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7910
        },
        {
            "id": "c8e16474-606a-42c0-9e26-8fc365a7eb81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7912
        },
        {
            "id": "c962ac86-1544-4222-b659-6fe00cf34380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7914
        },
        {
            "id": "4dcb52b3-074e-48f3-8d36-e8ac9b0d713a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7916
        },
        {
            "id": "e153985a-aeeb-4279-9998-ad597bd546ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7918
        },
        {
            "id": "168bbefc-124e-4ec7-b493-8711e107eeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7920
        },
        {
            "id": "2a1cddca-45c9-4d32-a36e-5c891b71e65c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7922
        },
        {
            "id": "6ebf9a15-4cc4-480f-ba1a-da23c871e03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7924
        },
        {
            "id": "2c34e332-06cb-45e3-a7f0-c583a0d77695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7926
        },
        {
            "id": "cad0820d-6c28-48f6-ac0f-b0798cccf38e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 7928
        },
        {
            "id": "53153d7b-c874-4db8-aac9-a9d0dcb004c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 84
        },
        {
            "id": "a6b022cf-06c8-4fbe-9dd2-ad90ab452844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 354
        },
        {
            "id": "c2efe93a-4e0b-41e5-afcf-30e2e9970c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 356
        },
        {
            "id": "bfe0417c-f892-4530-a037-7a57a70f710c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 538
        },
        {
            "id": "6de82ac2-6d96-44a0-a636-6750b15628f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 932
        },
        {
            "id": "24965b99-f217-4dba-9cf9-ba3a7be0f984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1026
        },
        {
            "id": "de36a968-2c55-42a3-8ad7-7b4b753a1cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1035
        },
        {
            "id": "5a3a3c1c-1df5-4b37-a4bc-9679adebd335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1058
        },
        {
            "id": "6a2242ab-60f8-4f7a-8078-90ce24eb2630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1066
        },
        {
            "id": "c6eea2d0-f63d-4bf6-bc20-561ed10ae9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1090
        },
        {
            "id": "24cc2b7d-4df7-4cf3-89b6-acf5d1c5ad86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1098
        },
        {
            "id": "2c311822-7f77-459e-b8c5-175f34c7aa8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1184
        },
        {
            "id": "e306f834-2064-4e26-81e1-d30450401cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1185
        },
        {
            "id": "04d4e455-ec42-4864-8d5f-c5cc304bfe51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1196
        },
        {
            "id": "ec355b7a-923f-4ab4-add0-fc6b11c9a765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1197
        },
        {
            "id": "02505358-98b0-4f95-8589-5ff5241acbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1204
        },
        {
            "id": "ec66c523-1305-430d-86b0-98cf166a5067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1205
        },
        {
            "id": "d5fcdb9c-065f-4514-8275-38f31fad2026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 1294
        },
        {
            "id": "0e2565e4-44e3-4ff8-bafc-0fb36534a33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1295
        },
        {
            "id": "2a89d9ea-befb-4f16-98a7-2c951d8ff8ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 67
        },
        {
            "id": "f08c2e3c-dec8-46f7-a094-067f6335ca26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 71
        },
        {
            "id": "274f1b89-8c2e-4142-9950-ca87174640df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 79
        },
        {
            "id": "1f605e25-dce9-4250-816b-0e52305a4a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 81
        },
        {
            "id": "1fd0a7f2-0cef-42cb-b528-6ca946d7fa15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 84
        },
        {
            "id": "1bdf5d19-e2e3-49cd-93e6-d6b97b6ad754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "7382f586-4ae0-4d25-8e28-50b9a84fdead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 86
        },
        {
            "id": "21555bc4-d780-4f76-9d08-215a8792fc66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 87
        },
        {
            "id": "a8f4b50b-a309-4bb6-8a43-09a1b0bd5b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 89
        },
        {
            "id": "84954927-8ec1-45ec-b153-deff99be2281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 199
        },
        {
            "id": "5d6f04e5-2816-4203-ac7b-10801390683c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 210
        },
        {
            "id": "ae741c2a-b23f-49a7-a30b-919377fb87de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 211
        },
        {
            "id": "902cc095-d21b-47c1-8b4f-e804b9312eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 212
        },
        {
            "id": "362e83cf-58f3-406e-b616-5311b213ba11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 213
        },
        {
            "id": "47abd2cf-9d99-46a3-9fe2-5d5f095a56ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 214
        },
        {
            "id": "848123bd-ec22-45d3-8b90-246fc858e794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 216
        },
        {
            "id": "6042b7a4-8eac-48ab-9c14-dbed0225dd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 217
        },
        {
            "id": "d42c1a98-51ed-4104-a8db-53b35b087900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 218
        },
        {
            "id": "668c6821-b4e3-4568-9246-897dc36e2693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 219
        },
        {
            "id": "1c026795-b8b7-4ace-9740-d057214e941b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 220
        },
        {
            "id": "e51e97c4-a3fe-4891-ae0c-97a6276fc957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 221
        },
        {
            "id": "5ab0f050-a3d5-4faa-a6d3-21c82ffcb64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 262
        },
        {
            "id": "a2b93a67-0b2f-489c-93cb-e779f1c2ff0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 264
        },
        {
            "id": "fde8422e-efa8-4ae3-a2d8-c7e6029676f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 266
        },
        {
            "id": "49db05f3-aec3-4c00-8f5c-4e255bba9b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 268
        },
        {
            "id": "10688dc3-9e5c-4056-9ac9-567e4add9597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 284
        },
        {
            "id": "2fa103ae-2948-4f01-bf74-3438dbc537c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 286
        },
        {
            "id": "9bf3527f-0a7c-4daa-8986-9e2006203861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 288
        },
        {
            "id": "d0b9d0c0-181e-48ed-89be-663b66afd566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 290
        },
        {
            "id": "b1fe5259-2864-4d6e-81e1-359db1574cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 332
        },
        {
            "id": "85ad4410-669f-4951-bfa5-79707a117cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 334
        },
        {
            "id": "afac4e46-7fc6-475e-8f91-539c7f43622b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 336
        },
        {
            "id": "05f1ca32-13d8-4c14-9efd-2555ea61e0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 338
        },
        {
            "id": "b89c6ecd-4edd-4093-b0d2-5d4ea98d9a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 354
        },
        {
            "id": "f59817bd-9069-4a72-b455-5c3ea56ada0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 356
        },
        {
            "id": "f9b82a19-edf1-41fd-8530-54e77e15e99d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 360
        },
        {
            "id": "b6735a45-6177-472c-a06e-6768d3ab9773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 362
        },
        {
            "id": "039cc9f9-c6f5-4c4c-b8bf-2136c1c57d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 364
        },
        {
            "id": "d6a94cee-7aa3-4333-9bb3-c3fce6be8d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 366
        },
        {
            "id": "de665dc4-23ff-4c51-b499-ffbb026f7e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 368
        },
        {
            "id": "1ee8131c-29e0-4f38-9536-9f15184a4742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 370
        },
        {
            "id": "eca45af0-7380-4ad2-9749-9fa5d7ef5cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 372
        },
        {
            "id": "3b669f76-3972-417c-90fa-4b77157402ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 374
        },
        {
            "id": "fe500201-feb9-448d-a313-e615553ae03a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 376
        },
        {
            "id": "2c616a17-3e8f-4ace-bdb9-389edc6cab41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 416
        },
        {
            "id": "9e493473-ee3d-434e-81d1-2e5d12c66cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 431
        },
        {
            "id": "837ba0a3-e63f-48d5-9b1c-9a29a1b03b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 510
        },
        {
            "id": "dd51adcc-99e0-4276-a182-5c68fc661591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 538
        },
        {
            "id": "53ec9c8a-2d1b-45fe-a7ef-da91897e4b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 920
        },
        {
            "id": "f11e8bf0-7eb3-4dd7-a0dc-3e281863270b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 927
        },
        {
            "id": "e79a9133-a328-4586-b5f2-65ae6221b6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 932
        },
        {
            "id": "73d395eb-dd9b-473c-9457-6fd2a91704ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 933
        },
        {
            "id": "c70c7158-6018-4d2b-a35d-39522f5640c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 934
        },
        {
            "id": "a67aa701-f97b-4436-bf58-71808c26ae6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 936
        },
        {
            "id": "886ed8a7-fe7c-4dfb-8643-1a277ae9905c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 939
        },
        {
            "id": "00f79057-4baa-4f6b-9794-795f93884c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 952
        },
        {
            "id": "6fcbdc87-938d-490e-9b75-6ee333b5ace0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 978
        },
        {
            "id": "22cf1d44-f9dd-494d-a92b-8e5f72299b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1026
        },
        {
            "id": "7cf92120-6b01-41bd-964f-1a97756fb901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1028
        },
        {
            "id": "00550abd-c04f-42b9-bebd-54b725413881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1035
        },
        {
            "id": "ca5f9526-9b54-4b12-aa4b-4cde514a7532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1054
        },
        {
            "id": "02c4ab39-a6f6-4694-994b-88e690ad977e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1057
        },
        {
            "id": "0df7524a-ff7c-4f1e-b8e4-0c5e4a33bb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1058
        },
        {
            "id": "428ea7bb-c57e-4177-9943-4d8cd47089c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1060
        },
        {
            "id": "f36eebe3-b64e-4f03-b84b-0be195adbe88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 1063
        },
        {
            "id": "f7e4a6ee-1665-4b42-ab9f-baa3c60d32ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1066
        },
        {
            "id": "0c5e2e22-ddeb-4895-847e-938efdc443da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1090
        },
        {
            "id": "32952c53-066b-477c-a6b9-8ca8f944b237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1095
        },
        {
            "id": "cb80d434-4d5b-4e36-9cbc-5a6d992be309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1098
        },
        {
            "id": "665b9b76-584d-4f60-852f-3a2015be3d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1120
        },
        {
            "id": "e748c7c1-5323-4905-9a9d-efac815a1a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 1136
        },
        {
            "id": "41b27d5a-ab1d-43ab-bcdb-8ff273f7648e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1138
        },
        {
            "id": "18bbcc60-0625-49e7-82d9-5d6d078e268c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1140
        },
        {
            "id": "4f6a5975-64b9-4f70-bbc8-e1c97fde9016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1142
        },
        {
            "id": "c94708bb-43f4-40f3-8d6c-aba73bb187e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1144
        },
        {
            "id": "7fa0c147-c2a0-497e-8804-04d99badfe3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1146
        },
        {
            "id": "f0db366b-7824-416d-9ded-866bc20e84b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1148
        },
        {
            "id": "63295d71-0515-4f40-b3a7-4329b4145e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1150
        },
        {
            "id": "c2e80932-264f-4ccf-a6ac-f523121e05ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1152
        },
        {
            "id": "4d719fb1-f5d9-40ac-9b61-60e99b8c6934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1184
        },
        {
            "id": "479dc943-db96-4790-852f-95fc2cd17bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1185
        },
        {
            "id": "1e7ec246-30de-4e58-b2b0-cfea62ce0708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1192
        },
        {
            "id": "0bb06e88-f4c8-493b-a392-7c51072368ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1194
        },
        {
            "id": "2321bf0d-bc15-4a10-b89b-e96f3af0bfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1196
        },
        {
            "id": "6568e7ce-a8ca-406e-99a7-2a2ae01c6919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1197
        },
        {
            "id": "7c240f79-b6a1-4490-ae84-963e9c8d0131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1198
        },
        {
            "id": "342266dc-c054-490e-bb4c-bbd5f670ce05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1200
        },
        {
            "id": "d904e904-591c-4495-98e4-df9a8f8c4ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1204
        },
        {
            "id": "08b2e409-1481-4cff-97bf-5b6e9ca8f35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1205
        },
        {
            "id": "bebbbb87-f20e-46dc-af46-ba1f61adfe5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 1206
        },
        {
            "id": "c825d585-4966-47ee-a966-be4557aa41be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1207
        },
        {
            "id": "da9e6f9d-1a06-428a-a275-f392e3776586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 1208
        },
        {
            "id": "8fba1b9a-4527-4963-8d44-dddcdf1cf178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1209
        },
        {
            "id": "9703bc65-1f86-4be8-a746-527ffd3b1887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1212
        },
        {
            "id": "26bb970a-7e31-40c3-afce-d3838fd2ab8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1214
        },
        {
            "id": "99870603-bb84-4c8a-8472-b5f6a8f5df22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 1227
        },
        {
            "id": "27485c40-887b-4123-90b4-c72f5ffefff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1228
        },
        {
            "id": "e9524b60-977b-4db7-8af4-6a5eabe3f199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1254
        },
        {
            "id": "404dcb38-32fe-4bc0-b029-bafed319c55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1256
        },
        {
            "id": "56cb4958-6c33-4910-a4a3-98d8dca67bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1258
        },
        {
            "id": "207d7fdb-8205-4664-a53e-d90e7d542ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 1268
        },
        {
            "id": "d7ad8994-9d4e-4feb-b2fa-8df947644dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1269
        },
        {
            "id": "04e47202-87ee-43eb-9821-99f952a04499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1284
        },
        {
            "id": "ae74dca3-a016-42bb-bcbf-e07d07a2ef1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1285
        },
        {
            "id": "4d1ee1af-905b-4cd6-b8bb-c53a76d8526a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1286
        },
        {
            "id": "568d11a5-71fa-4d10-a400-884e738db74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1287
        },
        {
            "id": "78c61960-492b-4cf2-916e-77b468c1a60d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1292
        },
        {
            "id": "87797751-2843-45e1-8241-55f0157a3ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1294
        },
        {
            "id": "70001304-b338-4354-9d44-4d6ad7597789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1295
        },
        {
            "id": "6ba982d0-cbfd-4ff2-9212-849b07e141b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7808
        },
        {
            "id": "e6515385-34a2-4819-846d-4b2118ab2cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7810
        },
        {
            "id": "d454afd9-21c9-4ab1-a1e1-02687b313e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7812
        },
        {
            "id": "9776ec35-d9c6-4dec-8949-da238cd603c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7884
        },
        {
            "id": "062bf8b0-ac3b-45c2-a82f-3243af1b0ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7886
        },
        {
            "id": "22320622-44f5-43b2-a6c2-52b1444415fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7888
        },
        {
            "id": "3f62935b-986e-44ba-9a65-b50d4e735351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7890
        },
        {
            "id": "074b8ba6-ef0b-44f0-b5f8-88fe0b4b6bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7892
        },
        {
            "id": "52fd67f1-c4fc-4ab1-9998-f79d9dec1240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7894
        },
        {
            "id": "ed59ebf9-de0d-46b4-820c-d53c87f28192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7896
        },
        {
            "id": "cbffc1cd-7ab4-43d9-925a-b58b06afd08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7898
        },
        {
            "id": "0e0f86fb-b64e-4ee1-bc88-cb66138ea932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7900
        },
        {
            "id": "eb9e15af-7473-49d4-84bf-c88703f8fc38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7902
        },
        {
            "id": "9d34e423-4e08-4de4-a050-c78ee6259877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7904
        },
        {
            "id": "79276f5e-652d-4506-b061-e38550be15c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7906
        },
        {
            "id": "f633180b-c4ca-445d-b8b7-44b725db2812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7908
        },
        {
            "id": "036adf6f-5224-411c-bef1-d5133a4ed0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7910
        },
        {
            "id": "b5d351cb-3afb-4af7-bbe4-045241696029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7912
        },
        {
            "id": "a0e3790c-d02f-4699-8e02-165211b8b49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7914
        },
        {
            "id": "01324fc2-7f90-4381-85e3-9312a39ea240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7916
        },
        {
            "id": "a98e5cd8-4682-40ba-b91f-69441cd9e7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7918
        },
        {
            "id": "42b3eed2-097c-4220-a994-6229bbd401d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7920
        },
        {
            "id": "c6865c0d-3a49-4ace-b00d-e5a8fe52b2a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7922
        },
        {
            "id": "3955a873-8c4d-4978-b2f1-d2142d7d2cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7924
        },
        {
            "id": "aeaa02a6-45d3-408e-9252-5be7cd9063b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7926
        },
        {
            "id": "937d2a5f-a060-40ba-9b75-c482d05bee2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 7928
        },
        {
            "id": "088df2a2-2465-4880-88dc-d1ac36f2c19c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 34
        },
        {
            "id": "d02c4ffc-d7b7-4aa0-81f4-dd8edc36a7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 39
        },
        {
            "id": "a5199dca-2a6b-426f-a9c5-ac1add740282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "1ab97f81-726d-4b34-8b41-9e4c61e0dd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "994b2c2f-6a51-4021-80e5-4c8d05884c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 8,
            "first": 65,
            "second": 74
        },
        {
            "id": "5e3dc6d3-ead5-4264-aefa-86ee92e59fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "59f44759-65a4-4f96-867d-74e975343171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "b9ad7b93-3792-40f5-94fc-a01a5a457416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "035148a6-b17c-4739-85f4-9e2488262d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "9425d511-c500-44c7-9a0a-9f69bfa56162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "8ec71fce-9b9f-4af8-ab34-943a8a5d1d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "a7abb932-9f6f-4818-9820-6c74f4a211cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "00a2bae6-1515-4363-9ebf-9ba181da70b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "37e334d4-2c1b-4ddd-84c1-52693ad944c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "d8dc79f8-2c75-404a-a589-a7f68ab75f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "6bf8aa2b-c122-44d5-a458-5f21b1be7f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "347de549-38dd-479b-8fe8-b082d6605a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "b4e0ae4a-65fb-47cc-a407-ca82033e305d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "17cac87f-d9c8-4acc-b69f-f257468780b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 221
        },
        {
            "id": "e417a03a-733c-46a9-ab0e-874749c27520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "a6424080-362f-4207-9c8c-16aa2a061c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "b6cf02f2-9ddd-49f8-bb2c-e40821ea3013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "c469475b-72c5-42e1-b14c-f5dcfed7cde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "d7820cba-1095-4564-8221-a787af712944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "48908a0a-9a1d-463e-b4e4-efc8ae975fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "17256864-28d0-4ebd-8a25-5341be2aa9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "4197869e-9ac4-4458-accd-674d7f828380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "cd71287f-c008-4bd4-b3cd-a345c1a94dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "8f3fc034-4342-402a-a9ef-462d4cdb31ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "d7f432b5-d975-4ee9-b12c-bd7a15a5345c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "31da849c-69a7-485f-b5cc-c5f6e849992f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "0b57820b-b699-457f-be36-4a378602861f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 354
        },
        {
            "id": "2653436f-55ed-477e-adca-36eb1bcfb160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 356
        },
        {
            "id": "ebc5b49b-a41a-4dd7-913b-b40db9739134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 372
        },
        {
            "id": "e1033657-ae59-4a50-918f-2c093bf3f345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 374
        },
        {
            "id": "70070d47-8114-4570-a8e9-27dc10d150a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 376
        },
        {
            "id": "89e4264c-e577-4ff8-853a-31e75618bcd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "e3dc1957-0113-438c-b86e-62b0e6cf69e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "6a4acd4c-d621-4a2a-8e9a-3d3a9831f3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 538
        },
        {
            "id": "7f040179-5e00-4b6e-b3fe-a28077a57d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7808
        },
        {
            "id": "f3ecbe43-0aef-4603-b8e8-a5150a36b758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7810
        },
        {
            "id": "20c0d49c-2321-4e13-9a2a-665c17e525f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7812
        },
        {
            "id": "292938ff-eac0-4162-9591-e880774b9f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "d944e84b-50ca-48d2-99a1-72341c82e219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "0bdb53ba-83a2-4489-a09d-ed69f3b35730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "48ed5155-677e-48e6-b2b7-cb1b199b36ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "f6b507dc-d8c7-4024-a45d-c1b12222e3ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "a724339c-06dc-43ea-ae05-2070578468bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "0502deeb-fedf-47d3-aa87-a22ec8e83e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "0a68d196-07f0-4bfa-b8ea-f601e01ff77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "f2190f12-6889-41dc-8782-aa4902a61e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "f9f85ba9-135d-40dc-9662-b580493598c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "60e831e4-43c2-4451-8dbb-eaa587b2deb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "b9502c5b-5739-4361-8bab-54976a55bb1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "2d42cd70-abfd-4faf-bc09-6a40a0201680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 7922
        },
        {
            "id": "536e1a08-78f5-4e38-b011-de2725c7ed10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 7924
        },
        {
            "id": "d73b660a-774e-49b6-a91f-62e64f0a41be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 7926
        },
        {
            "id": "4097e549-7469-403a-982a-b674ca67ed7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 7928
        },
        {
            "id": "115d8a91-3c49-4e88-8326-82a163538b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "d1de4af2-4d7d-418d-a5e2-6d18fbbb6dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8221
        },
        {
            "id": "7d5bcda3-90f6-4e97-b479-9940c51622aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 44
        },
        {
            "id": "abd09273-1315-41e0-97d2-65eb47692d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 46
        },
        {
            "id": "d68149e9-61a8-4e92-bd44-f4bc98a83e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "1e0af08a-7865-4ca8-91a9-eb85e227fc77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "12e6ff29-8002-4a9b-90e2-552a1411c05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "55f8d1d1-dcca-4a94-93be-536b116d2372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "b4fb5b8b-c6df-41b6-b51d-bc53e2963ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "e206c417-48cd-4f1a-86b6-e5084eeab363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "c0c0a23a-2e16-4ee5-a328-1b2080a42316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "2f536251-c471-42c4-8643-c3e6f6209675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 192
        },
        {
            "id": "7fa9dbef-e30b-47e7-80b7-4321a4f8b7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 193
        },
        {
            "id": "3466e60b-fe91-47e1-8b70-6cf125d14d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 194
        },
        {
            "id": "da1b3744-9263-48b6-b460-6c1b5b74bf94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 195
        },
        {
            "id": "ddb549fd-894e-42b0-b94d-a09d1661d282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "fdac91ee-f51e-456f-97ec-9e90e8361582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "01127d3c-fc45-4c00-94c2-f3c60d28abd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "c88101df-cdf7-43e2-88e6-4b584c078887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 256
        },
        {
            "id": "2d330d51-a01d-4ead-a86a-707c077c069a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 258
        },
        {
            "id": "c2833859-dd93-4c7b-8335-69a9c7d618ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 260
        },
        {
            "id": "0318083d-2ebb-4904-b13b-a4a5986dd54e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 354
        },
        {
            "id": "9758e967-3d0f-4c4b-af4f-dd3169e22385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 356
        },
        {
            "id": "b912ae87-2ba3-4a92-8889-ba10567c8e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 372
        },
        {
            "id": "6b701eaf-612b-4bc0-aae0-5da622690daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 374
        },
        {
            "id": "14b4a336-4390-4572-ab05-17b3ce9b8ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "893c4665-f52a-4c3f-969b-23cc366138a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 377
        },
        {
            "id": "2b614520-f87c-49a3-8ca0-2e2842d9805c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 379
        },
        {
            "id": "0843787d-d860-48dd-bc03-266dc5382f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 381
        },
        {
            "id": "519bb602-25bc-4dc2-bc22-ac71894f490b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 506
        },
        {
            "id": "636cba6f-5d53-4bb2-8210-e2798dc826cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 538
        },
        {
            "id": "79185b9a-19a1-47a4-8832-0c4c72807ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 1029
        },
        {
            "id": "b6223ba6-cdbe-4322-900b-4552706d57ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7680
        },
        {
            "id": "d4249a20-68df-4ada-8145-dbbcdbc74f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7808
        },
        {
            "id": "c651e3b2-2fdd-4302-858b-6cbcb46b35ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7810
        },
        {
            "id": "bfae730d-27bc-42c7-8fed-0c261fe09b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7812
        },
        {
            "id": "6eb676f2-278d-4bbd-8958-804e3aa461d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7840
        },
        {
            "id": "04515e81-122e-4537-985e-e0a856ee8e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7842
        },
        {
            "id": "f17c79e2-5498-415b-92ce-2b1b32341b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7844
        },
        {
            "id": "21e8d99c-399f-4fdf-bd87-33abd3127af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7846
        },
        {
            "id": "504cd18d-624a-4fc1-adb2-2409c6eb17e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7848
        },
        {
            "id": "3fbc63aa-baf8-4b71-860e-61a978e76a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7850
        },
        {
            "id": "61108ba7-2cfe-4644-848e-a2b0e2308e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7852
        },
        {
            "id": "948b66e4-c161-408b-97d1-969206f19fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7854
        },
        {
            "id": "513c574e-d1f5-4889-96d0-40c1ca03b69f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7856
        },
        {
            "id": "2b5e5fce-2a69-4702-913b-ded6daee9033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7858
        },
        {
            "id": "d5962a1c-ff36-4b9d-a297-cd23f409445a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7860
        },
        {
            "id": "659149e1-03c9-4dee-8cdb-1c00c903609d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7862
        },
        {
            "id": "8f0f0a3f-d740-41e1-bcdd-e608ac919af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7922
        },
        {
            "id": "007e2aac-25d0-4f28-badf-cc8c0a00969f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7924
        },
        {
            "id": "5294e3ed-c885-47e3-8b26-0579ab6566c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7926
        },
        {
            "id": "df9f7c54-3b42-42b7-b7f6-716395795779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7928
        },
        {
            "id": "94d2ad8c-8bd8-4549-8536-4514772abcf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 8218
        },
        {
            "id": "8e6396ea-bab0-4683-8592-8bb51476af7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 8222
        },
        {
            "id": "393dbe5b-9d93-4d2f-9968-1ab6fe52d1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "2061238d-f29f-41a7-9954-07957df5f181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "c0b851fd-b23a-4a29-9f56-3e98156a6ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "0627d079-b82f-47eb-a164-ddd106a39fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "58e8028c-9f0c-4227-8391-82c2c96a9d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "b53f6b56-2d9c-4189-a969-e1263e2bc53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "75ba1b8f-407f-4a39-83b6-9a44a51c239d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "22e38f5d-43be-4629-a6ad-77d2167a191c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "b00c17f0-84e0-45aa-9200-30548c84dfab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "d23087ea-d703-460d-993e-817d33632c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "417b32f4-2f6e-4423-9bd2-38434b0a0816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "267a9c72-df59-4664-9c00-c028879e43a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "ca4526d5-a6d3-4b87-8d72-33643bf58aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "a9c9b9c3-e084-4523-844a-26be86a270eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 266
        },
        {
            "id": "c56f1bdb-8733-434c-9cd9-501b9569ad00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "fc9d74cd-c85e-4ce2-ab59-019f6360d9be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "d5d43c14-72da-4e50-9d33-82ec95ff0b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "2ccdfa3a-10f5-4bee-a2f8-c7d7856c41b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "75492382-fe79-470d-be97-2c815c4c018c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "f86653cd-f94b-4ebd-b96e-dbbc92b4cc73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 332
        },
        {
            "id": "70f555ab-3839-47fa-861e-e7cfb56e15f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 334
        },
        {
            "id": "27ca656e-e3a8-4190-a570-ec6cfb770631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 336
        },
        {
            "id": "86224f08-101f-467a-bb8a-8fa6d1715ac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "3ee68136-ce2d-485a-ad1f-5b5d5fa90811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 416
        },
        {
            "id": "26ecf595-c251-4690-a585-00d61a63cef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 510
        },
        {
            "id": "7541bbb2-1d1c-422b-b337-230048d4460c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7884
        },
        {
            "id": "76188487-bef3-44f2-9563-c17cc4faf65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7886
        },
        {
            "id": "829cd29f-5db5-49b3-af39-80625872a0e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7888
        },
        {
            "id": "5a9c3ac1-a225-4496-a27b-3194d9d47921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7890
        },
        {
            "id": "33acdb32-361e-4cea-a423-04c721643eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7892
        },
        {
            "id": "8f334ecb-c8e3-41f8-9f6c-584b6a995edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7894
        },
        {
            "id": "f491f6e0-7fc3-40e7-8460-2b08dd86111f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7896
        },
        {
            "id": "4731687d-1fb8-4e99-996c-aa46052aa899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7898
        },
        {
            "id": "51c2b2f5-9042-4801-a8f7-9270a118d151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7900
        },
        {
            "id": "b2c2fb65-07e1-45ff-a656-b0ebd67d2e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7902
        },
        {
            "id": "64582047-03aa-45d5-bea1-fdb265151064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7904
        },
        {
            "id": "082e69b8-d2b4-4f46-a724-b6051198b30e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7906
        },
        {
            "id": "a8d3b2ed-c687-4c0d-873f-339b4fe4a0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 44
        },
        {
            "id": "1312eb4b-3382-49d5-997e-f06f6bf2e353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 46
        },
        {
            "id": "0d1cb752-bb95-46e8-b3ce-8f820eee6869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "f52f98f2-c33a-4181-baa8-e0d0402924a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 84
        },
        {
            "id": "1330f1a3-a9d1-400b-b184-3cd2c93b10db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "133156bc-7a18-42d7-a3f3-e4f69227059c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "a92dae00-a9ce-4787-affa-f3d29fbc5036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "032577c8-f20a-4a13-83c5-0a135ba3938f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "1837885b-ef84-4da7-8757-bd00a59662a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "99719f55-c614-45f9-9991-3513e8e55806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "401cabe0-92ee-4155-8900-b49539c710a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "da27f1bb-a2bb-4c59-9ddf-e8ce0e71a2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "7e32e156-8856-492b-b3af-aa9047101b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "f6a099a7-9a27-41e1-9e1d-5db1d2ec4548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "75b73163-b797-415f-b373-ab9ca8b89958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "5939bdaf-9bde-4add-97db-e98904c5a152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 221
        },
        {
            "id": "f1a31d97-202c-42d4-b6b9-cc92dcca7bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "84af1b13-6efc-4b7d-9023-72c57ba2691e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "6e70e921-bb14-485e-a2e8-205917279136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "47273adc-195b-403f-a4b0-19fbb7465f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 354
        },
        {
            "id": "a817d36c-407c-4b94-bc33-8fb6ab60f1b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 356
        },
        {
            "id": "de2e322b-44bf-474b-ae1d-e719f718ff47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 372
        },
        {
            "id": "3ac7de58-f427-4705-b0b6-ba05229ea204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 374
        },
        {
            "id": "1c68e5b9-76f6-4624-b36b-7dba55fc4fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 376
        },
        {
            "id": "da76d886-9b71-457f-8813-124fc4842fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 377
        },
        {
            "id": "2678cece-bd84-4487-ac81-e49bee7b48c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 379
        },
        {
            "id": "0e7f5259-84a1-4b33-b1b4-6f55447a9edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 381
        },
        {
            "id": "59c3c8e7-a3dc-4299-9629-34a3b2850974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "a70096e0-52ee-46fb-8e33-d4d19f601fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 538
        },
        {
            "id": "d6e1cb12-7e25-4bb4-8016-2945b1a4b758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 1029
        },
        {
            "id": "dcdfcd0c-a9e2-4d92-9f51-8d44fee08af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7680
        },
        {
            "id": "4a3d3114-04b3-4dd9-9dd5-3d406fef8bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7808
        },
        {
            "id": "9f9af414-f089-44d1-b651-592b03287ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7810
        },
        {
            "id": "188b00bb-1a79-4da1-9538-b93daca38634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7812
        },
        {
            "id": "e166c834-a4af-495a-96a9-715f0c87f89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "0a75d9c8-4552-4671-9027-ff562a2d1d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "7d885cad-e0a0-43ba-8698-b3c7218e1ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "5c9bf2ab-b514-42d4-b0b6-5cae8ac65597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "9fc07740-3518-4960-a139-1f496570ac45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "45ab2bda-637d-452f-858d-21713923c466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "959c3561-a99b-4718-adf9-b279a88abfc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "ba22d48b-a5c4-45ee-93d1-fdb7edf80238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "98205319-f63a-449d-9ee4-2076664347c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "4273ab8f-3b17-4ca0-8012-f311a63829ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "a62f2b50-6b83-437e-a17c-b1baa5ef5251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "8fa53d54-cc59-4a83-a806-20259d8e5b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "815bf9a7-90fa-42eb-a521-c6273eb479fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7922
        },
        {
            "id": "f612715e-d060-4221-8513-2c9a64ec54e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7924
        },
        {
            "id": "a2f7538e-703c-4fa6-a241-65b321fca9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7926
        },
        {
            "id": "a436302c-4423-4bad-ad91-7a7ff173c01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7928
        },
        {
            "id": "6c3686fd-3e94-460d-b499-99c50bc1df2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 8218
        },
        {
            "id": "2d49081d-c2c2-4179-a12c-8278ba4aa4a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 8222
        },
        {
            "id": "0d866164-ad9c-4683-a6f4-8089f977fb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 69,
            "second": 74
        },
        {
            "id": "74e71d3f-2e80-4e92-a619-409cd610ddd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "a2fb3e43-9f4d-491d-b81e-8593ad513862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "dba395f9-e6ac-496d-aff9-6bde1e2c664f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 63
        },
        {
            "id": "9e45094b-1681-4dcc-baaf-437a0db3bad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "279109d7-f9ab-4b34-83d9-e61f3280c4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "df51a8d2-959e-42fc-a32a-ed259be932bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "50c5cb5b-7e35-4066-ac61-eded2e8d2ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "7fd9d160-2a52-4ffb-aeea-003db7cc0cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "8abf36e8-d032-4dbb-ad8c-14ca06f43fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "c508a8c6-4781-4b4d-bfb8-b704c142e309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "40293ac7-c180-4394-bf96-5f65034c6ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "6bb82dd0-f03c-4833-a7f7-3ed4f97d85ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "01a1f364-219c-4448-b434-f4e2b0b0bb8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "5f8929fc-20ce-4785-970e-bb9e6dcab145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "d219756a-0813-4c04-94a2-208ca75709ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7680
        },
        {
            "id": "7faea2c2-e335-4a72-bb38-be3acb68bdac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "92460e1f-5800-4a73-8404-3a3378d5e13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "d6249341-ae7f-4315-ab65-38e78ddc5f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "8b871eb9-c97e-44b2-9b8f-c39a210e0c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "67db839b-da8e-42ec-b195-6f27b6dda544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "72cdebba-4c3e-43e8-8e0a-b029f57cef62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "dca12b6d-66f7-496f-9700-16552c97119f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "9ef5a9c8-1277-408d-9269-e125c8a435f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "7419a205-0331-4753-9b38-d622f6bdef5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "4c16d424-bb73-437d-aa91-6bd1a7caddec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "b39e7371-bdd5-4e27-86b4-3849e06c3c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "c5619599-b8b4-4036-b353-67e5f22afae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "31059a10-b891-4142-aac1-c0ecf6c92661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8218
        },
        {
            "id": "a4e41b4e-ca2d-4735-bf20-bced6c00bed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8222
        },
        {
            "id": "10330b3d-37a1-420f-903e-9be4af51d84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "ee679c96-9592-45ee-8bac-cb66a74adab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "ea3464ca-5214-4f0d-9edf-2f7744da8438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "ddb1202d-5b0b-4086-966d-ff7b45dd2d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "6788895d-9625-4722-bb3c-28e10fd37b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "f527c8d9-f507-4aa8-8c13-3b0b8773a205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "6c23a39f-5f09-4cf5-91c8-ed39fe64b85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "7375b83d-3c8b-4930-80d6-7e3b11f478bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "eb10704c-c6b8-48e1-9ff8-e5a51545eec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "580fe6c7-343e-4dfd-a7e9-c4badd818ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "814e9973-99fe-4f6f-84fe-102f8255b166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "2db2d803-fb97-4db1-aeef-84ff68c38292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "a427cdce-6a05-4ee6-aaa1-bf94e26b1e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "d6e75de3-714d-41ea-8e8e-3232e3d13810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "f31e5b5d-b0ca-4ca7-82c9-fc6fc690b842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "05f7d008-2727-48bb-b133-bf122021435a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "6718c69b-28d4-4d4b-86d7-2f0c3fa97868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "8a3c8a1a-4423-468e-9b54-61f266336afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "6b491f4f-f8c1-4359-bc2f-0256f3c35b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "6768175d-b2f5-4a61-ab00-1ae6a9ee9ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "414d17fe-7114-42fb-ac58-3f444df3ebcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "455ba82a-fbe7-4ceb-ba0f-f8a54ab042d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "d4e2b128-7f6e-4e5f-873d-238d7c4f600a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "e75274f4-9956-4c6e-beb3-5a669cc65731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "a6dd1439-46b0-4ebf-9509-cd430e395708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "9d1d4b85-fd1d-4b12-86d2-9769c4c45b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "f11c4b11-9af2-4cbc-a100-5581861137cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "f01d6d87-b6f1-479e-9364-3708fddf4e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "4e062df6-59d8-40fa-9a09-b845b2313480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "6e9c4627-6189-46ca-9fa2-e30dd25bb73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "8b345108-c674-4ff5-850b-b6a235aeb8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "e6d4e238-6bc9-4ad4-8ef3-b4d78a6a53f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "2d5ec0f6-5c47-450d-b582-89142479efdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "b011e8b1-6e60-4ec0-8875-5381641041cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "475f6021-3c22-44f3-9b85-78aebd2167cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "713149fd-2a1a-43d7-bb56-1a5eb74e1f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "a693d450-40b4-4cd2-aad0-ba3d17c6a5ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "6d32a1ff-502b-4ffe-a9a8-ce7f98f7056e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 34
        },
        {
            "id": "d2f9ee28-4c45-47d8-960d-e027026d81be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 39
        },
        {
            "id": "6a9d93fd-1135-4fd8-b655-53ea48f42e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "f46a67d0-0e04-4187-8d9d-6547761f396b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "a8464736-0da2-4f66-b58e-cbdbc1e0aeea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "125d3254-1c2d-434f-bdff-e3744da63652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "b3039b8d-b0c6-441f-aa92-daf3efd4f96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "85da1606-19ae-48d4-8f30-3ae263fa22dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "efb723f9-4f48-47ec-ad8b-6d6db6cc5d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "014e8422-f3c3-47db-a6d5-565a712747fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "40e3017e-deaa-4c93-9981-f392d6df37a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "f08c376e-3f58-4268-8a97-907121a40888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "8a21f3f7-487e-4a83-9c47-205d62f63306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "0892a669-a350-4e0c-beb2-5984ee2beaef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "5d4d5a44-1f87-41ba-b1f5-fd40cbb8ad89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "eb1f83e1-e2d7-4d79-aba0-c046b8c3f7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "ba9ba78d-4bf1-4499-8187-1cc0a9b32924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "6897cf09-a50c-46fd-8ef5-1464a515ddd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "a33145e0-3df3-4ad6-8917-f187342d2f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "f8c326e5-34ae-4d9c-9c8c-906c74e9ee25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "d14f2509-a942-405a-8da5-530e16e83507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "22d693b3-c17e-444b-b96a-aaaaf0eda7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "10d28f7b-3e71-4d0c-b87c-1bd9aa3b9972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "f6720110-be27-4b96-88d4-44f4102329cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "7a7cb9b0-65dd-4c68-a403-e8d0da6b445a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "9c726b8a-c656-4592-95c7-acde3dd5612b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "f20b998f-9d31-4750-a0a0-c113d9b998ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "d26a74b0-90b7-4003-95ec-1b0f36d0048d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "9c325ffb-a3bd-41ea-ac5c-0177731e470c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "52f2b3b8-6a87-4f99-8c63-01187b085aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "b9f84928-2056-461a-9df0-1caf0d57b053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "3bbf3510-67ff-449d-9e44-c1ac43f0e8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "397a1047-1634-4eba-b7e1-836a2375a033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "7b9ce624-cd78-4103-8d63-653e7cdb614b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "43e3dda4-c515-4998-946a-a7e48a754bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "7decf24e-e357-49d3-83a3-b8a151300c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "6c1bc9bc-8eb2-4ca8-a1f8-1b3fa5b8f267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "8164373b-ea26-4371-8214-08ccdb304a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "c744e78e-ba48-4ada-86b6-a965e9a052e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "f3ce6d76-b25a-4c41-aebb-46f6a141cce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "ef32c515-1c75-4e82-8cde-679cef4ae070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "18b62cbe-81bd-465a-940c-a5fbdc3ab720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "658a1040-2883-4c53-a8f9-73d1f4155299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "71b74753-fc88-47a7-81a5-30ef5292c229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "a31430cc-af80-433e-bf41-633bafb5f128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "569ca529-9ae2-4e05-8e10-fd76c7872338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "4e1b605d-800f-45b2-a18c-8693140746a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "c057d603-48d4-44ec-aa45-7ed2fe567e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "1f886d9c-16eb-4ee1-b5ec-433a1f8db902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "98505d7a-22df-4ce4-af61-0e9be2e24d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "78858169-8297-49d8-968e-ee21031e7115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "db894e39-bf14-4b60-940c-78b359fa7910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "77304cbf-898e-4cf4-885e-9a4154089974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "21951719-7c04-49f2-8e1a-840b6024c95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "b3709b8a-aa28-4cba-9fd9-f48afd89c434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "7f6f3c8c-a816-4f20-944f-6d4f8a91d8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "532e0a3e-3eda-47d9-8d71-088f34036267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "65f710fd-27b2-4ca4-95cd-9af9dd924ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "1225fe44-9cbe-4eb6-a53c-4842faef8304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "207541eb-4a0d-4c67-9f37-163d4f3e32e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "cdbea322-178a-43fa-9d90-cbac2100d9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "7b5991b4-4c87-4deb-bbb8-524c33686e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "5ce15755-896b-4fa4-8337-0ddb1ffd08b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "336829c6-04ca-44a5-809a-81bd51df62fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "2e718c65-d28a-4376-9de1-8db6629ee925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "197b13a1-e36a-40dd-b7a2-ac3c0319a0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "7883323a-2eba-4e85-805b-009a702e9f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "49cae175-a6d5-4b1d-88b8-e2e47b7c9b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "e789602a-1957-406b-8305-2ac9e7f2a791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "225fa7d2-0224-4cd9-baca-78093c848666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "f90f0e4e-73e6-4f54-9a37-f98603516ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "77bdd837-bf38-44a1-b1a7-0c220b142aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "0d3c4774-e769-4a40-9bc2-da22f64e85b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7922
        },
        {
            "id": "d64377f0-e595-4031-aadc-c36c897aadfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7924
        },
        {
            "id": "d0cf0fbb-7e0d-4df4-b821-c0af98003da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7926
        },
        {
            "id": "429f2623-bd75-4c17-a0c7-aa34b7fcbaca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7928
        },
        {
            "id": "82ff2699-8713-43a2-bb7b-92939ebf64af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8217
        },
        {
            "id": "d2cbe8da-1a1a-47ad-b509-bcf1eee29b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8221
        },
        {
            "id": "40e35f87-a6d1-40b1-bab5-8fac8d31a9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 44
        },
        {
            "id": "66058545-d8e1-47d4-92f4-4d2b3ac7a372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 46
        },
        {
            "id": "6df1f2ea-e142-481a-b826-5cc5b21cbd60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "9f3bbd97-e977-4e69-a4ea-c53c8ab07f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 84
        },
        {
            "id": "314a8b11-c563-44e8-9975-6e05f7cf5937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "643fccb1-c37b-4583-a47e-84701caadba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "62d174b0-2043-4915-ab14-f55cb406a14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "56214249-7030-4211-9d54-ea0780eb4278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "3183cfcd-0441-4f40-b78a-aa3ad6c5ac23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "6d7460db-7119-4972-ab79-62dd941df67a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "238c1f21-c873-4d8d-8345-6a72e9d5c28b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "6a006e19-182a-4184-90b7-69362507e549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "dce7d05d-fe8b-40d4-8b53-a4dd767f3a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "7b04c51d-d6a3-470f-a304-9c9ce04f2260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "09e5cb55-52d3-4ad5-94d0-487f8f20ba0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "409daaf9-f937-485c-b771-81e809fbc1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 221
        },
        {
            "id": "2a996535-c6bd-4b24-a674-8a597969fa2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "f1f4c165-54b3-464c-808c-3a3d5287143e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "3b65b01e-74f4-4879-90ca-f8eda4ba1693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "4cf97fc1-9e64-49e6-ab93-a400659cd802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 354
        },
        {
            "id": "e1903f5e-1782-4e3e-aa67-92aa8d9a3212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 356
        },
        {
            "id": "2a062f91-e795-4cb4-a2b6-a4cc6ddf2d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 372
        },
        {
            "id": "1fa3bac9-c841-403a-9492-8a2e8861785c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 374
        },
        {
            "id": "aee4b09d-5153-4aca-9ce4-4624b981b76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 376
        },
        {
            "id": "bf3a85db-bfb8-41d1-91f5-a7834d36b37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "ed773405-5be1-4e29-838b-7ed8218613d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "3eef1a78-a0eb-40e2-af2c-8ef0ebe7179e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "1e5d7919-3e5e-46ed-8574-9f09adc60942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "ba4fe362-0a97-4ecf-8d0c-8d281ca97e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 538
        },
        {
            "id": "bb2bd343-315d-4dc1-900f-acd09dcaf98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 1029
        },
        {
            "id": "02c7e8b1-e02e-4482-b7c4-c8722311af3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7680
        },
        {
            "id": "ae1d9c8e-fd4d-4008-93b2-88e946c434e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7808
        },
        {
            "id": "d7ad4db7-f3fa-467e-9a97-46c0a559aad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7810
        },
        {
            "id": "c2737c46-3005-4a69-84be-e05596703846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7812
        },
        {
            "id": "79748853-074c-429b-bb86-c50f2c06b4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "4600a7c7-4224-4397-a8d8-f6e2d0c2af88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "ab766916-cad8-4231-b704-a70ebb06e0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "a8ed663e-b4b1-4682-884f-49d25e2432bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "4df6e841-b7f4-4fd2-bf4e-bac8c3fb499b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "69c85d7e-b635-41bc-855e-53811f9ed025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "a852f5ba-7052-4201-b1e9-a504047e4673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "2f53cf21-d078-47ed-b36b-4ed4974e0676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "661581eb-fb56-480d-b67b-22089aacea2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "431aba8c-cfc2-4f1b-b597-a79af10a3268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "d5027e20-a51b-4ca4-88b8-964f640774bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "7f031e68-f2d6-425b-ae43-3ef0a14551ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "84f9a257-d35f-4a1d-b47f-f27720c369e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7922
        },
        {
            "id": "67d4d77d-f7db-40df-a3a1-0a452684a042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7924
        },
        {
            "id": "40ef5b73-8b8d-4384-8f10-9adc0983e457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7926
        },
        {
            "id": "467b7ebd-c190-4a67-bc35-27e3daac7909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7928
        },
        {
            "id": "2cf47c62-561a-47f4-b263-00fdcef65758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 8218
        },
        {
            "id": "269917a8-8075-4ab4-b0ee-bf76ae8a0abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 8222
        },
        {
            "id": "006d6c07-79a6-44e4-977c-f81ce170c623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "5b0960b7-6193-4b94-9b71-1ce74e6a2617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "22b25bcc-dcb1-4f2d-b36a-98451414596b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "38d9f0a1-12d2-44cf-a380-7cf8293b8d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "47214975-76e3-4b0e-95d3-cd4e19563e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 90
        },
        {
            "id": "cb47c613-5c27-4267-ae6e-c32351788e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 192
        },
        {
            "id": "4f145ba3-1303-4667-9d00-71c709f0c395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 193
        },
        {
            "id": "8a06b476-43c0-42d5-a01c-bcb8f84bc747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 194
        },
        {
            "id": "64466579-4de4-45ef-b9e2-d6cfc0aa7181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 195
        },
        {
            "id": "a97f20d3-9eba-463c-ac87-248b9c0110eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 196
        },
        {
            "id": "c9e0fe2e-af20-47c1-a343-2ac645245905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 197
        },
        {
            "id": "ebcf0210-6fdc-4584-89ff-7b4ff618dd5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 256
        },
        {
            "id": "92472dd1-04ab-42cd-81ce-9faafffc64dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 258
        },
        {
            "id": "bdf3c952-18ab-4c03-9fe9-e89b89acd28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 260
        },
        {
            "id": "382c4408-c2b5-4fd2-8ba1-bdf4eb54a9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 377
        },
        {
            "id": "f2d3ce4d-c20f-4337-af9b-d88ccb9a98c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 379
        },
        {
            "id": "3ed85f30-99bf-4c4d-a166-51d071238755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 381
        },
        {
            "id": "ffacf52a-5577-47cf-85ef-02c2439fc1f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 506
        },
        {
            "id": "be7151dc-c245-490f-b20e-119789771d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7680
        },
        {
            "id": "f3fe0296-42db-4e1c-9ec2-15c141ecbf16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7840
        },
        {
            "id": "a30e43de-974e-4bc0-9746-9ad8ff0ed288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7842
        },
        {
            "id": "5ea59f03-32b8-456c-b83c-cf201702dc98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7844
        },
        {
            "id": "881cf942-f563-400e-aea4-767d2394597c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7846
        },
        {
            "id": "69bbeb0f-8ebb-4c29-aeed-b864386bbd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7848
        },
        {
            "id": "0f966783-4f13-429d-be45-b11c41997a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7850
        },
        {
            "id": "aa5eeb1c-d732-464e-a007-b6556da7b6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7852
        },
        {
            "id": "cf9f8af6-f46e-433a-a25c-7e6928bc4d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7854
        },
        {
            "id": "7f82eff8-c4f2-4ff2-919e-ede22686f3e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7856
        },
        {
            "id": "aa2a4a6a-e8ce-483f-bf0c-6945e2ae14b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7858
        },
        {
            "id": "423bd7e5-3acc-4d0d-a3d7-4f53c032991f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7860
        },
        {
            "id": "c727db22-a8d0-4e99-b0df-2bd706165363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 7862
        },
        {
            "id": "bdfbf75c-0a6a-4f4a-b260-b2c9244d3106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 8218
        },
        {
            "id": "d40cd1bb-7066-4ff4-8509-a50ea5a7d26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 8222
        },
        {
            "id": "97aa1ef2-4d05-4dc2-869f-027d98b513d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 44
        },
        {
            "id": "554f746d-4ac5-4060-b74e-e1049ee7308c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 46
        },
        {
            "id": "ea24aa8d-f24a-45d7-a2eb-9cfef258cd88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "7cc592ca-3c81-4d58-b43d-e0e31eccfd19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "1408fc6e-3283-4b40-afe5-098a162576b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "796d9306-7900-4fe4-b9ab-434c3b29d1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "613bf89c-2c38-498d-890b-0c173eae5309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "56aff766-363c-4f4b-9e0c-0ddea6432065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "6425b12b-4f63-45c7-ad4d-6751fe6b29df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "e1a34728-45ba-4281-a32b-0312acefe035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "0bbaae73-026c-410c-8cfe-eb2e25389d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "32e37634-0662-445c-9c0c-9e712e4c9bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "b9646a16-1143-478f-b102-b10552f70105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "3b06714f-5b9d-4642-8e56-0641fcd4ae90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "51b389b0-0cc4-42fb-8ed5-60765ac60606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "cc4bf706-9673-469e-8cce-df2b270eab00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 221
        },
        {
            "id": "6ad94274-684e-42ed-b4c9-a5742ef35490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "91d9d61f-f07f-4f94-b960-a704e0babeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "2daa4060-7c20-4016-a940-63cedb66739a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "f0f1d1bb-6242-4dc5-9daf-22cafb1deaa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 354
        },
        {
            "id": "8a98b342-755d-40ab-85d5-74fd07542df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 356
        },
        {
            "id": "4c8da13e-e714-4295-8591-39ca5b57d4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 372
        },
        {
            "id": "22eadd0b-2975-461f-aaa8-915fc2529745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 374
        },
        {
            "id": "04f2c8df-a2b7-4a87-aaaf-1830c8756084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 376
        },
        {
            "id": "d31b191e-e019-49e8-9231-e3b68f08b8bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 377
        },
        {
            "id": "03030093-b0e0-4b19-aec0-66b7fd45233d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 379
        },
        {
            "id": "47fc83c9-0086-40c7-8016-40fe3baac722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 381
        },
        {
            "id": "2a3c9493-eb49-456b-b108-2c899a9151da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "c3d0a045-ca22-44e4-8b77-559c9af0f032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 538
        },
        {
            "id": "c29a5734-b3e6-498f-84d6-8bc9146cc036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 1029
        },
        {
            "id": "12079c28-16c7-4224-a9a9-aff83f49fb79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7680
        },
        {
            "id": "68520720-59c7-48ef-8390-765f4c186f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7808
        },
        {
            "id": "7283d745-2cf2-441c-8fe8-249d3b0b4a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7810
        },
        {
            "id": "a8668935-4e57-4396-b70e-61f1a78a8937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7812
        },
        {
            "id": "2660c967-a316-470a-b1d5-1e323be7ad89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "cd6110e6-ed2d-4cf7-bafd-00f9479cde71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "423dadd2-4899-4733-a51a-29e7412bcf98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "29ff8163-a435-4aa6-bba9-36468345ab53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "45bdac35-3e43-4a3b-a045-79610ee78ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "1afd0d23-6ded-4d82-beac-6e8bbfa366d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "d71c1de7-97ed-4761-8776-d0028edf5221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "2e54e845-618e-4182-bda0-abe2cd09a52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "e12d0974-64f0-41e0-aedc-039e09ef8a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "cb8724e5-9161-4bc9-8520-2de2bb3452b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "7c8480c7-1e64-41d0-bdbf-32c737b2529b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "ba1c9e85-ece1-4b12-bc45-f0b51607fcfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "23a8143e-815c-4bb4-837c-3ae500165ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7922
        },
        {
            "id": "6dd5cee0-e8fc-46ba-8c32-8994812b5117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7924
        },
        {
            "id": "ca271e55-ceec-4bd9-867a-54801cacc4ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7926
        },
        {
            "id": "2a7d8895-e619-4df8-8c3e-978d7db0f066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7928
        },
        {
            "id": "b879e9e4-4fd5-49c4-807a-6aada1264948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 8218
        },
        {
            "id": "40104bf5-9c6d-42c5-a369-d5ecb4ba0ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 8222
        },
        {
            "id": "5b8fb3c5-4678-46ad-aa24-9cd4d9e1fead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "6e984e2a-8ad5-4229-8d2d-dc7ab3f1536b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "3a5203de-2c85-420f-a7bf-67258992c507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "eed820a3-3a13-4685-afba-61f521b38d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 63
        },
        {
            "id": "45ef3235-4d80-4707-9d65-3439a11ce596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "08d6df56-1116-4fed-9f43-da427b10e0b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "37814faa-7945-4eab-9f27-bf766474d992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "eb4c6042-9023-4487-a979-6ba6e4e712bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "ecee0e22-b360-4e21-b58e-aba72aa93dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "3d4c9303-06fc-47ec-9ae0-b2e984babd1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "49632911-ceb8-4e01-b4e5-c3f1881fb0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 97
        },
        {
            "id": "3b4999a9-7260-4e12-a5aa-23d830b3fa98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "c46095df-f80f-4342-a1e5-90ea0215e3eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 100
        },
        {
            "id": "e5362aee-2d98-4644-94af-1691a39a0253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "30c32ac5-38a0-473c-b307-5708c3f49ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 103
        },
        {
            "id": "e3279b9d-01b2-4255-94b1-83b0f1e964e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 109
        },
        {
            "id": "af6694d0-9786-47a2-bb6a-6f989cfc9f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 110
        },
        {
            "id": "21578f96-103d-4a1d-adfb-2e178385a66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "f820cac7-e0db-4a9d-a909-4591d4818896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 112
        },
        {
            "id": "eca1edb1-0a72-4f9d-9e1a-c9b20f3f6df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 113
        },
        {
            "id": "e8a5f698-21a9-4c5d-a0c1-0b0a8eaef969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "b14b3fd9-7a35-4d41-8e1a-fa571890438c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "5bf9e0b6-6e25-418c-b49a-aa33d41bffe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "d932cd7e-fc7d-4731-872a-1a1ca1a692b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "daf7bf84-6f58-484f-a98f-bf60d09286ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "730bf65a-4d5f-4b28-91f0-068a6e574f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "8e763d0f-63c2-46f5-bbb7-69cbf48575e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "715f3029-65ba-444f-a827-1987e086feaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 122
        },
        {
            "id": "f5f50128-e168-4798-b8cb-b37a194b49fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 192
        },
        {
            "id": "df1d9de4-e0dc-43e8-ad2f-fff4ac17ceca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 193
        },
        {
            "id": "f9705907-3673-4305-8ffa-6689a3377a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 194
        },
        {
            "id": "f4d8c807-3f59-4e75-8d0b-eabfc639d2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 195
        },
        {
            "id": "0f9df24b-5950-400d-8c93-9b4d02f8dcda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 196
        },
        {
            "id": "45b77c78-3863-4745-a2b9-a18b600c17d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 197
        },
        {
            "id": "2eaeb521-9b9a-4733-b51f-de969555ed74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "cd57125d-7a8f-4467-8642-6881ceed519f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "33fe01d9-8015-4ff3-96fb-56eec997f6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "333d36e2-db22-4995-9a41-3efd0609a7c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "01c69d7e-9abe-48ae-a5aa-9cf809166aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "b3f36b1f-4794-40aa-9796-19f1fea2463b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "acb4d2d7-b740-4cfe-b1f5-daa897dc208c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "e59b5004-df3b-4056-b49b-1d870c65f57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 224
        },
        {
            "id": "12c0c1d6-8640-499d-bbd7-dbd9f6e68ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 225
        },
        {
            "id": "cd62207d-c1b1-45bf-b797-3811e60a8d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 226
        },
        {
            "id": "bd5bf897-9889-44d8-8b26-65ce1fba69ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 227
        },
        {
            "id": "f5316318-3c9f-4e56-981c-174140a0145b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 228
        },
        {
            "id": "d5264a14-d3d8-4a08-bcc3-b86a84288d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 229
        },
        {
            "id": "f5b75efe-13d4-468d-9415-e4e5b9545b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 230
        },
        {
            "id": "9bdd4a4b-1cb6-46e1-8009-bc25b10cdf2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 231
        },
        {
            "id": "a0d58803-5e0e-4939-a97c-76e157bfdbae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 232
        },
        {
            "id": "228e440b-1cbe-4d03-946f-717245682732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 233
        },
        {
            "id": "6701f152-5cd0-4c16-855a-5bfa68944dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 234
        },
        {
            "id": "ecf942c4-e532-4c8a-8257-da4d3287c39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 235
        },
        {
            "id": "698ad5a3-5039-4156-80a0-07626d9b7bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 242
        },
        {
            "id": "f95b9dd8-927f-4f56-9df0-3bdf86e49b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 243
        },
        {
            "id": "3acf9d53-3fb4-4d27-a70f-d0481a53de0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 244
        },
        {
            "id": "4eaa128a-b88e-400f-87f7-58623d29a357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 245
        },
        {
            "id": "d958b70e-ffae-447b-a691-cbbd6509a576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 246
        },
        {
            "id": "d317afe4-117b-4cdd-bd7a-76f051c2c280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 248
        },
        {
            "id": "5a4e9fd8-6f5a-4dcc-861e-58c94b2468ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 249
        },
        {
            "id": "92948f28-7258-47ed-aa86-4399853c8d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 250
        },
        {
            "id": "963b7aa3-b152-4b3e-9664-fde58ba5b8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 251
        },
        {
            "id": "cb94b112-bf89-4cfb-b46a-782bc18b188a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 252
        },
        {
            "id": "8324bf3c-ec97-4d3a-b3bb-f52f9a8c0e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "bd2e5479-29af-4d03-9c17-2b8c198d05fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 256
        },
        {
            "id": "15a0aeb1-9fa8-483c-947c-1117344b587b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 257
        },
        {
            "id": "64e40b62-8b88-49f7-8cd1-3df68f9f8ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 258
        },
        {
            "id": "fe70bb04-3558-424c-8edb-59c4b4342d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 259
        },
        {
            "id": "c1f0e607-f127-4ccd-96f6-21e793295d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 260
        },
        {
            "id": "227089e8-cb78-4f12-a084-2b65ebdbe21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 261
        },
        {
            "id": "8773d2d8-0710-4011-84ad-1362782e10d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "26a2323c-71c4-4cfc-b95b-7024d9544d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 263
        },
        {
            "id": "cd73db06-15d9-450c-8bfa-12512cccc308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "a3ee3198-31dd-451c-96be-6c1db73b8229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 265
        },
        {
            "id": "f50b224d-b74e-4e1e-afd1-8df545f083e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 266
        },
        {
            "id": "db2145bd-493d-4430-9d3f-c9a1537ef22d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 267
        },
        {
            "id": "13fc4b52-b1fb-46bc-aa29-2feaa946fd44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "dec42650-4d8b-493f-8653-87fbf84151d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 269
        },
        {
            "id": "1de9ccda-9992-43ba-beb8-44b82c212c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 271
        },
        {
            "id": "dc546832-222b-417c-8ee7-21358c7666ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 273
        },
        {
            "id": "c7725abe-a966-412f-badc-5e3c97c7bd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 275
        },
        {
            "id": "d8f1f607-e67d-44b0-995d-e9abdf3a92c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 277
        },
        {
            "id": "dad46a02-9c58-430d-92f6-4bec6b3b9396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 279
        },
        {
            "id": "4e53fb2c-7abb-4afe-87cd-af9921350cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 281
        },
        {
            "id": "a9eafdfc-2566-4e64-a5c4-3e3d6416a6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 283
        },
        {
            "id": "6f92ba90-8ea5-46ec-aecb-4f55b7e10ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "972e30af-54c8-4448-925c-0281b9a496df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 285
        },
        {
            "id": "c9798cc3-c7c8-43eb-8055-74dd7e6273a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "1e7e2e85-b601-4a78-a535-f5754aa6cd8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 287
        },
        {
            "id": "ea20338c-5d5b-4058-a329-c27850b7402b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "ceeb73ef-d9e1-423f-b3ee-af50e8f225c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 289
        },
        {
            "id": "ae9e16a2-cad7-4fda-b548-48cb9aa95cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "bd0f91d1-105d-4fff-ac59-917fd61ac4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 291
        },
        {
            "id": "99703e4b-3d60-41d4-9aeb-3aec3bf42806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 312
        },
        {
            "id": "01949919-bcbf-41c2-8c8f-c0e5c933db6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 324
        },
        {
            "id": "0aff0036-14ba-4a6c-8dfb-d8406879179a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 326
        },
        {
            "id": "bcd1bb05-5168-49f9-a408-309f12824c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 331
        },
        {
            "id": "6cd74063-52e4-424f-92be-8d5c2e782a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "ad1439e9-b379-44fd-8016-cd241695b1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 333
        },
        {
            "id": "13da6e2e-6434-40bc-9845-5947d28c3f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "d9fccadc-9c7d-4ba3-b681-4c8fba799f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 335
        },
        {
            "id": "b93a9bc3-5526-4b24-977b-91bd24a8949c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "572b0aba-961b-4341-bab4-391b1dc4133b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 337
        },
        {
            "id": "b0f577b2-0751-4ec8-b33e-2032c8fecb0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "2cc43825-1994-4b04-bf3b-9409ca98d9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 339
        },
        {
            "id": "2772e18c-efb5-46f1-b36c-ee9ca7fe7643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 341
        },
        {
            "id": "c92c9dfd-0073-4ce7-b5f2-779add7e2145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 343
        },
        {
            "id": "a1673b9e-2a25-49e6-af90-50543c8773e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 347
        },
        {
            "id": "a06fa8f3-de32-419f-ab79-9da8d2d81e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 351
        },
        {
            "id": "23174d62-1982-49e0-a9fe-15c43459c206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 354
        },
        {
            "id": "ab87041b-be62-433f-ad60-0b5972dde771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 356
        },
        {
            "id": "06747f7a-9794-411b-afe8-963b350b67de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 361
        },
        {
            "id": "2d62f320-893e-4aa2-ad3b-0f82a1353d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 363
        },
        {
            "id": "5258f89a-7969-4213-bf18-fe0f7ba3151c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 365
        },
        {
            "id": "cf51a78c-0763-491e-ad7e-47289a27a371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 367
        },
        {
            "id": "22dd5c4c-27b5-42ac-abb0-2967b6f09a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 369
        },
        {
            "id": "0c85cecb-1077-41f5-9af8-5492c5c3b4b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 371
        },
        {
            "id": "a396df02-8517-4f2b-aefd-e68d64fcb76b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "3a9d2b8f-4d38-4c97-9b72-62f0fb58e694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 378
        },
        {
            "id": "78faaa44-71da-4422-9519-63ee6613af99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 380
        },
        {
            "id": "e9d88e6e-a77c-44c3-b50c-5c50ce118fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 382
        },
        {
            "id": "72827e00-076f-4905-9a26-7802e9772187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 416
        },
        {
            "id": "4d458e36-4274-40f8-872b-f2669ba191b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 417
        },
        {
            "id": "51c08141-8a44-40fb-a4ae-bdf371bd2579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 432
        },
        {
            "id": "48c9853a-dfa4-4fc8-90f0-835960eb03be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 506
        },
        {
            "id": "2cb8f555-bd9d-4317-b72f-fe5e202d8066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 507
        },
        {
            "id": "953ae306-07ef-4825-be23-30e14c1931b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 509
        },
        {
            "id": "04178130-7574-4391-878b-92ad35cced5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "af39d93c-0c23-4d1c-8248-13a5af95b33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 511
        },
        {
            "id": "03c249c5-3080-47e5-89f3-70c4c3ac39e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 537
        },
        {
            "id": "82d6ec12-c88b-4f63-90fb-99652ffa69cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 538
        },
        {
            "id": "be4438f7-3f8d-4f90-af6a-074a7b5868d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7680
        },
        {
            "id": "7c4102d3-47e8-4c8c-923d-b9a291eafe7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7681
        },
        {
            "id": "7390728a-9fbe-42e2-9e5b-480f07e3eae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7743
        },
        {
            "id": "57d704de-ec99-408c-9124-8fe838ab1621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "27220cf4-cbaa-45b3-9eb4-5fd787c94ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "7b105ae4-3b85-4f0c-85bb-bc5d6d33155a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7840
        },
        {
            "id": "20408004-1a61-4303-a9a0-def2c67a1944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7841
        },
        {
            "id": "70d22936-9d41-47fa-b90e-239a7ecb7943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7842
        },
        {
            "id": "2f503d39-b2a6-4cb6-92b7-0543429c2133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7843
        },
        {
            "id": "b843fe2d-0913-435b-b8e3-d5bf7f028592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7844
        },
        {
            "id": "3c047a62-bec8-4aad-84bb-0d50126fd83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7845
        },
        {
            "id": "11354327-9138-4854-a747-b5c81a26ff37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7846
        },
        {
            "id": "aad76bfd-73c3-4488-bf68-fdafdd3b1067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7848
        },
        {
            "id": "b19e86ca-5d7e-4b19-a3a0-1f18495770bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7849
        },
        {
            "id": "70046d49-a5a1-42d3-8190-0b18879c1868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7850
        },
        {
            "id": "9d018606-868b-4d4b-9c12-f10170ff7d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7851
        },
        {
            "id": "eb88ef40-05cc-490a-9a8c-a7400b39f8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7852
        },
        {
            "id": "e6337ecc-f43a-4a39-b8ed-6b22d294418e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7853
        },
        {
            "id": "2038837e-25eb-4853-8e31-0c1e21509939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7854
        },
        {
            "id": "9831f433-e907-44bf-942a-1f6b042fe1eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7855
        },
        {
            "id": "1f12b650-a552-4732-bc93-189d7d44809d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7856
        },
        {
            "id": "27484c3f-7456-407b-9cb4-d8aeda1f47f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7857
        },
        {
            "id": "86b555be-ea8e-4cb6-9178-d7d0d90c98d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7858
        },
        {
            "id": "6232071b-2afb-41bd-b762-764fccff1999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7859
        },
        {
            "id": "763884b0-605d-4c76-bf7d-2a5620f02502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7860
        },
        {
            "id": "3a16a613-9758-4b1c-b8d0-41867fb1a339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7861
        },
        {
            "id": "cd50a950-e2ca-4253-bad4-8cc71b50e06c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7862
        },
        {
            "id": "f3fbdcc5-0c13-4250-8bd0-dce8e8beec3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 7863
        },
        {
            "id": "e7e9270f-98c5-43e6-a86c-25e325b6dee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7865
        },
        {
            "id": "d99d593c-87cc-4ac5-a90a-0225665e3e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7867
        },
        {
            "id": "6d4d4e2f-e2bb-421c-a54a-cd0721176529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7869
        },
        {
            "id": "aeb76e8f-37d3-43a5-8854-c9f30afc2214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7871
        },
        {
            "id": "8068da7e-3d8c-4951-823d-ea5f00ee64e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7875
        },
        {
            "id": "ad82af9a-9eb3-4ddf-9b8b-482373a33bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7877
        },
        {
            "id": "7f1ac9c0-def9-49b4-9d24-5a6a3b6d1c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7879
        },
        {
            "id": "c9f74f73-eca2-48f7-9d10-8cb81cc02e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7884
        },
        {
            "id": "c535407b-6464-4379-8caf-8839076656b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7885
        },
        {
            "id": "35a1b202-6648-4b4f-b049-39250de6758e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7886
        },
        {
            "id": "b0a4ab74-478f-4bb6-9e9a-2f4beae66d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7887
        },
        {
            "id": "404ea635-ef7a-4ff0-96de-9001310e73df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7888
        },
        {
            "id": "f0b9fd4c-7985-4357-b313-c80bd4fe468b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7889
        },
        {
            "id": "a73de50b-1d0c-4e63-8d38-4f84dbf61c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7890
        },
        {
            "id": "bf80e8b4-5032-413a-a0e8-76ab1899f994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7892
        },
        {
            "id": "25f4be4c-9dca-4f19-90ae-c2bd20bfe839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7893
        },
        {
            "id": "a22181bc-860e-459d-bb7b-a06202021bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7894
        },
        {
            "id": "744d9d2d-e1cf-4698-b526-24b6d57d789c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7895
        },
        {
            "id": "f3485b2d-b5ec-42e5-b482-c24499b19356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7896
        },
        {
            "id": "4ce01e1f-61c5-4b99-b75b-05dd941d3b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7897
        },
        {
            "id": "6dcabf2e-33c8-460c-b6d4-d8a5907ac05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7898
        },
        {
            "id": "486af216-a987-4a6d-9ee7-9df68f550de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7899
        },
        {
            "id": "e1e08227-1d6c-4512-bb09-cebed352033a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7900
        },
        {
            "id": "0b817fc8-ebe1-485c-8fff-f4d08e7b67a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7901
        },
        {
            "id": "a8039446-0ab6-4af0-82e0-d0c2f4903543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7902
        },
        {
            "id": "0b9badf4-1938-4c1d-a8d1-96936c60f74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7903
        },
        {
            "id": "cd51297c-77b4-45d0-ac6a-4f09821fac6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7904
        },
        {
            "id": "ee1732a0-e7f6-47f4-8fab-92f3854b11d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7905
        },
        {
            "id": "6f6ee44a-6c12-439a-b870-b22d5e4a37cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7906
        },
        {
            "id": "aabda9cd-e11d-4fe7-9b65-c7bd81fe410b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7907
        },
        {
            "id": "58e0bb11-5ab1-47bd-a63b-6b2a10fd68ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7909
        },
        {
            "id": "e9874719-2e28-4f81-893e-1407c86f7caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7911
        },
        {
            "id": "8a02c165-9986-4202-b482-d78f21c9a6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7913
        },
        {
            "id": "e255d6a7-4e04-4d11-b164-16db25a458b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7915
        },
        {
            "id": "150683e5-80fb-487d-85cb-63759de1a619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7917
        },
        {
            "id": "fa44c829-d441-42ed-ad1b-df907d76fd26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7919
        },
        {
            "id": "b23cf996-aa8c-401f-9de8-2394e00b16e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7921
        },
        {
            "id": "5731d753-1b69-46dc-9da4-ee454adf6141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "4362d761-c4db-407f-b885-10d60b77b9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8211
        },
        {
            "id": "51298154-0716-4344-b999-7c9fc3c59072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8212
        },
        {
            "id": "d71d3dee-ca5c-4b61-8234-9af2b4736da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8213
        },
        {
            "id": "e6fba0b7-36c0-4bae-8b0f-826e4f5007bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8218
        },
        {
            "id": "78e11fa9-b098-4a34-8de7-2c3a683d9ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8222
        },
        {
            "id": "3a660df3-cdf6-4e1d-aa60-7be4d0fed16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "ea206c61-7ec5-40bd-8879-f11860c2a886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "753958d5-8c6d-4dde-92cb-887943cf46bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "e8fb0a16-cda7-4c98-bd78-5dadb7ecf432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 192
        },
        {
            "id": "d56d8758-d080-414c-80a1-abedd31d768c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 193
        },
        {
            "id": "1d59e277-1a52-491c-8a95-1f0c199385a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 194
        },
        {
            "id": "e861ae68-0653-4082-9ef4-e5f4ada78053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 195
        },
        {
            "id": "26a13b97-ef93-4898-8376-99cbb014e0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 196
        },
        {
            "id": "a047ddc4-c734-4fef-8f25-b5b5dd7ec43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 197
        },
        {
            "id": "3b43d6d1-801f-48ca-aed8-eea3bf4d984b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 256
        },
        {
            "id": "5bbea380-b032-406a-be7d-ac8328715f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 258
        },
        {
            "id": "a7c4b6ea-22ea-4dc8-882a-85014549db58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 260
        },
        {
            "id": "8cbc56f7-40d0-4417-837e-780db8e91114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 506
        },
        {
            "id": "8727cbb5-2404-46e0-98e1-1e633dfaffd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7680
        },
        {
            "id": "5f65bfaf-7d2f-4472-b0fb-58c6a82edbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7840
        },
        {
            "id": "0e54acbe-d365-4746-abd8-cf39bbacf9ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7842
        },
        {
            "id": "1bd94483-425f-4583-bc52-c54f3d9f4a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7844
        },
        {
            "id": "de27c8ea-a421-4f0c-acba-f5ca5ea203e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7846
        },
        {
            "id": "2cd8715d-a346-4127-a005-1778ab9e35dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7848
        },
        {
            "id": "6b2cbdc1-be92-4756-87f7-62f45f9e9d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7850
        },
        {
            "id": "9d329e70-a76c-46c8-a442-6d70f6afd7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7852
        },
        {
            "id": "0536c541-0fc7-4236-ab81-893a474c03a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7854
        },
        {
            "id": "3dcab264-0baa-4440-99c6-4da3b957c9ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7856
        },
        {
            "id": "55050fb8-6535-4b82-938d-02481aff9ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7858
        },
        {
            "id": "f6a87294-630e-4d4c-af73-6050b79f0c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7860
        },
        {
            "id": "5de39a1c-33d0-423f-8990-30bac28a0aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 7862
        },
        {
            "id": "9a8a60bb-6e82-442c-a5b3-8adcc514ff96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "43d29d87-2d0e-428a-a6a1-2a79cc9e3869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "be02fae1-ca20-4abf-b856-776100dfc1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "cab9fe7f-8e43-4bdb-9fcc-80fd052b5d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "453aaba3-03a2-4f0d-8893-132faa0f3d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 63
        },
        {
            "id": "f61df449-a7c0-4bef-8183-940093eafa68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "60c09695-2bc7-4418-9b93-e70d8b6096ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "ddde1d1c-ee12-4503-b34f-b87a35c2e8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "85c02fe8-ab98-4524-88aa-ef17755e126b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "e0740ba1-54f9-476b-bfef-622de722527d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "8a75242a-b4b3-4e23-a475-ee5a92797088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "bd2b80ab-af1f-400c-a439-628ed654d1f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "a573b4d1-54ca-40e4-864a-85f8de89297f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "29bcd4a2-3911-48e2-a101-9313754e3cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "c57fe849-995c-4910-a69a-ffe1a9d8ad06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "753975ea-fbdb-4a11-b764-6b46b4d53b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "269c4c37-71b4-47f5-8bcd-65f6c87013d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "b1d59f6b-f007-4727-ba36-7b2d523202a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "1b92a644-8cbb-4af9-b21b-c781e7653543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "a67f8393-f4ac-4290-a485-20357d36869a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "9238d842-a6e7-4f95-99f7-c4d5355673e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "f7b860dc-1550-4ab0-8030-6503afb5c6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "2e2982ec-33b1-4c3b-b4c1-f359441ff98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "1b175446-7430-46bd-a7af-c730ee3e4e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 192
        },
        {
            "id": "e53bb5ec-78c9-4901-a777-6e7cf4de2336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 193
        },
        {
            "id": "a553a401-4246-459c-be99-6817597e5e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 194
        },
        {
            "id": "9648229d-b26a-4e87-86b0-fc020e2350b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 195
        },
        {
            "id": "b7981417-853f-40f3-8397-baa92cdefa78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "b3a71692-4e7e-43c0-a5f5-76edf42b95e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "08619f1a-c8b9-4936-a751-a880dd2a5120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 199
        },
        {
            "id": "85437d53-51ec-4db9-8282-a5450425d3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 210
        },
        {
            "id": "9081182a-ed27-49cc-aef4-5011769535a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 211
        },
        {
            "id": "fd217c54-d3db-448e-bc10-4e6a7f4df7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 212
        },
        {
            "id": "8c297a68-d238-424d-b537-d113f15b5804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 213
        },
        {
            "id": "974536fe-9158-4242-bf58-065bdf40504d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 214
        },
        {
            "id": "852bb0a1-effc-43be-93ee-d30ea9e01cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 216
        },
        {
            "id": "0f48fe1f-6f72-4615-a2e2-35219acd6edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "81628516-a48b-4075-80ea-4a62ec7f691b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "de121b48-6a22-42c9-9b37-b03d25cff905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "973bb3f7-3e93-45c9-9b8b-802aa8ca3a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "40ccd2a4-59e8-441a-b5cd-e3233a540194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "3cbab968-798a-445d-b680-8e4e44791b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "137c4a47-0581-4f3c-a567-f5712ca10430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "2fab6c43-30a6-474a-8394-1a7aa66491a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "2eb75e6e-91ba-464a-9851-65166123b6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "15d80f94-e086-4669-8565-96772391a625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "d51ba82e-9c15-49da-893e-a68c377a0a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "af9dd671-ea62-4d0d-ba7e-95c86b60e7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "9c17f69b-784a-4f86-afcb-1eca0f4d4b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "9bfa1d0b-cf7c-4c16-8614-4d2d01c0c58c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "cf0e8033-9b6c-405d-8965-10122a143735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "9685b6d1-378a-489c-9a49-c3cf0ee3248e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "961e808c-402f-447f-8a13-91c83ee9ef1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "91e8e505-87af-4324-a668-70dc46359a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "cc884fbb-8543-4d1b-ac9e-1b6b76e04eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "2803a39b-a143-47d2-b3be-e2c719252824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "8ca5249a-59fc-452a-9547-2cf743cd5302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "03b0139b-cb02-4fb8-8894-6a01a4bc4411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "bc0e2e80-2992-4699-9ff9-ed01a9a4cae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 256
        },
        {
            "id": "09ab9883-eb0b-477d-b0f8-3f706a7c3082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "d800a6f4-7839-4d06-9319-81367653453d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 258
        },
        {
            "id": "740a780b-dfd3-4b01-ae80-56655e021738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "36083d98-a260-4c0c-b10d-2e433f51979c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 260
        },
        {
            "id": "60c43e54-dd19-4018-a3e4-3b008084a8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "7aa674e8-f963-412f-a5b3-ecbb0f9737ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 262
        },
        {
            "id": "f151c373-f296-45cb-a1f3-aa0a7a422fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "8b7e7f1c-8aee-4cdb-97b6-00befa151d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 264
        },
        {
            "id": "300779fe-0804-4543-a3e4-301cc2b69076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "1f323524-6eab-4c40-9801-1857802defdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 266
        },
        {
            "id": "f80f76f1-3e0b-4438-a10d-5d7cdbc2f12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "7e0e5f44-548d-4668-8bfa-36db9394f3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 268
        },
        {
            "id": "55938df0-c63f-49b3-8a53-c73622020512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "f627fa5c-4929-4e15-a8ea-a3879c4b589c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "a6edf846-67e0-4253-af90-24a09b6f8423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "885ebf96-78cf-400f-bfa2-2cd55440c26a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "76fb1429-63f0-48c1-a4b4-092099c3d5b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "f61cc693-b847-458b-a604-39e222e3c2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "a64d24db-b4d3-4315-be83-ccb3fc04d1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "66692566-8d60-469f-af3a-95be8dce00e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "870a673d-6229-422c-85b6-e6e5ffc2f460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 284
        },
        {
            "id": "573495af-6033-435a-9f83-7a8c41e5bf82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "56dbf170-c79f-4029-b5f3-942b1bcac707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 286
        },
        {
            "id": "8d0bd9fa-9bf8-457d-ba01-3a2b6982cfc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "c1e0549a-7901-45b2-a02e-b0727b11d016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 288
        },
        {
            "id": "886632c6-a8e1-4f10-b176-a76bea820da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "2c996dac-643b-4e5c-9726-09eb2283d4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 290
        },
        {
            "id": "436fcd9a-c274-4a08-8115-64fca9b1f252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 291
        },
        {
            "id": "024b8bef-9d6f-4ee1-969e-070496033cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 312
        },
        {
            "id": "fecdf229-b637-4b9f-b918-dd1187041602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "f98c22fd-7d47-4aac-bbd1-ae822947eee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "c69eeec7-c877-4d13-b142-82b388077268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 331
        },
        {
            "id": "fdd5f1cc-1004-4fcd-a750-44b3a7c2fce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 332
        },
        {
            "id": "6ed801e1-8fdd-4d7b-af7c-f1ae502f4c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "623d4629-e075-4736-80e1-c29f9f22b312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 334
        },
        {
            "id": "c62fb84e-e6ef-4498-bf88-b5bd70758009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "441a83de-4002-41d2-92a0-aad87ada8162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 336
        },
        {
            "id": "2034f949-badf-418c-b3e5-e10111878b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "d07d46c1-e760-4472-b538-4634ffa8daf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 338
        },
        {
            "id": "3dd23a72-6683-4f9f-aee2-7c359b1ce775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "51f8c403-c6be-4906-8192-c1cb13b14e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "c89bf658-0984-482a-aa12-616629cbc0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "4059eb23-6d31-4b45-8a6a-b4a249419daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "32d507de-d3e2-40a3-8e8d-0af659b02a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "bcb05602-8aac-4cca-9afc-e6474638b8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "53de51a5-d0d9-4dad-bdc2-e7e4414e5446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "65a5b158-f986-4e98-93c9-da0ae2897d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "0bd7cdd1-21c6-458f-ba06-299d7b254a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "a9a60163-fb04-41eb-b624-cd77fcb882ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 369
        },
        {
            "id": "6ced68a7-c2c0-4e49-a4f9-1cd96ed734e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 371
        },
        {
            "id": "f6c740f1-9998-427e-b008-93d18faffc70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 416
        },
        {
            "id": "520866f6-347b-4689-8180-16a42358b25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "e00ddcfd-ca7e-42ff-af51-2441432b7ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 432
        },
        {
            "id": "b952c574-7aa1-4d2a-8df9-b4c7b04d9555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 506
        },
        {
            "id": "ae6701df-d16c-4e08-91ae-fc4ae7e22567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "ba4be016-2003-46ee-b65d-4abf9d6c61f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "11422c10-132b-4efb-8933-b23cc2a19c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 510
        },
        {
            "id": "43180b83-aa5c-4808-83d4-06b0952fccaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "c22d942c-211d-449f-9aa0-a1af8ccd6426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "68859ea9-ec13-48cc-9b9c-30b7ec7104b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7680
        },
        {
            "id": "615be6f7-5518-42aa-b001-958c5916d047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7681
        },
        {
            "id": "0aa168ae-1fb2-4ada-b020-40c750b88821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7743
        },
        {
            "id": "c9571557-cd8b-44ce-b82d-fd5c4caadaa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7840
        },
        {
            "id": "1d544ffe-888c-4343-a247-1ace253616d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7841
        },
        {
            "id": "91f0066f-21b9-4e39-8e41-e2060aa3cadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7842
        },
        {
            "id": "c4623315-55a4-4832-b9d7-6575ed232fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7843
        },
        {
            "id": "7f4344c8-c127-416e-9c19-cd87d18ba4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7844
        },
        {
            "id": "f56ceb01-2381-46fa-a6c4-aa6dfd28e89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "44104cdc-d249-4f33-923b-1bb2d996c32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7846
        },
        {
            "id": "58e09d98-64a5-4ac7-ae4e-0e46f795b681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7848
        },
        {
            "id": "e0e2a51c-bdce-435f-801a-6fa7909d79cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "d24e8e6c-ee35-4dc0-8be4-50bd2b91d7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7850
        },
        {
            "id": "17147822-fb5c-43eb-b8cf-7a4b8000cd81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "f3a78e0a-3001-4788-8996-34ac3bb54889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7852
        },
        {
            "id": "23bc9842-8028-4094-8996-ddfc74437514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "6827e3aa-c828-4717-bbcd-0bfadce8d809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7854
        },
        {
            "id": "cec27b1a-7558-493a-81dd-d416ee6c2dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7855
        },
        {
            "id": "271bbc1d-4898-43a6-91e6-3db4b9fcd77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7856
        },
        {
            "id": "b21890b6-1099-4c5b-8efb-36cb92aab9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7857
        },
        {
            "id": "94fbdd00-e707-4535-a315-c8c683616651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7858
        },
        {
            "id": "c34e176d-6b96-48f5-9446-3fb867d13b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7859
        },
        {
            "id": "b357e8f5-b5d4-4c4a-b45f-fd68ac6dea20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7860
        },
        {
            "id": "e9b744d4-b199-4905-a848-c70bc68d86f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7861
        },
        {
            "id": "810d2415-e299-4f3f-9d73-472fb648da14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 7862
        },
        {
            "id": "8b6fff9a-e5ee-4c8a-9cc5-382d4e2063c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7863
        },
        {
            "id": "31b7c996-ab84-4702-9ae9-83c8a642f178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "4de7a3ea-aba5-48b2-b504-cd3c2da7458d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "6bfdce4f-6d1a-4bb0-aead-2ec7f4cccfa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "e5140e02-0ace-4221-8b15-cceebc5928c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "5edde760-44dc-4e1e-8498-fb29ffd477ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "10ad6963-7e4f-4525-a0b2-ae27ceb6a9c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "d9d88c0e-9900-4a6a-9c73-69e28bb605a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "37db155a-cc16-4363-9f0d-94067c85e065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7884
        },
        {
            "id": "3f9f9fea-7be6-410d-a621-3914741077a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "fc63110b-a20c-4a10-ad4d-366f6ec92e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7886
        },
        {
            "id": "d7f03283-6a5a-434c-9b19-3547e9168002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "fe4874f2-a1c0-49bd-8dbe-35334e3c1bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7888
        },
        {
            "id": "31ed1bb2-8470-4558-aadd-77a80704f72b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "183019c7-cd14-4d30-84c7-ef489d1f9af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7890
        },
        {
            "id": "9baf185b-6233-4c3e-b5c0-548626f051b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7892
        },
        {
            "id": "8b4fea2f-134e-43d3-a095-b7562d82efeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "36b9f147-8a2a-483d-a4e9-09bbb9b81594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7894
        },
        {
            "id": "e7387288-0196-44cb-9f31-e26d468d150b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "52b0c94e-a292-4b07-adb0-d9dab38dbd6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7896
        },
        {
            "id": "bbba98fb-fca2-487f-846c-6a02f189b775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "86b3f8af-87c0-406e-9c21-728fa33796b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7898
        },
        {
            "id": "299fe63c-b29e-44b5-aa1e-0f96a5cc8a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "3d691141-94f5-4100-93f8-26f62a2bf276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7900
        },
        {
            "id": "dbc257d4-b918-4142-89bf-5f8be2dd868c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "a49a7c99-117a-4caf-a8b6-e8102cba1fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7902
        },
        {
            "id": "69d425d2-4ab2-46d0-b5d3-e7ffce6e5a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "5a7820bf-92bf-4d0d-852b-b43d9e877f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7904
        },
        {
            "id": "e4737beb-deb6-432c-9ebf-ba6a47c31f30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "c0b8a42d-b6a9-448c-b9c4-c5913a118704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7906
        },
        {
            "id": "316ed269-76b7-444a-b425-28ee63377f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "0e2e689f-6813-4514-b097-ad2562b81902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7909
        },
        {
            "id": "54b69079-7941-456b-8a1f-3b7fa36017f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7911
        },
        {
            "id": "71182421-106f-4460-a6a4-28f91602b02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7913
        },
        {
            "id": "c3b0bb2b-0880-4642-b033-70013af91fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7915
        },
        {
            "id": "9c7a3575-57a4-43e3-9371-a5110e90b7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7917
        },
        {
            "id": "54858ce3-f8f7-4d1e-b9a4-8bdb08b20661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7919
        },
        {
            "id": "d5a29560-6eee-46be-9265-cfbf75949055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7921
        },
        {
            "id": "fd505369-ab77-4634-8b52-203b08a4a433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8218
        },
        {
            "id": "cb0d27a4-d179-432d-b79e-e27e0d3f14c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8222
        },
        {
            "id": "79f21c93-17b6-4c16-aadd-a50537526852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "5fdc154f-227f-452e-9505-cd7ea9fac371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "62cb0673-0e09-4f2e-856c-39fa33fd9bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 63
        },
        {
            "id": "11841f69-484f-4677-80f8-02e393817922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "f410f489-04bb-4bed-ac08-042ba81711e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "64235423-642a-4e60-bb6b-23526e8d5d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "519b0bb4-5286-4afd-8196-f3c190cfcf03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 79
        },
        {
            "id": "aa0abf66-93e9-4e42-9853-704bd16b14d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 81
        },
        {
            "id": "d02ef5b4-dfcb-462f-b815-5442da94e7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a7a239ab-a44a-4051-a459-a225b0cf0f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "08b0cc0c-ae5d-4304-9e71-d181dec56685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "3875cd8c-6e47-4947-9ebd-6532a4d8c253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "320664c7-8801-4055-8358-4b831b4fec5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "50660056-e179-41ae-aebc-c4216396512d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 109
        },
        {
            "id": "7f26968d-dfd0-4536-8223-2458e6d5562c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 110
        },
        {
            "id": "bbfe4adb-afb8-4e6e-85bd-252ed00a04bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "96c0ca78-6bd7-4bcc-8156-b075b8a0346a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 112
        },
        {
            "id": "b8dfde7d-7d7f-4fce-b9ca-65cd449c8ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "b52ecbb2-3282-4d72-9d88-c521240e60f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "d8cc02cd-65d9-4b8c-924a-2de91ef2e280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "116696b2-b542-448c-bbfd-4cf2c712c84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "67145659-ca8d-46f5-9717-3992c972f777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 192
        },
        {
            "id": "b77f0a23-5be4-4126-bf2e-3a81d44b1ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 193
        },
        {
            "id": "58723e82-f093-4f7a-b700-884260aa60f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 194
        },
        {
            "id": "c4dc7de8-6e1f-4366-b1b3-24ddc6c22953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 195
        },
        {
            "id": "48eef8a9-064d-421e-8ab0-c3250c6d1ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 196
        },
        {
            "id": "bf8f50cb-040a-44c6-a14f-184308c168f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 197
        },
        {
            "id": "b338dbfa-92f7-4de0-8fc4-d3fc0253b50c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 199
        },
        {
            "id": "5ff1319d-ff46-4326-b175-7771530f9baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 210
        },
        {
            "id": "e961c843-0682-4413-b95a-789611750eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 211
        },
        {
            "id": "276552cc-a9f2-40ca-b229-e74bdad3ee6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 212
        },
        {
            "id": "250a8783-3826-41c8-a971-8a6afb7f54dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 213
        },
        {
            "id": "706f333b-ab83-417e-aa96-34f23813975c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 214
        },
        {
            "id": "fb9d9087-9f74-4d08-99d7-5867049e592a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 216
        },
        {
            "id": "56766309-58e2-4af8-83f8-cb351c4e1f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "ecf8544c-e0e1-4005-ada0-7da0a7f6fa6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "5612beef-ae70-4e19-9e04-504f2332e5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "7a59fc9f-fbb2-4a8d-a042-80e2c65a34f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "22c55e95-d299-44c3-b0a9-be704c5fbff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "cf5a3052-c138-4c1e-ae6d-4efdaf40a678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "58a3cb69-5e22-45b3-b391-44661efc2449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "521e8026-26a6-4928-9b96-0c607a05e965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "9f845b3d-3476-43e1-8f5a-9d09f9a64cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "611d5dbf-51b2-453a-bcf6-e61a920648d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "1d072db9-f884-4400-bf35-bcad020c1d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "60816a58-2186-4c5c-921e-d6c305dd9ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "be4e1080-c584-42b5-bce0-5aaee5c61b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "f83c4cbf-82a7-490a-bf7f-6c82ffec0ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "2d4febbf-0c01-458b-84b5-2937574af4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "ac55aece-280b-40cf-9a06-99c2b2fc0e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "a08cda1a-c455-4e88-b954-d74a0492f987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "b03620d2-8952-4b25-8e41-042dc13209bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "9dbd4b66-728b-4434-bda2-1c964104ba6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 249
        },
        {
            "id": "c2bac7eb-00de-4949-b02d-1c9ece650e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 250
        },
        {
            "id": "18e9f7c1-940e-47af-99c0-b1021bfe277a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 251
        },
        {
            "id": "d777907d-4335-4e2e-a789-e29892640d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 252
        },
        {
            "id": "36062a84-ec12-44c9-9b46-5daaa675c072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 256
        },
        {
            "id": "a7fe9858-210a-4835-957a-591e8731f56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "81bae1bf-6242-4f55-9d6a-51e1cea7f5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 258
        },
        {
            "id": "5c9ca2e0-d3ab-4b2e-90b5-280e31614a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "9f692350-364e-4626-80b3-c84535efacbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 260
        },
        {
            "id": "32b13bbb-3e6e-4645-a410-fefc58ab15e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "97653d2e-15d0-4223-aad2-bc46a2473032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 262
        },
        {
            "id": "6953f481-8b0b-42a8-9310-b6623f12012d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "28328830-3e96-45d9-b225-1e9d94539859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 264
        },
        {
            "id": "98ea7055-3e3a-47b9-9951-2a07fe2142c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "d01dcc1f-5dfe-440c-87d6-08c6ebd417c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 266
        },
        {
            "id": "abdc4c1b-e1d1-4fbf-9279-eaecc2923300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "d6ca4917-67aa-4323-a0fb-c9e7cdf2d758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 268
        },
        {
            "id": "16989b0a-2b99-414f-8c4f-559fee5c6528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "c08fccbf-8176-46e0-9169-d1c2eea86e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "162c3a71-2b43-4f78-b6ea-e365ad0006e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "1201c5d7-f4da-4f1b-9de0-d0395dffc589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "59c8e7a9-b86c-4fd6-9a77-3f4d382e9e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "ef857c23-3ba1-4c99-9181-75165ddeee1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "c275a4c0-11b2-45ec-9b90-c70cdf76ca81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "01277e0d-787d-4643-94b4-5cc8347d74dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "1c22fd29-636b-496c-88bb-e5afb51f9855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 284
        },
        {
            "id": "e7fa3d27-329e-48ac-9571-3b209e21b461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "39eca7a6-c489-467f-add9-0f1b901a0ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 286
        },
        {
            "id": "751c79a4-1b55-483e-8286-3c41e9fd2cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "301009f8-ffe6-4ade-b862-cad8f5ae3a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 288
        },
        {
            "id": "84f9beda-72ac-425e-9871-f3c7d397bf57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "50155b1c-41f4-4729-a796-a042a324936f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 290
        },
        {
            "id": "0f3e3022-e12e-4d31-8069-326bede43798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "3d3dbc0c-1b02-469f-afbe-b3b649ff8d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 312
        },
        {
            "id": "e7744b7a-35ae-4fdd-9da2-9542855d0ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 324
        },
        {
            "id": "02e4790d-b11f-4266-a7d5-9374ac49d43b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 326
        },
        {
            "id": "43f4cb6e-a649-4602-90f1-dba2eb568c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 331
        },
        {
            "id": "33057a28-1f89-45ee-af89-5047f05abc87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 332
        },
        {
            "id": "9570f5ad-f806-486e-80a3-00cb6bd8298b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "3aa28b80-2bd9-46f0-a270-d8a1b796f01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 334
        },
        {
            "id": "c9a561bd-da12-4cd7-8025-d48d16d53772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "1782f189-df20-46ea-a071-810ddebf02af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 336
        },
        {
            "id": "8f43a99d-eb57-4a91-b504-f0a235811fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "ec0ef506-3b3d-4624-b62d-608e6d6ed115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 338
        },
        {
            "id": "09d5fef7-964c-4661-be41-9db89c73a962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "247a521d-9bc9-4236-b0d8-c888c21ab57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 341
        },
        {
            "id": "636f666d-566f-41fe-9123-70fbcd03d1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 343
        },
        {
            "id": "ecf386b3-f6c6-4716-b196-ba9309eeb857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 347
        },
        {
            "id": "1ce5b275-be2f-4ef5-bd1d-2b69cfd40da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 351
        },
        {
            "id": "c9cb3f78-2459-46d1-86c3-504fec5d2d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 361
        },
        {
            "id": "bdfdda4f-8a2d-4a2f-a201-96d7e26a1c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 363
        },
        {
            "id": "8bd9eb18-0137-45bb-8fe0-382c8e86f2e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 365
        },
        {
            "id": "bf9c6296-655f-4b80-a634-6564769742a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 367
        },
        {
            "id": "c7de0ca7-ff73-4b92-ac49-feab24af4ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 369
        },
        {
            "id": "239c3498-1d9e-4618-b906-bd465d3df23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 371
        },
        {
            "id": "0598eef7-af9a-420b-9acf-49db7df5b538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 416
        },
        {
            "id": "9129f7ab-8db3-46fc-a2c0-066ee6db1916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "464693f6-70ae-4649-810b-7e08d5d42189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 432
        },
        {
            "id": "2213c4e7-e3bc-4827-8909-20c80617dcad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 506
        },
        {
            "id": "e6016787-5f5f-4a23-b542-5eef872c6744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "d6c8058c-e566-4928-b0a6-6bac33c3bc8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "9e5c972a-7d7c-4146-9253-056d90981673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 510
        },
        {
            "id": "5b1f45d8-7ff8-4c0f-a6a4-f605f94b8344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "ff439afa-854e-4f76-ac52-f23c7827e701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 537
        },
        {
            "id": "d28382eb-549e-4592-9be8-023a9a8b9fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7680
        },
        {
            "id": "d6b9c077-cf7e-47a9-97a0-659df3067ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7681
        },
        {
            "id": "8cfaf98d-de7a-4d0c-944b-dc2eff2c296e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7743
        },
        {
            "id": "0463ad3c-f7d2-4e87-932e-286f44d15ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7840
        },
        {
            "id": "2244da5e-8568-45ea-b86b-c684854f154e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "0f148cc1-2c3b-41ca-a489-845464c928f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7842
        },
        {
            "id": "ae6bf7e6-da78-4353-be30-8aad8f857c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "496424c0-dd56-4f33-9543-f280e0771ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7844
        },
        {
            "id": "eed9a4dc-98d4-42c6-9391-0e1f7c956863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "f4c7d3c8-69d7-4a00-b91d-ae8cd71417e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7846
        },
        {
            "id": "0d3be0e3-f693-4b19-8fe0-f6e0a3247989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7848
        },
        {
            "id": "b20df190-6b36-417c-9167-17f70141b188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "e8f25f6a-23d6-45e4-9e8b-5f802250f2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7850
        },
        {
            "id": "587b5471-0b9f-45c1-9d08-b09762f08e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "4681ab92-9388-4ec2-9b97-ed932be7c5e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7852
        },
        {
            "id": "dd3973c5-0bb7-4be5-ac73-add54352d185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "9945d42b-b90e-4c27-a013-7da4c7b556ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7854
        },
        {
            "id": "6b49e57a-bee2-4de1-b14f-4daacfd0d08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "b88c9c46-9adf-425a-af54-8cc26ef40431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7856
        },
        {
            "id": "4681bfe6-88ab-41f2-abda-1c7e4d698446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "e7dafd52-4b1b-4db8-91a2-2ff4372a3c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7858
        },
        {
            "id": "24021858-cdf2-487c-b920-b0c2e454ed53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "4ab27502-6166-4721-919e-12f8f548686d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7860
        },
        {
            "id": "c62ab227-93b2-4015-866b-e139b424632c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "48ed9556-9b9a-44b5-9c00-ed34373580d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 7862
        },
        {
            "id": "972e71a5-2d2a-4f35-8a8e-593a12f1aa77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "70bd01fa-0ef8-4a84-b1a9-04d426aab850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "d77fdd7e-f231-4199-801a-a8a7cf1149eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "763a2201-e420-41ab-af53-78ffa8992c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "51ef60ec-6d66-4c70-8bca-3be66421b5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "cfacd1f8-6256-4497-918c-565627e20be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "778ba717-5186-4bf2-b194-305ade6970c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "3852cb68-b10b-45c0-b5ad-837177965bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "5a80d5d1-3324-4b22-81a1-435c940aa9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7884
        },
        {
            "id": "73ae4b63-7a22-45ab-8a30-5c89934134b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "14cbc62c-8e65-4de4-bd4c-a77e12b08a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7886
        },
        {
            "id": "12e1358e-b23b-4f27-9c57-90b6d8f7a35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "486c0c2b-32d0-41a1-b526-c3aeba5ba8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7888
        },
        {
            "id": "a3cb80ea-ff7c-4658-bb75-b76e50126ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "f9a376bf-ef31-4c11-8ace-e38c5838c2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7890
        },
        {
            "id": "9922aedb-ca61-4c75-8e7e-6b62db3759b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7892
        },
        {
            "id": "ea3bdcae-b6eb-4a01-9906-897ba0e6e0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "f43a88e6-b3c6-4f41-b14b-1422d2b23f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7894
        },
        {
            "id": "749378be-850a-457a-b578-105ab1a63a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "86c8ea79-112f-49c8-88d0-9a8eaafdfd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7896
        },
        {
            "id": "a434bb80-9daf-4a12-a7fb-9a061a419644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "abd602bb-1aac-4c22-8384-2a0961f8ebd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7898
        },
        {
            "id": "8a49335a-4fa0-4198-9c44-2f5f39309e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "77ef696e-79b7-4ddb-87a3-15f67b6afbff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7900
        },
        {
            "id": "6aee5f47-161e-4054-ac0a-8db74b7ea0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "b7bca578-4e67-4177-a897-80f62640dbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7902
        },
        {
            "id": "009ec05a-d6df-4751-a7ed-8179bfe20e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "dd8c5bc9-73b0-41c5-9a8f-348b7bf08149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7904
        },
        {
            "id": "844519e6-a9c4-4a56-845e-4bce204d3c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "31179097-3057-44dc-af1e-32651c68d59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7906
        },
        {
            "id": "59f59c80-ef68-4ca4-916c-dc4aab0f414f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "43ae3ee7-4da6-4bd3-8f67-4d25c45433e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7909
        },
        {
            "id": "e95aff92-0792-4847-9ac1-91d9ec23d8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7911
        },
        {
            "id": "a8e00602-540d-46e8-9798-ac4674948499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7913
        },
        {
            "id": "c76ec6e6-9fb1-4782-b87d-dbf49838f98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7915
        },
        {
            "id": "ddbdcce7-8b23-417e-a412-4fa3d30d2bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7917
        },
        {
            "id": "fde64dd8-eda2-469b-8688-55b55f39338c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7919
        },
        {
            "id": "12e5d9d1-a769-4ef4-a1e1-14bc8d041638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7921
        },
        {
            "id": "87d26914-9fc4-470d-a447-d5933828341e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8218
        },
        {
            "id": "c698597a-f270-4797-9c2b-4e9bd554a74b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8222
        },
        {
            "id": "a09e9b9f-be61-4653-9318-c39059349c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "48c94673-2dd8-4f9e-9bb3-3324c56e67d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "256bdec5-565a-45b8-924b-a7eda3bb458b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "4162a372-434f-4fde-a4e9-f2cecbc80034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "b6e46c98-a9bc-496e-ba51-fb62fe1efd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "1177b8b1-a40b-44c7-b21f-e3a1f1b6ff98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "3ac6203d-14c1-4009-8de9-071801efa927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "8ffe2662-bb72-4551-b023-738bbfd4450c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "0bcbf72a-1b9c-4c6c-aaed-009e1f01c143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "65c422c7-dab6-4a60-aa04-5c35f5f89d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "6820db13-aec0-415b-8e0b-b9a17ef07456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "ea1601a2-86c6-4df1-aa86-a593196ce432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "815f9675-b8a6-4c67-96af-00081bc271c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "41c60306-4e62-4a74-a900-7a32393cfba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "8bf8de72-7b8e-48fa-9af7-a3e822ab700c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "347f9ffd-9bca-4e30-90ed-4bd60e53ed66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "fcfc5eeb-5a4a-4a63-910a-e231ac3f74b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "42b31399-1f71-469c-b9f8-b63cf081c656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "40e4c45e-625d-4676-9345-7453687d87f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "3bd4493a-d73c-4527-b43c-219efab85667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "beb7c59d-b498-47e0-87d4-b1377a358341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "d37cffef-228b-4153-b21e-e73d2c134817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "5a6feeb1-0274-4fdd-b9ca-c6e9eff46fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "cfbf2b0a-23b5-4170-b1d7-9293fd43f0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "67bbac6f-7240-4b3b-a9ff-c62709343597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "dca22f99-1390-4176-81cd-900809691af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "91cb9264-3e97-4330-8ddf-09dfd1165e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "829eb8c6-2c5d-40fd-8c42-33c163a4dc98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "be15f879-4d6c-49c0-a38f-c772a7508be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "972f1549-8b2e-409a-894b-c07936fc9a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "1769af51-0e66-4c3b-b6be-50b7247c42ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "ff9ff7c1-6672-46c4-8b03-c01c2b34a91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "511b5324-0cb0-4aa1-a76a-bad663396103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "1058c24b-1ae8-4f9f-ae76-2aa9155f7c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "15756d83-efa5-4e63-85d9-5764c135cfaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "b8f7055a-8878-4bbf-a6b1-b7365a143ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "331ba206-e99a-441a-86e0-097054433795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "69371143-6d76-49a0-9715-baa13b871e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "b79a9156-3d34-493a-ae0f-4be26e4eff35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "582fcaa3-9eab-450c-9443-91d09ceb765d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 63
        },
        {
            "id": "65fd7a3f-a126-4c1e-8a90-9a011b5d6234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "431416cc-fc7c-4896-895b-4b19eb538c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "2de55c5d-6ae8-422d-8533-499e083d0c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "feaab8f3-68d1-474d-82d3-1a84f483a209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "86aab12e-9e25-45e8-a2b6-d2a2a81d4b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "85bd4b51-4387-4b53-aeb4-62acd307a8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "1b55ff44-f287-4d94-97d6-0636f05a13df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "b092bbdb-5fd2-4945-89b3-68c64c928d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "0f2603e7-a4d1-42b8-b6ee-c4ae1edaaf04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "92b457d7-4ab1-46a3-98f6-57e04f115ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "3f6ca02e-af42-4210-a130-65b205eb83eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "e7658713-a942-4468-a1ce-8e851a1dc0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "acb45173-0cc2-4724-aa95-45fa0ecc3754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "12631dba-0b16-4645-9629-88448016e601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "48b09b31-f71c-4046-98d0-3cd5e4bf61ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "0ef8fcef-1ebf-4db3-8d36-d7e08e6bc184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "30ec5e79-bb8c-4b3e-85b3-9bb21808f8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 115
        },
        {
            "id": "0770bb32-5701-49cb-a6bb-3f7c0ddf8ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "9aa6dcce-3a27-4800-80a5-9c860f3bb4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "ac6f9d29-2ff2-4f1a-b3ff-29918bc98c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 192
        },
        {
            "id": "b7edf1c4-d5dc-4ee7-8e67-35da324b0960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 193
        },
        {
            "id": "0ae26ec0-6bee-4b3d-be5e-100b6840e9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 194
        },
        {
            "id": "cbd37da1-d847-4ad9-ad30-218612308f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 195
        },
        {
            "id": "dc61d4f3-cce6-46b0-9ed8-dce2f65f26f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 196
        },
        {
            "id": "137e44ac-3f9b-4bb0-9751-28fd6e54f54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 197
        },
        {
            "id": "02354e67-d1a6-4b07-bec2-75571aeed748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "9a5530b4-8264-4491-a4f0-7476d4c920c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "65135241-4ea5-411f-85ea-c11b921150df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "a17cd1c4-a3d6-46dc-b001-983ac26c86e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "74643a74-8442-40e4-b848-7ef89bb4843d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "87f80437-7014-4d2c-8c01-9384b44e05c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "2f25869b-31e2-4503-b56e-5d819bb3a58e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "3c774c9c-4c28-4767-a447-182be49f5f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "710905a9-75bf-4c80-bd62-ad1e8a61b034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "d3802e0a-79b2-485a-b758-01ba6b041a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 226
        },
        {
            "id": "4430d0c8-9b0d-4ba5-9f1f-b9b1617ebfa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 227
        },
        {
            "id": "2f061b5b-ef4b-4889-a3d3-3ad772a44e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 228
        },
        {
            "id": "332f5946-d96c-484c-9319-10ef1647d315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 229
        },
        {
            "id": "dfd3bdbb-9ed7-42a1-8d05-a84f48f1a924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 230
        },
        {
            "id": "5c7c4479-33e8-46c6-8093-55a84d7d7d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 231
        },
        {
            "id": "02c16b3a-bb0e-4286-9ebf-1ce43aa2099f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "30aabd13-8c7d-4e40-8d89-f8e504f11c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "73b599e5-c36a-455b-9264-2231fe35e517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "02bb4bf6-e7ee-484e-bb6c-fb1e2bf87e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 235
        },
        {
            "id": "d08e3602-5b10-48d1-aa34-584c459bd397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "d82ab34b-294c-457b-9c4e-cddbe6089d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "733d8e15-ae68-49ee-abe6-13a10c80e571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 244
        },
        {
            "id": "4c23fd6d-9b99-4fe6-ab74-023dcd4a60d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 245
        },
        {
            "id": "f2593cca-1ad1-49dc-ab03-ebe0ca59046b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 246
        },
        {
            "id": "d8479ff9-22be-43bc-8f28-c4ce32ba4836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 248
        },
        {
            "id": "e984a9bd-9489-47e9-9a0d-b232135f9c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "bd55c2e2-9a51-4998-b78b-abdda830eef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "2458f466-ecf8-456b-91cd-15b60dc7c1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 251
        },
        {
            "id": "f32cbe58-df0b-46a3-93c1-4475c5fd9046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 252
        },
        {
            "id": "5076f0eb-27e5-4ca1-8d8a-52a0f2cd17d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 256
        },
        {
            "id": "9f591ff0-9701-4911-bb9f-6bd2fd5f5321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 257
        },
        {
            "id": "41bddca5-9908-45af-beb0-c7001f020ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 258
        },
        {
            "id": "9d357069-40ae-484e-87b6-45c255291bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 259
        },
        {
            "id": "eacabb7b-8915-4a14-bf4b-bb4ccb2b65e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 260
        },
        {
            "id": "8c7ca21c-3bd9-4974-a5c7-e99b29874ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 261
        },
        {
            "id": "bd6c7c11-cbc0-45c1-b018-075347fd0dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "b4d61a05-fe01-48ed-ac54-e9f200f61618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 263
        },
        {
            "id": "211d89f3-dd56-4916-9b06-fdfb6a5b52ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "bf15d27a-21af-4fc3-b350-c4f235fec9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 265
        },
        {
            "id": "f640d166-669b-4914-823f-2080823bfb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "1e7d8df3-e9c8-4f2e-8c36-26b2cea4e04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 267
        },
        {
            "id": "31d6fd2c-714a-4992-bafe-928903ad9d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "860ce78d-ef0a-4c90-8cdb-38fd3e98eac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 269
        },
        {
            "id": "89463f7a-c4f1-4b6b-b36a-45b2e3dc5a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 271
        },
        {
            "id": "446fc310-d96a-4228-a795-9c636e58b789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 273
        },
        {
            "id": "c5c9b321-3626-4df1-86c4-1c813ef29b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 275
        },
        {
            "id": "243399f3-a3b1-4ad7-88cf-808f7f310038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 277
        },
        {
            "id": "41d9a5ef-d4a6-4784-903c-deff036a01a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 279
        },
        {
            "id": "0ed9e76e-42b3-4a59-9360-400f3027d3b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 281
        },
        {
            "id": "44ec6dcd-eebc-48de-8066-fe367b0f8050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 283
        },
        {
            "id": "98b203a9-65f4-4fb6-ae69-d6cc0cfed884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "989e8954-3349-4219-aed7-ae172141c2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "d06039e7-5e2e-4920-bae5-5f56cd86b763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "22844a04-1612-4196-b776-0a676f74cdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "a39a69f7-186a-45f8-823a-6400afc6c974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "3154d5f6-793f-4c85-958d-bf3844fb55b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "36ae7600-171e-41d7-82c2-aff9013ddc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "a03b1fcd-546c-43e3-8fb9-d6ce84659dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "568b702d-e52f-4d9e-b7ee-05ea5a4b2593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 312
        },
        {
            "id": "93134e9c-256c-4292-a8a2-2cb4f0b276d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 324
        },
        {
            "id": "697b77ad-146c-4163-b0e9-dc6ef08d5979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 326
        },
        {
            "id": "408018d6-2c1b-4356-b855-6a2cd9e740e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 331
        },
        {
            "id": "cee0c383-d38a-4aa6-8dfb-6493b9373565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "4073bee7-7fe1-4ca1-bd10-a50c06a90562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 333
        },
        {
            "id": "e5ebb498-10c8-414d-8c89-c87c6a16b291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "93e56c05-44c4-48ed-924d-3e89ea79216f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 335
        },
        {
            "id": "8f053afe-492e-413a-97d2-a5fcec312b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "83b3fa69-54c6-419a-8876-ce2ecadfb6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 337
        },
        {
            "id": "ebd7f34a-989f-4122-98dd-14cd92785abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "5e9f024b-c337-4fa6-a996-85b4494b8da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 339
        },
        {
            "id": "61a3bff4-fd20-4294-8f7b-60c923147399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 341
        },
        {
            "id": "df99615a-a709-482c-9aa8-aa64e8a89349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 343
        },
        {
            "id": "efd2a682-e1fc-49fd-aee1-23f52524846b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 347
        },
        {
            "id": "85ed74d9-e0d9-442c-9d5c-efa0f69524d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 351
        },
        {
            "id": "6a9e1e2f-f590-4fdc-ae5c-c23af128249d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 361
        },
        {
            "id": "80cd21ab-4c98-4885-81d6-775b7721338d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 363
        },
        {
            "id": "79afcbe1-9474-4b72-88e7-e50dd678cfcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 365
        },
        {
            "id": "3546ea22-1c30-4cdd-9f57-761e2bb22dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 367
        },
        {
            "id": "f1cd42c3-cce7-4b47-be60-2224acd51b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 369
        },
        {
            "id": "e6d86466-8557-432f-b9f3-75885af2a69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 371
        },
        {
            "id": "b4ee32dc-d2b2-42a9-b20a-2ef7ff4c985d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "e40013df-2ef6-45f2-bc72-7396c225d657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "72392fbc-351f-4bba-a328-3ff84ec6f90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "9e05532e-c696-4f1b-b567-b04d636af9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 416
        },
        {
            "id": "3a493edd-33b2-4298-9872-44223014c622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 417
        },
        {
            "id": "f04f92b9-0a55-4ab0-88c1-a9d87fc824ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 432
        },
        {
            "id": "48fcef6c-041b-456a-8104-ec0822cfac7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 506
        },
        {
            "id": "6b31703b-233f-4992-a6d0-34d69bdd9997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 507
        },
        {
            "id": "6cb72c3a-3eb0-49c1-87df-85c304437e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 509
        },
        {
            "id": "87d6e456-ebcd-4b0f-a9e7-9aae88b8c78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "ef9a9060-6525-4794-8b33-3b4203d81c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 511
        },
        {
            "id": "e2e68dc4-3558-491d-9878-75eb20a90271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 537
        },
        {
            "id": "2979599e-8c5b-4fde-b772-9a7a51f188f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7680
        },
        {
            "id": "4ec76121-2570-4cde-a040-b1c64c4f5e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7681
        },
        {
            "id": "82f92515-93e5-4020-a1c2-7f5159ebccfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7743
        },
        {
            "id": "2c51982b-ef2d-4ab5-bd46-bca3ba84d604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7840
        },
        {
            "id": "9797b0ed-7fd4-4d14-a3bd-535455027f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7841
        },
        {
            "id": "851404c6-b210-4262-a78b-48a94ad32cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7842
        },
        {
            "id": "b17b4e18-0aed-4976-882b-aebdc9b0f345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7843
        },
        {
            "id": "68d60f36-0e5d-4b21-9156-9f552d4ab66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7844
        },
        {
            "id": "80c66326-c26d-4744-afb4-8627853099aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7845
        },
        {
            "id": "9c3c1643-3b0b-4c9b-ad2e-2e0c75892c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7846
        },
        {
            "id": "ec4b269e-7e0a-4254-8a38-fd2b26eca3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7848
        },
        {
            "id": "f4ed5e6b-2c5e-414d-a4be-8711788240ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7849
        },
        {
            "id": "1b7855ec-c626-4b73-9cc2-5abf641c9a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7850
        },
        {
            "id": "fded4be4-828e-42b6-a5c2-5a867700b478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7851
        },
        {
            "id": "a9e580ee-1781-406d-850c-230a5a54d1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7852
        },
        {
            "id": "f77900c8-7d96-45c2-899d-28841a31246b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7853
        },
        {
            "id": "1dea3364-0a19-4194-a692-d26e5b1e63bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7854
        },
        {
            "id": "eb2a1876-831b-40c3-99c5-1a7df7b88458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7855
        },
        {
            "id": "8e34acfb-5376-4c82-8e81-564f890c1445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7856
        },
        {
            "id": "903a7755-48b9-4b45-a654-4e3489b746bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7857
        },
        {
            "id": "26876abd-c42f-44fd-9ef1-df3b376312ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7858
        },
        {
            "id": "2c208422-51a0-49f0-af2b-442742bf7b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7859
        },
        {
            "id": "4e865664-5b54-4a80-a273-7b1039155db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7860
        },
        {
            "id": "07d13115-8428-47b2-ae32-041973752a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7861
        },
        {
            "id": "03cd04cc-317f-4d3a-8446-21d8273711cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 7862
        },
        {
            "id": "90bb1ce5-a8e9-4b5b-a9eb-755b4a5046fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7863
        },
        {
            "id": "0146cc77-fcdd-47e1-9b28-58c6abdcdabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7865
        },
        {
            "id": "7d6a54c9-1647-42c9-bada-89293fb9eb96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7867
        },
        {
            "id": "329a28ae-03d3-459d-9ffd-d3696b3f2efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7869
        },
        {
            "id": "ac9ff047-c4eb-4440-a8f7-2280f05cc3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7871
        },
        {
            "id": "aaeb2cd0-340a-4392-9287-a4134043b638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7875
        },
        {
            "id": "64b097cd-e4e5-493f-a39e-999a71656a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7877
        },
        {
            "id": "5921dd84-70ef-4354-bb8a-92ed4731fccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7879
        },
        {
            "id": "85474cc2-b482-40ee-a312-2766f70e6605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7884
        },
        {
            "id": "0db6ec87-7c78-41e8-beaa-417e55bdfb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7885
        },
        {
            "id": "45fa2f06-3637-4c8d-81f6-dbaff5114aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7886
        },
        {
            "id": "21111fe4-ffee-4d05-9023-e093fe3c8dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7887
        },
        {
            "id": "e3ec9524-bac6-4cd9-a3bd-cbefefe488b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7888
        },
        {
            "id": "5d860bbc-6250-4f80-8b85-e8d4c15a73bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7889
        },
        {
            "id": "57e27a5a-2957-41a6-9731-c092d2212f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7890
        },
        {
            "id": "3b97a6a2-faaa-4397-89b2-5b175895764c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7892
        },
        {
            "id": "cd993560-6842-41df-879f-affc171385b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7893
        },
        {
            "id": "7eb95b29-028e-4667-b688-8b9b4248810d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7894
        },
        {
            "id": "d5522d45-e17a-4c1c-b7d7-9b30f47d0fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7895
        },
        {
            "id": "95062fed-4fd5-4ffe-b8ce-e2fc02d6d71c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7896
        },
        {
            "id": "e7d9922e-59d3-458b-b818-0ef193940715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7897
        },
        {
            "id": "7a413656-9bbc-40d1-ac3b-c8da62cf8c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7898
        },
        {
            "id": "4c0e8c48-0098-4649-9be0-02e4a3cec105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7899
        },
        {
            "id": "de222807-086b-4afb-bca6-16c2f0376c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7900
        },
        {
            "id": "10a24936-5b35-4026-b484-e83f0898a8c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7901
        },
        {
            "id": "08154c98-1ede-41bd-b875-cc7f522c0280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7902
        },
        {
            "id": "20952f7d-b0f6-4c0c-ae31-ec24846de930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7903
        },
        {
            "id": "de9dd785-aaa0-412c-a5a4-27238399e23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7904
        },
        {
            "id": "78883d1d-40b1-4499-bafa-79fe08866837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7905
        },
        {
            "id": "181ccd3a-f59a-493f-8f8e-1a9df624fe69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7906
        },
        {
            "id": "98971376-e6a7-4568-a281-35aa954fbbf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7907
        },
        {
            "id": "d1a6ce2b-8420-4a17-9d95-29c6312920dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7909
        },
        {
            "id": "c816b744-7804-4f80-a23c-ec55b9a0d4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7911
        },
        {
            "id": "619d2796-a9f6-4507-9f08-43e5b3069952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7913
        },
        {
            "id": "c21e843f-6606-4fb7-86b2-bd0f4e1b1f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7915
        },
        {
            "id": "5aa00862-c348-4620-ad76-4d4de0e8345f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7917
        },
        {
            "id": "89e5f37f-3c2a-4adb-9ccb-5bf5c46afac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7919
        },
        {
            "id": "19bd91e4-d738-47bc-b059-bc38addf6135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7921
        },
        {
            "id": "13fd5bd6-5cfa-4a40-9f37-679f520a82c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8218
        },
        {
            "id": "59d42f88-0129-4242-a1c9-8376d7f3a515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8222
        },
        {
            "id": "9db44123-1402-4fe1-b51e-68073a0ef370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "f9481710-da62-4d79-b754-c21cad5e8fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 71
        },
        {
            "id": "4db09564-d1aa-47a5-b1f4-c333fc4c8508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 79
        },
        {
            "id": "4a80ab4b-e9ca-43ee-b952-592ae50a998d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 81
        },
        {
            "id": "4fbdcf94-cf03-435f-8126-09b575bd1a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 199
        },
        {
            "id": "b4c6be3e-97e3-4076-912f-9a8de1e10f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 210
        },
        {
            "id": "71836f01-0a70-4754-8ff0-f2c4749d495c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 211
        },
        {
            "id": "fd898287-47e4-484c-a388-ffbecec98ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 212
        },
        {
            "id": "f7a02c50-b95f-4387-92db-c4582711dde2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 213
        },
        {
            "id": "2846724f-3477-48da-a733-1acb3a1dbaf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 214
        },
        {
            "id": "ebf00daf-ea0a-4c57-b4e9-32bea375c913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 216
        },
        {
            "id": "55241dbd-af57-416f-95a1-76737b9810ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 262
        },
        {
            "id": "fdb716af-1bf7-4dec-b69d-29a14d79b699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 264
        },
        {
            "id": "8c46e8dc-1ce3-4fa5-bd6b-5cb8d52cad5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 266
        },
        {
            "id": "931aacda-62db-4f5c-957b-bd891cbf1539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 268
        },
        {
            "id": "12f96126-58ab-4bfe-acbb-5527100b4ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 284
        },
        {
            "id": "3d4233b0-697b-472a-adf3-8d7b8253b4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 286
        },
        {
            "id": "4ddfea2c-d1b3-441e-b357-9eebfac067dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 288
        },
        {
            "id": "1b59d646-07c6-45a5-a864-fbd4bdc0ee4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 290
        },
        {
            "id": "73d4443e-595b-4fe8-9bd9-100016fb4e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 332
        },
        {
            "id": "2a34e4f5-4b39-4b41-9754-4aa0ec347d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 334
        },
        {
            "id": "e785bc74-376e-4377-9845-98539af58661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 336
        },
        {
            "id": "4cbe743a-e7f4-451d-8aa2-77e452860f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 338
        },
        {
            "id": "c6e4dcbb-9c35-48eb-bb69-40cb4f9eb073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 416
        },
        {
            "id": "16b5fe7d-408e-4184-a5c8-d708c6fd0236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 510
        },
        {
            "id": "45ba14cb-542f-42ed-b783-b31fc80e3299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7884
        },
        {
            "id": "de0835ea-0ea5-49d7-8014-953945e2c548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7886
        },
        {
            "id": "b8c42fd3-08fd-46d2-ab33-bef393dcfb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7888
        },
        {
            "id": "551b258e-e845-468a-95cb-7059729fcb9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7890
        },
        {
            "id": "4998d518-7f09-4b7a-9785-020e84c900da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7892
        },
        {
            "id": "f096dbb2-377a-4f7a-89da-d3849dca3a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7894
        },
        {
            "id": "fb2fe7c9-d15f-494d-9037-6db4b253f75c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7896
        },
        {
            "id": "1455f189-0b4c-42b1-9e27-a2fa49cf9261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7898
        },
        {
            "id": "55846b7d-b21a-4178-beab-b80f2ef9b492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7900
        },
        {
            "id": "f96414b1-c963-41a2-b966-f2e547b948c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7902
        },
        {
            "id": "7f563c3e-4c16-44f8-aecb-3ef683bf78a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7904
        },
        {
            "id": "a38a957a-cb10-4f71-a4f4-bd6038b7417a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 7906
        },
        {
            "id": "64395e10-9997-4150-bbde-45f7694f273d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 91,
            "second": 74
        },
        {
            "id": "0b87d0c4-aebd-467e-acba-f83d19a70fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 34
        },
        {
            "id": "a1870da9-b87c-4928-b202-edd11cbfc52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 39
        },
        {
            "id": "cd6c50e6-c917-4aad-97d9-9815fedaa111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8217
        },
        {
            "id": "ad0791f9-6e7e-4ed0-b31c-ba662008f688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8221
        },
        {
            "id": "3ba51f06-8174-4047-833d-52c7da3a2cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "a4563963-80cb-4b1c-bd10-5da529b2413f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "3973511f-fbb6-4532-b4a5-8acf03efbec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "732f7e9e-2c07-4ec5-a113-5def9d3ff4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "c8161b2c-b4e3-4313-a3d5-79e4773e9270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "e38f5228-a959-43e6-88b6-f905cc0bc008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "236e0ce7-185f-4d29-bfe6-4b1392f559f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 122
        },
        {
            "id": "e4ea1a08-00c8-4c0b-b1e4-d44c8472a292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 253
        },
        {
            "id": "c7bafb9e-4285-4821-a6a2-3d910ddd7eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 373
        },
        {
            "id": "3d6261e3-e273-4b16-86eb-f90dab89f10d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 378
        },
        {
            "id": "a2054b04-21b4-46c2-b85c-cfc085d40c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 380
        },
        {
            "id": "02031cd6-2c85-42cb-bd79-7070401b51f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 382
        },
        {
            "id": "ee102fa4-2bb7-4d39-acc5-266e96514e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7809
        },
        {
            "id": "b76195d1-47db-473f-a8e3-b59e8004d290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7811
        },
        {
            "id": "0d3ac8cc-7699-430d-a2b9-0b5e40871011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7925
        },
        {
            "id": "a3b8265b-b28c-41c9-89db-c0d89667205d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8217
        },
        {
            "id": "f78585ad-0ce3-49cf-8ba1-81a825202a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8221
        },
        {
            "id": "272d3959-5b7e-4c01-b1a7-609c6a56e835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 34
        },
        {
            "id": "b9388be7-3275-4654-a55f-3a9c068468c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 39
        },
        {
            "id": "93ff8739-da39-43f1-943e-cca237c21aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8217
        },
        {
            "id": "f0353e5e-e39d-4053-92ce-d15f45ab8f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8221
        },
        {
            "id": "65279859-d8ff-49a9-9a63-47a10cb71c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "35837748-b299-44d0-b079-2f61f636dbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "4ba3f4b1-505b-44d0-a0e6-351805432fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "9e0c0d54-1f59-482d-bb1a-ca39f6a04f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "441ea065-cff7-4fd9-a238-f462e79019b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "e1815989-cb4c-4247-80c2-6900ecc05a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "3aa55a4c-2340-4bec-aa89-0b81d22696a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "9987199b-6438-4be3-a64f-f072b1edadcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 253
        },
        {
            "id": "86714172-29a4-4812-b326-c4d3786e9bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 373
        },
        {
            "id": "084c0282-f733-4fa9-9696-ef7308852f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 378
        },
        {
            "id": "6b05f31f-c3de-431f-90d3-d3de00b3402e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 380
        },
        {
            "id": "0a2fb319-d68d-4260-b4c0-a1d113b9e0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 382
        },
        {
            "id": "bcdbe27b-593a-4d44-a9f1-7bbf19dfa5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7809
        },
        {
            "id": "f277d881-10b2-420d-bc5e-cb6c71e6c875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7811
        },
        {
            "id": "fe8eb432-6409-4168-97af-4ca4a344e4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7925
        },
        {
            "id": "1b1a9865-cc34-4c9c-b6d9-75c5b9c29763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8217
        },
        {
            "id": "0079e27f-579f-4e92-9466-3bc636fee6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8221
        },
        {
            "id": "17297a5c-52ca-455f-b7fc-1d7e447feb85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 34
        },
        {
            "id": "18ebc4c9-bfc8-4bf5-8d67-d17090d0687f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 39
        },
        {
            "id": "81076993-e095-4489-8d12-e0f5e794313a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 8217
        },
        {
            "id": "f32b1a44-c224-4aaf-8ac6-d2d634dd5bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 8221
        },
        {
            "id": "f12abebc-681b-44db-8a33-26c9db54160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "37220142-b3ec-41c7-85a8-0c709bc5af61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "98a56404-e34e-428a-814f-a2b48f8e5ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8217
        },
        {
            "id": "db5b1816-8057-4be5-b3c7-e8d1def2ec40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8221
        },
        {
            "id": "41c07273-563d-4a0c-840a-c315ce6e9f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "96080792-f8bb-41de-9b85-97e5c52cded9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "48382255-fd55-4a16-8fd9-95627a43c992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "30ff8a40-89e9-49bf-a92a-d7bae56f9aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "24026d1c-33e5-4b17-881a-a89080bbf112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "d4949ad4-863b-4af7-be2f-5ed88ce072df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 224
        },
        {
            "id": "21358840-2c9b-49b7-acf6-d06cb792e0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 231
        },
        {
            "id": "2cff59bc-9e13-4909-ad3b-e33fde768cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "70a76b63-4e55-498f-9a80-80ad9cef607b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "bfe37a56-ed22-4de2-aa91-e31febccf705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "ea5646b4-8b34-4f63-a51f-fa2fd140b735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 235
        },
        {
            "id": "1256355c-ec64-4a23-8bd9-2c16016c154c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "dbeadf36-c527-4a3d-8bdc-ecb670583b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "7ff8f2e3-2227-4837-9eb4-9a8cbf6c9195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 244
        },
        {
            "id": "f0f53620-d260-491c-9fde-49c8d6da435f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 245
        },
        {
            "id": "90bb3ff8-4765-4a17-98b7-f1533c92e3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "a619ad7e-dce7-469d-8a29-54e840a3cad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 248
        },
        {
            "id": "effcdd5f-b02b-4265-b5c3-0ca7ed1f928b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "f68b7bce-23bd-4729-844e-e1a75adc19e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "4574a5d6-d3fc-48e7-bfe0-30397c675d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 267
        },
        {
            "id": "5aa945bc-aa4b-4148-bb99-74e50baad9c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "4489f6e6-c245-4c1b-8b79-155cf2178dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 271
        },
        {
            "id": "50157cd2-774e-4b3e-a945-cbad079577d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 273
        },
        {
            "id": "844fe411-934c-4475-a311-d2e99d06cc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "e7729553-0e73-42a9-8318-a1b6b65bbff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "6032bd05-a1ec-46b5-b48d-70d2b9e947b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "4987595b-38a4-45e6-93af-558a49e5162d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "af6f1bdd-b87c-40c8-8b95-76b5d3ae8913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "7ccafac2-d059-4cf2-b0ee-ffb8fba9c95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "495d657a-680f-4608-83c8-478cdcbd5d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "b074ef6e-b28a-41e5-81ae-5b7c309cc1cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "669cc629-72d4-412f-a9b7-955167f4f55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 339
        },
        {
            "id": "ca22ba5e-3ed6-4cb9-b846-698c815110f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 417
        },
        {
            "id": "3f2fe6af-7ce6-4e4a-805d-3eccd08688c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "9bddcae0-2f90-4c76-98c1-ed1fa1c76857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7865
        },
        {
            "id": "43ca6c5d-f45a-45a5-b40d-f86632e0dd8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7867
        },
        {
            "id": "fd536f42-a1ff-4601-a360-eb0e14948fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7869
        },
        {
            "id": "62de1bd5-8b43-4d5e-9c49-7e7bbdbcf5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7871
        },
        {
            "id": "40613c47-eef7-4062-bbe0-cfcedec57bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7875
        },
        {
            "id": "b49c8e7c-d213-4642-a80e-b95a42908ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7877
        },
        {
            "id": "c993746e-d738-44ed-b609-6e7f500167fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7879
        },
        {
            "id": "593e4907-7391-4c2d-abc5-93ccede3983b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7885
        },
        {
            "id": "c9326afc-9527-4873-b90c-5acba2cf0206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7887
        },
        {
            "id": "572448b8-5cf9-407d-bd67-b18bbdbed104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7889
        },
        {
            "id": "4aec461a-d7f8-46a6-8518-b8e42fba1f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7893
        },
        {
            "id": "30635cc8-eac1-4aff-8012-4504fc0febf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7895
        },
        {
            "id": "57ee27af-99a0-40a8-8107-0f136c5f73fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7897
        },
        {
            "id": "0314ee19-6c71-4395-b3da-5975e99105e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7899
        },
        {
            "id": "b9a56788-2f9e-454b-abfc-c8410d629afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7901
        },
        {
            "id": "777ceacb-fb80-40eb-8a38-138a36989221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7903
        },
        {
            "id": "cce2999f-77aa-4c21-b048-3feccf9f11e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7905
        },
        {
            "id": "7acf1dcf-30f8-460d-93e5-5313fbed1d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7907
        },
        {
            "id": "be32933b-5c82-41df-b15c-89ba2f292118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 34
        },
        {
            "id": "e54bc0df-c699-40d5-b0a4-715cff76d656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 39
        },
        {
            "id": "7156bfad-9180-432d-bf1f-54ee95512bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 8217
        },
        {
            "id": "deb29363-5c11-4cdd-8bde-413ba6fdb6e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 8221
        },
        {
            "id": "1d98f9cc-99c7-4970-ac27-ec94515d579a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "661c8f9f-460c-4129-941e-550096988946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "a95b87ed-bc76-46b5-a36e-6257a03bd5a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8217
        },
        {
            "id": "3ef7de7a-173c-4014-85bc-d769a8e3068a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8221
        },
        {
            "id": "3c9b23e5-7d43-4a7d-b9ce-f831cf1649f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "be1b88f6-af83-44ec-a032-d658ae6d3d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "58369dc4-8bd4-44ee-a98c-2f0a0aa3ca79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "55e34501-eb3d-4683-ad5f-893f465685ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "f42f7640-1a27-4c14-a901-e290ecdadf04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "f0c2a7cc-e99a-44d8-9bf6-095f8e5a37a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "634adb4a-da77-46b5-8a82-32aa75c83253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 122
        },
        {
            "id": "fbbc99d3-8545-4698-91ac-c4be63358409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 253
        },
        {
            "id": "997049e5-e37d-4847-816d-cf975eb479a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 373
        },
        {
            "id": "255965e8-d3cf-46be-aced-03e47f98a428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 378
        },
        {
            "id": "905cb970-281d-4ea7-9faa-beacb0a03f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 380
        },
        {
            "id": "491febbb-a677-45de-ba9c-e6e5dbb26abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 382
        },
        {
            "id": "1ff69fed-865e-4ee7-8768-a679435ed401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7809
        },
        {
            "id": "03597317-b210-4035-82f8-6efa3cb05b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7811
        },
        {
            "id": "c977b7d1-06ee-40c4-ae13-31836c9f0a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7925
        },
        {
            "id": "a7467b6a-aa8f-4d13-93f6-856f66b4aeba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8217
        },
        {
            "id": "e96173e8-28ca-41f9-9a5c-28f5aaadcbc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8221
        },
        {
            "id": "580e6293-489b-47ac-a2d9-d37c4f1a8f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "59ba0588-8878-437c-83ad-eeb034fb9491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "3145b1f3-b51f-4705-a5b2-f2029a1a669b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "b368827b-32b6-4311-8318-763be10fdf42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "6aa727c7-ee3f-4ba3-b836-8517017ed8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "135d568e-3262-446f-8c37-85d53fef5d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "af57fdab-769f-442b-a3bc-906380def392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 122
        },
        {
            "id": "f756077c-cb58-4f99-991a-5d0cabf9e711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 253
        },
        {
            "id": "8026ec81-1f5e-464a-8313-4361007bfe37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 373
        },
        {
            "id": "e035093e-3b80-4a79-bda9-9d9b30c6dcb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 378
        },
        {
            "id": "53df1cc8-8600-4d00-b608-05643f3e8a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 380
        },
        {
            "id": "bd9b25b9-169d-40b1-9129-89d92e2d2749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 382
        },
        {
            "id": "3650bebf-2cdd-4d4d-93b1-c9b55af4eb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7809
        },
        {
            "id": "24965e23-059f-41a5-bd00-55af44ec857b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7811
        },
        {
            "id": "2e9edcb4-a1fa-494c-8a0a-6ee74a259abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7925
        },
        {
            "id": "8b3f527c-032a-4192-85d9-2d52f196d70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8217
        },
        {
            "id": "48d0a810-d2b2-4b09-a92c-0e06ac73c7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8221
        },
        {
            "id": "be417794-efb3-4644-b2ec-205aa92913d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 34
        },
        {
            "id": "b0ad30a1-368c-4f3d-99f3-8bbeadecac16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 39
        },
        {
            "id": "03f8d2f8-7276-4016-aad8-3fa4d38d2e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "4b3dd7b3-bd98-4460-9e31-583d7442d62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "91872c22-5536-412e-ad09-7f09c2304966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "a8e16618-1d81-4a5c-884d-2465c249a3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "2a30e8f6-3d6e-468c-9f90-2ec1f16a210e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "3f735faf-b589-40fb-8c8b-1255425671df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "7ee82bf5-9cb4-4d59-949c-d138b434a44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "8d7ea3d7-92ec-4ca1-b6f7-e1bd8a26d511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 224
        },
        {
            "id": "47737585-35c1-4ca3-99ca-69eee985d4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 225
        },
        {
            "id": "ddf12f5d-5a68-4603-bcb2-814174c29b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 226
        },
        {
            "id": "4b0b85da-cb43-4222-b1c1-583719c22d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 227
        },
        {
            "id": "cc00c800-f4be-49dd-aef2-48a7704211d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 228
        },
        {
            "id": "bb63aaaa-914b-499b-8da2-5f659af0e2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 229
        },
        {
            "id": "45489810-6098-49b9-b43a-b6aaadda269e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 230
        },
        {
            "id": "601fc805-8e17-4f8a-b508-72a7b9e41b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "a5602954-c8cd-4a6d-aebf-02157e163732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 232
        },
        {
            "id": "197d0ff4-022b-4df3-8106-7b8039997fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 233
        },
        {
            "id": "5a3d63ec-48f7-45b6-a6dd-591dc35dc982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 234
        },
        {
            "id": "4e707695-d1f7-483c-9122-9c9236b03eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 235
        },
        {
            "id": "b9bbd9f1-0a3f-4bd1-8e1c-60b1f15fce43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "13d22234-4bf6-480e-936b-0b32fa79b134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "394b561d-bbec-493e-bd0e-3ca8b139c01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "aeaec566-9926-48b7-a052-d59d39b62764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "aea1e31a-aee9-4e77-8de7-8b3522f0d98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "6835d7cf-fabc-4d12-bfb7-aed7d80fc178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "7b725361-6572-44a7-83b5-8df5a9ccd6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 257
        },
        {
            "id": "47964f49-510a-4113-89e6-c9d9308ee5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 259
        },
        {
            "id": "f783eed2-a8fe-4ef6-9824-54625219ab05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 261
        },
        {
            "id": "e2a03bae-f668-42c2-b9fc-7e205f0f5e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "dff037fc-a8e8-4c4e-9d07-d5be262cd4d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 265
        },
        {
            "id": "fd6aeabd-af43-4222-a7da-652b1fb182a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "22247c1a-e72b-4598-b542-74c0023b735f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "1eb816e0-1918-4a1b-a887-75016cf87803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 271
        },
        {
            "id": "9496fc80-dfe3-4d71-9285-d59d1c98dbc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 273
        },
        {
            "id": "e30f02ca-30df-4218-8c0c-02d3d2a29757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 275
        },
        {
            "id": "5ab24d56-b06b-4030-b29c-b06e0031f4db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 277
        },
        {
            "id": "904e477d-bd75-4a65-8bba-0e713967bdd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 279
        },
        {
            "id": "4ec14987-96cc-4f6b-a630-85a7d65f1113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 281
        },
        {
            "id": "b0dbafaa-f854-4dc7-b947-d253676e2acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 283
        },
        {
            "id": "ee7503e4-6bf5-4289-a045-78c8033a5319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 285
        },
        {
            "id": "106c2507-2ed1-4df4-8198-3edc032997c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 287
        },
        {
            "id": "a1b58a2a-92d4-43b4-a756-5d2a592b2ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 289
        },
        {
            "id": "5d9198b5-8cdd-4b20-b763-c70d9ce53ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 291
        },
        {
            "id": "a9b0760f-e164-4fa4-833a-d0a1680936ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "d358938e-fa95-493c-badd-51bcdf8269f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "8d65228f-9eac-4e24-8551-14697148f52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "1872fbc8-10fd-4d06-a4bd-c1454158a9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "b8009c27-d5bd-4482-901a-1f8f81879df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 417
        },
        {
            "id": "ebf460b1-cd6b-4dd2-9c49-db3b86ae06d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 507
        },
        {
            "id": "ece491b5-3410-48a4-88e8-ae5ab9e7fb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 509
        },
        {
            "id": "1e1df3c3-4cf5-40d5-a95c-0283b11a8aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 511
        },
        {
            "id": "7176d181-984a-41e1-81c8-414b6531b1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7681
        },
        {
            "id": "1350fdb2-8e4c-43f8-baaf-082fa5ad7ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7841
        },
        {
            "id": "2edae879-4257-4f3d-9437-e9b5909ab968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7843
        },
        {
            "id": "2a3aab6d-a62e-4e99-9c82-c4ba79435516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7845
        },
        {
            "id": "b399f89f-b3fb-438d-bdd1-47ee35002fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7849
        },
        {
            "id": "f72ca6a7-cfd9-4b00-bff5-8192831dc4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7851
        },
        {
            "id": "8893737b-77aa-4e1b-bba1-3f90dbdb4404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7853
        },
        {
            "id": "5c9eea3c-79bb-4441-b97b-cb2695a1017d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7855
        },
        {
            "id": "d59ccfcc-e0a5-460d-b625-696171ac017e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7857
        },
        {
            "id": "2e8354f7-6658-4634-92e9-b408e7b30f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7859
        },
        {
            "id": "4297987d-e044-4a50-acb8-86d5072e91cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7861
        },
        {
            "id": "98ef8108-c2eb-4724-9026-bbff30074078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7863
        },
        {
            "id": "a2f843f3-158e-49d9-8d43-bbc8ef68de82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7865
        },
        {
            "id": "c92ae9f3-0d7a-4786-b2f3-665bd4e6f235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7867
        },
        {
            "id": "e42a5525-aa33-4045-bbd5-4bb03322b64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7869
        },
        {
            "id": "56d58140-a899-48ee-bfe2-2fc5edca976f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7871
        },
        {
            "id": "02702832-caec-4a5b-b745-ac6b93958da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7875
        },
        {
            "id": "02651186-2d31-4845-bcdc-f7a9ad37f5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7877
        },
        {
            "id": "5b47b583-f257-4fb1-a2dd-b17707e732b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7879
        },
        {
            "id": "8d42165b-267c-43d0-a471-296f958bf6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7885
        },
        {
            "id": "df64aa72-b76b-494e-99b7-d666a32f2ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7887
        },
        {
            "id": "a39478e8-1079-41b7-96da-24c944728209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7889
        },
        {
            "id": "3ece0e07-849f-43d0-8a3b-79d5eef27cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7893
        },
        {
            "id": "9588d906-7e78-4e0f-b422-f58561c0160c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7895
        },
        {
            "id": "ff6112b0-85b9-49b0-b0d9-3eb65a03e3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7897
        },
        {
            "id": "36688c42-0427-415c-b9bf-9a714ee210f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7899
        },
        {
            "id": "8d0fabc0-2755-49e9-a08e-6c9558603121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7901
        },
        {
            "id": "ec1c1f73-aa2d-4380-a7e0-983ae9d88d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7903
        },
        {
            "id": "41e08c8e-7f88-4eac-8263-d81db00248de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7905
        },
        {
            "id": "200208d8-4335-4946-ad40-a1636ee173ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7907
        },
        {
            "id": "9b5a5bff-39f2-4ab5-9195-d3a7fc2cb410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8217
        },
        {
            "id": "9bf37cb8-def3-445d-8495-5c17d2f90908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8221
        },
        {
            "id": "0730456f-71af-4c01-a5b5-630dc0d660c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "b008fef4-2521-4427-b9d9-e637cc2cc23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "b7d2d5ce-eaed-4af0-988f-32ffacd22d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8217
        },
        {
            "id": "a6890ee3-68aa-4ea0-8f0c-29b20a205dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8221
        },
        {
            "id": "1efda4c5-e116-451f-b8ec-340c047247ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 118,
            "second": 34
        },
        {
            "id": "5645d94b-b9e5-44f8-b007-e28de2b7110c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 118,
            "second": 39
        },
        {
            "id": "46d6ef44-059d-4f31-ba5e-f795ac039f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "e8c99f4e-dd00-4c5c-bb41-489bdb499781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "9d690db8-0152-4749-b4e5-eb3c48a167d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 63
        },
        {
            "id": "cdcc80b2-3f29-4fd2-97b8-db7248a65f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 118,
            "second": 8217
        },
        {
            "id": "6318184b-442b-4f1d-a0c4-e7d6e363ecd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8218
        },
        {
            "id": "868215ff-88d4-436f-a2cd-5026b3492eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 118,
            "second": 8221
        },
        {
            "id": "cb555fd9-3abd-45f7-b963-8def1f9e8515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8222
        },
        {
            "id": "a83ab186-29a6-4863-9dd6-18fbd82515a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 119,
            "second": 34
        },
        {
            "id": "c7d6fbb3-19ee-4955-a16f-b788cd36794a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 119,
            "second": 39
        },
        {
            "id": "6e8dfce0-aeac-448d-ab7f-6f5de8256956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "d58ecaf4-fede-446b-b9f4-7383545d2cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "3e6e34e7-a63f-4c28-91a8-0e9ae4273f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 63
        },
        {
            "id": "44b270a3-5cf0-428f-a315-27ac5363adea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 119,
            "second": 8217
        },
        {
            "id": "69a9afa4-6567-4db0-a0dc-8410258454dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 8218
        },
        {
            "id": "e3300643-0791-47b3-979d-d64dad52db9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 119,
            "second": 8221
        },
        {
            "id": "97fb1f5e-5054-48ac-9673-dbcc3ec764c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 8222
        },
        {
            "id": "82a9e335-1ec0-4704-bf68-45cdb23b9c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "725cb7cc-a39c-417e-8fcc-386ae8758613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "71e1578c-e261-4474-8cd1-c02e712d9162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "0b92932b-f706-49d9-b4ce-99465ca8f465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "c75e7e21-6eab-4265-a8f7-fe456a026822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "90258a38-d902-4e0b-8ae1-8834f6b1016a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 224
        },
        {
            "id": "4e1dfc61-44ca-4331-8671-d7e7f5559fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 231
        },
        {
            "id": "02ec4294-8a8f-48d8-add8-705b26f1b3dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "5d64aed8-869a-4884-a38f-9ebce94be072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "68a40e8c-04af-40f7-9743-ca0e6ae473cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "e29546b5-7fc1-4c52-905d-1dad6a53be17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "ef4ddce0-deeb-4838-adde-0ec674573cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "6734c47d-f162-4a06-96ce-0942461c4dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "00bd1155-8c23-47e1-9941-ed20f4f6c6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "1acb9b8f-6632-43e6-9f50-694566be28a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "bcd2fc18-4aee-4242-9914-8ddfada4b02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "f4cfe631-e020-40c3-ab7c-3a7a13d1db66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 248
        },
        {
            "id": "c3bf1968-930a-4244-846d-2478f4e6d818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 263
        },
        {
            "id": "9c5482f3-4758-4756-a9cf-aa58f6b0dd55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 265
        },
        {
            "id": "00a250d7-a851-4cee-99fc-2fb2d4e0903e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 267
        },
        {
            "id": "1c37e911-6710-4d2d-8833-33e6472b9abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 269
        },
        {
            "id": "3eb7d825-8194-4213-b1fe-25e3300c8580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 271
        },
        {
            "id": "8a395bde-8ebd-4897-8bbb-e71b26ce788c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 273
        },
        {
            "id": "7a052b99-2893-440f-8425-2fe04fc449c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 275
        },
        {
            "id": "1154ecc1-7a78-429b-b8ec-781d445f4975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 277
        },
        {
            "id": "c93a73de-60e2-483d-bcbc-53a422208303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 279
        },
        {
            "id": "c5c9e99e-18ce-4e4e-8623-6adf012d9481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 281
        },
        {
            "id": "7828a768-bba2-415f-be1f-51a322b6d4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 283
        },
        {
            "id": "788123fb-9a9d-4c8e-a004-f7707dd426f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 333
        },
        {
            "id": "b5cc5491-d7e7-4522-823d-9e4ed9b88a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 335
        },
        {
            "id": "e6a250c6-e884-44c4-98a5-6e8f4eb2749c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 337
        },
        {
            "id": "8a59e94b-552d-4bc8-a100-0d36eda6d039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "503c7e9a-6b85-4712-9f20-abf2e56157c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 417
        },
        {
            "id": "6010197e-d2a9-433f-a4dd-29c4faa9e64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 511
        },
        {
            "id": "4d549e78-e18d-486e-846e-8a715c9f7730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7865
        },
        {
            "id": "588da8c8-ba29-40c9-8df2-c2164e257466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7867
        },
        {
            "id": "5387fc60-ee81-406f-877f-57f64d90e317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7869
        },
        {
            "id": "51082f35-6736-44dc-87bf-45083833883b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7871
        },
        {
            "id": "687d3cdc-13b1-4b38-badf-4749d7c11ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7875
        },
        {
            "id": "a4138e9f-78cc-4440-b6d9-0ad08c66fd6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7877
        },
        {
            "id": "97a6c332-cdf0-40d4-9cd6-ec34b590c72e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7879
        },
        {
            "id": "bcee7555-3305-45df-8ee7-c5058ec8b507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7885
        },
        {
            "id": "8d433586-2cb1-465c-aff6-4c33bc91b4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7887
        },
        {
            "id": "86384886-a866-47c5-b589-346b3cab2059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7889
        },
        {
            "id": "05df8243-4180-4bad-bdba-7505d01fdf5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7893
        },
        {
            "id": "331dccf1-49ad-428e-9f12-98b229bde498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7895
        },
        {
            "id": "bbb9dcfc-99e8-4cfe-b8c1-b2cba525d154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7897
        },
        {
            "id": "e4d7d1f4-fff1-4a2e-8db2-029faad117fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7899
        },
        {
            "id": "1807f40d-63ff-46d4-b8ab-f2c88179b60d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7901
        },
        {
            "id": "cfd66222-5912-478c-a4fe-ba92b08e7452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7903
        },
        {
            "id": "e545f115-0169-4929-a21b-709699b72b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7905
        },
        {
            "id": "c2b5dbd2-d8ab-4341-840a-2f02e3d87848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7907
        },
        {
            "id": "e2b8b181-ed55-49f1-9056-58283ec645aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 121,
            "second": 34
        },
        {
            "id": "69c3e963-5535-40f1-8ec5-45a49e4b555a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 121,
            "second": 39
        },
        {
            "id": "7e64e966-81db-4e31-a9f1-6c8f0dd1f687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "4ccfb52f-cf93-4b38-9ddb-4a305c044fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        },
        {
            "id": "7f2bf2da-27ae-4121-b3a4-231d1fb868bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 63
        },
        {
            "id": "61d7c3ff-deaa-475e-8c81-f4f97c9d278b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 121,
            "second": 8217
        },
        {
            "id": "eea53a91-ba69-4d27-a196-0052a3c50d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 8218
        },
        {
            "id": "3259f631-d990-4a46-99ed-1b4a0858e7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 121,
            "second": 8221
        },
        {
            "id": "6939f17b-c98a-4c28-8368-33989f1c59e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 8222
        },
        {
            "id": "ebb53736-4b10-4d0d-a64b-d2d35a7d9299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Bold",
    "textureGroupId": "65af0988-cb88-42df-b650-971d782eab72"
}