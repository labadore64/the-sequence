{
    "id": "d5f5c0f5-c443-4210-b14c-06d2cf45504f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_point_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 22,
    "bbox_right": 187,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b105fcc0-6525-4a96-9d6e-6d7fdd75773a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5f5c0f5-c443-4210-b14c-06d2cf45504f",
            "compositeImage": {
                "id": "a124d7c2-d1ea-4e59-a720-abdbf1273405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b105fcc0-6525-4a96-9d6e-6d7fdd75773a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f4ae84-82b8-4835-85e5-6203f8b79492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b105fcc0-6525-4a96-9d6e-6d7fdd75773a",
                    "LayerId": "8ec91770-1f0f-44f2-bed4-533ac22101f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8ec91770-1f0f-44f2-bed4-533ac22101f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5f5c0f5-c443-4210-b14c-06d2cf45504f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 200,
    "xorig": 156,
    "yorig": 55
}