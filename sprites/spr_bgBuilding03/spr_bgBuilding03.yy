{
    "id": "57ee00cf-6b9b-46c2-92be-0410c9efb40f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgBuilding03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 520,
    "bbox_left": 111,
    "bbox_right": 533,
    "bbox_top": 193,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68d7e44b-cf31-4cb5-b483-d62b3ac7e6a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57ee00cf-6b9b-46c2-92be-0410c9efb40f",
            "compositeImage": {
                "id": "5525f258-8db6-4818-9fa3-848ae8d12003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d7e44b-cf31-4cb5-b483-d62b3ac7e6a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f7c40e-2cd4-47d0-9158-943bb1eebc5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d7e44b-cf31-4cb5-b483-d62b3ac7e6a4",
                    "LayerId": "da719d79-8767-4318-9e8d-e74828ca8e44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "da719d79-8767-4318-9e8d-e74828ca8e44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57ee00cf-6b9b-46c2-92be-0410c9efb40f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "57570f21-0695-4f13-a303-519260b98bb3",
    "type": 0,
    "width": 825,
    "xorig": 0,
    "yorig": 0
}