{
    "id": "f160edfd-0d5f-4f70-b84a-df22b2ed41f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 75,
    "bbox_right": 198,
    "bbox_top": 80,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c89a907d-cbb9-4eb8-9abe-4aef047a39e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f160edfd-0d5f-4f70-b84a-df22b2ed41f1",
            "compositeImage": {
                "id": "bce5b18c-6b5f-4d99-86a5-32982be3271b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c89a907d-cbb9-4eb8-9abe-4aef047a39e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6406be1-5721-412a-ba87-1837f3da197e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c89a907d-cbb9-4eb8-9abe-4aef047a39e9",
                    "LayerId": "2562cdee-b2cf-4aef-9a57-0994ec1861b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2562cdee-b2cf-4aef-9a57-0994ec1861b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f160edfd-0d5f-4f70-b84a-df22b2ed41f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}