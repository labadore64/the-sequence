{
    "id": "6cac6904-9647-40c6-a69a-123b553ca15e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_vecdor_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 193,
    "bbox_left": 3,
    "bbox_right": 254,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fa2d8e8-138b-4b13-82ee-70c89fa82a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "6e5f4f2d-a57b-42b7-aa90-6b4903478f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa2d8e8-138b-4b13-82ee-70c89fa82a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1ef2b5-798c-4995-b235-f1560ba2b8f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa2d8e8-138b-4b13-82ee-70c89fa82a47",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "d5e1490d-7d4e-4eb5-964d-b4d732fa6a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "221b4718-ae88-44df-8c35-57447f14adb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e1490d-7d4e-4eb5-964d-b4d732fa6a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a1750b-cd47-4d38-9947-ebeb2ddbb359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e1490d-7d4e-4eb5-964d-b4d732fa6a3a",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "f5891ac0-7483-4236-ada8-17e47f65feef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "019dd677-a73d-4ed7-9e41-1c1ca559aa53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5891ac0-7483-4236-ada8-17e47f65feef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0264b85-9928-4152-84b5-234da92aa258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5891ac0-7483-4236-ada8-17e47f65feef",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "4f2ac070-a7ed-43df-b267-8d072639104e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "11dddb4f-064e-4a27-8f73-1fc841c27fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2ac070-a7ed-43df-b267-8d072639104e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "680c4309-adfc-4357-8140-08e59797fd6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2ac070-a7ed-43df-b267-8d072639104e",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "1e8dc4ee-8867-489b-aa2a-af7c57e42049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "8e935b87-e043-4819-97a1-c78f618c4b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8dc4ee-8867-489b-aa2a-af7c57e42049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32b5519b-e16a-4c1e-8d4e-79c4037a7764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8dc4ee-8867-489b-aa2a-af7c57e42049",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "b1208f1e-91a3-49e2-b7a7-32b6c9d394b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "fc21dc8d-3ffc-4ebf-916c-2c2e68c2236a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1208f1e-91a3-49e2-b7a7-32b6c9d394b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e20c07-b45d-4262-ac74-b69b97e6929a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1208f1e-91a3-49e2-b7a7-32b6c9d394b9",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "0783a79b-744a-4264-8762-98afa3655679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "8624a9a0-6c5e-4f64-80f1-0aa6184a9a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0783a79b-744a-4264-8762-98afa3655679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5a347a-c1e6-40bb-b070-4213d703650d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0783a79b-744a-4264-8762-98afa3655679",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "f8cb49c2-20a0-4573-828d-069bdd545b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "1971a4bb-4bb2-4725-8fc6-d3ed8ffe507a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8cb49c2-20a0-4573-828d-069bdd545b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9752a16b-155b-4569-a23d-c6cb73388705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8cb49c2-20a0-4573-828d-069bdd545b60",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "63832ef0-526c-4517-96c3-7bd715b8570f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "45738022-41fc-4397-b5cb-f7df7fbf06e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63832ef0-526c-4517-96c3-7bd715b8570f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5a0dd6-e101-4dcc-9489-7bcf629c9b46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63832ef0-526c-4517-96c3-7bd715b8570f",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "cea258ec-bfba-4a1f-ab9c-26f09d71d933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "fdd1c9b7-726f-4480-949f-367ad0d1cf12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea258ec-bfba-4a1f-ab9c-26f09d71d933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc701f6-50c6-4ab9-aaca-104d3e81cb7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea258ec-bfba-4a1f-ab9c-26f09d71d933",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "9dfeffef-4235-4c0a-ba91-b60434afa714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "d983c2b1-2b8f-43dd-ab8d-902401444f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dfeffef-4235-4c0a-ba91-b60434afa714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6356b75f-1209-46a9-be35-e80d0525cf18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dfeffef-4235-4c0a-ba91-b60434afa714",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "e45cd997-77a3-4145-a7fa-25848174d5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "8d8a6a7c-6a77-45af-9e44-7fda6fc708ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e45cd997-77a3-4145-a7fa-25848174d5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7643b8f0-f680-4637-bd35-7f6d9c425bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45cd997-77a3-4145-a7fa-25848174d5c2",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "4c679ea8-8fb7-458e-8ded-8e131c22a568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "7306275a-71dd-407f-8a30-17dad26f197a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c679ea8-8fb7-458e-8ded-8e131c22a568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a016a8-1279-4f5f-8a15-bf11d9f8e977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c679ea8-8fb7-458e-8ded-8e131c22a568",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "9f760406-133c-4726-a426-150befa3ed5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "b3511250-2777-4652-b7bd-f358377e1f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f760406-133c-4726-a426-150befa3ed5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc49be6b-2d45-43fd-a0f8-9fcb7c2e76d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f760406-133c-4726-a426-150befa3ed5a",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "65b39245-476f-4717-ad71-ac91534f5ce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "34afa0af-a2d0-493b-b1ed-585afe0963e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b39245-476f-4717-ad71-ac91534f5ce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b83a564-e63d-42e3-9848-ad827fc67624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b39245-476f-4717-ad71-ac91534f5ce1",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "12f65d1d-f982-4fe6-af29-c0e651a65ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "4b203686-cb67-4de5-a5de-73b9410d4a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f65d1d-f982-4fe6-af29-c0e651a65ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a39c662-4192-4548-b306-ae5650f5a1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f65d1d-f982-4fe6-af29-c0e651a65ef4",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "41bb4275-4e6f-44ef-a007-724b7a244141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "6a54eec7-be36-43d7-a051-ef8078ae6dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41bb4275-4e6f-44ef-a007-724b7a244141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f176b51c-bc31-41df-abc0-990e76a74dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41bb4275-4e6f-44ef-a007-724b7a244141",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "4ba6aba9-0fb0-44b7-be3b-1e542c65d1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "a48a4c16-820a-4b57-8b13-cac3b8e4b397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba6aba9-0fb0-44b7-be3b-1e542c65d1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c639eb3-2285-4ac4-8ef6-1853a8b0da5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba6aba9-0fb0-44b7-be3b-1e542c65d1c5",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "bc06d053-315f-4326-b14b-73ef9a36db3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "efbef878-5165-465a-933e-f7991247fd3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc06d053-315f-4326-b14b-73ef9a36db3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ebcb48-1f43-49d6-9343-578fba9c6faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc06d053-315f-4326-b14b-73ef9a36db3e",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "c93d7101-2c28-4091-8981-b8d3e0335cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "80b3931e-ff24-496b-92d5-27b5670312f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93d7101-2c28-4091-8981-b8d3e0335cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6549301b-5aba-4613-afa3-8ae14f3b8ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93d7101-2c28-4091-8981-b8d3e0335cce",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "dbd42a46-ff3a-4a85-8ceb-564e1894dea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "d83ed2cc-7b92-43a5-921c-09d017646b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd42a46-ff3a-4a85-8ceb-564e1894dea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3fb5eb-0cdb-4a70-8cb2-d0b31a689791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd42a46-ff3a-4a85-8ceb-564e1894dea0",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "3894e431-4229-4fcf-b030-8a9d0836769b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "bf08fbc0-2425-46a7-b7eb-5065185dd3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3894e431-4229-4fcf-b030-8a9d0836769b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef9162b-726c-43fe-b090-3a51abaf20a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3894e431-4229-4fcf-b030-8a9d0836769b",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "9487aebc-c8c9-4f22-8ea4-2b4e196c2507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "53e7d056-820f-4adb-89a1-6b57fd372bf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9487aebc-c8c9-4f22-8ea4-2b4e196c2507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bfb73de-7bc6-4983-8b3d-8afee8a4d39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9487aebc-c8c9-4f22-8ea4-2b4e196c2507",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "4b285412-3073-4fcd-95d7-234875d8cc84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "abd6fcc1-c79b-4935-a7b1-97276ea56ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b285412-3073-4fcd-95d7-234875d8cc84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c323c7-819b-45da-9fb4-974a190a3264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b285412-3073-4fcd-95d7-234875d8cc84",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "14f41c8b-7c3a-452a-820c-16dd778bfe8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "114788c6-58b2-4c13-aace-cda4664b892a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f41c8b-7c3a-452a-820c-16dd778bfe8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e125b4c-81d7-4e1b-86a4-47aeda2fe2ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f41c8b-7c3a-452a-820c-16dd778bfe8a",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "9eff51f7-1610-45bb-a4e2-7f78b684ecfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "45c61df8-be11-4762-9091-d0a34b0b051d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eff51f7-1610-45bb-a4e2-7f78b684ecfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9c561a-98d7-4b56-9515-e5001052a8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eff51f7-1610-45bb-a4e2-7f78b684ecfd",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "77d487a3-ced3-4d4f-9243-426e0c1d29c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "34d7f262-c3b4-4765-9d3a-f0c1c0dbffeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d487a3-ced3-4d4f-9243-426e0c1d29c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd523c30-ebb1-4107-b0a2-d7896b6c50f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d487a3-ced3-4d4f-9243-426e0c1d29c1",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "9aa0d79e-1884-45cb-b119-4904b0b23808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "504b10fd-d560-4378-b1ef-24f33c0170ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa0d79e-1884-45cb-b119-4904b0b23808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a8460bc-19be-44a9-b4a8-7b2a2eccdeee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa0d79e-1884-45cb-b119-4904b0b23808",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "02390786-d63f-4b9b-8ba0-2ab56c9149aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "cd3c8f03-b870-496c-b682-5d060744c0f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02390786-d63f-4b9b-8ba0-2ab56c9149aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ae6280-4498-4760-bd24-df8815a6a2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02390786-d63f-4b9b-8ba0-2ab56c9149aa",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "0ba8ba89-f833-4e9c-8255-c4142f11b27d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "d889617d-57c2-4831-aed7-5b698062748e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba8ba89-f833-4e9c-8255-c4142f11b27d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6270f9b-6d82-4abf-9bae-4f4103e7e04d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba8ba89-f833-4e9c-8255-c4142f11b27d",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "d0476cc8-0dff-4ce1-8088-c318e1e20efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "3d2242e4-d558-4e78-b84c-556715f2ecf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0476cc8-0dff-4ce1-8088-c318e1e20efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e30c49b8-7769-4e5a-8f43-71c06379403f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0476cc8-0dff-4ce1-8088-c318e1e20efd",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "f87f77d0-1f45-4893-aabc-3d4f62f16a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "1327ef55-f334-47df-a51e-5e765f6e33c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87f77d0-1f45-4893-aabc-3d4f62f16a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0df5a98c-21c1-4d59-9295-7739b494a10b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87f77d0-1f45-4893-aabc-3d4f62f16a9d",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "a5f51dbc-cf6d-4854-be3a-6861bcbd704c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "456845ef-819e-4c3b-a47a-495fbec7b4de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f51dbc-cf6d-4854-be3a-6861bcbd704c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fbb9f08-677f-4064-8882-7afdf986684b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f51dbc-cf6d-4854-be3a-6861bcbd704c",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "b779c06f-0f7d-4587-ab92-be1ba85d3b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "5aa8ac3b-6c87-4b97-97d0-0323d83967d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b779c06f-0f7d-4587-ab92-be1ba85d3b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d992c6e9-1e7d-44d1-a481-9d41df1e817c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b779c06f-0f7d-4587-ab92-be1ba85d3b20",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "c3dcc3cf-eb87-4a18-ba9b-839e3d728863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "5357d0d9-2d6f-4a54-a31b-89ede65c8158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3dcc3cf-eb87-4a18-ba9b-839e3d728863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8608b33a-e4bd-462d-b1de-4b0af0f6696a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3dcc3cf-eb87-4a18-ba9b-839e3d728863",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "52f9cdd9-e81a-4a12-aacd-e00c12239066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "cd7652aa-d4ae-4e9e-9a4e-99a07b1d1a7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52f9cdd9-e81a-4a12-aacd-e00c12239066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37fddc58-676b-4d36-88ee-1d56c9392877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52f9cdd9-e81a-4a12-aacd-e00c12239066",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "8c4c23d0-ba6d-45d0-a614-e2b687c36965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "ede1af09-ad88-411e-a89f-72af8c196f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4c23d0-ba6d-45d0-a614-e2b687c36965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c7032e-0308-492e-9e95-bc8dfee17003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4c23d0-ba6d-45d0-a614-e2b687c36965",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "70a7200f-6b1d-4f9b-bff1-7040df11f8ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "3b858129-0f8e-48e2-a0e1-c88a9e201150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70a7200f-6b1d-4f9b-bff1-7040df11f8ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23c6f9a-b831-4fe8-91ea-af9febe65134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70a7200f-6b1d-4f9b-bff1-7040df11f8ba",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "24f5d296-9d05-44c8-85f5-338949863df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "3fb41609-f666-4a52-b826-ebe40d7f06bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f5d296-9d05-44c8-85f5-338949863df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7bd5319-3fcb-473d-a171-af76287c197a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f5d296-9d05-44c8-85f5-338949863df9",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        },
        {
            "id": "0bc9a5e8-d8bf-4986-b2b0-4ab1141b713d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "compositeImage": {
                "id": "cfce0ecf-fdf9-4281-a330-462bf39db852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc9a5e8-d8bf-4986-b2b0-4ab1141b713d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c49c2d97-feea-4f4b-8f88-cc5c92a7a5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc9a5e8-d8bf-4986-b2b0-4ab1141b713d",
                    "LayerId": "a636edff-3101-4d14-91b6-2ac070be1a6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a636edff-3101-4d14-91b6-2ac070be1a6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cac6904-9647-40c6-a69a-123b553ca15e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "2e11f373-00b4-4204-976b-3ba150910fec",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}