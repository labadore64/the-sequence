{
    "id": "0544adb9-df64-4011-ae27-b5d91ee8f1f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "braillecell_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f581dd5c-2120-434c-80b3-e7c7aa5948d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0544adb9-df64-4011-ae27-b5d91ee8f1f1",
            "compositeImage": {
                "id": "b9ce5adf-4f21-45c4-a2b3-bbdb96904a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f581dd5c-2120-434c-80b3-e7c7aa5948d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe9a2b2-199f-48bb-ad43-30cec45a2a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f581dd5c-2120-434c-80b3-e7c7aa5948d0",
                    "LayerId": "67f1fe66-4c78-49b9-86e4-c135ba4862ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "67f1fe66-4c78-49b9-86e4-c135ba4862ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0544adb9-df64-4011-ae27-b5d91ee8f1f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}