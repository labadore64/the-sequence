{
    "id": "4c0d5f65-f84c-44ad-9f80-cdd3b12b6970",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_yourHouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 9,
    "bbox_right": 120,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10366534-005b-430e-82c1-1fe21e8580b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c0d5f65-f84c-44ad-9f80-cdd3b12b6970",
            "compositeImage": {
                "id": "ed018719-76b9-44e4-b017-b4dc2f685f93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10366534-005b-430e-82c1-1fe21e8580b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d722a0f5-94d7-47d4-a152-e7a66eeb53c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10366534-005b-430e-82c1-1fe21e8580b6",
                    "LayerId": "3e5dce2f-04fa-42f6-b67b-9076a626f3fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3e5dce2f-04fa-42f6-b67b-9076a626f3fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c0d5f65-f84c-44ad-9f80-cdd3b12b6970",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}