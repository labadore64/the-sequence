{
    "id": "37d0d680-374a-47ac-9496-2a1d96118b40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spepower_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52a568c7-81c6-4056-bc14-0775557a9cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37d0d680-374a-47ac-9496-2a1d96118b40",
            "compositeImage": {
                "id": "4f84fa4c-47c4-4812-8e93-2a34a7f52796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a568c7-81c6-4056-bc14-0775557a9cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd16ec1-e566-49eb-b090-d489d5331487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a568c7-81c6-4056-bc14-0775557a9cd6",
                    "LayerId": "3a1b160b-c546-4f87-9689-4f3cb9e08a5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3a1b160b-c546-4f87-9689-4f3cb9e08a5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37d0d680-374a-47ac-9496-2a1d96118b40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}