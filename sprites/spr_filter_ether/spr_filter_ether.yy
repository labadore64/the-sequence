{
    "id": "b24d82e9-c4e3-4b0b-aed5-f6cd5904966b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_ether",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 42,
    "bbox_right": 222,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "172451d5-814d-4cbd-9dbf-9ccd19b69971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b24d82e9-c4e3-4b0b-aed5-f6cd5904966b",
            "compositeImage": {
                "id": "4c8cfa1f-347e-46d1-be59-d3cb7306d621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "172451d5-814d-4cbd-9dbf-9ccd19b69971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47126dc-2693-4e3d-8ba8-923b3be10ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "172451d5-814d-4cbd-9dbf-9ccd19b69971",
                    "LayerId": "a4ba9216-33d3-4af3-87b5-71a5e0f3fd1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a4ba9216-33d3-4af3-87b5-71a5e0f3fd1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b24d82e9-c4e3-4b0b-aed5-f6cd5904966b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}