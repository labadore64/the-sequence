{
    "id": "31be2d49-9650-4025-8f1c-a1f6f132307b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 234,
    "bbox_left": 5,
    "bbox_right": 243,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "982a86cc-3b1a-4556-936a-a418a018831b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31be2d49-9650-4025-8f1c-a1f6f132307b",
            "compositeImage": {
                "id": "41fef72e-7afd-46d9-81a1-4c47d55fecd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982a86cc-3b1a-4556-936a-a418a018831b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c3bd10-6f49-449f-867a-72e066b80c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982a86cc-3b1a-4556-936a-a418a018831b",
                    "LayerId": "8b4fe2f5-1e0c-4173-ab19-8e7aa12a5df2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8b4fe2f5-1e0c-4173-ab19-8e7aa12a5df2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31be2d49-9650-4025-8f1c-a1f6f132307b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}