{
    "id": "2d150bda-74d0-4f15-becc-94c7af1ef868",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_stick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 18,
    "bbox_right": 243,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "303cbd5f-a66d-4990-b926-885fe15830b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d150bda-74d0-4f15-becc-94c7af1ef868",
            "compositeImage": {
                "id": "85d99ef7-8aac-4b25-878e-bc1089e0c444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "303cbd5f-a66d-4990-b926-885fe15830b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92cf1883-aab2-4d25-bb46-46f18168fa1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303cbd5f-a66d-4990-b926-885fe15830b7",
                    "LayerId": "bc20a05a-fcee-42bf-87d2-8d0f0946edb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "bc20a05a-fcee-42bf-87d2-8d0f0946edb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d150bda-74d0-4f15-becc-94c7af1ef868",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}