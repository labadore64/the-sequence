{
    "id": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aspyre_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 6,
    "bbox_right": 249,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af6ebd7d-35b1-4abe-b3fb-6c58e526f2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "6dc5dbcb-41d6-4364-9760-97f2af37dc0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6ebd7d-35b1-4abe-b3fb-6c58e526f2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f910a06-f715-4c51-bf9f-6acc2e5c2f7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6ebd7d-35b1-4abe-b3fb-6c58e526f2c1",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "90991ff3-2b81-4ba6-beb3-61e99e0a40af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "54f07474-3539-40ef-b00f-8f14aa0ca23b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90991ff3-2b81-4ba6-beb3-61e99e0a40af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e03de36-f475-43b0-9a81-599ac897ca52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90991ff3-2b81-4ba6-beb3-61e99e0a40af",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "0486f0a7-1aaf-4093-96a2-7dba6ef3c8b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "c4f96e1b-b383-459c-a7dd-7605529124f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0486f0a7-1aaf-4093-96a2-7dba6ef3c8b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e59315-7189-46f2-b801-402c7258b605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0486f0a7-1aaf-4093-96a2-7dba6ef3c8b6",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "25a5340e-391a-4f91-a2c5-edaae1994f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "3cb48d30-7ca1-4ff9-b788-414b2ff6a199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a5340e-391a-4f91-a2c5-edaae1994f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e781c9f6-d255-416e-b4c7-e23983561043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a5340e-391a-4f91-a2c5-edaae1994f05",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "ce82db63-a19f-4fad-9246-63146e2d138c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "631b4f91-63eb-421c-8c2f-f31766c6c550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce82db63-a19f-4fad-9246-63146e2d138c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b0323c-73b4-4d84-bf59-a03cafb2e114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce82db63-a19f-4fad-9246-63146e2d138c",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "d77de4f0-e7c6-44af-be63-682ee83f4612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "9a5e4002-54ca-484f-9f88-53722575b821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d77de4f0-e7c6-44af-be63-682ee83f4612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75878a2e-972e-4466-bc3e-bb6063a53e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d77de4f0-e7c6-44af-be63-682ee83f4612",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "2fc13432-2467-411d-96dc-5a41988db1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "01319f82-d3bd-4792-9755-99d2580bb789",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fc13432-2467-411d-96dc-5a41988db1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b8a117-0ebf-4b8d-9562-01478503185a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fc13432-2467-411d-96dc-5a41988db1b6",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "333b1140-d403-4a32-aa37-abd1e450f44f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "fcd9f842-23ae-44ad-a447-a5fc5708cf61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "333b1140-d403-4a32-aa37-abd1e450f44f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b55a3375-8a8c-4c07-bd1c-e2c32176e0b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "333b1140-d403-4a32-aa37-abd1e450f44f",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "b8b076c8-f149-44f7-8cd5-29d1fdff006f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "7030042e-acc4-4904-baf7-1411b2f4af4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b076c8-f149-44f7-8cd5-29d1fdff006f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbbdc1cd-f56f-4150-93eb-59055a4a0059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b076c8-f149-44f7-8cd5-29d1fdff006f",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "aa015812-722a-4be5-85b8-0441a8ae3826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "d84204af-6372-4f32-8834-de9f8067b503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa015812-722a-4be5-85b8-0441a8ae3826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6e6649e-e98d-4185-ad64-248d3766890b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa015812-722a-4be5-85b8-0441a8ae3826",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "cf614d4e-edcd-450e-9ac2-d47b461169e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "08cbdc9a-f4e3-4c9d-a50d-7371beb7c8d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf614d4e-edcd-450e-9ac2-d47b461169e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddbaa2fe-7b02-4735-8a13-963b4fd771a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf614d4e-edcd-450e-9ac2-d47b461169e1",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "9357d7a9-a580-443a-a9f3-fec062bced9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "72d013c9-637c-45ea-9e84-3cbee1395a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9357d7a9-a580-443a-a9f3-fec062bced9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff6c693-2968-4b1a-a78e-b5113e58c6a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9357d7a9-a580-443a-a9f3-fec062bced9b",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "7dfd96e2-c062-43b3-9ae5-b8d440076721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "46d5848c-c234-4d8c-93d9-e191cf8a8e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dfd96e2-c062-43b3-9ae5-b8d440076721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff6e6ad-6f10-4575-b975-0dc47efe445d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dfd96e2-c062-43b3-9ae5-b8d440076721",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "e5f46780-3a71-4561-b5b5-c1bb8808fe25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "8616f341-9287-41ee-bba5-65d8340bfa76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5f46780-3a71-4561-b5b5-c1bb8808fe25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e167f60-3827-4790-b413-599e643a9374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5f46780-3a71-4561-b5b5-c1bb8808fe25",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "f02011ce-f01b-43d5-bc9b-c91ca3f9e4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "eca96a5d-cf64-4d49-a748-72965a69157a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02011ce-f01b-43d5-bc9b-c91ca3f9e4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93afaea7-4acf-40e7-8e87-f9a0908fe143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02011ce-f01b-43d5-bc9b-c91ca3f9e4f0",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "22cc2c96-b204-420b-b34b-8a2b89cea99e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "73364b70-1843-4873-b7be-d345514d2d81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22cc2c96-b204-420b-b34b-8a2b89cea99e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa9e402-3f5d-4ce2-8fcb-3d17e8d3de74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22cc2c96-b204-420b-b34b-8a2b89cea99e",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "0cbeefc4-06e4-43df-a260-7035eb798f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "20e9e754-b2ec-490f-a2c7-392f91b6e670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbeefc4-06e4-43df-a260-7035eb798f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae216589-92e5-4430-b33b-589a7635b227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbeefc4-06e4-43df-a260-7035eb798f7e",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "7e1e5a3b-e6d2-42e2-a69f-3b73bcf026c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "0aa48f8f-2828-46dd-9bff-e1f3d00d91e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1e5a3b-e6d2-42e2-a69f-3b73bcf026c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4051ef94-7f3a-4258-bf87-b86ead640964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1e5a3b-e6d2-42e2-a69f-3b73bcf026c1",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "41bdf1c6-304f-4d11-b4d9-f4480b5a8ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "7d2ec44f-37e6-4332-9df1-5dbc4d723e61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41bdf1c6-304f-4d11-b4d9-f4480b5a8ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2900a9f4-d5eb-4864-a7f7-6b5b60adcb67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41bdf1c6-304f-4d11-b4d9-f4480b5a8ca4",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "c2e5f520-7f81-457c-b1fa-dc4951fdb315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "2e2dc59b-c98d-4794-921f-20b0e5a33b79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e5f520-7f81-457c-b1fa-dc4951fdb315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c1f9e5-0387-4f43-b8f0-07257549c0e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e5f520-7f81-457c-b1fa-dc4951fdb315",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "82a0fb57-8aeb-4199-9369-0c0b74f8951f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "174115e3-6a03-4e5d-ba0a-c3dae1df9bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a0fb57-8aeb-4199-9369-0c0b74f8951f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f0f339-7c2b-4232-a4b4-ca3d24faccbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a0fb57-8aeb-4199-9369-0c0b74f8951f",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "c4e2e1e6-ad68-4bd7-89d7-f6dc9d6964b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "ba148500-75ba-4d44-b369-61493f825164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e2e1e6-ad68-4bd7-89d7-f6dc9d6964b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831b806a-df30-472a-9187-07f8a9e1d793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e2e1e6-ad68-4bd7-89d7-f6dc9d6964b3",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "89926160-6639-4d2c-b55c-12ae1be7a43e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "6f4c8918-f645-42be-9c03-af8c651a94b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89926160-6639-4d2c-b55c-12ae1be7a43e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5373c0-d1b4-4639-83ad-663089c26daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89926160-6639-4d2c-b55c-12ae1be7a43e",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "1ab50276-c217-4329-a80a-8bed182696a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "a46fa39b-4351-43ac-9b49-c0c431dd11ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ab50276-c217-4329-a80a-8bed182696a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750eadb3-a7aa-48be-80d6-499da2b46175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ab50276-c217-4329-a80a-8bed182696a2",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "b64c7673-8498-47db-a9b6-95728b7d8a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "aae4056b-a482-4d0c-ae02-31adea6e0def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b64c7673-8498-47db-a9b6-95728b7d8a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfa95f6-5b33-4d33-9151-ec1659dbcb37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b64c7673-8498-47db-a9b6-95728b7d8a06",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "e863dc74-d33f-4211-a3c5-ac2d9a82454e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "4be36177-6865-4c65-9ee5-6cf6aadafb28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e863dc74-d33f-4211-a3c5-ac2d9a82454e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772f8eb1-c08c-4a49-9b22-d72e22d0f983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e863dc74-d33f-4211-a3c5-ac2d9a82454e",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "0ac1b664-0954-4abb-ac21-6dc79803ff9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "c752b298-883d-4ad8-b41a-ffb43d150a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ac1b664-0954-4abb-ac21-6dc79803ff9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218d3615-6a3e-41ee-adea-75fe64f521aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ac1b664-0954-4abb-ac21-6dc79803ff9d",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "276a243d-9f44-4889-a60d-01b58b22e42f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "0d873556-2c2c-429f-874f-26b696fcb019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "276a243d-9f44-4889-a60d-01b58b22e42f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2727e2ab-15c9-476a-8260-8b370822b3e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "276a243d-9f44-4889-a60d-01b58b22e42f",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "1ed36ca1-2a0a-4caa-bc7d-b1a09b86c563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "5c743306-a53b-432e-9d33-fa3e37dd4cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed36ca1-2a0a-4caa-bc7d-b1a09b86c563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d55128-4ccc-4af4-86f8-7deba41529b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed36ca1-2a0a-4caa-bc7d-b1a09b86c563",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "8f0463b8-9156-45a0-b5bf-e1fc26c0f541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "ae06b25e-f85e-42ca-877d-d1a3a748468a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0463b8-9156-45a0-b5bf-e1fc26c0f541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3af97b4-e92d-417d-84f7-63d7b0553b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0463b8-9156-45a0-b5bf-e1fc26c0f541",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "b5e46e7d-33f8-443b-8e1b-b81519682ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "6dea00f9-84db-4617-a13e-f903b51a2abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e46e7d-33f8-443b-8e1b-b81519682ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d33eadd-da3c-4325-b6aa-64afe3a83fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e46e7d-33f8-443b-8e1b-b81519682ce5",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "4d5e6172-59dc-40c4-a59e-f44b7f5c9409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "0d32783c-ee35-47ec-8584-4be1ace5242b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d5e6172-59dc-40c4-a59e-f44b7f5c9409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b214934e-effb-466e-af87-f40f50950bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d5e6172-59dc-40c4-a59e-f44b7f5c9409",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "0dcdf565-587b-4fe7-a15f-bff4c569f249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "490457c8-6f24-4434-b455-018de2a9d0b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dcdf565-587b-4fe7-a15f-bff4c569f249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ba60f9-22ad-47eb-825a-6c39060ded02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dcdf565-587b-4fe7-a15f-bff4c569f249",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "031ce745-3afe-4c43-8b7d-61664a0b6725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "58c112c8-54e4-423f-8801-dac21af58f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "031ce745-3afe-4c43-8b7d-61664a0b6725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2c0dee-8cf0-4987-9d68-4df65facecab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "031ce745-3afe-4c43-8b7d-61664a0b6725",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "6cdcfc91-dbf8-438e-9cc2-96d937a4f124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "31b93c95-7e07-41d9-8557-3b8d4c70987a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cdcfc91-dbf8-438e-9cc2-96d937a4f124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e18717-c461-43c6-a60d-aa17cd28b039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cdcfc91-dbf8-438e-9cc2-96d937a4f124",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        },
        {
            "id": "eed12ac4-d07c-4883-9975-0005a6f4e34b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "compositeImage": {
                "id": "4714f323-37a2-4130-9de9-36d143cef403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed12ac4-d07c-4883-9975-0005a6f4e34b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "894ae228-a976-4161-aec4-b1c1eb778072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed12ac4-d07c-4883-9975-0005a6f4e34b",
                    "LayerId": "7cf56966-ad37-4175-8b60-808d755f0573"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7cf56966-ad37-4175-8b60-808d755f0573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c08b990-c21d-4c8a-9eb1-41e98ab21dcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "808f5ba0-e9e9-4dd6-a3e1-5fd671722977",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}