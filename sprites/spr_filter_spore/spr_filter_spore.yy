{
    "id": "fd2a397b-517a-4025-b493-aec6ba08a0bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_spore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 6,
    "bbox_right": 255,
    "bbox_top": 129,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30c85fdc-d824-4f4f-81a8-5b9ffc114511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd2a397b-517a-4025-b493-aec6ba08a0bb",
            "compositeImage": {
                "id": "f600c8b7-860e-4ab0-905d-9d8c1223b159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30c85fdc-d824-4f4f-81a8-5b9ffc114511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1b7dae-685a-4131-9ff8-bb44d211b0d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30c85fdc-d824-4f4f-81a8-5b9ffc114511",
                    "LayerId": "adf07284-95cc-4237-b36d-e31be0090d04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "adf07284-95cc-4237-b36d-e31be0090d04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd2a397b-517a-4025-b493-aec6ba08a0bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}