{
    "id": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 33,
    "bbox_right": 221,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c0ae894-8b72-4aa4-9278-cfc0a9ce2280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "5513f39a-8975-4cd2-a0cf-5c83a0b143dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0ae894-8b72-4aa4-9278-cfc0a9ce2280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6fa4c7-a8e5-4c95-85df-0295eee5eea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0ae894-8b72-4aa4-9278-cfc0a9ce2280",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "084cc786-7b34-4326-9836-af0738ebf391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "bf196a24-5352-412e-92b7-802393896617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084cc786-7b34-4326-9836-af0738ebf391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c575376e-0b31-4634-92b0-6f9288b7e8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084cc786-7b34-4326-9836-af0738ebf391",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "82ec6897-4bb0-4c1a-bcb5-f7339f1f081d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "22c8d1a8-d8d3-4afd-b0df-baa6d98f5303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ec6897-4bb0-4c1a-bcb5-f7339f1f081d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7365e73-504c-4d00-899b-4c3e483c5d78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ec6897-4bb0-4c1a-bcb5-f7339f1f081d",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "efab759a-5f07-495a-a7f9-73e8e98968ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "d2b52d7d-37e7-40b9-8773-ff4c53d8ec40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efab759a-5f07-495a-a7f9-73e8e98968ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06a351c-cf8d-44c0-a256-a8ed124240a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efab759a-5f07-495a-a7f9-73e8e98968ce",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "34ef97c4-1568-49d2-88ac-bdf16d71debc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "8b1c7b6b-e2bc-488b-bfaa-ebb377584f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ef97c4-1568-49d2-88ac-bdf16d71debc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9529aab0-d506-4a23-ba08-387ba3e0a2f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ef97c4-1568-49d2-88ac-bdf16d71debc",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "8c480f26-58ea-40c7-86ef-4ebbabd9fb62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "a71eac90-086d-4654-a243-2a40e1f4b8a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c480f26-58ea-40c7-86ef-4ebbabd9fb62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e994eb57-01cd-477c-9188-770a889ab058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c480f26-58ea-40c7-86ef-4ebbabd9fb62",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "970483c6-c116-46cf-bd15-af6d6306db7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "f5f00b75-9a0b-423b-a2a8-150d6ad1a2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "970483c6-c116-46cf-bd15-af6d6306db7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1392b16f-c3fe-4afb-a3ab-d3a237e4e093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "970483c6-c116-46cf-bd15-af6d6306db7e",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "480b4dae-cbda-485a-9116-541aa3877f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "68b2f369-2343-44b0-8c8d-337bb9a92dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480b4dae-cbda-485a-9116-541aa3877f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7699c96-2920-44a4-b3eb-1029ac629043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480b4dae-cbda-485a-9116-541aa3877f7a",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "033cbf3c-9ac8-4083-970a-eeefe3fa3ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "d751c237-3883-4c89-82c0-262ec7439394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033cbf3c-9ac8-4083-970a-eeefe3fa3ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ce3769-e350-472f-a619-cbe41e89c4ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033cbf3c-9ac8-4083-970a-eeefe3fa3ec3",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "7eeba229-ecef-4bfa-b870-3b7b3f28be2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "5de94c40-377f-425e-bd25-67000c934e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eeba229-ecef-4bfa-b870-3b7b3f28be2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da1e5523-c49b-4516-bf74-cb864c69d9b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eeba229-ecef-4bfa-b870-3b7b3f28be2f",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "d3b33f17-3e4f-4728-b7e8-1889a1028e38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "6bab147f-6b6e-4866-9a21-167ca4019ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b33f17-3e4f-4728-b7e8-1889a1028e38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce3a9a15-8542-4618-ae08-e56061fc0196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b33f17-3e4f-4728-b7e8-1889a1028e38",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "ab45b218-e3ec-4e79-ba03-0bdc1d035878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "129f9c3a-d2cf-4d64-93ff-204ad57305d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab45b218-e3ec-4e79-ba03-0bdc1d035878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd72ad3-287d-4bc4-9f45-d2c880ed49e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab45b218-e3ec-4e79-ba03-0bdc1d035878",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "a6d60d53-64ad-4bd4-95ec-61d3507c76c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "013dd9b1-9bcd-4dc4-9660-8aefb3deb837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6d60d53-64ad-4bd4-95ec-61d3507c76c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41eef37-b862-4ce5-af51-1dc64da6a2d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6d60d53-64ad-4bd4-95ec-61d3507c76c1",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "cb1acebe-afc0-48bc-831e-48a31294c503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "12b49adf-f8ff-40cd-873b-c2a34e74ee20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1acebe-afc0-48bc-831e-48a31294c503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "264f1761-9ccb-4453-a197-b003173187be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1acebe-afc0-48bc-831e-48a31294c503",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "30dfc872-b505-4d77-861a-e0cb77fa116e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "5f5ce4f2-6e7b-4cd5-9bd8-e4274e2944f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30dfc872-b505-4d77-861a-e0cb77fa116e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb0e6be3-c88f-496a-b1a7-78394d205405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30dfc872-b505-4d77-861a-e0cb77fa116e",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "cdd32139-dc43-4a9e-8f0a-0fa079285dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "d5b1efeb-d4cf-4a86-a2ef-bc4633b622ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd32139-dc43-4a9e-8f0a-0fa079285dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "654a1aab-4873-4931-be1d-f2c8c790323c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd32139-dc43-4a9e-8f0a-0fa079285dcf",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "40398f94-b97e-4217-9ffd-6ecf99ad9cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "8f6a5172-bc59-43eb-b105-0c6fee583f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40398f94-b97e-4217-9ffd-6ecf99ad9cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08e9c6a5-839a-467d-bcf7-925453f9051b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40398f94-b97e-4217-9ffd-6ecf99ad9cc0",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        },
        {
            "id": "dd1e8afe-7866-42a5-b40d-9a7c66302dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "compositeImage": {
                "id": "6870fdab-9ebb-4358-abc2-9627626179c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd1e8afe-7866-42a5-b40d-9a7c66302dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62ab51b-0eb1-4301-9171-3c4f700b31c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd1e8afe-7866-42a5-b40d-9a7c66302dbc",
                    "LayerId": "8dc63641-a492-4ebc-965b-8edf3c8eb81f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8dc63641-a492-4ebc-965b-8edf3c8eb81f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d80b3ce-deb7-43b7-a8a6-e1f831eaa39b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}