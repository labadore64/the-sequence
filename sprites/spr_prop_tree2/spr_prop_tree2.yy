{
    "id": "2eda9a96-11cc-4cda-a49d-ef97aeaf76fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_tree2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 17,
    "bbox_right": 114,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cca3720b-6205-4995-85aa-3ed15e7f2dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2eda9a96-11cc-4cda-a49d-ef97aeaf76fa",
            "compositeImage": {
                "id": "33afeac4-bf8c-452b-b1f6-ed81a0cdab8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca3720b-6205-4995-85aa-3ed15e7f2dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba51e39f-d93e-41d4-9ab1-30df21acfc32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca3720b-6205-4995-85aa-3ed15e7f2dea",
                    "LayerId": "7212226f-cdef-4a3b-a4a6-4598ab6957d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7212226f-cdef-4a3b-a4a6-4598ab6957d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2eda9a96-11cc-4cda-a49d-ef97aeaf76fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.470750332,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}