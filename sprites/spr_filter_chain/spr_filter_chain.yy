{
    "id": "f6bb70c5-94b4-4fad-b188-8d3ff2200f7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_chain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 211,
    "bbox_left": 49,
    "bbox_right": 225,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "211c2cfa-665d-4074-9b62-cb8a4ad4cde9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6bb70c5-94b4-4fad-b188-8d3ff2200f7c",
            "compositeImage": {
                "id": "f29f189a-d383-421d-aee3-28a7d57a1477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "211c2cfa-665d-4074-9b62-cb8a4ad4cde9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeadb20f-c99e-4b9a-963f-0660e4501052",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "211c2cfa-665d-4074-9b62-cb8a4ad4cde9",
                    "LayerId": "2671acd1-ee05-48b0-9526-686ea9ff2ac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2671acd1-ee05-48b0-9526-686ea9ff2ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6bb70c5-94b4-4fad-b188-8d3ff2200f7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}