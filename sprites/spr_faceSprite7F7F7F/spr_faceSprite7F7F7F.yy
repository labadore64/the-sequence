{
    "id": "bc5b04b2-630b-4abe-8b70-990cc84dfc69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faceSprite7F7F7F",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ea699f4-cfe1-4852-bd51-66b085fe0401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc5b04b2-630b-4abe-8b70-990cc84dfc69",
            "compositeImage": {
                "id": "5b11c380-2cb5-4db2-baa1-d50d25b63a05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea699f4-cfe1-4852-bd51-66b085fe0401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7b751e-003e-40fe-94a2-d543b55e16df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea699f4-cfe1-4852-bd51-66b085fe0401",
                    "LayerId": "0c83e309-afb9-4a9b-8354-02d99ad3039a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0c83e309-afb9-4a9b-8354-02d99ad3039a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc5b04b2-630b-4abe-8b70-990cc84dfc69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "19b41542-e9b6-42a9-94c7-05cbadc1d852",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}