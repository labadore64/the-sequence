{
    "id": "441e7915-05f6-4bbc-9bd8-1fcea26b850f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sparklePurple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 55,
    "bbox_right": 73,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f7a9fe6-7c1d-4055-8089-5552b30d2430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "441e7915-05f6-4bbc-9bd8-1fcea26b850f",
            "compositeImage": {
                "id": "65d4706d-98cd-497d-b97c-204896f5dac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7a9fe6-7c1d-4055-8089-5552b30d2430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a1f742-8250-4749-a76b-30858a7d11f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7a9fe6-7c1d-4055-8089-5552b30d2430",
                    "LayerId": "3eac94a9-870b-4e25-804a-f2176cfdab10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3eac94a9-870b-4e25-804a-f2176cfdab10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "441e7915-05f6-4bbc-9bd8-1fcea26b850f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}