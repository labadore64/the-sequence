{
    "id": "b0686f71-29ee-4527-b952-70b74ad3c7a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_cast1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 87,
    "bbox_right": 202,
    "bbox_top": 134,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2eed5b3-7c44-4475-b716-3ac9b9ef5dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0686f71-29ee-4527-b952-70b74ad3c7a7",
            "compositeImage": {
                "id": "50481a8f-6028-4c43-8465-82226f9979de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2eed5b3-7c44-4475-b716-3ac9b9ef5dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47465103-00dd-4b95-b996-d277c2d0bfa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2eed5b3-7c44-4475-b716-3ac9b9ef5dd0",
                    "LayerId": "7bb4c673-f119-48ca-9cbc-54840634ce1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7bb4c673-f119-48ca-9cbc-54840634ce1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0686f71-29ee-4527-b952-70b74ad3c7a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}