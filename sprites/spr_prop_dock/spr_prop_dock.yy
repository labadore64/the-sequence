{
    "id": "dc2c02b7-959f-43e3-8425-2c02234bbd35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_dock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 44,
    "bbox_right": 59,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39524c1a-0128-4f8c-8745-c8ac137f3ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc2c02b7-959f-43e3-8425-2c02234bbd35",
            "compositeImage": {
                "id": "f983f91c-ddc8-4bb5-af4b-11605155aad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39524c1a-0128-4f8c-8745-c8ac137f3ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83a2396-9995-46b9-8f6f-31c757ecaf7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39524c1a-0128-4f8c-8745-c8ac137f3ea8",
                    "LayerId": "6c0305cf-6d49-462b-a7b0-fbb792c5669e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6c0305cf-6d49-462b-a7b0-fbb792c5669e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc2c02b7-959f-43e3-8425-2c02234bbd35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}