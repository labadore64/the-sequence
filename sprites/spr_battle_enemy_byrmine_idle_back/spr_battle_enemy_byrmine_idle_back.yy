{
    "id": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 11,
    "bbox_right": 232,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c2ed30e-7f99-4329-b920-3444de198f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "7427bd92-6460-4fe9-9e14-86723909da39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2ed30e-7f99-4329-b920-3444de198f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16605182-9ce9-4033-a2a8-d394fb6f2a78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2ed30e-7f99-4329-b920-3444de198f59",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "e4855fff-f748-4a00-9b68-dfcede141cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "9a31d823-35f0-424d-af2e-e9124a58621e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4855fff-f748-4a00-9b68-dfcede141cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b83b6e2e-bea2-4950-a23c-146fa8e82540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4855fff-f748-4a00-9b68-dfcede141cf7",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "eb5f2c6b-4ab4-44d7-a6ee-92d944a234af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "56afd2a6-6723-45ea-8e8a-9fdd40afa27c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb5f2c6b-4ab4-44d7-a6ee-92d944a234af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02605e9a-d72a-499d-83e7-292e568fab1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb5f2c6b-4ab4-44d7-a6ee-92d944a234af",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "5c2c369e-062a-4398-828b-b9e35f542d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "3db0273a-fc05-4ef8-9546-1f231b1f11b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c2c369e-062a-4398-828b-b9e35f542d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c36ee20-f300-4ca9-af96-61127888a99a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c2c369e-062a-4398-828b-b9e35f542d86",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "df559374-e36c-4d16-93b6-f6bec6018140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "b2f26086-f26d-4f4c-9c86-43142907ae28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df559374-e36c-4d16-93b6-f6bec6018140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c73a38d-3a67-463f-a4a8-20b8b35e4201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df559374-e36c-4d16-93b6-f6bec6018140",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "fffbd2aa-ba79-4983-b5f6-1f2de636e898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "0b1afb84-ba16-411a-b083-9dc01a8871c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fffbd2aa-ba79-4983-b5f6-1f2de636e898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ad9a0e-7109-4ab9-ac05-048ec3936434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fffbd2aa-ba79-4983-b5f6-1f2de636e898",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "699a6f11-afaa-400d-ab12-1218b962943d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "2044140b-1d95-4d0b-a8f8-f4cd3cf88b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699a6f11-afaa-400d-ab12-1218b962943d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7781b6fc-c33f-45cb-80f4-38674646e53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699a6f11-afaa-400d-ab12-1218b962943d",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "77eea469-b9e7-400e-9060-350e15409ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "907ca9c7-f552-4fd4-ac0e-eb98f6021b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77eea469-b9e7-400e-9060-350e15409ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee13af7f-e869-4a18-808e-0237261da022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77eea469-b9e7-400e-9060-350e15409ba3",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "55405a31-7c72-48cb-bacf-c5468df0f527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "76d08958-4bd9-4035-a964-6d1303063db3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55405a31-7c72-48cb-bacf-c5468df0f527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be1157f-ab30-43de-9a43-2eecd94e2f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55405a31-7c72-48cb-bacf-c5468df0f527",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "17a47f0b-5200-4fed-9107-c7c3d044e1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "1c27f871-c0bf-4bf4-abda-14a90380b71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a47f0b-5200-4fed-9107-c7c3d044e1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02702905-27c3-4349-8d70-bc2ad954fed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a47f0b-5200-4fed-9107-c7c3d044e1db",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "310f7145-664c-460e-9920-63fdc13e0900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "4a65a46e-9aef-4fe8-bb66-424e626a892f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310f7145-664c-460e-9920-63fdc13e0900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "740438d6-11f4-4ba1-bf11-f79748b4343a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310f7145-664c-460e-9920-63fdc13e0900",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "f406f8ff-59b5-4fd2-9b0f-3fdcb89cf5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "84447b9c-5a8b-4ee3-833c-259573942a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f406f8ff-59b5-4fd2-9b0f-3fdcb89cf5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4245edee-be51-46fe-882f-2ab875d8b3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f406f8ff-59b5-4fd2-9b0f-3fdcb89cf5df",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "22e3474a-3e27-4aa3-b395-7b78991f46d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "06ec9ef8-ac7e-4c23-b1d4-dd920cd389ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e3474a-3e27-4aa3-b395-7b78991f46d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81111d52-223f-4726-bd68-4386a28cf34c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e3474a-3e27-4aa3-b395-7b78991f46d4",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "81ef16ac-35b2-4bd9-9d92-9cffa3691ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "de1ba31c-798d-41ab-b206-74624e46d491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ef16ac-35b2-4bd9-9d92-9cffa3691ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a5b9d5-677f-4bb2-a947-772765521cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ef16ac-35b2-4bd9-9d92-9cffa3691ebb",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "57ac7674-4417-46b2-b7e8-455164b86ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "aebaba09-0b1e-498f-b90c-6ee18ccfbce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57ac7674-4417-46b2-b7e8-455164b86ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619d75d6-c211-4bb7-877a-ee6ae02b8790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57ac7674-4417-46b2-b7e8-455164b86ccc",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "b5600b17-4b64-4031-9b87-ed6d61d4a868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "40ddb0d9-1f0e-4db2-aa93-239e92212205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5600b17-4b64-4031-9b87-ed6d61d4a868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "326b67f3-879f-4b66-be20-3359d3006e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5600b17-4b64-4031-9b87-ed6d61d4a868",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "e091b7a5-d8de-4afc-adeb-24117fe38f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "a11ec6c1-8a6d-4897-99b6-f2ff3479886e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e091b7a5-d8de-4afc-adeb-24117fe38f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a0f9434-0b73-4549-8511-380f5bd05dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e091b7a5-d8de-4afc-adeb-24117fe38f5a",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "8e14d47d-6353-4b7b-a95e-af90c048983d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "33960c3b-1a80-4643-ba8c-ea49db38506c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e14d47d-6353-4b7b-a95e-af90c048983d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6d95799-4e6d-4ec7-8498-2182a5ff61d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e14d47d-6353-4b7b-a95e-af90c048983d",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "fab5bdfe-3e55-40b9-acc7-bc6eff113a2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "f7846453-715f-4a94-8fba-05905544de72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab5bdfe-3e55-40b9-acc7-bc6eff113a2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71456bb-11a1-4b85-896c-f303372be495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab5bdfe-3e55-40b9-acc7-bc6eff113a2f",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "d2704ff7-3855-4e0d-ab8f-007d54f7cb71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "a17d5b7d-cae4-42f6-bf2a-29933008462a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2704ff7-3855-4e0d-ab8f-007d54f7cb71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c864dd82-67b7-4881-9b38-8a06b668ebf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2704ff7-3855-4e0d-ab8f-007d54f7cb71",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "c7eb1812-641e-4597-b21f-2328f23ef012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "eca29e88-f17b-4e24-abd4-2cbd99113517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7eb1812-641e-4597-b21f-2328f23ef012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237d88ab-3c80-46a8-ab2e-85d6778188f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7eb1812-641e-4597-b21f-2328f23ef012",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "d2e063df-779f-4b15-ae5d-0d26bbfa7764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "a58e7c9e-b59c-4f52-8876-263d39c0de00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e063df-779f-4b15-ae5d-0d26bbfa7764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd92572-2f27-4a3d-a959-d7eb4774782e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e063df-779f-4b15-ae5d-0d26bbfa7764",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "c6800b02-d4cb-4196-ae85-11593a462c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "549f9cb9-0a68-41bb-b864-80950bcf8c2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6800b02-d4cb-4196-ae85-11593a462c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a21697f-f493-4325-94d1-44fcaff045d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6800b02-d4cb-4196-ae85-11593a462c04",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "9155981e-2b32-4413-b791-b345fdeff77b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "4e797f5b-6a0c-4e6d-99f4-4780895f828b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9155981e-2b32-4413-b791-b345fdeff77b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab33dc5-2c87-4087-8f81-afa4806ec50d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9155981e-2b32-4413-b791-b345fdeff77b",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "2257d895-0ba4-414c-8040-c9614863578f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "6ac45b93-6402-4b83-92a0-caca9ea364b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2257d895-0ba4-414c-8040-c9614863578f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1855558a-e8d3-4695-8a93-8a7d9b61c201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2257d895-0ba4-414c-8040-c9614863578f",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "f7352037-093a-4ae0-afcc-d89ddac2cbb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "fafdf7a3-b2ba-4787-8b51-8128a53773e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7352037-093a-4ae0-afcc-d89ddac2cbb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87303a0c-96d7-4e34-aef6-4c666a03aa00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7352037-093a-4ae0-afcc-d89ddac2cbb2",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "cecb3a21-8447-42a4-a9ac-038afb11f05f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "199ec15b-04ce-4ce6-b6d8-56c092c25977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecb3a21-8447-42a4-a9ac-038afb11f05f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55591867-edeb-46af-9f12-4bd3d98ce668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecb3a21-8447-42a4-a9ac-038afb11f05f",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "2372d465-0fe1-4b0d-b7bd-c2202a3da164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "0ea484dc-bc20-4314-b9bb-c9594cb8638a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2372d465-0fe1-4b0d-b7bd-c2202a3da164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb1c7f8-dfa0-4984-9229-ff5e8a08e416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2372d465-0fe1-4b0d-b7bd-c2202a3da164",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "df7ad2e7-e570-42e4-b909-312c75abdf6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "5ef94fc7-7fad-40c7-9cfc-e70c1ba4c7ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df7ad2e7-e570-42e4-b909-312c75abdf6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12adb0b4-4a5f-41f7-80f1-d1dbfb4d9aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df7ad2e7-e570-42e4-b909-312c75abdf6e",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "5787964c-0dbe-4be9-8706-85378aee6047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "77546c45-44bf-4f1c-8abd-2fc03f906529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5787964c-0dbe-4be9-8706-85378aee6047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94cb4651-e2d1-4c97-8243-74de34681c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5787964c-0dbe-4be9-8706-85378aee6047",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "f617de53-88f4-4dc5-a8b9-972baed4e06a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "3aee4016-81ca-4470-90d3-1237198e35a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f617de53-88f4-4dc5-a8b9-972baed4e06a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc8a711b-63ea-4c52-9eb1-2924528970d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f617de53-88f4-4dc5-a8b9-972baed4e06a",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "a87456e4-48d7-408e-a168-bcd7d7f47989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "f2a02024-14eb-4239-a291-af626e25d13e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87456e4-48d7-408e-a168-bcd7d7f47989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2871f453-2523-4f51-9bb7-1974989c1f63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87456e4-48d7-408e-a168-bcd7d7f47989",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "3679a2d4-8fbc-41c6-b63d-de74174c3db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "d1b717aa-d7d8-4605-ac96-210403e086c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3679a2d4-8fbc-41c6-b63d-de74174c3db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2874ef52-1b1d-4bfc-a71a-62136eb40f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3679a2d4-8fbc-41c6-b63d-de74174c3db2",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "cb6770fb-13e6-4fc5-9c54-b6ae1ad8f45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "adf1fbc5-e030-4cc9-a3ed-7a4aebf27624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6770fb-13e6-4fc5-9c54-b6ae1ad8f45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69ef209e-9e09-4a3c-b821-6fcf645d25f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6770fb-13e6-4fc5-9c54-b6ae1ad8f45c",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "77e416f0-6ea8-4030-b320-3ad7694ebbf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "32a166bf-0cf4-49c0-9b0d-cb9ad9214fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e416f0-6ea8-4030-b320-3ad7694ebbf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b620d8-0efa-4df5-b1eb-112da7795b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e416f0-6ea8-4030-b320-3ad7694ebbf9",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "4eb23aa2-dd66-41c9-90a9-c6eb96d7f29e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "57db799b-0681-45b5-bf5b-ff99cca2664c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb23aa2-dd66-41c9-90a9-c6eb96d7f29e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c3586cc-5446-4705-b070-209a1e93bd5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb23aa2-dd66-41c9-90a9-c6eb96d7f29e",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "81cb9a26-d590-4ffd-a99b-71e8d1414b07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "8dac60c9-4ca5-4940-8897-cfd396a445e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81cb9a26-d590-4ffd-a99b-71e8d1414b07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3468dce-3a8c-4a7e-aee4-572a20ded986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81cb9a26-d590-4ffd-a99b-71e8d1414b07",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "846c750f-2a2e-4c0c-89c7-400755d25120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "bb19fe48-caf2-49ba-86f6-9cea416d4add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "846c750f-2a2e-4c0c-89c7-400755d25120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1cead35-f886-4d12-95ad-03f2622e987c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "846c750f-2a2e-4c0c-89c7-400755d25120",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "27d61008-7a64-45dc-b206-6b19dd23bb37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "e90a41d5-5490-4394-b7d9-0525b36d8f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d61008-7a64-45dc-b206-6b19dd23bb37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1e0248-4816-4801-8c7c-7e20e0d1804c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d61008-7a64-45dc-b206-6b19dd23bb37",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        },
        {
            "id": "0c37a579-66d7-4b80-8379-15d025401628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "compositeImage": {
                "id": "19dad11b-3622-4d14-93e1-9627b43301f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c37a579-66d7-4b80-8379-15d025401628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5119724d-b4d9-4472-85fe-f12e71c7b450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c37a579-66d7-4b80-8379-15d025401628",
                    "LayerId": "76bc5854-0194-4bfc-879c-4efadd76df49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "76bc5854-0194-4bfc-879c-4efadd76df49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb71abde-ee67-4827-97e7-b8e591d9ab79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}