{
    "id": "38d8cd8b-9f18-476d-8574-3bb7b34de10d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_sprite_braille_burn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e81d7c84-c606-471d-a588-3abf75da6be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38d8cd8b-9f18-476d-8574-3bb7b34de10d",
            "compositeImage": {
                "id": "81d2d459-fdb1-41f4-b43f-ff1d3cd2b691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81d7c84-c606-471d-a588-3abf75da6be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a825f6dd-51b6-4df8-9502-ecac9d9b8943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81d7c84-c606-471d-a588-3abf75da6be4",
                    "LayerId": "f53e541c-d2c7-4167-9304-9bff6d8124f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f53e541c-d2c7-4167-9304-9bff6d8124f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38d8cd8b-9f18-476d-8574-3bb7b34de10d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}