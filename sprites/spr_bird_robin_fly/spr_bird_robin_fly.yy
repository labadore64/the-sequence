{
    "id": "dd56dc09-65dc-4905-8670-8580855d4e17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_robin_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 13,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c85185ba-4f0c-4d11-8bdb-0a96f864c45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd56dc09-65dc-4905-8670-8580855d4e17",
            "compositeImage": {
                "id": "2b3df0f5-1cfd-4b5d-b152-3828e4913f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85185ba-4f0c-4d11-8bdb-0a96f864c45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3603cd4a-a502-4cc8-b234-fb645b2a99b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85185ba-4f0c-4d11-8bdb-0a96f864c45e",
                    "LayerId": "4c2c23dc-d9ff-4418-a8f1-6bc8ea57a193"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4c2c23dc-d9ff-4418-a8f1-6bc8ea57a193",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd56dc09-65dc-4905-8670-8580855d4e17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}