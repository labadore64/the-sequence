{
    "id": "9bed459c-a0ff-47bf-8009-4820bb562d5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_attack_front1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 49,
    "bbox_right": 210,
    "bbox_top": 102,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92d8b6b5-8e0e-4c86-a906-8786dfebdbb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bed459c-a0ff-47bf-8009-4820bb562d5f",
            "compositeImage": {
                "id": "7c9ef746-92bf-45e0-a92e-db8a76ee752e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d8b6b5-8e0e-4c86-a906-8786dfebdbb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e09aa85-cbb8-4092-94bf-ab2b0c4f60ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d8b6b5-8e0e-4c86-a906-8786dfebdbb3",
                    "LayerId": "1b762a3b-977f-48f2-9351-c87b50611531"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1b762a3b-977f-48f2-9351-c87b50611531",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bed459c-a0ff-47bf-8009-4820bb562d5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}