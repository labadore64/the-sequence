{
    "id": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 200,
    "bbox_top": 125,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3682efa1-0def-41ec-9ccf-ba992047e974",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "f9e4fa79-7e0c-451b-ae91-7f40d7fb66ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3682efa1-0def-41ec-9ccf-ba992047e974",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6622c3ed-ef56-4f7b-a574-8f6f776e0d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3682efa1-0def-41ec-9ccf-ba992047e974",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "50ec9ba6-6000-4a05-827f-5fd05e340e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "28cef275-b546-455e-a2f0-748e0ce84f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ec9ba6-6000-4a05-827f-5fd05e340e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d52f852-df1d-44cc-b6fb-ef84b66a18bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ec9ba6-6000-4a05-827f-5fd05e340e00",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "d90f5775-ec17-4963-8e83-135627c0a0a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "780c9fd7-275c-4936-a6de-5543fc261267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90f5775-ec17-4963-8e83-135627c0a0a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6addbdd-d81f-4d92-8498-94f97edaa81b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90f5775-ec17-4963-8e83-135627c0a0a3",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "4a3781c6-5399-4897-b130-91d2ad423bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "76f4298b-5403-4b41-b913-9d0ea7e28b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a3781c6-5399-4897-b130-91d2ad423bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b8d7fa4-c54f-4ede-81e8-75762cc55392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a3781c6-5399-4897-b130-91d2ad423bcc",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "d3e5e8ca-0caf-42a5-8b12-505cab8868c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "00d25132-905f-49dc-b277-e5a1b73682ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3e5e8ca-0caf-42a5-8b12-505cab8868c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd7c170b-6b31-4521-9d78-3f16d9d65136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3e5e8ca-0caf-42a5-8b12-505cab8868c7",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "e0c77932-1287-4229-a859-30630a9e37c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "53bd05be-b516-4a77-870f-2faf96f1dd4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c77932-1287-4229-a859-30630a9e37c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ba88649-76e7-4040-800b-18bdf5763b54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c77932-1287-4229-a859-30630a9e37c5",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "b84761c0-81b4-427f-a864-8593460f17bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "6783eebe-7f02-408e-a427-0b8c330b371c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84761c0-81b4-427f-a864-8593460f17bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ffea10-1c47-41a3-986c-2365063c00a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84761c0-81b4-427f-a864-8593460f17bd",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "afb63575-5d48-43c7-8309-cc51ab3b042b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "8960df53-8050-439f-a30e-8e4cf20dcb89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb63575-5d48-43c7-8309-cc51ab3b042b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef07f15-2030-419f-9474-35d2891b9417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb63575-5d48-43c7-8309-cc51ab3b042b",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "ae81d935-7e2a-4ff2-a0b5-80846233831d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "d3b5aed7-9432-449e-aaa0-c625f2bff48c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae81d935-7e2a-4ff2-a0b5-80846233831d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25b9ab1-e3e2-471e-9fff-fb58f19822ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae81d935-7e2a-4ff2-a0b5-80846233831d",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "e1fb9aad-8d80-4d58-b7c5-f3e0b971910b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "249bb15f-ba4e-45cd-a1c6-a13e0d49683e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1fb9aad-8d80-4d58-b7c5-f3e0b971910b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32524b8-1708-4dc7-b568-c13796c24bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1fb9aad-8d80-4d58-b7c5-f3e0b971910b",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "347c55ce-0e18-43f0-a7b2-340bff704567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "b9e3da17-d64e-41d8-b7af-e8ee3bca0710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "347c55ce-0e18-43f0-a7b2-340bff704567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4321bfdf-ec40-4d31-bc00-f6dc6ab4043b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "347c55ce-0e18-43f0-a7b2-340bff704567",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "5909001a-a01c-4c0e-918c-ae3eefe02ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "bed63975-5ca6-4db3-ac4d-d3f5aef1d4a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5909001a-a01c-4c0e-918c-ae3eefe02ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fb0a8c0-c5b8-4819-a1bf-15233d88a8bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5909001a-a01c-4c0e-918c-ae3eefe02ffe",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "b01db595-755d-49d2-9146-d2a21554701f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "4b9e3910-9cd7-4200-9b18-9b977dfddd6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01db595-755d-49d2-9146-d2a21554701f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04303031-3e7d-4f8a-92ff-4129c05601fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01db595-755d-49d2-9146-d2a21554701f",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "658834f6-0ac3-4432-ad8b-228a4add7a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "519255fa-acb7-4c76-864d-e281d37454dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658834f6-0ac3-4432-ad8b-228a4add7a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea30ea73-28a3-4032-bb91-e7edb48b4954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658834f6-0ac3-4432-ad8b-228a4add7a83",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        },
        {
            "id": "1b2c74f1-f44a-413d-956e-b9567b0360eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "compositeImage": {
                "id": "a99ccebd-0e15-4a97-883d-6e987adefeee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b2c74f1-f44a-413d-956e-b9567b0360eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3fe68d4-f0b1-41c3-95af-8602ec9677b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2c74f1-f44a-413d-956e-b9567b0360eb",
                    "LayerId": "f562b1ed-1a45-4d6d-9517-5781281d06c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f562b1ed-1a45-4d6d-9517-5781281d06c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a95867fe-2d2b-4894-8b98-bc32b9a119be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}