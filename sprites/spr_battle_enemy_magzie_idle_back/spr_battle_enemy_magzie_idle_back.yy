{
    "id": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_magzie_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 38,
    "bbox_right": 202,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8043414-e77e-48d6-bebb-028bf0eb5564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "44771995-914e-419d-a3f5-f60133501edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8043414-e77e-48d6-bebb-028bf0eb5564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75aba85f-c9e1-41cf-8743-406807f599d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8043414-e77e-48d6-bebb-028bf0eb5564",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "a41a3402-0a9f-4201-a1d7-cb6f8f461d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "2a9bfa1b-9926-41c3-a949-fc42872cba79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a41a3402-0a9f-4201-a1d7-cb6f8f461d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2681f014-8874-4deb-a738-d62fa912268c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a41a3402-0a9f-4201-a1d7-cb6f8f461d4b",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "b5e57578-0a0f-4c97-a8ee-797ef6abe8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "7e049e2f-d9a5-41fe-b6ad-854d985725da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e57578-0a0f-4c97-a8ee-797ef6abe8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9553c8e9-b470-43cb-a5ce-68f5e90df4ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e57578-0a0f-4c97-a8ee-797ef6abe8f2",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "3df587f8-0d01-464f-8f2e-4a6caddf29e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "03581880-10df-410e-ad36-0865c8969312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df587f8-0d01-464f-8f2e-4a6caddf29e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e1555a-bead-4f61-a1fc-f451fba0e840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df587f8-0d01-464f-8f2e-4a6caddf29e8",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "e496cb1f-beab-4df8-9798-27cea83fe4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "70088219-a452-4107-9bf7-28e8766bfe03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e496cb1f-beab-4df8-9798-27cea83fe4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4e7f5b-ea95-499a-b2ba-5e3d4d8619e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e496cb1f-beab-4df8-9798-27cea83fe4cc",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "a61fac2a-a6c7-4af0-980f-09c0e87a2f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "5c42a5cc-769b-4ebc-8aef-1b8f61181e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61fac2a-a6c7-4af0-980f-09c0e87a2f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c4cf53e-7011-4378-bd9e-c073543460a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61fac2a-a6c7-4af0-980f-09c0e87a2f67",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "da7c7d92-2816-4a2c-8192-ca0593cb78f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "fb8d33f3-fd4b-4b41-b558-006cea00f756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da7c7d92-2816-4a2c-8192-ca0593cb78f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ec04d2-3535-483e-bfda-660f3b1245c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da7c7d92-2816-4a2c-8192-ca0593cb78f0",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "e3430e23-48bd-4532-ab7b-681383b8d1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "42978bba-fc68-4909-bea2-13ff98e2402e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3430e23-48bd-4532-ab7b-681383b8d1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5996a98-44ed-4a7c-bfd8-05ccde1ddfe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3430e23-48bd-4532-ab7b-681383b8d1e2",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "a3aa2603-eafc-481d-9476-0b8d4b8aa8f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "faa48440-b1cd-4ee8-827c-12e392d86d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3aa2603-eafc-481d-9476-0b8d4b8aa8f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "427c8559-6913-4e8a-bb66-97d2563d844c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3aa2603-eafc-481d-9476-0b8d4b8aa8f9",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "5389cff7-d472-43f9-8fcd-31cedc7e7ab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "8c20a942-e6d4-4632-add5-eab489f959ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5389cff7-d472-43f9-8fcd-31cedc7e7ab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498d1184-ebad-43cb-a2a4-458a8fe8f98e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5389cff7-d472-43f9-8fcd-31cedc7e7ab4",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "0d46367d-c183-4fa8-a26c-3f44b5462fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "6cbd84fd-aeeb-4fee-9cad-cc2ec03cb02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d46367d-c183-4fa8-a26c-3f44b5462fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8596f6-6fc9-4ddc-974a-33c55d5de0f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d46367d-c183-4fa8-a26c-3f44b5462fbc",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "be3b868c-501d-4353-968c-5a192175276e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "831dad68-7d22-42ef-9e0f-053c8e76104f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3b868c-501d-4353-968c-5a192175276e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ed0fb4-9578-4990-b586-cae95e31dc06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3b868c-501d-4353-968c-5a192175276e",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "ab8fb9a5-d83f-4560-afb3-5843336e52fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "1ffea953-e39b-4f60-ad4c-dc195417f196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab8fb9a5-d83f-4560-afb3-5843336e52fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94b45b9c-21aa-47d1-8dca-2b057ba6892c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab8fb9a5-d83f-4560-afb3-5843336e52fb",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "7e2e8840-5493-454f-a605-0a423f72fcd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "e0222338-d0e3-4cf6-8016-655693c03fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e2e8840-5493-454f-a605-0a423f72fcd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d674c3-01e0-4324-aeba-724b9749dc18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e2e8840-5493-454f-a605-0a423f72fcd8",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "c187a5e1-81c8-49d0-99b5-2e2bc49a1102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "8aba8326-5a0e-495c-a153-bde088cf765d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c187a5e1-81c8-49d0-99b5-2e2bc49a1102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e8b1dc-b883-4af2-b191-0a4546c351f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c187a5e1-81c8-49d0-99b5-2e2bc49a1102",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "c2b8cc8e-0370-4b89-afd7-e890155edcbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "ce078b7b-4122-41fb-8548-cb21ce26da2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b8cc8e-0370-4b89-afd7-e890155edcbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7ff871d-30d0-4a04-8046-34ea77a72202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b8cc8e-0370-4b89-afd7-e890155edcbd",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "739807c5-53cc-4fbb-a632-c8f7e2084bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "7278df86-8883-4b5c-9482-94b9b29a46d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "739807c5-53cc-4fbb-a632-c8f7e2084bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c1469ab-4376-468f-a958-d0c4b07cbf69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "739807c5-53cc-4fbb-a632-c8f7e2084bcd",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "0e0c365f-895d-4479-84de-49e795a9bc46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "8ef54cf5-486c-4318-b3c6-e07b845f374e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e0c365f-895d-4479-84de-49e795a9bc46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "660f69d4-9e9f-4e47-afc2-ea7b98eecfc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e0c365f-895d-4479-84de-49e795a9bc46",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "e91d059e-0856-4577-8998-de09445c2c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "ee7b8d4e-be2f-4bcb-8106-34fa5af66e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91d059e-0856-4577-8998-de09445c2c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2a80a8-f382-4618-b249-0b6283fc4786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91d059e-0856-4577-8998-de09445c2c02",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        },
        {
            "id": "a6d853c6-1209-4f03-bd11-eefe42147144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "compositeImage": {
                "id": "54eaf5b1-1976-4ad7-b5c0-c8be743a2a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6d853c6-1209-4f03-bd11-eefe42147144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ee45a0-a79a-4472-9e48-deb3b67d39c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6d853c6-1209-4f03-bd11-eefe42147144",
                    "LayerId": "8643622a-4ce9-4128-8c19-261bf6205d40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8643622a-4ce9-4128-8c19-261bf6205d40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5d0f79a-7a63-4866-bc1c-03577cbbeb71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "c521fe91-d359-41ad-a304-750ae173f15c",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}