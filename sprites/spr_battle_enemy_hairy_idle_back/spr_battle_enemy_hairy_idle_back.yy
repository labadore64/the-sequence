{
    "id": "caeba86d-88ec-424c-b2d5-750463d74ae4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 10,
    "bbox_right": 242,
    "bbox_top": 113,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd841c0b-a096-435d-9f96-d83d43c26a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "a23b3907-4b98-4972-bcb6-f4e9a06949ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd841c0b-a096-435d-9f96-d83d43c26a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1127bb67-ebc0-4ce5-96bb-fa08dfc6ca36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd841c0b-a096-435d-9f96-d83d43c26a05",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "a299557e-f01d-4d23-953d-f87f64b441e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "1ab12168-9543-4a80-94b4-9cf8ad9f5073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a299557e-f01d-4d23-953d-f87f64b441e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33409453-267b-46ac-91f5-58cbe75232c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a299557e-f01d-4d23-953d-f87f64b441e2",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "1198a781-98f4-43c7-b3e4-d0bf9efa7bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "bdc9a584-75a6-4d47-a407-6effe88d841a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1198a781-98f4-43c7-b3e4-d0bf9efa7bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a26abdc-c553-43b6-81ef-b681ed66de14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1198a781-98f4-43c7-b3e4-d0bf9efa7bd8",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "b6c8840a-9798-40c7-912a-f7883488eab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "41de2b1d-4b29-458d-a7ee-8596474b815f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c8840a-9798-40c7-912a-f7883488eab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ba899b-a955-41b5-b805-92b64b761903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c8840a-9798-40c7-912a-f7883488eab9",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "534f6a5f-98ca-4558-80d2-3a624d4a1d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "6f1fbc25-f022-4ddd-85b4-f4e998940cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534f6a5f-98ca-4558-80d2-3a624d4a1d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63751cc-6abe-469b-94a6-61be30ef625e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534f6a5f-98ca-4558-80d2-3a624d4a1d99",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "ca912419-e9b4-4e1c-8deb-b96d24228693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "3fce8b24-41d5-4640-8773-09ef4962bdb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca912419-e9b4-4e1c-8deb-b96d24228693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "514dfd56-5ce7-47bc-be21-9f854c7e6c81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca912419-e9b4-4e1c-8deb-b96d24228693",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "1c3f3a9d-3c60-44d3-a57b-0f0fe7af53c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "c720e5ce-50df-47eb-952e-892c9920e1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3f3a9d-3c60-44d3-a57b-0f0fe7af53c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24a70c09-d3d7-4e24-b6ce-c180268588d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3f3a9d-3c60-44d3-a57b-0f0fe7af53c1",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "87e11776-d121-476c-b865-798d56a70f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "0e58fd26-3426-4a06-926f-aff45cdc0ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87e11776-d121-476c-b865-798d56a70f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa51b36-eecd-4ed9-9adc-8bf031790912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87e11776-d121-476c-b865-798d56a70f90",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "51439bfa-39f3-4e42-b3c5-fab5bb24ccd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "f048e06b-5d62-4217-b8da-622301df2e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51439bfa-39f3-4e42-b3c5-fab5bb24ccd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1d804c-960a-48a2-b1b1-2aafac35e6f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51439bfa-39f3-4e42-b3c5-fab5bb24ccd2",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "8044a713-85e7-43c1-a5cb-78fa2e817d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "46ead023-f53e-4832-a1e7-2ccf3678f3cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8044a713-85e7-43c1-a5cb-78fa2e817d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e592b328-3fa3-4530-b367-7d52704e7b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8044a713-85e7-43c1-a5cb-78fa2e817d43",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "9d343992-0a9f-4e14-bec8-9860a01a0fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "f9d96874-083e-49bc-ae74-debf4268ea54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d343992-0a9f-4e14-bec8-9860a01a0fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e38a5e0-25d8-4aff-9f1b-3359c7dbf68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d343992-0a9f-4e14-bec8-9860a01a0fa3",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "0e96c819-63df-4656-a03c-62e169eabb23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "fb1a2508-9844-4ea8-833e-59ba63930598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e96c819-63df-4656-a03c-62e169eabb23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0d466f-11ac-4f60-94e2-3ae03c01dab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e96c819-63df-4656-a03c-62e169eabb23",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "724ed2d2-eb95-48ec-96ec-143be1eba3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "767f517f-f40e-4eea-b684-782e20d8907f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "724ed2d2-eb95-48ec-96ec-143be1eba3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccfbc0f7-b653-482a-891f-265428492d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "724ed2d2-eb95-48ec-96ec-143be1eba3a6",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "beba0148-873e-439d-8c0f-8ac699f7f607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "3cf9357d-36b7-420d-8bd9-f7767af5843e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beba0148-873e-439d-8c0f-8ac699f7f607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6e04c7-8afb-4bf1-9978-cb302899c2cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beba0148-873e-439d-8c0f-8ac699f7f607",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "3024f067-ebad-4063-b97c-4dfa87877e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "10ece2bc-2887-4419-a737-5a596a23a4fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3024f067-ebad-4063-b97c-4dfa87877e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b52d892-4f83-424d-ab46-1ae804de2fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3024f067-ebad-4063-b97c-4dfa87877e7b",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "77e6b234-2d64-451f-a26b-51281f4d541a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "c7937aac-9aa7-43a5-94a0-66f6f4f5306d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e6b234-2d64-451f-a26b-51281f4d541a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15648ee0-b991-442f-9ed9-ccb520b317f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e6b234-2d64-451f-a26b-51281f4d541a",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "7bea7d48-0011-4e92-8d41-f4529b42374c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "b7dd205e-63fe-4492-ad5d-28473996b980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bea7d48-0011-4e92-8d41-f4529b42374c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04394ca9-27ff-4a41-8994-be08646e5d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bea7d48-0011-4e92-8d41-f4529b42374c",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "3806b13e-79de-4614-92a1-1b4ea649aeca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "fe2d50e6-1ee9-4447-b846-005c2935235d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3806b13e-79de-4614-92a1-1b4ea649aeca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b02207-dcd5-46c4-a6ee-901b1b3d5673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3806b13e-79de-4614-92a1-1b4ea649aeca",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "87ca0d6e-5f9d-42b7-ab44-770994705321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "c7f8a652-b30f-4fb8-8217-f7464de470e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ca0d6e-5f9d-42b7-ab44-770994705321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8743734a-733d-417e-a92f-678528a49c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ca0d6e-5f9d-42b7-ab44-770994705321",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        },
        {
            "id": "d7a01263-b45e-473f-904c-ddf8d4ea7caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "compositeImage": {
                "id": "de26e4e2-abc8-47ef-9c31-9174fc8d67e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a01263-b45e-473f-904c-ddf8d4ea7caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47790f8-6e74-42f1-958d-3d90e179e9de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a01263-b45e-473f-904c-ddf8d4ea7caa",
                    "LayerId": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "26b4144a-0810-40b4-a3a9-52e8d6c09e9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "caeba86d-88ec-424c-b2d5-750463d74ae4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}