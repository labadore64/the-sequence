{
    "id": "f762dca8-83d7-4cab-b4e9-52121cf27143",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_jump_front1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 33,
    "bbox_right": 198,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fa51ed6-1158-41d7-bc7a-860daf685044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f762dca8-83d7-4cab-b4e9-52121cf27143",
            "compositeImage": {
                "id": "7925e44d-5371-497e-93ed-25392f59537b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa51ed6-1158-41d7-bc7a-860daf685044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efe764ce-2693-41b8-a58f-83063c969537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa51ed6-1158-41d7-bc7a-860daf685044",
                    "LayerId": "a7024583-42d5-4f4b-8a8f-5b5ee255858d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a7024583-42d5-4f4b-8a8f-5b5ee255858d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f762dca8-83d7-4cab-b4e9-52121cf27143",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}