{
    "id": "3f068d61-e4f2-4b33-9068-2a28ac10188d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_jump_back1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 233,
    "bbox_left": 43,
    "bbox_right": 191,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38357de3-c21b-474e-944b-b852d8cbd626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f068d61-e4f2-4b33-9068-2a28ac10188d",
            "compositeImage": {
                "id": "77056b49-01af-4f2c-a9e1-e0772c823d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38357de3-c21b-474e-944b-b852d8cbd626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a18872-e51f-4c42-8f52-68fcf2397e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38357de3-c21b-474e-944b-b852d8cbd626",
                    "LayerId": "1dbb7fe2-9d74-4a9e-b214-fe9584974122"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1dbb7fe2-9d74-4a9e-b214-fe9584974122",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f068d61-e4f2-4b33-9068-2a28ac10188d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}