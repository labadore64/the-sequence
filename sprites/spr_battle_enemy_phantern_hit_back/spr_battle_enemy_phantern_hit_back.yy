{
    "id": "85c5b723-6ea2-40dd-b447-9cd39802c922",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 78,
    "bbox_right": 183,
    "bbox_top": 121,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03e753e5-ca3d-4dd5-8953-e21245ff6737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c5b723-6ea2-40dd-b447-9cd39802c922",
            "compositeImage": {
                "id": "e8f0774a-0fd4-4c3f-8cc9-0d258cb05650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e753e5-ca3d-4dd5-8953-e21245ff6737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f1f882-e9e5-4db6-9360-b05f21ebb06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e753e5-ca3d-4dd5-8953-e21245ff6737",
                    "LayerId": "d9ec564b-ef78-458e-9b10-67250d895fe3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d9ec564b-ef78-458e-9b10-67250d895fe3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85c5b723-6ea2-40dd-b447-9cd39802c922",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}