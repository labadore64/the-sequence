{
    "id": "10b6513c-8eb8-4a2e-959e-7c04411b26ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 40,
    "bbox_right": 225,
    "bbox_top": 78,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9f57416-0ece-4cb0-8a0a-ee32e27fe76b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10b6513c-8eb8-4a2e-959e-7c04411b26ec",
            "compositeImage": {
                "id": "84f6c139-36f5-45af-a3ec-9c15d901e6f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f57416-0ece-4cb0-8a0a-ee32e27fe76b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ddcd4e-77ad-4f36-bd78-c3a005657cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f57416-0ece-4cb0-8a0a-ee32e27fe76b",
                    "LayerId": "b015b0bf-16b6-43e5-9ef4-66973288992c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b015b0bf-16b6-43e5-9ef4-66973288992c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10b6513c-8eb8-4a2e-959e-7c04411b26ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}