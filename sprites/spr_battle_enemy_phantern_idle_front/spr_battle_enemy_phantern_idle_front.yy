{
    "id": "36dca05c-6a82-4a7d-864d-a0372451cff5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 73,
    "bbox_right": 182,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be3eb74e-cc88-406c-bed6-637ec6599e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "af9f57ea-17f5-4e14-b58f-50fe5040b881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3eb74e-cc88-406c-bed6-637ec6599e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e44f4730-2ddf-44fc-be2a-924e5c5dff32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3eb74e-cc88-406c-bed6-637ec6599e9f",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "a59d6113-f9c1-4139-9d79-7790943f9613",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "fb024882-1449-4f7e-9db6-933bd181754a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a59d6113-f9c1-4139-9d79-7790943f9613",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42c4a31d-b9d2-4558-9ddd-6cfc51a468b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a59d6113-f9c1-4139-9d79-7790943f9613",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "f77d50af-028d-407f-8cee-faec28f0fb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "cc19a894-69b9-46a6-a31f-d39c4b9dcd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77d50af-028d-407f-8cee-faec28f0fb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf3a1d6-0c3f-4cac-8b25-f9abdd19eac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77d50af-028d-407f-8cee-faec28f0fb3f",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "e84f7285-96bf-47e4-8b7e-f471552da63e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "6446737b-99e7-498f-923b-b8167371b8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84f7285-96bf-47e4-8b7e-f471552da63e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1663d8bf-603d-457a-b73c-f653082d3219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84f7285-96bf-47e4-8b7e-f471552da63e",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "41a726ed-55e1-4790-9a60-74d876d179b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "d1bec8ac-3135-41e4-9f70-5a058635bfbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a726ed-55e1-4790-9a60-74d876d179b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9e4b5d-0d32-4dae-900e-ce8dc5f4d325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a726ed-55e1-4790-9a60-74d876d179b3",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "6d030536-ed2f-4599-9803-6e9fca786f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "78124534-26d6-4825-8722-161573af90cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d030536-ed2f-4599-9803-6e9fca786f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a86f3b5-d689-49e2-9e20-99438482ae86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d030536-ed2f-4599-9803-6e9fca786f1d",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "374c491c-4e12-427e-a228-31dffaeaf826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "778504cd-fad4-4843-b2e1-1e9a4261e985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374c491c-4e12-427e-a228-31dffaeaf826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded75ba9-2ff9-49f0-bcce-e553fd8486d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374c491c-4e12-427e-a228-31dffaeaf826",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "c491c668-544b-43ef-8483-60d7cdd0e8d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "b33ef5e2-58e1-4161-90d4-3adf8f8e38a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c491c668-544b-43ef-8483-60d7cdd0e8d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c585c642-233b-4847-9b21-193a02c6e1d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c491c668-544b-43ef-8483-60d7cdd0e8d3",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "a84d4254-1e87-424a-994b-b9982b4ccfca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "ff5bfeb7-564e-43db-b9db-96116dc1c468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84d4254-1e87-424a-994b-b9982b4ccfca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "749f28ad-e485-4d7e-8ed0-7867023a8667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84d4254-1e87-424a-994b-b9982b4ccfca",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "d3e4645f-4b71-47f5-b311-058912094d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "0ad44628-8cf7-4bf1-8f33-4882a3803578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3e4645f-4b71-47f5-b311-058912094d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c6c3ce-54df-4edf-9cf2-b99eee0ff750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3e4645f-4b71-47f5-b311-058912094d5b",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "e2acb1cd-2346-48e0-adb0-17fad4c66933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "1ce07799-b204-42a8-80fb-ccce3dc9c931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2acb1cd-2346-48e0-adb0-17fad4c66933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b21d261-eed4-4241-9d0e-2d5563a9a5f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2acb1cd-2346-48e0-adb0-17fad4c66933",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "7e1f3f71-7cac-4b6b-9175-668674213d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "79713481-fd68-4e9c-b1c0-c04a624e8c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1f3f71-7cac-4b6b-9175-668674213d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54530567-218d-4b8a-9f23-43939a969ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1f3f71-7cac-4b6b-9175-668674213d76",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "ef2f3065-05c6-47f2-8b65-10a1dc9537e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "83085043-5102-427e-b5cc-6afc5bc69504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2f3065-05c6-47f2-8b65-10a1dc9537e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ff5be2-432d-4d14-b502-025a8f7b457e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2f3065-05c6-47f2-8b65-10a1dc9537e6",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "7e1fd6b9-098a-416c-9818-90b8fbf74506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "2141ad14-8d97-4951-a0d9-24b12e0eb187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1fd6b9-098a-416c-9818-90b8fbf74506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d390196e-ee03-425b-a7bb-6cc10e10d66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1fd6b9-098a-416c-9818-90b8fbf74506",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "5ac6ba72-6443-44bb-85d0-a3b7d49248e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "30bd591c-7f17-4462-8fd5-1a3f7d4a60d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ac6ba72-6443-44bb-85d0-a3b7d49248e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0147d59-b39e-48ee-bad3-96ae93ef68e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ac6ba72-6443-44bb-85d0-a3b7d49248e1",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "a1f7cd7b-021e-448e-be76-51aa209d165b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "ebc7a306-b1f8-4eca-957f-8f7ed7425272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f7cd7b-021e-448e-be76-51aa209d165b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67a8d8c9-4e47-4b9b-97b9-04c568296a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f7cd7b-021e-448e-be76-51aa209d165b",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "a452e5be-1f9f-42b8-bd0e-5028f5d888c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "2d5f09a0-bc80-48a4-9c8d-a52f6002e84b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a452e5be-1f9f-42b8-bd0e-5028f5d888c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9d68a2-8e45-4acc-bf32-8e9bffa2f8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a452e5be-1f9f-42b8-bd0e-5028f5d888c7",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "87581ff9-6a8b-4f6f-92ab-350cae2d5e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "bb7d8f6b-14d1-4d55-a631-7627874c9ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87581ff9-6a8b-4f6f-92ab-350cae2d5e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4cfc22-a6ac-4038-a426-400f9ead6174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87581ff9-6a8b-4f6f-92ab-350cae2d5e2c",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "3b8a05bf-923b-4636-9e6d-59e4d4d8ea5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "2543a2f8-9fc0-4026-b538-7e1425aebd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8a05bf-923b-4636-9e6d-59e4d4d8ea5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b711c3f-4149-4e31-93ee-1165ec4029a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8a05bf-923b-4636-9e6d-59e4d4d8ea5b",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "87b448f2-d4b3-4ad2-8a8b-54f68d70b588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "f6af8aa6-24f2-4795-b45c-3b9d933f12f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b448f2-d4b3-4ad2-8a8b-54f68d70b588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f4fb9b-7f31-417e-aac2-7a124ad1d8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b448f2-d4b3-4ad2-8a8b-54f68d70b588",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "a4d386c1-af4f-42ea-9c57-ef573bb90f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "38f93525-d4d0-41d7-8eaa-81e0b2536058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4d386c1-af4f-42ea-9c57-ef573bb90f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1515ce-d343-49f0-8bf3-42d23b05abfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4d386c1-af4f-42ea-9c57-ef573bb90f39",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "92b26638-33fb-4ccb-ac8b-8e5c270a1c99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "e28214a0-1f24-4764-bd1c-b2cc3b7e0305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b26638-33fb-4ccb-ac8b-8e5c270a1c99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa6384b-5bab-474c-9164-0b40553babf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b26638-33fb-4ccb-ac8b-8e5c270a1c99",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "d3966f03-35c6-4457-9704-5cdc254f2af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "413309b2-888a-4c16-979a-bec6751fc0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3966f03-35c6-4457-9704-5cdc254f2af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd4cd02-07e0-4f6a-97f0-0dfaa80a37ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3966f03-35c6-4457-9704-5cdc254f2af5",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "b213f1ae-9bcf-4bca-8002-1b9cba2179fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "2f27bcc2-b5bb-4820-8b8b-f9190779dbcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b213f1ae-9bcf-4bca-8002-1b9cba2179fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3319c990-bae7-4674-a453-49d5090847c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b213f1ae-9bcf-4bca-8002-1b9cba2179fc",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "1a1b968d-2a73-44a6-bd74-53dec5e0d095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "15506e8d-f1e5-4878-9559-30cbd5bcb31e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a1b968d-2a73-44a6-bd74-53dec5e0d095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c854acc-d0c2-4f87-a36f-9388670ce579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a1b968d-2a73-44a6-bd74-53dec5e0d095",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "62b470c1-ebf8-462d-b7e9-0ea25ce78e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "1f5ef40c-cf6e-4a71-808c-dadb14d0fe06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b470c1-ebf8-462d-b7e9-0ea25ce78e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be03c61d-85c4-4890-9970-6cbbb725243e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b470c1-ebf8-462d-b7e9-0ea25ce78e0d",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "a93a3538-bc0b-437f-8329-6321e2e3389e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "4f580a82-80ea-49ee-b131-5d06c1fa9451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a93a3538-bc0b-437f-8329-6321e2e3389e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db21761-be32-488d-9d54-0a8c06980059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a93a3538-bc0b-437f-8329-6321e2e3389e",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "768b54d1-1e35-4b85-8b16-cabfefa14097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "6111fc92-ee38-4cfb-8e4e-a3e12fb37430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "768b54d1-1e35-4b85-8b16-cabfefa14097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2785c379-70cf-4cf2-8dba-c9f21cfec922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "768b54d1-1e35-4b85-8b16-cabfefa14097",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "c62fec56-ce88-4aa3-b0d9-719e2975dccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "90757fe8-f69e-4d93-9f9d-fbcac3329ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c62fec56-ce88-4aa3-b0d9-719e2975dccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29180c0-2c11-48be-8a2b-5ddc12aefd50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c62fec56-ce88-4aa3-b0d9-719e2975dccf",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "3c72db30-5d4f-4129-b899-1b7409ade4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "f55d0487-26a6-493b-af78-0c028a5d7516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c72db30-5d4f-4129-b899-1b7409ade4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60d1b12-ed61-428d-8501-b330259beb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c72db30-5d4f-4129-b899-1b7409ade4cc",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "4d87b022-b765-4648-b5b9-899b6c139e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "67ca6ac8-bad0-44cf-b6f4-b0e20982aa02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d87b022-b765-4648-b5b9-899b6c139e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d94319b5-55dd-4e3f-a4b5-a204283683d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d87b022-b765-4648-b5b9-899b6c139e9e",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "0f1b995d-177a-4123-9c42-d04d5b8be424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "abb527b1-fe11-46a4-9a46-45b0fdd4a0bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f1b995d-177a-4123-9c42-d04d5b8be424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ad11a3-d879-4531-ad43-348a9c9b924d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f1b995d-177a-4123-9c42-d04d5b8be424",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "ec53932d-9b0f-4050-8891-b112388dd154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "62fed6f2-52ba-4908-9871-309c0d4fadfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec53932d-9b0f-4050-8891-b112388dd154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2d75e6-e760-4f88-b3a2-d6451d64639b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec53932d-9b0f-4050-8891-b112388dd154",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "71376bd1-2708-4c4c-a74a-d6c33c57400c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "62f565a4-7e7f-47a3-97d7-3e1d2ed9b6cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71376bd1-2708-4c4c-a74a-d6c33c57400c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54898e4f-8044-4d2c-a896-dd5dbc9cafc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71376bd1-2708-4c4c-a74a-d6c33c57400c",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "c0888b19-49e2-4222-a0da-6941e14fc0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "d076913d-0df4-4f92-a42f-093f47089909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0888b19-49e2-4222-a0da-6941e14fc0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12499239-4efe-4e3a-8157-19512fa81e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0888b19-49e2-4222-a0da-6941e14fc0da",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "34b9d3fb-bcd1-4eba-9584-17247167d521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "a2484d81-d55c-4de2-a0b4-da6b4cf69a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b9d3fb-bcd1-4eba-9584-17247167d521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f20de8-b5fe-4614-af3c-da68dd6ceb90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b9d3fb-bcd1-4eba-9584-17247167d521",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "484713cb-d999-4ace-917a-541af92dff62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "3101ad4d-1920-446f-9860-2baa318089b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "484713cb-d999-4ace-917a-541af92dff62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66dc5ddc-a096-4cf0-bd13-2d51ce510be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "484713cb-d999-4ace-917a-541af92dff62",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "c02a6f89-f217-422c-afa2-e706a29e5d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "d709d5c7-9d3f-4d49-9f92-4567452934c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c02a6f89-f217-422c-afa2-e706a29e5d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ee3e962-d141-4a70-bd16-52c85467146b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c02a6f89-f217-422c-afa2-e706a29e5d67",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "2c1df624-31ef-486b-9950-0f9da5a9a076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "d5d0c69d-7866-43b3-804a-7853f1f96c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c1df624-31ef-486b-9950-0f9da5a9a076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24885257-fd54-4338-885a-0524033c5931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c1df624-31ef-486b-9950-0f9da5a9a076",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        },
        {
            "id": "1668cf19-842a-44cd-be5a-d737c8b8a217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "compositeImage": {
                "id": "38380015-a5e9-4dd0-8ea9-c84d94a9e758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1668cf19-842a-44cd-be5a-d737c8b8a217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ce78dfb-9943-4287-a144-3c951b804d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1668cf19-842a-44cd-be5a-d737c8b8a217",
                    "LayerId": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1f676b1e-2fcd-40f9-831f-6a34e6bf948d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36dca05c-6a82-4a7d-864d-a0372451cff5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}