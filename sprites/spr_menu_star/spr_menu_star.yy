{
    "id": "b7b05132-133c-410a-b09c-dcf09994c823",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_star",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7728e814-d21b-4f22-98c0-2be2c1d7ee4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7b05132-133c-410a-b09c-dcf09994c823",
            "compositeImage": {
                "id": "fdee3399-e2e2-48f0-afff-1f10ca7e98bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7728e814-d21b-4f22-98c0-2be2c1d7ee4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c0e591f-81b4-4996-a3b3-2f8ad5f15f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7728e814-d21b-4f22-98c0-2be2c1d7ee4b",
                    "LayerId": "18339d08-bdfd-4124-af6b-fce907c298a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "18339d08-bdfd-4124-af6b-fce907c298a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7b05132-133c-410a-b09c-dcf09994c823",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.604,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}