{
    "id": "3bc7f78a-461a-44e9-be69-c920d35b7afa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_playerBed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 15,
    "bbox_right": 106,
    "bbox_top": -7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c836478-dd0f-4990-bdf8-66e923540c6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bc7f78a-461a-44e9-be69-c920d35b7afa",
            "compositeImage": {
                "id": "1bb80e7d-0744-4cc2-906f-d40e85bc0776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c836478-dd0f-4990-bdf8-66e923540c6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de33b847-5f10-457e-9017-6504c29d1954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c836478-dd0f-4990-bdf8-66e923540c6c",
                    "LayerId": "b0228cdb-6b25-4b1c-811d-efc2c77fd437"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b0228cdb-6b25-4b1c-811d-efc2c77fd437",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bc7f78a-461a-44e9-be69-c920d35b7afa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.730872631,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}