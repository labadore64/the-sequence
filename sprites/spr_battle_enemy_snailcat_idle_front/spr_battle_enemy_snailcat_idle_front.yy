{
    "id": "678daf9d-7b82-4537-beed-0922905035b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 5,
    "bbox_right": 234,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a93ccd68-44cd-4254-9a35-39997e1d4834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "d67dd5e7-12ba-4b77-93cf-e5727b14ce48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a93ccd68-44cd-4254-9a35-39997e1d4834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90a6bfc0-624d-4690-ab1b-cbe714759630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a93ccd68-44cd-4254-9a35-39997e1d4834",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "3c35eacc-543e-4b7e-8ccc-624459acb87a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "e91b71d9-1aec-422f-8d79-c3a376e2f0e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c35eacc-543e-4b7e-8ccc-624459acb87a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc599227-2cf4-43b7-90ad-a11522852dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c35eacc-543e-4b7e-8ccc-624459acb87a",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "df16f393-4156-4004-ba90-bb84c4f9f027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "d374760c-b27a-4af9-92e7-70b3dadda67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df16f393-4156-4004-ba90-bb84c4f9f027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1d85c5-9d52-4f5d-97a4-2fcd7b31da06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df16f393-4156-4004-ba90-bb84c4f9f027",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "ff52bcbc-3749-4464-ae6e-40d476296ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "bc14e0a1-b04b-4048-a53a-0a278de9534b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff52bcbc-3749-4464-ae6e-40d476296ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edfd114f-0776-4592-9184-53e7549af904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff52bcbc-3749-4464-ae6e-40d476296ca1",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "947567b6-a29a-42be-9938-e4aa8d2a73c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "3595a15d-c27a-4f4c-9482-d90c7fba01f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "947567b6-a29a-42be-9938-e4aa8d2a73c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b3ccc4f-728b-4ed2-b818-b422377752d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "947567b6-a29a-42be-9938-e4aa8d2a73c5",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "1c0ec066-3143-4732-88eb-9c75dfaf2134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "26381cfe-9b06-4883-aa74-673f310ffcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c0ec066-3143-4732-88eb-9c75dfaf2134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "613e72ef-0afe-4007-a227-37354cfff12e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0ec066-3143-4732-88eb-9c75dfaf2134",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "b8454d50-f48a-4c82-b844-0f9819980e13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "5aea1fff-f10f-43ee-af50-e3067818d7b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8454d50-f48a-4c82-b844-0f9819980e13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc7a6b4-36d8-4efb-8602-93ec83f46fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8454d50-f48a-4c82-b844-0f9819980e13",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "f89245b6-91a5-4cbb-a64e-13d03e210be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "8e1129c8-ddce-403d-971f-f22d960751f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f89245b6-91a5-4cbb-a64e-13d03e210be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e201901-53bb-43eb-9b3c-83ea06ce2025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f89245b6-91a5-4cbb-a64e-13d03e210be8",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "07004d2b-8784-4012-bd85-8f59be4b9d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "8caad120-abb5-4109-93c2-942b0398b03f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07004d2b-8784-4012-bd85-8f59be4b9d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38631440-cc21-4c09-9313-7efedea75b70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07004d2b-8784-4012-bd85-8f59be4b9d19",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "51252d35-92a8-4dc3-93d1-c3011ad0b8fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "04606258-2add-4314-83ed-b688367a9541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51252d35-92a8-4dc3-93d1-c3011ad0b8fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ef3c44-7343-49a3-8db3-cd8e807084ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51252d35-92a8-4dc3-93d1-c3011ad0b8fe",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "a51c5146-dd97-479d-9be0-63649e7acc39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "7f1751cb-544a-40a6-bd8a-9e7321e49042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51c5146-dd97-479d-9be0-63649e7acc39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e25d02a-f817-4367-8ed0-b2cd5489f81b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51c5146-dd97-479d-9be0-63649e7acc39",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "1f3844a9-a7dc-4d5f-9136-759170fd4ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "918656e8-80a5-4db6-8630-981d2e2e9612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3844a9-a7dc-4d5f-9136-759170fd4ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcdd9c27-7851-4cd1-8c6c-e3ca9915be0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3844a9-a7dc-4d5f-9136-759170fd4ecd",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "ef6ad6f7-1351-4333-98bd-1cac7e3b1624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "c5bcbdf5-90a1-43a1-be89-a290eb76aa4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6ad6f7-1351-4333-98bd-1cac7e3b1624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd1d4fa-397c-4425-b97b-e012bdd7e933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6ad6f7-1351-4333-98bd-1cac7e3b1624",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "c83636a4-1be8-43cd-9d91-93b63a621a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "4fd4527b-452e-4f6b-88cd-35f75fc8e7de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c83636a4-1be8-43cd-9d91-93b63a621a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b2a60d3-4ccd-40e8-9324-fee37f256003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c83636a4-1be8-43cd-9d91-93b63a621a3f",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "d96d7c4d-94f1-43bf-81b1-1a510d2f2678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "baac2c45-e2ef-4dcb-be17-ec8896ae1ac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96d7c4d-94f1-43bf-81b1-1a510d2f2678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f19ff904-8516-4589-abe4-cee9a61540e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96d7c4d-94f1-43bf-81b1-1a510d2f2678",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "7ebfdb3b-a0b9-4967-a273-930786e8c967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "06f11430-961b-49b6-95c9-d30ff4d38c9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ebfdb3b-a0b9-4967-a273-930786e8c967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586a83d2-5117-4618-8fc7-2199e25c59fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ebfdb3b-a0b9-4967-a273-930786e8c967",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "cc0765fa-9121-4fce-a56c-046bd5655cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "aa1bbfdb-9fcb-471c-a124-cf14acc6ca44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0765fa-9121-4fce-a56c-046bd5655cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af39548-cc1b-4ead-bb2e-f159da93648d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0765fa-9121-4fce-a56c-046bd5655cc1",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "4d423be6-a76f-4835-983f-8fe64e806356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "feeb5a32-f1d4-4c00-b27b-e9703e8d7d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d423be6-a76f-4835-983f-8fe64e806356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361a07e5-a0f5-4ff6-a312-e81e2656c99e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d423be6-a76f-4835-983f-8fe64e806356",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "45dcf691-5269-42bc-a251-e81c1245b5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "da5d94e6-ad29-427c-a8d6-ad428e295f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45dcf691-5269-42bc-a251-e81c1245b5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b9196e-e6cc-4d6f-91aa-84c2e930fc16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45dcf691-5269-42bc-a251-e81c1245b5be",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "50d7c206-ad9e-4526-b157-d285736eb03c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "8de04501-820b-416d-88a4-3580f49ab007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d7c206-ad9e-4526-b157-d285736eb03c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d899a3df-0b04-485c-8c7b-3da5c96c00c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d7c206-ad9e-4526-b157-d285736eb03c",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "a87c61e5-e189-4e60-a9e8-1838be299e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "91f8172c-72cc-4b9e-865f-ebb44739aec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a87c61e5-e189-4e60-a9e8-1838be299e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc6f6f7-6e52-4c2d-93f4-42239daa3fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a87c61e5-e189-4e60-a9e8-1838be299e61",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "197dd9be-1ac9-4a21-aa08-b5dbeaaa34c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "c168f00e-6e63-44a3-92fc-71cec9a0c64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "197dd9be-1ac9-4a21-aa08-b5dbeaaa34c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d97570d3-26a3-4d3c-b5ba-4ac65deec691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "197dd9be-1ac9-4a21-aa08-b5dbeaaa34c5",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "06dd7b1e-7382-49b5-9748-2d4add061b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "1d518630-9a9b-40ff-ba05-a3d45f29183a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06dd7b1e-7382-49b5-9748-2d4add061b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ed216b-bd01-449a-83e5-f6a3cf4a394a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06dd7b1e-7382-49b5-9748-2d4add061b05",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "06d25a5c-b8a1-469a-9026-0444cbd1e86f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "0ab7dd65-0ba4-418e-a671-dd98e5ca8eff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d25a5c-b8a1-469a-9026-0444cbd1e86f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df31683b-9041-4c4d-85b3-d3d3894f0eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d25a5c-b8a1-469a-9026-0444cbd1e86f",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "5f67f52e-32ac-4e2e-883b-585def19376b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "89c45bbc-10e9-4f6b-ab83-9d6bd0cb0442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f67f52e-32ac-4e2e-883b-585def19376b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f56745f3-c667-4a6a-95a5-2120c40e4e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f67f52e-32ac-4e2e-883b-585def19376b",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "7c1839e7-de49-4af0-92bf-ad01cf336229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "e79294e0-8dc4-41eb-b706-b3a18e247333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c1839e7-de49-4af0-92bf-ad01cf336229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f162fd-dbbd-4066-b3cf-8e578201cb3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c1839e7-de49-4af0-92bf-ad01cf336229",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "d449eb00-1c5e-42d7-a4a8-8195e97ca87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "44502724-7301-45fb-97b5-2d1279ae639c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d449eb00-1c5e-42d7-a4a8-8195e97ca87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831dfc0e-aaec-4f5e-9cc1-fe62431b15cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d449eb00-1c5e-42d7-a4a8-8195e97ca87f",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "9dc7e03d-396e-4e63-b910-224c2dab4190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "e60b8191-075f-4127-8e36-c0ab5f448d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc7e03d-396e-4e63-b910-224c2dab4190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51de81e6-be14-426d-84bf-1f5970fb420e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc7e03d-396e-4e63-b910-224c2dab4190",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "9fc82db0-5adb-41c2-9ce6-70a206458db4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "e4ffbedc-f694-44e1-a0d7-97b61bcfe5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc82db0-5adb-41c2-9ce6-70a206458db4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2096c0-c0ba-46e1-b1da-0c0e1de9adf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc82db0-5adb-41c2-9ce6-70a206458db4",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "7828d845-1811-4554-8a95-c4368af494d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "545c63ad-4b2e-4796-a74d-1eb11baa9066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7828d845-1811-4554-8a95-c4368af494d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbbe8cfb-71ea-4dcb-b8b6-784b6b705482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7828d845-1811-4554-8a95-c4368af494d7",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "47589020-664e-4371-862d-c74f827873aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "4b7090be-3577-4383-af8a-b28e6d299bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47589020-664e-4371-862d-c74f827873aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f345cd74-0ba1-42c2-9a4b-7785c8faabb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47589020-664e-4371-862d-c74f827873aa",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "4d9437f6-3618-4649-8ad6-57cf6243ab36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "1789469d-99dd-4f5b-ba56-6bc23b501715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d9437f6-3618-4649-8ad6-57cf6243ab36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ae40b4-6fd4-4446-9a8f-d8fad41d8a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d9437f6-3618-4649-8ad6-57cf6243ab36",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "b226ed54-541d-455b-89c3-e1f8f010fa8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "4f053d9f-6df3-4f19-855d-99d352a9caf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b226ed54-541d-455b-89c3-e1f8f010fa8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88721e43-45af-4b80-9911-1c4b81863018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b226ed54-541d-455b-89c3-e1f8f010fa8c",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "7fa98380-e333-4477-86f6-b321753f8bfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "edf912af-1065-42d5-ba20-3eabd685c029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa98380-e333-4477-86f6-b321753f8bfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5817f3e4-9390-40ea-815b-537165d2b6bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa98380-e333-4477-86f6-b321753f8bfc",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "f39ae948-047c-4f9f-b44c-1a75d95e804f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "d4071ca1-7bcf-4b4e-be46-02100c1ef9c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39ae948-047c-4f9f-b44c-1a75d95e804f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d984766-616c-4af5-b2dc-eccd1e2a0b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39ae948-047c-4f9f-b44c-1a75d95e804f",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "65af0f2f-a267-45b7-895c-19863a295327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "4f57357c-8069-4cb6-92ae-d09c2c9f3987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65af0f2f-a267-45b7-895c-19863a295327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a9a1c76-3039-491b-93ee-d5b1873a7f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65af0f2f-a267-45b7-895c-19863a295327",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "9571d6fa-74fe-4223-b5ef-1e78958445a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "aa8ed51c-2681-4f70-8ca5-45e3498df175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9571d6fa-74fe-4223-b5ef-1e78958445a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31137066-e5fb-4daa-87d6-98283fc5ae34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9571d6fa-74fe-4223-b5ef-1e78958445a0",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "24cdffa3-a88c-439e-81be-92f702e7d67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "e7262668-d6c6-4161-a6f2-62f0d4776536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cdffa3-a88c-439e-81be-92f702e7d67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c8d7f5-2690-4a2f-92d3-180c8485c8ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cdffa3-a88c-439e-81be-92f702e7d67f",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "a9b6c45a-5bdb-4d4a-8636-73e207f505ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "fe80ba55-88fe-41fa-9f82-dceb89b7aab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9b6c45a-5bdb-4d4a-8636-73e207f505ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03911afe-22b5-4730-a989-da781fad40e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9b6c45a-5bdb-4d4a-8636-73e207f505ae",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "8a962fdd-b584-4796-8eca-c616703b4361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "695dad55-e3d4-49f5-b8df-00c9854362d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a962fdd-b584-4796-8eca-c616703b4361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141e6d09-6ccc-4efc-80e3-bb8b233dd198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a962fdd-b584-4796-8eca-c616703b4361",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "05344e5f-1a88-44af-8a39-1fe97b4567da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "cacaaf25-cc00-48db-9192-35779d001fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05344e5f-1a88-44af-8a39-1fe97b4567da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328bae44-9e48-4fff-9bbb-be843142fba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05344e5f-1a88-44af-8a39-1fe97b4567da",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "ac4446d7-a907-497c-a6cd-46e8661fde1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "e48aac0b-f499-453a-9f38-a0fe1c6c9e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4446d7-a907-497c-a6cd-46e8661fde1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "807336b1-bad9-4700-956e-96af48e58019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4446d7-a907-497c-a6cd-46e8661fde1b",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "85e4d4dd-d27d-4109-b297-e0201fdd3277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "dfcccb8a-8a7f-4cde-aecb-e9f4572abdee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e4d4dd-d27d-4109-b297-e0201fdd3277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e5f4de4-275c-4a69-aeab-f39a6396d5b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e4d4dd-d27d-4109-b297-e0201fdd3277",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "3060bb4d-f0db-4775-90e6-fc2a56ea0a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "58088f0a-47dd-4962-be75-29f94184906c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3060bb4d-f0db-4775-90e6-fc2a56ea0a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f877fc3-1217-4273-85fc-feb88fa2198d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3060bb4d-f0db-4775-90e6-fc2a56ea0a5f",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        },
        {
            "id": "396d466d-2b23-4f13-8ded-e782309bbbf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "compositeImage": {
                "id": "7976b1cb-e3da-498b-abf5-399603732e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "396d466d-2b23-4f13-8ded-e782309bbbf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721e37e0-94f1-4b38-99e8-953209325eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396d466d-2b23-4f13-8ded-e782309bbbf4",
                    "LayerId": "8c296d76-7679-475b-8177-4fee840e008f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8c296d76-7679-475b-8177-4fee840e008f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "678daf9d-7b82-4537-beed-0922905035b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}