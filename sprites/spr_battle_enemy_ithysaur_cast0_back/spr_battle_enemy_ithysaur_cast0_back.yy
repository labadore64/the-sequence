{
    "id": "f47fc49a-7e18-44c4-9620-1cb1bb569d13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_cast0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 14,
    "bbox_right": 207,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f58abb6-4240-483e-9d70-7f28e30f071c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f47fc49a-7e18-44c4-9620-1cb1bb569d13",
            "compositeImage": {
                "id": "a1bf9460-64cf-4b15-a784-f970b2bdf897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f58abb6-4240-483e-9d70-7f28e30f071c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa68cc4c-fda0-49b0-b3ed-552e9d4ee8da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f58abb6-4240-483e-9d70-7f28e30f071c",
                    "LayerId": "4f1f88b1-20d3-4c67-926e-9833bff9d751"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "4f1f88b1-20d3-4c67-926e-9833bff9d751",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f47fc49a-7e18-44c4-9620-1cb1bb569d13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}