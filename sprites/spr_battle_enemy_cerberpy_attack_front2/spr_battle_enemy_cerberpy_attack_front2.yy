{
    "id": "2cac6e22-8e95-4e27-ae85-5ff80c995de5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_cerberpy_attack_front2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 27,
    "bbox_right": 209,
    "bbox_top": 109,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d878dc6-2c74-4256-acf4-1431425d9e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cac6e22-8e95-4e27-ae85-5ff80c995de5",
            "compositeImage": {
                "id": "c75c092d-86fc-4b14-8c74-91cd508c7a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d878dc6-2c74-4256-acf4-1431425d9e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4058de14-4207-414c-88a6-efc2c896f617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d878dc6-2c74-4256-acf4-1431425d9e14",
                    "LayerId": "4bc631db-9c27-4246-9f19-04c485028a5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "4bc631db-9c27-4246-9f19-04c485028a5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cac6e22-8e95-4e27-ae85-5ff80c995de5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}