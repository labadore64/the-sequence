{
    "id": "ca521c32-5848-4ff7-87f9-dd823bc44496",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_treeSwing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 2,
    "bbox_right": 126,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a55e3926-92c8-472e-944c-94c7fd7fdd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca521c32-5848-4ff7-87f9-dd823bc44496",
            "compositeImage": {
                "id": "dcc504ae-df9c-4be3-b249-6dbc13b0490a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55e3926-92c8-472e-944c-94c7fd7fdd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23ce9ef1-ace8-4d38-aa48-c60eccd5a934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55e3926-92c8-472e-944c-94c7fd7fdd5a",
                    "LayerId": "56bdbac4-dff8-4069-9006-2f0c0c44e233"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "56bdbac4-dff8-4069-9006-2f0c0c44e233",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca521c32-5848-4ff7-87f9-dd823bc44496",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 1.21325016,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}