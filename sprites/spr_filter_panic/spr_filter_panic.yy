{
    "id": "b20d512a-371a-4472-b8ce-3713303ccb7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_panic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 53,
    "bbox_right": 202,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7c643fb-a7e4-42b1-8773-0d6ec7ea70b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b20d512a-371a-4472-b8ce-3713303ccb7c",
            "compositeImage": {
                "id": "60af098f-815d-47f8-a20b-6626e08028de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c643fb-a7e4-42b1-8773-0d6ec7ea70b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634fd0b8-cd3b-41df-a166-241e72685e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c643fb-a7e4-42b1-8773-0d6ec7ea70b1",
                    "LayerId": "e286b83e-b012-4622-96f8-fccb0d8c64b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e286b83e-b012-4622-96f8-fccb0d8c64b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b20d512a-371a-4472-b8ce-3713303ccb7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 145
}