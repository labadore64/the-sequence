{
    "id": "6207a491-b198-416c-ac9e-8191d6489e61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_birdguide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 11,
    "bbox_right": 248,
    "bbox_top": 47,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7f2a94c-3d67-444b-8216-fa66efb2928a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6207a491-b198-416c-ac9e-8191d6489e61",
            "compositeImage": {
                "id": "e28fd5e1-f85b-44fe-9e58-3ad266471b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7f2a94c-3d67-444b-8216-fa66efb2928a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e44499-10b4-4d77-8d27-f9f056e964eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f2a94c-3d67-444b-8216-fa66efb2928a",
                    "LayerId": "5f4f189b-c28c-4984-b00c-812fe27c35d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5f4f189b-c28c-4984-b00c-812fe27c35d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6207a491-b198-416c-ac9e-8191d6489e61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.693500042,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}