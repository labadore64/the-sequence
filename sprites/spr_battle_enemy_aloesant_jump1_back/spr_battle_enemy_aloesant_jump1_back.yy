{
    "id": "6fa940d6-44a8-429c-9618-e8d8aabfe9c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 43,
    "bbox_right": 225,
    "bbox_top": 77,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "388ae621-70c6-4b6d-90dd-ddc9261c2a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa940d6-44a8-429c-9618-e8d8aabfe9c9",
            "compositeImage": {
                "id": "49194f86-d0e1-4f61-8bcc-58f3b4dfec3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388ae621-70c6-4b6d-90dd-ddc9261c2a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67a74dae-2e81-42fa-93d4-5081945df388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388ae621-70c6-4b6d-90dd-ddc9261c2a8f",
                    "LayerId": "dd4df647-8f10-413c-8528-fa4802bdec11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "dd4df647-8f10-413c-8528-fa4802bdec11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fa940d6-44a8-429c-9618-e8d8aabfe9c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}