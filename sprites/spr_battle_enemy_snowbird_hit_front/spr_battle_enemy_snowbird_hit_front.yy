{
    "id": "660800b2-883a-4708-bcba-400fd0834f41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snowbird_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 43,
    "bbox_right": 183,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bf6ec9e-eba0-440c-9363-26c6b9291d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "660800b2-883a-4708-bcba-400fd0834f41",
            "compositeImage": {
                "id": "ee0d627f-726a-4d47-a7c7-36d68a95bee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bf6ec9e-eba0-440c-9363-26c6b9291d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a20ca2-7704-43a7-b631-7e9d25e8866c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bf6ec9e-eba0-440c-9363-26c6b9291d84",
                    "LayerId": "67ac9ad0-5cf5-4486-ad0a-25d3b0ffab47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "67ac9ad0-5cf5-4486-ad0a-25d3b0ffab47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "660800b2-883a-4708-bcba-400fd0834f41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d41a043f-5316-4f92-8dc3-11dc8826a28b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}