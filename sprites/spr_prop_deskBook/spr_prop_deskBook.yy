{
    "id": "0c1daab6-da6f-4f68-8efa-1ae106bed06b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_deskBook",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 8,
    "bbox_right": 115,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af8c05a4-1277-4502-b877-35af1dae40ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c1daab6-da6f-4f68-8efa-1ae106bed06b",
            "compositeImage": {
                "id": "4cc6e514-a44c-42fd-ab6a-176098785abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8c05a4-1277-4502-b877-35af1dae40ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "564cb546-afcd-45ab-b625-963d23dece5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8c05a4-1277-4502-b877-35af1dae40ae",
                    "LayerId": "ef1ca11c-c4b7-4bbd-a7cb-bebf368449b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ef1ca11c-c4b7-4bbd-a7cb-bebf368449b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c1daab6-da6f-4f68-8efa-1ae106bed06b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}