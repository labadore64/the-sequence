{
    "id": "3e71716f-8a92-4554-ad1e-09297f9b600c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign_beak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 2,
    "bbox_right": 122,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9c18705-7eb4-4200-9c0a-de4aa9b915a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e71716f-8a92-4554-ad1e-09297f9b600c",
            "compositeImage": {
                "id": "fae0b68e-e5df-4bdc-8f4f-1a1001213bb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c18705-7eb4-4200-9c0a-de4aa9b915a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8790633-632e-4f6d-aa06-716bed831e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c18705-7eb4-4200-9c0a-de4aa9b915a5",
                    "LayerId": "75cac265-422a-4a77-ae6b-610cd7042925"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "75cac265-422a-4a77-ae6b-610cd7042925",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e71716f-8a92-4554-ad1e-09297f9b600c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}