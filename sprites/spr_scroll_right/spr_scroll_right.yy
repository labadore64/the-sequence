{
    "id": "a38b528e-c0c8-4d20-b366-577131090ef7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_scroll_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 15,
    "bbox_right": 81,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c464197-e8a0-410a-a636-6cdb25cd19f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a38b528e-c0c8-4d20-b366-577131090ef7",
            "compositeImage": {
                "id": "cd84171b-914e-4bd4-af59-3b9dbcee3c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c464197-e8a0-410a-a636-6cdb25cd19f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ba5bab-9de0-45cf-91ba-6595a519d9d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c464197-e8a0-410a-a636-6cdb25cd19f8",
                    "LayerId": "aba6c2a9-828a-400e-9163-b2fa2b4bb9da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aba6c2a9-828a-400e-9163-b2fa2b4bb9da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a38b528e-c0c8-4d20-b366-577131090ef7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.4555,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}