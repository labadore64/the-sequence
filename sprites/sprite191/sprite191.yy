{
    "id": "4ed7965c-678d-4847-87e0-8e4edc504b92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite191",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d42d2770-2dc0-456f-8087-910d5d1f3af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ed7965c-678d-4847-87e0-8e4edc504b92",
            "compositeImage": {
                "id": "eacbd147-a464-4b13-873a-dc2bd72879db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d42d2770-2dc0-456f-8087-910d5d1f3af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4592dd43-7ee1-4b1e-bad0-5ba8a9851d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d42d2770-2dc0-456f-8087-910d5d1f3af7",
                    "LayerId": "bf1279e8-1393-4557-8035-29245cff7650"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bf1279e8-1393-4557-8035-29245cff7650",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ed7965c-678d-4847-87e0-8e4edc504b92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 33,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}