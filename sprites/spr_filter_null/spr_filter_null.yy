{
    "id": "31bf3649-4f55-4844-8b5f-b3916923ab65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_null",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 182,
    "bbox_left": 55,
    "bbox_right": 209,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a67293e-3946-47ca-8e60-5cd2f9a3bbe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31bf3649-4f55-4844-8b5f-b3916923ab65",
            "compositeImage": {
                "id": "a52f62cf-2aca-4b4e-8f12-b2b47f0a83d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a67293e-3946-47ca-8e60-5cd2f9a3bbe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "140c403a-1c91-49a5-8623-8dc3ea3fe5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a67293e-3946-47ca-8e60-5cd2f9a3bbe8",
                    "LayerId": "d218f67e-c09d-451e-9784-bc7003417e08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d218f67e-c09d-451e-9784-bc7003417e08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31bf3649-4f55-4844-8b5f-b3916923ab65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}