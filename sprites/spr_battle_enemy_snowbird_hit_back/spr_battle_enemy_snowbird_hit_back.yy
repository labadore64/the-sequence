{
    "id": "5ee9a985-6c32-4dfd-96ee-44b4ca1a37ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snowbird_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 14,
    "bbox_right": 192,
    "bbox_top": 114,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9bdc308-6583-49f7-bd6a-50c726c4c5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ee9a985-6c32-4dfd-96ee-44b4ca1a37ab",
            "compositeImage": {
                "id": "11ec23c5-e353-49c8-93e9-ec37f961145b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9bdc308-6583-49f7-bd6a-50c726c4c5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "296ab8d6-fa8b-43c4-b260-a819a706199e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bdc308-6583-49f7-bd6a-50c726c4c5db",
                    "LayerId": "a31c722f-cd45-449a-ae05-db8362fba2c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a31c722f-cd45-449a-ae05-db8362fba2c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ee9a985-6c32-4dfd-96ee-44b4ca1a37ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d41a043f-5316-4f92-8dc3-11dc8826a28b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}