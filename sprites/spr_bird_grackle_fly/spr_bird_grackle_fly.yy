{
    "id": "6703c0cc-2374-4e34-be9c-9706c6439dab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_grackle_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 19,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ed66894-e77d-458c-9fc6-5b69064d502f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6703c0cc-2374-4e34-be9c-9706c6439dab",
            "compositeImage": {
                "id": "4523c3b8-3bbe-41e4-8a23-32b1047bb378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ed66894-e77d-458c-9fc6-5b69064d502f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "022fdfda-0f93-41fc-a506-8220860e5bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ed66894-e77d-458c-9fc6-5b69064d502f",
                    "LayerId": "148c181f-b15c-4d4a-93d2-003058c9250b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "148c181f-b15c-4d4a-93d2-003058c9250b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6703c0cc-2374-4e34-be9c-9706c6439dab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}