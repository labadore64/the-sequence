{
    "id": "208d82c1-6c1c-44b8-8b3c-1d63d6e81f8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 243,
    "bbox_left": 35,
    "bbox_right": 194,
    "bbox_top": 78,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43532c96-7601-49e5-9f1a-49d47ba1b467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "208d82c1-6c1c-44b8-8b3c-1d63d6e81f8e",
            "compositeImage": {
                "id": "11bd027b-4a77-4c8d-b626-877173e10371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43532c96-7601-49e5-9f1a-49d47ba1b467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7fcc22-4173-4d69-a4c4-8c990e8656e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43532c96-7601-49e5-9f1a-49d47ba1b467",
                    "LayerId": "b91e780d-73b7-40cc-9f7d-5b373ac6a95f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b91e780d-73b7-40cc-9f7d-5b373ac6a95f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "208d82c1-6c1c-44b8-8b3c-1d63d6e81f8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}