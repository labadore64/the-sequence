{
    "id": "cad52a44-33e1-4bf3-8e72-222c36ca143b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_self_meditate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4e482fc-3882-493b-b907-a8c352889daa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cad52a44-33e1-4bf3-8e72-222c36ca143b",
            "compositeImage": {
                "id": "daf6dfbe-277c-4aee-839b-f9b76e19705c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e482fc-3882-493b-b907-a8c352889daa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2548ad-73f9-46ef-ae04-2fc0e7a4eb50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e482fc-3882-493b-b907-a8c352889daa",
                    "LayerId": "a39acaf8-bace-44c1-9e32-e2471c29ee4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a39acaf8-bace-44c1-9e32-e2471c29ee4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cad52a44-33e1-4bf3-8e72-222c36ca143b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.470750332,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}