{
    "id": "2d8e32d5-4230-432e-8e5b-103b8eb255e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_water_shine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 19,
    "bbox_right": 113,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "430c2057-1bb7-42e7-b0c3-9208c30bf512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d8e32d5-4230-432e-8e5b-103b8eb255e6",
            "compositeImage": {
                "id": "9ef0c4a2-5e01-4d74-b84f-e1bec9e3df42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430c2057-1bb7-42e7-b0c3-9208c30bf512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02230021-53b6-4170-91cc-4ce9d0b8b62f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430c2057-1bb7-42e7-b0c3-9208c30bf512",
                    "LayerId": "cbc19385-8303-47aa-ad8f-be0586ea6b29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cbc19385-8303-47aa-ad8f-be0586ea6b29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d8e32d5-4230-432e-8e5b-103b8eb255e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.1585,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}