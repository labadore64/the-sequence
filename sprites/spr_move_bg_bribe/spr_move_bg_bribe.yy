{
    "id": "38695ed8-a861-4af4-a828-0a883b0af7e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_bribe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "609ef0bf-370b-4b60-a162-0cc1f83639e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38695ed8-a861-4af4-a828-0a883b0af7e6",
            "compositeImage": {
                "id": "e6d1d952-977d-44a8-a99b-a4a5d4b7d75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609ef0bf-370b-4b60-a162-0cc1f83639e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febe30d9-615f-440d-bec2-c6a9db1a2a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609ef0bf-370b-4b60-a162-0cc1f83639e4",
                    "LayerId": "e9937e80-b973-4c50-9b8d-9c8702fabbc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "e9937e80-b973-4c50-9b8d-9c8702fabbc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38695ed8-a861-4af4-a828-0a883b0af7e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}