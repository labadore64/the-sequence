{
    "id": "574fb61c-25ae-406b-aa0d-7c637ffe1e8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_aquariumFrog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 4,
    "bbox_right": 123,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "973452e0-1157-4e29-b3dd-29bc5a776110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "574fb61c-25ae-406b-aa0d-7c637ffe1e8c",
            "compositeImage": {
                "id": "d9948920-0b69-4514-a647-b280febc2fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973452e0-1157-4e29-b3dd-29bc5a776110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f81b49f2-cf8f-4b3d-9c25-7822c5a4f6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973452e0-1157-4e29-b3dd-29bc5a776110",
                    "LayerId": "7222fb97-64a0-494c-97e6-fe3c432c598c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7222fb97-64a0-494c-97e6-fe3c432c598c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "574fb61c-25ae-406b-aa0d-7c637ffe1e8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}