{
    "id": "218e20ca-39c6-4fe9-b621-8b57bceaf4c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 77,
    "bbox_right": 183,
    "bbox_top": 107,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "822b4530-0c90-4dcf-b1a9-607c90c5df06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "218e20ca-39c6-4fe9-b621-8b57bceaf4c1",
            "compositeImage": {
                "id": "42656fc7-ebca-488a-a367-20cd7db68df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822b4530-0c90-4dcf-b1a9-607c90c5df06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d480138e-0f6f-4ddf-8812-731d670196c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822b4530-0c90-4dcf-b1a9-607c90c5df06",
                    "LayerId": "380a1a79-a024-432c-8392-bea6c065fde0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "380a1a79-a024-432c-8392-bea6c065fde0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "218e20ca-39c6-4fe9-b621-8b57bceaf4c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}