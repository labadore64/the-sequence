{
    "id": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_magzie_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 23,
    "bbox_right": 186,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1314ab79-0f74-433b-946d-e706a02a49ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "f0d88445-fce9-4397-a508-457b0f50a720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1314ab79-0f74-433b-946d-e706a02a49ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74fd9fa-1576-4dfd-ae4a-cbdb13903b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1314ab79-0f74-433b-946d-e706a02a49ed",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "100676ab-ab7b-4884-98f9-b7c5592f3de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "1ea35a66-82d3-4cfc-8eeb-641fe88c1a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "100676ab-ab7b-4884-98f9-b7c5592f3de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88cfdb97-7bbc-437f-b888-2a5b925d75d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "100676ab-ab7b-4884-98f9-b7c5592f3de3",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "ed56bb0f-e9a4-4d05-ba65-611edec7fb94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "821ce6cc-07e6-41cc-8fdd-3c5beed0dd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed56bb0f-e9a4-4d05-ba65-611edec7fb94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8207899-2397-451f-aa03-b03c077fb67d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed56bb0f-e9a4-4d05-ba65-611edec7fb94",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "3a5dc329-be5a-41fd-8877-d4d967667dda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "23a121f4-eec6-4a2e-add2-94a7d8380087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a5dc329-be5a-41fd-8877-d4d967667dda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f323ce0a-85fc-4ee6-b8ed-3df7344ab650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a5dc329-be5a-41fd-8877-d4d967667dda",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "2eaf1963-bf6d-484b-970c-48692c6cf7ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "6a37500f-de6c-4a93-baf9-55874bd5dbd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eaf1963-bf6d-484b-970c-48692c6cf7ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33ead6d-5a02-419b-85ce-6e0f5e50cf15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eaf1963-bf6d-484b-970c-48692c6cf7ec",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "e48fc8e2-f2d8-48cb-8b06-e8efb69b0a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "f898a9e0-efc5-44b3-9c1d-e3b09a24ffac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e48fc8e2-f2d8-48cb-8b06-e8efb69b0a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d94c739-d9cb-4ec2-a54b-bd3540927dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e48fc8e2-f2d8-48cb-8b06-e8efb69b0a35",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "0044a844-79bc-40cb-b5e3-cc8af84e73c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "ac85bb41-791c-47d1-9258-aa65f8479817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0044a844-79bc-40cb-b5e3-cc8af84e73c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8710b5a5-1fda-4849-9b63-107a4c45cd86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0044a844-79bc-40cb-b5e3-cc8af84e73c0",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "875d8f52-7216-46cf-ba3d-93e476e30b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "ca9c51d6-b31b-4e16-a971-187ae07b1f13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875d8f52-7216-46cf-ba3d-93e476e30b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7998aa28-9747-42f1-9d6e-1e30039582c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875d8f52-7216-46cf-ba3d-93e476e30b05",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "b678fd16-711e-46a6-97fb-a46e59e4df4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "36035e57-0d4a-4b86-9b23-bac9d0fa5f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b678fd16-711e-46a6-97fb-a46e59e4df4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff700f70-d41d-469b-acb9-0683c40b85ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b678fd16-711e-46a6-97fb-a46e59e4df4e",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "d0d3f899-c022-4afe-b2a3-fc6a8bc363fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "974888fa-cd33-462e-a44f-4694df69ae14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d3f899-c022-4afe-b2a3-fc6a8bc363fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73597fa5-28c5-4ad5-883f-021ac03c46c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d3f899-c022-4afe-b2a3-fc6a8bc363fc",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "c0ae2253-f190-4982-a582-9fbd70344369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "f9763c2a-684c-4c17-a94f-aa4b0ad68990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ae2253-f190-4982-a582-9fbd70344369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae82895-7167-4374-97e4-a3cb53644548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ae2253-f190-4982-a582-9fbd70344369",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "1a3376ca-2912-4940-a8a6-a1536e38a957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "af7370e6-c4ba-4f30-9e67-69ff5b84e3c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3376ca-2912-4940-a8a6-a1536e38a957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac82ece7-31d4-4b6d-9715-886e2d91d772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3376ca-2912-4940-a8a6-a1536e38a957",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "8e902344-0e24-48fa-9212-54c054747fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "3b0c8bec-07d5-477f-ad75-dbcb5d004343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e902344-0e24-48fa-9212-54c054747fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff14299-db17-43b8-9ed0-7ef26a5a4e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e902344-0e24-48fa-9212-54c054747fea",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "e57dd7b3-fbb1-48a4-be35-9d79b8e2d64a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "5d096a50-56b0-42ba-9737-a1696677fd3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e57dd7b3-fbb1-48a4-be35-9d79b8e2d64a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c64c645-67d7-4311-aea3-dbb45470e2b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e57dd7b3-fbb1-48a4-be35-9d79b8e2d64a",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "aef2adc4-8c0c-4357-9ea0-43c1c18d849e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "34368585-c445-48bb-a5ab-715bdd06f668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef2adc4-8c0c-4357-9ea0-43c1c18d849e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44ad81dc-0836-4362-bacf-173dc4aeb32e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef2adc4-8c0c-4357-9ea0-43c1c18d849e",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "eed22098-9f4e-415f-a098-d0c7bcd34bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "f7fb5efc-44bb-4a3c-a8dc-8306df79dafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed22098-9f4e-415f-a098-d0c7bcd34bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77bbaec-0251-48be-a00d-dd7e7fec7e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed22098-9f4e-415f-a098-d0c7bcd34bc5",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "676d68c4-481b-4ae5-8c95-9e98e3a1caef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "1af2a57d-8190-4510-8536-ccd417cacac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676d68c4-481b-4ae5-8c95-9e98e3a1caef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312b8fff-b55d-4768-8923-cc3efa022f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676d68c4-481b-4ae5-8c95-9e98e3a1caef",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "506f3813-88da-4767-83b1-97c5bfe20593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "169d62f1-2be9-4137-a6d9-269db3e7721f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "506f3813-88da-4767-83b1-97c5bfe20593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8ebe96-defd-4309-a9aa-c5cde174c67d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "506f3813-88da-4767-83b1-97c5bfe20593",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "81e346e2-badc-4db0-af89-026acbf552d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "5c5718c6-7090-41d3-b50d-6b88225417cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e346e2-badc-4db0-af89-026acbf552d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64612c2f-7cd8-4907-bbd4-28fba28d2526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e346e2-badc-4db0-af89-026acbf552d2",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        },
        {
            "id": "b4305239-8fc8-42c2-a2ac-d2a6d36c23d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "compositeImage": {
                "id": "c5a034e8-dab0-46a1-a56d-3205979fa8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4305239-8fc8-42c2-a2ac-d2a6d36c23d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311f2aae-5707-42d8-8005-1803057f976a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4305239-8fc8-42c2-a2ac-d2a6d36c23d2",
                    "LayerId": "0c23b853-912f-490a-abee-10d3af3e9249"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0c23b853-912f-490a-abee-10d3af3e9249",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf49b07-2775-4fae-9fae-69bee42baf3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "c521fe91-d359-41ad-a304-750ae173f15c",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}