{
    "id": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_crookadoo_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 11,
    "bbox_right": 255,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "606ec552-e74d-4871-ae68-22c5444982c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "5639177b-1776-4a03-a68b-38ede20380c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606ec552-e74d-4871-ae68-22c5444982c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f73496-27b0-4632-81bc-a5cd7bc32fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606ec552-e74d-4871-ae68-22c5444982c5",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "3b2ebb8a-93d8-49f2-ba6c-e28d7c024bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "fac22b15-a6a9-4da9-ac78-74d79afd1951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2ebb8a-93d8-49f2-ba6c-e28d7c024bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a275b046-13a3-4bef-9adb-59db7464f818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2ebb8a-93d8-49f2-ba6c-e28d7c024bf1",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "bb862a05-d9a4-4881-b8cf-782b1c707c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "efc88904-9b10-4a44-a603-02dd9938fbf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb862a05-d9a4-4881-b8cf-782b1c707c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3626ff08-2157-40e2-b5b8-2cbb3dbee7ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb862a05-d9a4-4881-b8cf-782b1c707c70",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "bdbf4369-dd03-4a13-8f4b-417c053d5d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "0be2125d-6e8a-451d-b6c4-956cee8c3592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdbf4369-dd03-4a13-8f4b-417c053d5d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a624d83-e4b9-4f55-a57b-05a98a02c206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbf4369-dd03-4a13-8f4b-417c053d5d53",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "634d72c9-336f-429a-a1ad-731ee8d2d573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "41daa084-d12a-4c41-8535-1addada2ca36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "634d72c9-336f-429a-a1ad-731ee8d2d573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0b784c-26a4-4235-a932-53519213f885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "634d72c9-336f-429a-a1ad-731ee8d2d573",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "02d9ae14-242e-410a-b4cd-e6b42f54e82b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "c6921861-c18b-4a30-ab71-78c52e8a2c06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d9ae14-242e-410a-b4cd-e6b42f54e82b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "995956e6-330f-4309-94a8-bbabd086f5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d9ae14-242e-410a-b4cd-e6b42f54e82b",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "99f629e3-ea29-4f66-8899-16575ba8fb82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "9be75d43-eaf7-47f1-855d-652badfdc7ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f629e3-ea29-4f66-8899-16575ba8fb82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "416241e7-51c0-4a55-ac53-a7b5a5fb12c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f629e3-ea29-4f66-8899-16575ba8fb82",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "5872fa3b-8214-45d5-983e-6fe788560b56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "fa14387b-757a-4e1b-8856-53d6c5893fbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5872fa3b-8214-45d5-983e-6fe788560b56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52e1ddd-1fd3-47d3-ab67-dec2751a6d57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5872fa3b-8214-45d5-983e-6fe788560b56",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "98268b8e-e611-4e07-8eaa-1bf52ee39445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "0ca7ffad-68c1-4cd0-b37a-62c220afee51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98268b8e-e611-4e07-8eaa-1bf52ee39445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157e1a2a-a1dc-4650-9381-31f69167c679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98268b8e-e611-4e07-8eaa-1bf52ee39445",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "d5d04583-3210-4db4-9e84-f5eaadd533b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "0dc6346f-70bc-4f6d-843d-01462ed9f494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d04583-3210-4db4-9e84-f5eaadd533b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82f795d-ee87-4d8f-91c9-0855b8069cd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d04583-3210-4db4-9e84-f5eaadd533b0",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "a281ea03-6df6-4154-8d14-8d291e087670",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "16ec8d53-3536-4216-a185-3f144749e603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a281ea03-6df6-4154-8d14-8d291e087670",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed8f045-ee16-473f-84cb-79006d817de1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a281ea03-6df6-4154-8d14-8d291e087670",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "6df8d58f-84f3-4e8c-b2a8-2f2446d89c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "e58b96ca-1caf-4c72-b9d8-42d33c1682fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df8d58f-84f3-4e8c-b2a8-2f2446d89c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4088dcf-dc19-4dce-ab1d-50ed23a1e47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df8d58f-84f3-4e8c-b2a8-2f2446d89c1c",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "32c3bc38-81b5-4221-a644-85d9662d54c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "2e6909c9-6216-4e63-a7b9-923ef3d39fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c3bc38-81b5-4221-a644-85d9662d54c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f647273-d1d8-43ad-bf44-174fc56611d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c3bc38-81b5-4221-a644-85d9662d54c1",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "4d28ae88-23ec-48a6-899b-62e80f267bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "5a033eb3-31ce-47c0-a0dd-295c113e8bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d28ae88-23ec-48a6-899b-62e80f267bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a301b51a-aeba-4e80-8b96-b750a8ea7b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d28ae88-23ec-48a6-899b-62e80f267bc2",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "6307585d-ea94-45f0-afbf-fd2d8442647b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "6193dca4-74a5-47b0-8189-2265bab39bc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6307585d-ea94-45f0-afbf-fd2d8442647b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b866f5dd-f9c0-48f6-b403-93e9eefbf906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6307585d-ea94-45f0-afbf-fd2d8442647b",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "d65fdb12-9606-4ea2-89fa-3e4050a9392d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "c54a645f-b5dd-4880-b9af-46781fc4f448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d65fdb12-9606-4ea2-89fa-3e4050a9392d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "701d4d65-82a8-49a5-8243-ee93990366c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d65fdb12-9606-4ea2-89fa-3e4050a9392d",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "10d727ee-fabf-49d5-8ce0-c3955aa05bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "df3d9ac6-0647-4ab0-86a3-455840ea1a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d727ee-fabf-49d5-8ce0-c3955aa05bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9febee65-a298-40c1-a1bb-d1f7e65ea930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d727ee-fabf-49d5-8ce0-c3955aa05bd0",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "cc2f564b-ef8d-48a9-8304-cf377d3ec134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "55e282f0-659f-4789-a6d0-f2854c5b3c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc2f564b-ef8d-48a9-8304-cf377d3ec134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a07d67c-5719-413d-96d2-6505eecf0ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc2f564b-ef8d-48a9-8304-cf377d3ec134",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "ebd8b0fb-af57-4802-84cc-978193f64178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "c06c2da0-b9ca-4a9b-bad6-9a9e299d0617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebd8b0fb-af57-4802-84cc-978193f64178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac9cdbb-f59c-423c-83e5-fc7033054275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebd8b0fb-af57-4802-84cc-978193f64178",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "3c035c97-f33b-4720-a245-aa0e4d4a3d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "5b8f8d0b-35e4-470d-86e8-350e2a8eb197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c035c97-f33b-4720-a245-aa0e4d4a3d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04d15a0e-1747-4752-8288-2929c7222ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c035c97-f33b-4720-a245-aa0e4d4a3d15",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "8ed63176-f456-43ac-967d-53d00bba8fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "dc61e5e7-db48-442c-acd7-34c06c6414ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ed63176-f456-43ac-967d-53d00bba8fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94cdaf62-4b0f-4f6e-bca1-79da00bf9892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ed63176-f456-43ac-967d-53d00bba8fc4",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "568fabbd-306d-4a1d-b7b6-50ee5ea80471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "03fece95-d9a9-4d40-bf4a-9dbcc6f41002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "568fabbd-306d-4a1d-b7b6-50ee5ea80471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a9f239-8c9a-4d76-9870-6fbf2fd9be7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "568fabbd-306d-4a1d-b7b6-50ee5ea80471",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "a5f639d7-2f0d-408d-b66a-029fdc2334d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "e5f681b4-c5b3-48b5-b536-29af9d653a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f639d7-2f0d-408d-b66a-029fdc2334d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc907310-1f80-42fc-857e-3efe7c7de996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f639d7-2f0d-408d-b66a-029fdc2334d2",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "ac250957-5769-476b-b2c1-7ea59bdceeba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "2b787ae9-c128-46c5-b582-b15d9bed9afd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac250957-5769-476b-b2c1-7ea59bdceeba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9ac39db-beb5-461f-808a-bbeba4ac0aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac250957-5769-476b-b2c1-7ea59bdceeba",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "c2754cdd-210a-4ce9-99b7-1fe712667209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "a6aa0c13-bb71-443d-995e-d7c814dd787d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2754cdd-210a-4ce9-99b7-1fe712667209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd2ed0c-35ab-4f06-906b-06bf806ffe49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2754cdd-210a-4ce9-99b7-1fe712667209",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        },
        {
            "id": "c8c94c04-80b0-44a3-b83c-dea1d0f605e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "compositeImage": {
                "id": "97e4289b-1fce-4f7b-9dbb-74063ffc394b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c94c04-80b0-44a3-b83c-dea1d0f605e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "451cc162-a0a3-4931-a6ff-038aaecd742d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c94c04-80b0-44a3-b83c-dea1d0f605e8",
                    "LayerId": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "582a44e3-8b83-45f5-87a8-fa2a9d7e92fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e59c5f-2e91-4646-bffe-aeb51358eef1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "580e916e-bc19-4b49-aabe-9fd387268b2e",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}