{
    "id": "978fc0e3-d6fb-49ad-9243-1873ebde46e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 20,
    "bbox_right": 237,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7aa569fc-1b7f-46d4-8a24-053f27c01110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "978fc0e3-d6fb-49ad-9243-1873ebde46e6",
            "compositeImage": {
                "id": "475cb30b-3924-4091-9395-e175059ab760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa569fc-1b7f-46d4-8a24-053f27c01110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7c6e025-32e1-4399-aa4a-d459b1fd8a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa569fc-1b7f-46d4-8a24-053f27c01110",
                    "LayerId": "7d094011-1e5b-4717-9c6d-afd2b9c8fa43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7d094011-1e5b-4717-9c6d-afd2b9c8fa43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "978fc0e3-d6fb-49ad-9243-1873ebde46e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}