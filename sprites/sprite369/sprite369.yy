{
    "id": "2279f130-7a8d-4ab1-a858-ca80c659b465",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite369",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 379,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "240e909d-095a-4988-bf6c-1f0c6cbe36ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2279f130-7a8d-4ab1-a858-ca80c659b465",
            "compositeImage": {
                "id": "652cf193-dfdd-4a4e-81b4-775d55afba08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240e909d-095a-4988-bf6c-1f0c6cbe36ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c68134e-c658-4b20-899b-4d167bb6b930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240e909d-095a-4988-bf6c-1f0c6cbe36ee",
                    "LayerId": "0574d6b5-82c4-4d87-954e-a41c203d9eeb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "0574d6b5-82c4-4d87-954e-a41c203d9eeb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2279f130-7a8d-4ab1-a858-ca80c659b465",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}