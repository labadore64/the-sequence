{
    "id": "33cf858b-c16d-4254-a149-798db8b6a803",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgBuilding05",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 327,
    "bbox_left": 0,
    "bbox_right": 422,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b495166-324f-4039-95b9-abc6784b7d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33cf858b-c16d-4254-a149-798db8b6a803",
            "compositeImage": {
                "id": "a4774dbd-f4a0-470a-8e58-40d3f5de7a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b495166-324f-4039-95b9-abc6784b7d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a7b3cd-6421-47c1-a188-fd2599340e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b495166-324f-4039-95b9-abc6784b7d6a",
                    "LayerId": "7b98dddc-f45c-4546-80a3-25b9a33cf0a9"
                },
                {
                    "id": "33887439-3734-45e9-b8cd-85506c73fb58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b495166-324f-4039-95b9-abc6784b7d6a",
                    "LayerId": "a797e419-9def-4b47-a61a-09e11d643b4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 328,
    "layers": [
        {
            "id": "7b98dddc-f45c-4546-80a3-25b9a33cf0a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33cf858b-c16d-4254-a149-798db8b6a803",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a797e419-9def-4b47-a61a-09e11d643b4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33cf858b-c16d-4254-a149-798db8b6a803",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "57570f21-0695-4f13-a303-519260b98bb3",
    "type": 0,
    "width": 423,
    "xorig": 0,
    "yorig": 0
}