{
    "id": "c31b62e2-aea6-450a-9009-05649e8af97a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0bbddbb-530a-41d8-8265-21c35afa3e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31b62e2-aea6-450a-9009-05649e8af97a",
            "compositeImage": {
                "id": "7e02f4ef-17f3-4a19-9aab-f410055bbf82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0bbddbb-530a-41d8-8265-21c35afa3e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c9672b-a213-4ac1-a8a3-caf8b69a9c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0bbddbb-530a-41d8-8265-21c35afa3e1e",
                    "LayerId": "c3663f40-b066-4fd1-a0ef-3a541de413de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c3663f40-b066-4fd1-a0ef-3a541de413de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c31b62e2-aea6-450a-9009-05649e8af97a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 18,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}