{
    "id": "e715e778-3e6c-4ef1-a00a-14d7059602cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_depth_of_field",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 490,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1ba26d7-bd39-4df0-a689-a8a55d70a3e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e715e778-3e6c-4ef1-a00a-14d7059602cb",
            "compositeImage": {
                "id": "e8930ae4-169e-4db3-b01b-86614c6ebf65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1ba26d7-bd39-4df0-a689-a8a55d70a3e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4edf4cc5-e848-4ada-bec6-a2c9c26cef5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1ba26d7-bd39-4df0-a689-a8a55d70a3e7",
                    "LayerId": "fc3551cb-e9ec-4b70-9d23-717f66048894"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "fc3551cb-e9ec-4b70-9d23-717f66048894",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e715e778-3e6c-4ef1-a00a-14d7059602cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7976e58a-40f8-45f7-916f-55559250f195",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}