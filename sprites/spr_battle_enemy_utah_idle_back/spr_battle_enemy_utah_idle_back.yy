{
    "id": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 36,
    "bbox_right": 253,
    "bbox_top": 78,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ba2a4ba-dc0b-4e87-a26c-9cf26172cbb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "c4e10b36-fd19-4e4a-8790-9372f8b75eca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba2a4ba-dc0b-4e87-a26c-9cf26172cbb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5f79b6-5762-4a0d-bf43-ae2e881f78d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba2a4ba-dc0b-4e87-a26c-9cf26172cbb1",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "2ff5c8c3-4dba-4f47-b079-32365f0d489e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "73e9e1f1-454b-4670-83b2-96271974dcac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff5c8c3-4dba-4f47-b079-32365f0d489e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078631f1-e0da-48a1-be31-9f944124076d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff5c8c3-4dba-4f47-b079-32365f0d489e",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "20280dc2-9346-4dd3-84b1-70ea7450b371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "b25c79f2-ac98-44df-9d18-3a6a84ddba74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20280dc2-9346-4dd3-84b1-70ea7450b371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e964f0f0-c95d-4b61-bb70-b4131616dc49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20280dc2-9346-4dd3-84b1-70ea7450b371",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "6ee201f5-0ab0-46de-bf3d-679ed31fcdfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "ef055f3b-0933-4340-91a2-b83a9c7bb685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee201f5-0ab0-46de-bf3d-679ed31fcdfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d7dea6-cddc-4f34-96d1-64ae3d5658c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee201f5-0ab0-46de-bf3d-679ed31fcdfb",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "5bfcc56c-7f38-4f1e-85f7-44d3d96a3daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "886d71b8-bc2d-4e53-81ee-45445e2730c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bfcc56c-7f38-4f1e-85f7-44d3d96a3daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a98e657-516f-40a0-afce-ef3a45b15ada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bfcc56c-7f38-4f1e-85f7-44d3d96a3daf",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "4cc2e267-1fed-4a14-85fd-ca060975b3a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "6403ab0a-7827-420c-9aea-cf8f937703db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc2e267-1fed-4a14-85fd-ca060975b3a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f068bfb-6ada-4026-bf62-d738e219cb9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc2e267-1fed-4a14-85fd-ca060975b3a4",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "0c69d636-19d4-49c9-b13b-81cace61d879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "9ce0a2e7-97b7-4d5e-ac84-109d978c3635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c69d636-19d4-49c9-b13b-81cace61d879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bf6b146-b6a8-4ae9-9021-da9e966c0dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c69d636-19d4-49c9-b13b-81cace61d879",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "146d7f7a-bc1a-4ee6-bab8-8e0979867f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "2a8fe19c-5822-4c4a-8eee-a7bf6bcf223c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "146d7f7a-bc1a-4ee6-bab8-8e0979867f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abcbc7d6-508e-4b1e-953e-da010876cfb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "146d7f7a-bc1a-4ee6-bab8-8e0979867f8b",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "93c0cfa7-ae55-4cc1-9840-e152d0bec62d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "73db59ce-b6a4-4ca3-bed9-4dd7ec2d9f82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c0cfa7-ae55-4cc1-9840-e152d0bec62d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a6e3cd4-b8b1-49c0-806c-3530f492073f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c0cfa7-ae55-4cc1-9840-e152d0bec62d",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "81c96de2-008d-4dbd-8079-328f5220ba5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "5a45cf98-2806-4948-b463-97d8e91f87c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81c96de2-008d-4dbd-8079-328f5220ba5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d0015a-01c1-41b2-9d6b-a8252a4de5bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81c96de2-008d-4dbd-8079-328f5220ba5e",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "7dedb8b8-0e5d-4a71-9b08-481fbb6ca05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "ef591d47-b234-47b3-b7ec-963ba99e2673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dedb8b8-0e5d-4a71-9b08-481fbb6ca05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9891b28-e8e9-4f59-8c18-216cf3b3c987",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dedb8b8-0e5d-4a71-9b08-481fbb6ca05d",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "14bd2e85-e402-4e28-a4d9-79b0697eb0a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "a8c075fb-5291-467a-9fa5-6c91d16cc4d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14bd2e85-e402-4e28-a4d9-79b0697eb0a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e72142d-d561-43d9-9e99-bd2ca46c259c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14bd2e85-e402-4e28-a4d9-79b0697eb0a8",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "3b28adb5-e834-412e-bd39-65b324b0efbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "80aa6b20-6eb2-48c1-8dff-7fec227fbf70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b28adb5-e834-412e-bd39-65b324b0efbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7c8ef75-3578-4c06-803e-285fabfc2813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b28adb5-e834-412e-bd39-65b324b0efbb",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "0dba0834-f2ed-4d6a-ac96-af0ad9567ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "6e9cd5a6-390f-4294-b82b-0354ae5523eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dba0834-f2ed-4d6a-ac96-af0ad9567ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d880f91-73a3-4de8-a841-fa7e4c527240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dba0834-f2ed-4d6a-ac96-af0ad9567ad0",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "3b3358be-9495-417b-9fe5-0da4bc4f3b01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "ebc6e8c1-8767-4003-a220-8442f03f1dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b3358be-9495-417b-9fe5-0da4bc4f3b01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "291f3ef6-1261-4362-9325-7e02c0e2bf2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b3358be-9495-417b-9fe5-0da4bc4f3b01",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "f3f9ca36-0174-44cb-bed9-15c81c8f7ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "7730b356-dc27-4395-a05b-d979367deb62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f9ca36-0174-44cb-bed9-15c81c8f7ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6492612-0321-4ae5-b4b4-aadb8fbbc043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f9ca36-0174-44cb-bed9-15c81c8f7ce6",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "7042e4d2-c28e-4377-a56a-4e356865ac4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "8083578b-400e-4247-8cca-e0785db4e90a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7042e4d2-c28e-4377-a56a-4e356865ac4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ef10b5-db3a-4e44-927a-9603d79f4fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7042e4d2-c28e-4377-a56a-4e356865ac4b",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "5a1bdec6-b476-42fb-87b2-312cafbaaa69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "bda2f0fb-a67f-4b1d-bf7c-086adb7fd38b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a1bdec6-b476-42fb-87b2-312cafbaaa69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1233984-388d-4705-9b0f-23350d4c00e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a1bdec6-b476-42fb-87b2-312cafbaaa69",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "3cf42c4b-6258-4a3c-a872-df2d54c369a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "76171cca-0599-49d2-9e2d-65d6a9526228",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf42c4b-6258-4a3c-a872-df2d54c369a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ac684e-c3ad-4c41-b73e-895d4c27eb04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf42c4b-6258-4a3c-a872-df2d54c369a1",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "dbbace81-4860-4482-a605-03a91cadae2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "2009053a-97d9-4ef7-80b8-8bc4ead72ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbbace81-4860-4482-a605-03a91cadae2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650ebd5c-a8e8-4229-9f2c-a55e5af94e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbbace81-4860-4482-a605-03a91cadae2d",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "8f4e52e2-a96d-4b81-8e33-6253f5dc833c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "0a987473-2c76-4923-8e3a-f46f9063d5ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4e52e2-a96d-4b81-8e33-6253f5dc833c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74dc8924-a682-4edb-8eaf-8a545b5e5757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4e52e2-a96d-4b81-8e33-6253f5dc833c",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "e5208daf-bf6a-4a6b-bf55-b4570d998140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "6a691958-b8c8-439d-ada3-349c05024315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5208daf-bf6a-4a6b-bf55-b4570d998140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cad946e-63af-4c34-8eca-d4d639d5f336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5208daf-bf6a-4a6b-bf55-b4570d998140",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "060df3fb-9375-4985-ac1a-2117edf63a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "fc8bde44-48e3-40d7-beed-faad55570dbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "060df3fb-9375-4985-ac1a-2117edf63a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58fd4e5d-0659-46dc-a530-ee9417e74ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "060df3fb-9375-4985-ac1a-2117edf63a47",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "dbbcfb67-0192-4bac-a874-3e8a85893c24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "1a9aba15-d29c-49c3-8fa1-bc892d923585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbbcfb67-0192-4bac-a874-3e8a85893c24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03bdeb99-aca8-4742-b09c-2270982b79c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbbcfb67-0192-4bac-a874-3e8a85893c24",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "65526943-0aa1-4ae1-9913-630d7e2abb57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "471aab29-9af9-40dd-89af-a7dfd0d2fca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65526943-0aa1-4ae1-9913-630d7e2abb57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9055ea2c-0a04-4c88-8247-97cfc3c6fe90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65526943-0aa1-4ae1-9913-630d7e2abb57",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "cf8b90ee-f8a2-4c15-adf3-89cad25c5292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "47ad0578-d114-47f8-8135-a3875611044f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf8b90ee-f8a2-4c15-adf3-89cad25c5292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ffa334-e275-4e5a-8ca1-ead0f9fcb586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf8b90ee-f8a2-4c15-adf3-89cad25c5292",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "5d8ba289-7de8-4c00-8202-607638eac783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "47c6d3bf-41b7-4359-9608-65929ad3657a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8ba289-7de8-4c00-8202-607638eac783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "991458ce-c289-4045-be67-78654ad8e27f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8ba289-7de8-4c00-8202-607638eac783",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "a2831456-77af-4142-a19b-5838c661c92b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "ecade63d-34bd-400e-bf60-9c4a23a59a81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2831456-77af-4142-a19b-5838c661c92b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b0ab6c-096b-415f-9cd1-de62ddf48525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2831456-77af-4142-a19b-5838c661c92b",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        },
        {
            "id": "42302b82-5044-4b54-9b01-98224884f161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "compositeImage": {
                "id": "688bb103-cbc4-4131-9cb9-3c0ecc7a2865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42302b82-5044-4b54-9b01-98224884f161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b5fbccc-47d9-423f-9a36-c5b9d1890fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42302b82-5044-4b54-9b01-98224884f161",
                    "LayerId": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2ea5b8c4-1dbc-46f6-a965-c8c17afffbeb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aacfef36-8ea5-4c63-aa8f-f982523c64b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}