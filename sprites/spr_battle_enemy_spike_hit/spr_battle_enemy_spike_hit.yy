{
    "id": "10e1fa87-dca1-4b7e-9dd6-b47124f38884",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_spike_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 39,
    "bbox_right": 233,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fda51c1-2f52-4fc3-a429-9f77ee046ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e1fa87-dca1-4b7e-9dd6-b47124f38884",
            "compositeImage": {
                "id": "7cda60ea-4ac0-4281-b2e2-abb6330dd329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fda51c1-2f52-4fc3-a429-9f77ee046ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b281aa-0fe7-475d-96a9-fddc55be849c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fda51c1-2f52-4fc3-a429-9f77ee046ccd",
                    "LayerId": "8930e5db-5b54-402e-8aaf-6ea5e9ed7210"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8930e5db-5b54-402e-8aaf-6ea5e9ed7210",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e1fa87-dca1-4b7e-9dd6-b47124f38884",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6535ae2f-d8ac-44c4-b49f-b65f6f1f572d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}