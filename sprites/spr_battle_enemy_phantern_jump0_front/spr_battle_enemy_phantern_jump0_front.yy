{
    "id": "1697761e-6bf7-4892-b664-b61e2475708c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 71,
    "bbox_right": 178,
    "bbox_top": 106,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d9d2e0a-1e79-427f-89e4-55218bd90b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1697761e-6bf7-4892-b664-b61e2475708c",
            "compositeImage": {
                "id": "d1410c2c-caba-4d5e-a40a-990d95be1930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d9d2e0a-1e79-427f-89e4-55218bd90b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65ec5d83-5cc6-40b0-ab2e-8a7e9c4a2310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d9d2e0a-1e79-427f-89e4-55218bd90b77",
                    "LayerId": "f036268a-8ef9-4995-9377-f6a0ce81c612"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f036268a-8ef9-4995-9377-f6a0ce81c612",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1697761e-6bf7-4892-b664-b61e2475708c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}