{
    "id": "dfecf79a-2560-4ecb-b29b-fdf2bad352af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_cardinal0_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 28,
    "bbox_right": 103,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43d7418b-540e-4d1e-9382-14572bf63627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfecf79a-2560-4ecb-b29b-fdf2bad352af",
            "compositeImage": {
                "id": "775f7173-7e9a-4f09-a412-54037dbe8fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d7418b-540e-4d1e-9382-14572bf63627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3f5507-97d5-4912-9fb4-751a2212cad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d7418b-540e-4d1e-9382-14572bf63627",
                    "LayerId": "a831e853-2631-4c79-bba6-59f257f5efd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a831e853-2631-4c79-bba6-59f257f5efd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfecf79a-2560-4ecb-b29b-fdf2bad352af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}