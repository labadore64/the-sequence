{
    "id": "b0d69eb2-0b78-4e59-9963-27039af75c62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 3,
    "bbox_right": 210,
    "bbox_top": 101,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b743f446-6b23-4826-a592-22879e31872b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d69eb2-0b78-4e59-9963-27039af75c62",
            "compositeImage": {
                "id": "38262332-46db-456a-9122-c4a2d479d020",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b743f446-6b23-4826-a592-22879e31872b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d94cc2-3e01-48db-b886-acb767a4e414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b743f446-6b23-4826-a592-22879e31872b",
                    "LayerId": "f60e043d-56ea-4c14-8286-f42a489a55cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f60e043d-56ea-4c14-8286-f42a489a55cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d69eb2-0b78-4e59-9963-27039af75c62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}