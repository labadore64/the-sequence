{
    "id": "583a48c0-25ef-4f08-a902-e8705b94beac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typeMag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76077d22-ce9b-4e5c-9f29-2c4bc26aec89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "583a48c0-25ef-4f08-a902-e8705b94beac",
            "compositeImage": {
                "id": "c61a7586-b9fb-4ae1-b471-1162a5da484a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76077d22-ce9b-4e5c-9f29-2c4bc26aec89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e12b220-6a11-4bc2-958b-a59e96047707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76077d22-ce9b-4e5c-9f29-2c4bc26aec89",
                    "LayerId": "949d93b3-e3b3-41f9-a28b-1ff50e736f59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "949d93b3-e3b3-41f9-a28b-1ff50e736f59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "583a48c0-25ef-4f08-a902-e8705b94beac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}