{
    "id": "93bb999a-5364-4d45-a46d-37fed305fd03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_robin_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 8,
    "bbox_right": 126,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bc9cc08-be7c-4089-8aee-9c0435e9f6f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93bb999a-5364-4d45-a46d-37fed305fd03",
            "compositeImage": {
                "id": "bb080c86-b0c7-47f0-bc80-601d3759599e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bc9cc08-be7c-4089-8aee-9c0435e9f6f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa6b3176-31ab-4654-b6b3-a4d08bc80cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bc9cc08-be7c-4089-8aee-9c0435e9f6f0",
                    "LayerId": "e128959f-8238-4d65-ab35-958f5af01cec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e128959f-8238-4d65-ab35-958f5af01cec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93bb999a-5364-4d45-a46d-37fed305fd03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}