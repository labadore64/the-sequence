{
    "id": "c99edc27-250e-4e64-a2d9-c20acbac5988",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cellphone_phone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 9,
    "bbox_right": 51,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bfba556-6448-4a80-9f64-2b01d2ec0c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c99edc27-250e-4e64-a2d9-c20acbac5988",
            "compositeImage": {
                "id": "5d14b09e-42ed-4603-94b8-2361b7ed68b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bfba556-6448-4a80-9f64-2b01d2ec0c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f099d2-99c6-46dd-9195-e686578da872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bfba556-6448-4a80-9f64-2b01d2ec0c97",
                    "LayerId": "401efa08-67d4-4e65-8416-5569d80922e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "401efa08-67d4-4e65-8416-5569d80922e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c99edc27-250e-4e64-a2d9-c20acbac5988",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 1.9626801,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}