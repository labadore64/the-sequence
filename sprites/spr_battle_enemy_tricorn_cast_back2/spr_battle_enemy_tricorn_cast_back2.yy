{
    "id": "fdc4e6d0-b3c1-4b85-81b0-4363c0bf74f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_cast_back2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 41,
    "bbox_right": 252,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f59def8-e955-4945-b494-a99f23f64375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc4e6d0-b3c1-4b85-81b0-4363c0bf74f8",
            "compositeImage": {
                "id": "0d16faa1-7393-4b8a-96a8-5138fe8ab8dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f59def8-e955-4945-b494-a99f23f64375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e075abfe-ddba-4980-9f92-a211c55523d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f59def8-e955-4945-b494-a99f23f64375",
                    "LayerId": "0a8da5d5-de38-44ef-b1f9-84befddbe0f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0a8da5d5-de38-44ef-b1f9-84befddbe0f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdc4e6d0-b3c1-4b85-81b0-4363c0bf74f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}