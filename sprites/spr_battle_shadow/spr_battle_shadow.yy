{
    "id": "b1246495-e875-4884-8790-f3dc486edf54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 1,
    "bbox_right": 194,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30bfd553-5663-46e5-aaa5-5af2e21c5a73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1246495-e875-4884-8790-f3dc486edf54",
            "compositeImage": {
                "id": "ce226234-61e9-42a8-a9d9-beb5fc58f4c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30bfd553-5663-46e5-aaa5-5af2e21c5a73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5077f893-0e7f-415f-bdd3-7e3c1f125884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30bfd553-5663-46e5-aaa5-5af2e21c5a73",
                    "LayerId": "58c1b429-ea0e-4164-a85c-61333af9df5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "58c1b429-ea0e-4164-a85c-61333af9df5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1246495-e875-4884-8790-f3dc486edf54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7976e58a-40f8-45f7-916f-55559250f195",
    "type": 0,
    "width": 196,
    "xorig": 98,
    "yorig": 49
}