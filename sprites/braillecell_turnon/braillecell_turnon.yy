{
    "id": "d568f432-3def-4bfc-926f-2fdad1461c1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "braillecell_turnon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70436401-f00e-4923-b1ae-bf38b33b730f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d568f432-3def-4bfc-926f-2fdad1461c1e",
            "compositeImage": {
                "id": "87c14c5f-db84-4ea8-8063-b5401d14b126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70436401-f00e-4923-b1ae-bf38b33b730f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28afb660-8d4b-49a9-ac91-c6d5c8b9d1bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70436401-f00e-4923-b1ae-bf38b33b730f",
                    "LayerId": "70927e67-6bb1-475e-8f2e-1686fbf13548"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "70927e67-6bb1-475e-8f2e-1686fbf13548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d568f432-3def-4bfc-926f-2fdad1461c1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}