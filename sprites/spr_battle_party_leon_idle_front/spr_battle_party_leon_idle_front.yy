{
    "id": "d639b63b-a573-4b7a-873f-65c511994471",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 26,
    "bbox_right": 190,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90e51d32-aabf-4d7e-bbb5-db8abf3c3f08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "345dd77c-19c5-4df1-b78d-32352a8bc9f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e51d32-aabf-4d7e-bbb5-db8abf3c3f08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda584d8-135f-4b07-8054-f76ac3c8395b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e51d32-aabf-4d7e-bbb5-db8abf3c3f08",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "b8a24fe8-77c8-492d-be57-99766485f55a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "366ba88f-3a60-4c51-8e5e-6dc974fa065f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a24fe8-77c8-492d-be57-99766485f55a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcb1f67d-1528-45b3-97f8-9331983a3556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a24fe8-77c8-492d-be57-99766485f55a",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "fe558a8e-577d-46cb-8d56-bf67a4cffb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "833be5e4-640f-4f00-873c-7b26f089df5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe558a8e-577d-46cb-8d56-bf67a4cffb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5d71c1-bc8f-40ed-a96a-380294b45b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe558a8e-577d-46cb-8d56-bf67a4cffb2e",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "f6a72e08-4c41-4d3f-bf0e-7d87b40d8419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "f9e0b61c-8c28-4266-b319-a9644d186843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a72e08-4c41-4d3f-bf0e-7d87b40d8419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75965348-40ec-4b0a-ae54-102e866776d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a72e08-4c41-4d3f-bf0e-7d87b40d8419",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "38d9e542-a384-42f2-81c5-ffda8538d55a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "17468b5e-268c-4063-98d2-d2d5a030593b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d9e542-a384-42f2-81c5-ffda8538d55a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1519d9a1-dc1c-428b-a717-e1095e196e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d9e542-a384-42f2-81c5-ffda8538d55a",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "594ecaf0-9a95-44b8-8df6-30fb611207f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "c751e20d-2342-495c-8449-6bdab8d755ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594ecaf0-9a95-44b8-8df6-30fb611207f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "414d5b52-98f0-4eab-8f80-803d9c0f2b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594ecaf0-9a95-44b8-8df6-30fb611207f8",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "bb282b1d-3ecb-4230-a5f2-e400d617731c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "8ea3a5bd-1592-4393-8f02-456bad64ba97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb282b1d-3ecb-4230-a5f2-e400d617731c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "796dafda-3516-458d-ab8a-f570b880f722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb282b1d-3ecb-4230-a5f2-e400d617731c",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "90cbd49c-8fb3-48c6-8856-279a97fb50f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "4e045bb0-66e9-46f4-9054-16dd8d7c87e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90cbd49c-8fb3-48c6-8856-279a97fb50f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6615229d-d0f4-4eec-9fcb-d7542d8a3bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90cbd49c-8fb3-48c6-8856-279a97fb50f1",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "494c3312-6d48-40f9-bad3-0d4c28cbb622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "f52e9a7f-b8f6-44f1-9848-a746a34879e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "494c3312-6d48-40f9-bad3-0d4c28cbb622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d0e7898-0bbd-4f66-abed-1072cfb15d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "494c3312-6d48-40f9-bad3-0d4c28cbb622",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "4bebfbb7-9ce8-4a7a-a689-ae79695545b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "c75e75be-3280-4746-bd53-c7eb64b0a7a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bebfbb7-9ce8-4a7a-a689-ae79695545b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9647b889-4494-41fd-9b86-fde7849818cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bebfbb7-9ce8-4a7a-a689-ae79695545b3",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "24493320-3da6-419f-8217-b5f8518c6079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "47a37e0d-6f5b-443d-a9f3-08211a5d68fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24493320-3da6-419f-8217-b5f8518c6079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c62a71-5b0e-475f-805b-43b825b819f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24493320-3da6-419f-8217-b5f8518c6079",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "7b4e8cf4-18f6-4c80-8525-4ee71d750b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "bab182ae-3678-432e-a1b0-1ef12445095f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b4e8cf4-18f6-4c80-8525-4ee71d750b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8523533-0fad-460c-83c3-8c21f9d3465d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b4e8cf4-18f6-4c80-8525-4ee71d750b78",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "513601ed-4811-49a8-b708-ff57c305eb56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "c920699a-d5d9-4f13-96ed-1bd9957da025",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "513601ed-4811-49a8-b708-ff57c305eb56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "410df1d3-085b-44f8-93f7-2eaf37fe5888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "513601ed-4811-49a8-b708-ff57c305eb56",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "64ffd8a4-7e61-4a61-95c5-d168f546b18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "4bf00d98-242c-477e-8cb5-7f23d2b7ae4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ffd8a4-7e61-4a61-95c5-d168f546b18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "627f4732-bcfe-44dc-8a99-c27553185e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ffd8a4-7e61-4a61-95c5-d168f546b18a",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "8ba3f68d-0017-4152-8f01-70ee2798a4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "f420185c-c8d8-4e90-b011-235d7bf89bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba3f68d-0017-4152-8f01-70ee2798a4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ec2c19b-b91e-421b-ae25-52c59d97e131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba3f68d-0017-4152-8f01-70ee2798a4a5",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "0a24c6b6-0a26-4e2a-ad21-5efb9d8e7b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "94087f33-22bb-4caa-a181-6c1325e399ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a24c6b6-0a26-4e2a-ad21-5efb9d8e7b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "144ee244-dbbe-49d6-b6f0-add6441069c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a24c6b6-0a26-4e2a-ad21-5efb9d8e7b98",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "994e03ae-51c9-40af-8eb4-080f9611b83c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "08ae7884-9a46-42b2-8af9-7aa9d675064c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994e03ae-51c9-40af-8eb4-080f9611b83c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05664035-7626-49bb-b81f-8748b38a8a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994e03ae-51c9-40af-8eb4-080f9611b83c",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "54afad13-cd2b-4816-aa58-beb67697b01e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "eeb8199e-7e0c-408f-bc6c-5894636947d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54afad13-cd2b-4816-aa58-beb67697b01e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9e425a-53c6-4a98-8361-968a8e62506f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54afad13-cd2b-4816-aa58-beb67697b01e",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "a9470d85-ee4c-4c38-8631-a3e10303b1e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "cbde4f8d-7e29-4c40-88bf-7d32f3507a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9470d85-ee4c-4c38-8631-a3e10303b1e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83fd6ba-6544-401c-9def-22b5621ce5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9470d85-ee4c-4c38-8631-a3e10303b1e4",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "6a9ac1a5-8d6a-4fc6-8371-b3382cb94299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "faa7fd6a-4cc4-4460-8942-899249f1eff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a9ac1a5-8d6a-4fc6-8371-b3382cb94299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a711b3f6-2246-4fa2-a74f-f7e624728dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a9ac1a5-8d6a-4fc6-8371-b3382cb94299",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "92692f03-a47b-4d64-972c-787d0740646b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "12172026-bbe0-46cf-8251-6560254a9210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92692f03-a47b-4d64-972c-787d0740646b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fcd58a8-8247-44a0-a243-e939859ee7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92692f03-a47b-4d64-972c-787d0740646b",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "e46b17bc-a59e-4985-97d4-05be88fbca95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "edc823aa-5a83-4ce8-8bfb-07e624dd808a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e46b17bc-a59e-4985-97d4-05be88fbca95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d742a44e-381b-4b16-9392-35667dc0fe0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e46b17bc-a59e-4985-97d4-05be88fbca95",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "1459fdb3-08ae-4762-b8a5-6735ec809fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "2c702774-04f2-4930-bdc6-c67cf61e0446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1459fdb3-08ae-4762-b8a5-6735ec809fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d7541d-8929-4318-9c50-c13ee0b44917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1459fdb3-08ae-4762-b8a5-6735ec809fef",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "457a94be-e927-476a-a7cd-5c948a9483e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "c0ea44c5-ac2b-488a-9521-91fde01c0fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457a94be-e927-476a-a7cd-5c948a9483e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cdd0035-d486-4da6-b0ef-caaa0abb566a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457a94be-e927-476a-a7cd-5c948a9483e5",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "db95d84d-f57f-49bd-8e78-6971dd641711",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "ba511874-40a4-419c-beff-b967de511075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db95d84d-f57f-49bd-8e78-6971dd641711",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352033ef-7bda-4906-b1fa-e399b0376f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db95d84d-f57f-49bd-8e78-6971dd641711",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "19180644-d731-49fb-9800-89fe9187545e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "79e12549-fd81-48ee-8c72-aa42e1145098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19180644-d731-49fb-9800-89fe9187545e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ca25a62-2cad-49eb-8f1c-42cf520c00eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19180644-d731-49fb-9800-89fe9187545e",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "dac666e7-d2c1-466d-9ad2-09db8cad0609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "486841fb-c7b0-4f98-a93d-2d7f00b4d5c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dac666e7-d2c1-466d-9ad2-09db8cad0609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec2ceaae-703a-4ef5-a0f3-7a49194d1b19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dac666e7-d2c1-466d-9ad2-09db8cad0609",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "bb6be44a-be42-40cf-9903-35d3ad12cd91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "54cba2e3-7b2c-4fd0-9536-0b85be7e822f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb6be44a-be42-40cf-9903-35d3ad12cd91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80aabde-ecaf-41a4-95bb-7ce869ea0d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb6be44a-be42-40cf-9903-35d3ad12cd91",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "ca1e55c7-b0e7-443c-a930-c85ca122a15c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "057fa9f9-ba29-46b0-b0f7-e002b8efa665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca1e55c7-b0e7-443c-a930-c85ca122a15c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b98a91a-d8e1-44c4-a1b8-9ecc5e73ddc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca1e55c7-b0e7-443c-a930-c85ca122a15c",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        },
        {
            "id": "2b8fd284-05a8-4c54-b66a-5c7d8bc3c46a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "compositeImage": {
                "id": "c0b852b1-b64f-47a7-b7de-4e52035a937f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8fd284-05a8-4c54-b66a-5c7d8bc3c46a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66a81f23-a7c7-4fae-889e-7a24b47712f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8fd284-05a8-4c54-b66a-5c7d8bc3c46a",
                    "LayerId": "e5f9d631-b771-447f-b56e-25c7581b16eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e5f9d631-b771-447f-b56e-25c7581b16eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d639b63b-a573-4b7a-873f-65c511994471",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}