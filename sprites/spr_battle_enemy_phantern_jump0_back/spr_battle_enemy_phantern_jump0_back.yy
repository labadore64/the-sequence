{
    "id": "d75b9eef-d4e7-4341-ad98-b420f1e18f99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 71,
    "bbox_right": 178,
    "bbox_top": 106,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e571c762-8db8-41d7-8554-1d64c1bdbe0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d75b9eef-d4e7-4341-ad98-b420f1e18f99",
            "compositeImage": {
                "id": "e18ef001-b4df-4f6a-8f3f-5c9f94d4749a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e571c762-8db8-41d7-8554-1d64c1bdbe0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148824af-cb63-4f82-b781-4d480629291b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e571c762-8db8-41d7-8554-1d64c1bdbe0c",
                    "LayerId": "436f53be-9b79-461f-aa22-a21ac4d3db7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "436f53be-9b79-461f-aa22-a21ac4d3db7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d75b9eef-d4e7-4341-ad98-b420f1e18f99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}