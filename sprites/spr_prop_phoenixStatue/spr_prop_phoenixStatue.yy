{
    "id": "76690117-10ea-4093-850c-16329f790602",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_phoenixStatue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 28,
    "bbox_right": 104,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c69909e0-daf4-4ab9-bc96-53a5c5e054e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76690117-10ea-4093-850c-16329f790602",
            "compositeImage": {
                "id": "857df209-ad07-463d-900c-c118a84e5524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c69909e0-daf4-4ab9-bc96-53a5c5e054e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4d3c38-7562-4584-9988-df60ebd2501b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c69909e0-daf4-4ab9-bc96-53a5c5e054e3",
                    "LayerId": "e58131e4-c8f3-4220-97b2-82ed0c4e700e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e58131e4-c8f3-4220-97b2-82ed0c4e700e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76690117-10ea-4093-850c-16329f790602",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}