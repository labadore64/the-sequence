{
    "id": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 73,
    "bbox_right": 182,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af8050ce-ef7b-421f-af04-8e3e71f61d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b5404367-a93a-4155-97e6-c5c9296c85b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8050ce-ef7b-421f-af04-8e3e71f61d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca0e1cc-fcc8-4a25-9d20-ad91a2f9e07a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8050ce-ef7b-421f-af04-8e3e71f61d50",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "b895222a-e31e-4abd-9217-7b8e8e8a0ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "d8feea3c-4e49-45d6-a812-b6a70ef38536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b895222a-e31e-4abd-9217-7b8e8e8a0ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "883ae22e-d30b-4bf4-a3e0-72423c9a4d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b895222a-e31e-4abd-9217-7b8e8e8a0ea4",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "1434e8ba-114e-4f65-843e-e6bf6ac2f92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "bcbc2ded-7696-4088-a6c4-967872d369ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1434e8ba-114e-4f65-843e-e6bf6ac2f92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65fc0cf9-26ab-4e84-a847-4849b010d625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1434e8ba-114e-4f65-843e-e6bf6ac2f92e",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "fa7276f9-7ed3-4ec6-8977-8872737cacde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "e08ffce6-e567-4c19-bf61-f6d4675a9801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa7276f9-7ed3-4ec6-8977-8872737cacde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc3a79c-d5a3-42cc-9119-cce2c5a07437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa7276f9-7ed3-4ec6-8977-8872737cacde",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "883e1618-f55d-449d-9993-e62b27be187e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "35a90cc3-b225-4347-bba2-f28d832333d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883e1618-f55d-449d-9993-e62b27be187e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873f9ce5-56c7-4651-bd94-b4470ef6a8d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883e1618-f55d-449d-9993-e62b27be187e",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "fcd0652e-408a-45d8-859a-e225e44c3962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "1aa51f66-0ac8-4323-8b1a-1ca873a8ae46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd0652e-408a-45d8-859a-e225e44c3962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab33d9c7-e2e0-4896-bac8-dd535ce8fc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd0652e-408a-45d8-859a-e225e44c3962",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "3fb2c5c9-1bc3-44d9-91bf-ca684ced8412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "8254afb0-dc7f-4f13-bc0d-0243347be3cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb2c5c9-1bc3-44d9-91bf-ca684ced8412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee5f9fd-5023-478d-8b96-af9242de9e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb2c5c9-1bc3-44d9-91bf-ca684ced8412",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "059001e7-a2d5-4ea1-a9ab-07721362c3c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "d2b22aa4-e764-4f18-86bb-e20d837b17b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059001e7-a2d5-4ea1-a9ab-07721362c3c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a77c07ad-f3b7-4ac6-b68e-123eac262c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059001e7-a2d5-4ea1-a9ab-07721362c3c4",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "9c540ca1-0ec7-4539-a43c-161df5c8a6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "77f917b5-fea3-4303-8863-88c27b519350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c540ca1-0ec7-4539-a43c-161df5c8a6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "310432cc-fdc6-4f62-b46b-da8cf18f2f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c540ca1-0ec7-4539-a43c-161df5c8a6b9",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "9988bc72-5f0e-4f65-a205-97d1383f1669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "182aa121-d7ba-4129-844e-53fcd700218a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9988bc72-5f0e-4f65-a205-97d1383f1669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b65f842d-8e83-4abd-a7fa-8d3070f1c821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9988bc72-5f0e-4f65-a205-97d1383f1669",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "e1c11a57-eb54-4881-ba0b-e6bb925739ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b8d8a145-0d8b-4a28-ae15-48bf45021a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c11a57-eb54-4881-ba0b-e6bb925739ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be808e62-47cb-4897-849e-ce4b3b2ebfa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c11a57-eb54-4881-ba0b-e6bb925739ef",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "d3228431-e7fe-4c7b-b26a-3267ff7d34fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b40dd823-aae6-40a3-8397-1695b7cf746d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3228431-e7fe-4c7b-b26a-3267ff7d34fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd23fa65-6064-47b0-a679-db6896be0c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3228431-e7fe-4c7b-b26a-3267ff7d34fc",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "83233951-35db-4fae-91b6-4ebf7102e6dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "501b3f3c-0052-414f-8357-39b706ddc32a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83233951-35db-4fae-91b6-4ebf7102e6dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "193a41ec-7f48-45bb-bbde-789455803372",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83233951-35db-4fae-91b6-4ebf7102e6dd",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "e91b2a96-077e-46b7-a8d5-c9630b1942aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "aef1c9a5-3554-418e-9f9f-5829ae3b189a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91b2a96-077e-46b7-a8d5-c9630b1942aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f967fa8-5ec2-4249-ae83-28817906b2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91b2a96-077e-46b7-a8d5-c9630b1942aa",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "16cd2e8e-1422-4e0c-9a01-644ee2f1de82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "fdaa890b-35cf-4e88-83e9-c48d4d01e758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16cd2e8e-1422-4e0c-9a01-644ee2f1de82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1055a452-6b7d-46ca-bd21-7716281c90c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16cd2e8e-1422-4e0c-9a01-644ee2f1de82",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "4e3a0b08-2d41-4239-9a21-8d3b3d3e5cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "974aa1d7-48bc-4a68-bec1-f6b3a36d7792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e3a0b08-2d41-4239-9a21-8d3b3d3e5cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9267658-2b27-4649-8a03-eb7e277cd7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e3a0b08-2d41-4239-9a21-8d3b3d3e5cdd",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "ca0037ed-ad66-4622-8462-42deef2816e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "e0658175-e5ef-48ca-a15a-23c3788d6198",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0037ed-ad66-4622-8462-42deef2816e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38b21266-7127-4685-974d-9c05a1a6aff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0037ed-ad66-4622-8462-42deef2816e5",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "dc1098c7-8c87-48d8-88ce-2eaaf84bfe13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "eae67071-6ba7-499e-a2cb-48c031d25b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1098c7-8c87-48d8-88ce-2eaaf84bfe13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed91ea1-6d3e-4093-9057-665036cd2790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1098c7-8c87-48d8-88ce-2eaaf84bfe13",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "d78e7d2b-496f-4413-9d82-526fb7e7a542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b2e186d4-12b7-4691-97f4-976cdae191e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d78e7d2b-496f-4413-9d82-526fb7e7a542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf844280-fec9-443d-a99c-d120d723039c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d78e7d2b-496f-4413-9d82-526fb7e7a542",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "ffff4f12-687d-41be-a0d9-4c7ade4fddaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b1680dda-2132-4f47-981c-a86c29e3d9e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffff4f12-687d-41be-a0d9-4c7ade4fddaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09740f29-2d89-4cc7-888f-1d7de52834a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffff4f12-687d-41be-a0d9-4c7ade4fddaa",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "f671dd0a-985a-4a6b-8824-2f0fbb4c9697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "5a93e287-3f3e-4988-9553-47e26ac08740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f671dd0a-985a-4a6b-8824-2f0fbb4c9697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d522c422-8f92-4e67-826e-ecb60b90c402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f671dd0a-985a-4a6b-8824-2f0fbb4c9697",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "3e03a07a-7cbd-4204-8416-f1abbb146524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "68f94d69-90e6-4f83-89d4-d13d5fbdb437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e03a07a-7cbd-4204-8416-f1abbb146524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f09d50ef-1e97-4492-8f20-63d9cbe33287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e03a07a-7cbd-4204-8416-f1abbb146524",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "c0bd7b3b-4301-4b1f-aeed-2e01bc3a503c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "fcfc475c-6366-4dc5-8a4f-0c6e94d2f411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0bd7b3b-4301-4b1f-aeed-2e01bc3a503c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8560ae05-0aa3-42fd-8e61-54f8eccc9c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0bd7b3b-4301-4b1f-aeed-2e01bc3a503c",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "daf6239a-2254-4011-9da4-9069a05d1e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "6ef98e2a-8f37-4ed9-89c7-17a6a95702b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf6239a-2254-4011-9da4-9069a05d1e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe465fcc-a3ea-41ee-a604-45a4cdaeba4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf6239a-2254-4011-9da4-9069a05d1e2f",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "3a1f5bca-2226-4dbb-85a5-5f5d830240ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "ab198d38-4b96-49e5-bcfb-90537a55c5fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a1f5bca-2226-4dbb-85a5-5f5d830240ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94115e80-a199-405e-a574-47be6bf281ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a1f5bca-2226-4dbb-85a5-5f5d830240ea",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "999e7ec0-15f5-4573-ad75-3f527afacb6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "c21a2fe3-05c2-4bb4-a066-983e44c5e4e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "999e7ec0-15f5-4573-ad75-3f527afacb6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50158398-bcda-49c7-85e6-bfd0080aa73c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "999e7ec0-15f5-4573-ad75-3f527afacb6e",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "287ab136-62d0-4094-bb69-841d2dc5ef78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "54c4598f-5299-41f3-971d-bd94372e7fa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287ab136-62d0-4094-bb69-841d2dc5ef78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4358fc68-0c92-4af9-8bc2-cd44fc0cd623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287ab136-62d0-4094-bb69-841d2dc5ef78",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "ad81994f-eb88-42b4-8a12-811e6d9c8123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b886250b-3bb9-4dbf-aa04-d16e36692d54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad81994f-eb88-42b4-8a12-811e6d9c8123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9326c4-f11e-48d4-9cf4-c1aa6a75cfa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad81994f-eb88-42b4-8a12-811e6d9c8123",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "1f176050-975d-4c85-a713-ac3eba0517f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "06c57462-a57b-42eb-8189-03bd89e2549e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f176050-975d-4c85-a713-ac3eba0517f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b907c3b1-8b47-4bdf-aeed-0504e48d6552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f176050-975d-4c85-a713-ac3eba0517f8",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "87ef2262-7371-4d83-9934-e73b4e35ab3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "b90e42f7-5b7d-4a29-ad2f-073c8f67dec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ef2262-7371-4d83-9934-e73b4e35ab3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87dabb3-b079-4dc1-844c-fc1a99942cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ef2262-7371-4d83-9934-e73b4e35ab3f",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "5379aa1a-4fba-4384-a404-05b937492198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "9ed60142-9f1a-4c4c-b5ab-66b2ee9f4442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5379aa1a-4fba-4384-a404-05b937492198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e31f914-72a3-4f46-81ef-700505ec0668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5379aa1a-4fba-4384-a404-05b937492198",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "c4384612-12bf-44d6-aa31-ed3f978667b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "0ffc7420-9e38-4d9e-baa9-25881931a5ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4384612-12bf-44d6-aa31-ed3f978667b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bc2295-c86a-4db1-9c72-477df4392889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4384612-12bf-44d6-aa31-ed3f978667b0",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "58932061-3dc3-4c0d-a9fd-c5aec0087cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "988c8b10-1d01-4185-a044-59583d6634ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58932061-3dc3-4c0d-a9fd-c5aec0087cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f288bb-4a9d-4dff-bcb6-8a58c093addc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58932061-3dc3-4c0d-a9fd-c5aec0087cff",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "5059ae30-b2e5-45da-bd6f-cfe064997fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "3b1814e7-95ca-4da5-ab3f-61b96df43f13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5059ae30-b2e5-45da-bd6f-cfe064997fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cce2824-2bd5-4193-bf33-398e7c9e4e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5059ae30-b2e5-45da-bd6f-cfe064997fc9",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "15ef683e-80fb-4904-ae7d-ba45c44c6087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "8d64c59e-bf46-4127-a8db-1e20545e814e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ef683e-80fb-4904-ae7d-ba45c44c6087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdfdb1be-2065-4f1a-be8d-92f13492b8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ef683e-80fb-4904-ae7d-ba45c44c6087",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "4920ff30-4ac7-4214-9a64-14e5cb64e765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "e7dde5ab-03f5-4ca6-89d6-e67e0b58c247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4920ff30-4ac7-4214-9a64-14e5cb64e765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fabf982-ce0a-4060-84a5-a5d80173b945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4920ff30-4ac7-4214-9a64-14e5cb64e765",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "c9ecf520-a273-41dc-bfb9-43b5faab45ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "143969f0-f621-4667-bd6c-147b5b0d0391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ecf520-a273-41dc-bfb9-43b5faab45ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2eb155a-7183-438a-893d-ea059d7b065b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ecf520-a273-41dc-bfb9-43b5faab45ef",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "d07f5f3b-af90-478a-ae46-625ba93bff28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "3ce96f90-3560-4ebc-931b-fef433127fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07f5f3b-af90-478a-ae46-625ba93bff28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634bda79-90e3-4a9e-a9fb-0266cec6c1b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07f5f3b-af90-478a-ae46-625ba93bff28",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "8f1b8322-66c9-4ed7-8758-3dbd76b0b04b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "32ca9e3f-0ffc-4132-9112-9286067a4bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1b8322-66c9-4ed7-8758-3dbd76b0b04b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe9b53a-f0a7-4458-9e6c-68fb37ae3841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1b8322-66c9-4ed7-8758-3dbd76b0b04b",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        },
        {
            "id": "56204970-5882-4763-8dfe-f2079a3ac588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "compositeImage": {
                "id": "288229c8-6e29-492e-8bae-ec3361ce0dde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56204970-5882-4763-8dfe-f2079a3ac588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b51143a-fe83-4eea-a898-7b46aea52ece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56204970-5882-4763-8dfe-f2079a3ac588",
                    "LayerId": "b3292b09-a809-4c0e-bef1-46a94e8acc79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b3292b09-a809-4c0e-bef1-46a94e8acc79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e9ae487-0880-466a-b8c8-afb74a77cab4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}