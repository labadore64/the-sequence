{
    "id": "5a515854-f6a3-41d3-86b6-8f97e9e0fd5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 10,
    "bbox_right": 224,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b61c9e2-804f-435f-9a5e-d5b4b2af0ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a515854-f6a3-41d3-86b6-8f97e9e0fd5c",
            "compositeImage": {
                "id": "a4d56ab9-b5dc-4b0f-83b8-9cbb88fa5e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b61c9e2-804f-435f-9a5e-d5b4b2af0ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b02ace-c842-40e2-be1c-737559c44543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b61c9e2-804f-435f-9a5e-d5b4b2af0ef3",
                    "LayerId": "7ae04cea-1aac-4085-b0c3-0684cf8140ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7ae04cea-1aac-4085-b0c3-0684cf8140ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a515854-f6a3-41d3-86b6-8f97e9e0fd5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}