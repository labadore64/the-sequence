{
    "id": "c0466b84-c109-4598-944e-716bb85972a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typeUnknown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da0b15ad-15a5-41be-ab14-9e1513f4de3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0466b84-c109-4598-944e-716bb85972a9",
            "compositeImage": {
                "id": "50515b72-c9e5-4630-b873-322ace4eb431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0b15ad-15a5-41be-ab14-9e1513f4de3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03382589-1a49-4f12-9a71-221a150a221c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0b15ad-15a5-41be-ab14-9e1513f4de3b",
                    "LayerId": "3fdd895d-ead1-48a6-8e52-450f440f4d74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3fdd895d-ead1-48a6-8e52-450f440f4d74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0466b84-c109-4598-944e-716bb85972a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ff175901-ec9d-483a-9180-48c4f474844c",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}