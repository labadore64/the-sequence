{
    "id": "14cfe7b9-b316-41a2-834f-49a3d179d396",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 25,
    "bbox_right": 237,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "389e5896-0d11-4d5f-aa37-10ae518381ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14cfe7b9-b316-41a2-834f-49a3d179d396",
            "compositeImage": {
                "id": "6dc68da4-2991-44d7-a701-2d6a942143fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389e5896-0d11-4d5f-aa37-10ae518381ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdedfe9-e241-4b40-b312-63515f1ddd54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389e5896-0d11-4d5f-aa37-10ae518381ef",
                    "LayerId": "15aa7790-717d-4daa-a9c5-4388170d2a61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "15aa7790-717d-4daa-a9c5-4388170d2a61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14cfe7b9-b316-41a2-834f-49a3d179d396",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.604,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}