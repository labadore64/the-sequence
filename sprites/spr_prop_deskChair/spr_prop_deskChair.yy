{
    "id": "46b5b999-1d91-4c15-a525-8b231c786fa6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_deskChair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 38,
    "bbox_right": 85,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22be4e51-9da8-43c3-9c78-6a0591af03a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b5b999-1d91-4c15-a525-8b231c786fa6",
            "compositeImage": {
                "id": "c74b405d-745c-400b-80d0-5caa22f2a4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22be4e51-9da8-43c3-9c78-6a0591af03a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9b9126-84ad-4667-bddc-ddbf9f36cb2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22be4e51-9da8-43c3-9c78-6a0591af03a3",
                    "LayerId": "d6996ea5-aff5-4f6e-910c-b898115c29c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d6996ea5-aff5-4f6e-910c-b898115c29c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46b5b999-1d91-4c15-a525-8b231c786fa6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 1.63400006,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}