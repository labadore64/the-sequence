{
    "id": "03a691b6-6529-4651-87f0-c3a5ba577ca9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "brailleMinigameBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f2eba1e-cc26-4a9b-b4c9-ae8068275dc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a691b6-6529-4651-87f0-c3a5ba577ca9",
            "compositeImage": {
                "id": "c02abf54-f56d-46d5-bf21-e71b56c81b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f2eba1e-cc26-4a9b-b4c9-ae8068275dc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca2b408f-83f9-4341-942b-0ee21ea93bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f2eba1e-cc26-4a9b-b4c9-ae8068275dc4",
                    "LayerId": "9df70425-ae51-42e1-a9e9-998175a4e614"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "9df70425-ae51-42e1-a9e9-998175a4e614",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03a691b6-6529-4651-87f0-c3a5ba577ca9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}