{
    "id": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fontNormal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "414f38f0-84a4-4452-bcbb-03054e717cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "df7bb1d3-a723-4546-a215-2a6579bfcaf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414f38f0-84a4-4452-bcbb-03054e717cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e936573-24f0-4123-b33c-ced6cb906a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414f38f0-84a4-4452-bcbb-03054e717cc3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "96759603-6447-41c6-b2fe-4cfa01543cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "eaba3ced-b433-4d21-9214-94f4ce9881ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96759603-6447-41c6-b2fe-4cfa01543cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "123e0a8b-a8b5-4e81-8033-7332ec36c39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96759603-6447-41c6-b2fe-4cfa01543cff",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "fc468dbd-64bb-43fe-b909-17a53924259f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "658c243f-14ad-4e2f-af65-dec4613b657e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc468dbd-64bb-43fe-b909-17a53924259f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff24c8c-ee5d-49f8-9284-08407b951f6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc468dbd-64bb-43fe-b909-17a53924259f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "66eef2b9-dff3-491b-a765-cc8f7525988e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff43d05-0db9-496c-b065-0027b02c2e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f4119c-be5b-4abe-8387-f57e283bf8f1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "159f68ad-362d-467f-8e47-ff7c25db8f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "eed060d8-dc00-4291-8952-406def7490a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "159f68ad-362d-467f-8e47-ff7c25db8f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86153f0d-17c5-456d-b265-223ebddd7382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159f68ad-362d-467f-8e47-ff7c25db8f32",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "69a0a8d5-087b-47f6-b3e4-4352790578a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3578c013-9ad3-4c87-9928-c3985d1e050e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba67f6b-70bc-4692-a942-86be1961dcc6",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "18326649-3b43-4094-b638-14b1a46c060b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "38ff6b09-f9ff-452f-91b3-4390beae0ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18326649-3b43-4094-b638-14b1a46c060b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a31ea6f-3bd7-466a-84f1-95fa976e08ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18326649-3b43-4094-b638-14b1a46c060b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bc509196-4522-4c25-bb47-bc46eee5ab03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5341d903-e675-45bb-bf5e-ae16fcde04d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07fd78a6-8f87-498e-adc5-27e61d51baa2",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "95be07a5-3f09-4240-82ad-00bbd5687de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b292c442-c43a-4e6e-bc05-58d58b586593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a77515a-ae32-4ae2-b8db-0fd63fac0a06",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "f081018b-fa92-4e83-b891-9f178e7cce66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "5ce60bac-96b1-47b2-bc19-e16daeb1e266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f081018b-fa92-4e83-b891-9f178e7cce66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e11214-e052-4ab6-a091-05a0834b4c61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f081018b-fa92-4e83-b891-9f178e7cce66",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "b6532597-138f-49c7-b4e4-454874212935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "1a6a8aa8-e85b-4be2-ae73-7ba3a47e9cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6532597-138f-49c7-b4e4-454874212935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a91b518f-2cd4-4090-8522-5a1636a56ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6532597-138f-49c7-b4e4-454874212935",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "b6619143-6d77-49f1-97de-ff151edfb98c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4c5ea8e7-482a-4d8e-bf9d-5d58b32cb896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6619143-6d77-49f1-97de-ff151edfb98c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7be0af6-8440-4aef-8e55-c8ad171da8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6619143-6d77-49f1-97de-ff151edfb98c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "161c0fd1-b49c-4e0f-af53-60754ed28e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ae6bbe-d194-4a7f-ad12-4b80f57ca051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20f102f-dc1a-40f6-87f3-280a518ebc9f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "1a5f46d3-9f7f-4a27-9d89-1f274f4743b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6ba922-f8d2-4b30-932e-c69dafb02df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20c58f1a-93de-4edf-86f9-ed9f18c66c8d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "360d78c0-a2d7-4d74-816d-52b652546f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "144367e1-8cfb-4cf0-ba08-4ed3c36e32e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360d78c0-a2d7-4d74-816d-52b652546f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9c3f15-ef53-4e09-b6bf-d0dc5de3e822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360d78c0-a2d7-4d74-816d-52b652546f3d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "95cdcd5a-298e-46fa-b227-c69c6613c926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ceec0d2-ff1e-4a6b-9ab4-fac2c9045080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a39c6b5-9576-4b47-b5df-92fe8fe1164e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "0e6331df-ecda-4fed-88ec-0c700c3fb608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e74821-dd8f-4b41-942d-65264d11f540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46ed4d9-de26-4e52-96bf-4117742bdf57",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "71c80e35-5a84-4089-a9d0-c145fd204b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6d82065-e0c3-4e28-9bd0-c1678d58b3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f1fc702-dd54-49c8-92ce-489bbd428c29",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ec67a29d-7c0c-4880-8dc2-c7cf1e3c0ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6482a0ad-bb24-425e-b8aa-3af6c61ed39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca9ae53-5d2e-40f0-bf4b-8edd43554c7a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "885233f3-ba2f-4899-ab2a-ca843759d7dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8beb556b-89fc-4ee5-aaf0-d498b76c5af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88e7cd8-99fc-4e8c-af39-876e73bb00ad",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a3edf169-3e71-41e9-9658-625eaa693ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f71c0c-2dcd-40aa-a0d0-8ccc2028626b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f522c27e-97b3-4561-8eef-4dc4e42870fa",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f4304e91-0f8e-4919-afa1-6feeed3ccb19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2357b4d-2f1c-48ca-b7f7-dcf814b2f378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388198f1-eea9-4e88-8de2-018c31e7b2fa",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "bbafe227-7d74-41e6-a161-48870c0f994d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "000ec0c0-62e9-4337-a45b-4320edc5a949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbafe227-7d74-41e6-a161-48870c0f994d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb45177-c95a-44f5-8061-7f29053e4ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbafe227-7d74-41e6-a161-48870c0f994d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "eab18968-2c95-421d-878a-63889f1978d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "18c6ddc0-be63-4bf6-ac90-f9e1cbad6b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab18968-2c95-421d-878a-63889f1978d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969a09e1-7167-4260-a9c7-458ec0634083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab18968-2c95-421d-878a-63889f1978d3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "61c47459-11f0-4af6-9c47-f87ef5a241dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b01d43-b258-42ab-a0db-0f5e02810ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacfb841-229d-4f47-b6e2-fd4b22378f76",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4cbfeae6-8661-4a3d-8dee-06bccbea66ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72fdd3ac-4e42-488b-90e9-f78c39d59e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d953dd7c-2812-4aa9-bc6f-65e23cb00660",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "96d7faed-36fe-432a-b1af-c6469ba46f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bd5f6951-ac92-4ffb-9ac7-662a8b7fcba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d7faed-36fe-432a-b1af-c6469ba46f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a75e579b-aa3e-4fd8-a7d4-6100e5d0154c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d7faed-36fe-432a-b1af-c6469ba46f25",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "17a60aba-8220-41a6-9a92-3953ea63ac0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e7dc96-a6cf-459d-94a1-718536b95d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3141c9-a64f-43d3-8f43-dbc86ebab0f1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "ee66136e-6181-4963-939b-ee25e8ba9407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4e088aa1-b864-41e1-937a-c7e4102458dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee66136e-6181-4963-939b-ee25e8ba9407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7530010-02c8-4783-8147-ae7202aeab87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee66136e-6181-4963-939b-ee25e8ba9407",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "4f825655-97d3-4bbe-8de3-81871136f21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "557067ea-d6a9-4f4e-91d6-c4c346ba5e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f825655-97d3-4bbe-8de3-81871136f21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70fee4a-0c62-4cc5-9494-4de142831ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f825655-97d3-4bbe-8de3-81871136f21e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "444e78df-a032-49c9-8621-83271fbadaa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4d38ffa5-9930-422d-8417-bb8b7ed99537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444e78df-a032-49c9-8621-83271fbadaa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2d69f5-1d9a-478e-ad5c-1d14804e3133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444e78df-a032-49c9-8621-83271fbadaa8",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "685b2d60-724b-4aee-bb36-cc52c856441e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7b775dce-b122-4ec9-9c4f-92308f0b375a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685b2d60-724b-4aee-bb36-cc52c856441e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af1d0f4b-9368-403e-9184-455fa7a9e4e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685b2d60-724b-4aee-bb36-cc52c856441e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8d645b55-fd66-46d8-ac59-c3a8adfbaf48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c8e8cb-e433-4edb-8b17-7ebe8ec70f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbae349b-12d8-4fcb-9042-4fbc6677984e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "f27a9fce-641d-4d5a-afc0-d3f4b1a77a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ef2b04-da2f-4e12-b076-cd88844fa8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f18ac78-8d67-4161-9b6a-e3fd5b1037a4",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "5b2cb454-68db-472e-8bf3-2055f307e732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8453a0cb-b6f5-4998-8080-20fec29f49c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd514c1-1d76-4ae5-8867-b6ecae2c1476",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "c6fd742e-370f-4bf7-a8e6-5733e8c40e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9f64d8c-2af0-45cb-8ff2-02af8231c5a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbdc11a-ac6b-4b7b-98e8-f85cc964643d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "25104ec9-27e3-4fc5-a1e5-a9a3ede168e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b392e78-ae0a-4b5d-b9f8-e650a2db5b21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5b2acb-2241-4661-96db-9ff58d71fbd4",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "de660742-ff20-4381-aee8-2eee9c3b92aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d1a7a8-e510-40cf-aaa8-a862951339d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebe5d12-42c8-4716-a1e8-c11ed3e748a3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "d0240b50-1df9-481d-8b8e-427b695d2250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "d1bef301-6897-4cfb-8748-ea25b7a4e69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0240b50-1df9-481d-8b8e-427b695d2250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491d88a3-f5bd-4b6a-9d2d-fcab9f2d5d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0240b50-1df9-481d-8b8e-427b695d2250",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "03c061b4-f6bb-4a3f-8226-ccd2efb73299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62aa93b5-9c9f-4ce7-9a38-a4e8c28d6966",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ec1214-c493-422c-a068-ee25ec2ab1e1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ec51b1dd-0388-49ff-9fb2-1d110284e673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30644fde-c571-4014-a859-d539bea7556d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e183aac-35a2-4991-bb7c-1774bc426f7b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "a2d222a6-8c17-4e28-9795-7e351d588110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eca81d3-e70f-43b1-a86a-e7c5a2b5a2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8468fa48-8e6f-4e39-b8a0-d17508e71d0c",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "090527eb-bbc8-4820-8b90-137f7ae0635f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "84c5e54f-e0cb-4d88-9d54-f316722fe12f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090527eb-bbc8-4820-8b90-137f7ae0635f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "216d0142-71c0-404e-a8be-63070a6e5f22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090527eb-bbc8-4820-8b90-137f7ae0635f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "6784b0c7-2a19-49f7-aa52-59e8436541d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28ed432-5078-49a2-82f2-5ca87aee0e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38f3e76d-ccd2-4f8b-8477-ee393b42a2f5",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "82c09c26-919d-4482-9827-6bfe6a7ff860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3913ba97-036d-44f4-8b82-0bbee76b12ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e9f7cc-0b47-4501-ad8c-f11b57b6fadc",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "76d1a733-6894-412b-9e7b-df0934e632ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "20d6e4af-6676-4a45-98f9-09e748527fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d1a733-6894-412b-9e7b-df0934e632ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001781b1-20e0-4c5d-88a3-52edbcd6d648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d1a733-6894-412b-9e7b-df0934e632ca",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "905f2863-854a-4d2f-8f83-ae566001ec36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7f3f65a-c706-4852-8e9e-5f8a2ce348ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b92c09a-4c24-475f-93f5-cf22986b5aa5",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ff800a40-603c-4479-9028-f8b06e56001c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74640699-0290-4fa4-8cf6-6b516f078efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c571f8d-9e46-4b4c-8d79-8e97507cfe47",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "06eaadb5-3613-4942-bcf4-6d8a51a4a2e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7866dbeb-960e-45dc-b8d6-8e02c76cd0fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ec9484-81a3-49cc-b1de-c15f5f4afef9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "72fe3477-3e47-416d-837d-e738fe5b4f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed237a9b-ab0d-458e-8498-a780d12151e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a0ad88-feb7-4890-afb3-df12a49bb3ad",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "d9d281cd-9fe0-4624-aa91-ccda765645dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f3e46ac-cf70-4b61-8167-de0698ff432a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf3f700-7bdc-434d-9aee-c5b3d29e76be",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "cdf4ad54-1eaa-4b7b-be35-1b44e665754a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8597f5-8a19-4b1b-941d-129ce64604b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e4083b-e6b7-44b1-9396-18ca6adbdbb9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "3e6cbfbe-96df-46a6-bee0-a0de872fcc33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7422547-af9a-488c-9249-e828728c546b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e55da2d-1acd-4fbe-a805-c8af1d0da43f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "152358de-b5d4-42a5-9477-1aac61e55f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb3e9510-5ddc-4b94-a0b4-e9c946d98ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ed2746-787d-4713-a92b-6dfe2c8ef47d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "3553e832-f408-444e-a407-706984b2aa80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "fd32b110-556c-4ca6-8d98-875451c8077c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3553e832-f408-444e-a407-706984b2aa80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7e4a69-adc7-4286-b923-d92b37b445ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3553e832-f408-444e-a407-706984b2aa80",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e73e4d96-604d-4eb2-b45f-7ec066bae497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "777e35a1-9497-49ad-ad2b-6490d68aa5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0811c09f-3159-41cd-86f2-f04e0cfa769b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "77ece326-f1ed-4298-bb3a-5ec99c7116f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b85d45-8542-42e8-86a2-fee58b51f496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97b62b5-4901-4b7a-b0a9-ac0d864e7107",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "d485a962-f87b-402f-a805-7ad96a85a67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "490e17ea-05be-4072-87d4-042f99049e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d485a962-f87b-402f-a805-7ad96a85a67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fad585b-7e75-4d58-925c-1667940b70cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d485a962-f87b-402f-a805-7ad96a85a67f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "798fad61-8256-4976-90be-a96a7a67190e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "fd56bc03-1adb-43a4-8353-ec7f0a89d566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798fad61-8256-4976-90be-a96a7a67190e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "153ed927-d2df-4e62-b144-92b4be5956fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798fad61-8256-4976-90be-a96a7a67190e",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "cf4eef31-b003-4379-a8fa-bf7c5e198277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3da79593-1e82-4a4b-8bf3-cdbea866f9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5977dd-3c3b-46e6-aa7c-827c62028ca9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "3d26ea54-81b5-4a35-958d-14223ea931bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "21cd4f3a-d795-4806-9ce4-58a0adf90339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d26ea54-81b5-4a35-958d-14223ea931bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095a6049-2909-4499-9789-6241e708fa41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d26ea54-81b5-4a35-958d-14223ea931bd",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "136278d2-2ca1-4d1f-9d61-9a4e9959b271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a77df8da-12b3-425a-98bc-e5ffed29060d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc67c92-5c91-47e2-ad16-6f1869594c21",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ae75c9eb-7644-4797-b8fe-ee13ec913cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be1eaaff-268b-4fd3-baf1-297aecc486fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd6c90d-9a66-4750-8165-afd73e1cb513",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "4a313577-c42b-40ac-b527-e70c64b20308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2ae494-3935-443f-8cb2-b582d6866a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053ff864-1b57-4c1a-b43f-c1b5d79314aa",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "9657e363-7f5e-4109-95a3-5b2d31295795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "afccb4dc-5b02-4592-a09d-54c095d5dd9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9657e363-7f5e-4109-95a3-5b2d31295795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4f1d51-ad5d-4948-99af-cd8842ea45d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9657e363-7f5e-4109-95a3-5b2d31295795",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "db95036e-efd3-4bc9-ae47-6e0c312b2243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24fc226a-653e-463d-9cb1-f09c83e870f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7348d60a-1cba-4ef0-9c1d-0834b0008ee1",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "d2d8e140-e4ce-45de-940f-3848223f1a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "0fb4a6a8-8ec8-4294-b990-8a8b24861d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2d8e140-e4ce-45de-940f-3848223f1a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4f2aab-9bb0-4f99-9453-f0f70f73ce50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2d8e140-e4ce-45de-940f-3848223f1a68",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "beaabd0f-2b8a-4ef6-96e3-89f9b3eb810e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ad9a2b-863d-431e-9235-839fba41ef95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d7350d-3bdd-4bd9-ae77-10f1f9bf01f7",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "41591d60-6797-41aa-aeee-49928cd81643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96a9e03-16d6-4ef2-b736-d6a519165e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49f6909-d53a-4f56-9e0c-d3d00bb2e676",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "3611edad-5353-4f21-a533-bdbc9919bb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8219feb2-9f61-4444-95fb-8161d5ca49bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3611edad-5353-4f21-a533-bdbc9919bb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27cf89e-be77-4e86-a4b1-6f9e0c5294fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3611edad-5353-4f21-a533-bdbc9919bb3f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ee37900f-1bc2-4fb8-880f-68a73a25e065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a30623a-613e-4f5b-83f5-210b04ec04f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107a1056-a3bc-4861-9c2a-91b6b5dd4291",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "8c5c8d80-dfd1-4d5b-8371-2caa49711440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13c631b-997a-4cae-a27e-841f1961847e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4202eed0-ca8f-4f15-9edd-68adac2a68f7",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "12f12d72-175e-46b6-a1b4-e59586e7297f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220e7be2-ab61-4d5c-b35d-1c3e1ac5a95f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4d39e5-17ec-4ba1-911c-75c9584957c9",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "5ce508e2-5604-4892-ac56-80c26356c39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ccb4b214-6827-4259-b2a0-52f0800f1898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce508e2-5604-4892-ac56-80c26356c39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96c6d5b-68e8-4766-b7b5-5b78ad0068ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce508e2-5604-4892-ac56-80c26356c39b",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e1af8bf3-f821-4028-aabb-0931ab3aee94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a67991-a67c-4169-8065-89e546960482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab1cd7e-607b-4ffd-a9ea-206ab653246d",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "ebf2e6c9-818b-48ca-84f0-9ed379672c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38677c7-d36d-4826-926e-a5c765fb3719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896ab5ea-6a18-49b9-ba17-58be7ea2952f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "833c2b76-f2db-4179-a7f9-90470dc98965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "e26a1b34-cbd9-4083-a99c-5ae14e578694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833c2b76-f2db-4179-a7f9-90470dc98965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40806269-895d-4d73-bc8a-62b7784e7b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833c2b76-f2db-4179-a7f9-90470dc98965",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "256b82ad-4f2d-432a-9f11-a2e47029a109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7289a8d9-d459-42b7-a14d-1b58fcf79095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256b82ad-4f2d-432a-9f11-a2e47029a109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d5ef59f-45ec-44c0-bad7-2ebc11486191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256b82ad-4f2d-432a-9f11-a2e47029a109",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "e58f7347-14e0-422d-866d-91194d6f30b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "11fd3d67-17e3-4718-8ddd-ac5af7a96e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e58f7347-14e0-422d-866d-91194d6f30b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58bc310c-9a7c-46c3-985a-b95afd873b5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e58f7347-14e0-422d-866d-91194d6f30b3",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "7bc8d77b-c38f-4172-924f-b5b88c6db620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4727eb93-6f96-41a3-b616-8a18e6b74092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafb971e-f7cf-4a05-bfb4-abf0fbbf6f5a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bde9c031-daac-492c-87ed-e97367afaca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4cea2d-9628-46b3-9217-aa2106a4268d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24558901-fe18-4ae6-bc3c-284e5540f9ab",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "ab729e3d-68f3-4907-994d-23ae430453ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bdca6fe0-2bc1-4e23-97e5-245fde0abfc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab729e3d-68f3-4907-994d-23ae430453ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ccc838-3758-45c7-82c2-45fe368cc573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab729e3d-68f3-4907-994d-23ae430453ac",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "8342820d-7e71-4ebc-927e-51d5674df89f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "84e2f0de-cefb-4ec4-849f-db4c415cd05a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8342820d-7e71-4ebc-927e-51d5674df89f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcbeda8f-2989-472b-a5e5-d63223ea319c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8342820d-7e71-4ebc-927e-51d5674df89f",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "9c5f8251-1fa5-400b-82c1-c7cc5fac390c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9abbe37d-b985-46e4-b31a-c31463c8e2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ab957c-481b-4fa5-8145-e98e8e54894a",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        },
        {
            "id": "668ea954-4ef1-477a-8db8-37b920f7d819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "compositeImage": {
                "id": "bb5a6564-92e6-42ff-bcab-604881284bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668ea954-4ef1-477a-8db8-37b920f7d819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36a7ed5-136c-4200-9195-3695fc5329fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668ea954-4ef1-477a-8db8-37b920f7d819",
                    "LayerId": "50d94fb2-b853-4446-95b9-39c8bc2ac501"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "50d94fb2-b853-4446-95b9-39c8bc2ac501",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdb4bee1-74ca-4a37-986d-22f4aa4b977e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "65af0988-cb88-42df-b650-971d782eab72",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}