{
    "id": "99e3cc97-04f8-477f-93b0-9f1ac1ff86a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 230,
    "bbox_left": 48,
    "bbox_right": 192,
    "bbox_top": 130,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cf2fcc9-48d1-42a1-b193-54bd3faaf520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99e3cc97-04f8-477f-93b0-9f1ac1ff86a1",
            "compositeImage": {
                "id": "e039b2bd-8a48-47a6-b57f-4e6cc6f7215e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf2fcc9-48d1-42a1-b193-54bd3faaf520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2547bb4c-0d67-4098-b15e-db8e68503831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf2fcc9-48d1-42a1-b193-54bd3faaf520",
                    "LayerId": "b531726b-26b7-49c0-9612-26b697850e45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b531726b-26b7-49c0-9612-26b697850e45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e3cc97-04f8-477f-93b0-9f1ac1ff86a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}