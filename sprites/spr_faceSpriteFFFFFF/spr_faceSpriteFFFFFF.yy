{
    "id": "ad93a0b8-9f5e-4ff4-a57c-b244c41a661b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faceSpriteFFFFFF",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f01b10c8-8933-4bbe-a267-36e8bee0de84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad93a0b8-9f5e-4ff4-a57c-b244c41a661b",
            "compositeImage": {
                "id": "5235ddf9-8a45-48df-aafe-5d468651edb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f01b10c8-8933-4bbe-a267-36e8bee0de84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d045c269-471e-40ea-8e02-8809ed9d5f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f01b10c8-8933-4bbe-a267-36e8bee0de84",
                    "LayerId": "085a358a-9be0-48d2-8f03-dba721838c63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "085a358a-9be0-48d2-8f03-dba721838c63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad93a0b8-9f5e-4ff4-a57c-b244c41a661b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "19b41542-e9b6-42a9-94c7-05cbadc1d852",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}