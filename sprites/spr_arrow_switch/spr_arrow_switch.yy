{
    "id": "7571bb5c-00f1-4e53-8562-d5656ca8334c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow_switch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 8,
    "bbox_right": 118,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ef130bc-bb65-4700-9fe1-d3889ffaf90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7571bb5c-00f1-4e53-8562-d5656ca8334c",
            "compositeImage": {
                "id": "2f22e509-d09d-4c8c-a88b-5557ca9b859f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef130bc-bb65-4700-9fe1-d3889ffaf90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3726a2d-a6c4-4809-b577-34ea124bd6a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef130bc-bb65-4700-9fe1-d3889ffaf90c",
                    "LayerId": "0a28318d-c0ad-4130-8868-abe82054c72c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0a28318d-c0ad-4130-8868-abe82054c72c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7571bb5c-00f1-4e53-8562-d5656ca8334c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}