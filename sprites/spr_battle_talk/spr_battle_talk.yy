{
    "id": "57616408-9367-48fd-96fb-4b98ebc0491e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_talk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 344,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3add55b2-8d68-4b80-8a53-3c048da8516f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57616408-9367-48fd-96fb-4b98ebc0491e",
            "compositeImage": {
                "id": "de218ad8-aaf1-4a17-be69-3b7f48fc0ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3add55b2-8d68-4b80-8a53-3c048da8516f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ddb5ca-9a1e-49e7-9a66-9d8bed96e159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3add55b2-8d68-4b80-8a53-3c048da8516f",
                    "LayerId": "4cabff28-c137-462f-9d20-7575b199dbce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "4cabff28-c137-462f-9d20-7575b199dbce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57616408-9367-48fd-96fb-4b98ebc0491e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}