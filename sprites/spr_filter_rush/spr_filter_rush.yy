{
    "id": "3eb581fb-154b-4339-836e-1a06b69b82d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_rush",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 82,
    "bbox_right": 172,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66781e1d-f995-49d3-abf1-9b61aa3a455e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eb581fb-154b-4339-836e-1a06b69b82d0",
            "compositeImage": {
                "id": "4066630a-0977-4b2b-998f-d84033c83f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66781e1d-f995-49d3-abf1-9b61aa3a455e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "212d3a12-f02c-47c5-a8cd-597c3c422340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66781e1d-f995-49d3-abf1-9b61aa3a455e",
                    "LayerId": "a0b4f113-4302-4a01-b6c6-4ebc6f30d788"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a0b4f113-4302-4a01-b6c6-4ebc6f30d788",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eb581fb-154b-4339-836e-1a06b69b82d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 180
}