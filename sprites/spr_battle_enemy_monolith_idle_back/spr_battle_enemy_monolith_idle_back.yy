{
    "id": "030d6a95-31be-4215-9b53-137d8daa7271",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_monolith_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 56,
    "bbox_right": 208,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbf9e97e-d31b-4b2a-b4c5-79079241b56c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030d6a95-31be-4215-9b53-137d8daa7271",
            "compositeImage": {
                "id": "2ee87a43-5725-41f5-8b48-eaddf64a61ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbf9e97e-d31b-4b2a-b4c5-79079241b56c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60195c0b-a8b6-4273-92b4-2703e0e3baba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbf9e97e-d31b-4b2a-b4c5-79079241b56c",
                    "LayerId": "216933bc-5d85-4ae2-b4e8-65320ddc5ae3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "216933bc-5d85-4ae2-b4e8-65320ddc5ae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "030d6a95-31be-4215-9b53-137d8daa7271",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "e3c3b26b-ae20-44a3-ba2c-2cd239127348",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}