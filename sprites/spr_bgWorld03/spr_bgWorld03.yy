{
    "id": "1fe6bef8-2e25-4ef1-bb93-cdef7c59af94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgWorld03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1199,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b02c8ffc-abfa-4252-a9a1-2ad1c34d9fd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fe6bef8-2e25-4ef1-bb93-cdef7c59af94",
            "compositeImage": {
                "id": "7a066ce2-1a52-45a7-b29d-b459a4aa6dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b02c8ffc-abfa-4252-a9a1-2ad1c34d9fd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a6946ee-8451-4a56-a453-c6cd500a4ad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b02c8ffc-abfa-4252-a9a1-2ad1c34d9fd1",
                    "LayerId": "36d24637-e9b1-4406-8d5c-85d1d961ee2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1200,
    "layers": [
        {
            "id": "36d24637-e9b1-4406-8d5c-85d1d961ee2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fe6bef8-2e25-4ef1-bb93-cdef7c59af94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "aca45c45-6cf3-4f13-8018-e7bdaed5cbd1",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}