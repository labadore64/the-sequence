{
    "id": "01a37d49-4df8-4303-a1fd-41b04f809fc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faceSpritePierre",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afff6bb6-a24d-4304-a1a9-6fe1d769e443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01a37d49-4df8-4303-a1fd-41b04f809fc6",
            "compositeImage": {
                "id": "a894417a-557a-4632-b37b-534a455dcd15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afff6bb6-a24d-4304-a1a9-6fe1d769e443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f04a6b04-0594-4ee9-841a-442ba62ed280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afff6bb6-a24d-4304-a1a9-6fe1d769e443",
                    "LayerId": "5057a5c2-4d2a-4eb3-b276-d877e2e2b331"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5057a5c2-4d2a-4eb3-b276-d877e2e2b331",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01a37d49-4df8-4303-a1fd-41b04f809fc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "19b41542-e9b6-42a9-94c7-05cbadc1d852",
    "type": 1,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}