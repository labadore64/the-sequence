{
    "id": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_spike_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 244,
    "bbox_left": 11,
    "bbox_right": 245,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94131ae4-d31a-41fe-9356-1f370fc2ae1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "f08eaa57-337a-492d-bcaf-06469a21a00f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94131ae4-d31a-41fe-9356-1f370fc2ae1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882df020-d882-4635-bc8a-cd744c8f7b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94131ae4-d31a-41fe-9356-1f370fc2ae1a",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "0d336a1c-0ef3-43ee-accd-69771cd8cc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "15a2e5a6-ebb9-440c-abc8-159e9df67233",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d336a1c-0ef3-43ee-accd-69771cd8cc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e900ffb8-d123-4d24-8c8f-c84324bff0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d336a1c-0ef3-43ee-accd-69771cd8cc5c",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "56e6f82e-1e1b-476a-ad94-8eb30467568b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "2c1d2159-f47b-4458-8fce-0e05cce56a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e6f82e-1e1b-476a-ad94-8eb30467568b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47b03ce-ecf8-498f-a8f9-b582bc39b476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e6f82e-1e1b-476a-ad94-8eb30467568b",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "ac6a4003-827d-4947-9739-0940ba2b7dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "e0fdfefd-8e4b-4ff1-9ec9-2e5d1a386d81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac6a4003-827d-4947-9739-0940ba2b7dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d58f0ceb-10d0-4a41-925b-9f61f0b0a03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac6a4003-827d-4947-9739-0940ba2b7dd6",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "8f99872b-3c37-4613-b62a-d2ec0c655a4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "b3a92f7a-8d9b-44c3-a25c-907d1956ba4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f99872b-3c37-4613-b62a-d2ec0c655a4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad109468-12a1-4073-8611-4ab639181ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f99872b-3c37-4613-b62a-d2ec0c655a4a",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "e1794e54-2b23-46b8-8080-f9959beeb670",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "70ebca2d-5460-4107-979b-d9f65640c5be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1794e54-2b23-46b8-8080-f9959beeb670",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b366c7b2-4a77-44e2-8906-9fc460b82769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1794e54-2b23-46b8-8080-f9959beeb670",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "d17de04d-28a3-4fca-b7cd-dfabf72233ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "af377681-fddd-4095-b13c-f6bc14d56bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d17de04d-28a3-4fca-b7cd-dfabf72233ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d190ba-27ce-43c9-bd4a-e4f6403b9ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d17de04d-28a3-4fca-b7cd-dfabf72233ae",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "51dbe695-89ef-4e1d-9d7e-152d97ed7ac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "9dca3c48-ef73-4fdc-9c17-4054e8a7e2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51dbe695-89ef-4e1d-9d7e-152d97ed7ac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6391aab6-2bc3-4d7d-9c37-fdb8692885ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51dbe695-89ef-4e1d-9d7e-152d97ed7ac6",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "855b1c09-44a5-467f-a79b-a91cb3708209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "58921b94-fb21-4d4a-8ca6-4a3d437c4455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855b1c09-44a5-467f-a79b-a91cb3708209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "163567c2-dbc6-4a54-9081-b852ac414acc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855b1c09-44a5-467f-a79b-a91cb3708209",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "1338147a-8be9-4e03-9c8c-bd5de3c9f8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "b1099fa6-f260-4afc-b78a-95a47a9efb11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1338147a-8be9-4e03-9c8c-bd5de3c9f8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9546ab36-f874-44f9-9ffe-11313edea5b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1338147a-8be9-4e03-9c8c-bd5de3c9f8ea",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "5e777868-1039-4af7-b99e-d2bec945474f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "91cf5773-724f-400d-a19a-78de375e6fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e777868-1039-4af7-b99e-d2bec945474f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4661419-a9c1-4590-a165-757201a9ab44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e777868-1039-4af7-b99e-d2bec945474f",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "13a68a5f-66e9-411c-8668-cf4c551909ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "51f150dd-0776-4a11-8047-58c04bd84707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a68a5f-66e9-411c-8668-cf4c551909ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a22890-484a-4f7d-88d7-df578ca43b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a68a5f-66e9-411c-8668-cf4c551909ee",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "83f8f7bc-3438-40ed-9a66-61454292e020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "f70f33e9-06f6-4e0a-819f-25b3fd49907e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83f8f7bc-3438-40ed-9a66-61454292e020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "908b79cd-bdf8-4eb2-8416-db208f08387a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f8f7bc-3438-40ed-9a66-61454292e020",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "c2c1a5ff-3bc4-4cc4-a879-44e01129fb7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "904fa9e1-a755-44e3-9934-6d764a8fe570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c1a5ff-3bc4-4cc4-a879-44e01129fb7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80fc35b2-4957-4ab9-bf4b-77ea3f54c3c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c1a5ff-3bc4-4cc4-a879-44e01129fb7d",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "931b3c10-e004-4f95-ba1d-94836bdb1c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "90f93c15-ccec-4f68-b784-fd7db070a4b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "931b3c10-e004-4f95-ba1d-94836bdb1c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f84665e-f2cc-4e7f-9497-e29546a1bae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "931b3c10-e004-4f95-ba1d-94836bdb1c48",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "542759e5-ddcf-4fe8-be59-32296dbf7d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "de0a4341-a7a0-4238-a53a-210720a1af4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542759e5-ddcf-4fe8-be59-32296dbf7d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eac8396-be6c-4580-913d-991b7f76b3d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542759e5-ddcf-4fe8-be59-32296dbf7d00",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "20322aeb-f200-4116-afb7-2d14cc583815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "c3769d64-04a7-4b09-ab85-7b66fc66ad19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20322aeb-f200-4116-afb7-2d14cc583815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c606d98-62f8-4d62-924e-d8c9cfa7c318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20322aeb-f200-4116-afb7-2d14cc583815",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "bcfb8755-4644-4557-9cdd-d581a5749306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "508b4671-5303-4ed2-a731-8cbc12dfa2ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcfb8755-4644-4557-9cdd-d581a5749306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07883b31-44b7-4b2c-b422-a872a8d66e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcfb8755-4644-4557-9cdd-d581a5749306",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        },
        {
            "id": "b23defdc-17c6-4709-bd7c-590bd7dd9ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "compositeImage": {
                "id": "5365bc62-b3d5-455a-83df-597801898f8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23defdc-17c6-4709-bd7c-590bd7dd9ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20d885ed-6f47-4dee-aecb-bfc0ca72d32c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23defdc-17c6-4709-bd7c-590bd7dd9ff0",
                    "LayerId": "17f66797-663a-4c5c-947c-ce083e2f8fcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "17f66797-663a-4c5c-947c-ce083e2f8fcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46b886c7-4d02-4acb-a5ab-b4697bb7725b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6535ae2f-d8ac-44c4-b49f-b65f6f1f572d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}