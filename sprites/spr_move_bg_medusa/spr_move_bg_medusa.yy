{
    "id": "1fc97acf-21a4-4a67-a1e9-04035c5766cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_medusa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 569,
    "bbox_left": 0,
    "bbox_right": 727,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eefae6a-4cf3-4585-981d-6b9e223880ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fc97acf-21a4-4a67-a1e9-04035c5766cb",
            "compositeImage": {
                "id": "f6786fd7-c784-4611-b1bc-a0ad98e0268a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eefae6a-4cf3-4585-981d-6b9e223880ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "344d1815-a4cb-42bb-8ffe-a868876e1302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eefae6a-4cf3-4585-981d-6b9e223880ea",
                    "LayerId": "9074da41-0586-4c6f-bf0a-97bb6c2196a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "9074da41-0586-4c6f-bf0a-97bb6c2196a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fc97acf-21a4-4a67-a1e9-04035c5766cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}