{
    "id": "c56f0de9-8996-47ec-a237-b6c503c87836",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_cerberpy_attack_back2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 27,
    "bbox_right": 209,
    "bbox_top": 109,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cf4a421-be66-4864-bb13-8a47bbfc1c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c56f0de9-8996-47ec-a237-b6c503c87836",
            "compositeImage": {
                "id": "150c0c74-903a-4963-87d7-114c808cd166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf4a421-be66-4864-bb13-8a47bbfc1c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ccc58b0-fb84-4ae9-9879-5ceb28103ca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf4a421-be66-4864-bb13-8a47bbfc1c3a",
                    "LayerId": "7973b579-761a-410f-8e9d-37918893ef44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7973b579-761a-410f-8e9d-37918893ef44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c56f0de9-8996-47ec-a237-b6c503c87836",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}