{
    "id": "e13f3bbf-5fe1-4c9a-ad1c-59aca86fc417",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 37,
    "bbox_right": 201,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d89d25f-fa03-4b1e-9a13-3c7d08de4919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e13f3bbf-5fe1-4c9a-ad1c-59aca86fc417",
            "compositeImage": {
                "id": "4ad03533-6e2d-498a-b937-c3b962c3069d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d89d25f-fa03-4b1e-9a13-3c7d08de4919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce3e431e-421f-4458-bdb4-bdeb1a2e7a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d89d25f-fa03-4b1e-9a13-3c7d08de4919",
                    "LayerId": "a92c90d0-2fae-4f5e-907c-0fe14657bb55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a92c90d0-2fae-4f5e-907c-0fe14657bb55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e13f3bbf-5fe1-4c9a-ad1c-59aca86fc417",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}