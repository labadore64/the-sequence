{
    "id": "864fee13-0091-4a62-a773-58dfd25c3381",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 32,
    "bbox_right": 238,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b1e4788-81c4-4757-acbf-5d6463a9b9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864fee13-0091-4a62-a773-58dfd25c3381",
            "compositeImage": {
                "id": "2fc58137-4a7b-4756-88f2-3d5cfa1e5a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1e4788-81c4-4757-acbf-5d6463a9b9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7148d7a3-564f-4549-b7e2-67630e862bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1e4788-81c4-4757-acbf-5d6463a9b9cc",
                    "LayerId": "41170af3-84d1-4180-ae3a-e054d6af8d41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "41170af3-84d1-4180-ae3a-e054d6af8d41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "864fee13-0091-4a62-a773-58dfd25c3381",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}