{
    "id": "ee2a29f8-f686-4550-914d-0631f1bc4640",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 243,
    "bbox_left": 28,
    "bbox_right": 214,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e857e91-52d5-4487-b43f-b47a4cd13d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee2a29f8-f686-4550-914d-0631f1bc4640",
            "compositeImage": {
                "id": "2fbd3bae-1765-421b-a3fe-514e487e81d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e857e91-52d5-4487-b43f-b47a4cd13d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "618ce411-414e-4c51-9aff-1c3555067a4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e857e91-52d5-4487-b43f-b47a4cd13d35",
                    "LayerId": "e7880442-4d64-4778-9ea2-fe89e6605d4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e7880442-4d64-4778-9ea2-fe89e6605d4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee2a29f8-f686-4550-914d-0631f1bc4640",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}