{
    "id": "2cb915fc-5651-4292-8f28-78e4358d9775",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 30,
    "bbox_right": 203,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc63c865-36e8-458a-9fa4-5988d6b72737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cb915fc-5651-4292-8f28-78e4358d9775",
            "compositeImage": {
                "id": "0c98540d-56ee-49fa-8614-0a667aabef88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc63c865-36e8-458a-9fa4-5988d6b72737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e596318f-905b-40e3-98aa-54c4b127c108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc63c865-36e8-458a-9fa4-5988d6b72737",
                    "LayerId": "d0966d29-c768-4ffd-b125-9f49811f9d90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d0966d29-c768-4ffd-b125-9f49811f9d90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cb915fc-5651-4292-8f28-78e4358d9775",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}