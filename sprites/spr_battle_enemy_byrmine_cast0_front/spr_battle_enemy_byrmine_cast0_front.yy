{
    "id": "a25a0bf7-9dea-4e31-9c64-82140c632b36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 51,
    "bbox_right": 215,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e514fde5-3ed1-4739-ac15-7f526a6c20ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25a0bf7-9dea-4e31-9c64-82140c632b36",
            "compositeImage": {
                "id": "ca227260-7ce5-474a-85ad-dfeb13d5501f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e514fde5-3ed1-4739-ac15-7f526a6c20ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b7092f7-cfe3-49a7-abda-3d5f89ad1b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e514fde5-3ed1-4739-ac15-7f526a6c20ad",
                    "LayerId": "d5c9be6e-1d8c-4471-93f8-685b75ac8222"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d5c9be6e-1d8c-4471-93f8-685b75ac8222",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a25a0bf7-9dea-4e31-9c64-82140c632b36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}