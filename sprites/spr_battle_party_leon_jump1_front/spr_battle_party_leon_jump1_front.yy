{
    "id": "08329fe1-a7be-49f3-b157-77e52b8fd1c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 13,
    "bbox_right": 214,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b162967f-0881-4ef2-823d-84cbd8cb50be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08329fe1-a7be-49f3-b157-77e52b8fd1c1",
            "compositeImage": {
                "id": "6bf834b7-7eb3-4a9f-bc53-9c308f75ab9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b162967f-0881-4ef2-823d-84cbd8cb50be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a6b668-266c-4aeb-98c7-fd226aca4971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b162967f-0881-4ef2-823d-84cbd8cb50be",
                    "LayerId": "1c4778e7-b079-417f-aa15-9da8f4821c99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1c4778e7-b079-417f-aa15-9da8f4821c99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08329fe1-a7be-49f3-b157-77e52b8fd1c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}