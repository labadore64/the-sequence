{
    "id": "663501f4-94ef-4ff6-97a3-83729251550f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_bruce_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 3,
    "bbox_right": 201,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83c8639e-de43-44bd-87c0-23f9bf5cabd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "091783df-7740-4395-af0f-e90724c1b2a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c8639e-de43-44bd-87c0-23f9bf5cabd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4381ec94-7c59-400c-b781-9af9f7af8df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c8639e-de43-44bd-87c0-23f9bf5cabd9",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "ea4a0fef-735e-4a11-98de-a2105604c223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "5590f836-12e9-4d77-8f26-f0d70a562c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea4a0fef-735e-4a11-98de-a2105604c223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02b4b567-19fc-4d0f-b3a1-525479f3d152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea4a0fef-735e-4a11-98de-a2105604c223",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "81eb4538-c759-4f3a-98c6-1f41c3be8cfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "989bb790-554c-4866-8140-866b65ba9e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81eb4538-c759-4f3a-98c6-1f41c3be8cfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4691e4-b8e1-4481-b6ba-9f5662baf887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81eb4538-c759-4f3a-98c6-1f41c3be8cfe",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "932a62e4-696f-44cf-a26b-aa2c9e232150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "fee22cc2-6172-489f-9a35-6bcf8dba61e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "932a62e4-696f-44cf-a26b-aa2c9e232150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e11d528-204d-4012-b12d-d07e01e7dbeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "932a62e4-696f-44cf-a26b-aa2c9e232150",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "d0044001-e6ed-484f-8d73-a87b896ec48f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "b41f6611-6770-4120-88b8-12034d2c718c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0044001-e6ed-484f-8d73-a87b896ec48f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e76ae995-d5ef-4ee3-97cb-3b57afbb23bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0044001-e6ed-484f-8d73-a87b896ec48f",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "93672015-8dc4-4311-bbfa-6f87e330113f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "5ab3cf99-26e5-4b6c-a64a-a5859ff1d1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93672015-8dc4-4311-bbfa-6f87e330113f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443346c6-1fac-4231-b141-2b81650c946d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93672015-8dc4-4311-bbfa-6f87e330113f",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "010e51e7-5232-436c-abbc-93fedf9592ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "0f72ffc3-d008-4070-b25a-48c37a3803dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010e51e7-5232-436c-abbc-93fedf9592ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150ebfcd-6c8d-4078-9487-3ae4d9f932a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010e51e7-5232-436c-abbc-93fedf9592ff",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "181cff3c-20e9-4e68-8f1e-40744022558f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "fba79247-bbc4-4043-831f-20b1ba2d9dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "181cff3c-20e9-4e68-8f1e-40744022558f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73bc48a3-e58d-49c6-a578-6eaad3a89cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "181cff3c-20e9-4e68-8f1e-40744022558f",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "d359a104-62ba-427b-8feb-08f23e027b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "beb574c0-4c0d-4289-9b62-943a894183cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d359a104-62ba-427b-8feb-08f23e027b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8c57b67-046d-4c3a-ae9a-31942d1e352d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d359a104-62ba-427b-8feb-08f23e027b9f",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "f42232e2-27e9-4895-8fd0-68f6f2774c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "8f9a3d5c-2d0a-4105-8a88-3e246cb6c3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f42232e2-27e9-4895-8fd0-68f6f2774c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "009aa3f9-8cc0-44ac-a76d-cfd0855938c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f42232e2-27e9-4895-8fd0-68f6f2774c5c",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "6a49f0bb-c4d1-4436-830f-f77bc02606b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "57b87153-d78f-4a71-937c-12546c55f894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a49f0bb-c4d1-4436-830f-f77bc02606b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567914c7-d0ab-4580-bb44-5a0e9f9aebb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a49f0bb-c4d1-4436-830f-f77bc02606b7",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "c1d8b171-cd33-4208-8939-d15631c50811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "caae4eb5-2935-44ca-8270-ab4665780465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d8b171-cd33-4208-8939-d15631c50811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68800d2b-a126-4a58-ab9c-c58439d9adb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d8b171-cd33-4208-8939-d15631c50811",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "ec3a0fb3-1a1f-4ad9-9459-b823d771e34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "002c7d62-6570-4ca6-9baf-822ddb266119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3a0fb3-1a1f-4ad9-9459-b823d771e34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87bdefe8-d6c9-4eb5-b52a-50bfff785ab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3a0fb3-1a1f-4ad9-9459-b823d771e34a",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "1bd8c6bd-3bc8-4010-84cd-30a463265f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "467a55df-790a-4529-a0c0-db38ae97eff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd8c6bd-3bc8-4010-84cd-30a463265f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8bc5e8-08fc-4263-a1f9-eed1e34f9395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd8c6bd-3bc8-4010-84cd-30a463265f25",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "922be101-d155-4558-b530-fb6153a632d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "f0f71318-ced8-4786-9ae5-87ca5281ee1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "922be101-d155-4558-b530-fb6153a632d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d29a1be-86fb-4e64-bd9b-81257f6c4283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "922be101-d155-4558-b530-fb6153a632d5",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "ff796454-d951-4d49-a433-35c0fc85207d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "051aabfe-ca61-4e01-a143-62fa9c00da3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff796454-d951-4d49-a433-35c0fc85207d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b2b90b-b3c4-484d-8eb4-db5afc4047e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff796454-d951-4d49-a433-35c0fc85207d",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "e0f1d132-bc9e-4457-845f-e10275cca8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "91f3a768-a86d-4178-a44b-9c03f92c6ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f1d132-bc9e-4457-845f-e10275cca8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f537de3-ae24-4d1b-af8a-086a0f1bae2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f1d132-bc9e-4457-845f-e10275cca8ad",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "4c4a4ffc-680a-41c4-99bd-e25255938cfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "e77c656e-f065-408d-ace4-f09a4dfee221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4a4ffc-680a-41c4-99bd-e25255938cfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a36a0736-5c2d-4b5f-9c23-994cfeff3e09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4a4ffc-680a-41c4-99bd-e25255938cfb",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "4be5f166-220a-498b-8989-be5fc0683d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "c1275113-ee1d-4c07-bf58-b2d03c04a3a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be5f166-220a-498b-8989-be5fc0683d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0aacb74-37ec-4d1c-85aa-4e9d71837ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be5f166-220a-498b-8989-be5fc0683d12",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "da71dea3-d259-4521-a981-dbee50ecf1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "7054dfaf-c445-4596-bd34-8dece9a14bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da71dea3-d259-4521-a981-dbee50ecf1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de31a65d-c8a4-4936-9230-b34eaabe8a0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da71dea3-d259-4521-a981-dbee50ecf1a3",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "ec63387a-5a99-4d02-a4ab-5b9dcd6b2114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "e3b937af-197f-4521-bf2e-106abcef8483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec63387a-5a99-4d02-a4ab-5b9dcd6b2114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2fdf70b-7a9b-438b-81ed-1677c05b0cf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec63387a-5a99-4d02-a4ab-5b9dcd6b2114",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "a2a1cf0b-54db-4c97-bde6-dde11004f250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "3917d25a-a04d-45e9-8c98-b33226727eb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2a1cf0b-54db-4c97-bde6-dde11004f250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25234b0b-027c-4bc1-b803-048eb4e0f273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2a1cf0b-54db-4c97-bde6-dde11004f250",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "488ea0cf-b882-4be6-98c2-e18d385073ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "ef156160-2288-403e-b404-0147861249d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488ea0cf-b882-4be6-98c2-e18d385073ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8f457b-cb0a-4d64-b8a9-e62b346f3c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488ea0cf-b882-4be6-98c2-e18d385073ae",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "7a4d9166-d47e-4aa3-a7fa-b32d2ed72ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "919ba1df-6218-486a-a534-39b631d118e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4d9166-d47e-4aa3-a7fa-b32d2ed72ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b2c92e9-9d3c-4f50-a494-93b1c7cd3664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4d9166-d47e-4aa3-a7fa-b32d2ed72ae4",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "7f8c9999-d1e9-45b6-9359-ceaadfadb7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "a6d3bfc4-f59d-4b17-9932-8643aa7fd19b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8c9999-d1e9-45b6-9359-ceaadfadb7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a301255-b0a0-4544-b10a-7f07e1eeba33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8c9999-d1e9-45b6-9359-ceaadfadb7e8",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "9ddfa3f3-2514-41b7-935a-60a1b1821f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "934827ed-d94c-40d3-b1fd-a151ffa595c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ddfa3f3-2514-41b7-935a-60a1b1821f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bf7e684-6e47-439f-86a0-7dafb06151a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ddfa3f3-2514-41b7-935a-60a1b1821f61",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "bf72bc73-f84e-4cba-b787-fcd790d1728d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "68d45adb-f487-44fe-82f1-6accbe2864df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf72bc73-f84e-4cba-b787-fcd790d1728d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b466184a-d65c-44f2-abda-c41597d42948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf72bc73-f84e-4cba-b787-fcd790d1728d",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "80319f8d-9168-4bfc-be36-39efc794225d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "73a7bd4d-c32f-421a-b4ae-63c24d3d9178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80319f8d-9168-4bfc-be36-39efc794225d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d2c93c-76e8-48b6-99ba-fdbb07103b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80319f8d-9168-4bfc-be36-39efc794225d",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "fdc44cbd-4f4e-477c-875c-0f26baa9d3b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "cba79115-a63b-423d-b1f4-f6c8a187592f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc44cbd-4f4e-477c-875c-0f26baa9d3b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75def98-f5a2-4999-9886-c9382d904497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc44cbd-4f4e-477c-875c-0f26baa9d3b4",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        },
        {
            "id": "198261f9-b31d-4ffc-8cc8-771ec4da09e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "compositeImage": {
                "id": "c832d9f2-6077-4543-911b-37835d07b842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198261f9-b31d-4ffc-8cc8-771ec4da09e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a6e7dc-49e2-4a87-85f3-ff3fcb4257f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198261f9-b31d-4ffc-8cc8-771ec4da09e3",
                    "LayerId": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fcbdfcca-0c76-4f6d-9637-1561a253cbf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "663501f4-94ef-4ff6-97a3-83729251550f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "885781c6-d6ce-4320-b0de-d7a48d43760d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}