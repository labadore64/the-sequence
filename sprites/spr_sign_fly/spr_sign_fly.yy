{
    "id": "762528c9-5580-4206-b6d7-09928a4254ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 1,
    "bbox_right": 127,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93c6f146-d8ad-46c3-b637-bef9f85166b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "762528c9-5580-4206-b6d7-09928a4254ee",
            "compositeImage": {
                "id": "f2fcb202-cc88-4b84-a83f-89c29e0b7900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c6f146-d8ad-46c3-b637-bef9f85166b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9ec211-c32a-4833-9f1d-a5b6e59c6df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c6f146-d8ad-46c3-b637-bef9f85166b2",
                    "LayerId": "ca8d6728-3c16-4939-b333-1005f24f24f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ca8d6728-3c16-4939-b333-1005f24f24f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "762528c9-5580-4206-b6d7-09928a4254ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}