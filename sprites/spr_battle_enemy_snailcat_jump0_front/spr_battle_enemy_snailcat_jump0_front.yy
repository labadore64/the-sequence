{
    "id": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 7,
    "bbox_right": 200,
    "bbox_top": 128,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "868b7cb1-d8b0-4bdb-be2d-ddf3ce757564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "d967fa85-4dbe-4b8d-bdc0-5c0b57716337",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "868b7cb1-d8b0-4bdb-be2d-ddf3ce757564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e780e007-b282-40a5-9417-64cf3339ea3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "868b7cb1-d8b0-4bdb-be2d-ddf3ce757564",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "2e9908a9-e971-4c1f-92ef-a280143f9071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "7edd3f6f-e84e-442b-b51a-0942e6cf88b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9908a9-e971-4c1f-92ef-a280143f9071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b576118-6e85-4800-9b0b-de904ea81256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9908a9-e971-4c1f-92ef-a280143f9071",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "4f2232df-855d-4b09-85ec-1ef7d639dc8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "4876e090-1cb1-4376-ba96-19f25d17f564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2232df-855d-4b09-85ec-1ef7d639dc8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae3920bd-479b-4a92-a540-1ebbd0e17c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2232df-855d-4b09-85ec-1ef7d639dc8f",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "4478806f-db3d-4541-9443-27a9c197d162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "f6620f8e-6f7f-4b07-8a94-98f1ed1fa582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4478806f-db3d-4541-9443-27a9c197d162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2001370-6793-4ac7-9be9-4860e8150330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4478806f-db3d-4541-9443-27a9c197d162",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "5a2af0f8-eca0-46eb-baf5-03c3f6821682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "1d2fa796-3274-4f18-83a0-a8cd572c377a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2af0f8-eca0-46eb-baf5-03c3f6821682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b144b3-6b97-4cfa-a20b-f56095fc5525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2af0f8-eca0-46eb-baf5-03c3f6821682",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "5ee9178f-6bcd-4444-9125-4b4da802792b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "9743605c-0d4a-44b1-9d9e-607ac5608292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee9178f-6bcd-4444-9125-4b4da802792b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "390c805c-cefc-47a3-9884-7a0d17b4db5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee9178f-6bcd-4444-9125-4b4da802792b",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "6cb67668-711f-42c7-8cbb-73ed82dfdc44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "bd5207ad-7488-458b-8443-d7f732653d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb67668-711f-42c7-8cbb-73ed82dfdc44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a1c7fc-28b5-4372-999f-a40caacbc3e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb67668-711f-42c7-8cbb-73ed82dfdc44",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "3f56d10d-f3f2-4ce5-8c71-0dff11d9d353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "738b3829-dd7e-4071-9399-35c34c4f91ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f56d10d-f3f2-4ce5-8c71-0dff11d9d353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "314d22cc-d4a7-4ea9-9466-d61c76f84692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f56d10d-f3f2-4ce5-8c71-0dff11d9d353",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "085e9d2b-f431-4484-a08d-517e23a5c08b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "8703df0f-f8c9-4a82-8291-8af416001d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085e9d2b-f431-4484-a08d-517e23a5c08b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e143cce-4422-4b3b-ae0b-c741d6b1d9f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085e9d2b-f431-4484-a08d-517e23a5c08b",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "aab32adb-fec5-4d7a-a2ad-7c836ad372f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "575e2987-3884-43d2-ac35-a0d9ba7c4d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab32adb-fec5-4d7a-a2ad-7c836ad372f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5763266c-af2e-40d2-99a8-25b3adf2c848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab32adb-fec5-4d7a-a2ad-7c836ad372f3",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "4b5fe35f-87c2-4733-96ef-ff2466f202f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "333b86c4-ad7d-49d0-8a9d-83114fc9a71a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5fe35f-87c2-4733-96ef-ff2466f202f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08eb04b9-a598-451a-8334-a9021fed3cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5fe35f-87c2-4733-96ef-ff2466f202f9",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "26699416-cb42-454e-aa5d-bf33d64c643a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "f84b9d67-f84f-41d7-affb-e99c2388e7a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26699416-cb42-454e-aa5d-bf33d64c643a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cff2aeb-b5bb-4ae3-b295-a21dd1d128ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26699416-cb42-454e-aa5d-bf33d64c643a",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "6fa0cb09-f448-4330-aec1-6aa3744ce2e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "e5331dff-c1c1-490a-b63f-3a579f833e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa0cb09-f448-4330-aec1-6aa3744ce2e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c14ef68-0e18-4aac-98d7-fece9f2ca712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa0cb09-f448-4330-aec1-6aa3744ce2e9",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        },
        {
            "id": "34623192-2eb3-4f7e-bd82-f226ce8fc0c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "compositeImage": {
                "id": "06705044-58d1-4a69-8bc9-39e94d67795a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34623192-2eb3-4f7e-bd82-f226ce8fc0c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9fe990-e6bc-4852-991b-f305543aedf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34623192-2eb3-4f7e-bd82-f226ce8fc0c2",
                    "LayerId": "fcf121f8-0286-43f3-ab03-5eafd4dadedf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fcf121f8-0286-43f3-ab03-5eafd4dadedf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5750c8a0-4f15-46a3-a91c-4c835b8d7b3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}