{
    "id": "4dea2db9-4e45-422c-a982-5cc197afa25e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 210,
    "bbox_left": 2,
    "bbox_right": 242,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "167abd91-984e-449e-af41-f90c64c8d044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "06eb3dd4-7bc8-442f-be77-ec8418e0ad26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167abd91-984e-449e-af41-f90c64c8d044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf6b8c4-8632-4b2e-ab7c-707afc967def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167abd91-984e-449e-af41-f90c64c8d044",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "79ba9963-571d-495c-bc2c-a0c811b6410a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "7a671d21-07fd-4ea8-b1f8-0c9d17715b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ba9963-571d-495c-bc2c-a0c811b6410a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc2a168-c108-4634-b9ad-63c48ad8f78b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ba9963-571d-495c-bc2c-a0c811b6410a",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "bf09fe8f-5ea1-4d26-993a-0586d554d900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "155a1c29-956a-426d-9b25-b609b9e78038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf09fe8f-5ea1-4d26-993a-0586d554d900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e74d05-4bb1-4dbc-9e4e-8c593b1688a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf09fe8f-5ea1-4d26-993a-0586d554d900",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "b412bd75-aa8f-43b1-8d3f-692e2ab32071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "d6ed2062-8e81-4b20-868d-f21cdfd6f444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b412bd75-aa8f-43b1-8d3f-692e2ab32071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "682d148d-04ea-4729-8d44-bfe1450aa25f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b412bd75-aa8f-43b1-8d3f-692e2ab32071",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "d321c30c-6e2c-477d-bae7-32ea1030f106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "322579d3-18b8-497f-acf7-653ea3e9883c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d321c30c-6e2c-477d-bae7-32ea1030f106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1287542f-cd6a-46dc-89fb-8c989f9c0644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d321c30c-6e2c-477d-bae7-32ea1030f106",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "b5933236-b39b-4680-9cdc-dca1ef460c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "4c8ba90d-7a50-4746-a8e1-6011703ffd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5933236-b39b-4680-9cdc-dca1ef460c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674899ed-6a93-42cc-aa57-08330fbcd677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5933236-b39b-4680-9cdc-dca1ef460c84",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "ba0b4ada-686d-412e-98ac-ab28c99b218f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "1e7fc831-d45d-435d-9f44-7a7fccf5bff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0b4ada-686d-412e-98ac-ab28c99b218f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2553f5-2161-4b23-b822-b3a31ac621a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0b4ada-686d-412e-98ac-ab28c99b218f",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "713171df-bb20-4c29-82b8-58920098d9e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "af9b4f9b-ee49-456a-9394-7a6d7eaab384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "713171df-bb20-4c29-82b8-58920098d9e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c353b5c-93f0-48d3-ad14-affc2ff0e816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "713171df-bb20-4c29-82b8-58920098d9e5",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "357da97f-5e01-4c5c-8b22-41a7b49544ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "7201f23a-0fe7-414c-84a3-98a1a4a0f99a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "357da97f-5e01-4c5c-8b22-41a7b49544ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e00680-9472-4b1b-9bdb-c021cd6b5baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "357da97f-5e01-4c5c-8b22-41a7b49544ed",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "3a4e70fc-0f16-4319-bf1c-41f5ff62fcff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "50e47e89-51fc-4660-a6fb-46b567c8160b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a4e70fc-0f16-4319-bf1c-41f5ff62fcff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac1875f-6f63-4374-8a61-0cf97dff7e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a4e70fc-0f16-4319-bf1c-41f5ff62fcff",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "5b4f5779-e30c-49dc-a462-08de3455570c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "fe3c7687-ccdd-4e52-b282-590a3ea0e839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4f5779-e30c-49dc-a462-08de3455570c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689b2703-6246-4fc7-9e1e-83273820a664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4f5779-e30c-49dc-a462-08de3455570c",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "06aadba5-a77a-456d-b8b0-42410f945fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "8669a3f5-e272-4d5e-a123-8098d315cab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06aadba5-a77a-456d-b8b0-42410f945fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d48913c-5026-4974-9b9b-9a0813c5fed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06aadba5-a77a-456d-b8b0-42410f945fe1",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "1953eb7d-222d-4e94-ba7b-3ad4d2f5b1fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "bd263862-2dcd-4285-b886-232069c40719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1953eb7d-222d-4e94-ba7b-3ad4d2f5b1fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750cc6b9-054f-4ec8-9c6e-be15a3bfd6ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1953eb7d-222d-4e94-ba7b-3ad4d2f5b1fd",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "1d58dd80-cd18-400b-9c70-2b368da78304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "64e36611-8e39-494a-96ca-fc8bb1ee1d7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d58dd80-cd18-400b-9c70-2b368da78304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81deb23d-bb17-4e0a-abcd-9df6a65cf521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d58dd80-cd18-400b-9c70-2b368da78304",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "bc959ced-b1b0-4514-9df2-c11f86450fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "16f3cbc6-61ae-471f-a695-ed708e0b14d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc959ced-b1b0-4514-9df2-c11f86450fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd37725-1a03-4c52-a53f-dc80e2ddeda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc959ced-b1b0-4514-9df2-c11f86450fac",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "0f893935-d108-44ac-8385-93f6764d0404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "2a3e9e7d-239b-4ecb-9f99-b837e0693972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f893935-d108-44ac-8385-93f6764d0404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acfddfd9-1bc6-4cee-8074-89f059d92a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f893935-d108-44ac-8385-93f6764d0404",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "d24ae7c0-a1b0-4f2b-ba21-d58357bcaed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "f7fbd7cb-aa43-443f-aaec-fc0923422972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d24ae7c0-a1b0-4f2b-ba21-d58357bcaed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d0b649-6ab3-43a1-83b6-a19ec24f1f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d24ae7c0-a1b0-4f2b-ba21-d58357bcaed4",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "abeb5df2-9af8-42a6-9822-11a77b288cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "97eef4b5-63ee-445f-b396-070ae7b673b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abeb5df2-9af8-42a6-9822-11a77b288cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ad1097-c8de-406c-a080-16e5cd64a226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abeb5df2-9af8-42a6-9822-11a77b288cc4",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "cea73a9c-153c-4f10-b632-c0cdaf4ff5dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "92de0ce1-d2ad-4de7-ac15-ca0b1dde635c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea73a9c-153c-4f10-b632-c0cdaf4ff5dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3589d920-3f1a-42bb-8abb-fa614a781a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea73a9c-153c-4f10-b632-c0cdaf4ff5dd",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "bec8f722-a39c-416b-b79f-956abaecaed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "3b8ef2d0-b6f1-4d45-8783-c97613152d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bec8f722-a39c-416b-b79f-956abaecaed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "381dd6a2-a5e5-4699-a7cc-891a89403574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bec8f722-a39c-416b-b79f-956abaecaed8",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "feb0f564-4600-4d31-a848-51a9f912ab5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "2b99320a-857e-4d28-833b-2d20e2af41b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb0f564-4600-4d31-a848-51a9f912ab5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0844d740-084e-4190-93ce-26acd238b93e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb0f564-4600-4d31-a848-51a9f912ab5f",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "d903ac89-cede-418f-a59f-9e7ff525080a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "9b8fd9fa-248b-49bd-99d2-394253a85333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d903ac89-cede-418f-a59f-9e7ff525080a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d06086cc-70a9-444e-8493-28dd5e7f3174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d903ac89-cede-418f-a59f-9e7ff525080a",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "8c8c3f4d-a12a-4dbf-bed2-9d42261c1f18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "d38d66fd-c5f1-4ab5-ab12-cd135ed54772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8c3f4d-a12a-4dbf-bed2-9d42261c1f18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f827e9-4618-4b55-9f87-f48ead35c76d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8c3f4d-a12a-4dbf-bed2-9d42261c1f18",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "70529cdc-1a47-46aa-910f-df44d567416b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "a563e723-8778-455e-b8b6-1e568a091b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70529cdc-1a47-46aa-910f-df44d567416b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072377b3-61d6-416f-91b1-7fcab667c030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70529cdc-1a47-46aa-910f-df44d567416b",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "d5977fec-5244-4360-98e3-dd57a72616e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "c3154585-71d9-4ee6-a216-8d6ec788527e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5977fec-5244-4360-98e3-dd57a72616e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99c04409-9a5e-4d6e-963d-7acd50eac6ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5977fec-5244-4360-98e3-dd57a72616e5",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "af96664f-7a74-4fce-b75b-dd51218ad9e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "dc7bf19a-c812-44ed-bfb9-0b2ff95e5721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af96664f-7a74-4fce-b75b-dd51218ad9e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e1c5cd5-296d-4a06-856d-53a93c6063f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af96664f-7a74-4fce-b75b-dd51218ad9e6",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "7e30e67e-8f4e-43bf-afda-9ecc6fd54390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "a5d47afb-959a-443d-b9e4-7a692f1a3b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e30e67e-8f4e-43bf-afda-9ecc6fd54390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce6b64b5-3571-4ab6-99e2-6334b9a2cd78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e30e67e-8f4e-43bf-afda-9ecc6fd54390",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "4a288335-3124-46bf-9baf-dc7c7fead222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "d731eb80-2d66-4b64-9032-2fa7c7460479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a288335-3124-46bf-9baf-dc7c7fead222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8316419c-10f3-42f1-983c-1b1a1d10333e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a288335-3124-46bf-9baf-dc7c7fead222",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "f857316e-0a32-4696-affc-9a2dc77933ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "036cbd42-7e1b-4490-b0e9-9ebeffa01a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f857316e-0a32-4696-affc-9a2dc77933ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc78d9e-0698-41c9-97f0-a5a6502110c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f857316e-0a32-4696-affc-9a2dc77933ee",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "b32ececf-10e9-46ca-98c6-74547f92a673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "6d820b6b-e1e9-4011-8253-22cd8644d609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b32ececf-10e9-46ca-98c6-74547f92a673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "105e96ce-804a-400d-aeff-bd324359eaa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b32ececf-10e9-46ca-98c6-74547f92a673",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "feffa502-366a-4f03-8ae3-b0a6451a5d21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "f078b065-247a-4692-af69-932e76dd58e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feffa502-366a-4f03-8ae3-b0a6451a5d21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa9c9b1-128f-424c-b02b-88ebbad2e2e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feffa502-366a-4f03-8ae3-b0a6451a5d21",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "60e38ad9-a22a-46d0-88bf-d9110b895647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "9ffcc03f-527f-4e82-a35a-71c950a51b31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60e38ad9-a22a-46d0-88bf-d9110b895647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1dd6ff3-14e3-4da1-b0f5-8f4576fedbd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60e38ad9-a22a-46d0-88bf-d9110b895647",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "bb27957e-fdbc-4f70-bfa5-7261870d46e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "9e7aea64-ef03-486b-92b6-671d603ecea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb27957e-fdbc-4f70-bfa5-7261870d46e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed9fa26-0858-4e9f-98e0-75f36aa06e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb27957e-fdbc-4f70-bfa5-7261870d46e6",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "c82ffb48-a5bd-4f85-ac4b-f7558d7b7f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "bb9c6f9a-b493-4cd2-becf-8710123d266d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82ffb48-a5bd-4f85-ac4b-f7558d7b7f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c9d30e7-f5b1-4e10-8a10-b7ffd4b32551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82ffb48-a5bd-4f85-ac4b-f7558d7b7f63",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "f1094388-86a0-49c6-b646-3c9b8b277221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "86e41e74-b42a-4ceb-99fe-f9f16c6a1fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1094388-86a0-49c6-b646-3c9b8b277221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad96e80-ff8a-4710-ade3-c957eec2e170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1094388-86a0-49c6-b646-3c9b8b277221",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "d1adb78d-68c6-42bb-b02e-fa84999d8111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "8e85dd89-50e1-4667-a954-bdd125c47af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1adb78d-68c6-42bb-b02e-fa84999d8111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c6dee0d-30d0-4aa2-a09b-95ee5424b330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1adb78d-68c6-42bb-b02e-fa84999d8111",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "3b9be65d-31c8-4abf-9671-a915e40ca215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "1abdcc08-e767-48ea-99b2-6bdc60d87edc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b9be65d-31c8-4abf-9671-a915e40ca215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db970160-465f-4359-b884-b60e1aa35c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b9be65d-31c8-4abf-9671-a915e40ca215",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "28ed19aa-2b99-44d6-99e9-54a0297836f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "1e6695b2-6e53-4691-a78f-325a80cfc75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ed19aa-2b99-44d6-99e9-54a0297836f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f040c55-afc7-4fe1-9bef-ca4c4492fc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ed19aa-2b99-44d6-99e9-54a0297836f0",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "891e3f21-74f3-4a1f-889c-3b8e0dd121dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "5cca7b47-5683-4201-a647-8a3939de6085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "891e3f21-74f3-4a1f-889c-3b8e0dd121dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e61ffc-f392-4733-b7da-c8605082031c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "891e3f21-74f3-4a1f-889c-3b8e0dd121dc",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "66eb9e21-58cd-4da0-82ff-039e2c2d71ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "4e350b4c-a138-46e6-a662-174e0139af88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66eb9e21-58cd-4da0-82ff-039e2c2d71ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d903de55-85f2-4514-be1c-96815a59902e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66eb9e21-58cd-4da0-82ff-039e2c2d71ca",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "3bc61b60-c110-4f3d-bf64-05acf6156702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "f59cd2b2-5728-44bf-bac7-7f538953b3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bc61b60-c110-4f3d-bf64-05acf6156702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f28e4150-fc84-420c-af56-61f07370552c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bc61b60-c110-4f3d-bf64-05acf6156702",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "214c583c-0691-42e9-8a23-d5b64d0568a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "e9ea9c9e-b8c5-4ff2-9c6a-5c9c2d5ffc1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "214c583c-0691-42e9-8a23-d5b64d0568a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c924a51c-d8d9-416a-8007-609a3d6ae4de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "214c583c-0691-42e9-8a23-d5b64d0568a7",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "8b7aca5e-e218-4ffc-ae62-6845fbd31a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "0e8442f6-36bb-4321-9e07-a33e8023714d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b7aca5e-e218-4ffc-ae62-6845fbd31a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e80476d-eabb-47b8-a40d-05ec236de204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b7aca5e-e218-4ffc-ae62-6845fbd31a9f",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "e857c0aa-6d56-4424-af36-6ac6dfe72a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "8babdf51-0cf4-453c-82cc-5d818cd16af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e857c0aa-6d56-4424-af36-6ac6dfe72a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20471e87-3f68-4c30-ba60-492de6ef24e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e857c0aa-6d56-4424-af36-6ac6dfe72a90",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        },
        {
            "id": "d4506d34-48e0-40cf-a28b-ce5c5eb36599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "compositeImage": {
                "id": "6365937a-55d9-4776-a665-f7f87dd5b3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4506d34-48e0-40cf-a28b-ce5c5eb36599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c15181-b738-46a1-a918-25f89984540e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4506d34-48e0-40cf-a28b-ce5c5eb36599",
                    "LayerId": "901c2aff-6a76-460b-90d2-bf03c88b3121"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "901c2aff-6a76-460b-90d2-bf03c88b3121",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dea2db9-4e45-422c-a982-5cc197afa25e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}