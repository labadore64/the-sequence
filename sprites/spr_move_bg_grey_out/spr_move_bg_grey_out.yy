{
    "id": "9f8af133-e8e3-4081-a69c-0f7c77749382",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_grey_out",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41d1392c-3830-4109-8fa4-98d1b4196f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f8af133-e8e3-4081-a69c-0f7c77749382",
            "compositeImage": {
                "id": "595ee0db-e25e-4019-95ae-66c399890790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41d1392c-3830-4109-8fa4-98d1b4196f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31108d03-0a52-471f-b2e1-567ddd38a09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41d1392c-3830-4109-8fa4-98d1b4196f4d",
                    "LayerId": "c52af11c-e9c7-4526-97e4-b672b50e1ae0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "c52af11c-e9c7-4526-97e4-b672b50e1ae0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f8af133-e8e3-4081-a69c-0f7c77749382",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}