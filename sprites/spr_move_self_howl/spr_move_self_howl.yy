{
    "id": "7321f9cf-588c-4897-a037-90f1dd9cfbe6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_self_howl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f7ab320-fb04-4dcb-9fc5-6f681d3435ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7321f9cf-588c-4897-a037-90f1dd9cfbe6",
            "compositeImage": {
                "id": "a6b1ce84-8b92-4990-b25e-3b6560517a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7ab320-fb04-4dcb-9fc5-6f681d3435ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e894180-3e1d-411d-90ae-8882df715f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7ab320-fb04-4dcb-9fc5-6f681d3435ed",
                    "LayerId": "763b7e90-21a6-45e5-ae7b-553613527756"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "763b7e90-21a6-45e5-ae7b-553613527756",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7321f9cf-588c-4897-a037-90f1dd9cfbe6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}