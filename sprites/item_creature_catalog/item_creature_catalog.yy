{
    "id": "615429ac-56a1-4f1f-a4b6-b24cc121ba66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_creature_catalog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 46,
    "bbox_right": 214,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "522a13ff-aa97-4773-83b2-ae2bad05ea06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "615429ac-56a1-4f1f-a4b6-b24cc121ba66",
            "compositeImage": {
                "id": "13d839dc-414a-4374-9316-87b16c07407f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522a13ff-aa97-4773-83b2-ae2bad05ea06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c948c8-9609-48b4-8fa1-a1d751142647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522a13ff-aa97-4773-83b2-ae2bad05ea06",
                    "LayerId": "efa99e59-d595-4ffe-9860-37ec93d53c23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "efa99e59-d595-4ffe-9860-37ec93d53c23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "615429ac-56a1-4f1f-a4b6-b24cc121ba66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}