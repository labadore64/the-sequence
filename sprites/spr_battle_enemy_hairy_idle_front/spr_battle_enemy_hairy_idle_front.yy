{
    "id": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 18,
    "bbox_right": 250,
    "bbox_top": 110,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b1d9ecc-b731-415e-a453-ee4832ded57a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "9e6865c0-012d-4584-b070-d01516b123b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b1d9ecc-b731-415e-a453-ee4832ded57a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be7acb37-e376-4c72-93ec-bd96565c91d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b1d9ecc-b731-415e-a453-ee4832ded57a",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "272c3f46-12f0-4cc2-a0d4-c344e9c8c4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "1fc5cda7-22b5-49a7-a26d-0ced368bff51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "272c3f46-12f0-4cc2-a0d4-c344e9c8c4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8263f56-f0ef-4da6-85e7-256676eef3b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "272c3f46-12f0-4cc2-a0d4-c344e9c8c4ef",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "9d87fa86-8fc2-48c0-941d-a2b0706926e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "9cdc355c-6fcb-4fe9-812b-391379ff036c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d87fa86-8fc2-48c0-941d-a2b0706926e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54a4121-b9c5-4717-b708-3f34c8dc08fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d87fa86-8fc2-48c0-941d-a2b0706926e3",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "b7291ac3-284c-44e0-91ae-e534af6518a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "c17d1b2b-f9b3-41c2-98c8-30b7df8a4944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7291ac3-284c-44e0-91ae-e534af6518a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba33c331-07eb-46e8-9b23-466b9f0268b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7291ac3-284c-44e0-91ae-e534af6518a1",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "e5636256-1155-4911-95d0-4624e63c3279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "c2ae08cc-0190-4688-8ab3-49d5e65da891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5636256-1155-4911-95d0-4624e63c3279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af0f8b5-109d-4e5a-99ba-26bc48268eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5636256-1155-4911-95d0-4624e63c3279",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "369dbe4b-086c-466c-b629-6e69c2723d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "60a54b03-b170-4bac-9bbe-a616dcf3350e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "369dbe4b-086c-466c-b629-6e69c2723d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67a63536-68b5-4cf0-896d-f285766dec7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "369dbe4b-086c-466c-b629-6e69c2723d57",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "6d2217fd-6978-431a-ad51-f4790da646b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "0c346f73-5e8e-4e63-ae39-3ebd891e45c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d2217fd-6978-431a-ad51-f4790da646b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2427b812-66b7-4f2b-a78a-831fd3c70b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d2217fd-6978-431a-ad51-f4790da646b3",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "167b7ac6-9b99-45de-b963-a022754af49d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "25700a15-9cb9-49ad-b54a-00edace5d225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167b7ac6-9b99-45de-b963-a022754af49d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f0d2bda-1dd2-4b95-b23d-238861e81d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167b7ac6-9b99-45de-b963-a022754af49d",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "640e3959-f73f-4262-b97a-b90fe8f578fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "a6cf31dc-291f-48d3-be93-97ae9c318856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "640e3959-f73f-4262-b97a-b90fe8f578fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99b3df6c-39a2-40c2-a5c8-b5fa895a59c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "640e3959-f73f-4262-b97a-b90fe8f578fd",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "8c362977-5baa-4356-b7df-8e605136d2a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "57742f09-7a7a-4d42-b055-e70ea754f428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c362977-5baa-4356-b7df-8e605136d2a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f00c987-8f9e-4134-bdf1-ad0774776cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c362977-5baa-4356-b7df-8e605136d2a7",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "307dde3f-c672-4140-8eb3-8d423ec858e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "8ef172a2-f229-4ee8-823f-362898f76ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307dde3f-c672-4140-8eb3-8d423ec858e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff4c6d3-3bce-44b2-a7c1-cd4a64537ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307dde3f-c672-4140-8eb3-8d423ec858e4",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "16dac9b9-57f0-4a63-935d-3253ddec1aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "319ffa24-2054-43e5-8272-8819932ea9b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16dac9b9-57f0-4a63-935d-3253ddec1aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02941295-42fd-4251-ab49-8eec1a0adfe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16dac9b9-57f0-4a63-935d-3253ddec1aa6",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "7920104b-b698-4eef-84cb-c32123aabdbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "f1416185-c488-4844-887c-14d70c1169b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7920104b-b698-4eef-84cb-c32123aabdbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98425557-6c72-4da6-80b2-35356bb27cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7920104b-b698-4eef-84cb-c32123aabdbf",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "579a9fcf-198d-4285-b82e-6da8b0c6cc45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "5b56cceb-19bc-48a7-9763-d541d8a106fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579a9fcf-198d-4285-b82e-6da8b0c6cc45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8ce5e1-67a5-4017-817c-b90efb1a6d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579a9fcf-198d-4285-b82e-6da8b0c6cc45",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "3cd0a5e0-f56d-400d-9655-02eeb976e769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "44024583-0b75-4883-ba3f-95a797c6ab2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd0a5e0-f56d-400d-9655-02eeb976e769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1ce86e-872d-4246-8b3f-f17ea3872bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd0a5e0-f56d-400d-9655-02eeb976e769",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "29c376c1-0480-4c52-be78-d52563b88ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "096b737e-43f0-4ecc-9874-2b8855dcccf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c376c1-0480-4c52-be78-d52563b88ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac1f7202-3797-4b39-a2de-07fef5208194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c376c1-0480-4c52-be78-d52563b88ecd",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "78c14a52-9b5b-4b8e-99e3-94035684e1ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "03bff91a-dfe8-4179-a745-95fe7c4b2d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78c14a52-9b5b-4b8e-99e3-94035684e1ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf7586ef-58a1-47ee-b377-24d4fe88e150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78c14a52-9b5b-4b8e-99e3-94035684e1ed",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "1a27c3b3-63cf-4a58-a6cb-9d6f38953f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "c368314b-accf-4503-b482-5810bf7c2e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a27c3b3-63cf-4a58-a6cb-9d6f38953f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335d7af6-333a-4c59-abbe-5cb591b4eff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a27c3b3-63cf-4a58-a6cb-9d6f38953f6a",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "385a8056-891c-468b-af20-47e08985d98f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "2a8040b0-783c-4c86-afd8-017f11714d43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385a8056-891c-468b-af20-47e08985d98f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e54f1d5-057b-4cb4-ad44-a1faa4241fc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385a8056-891c-468b-af20-47e08985d98f",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        },
        {
            "id": "7d4b42b4-b7f5-45e7-9129-38c277be7d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "compositeImage": {
                "id": "7776ee62-8817-4954-b558-4e6ff294af53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4b42b4-b7f5-45e7-9129-38c277be7d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6846f2-c3b1-4f4b-a4a8-dc2b90b07829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4b42b4-b7f5-45e7-9129-38c277be7d27",
                    "LayerId": "d840e699-b663-4b52-9515-1170e1bea960"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d840e699-b663-4b52-9515-1170e1bea960",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0a83ae5-6671-4c40-87c5-0c035ec83567",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}