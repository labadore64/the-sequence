{
    "id": "a5d0f89b-94e4-443e-88a5-b6c70599047c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_birdFeeder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 251,
    "bbox_left": 48,
    "bbox_right": 213,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20dbd3da-e73d-4f7b-8efc-5e74e526b3f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5d0f89b-94e4-443e-88a5-b6c70599047c",
            "compositeImage": {
                "id": "23a1196b-dcbf-4c84-bf31-5ca031723ebe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20dbd3da-e73d-4f7b-8efc-5e74e526b3f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5aef28d-e8a6-4ba7-b6cd-615e09246e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20dbd3da-e73d-4f7b-8efc-5e74e526b3f6",
                    "LayerId": "e325448b-0447-4e8d-a1f9-429f4628ad62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e325448b-0447-4e8d-a1f9-429f4628ad62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5d0f89b-94e4-443e-88a5-b6c70599047c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}