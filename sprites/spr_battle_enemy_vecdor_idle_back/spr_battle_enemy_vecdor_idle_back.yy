{
    "id": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_vecdor_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 190,
    "bbox_left": 2,
    "bbox_right": 255,
    "bbox_top": 68,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7051e8e6-9299-425f-8e73-a1ff64a128d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "309327f1-16a6-4097-9730-76f22841105e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7051e8e6-9299-425f-8e73-a1ff64a128d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29aa61a-1c83-45ea-8770-659f7c3e04c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7051e8e6-9299-425f-8e73-a1ff64a128d0",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "0e5056c6-0b37-43f8-897d-ab2b7085dce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "1905185a-db80-4f7d-977f-a43c39d2d0d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e5056c6-0b37-43f8-897d-ab2b7085dce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c07d50-a9ac-49f4-a837-54c3f273d480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e5056c6-0b37-43f8-897d-ab2b7085dce9",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "750bff3f-c019-41c1-8274-dc5a8f109e43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "e4ca6ad5-7304-417b-89a2-755bb69791d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750bff3f-c019-41c1-8274-dc5a8f109e43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42957894-0352-41c5-8f73-1f55f0c501d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750bff3f-c019-41c1-8274-dc5a8f109e43",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "34145251-768b-4ed4-ae75-a0091b6adc89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "bab13f94-d6d0-49bc-bd58-7f7bc7ec8d85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34145251-768b-4ed4-ae75-a0091b6adc89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e8c816-ccd8-4372-9107-da82cacc1681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34145251-768b-4ed4-ae75-a0091b6adc89",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "867b3799-9948-4792-9f34-d9015edc823b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "899d90b1-31e9-4932-a0f8-e1a3471f673b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "867b3799-9948-4792-9f34-d9015edc823b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd29b514-ac5c-4841-bcc2-c4c5106ba82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "867b3799-9948-4792-9f34-d9015edc823b",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "05282184-d7bc-4063-8a43-91f7df22f834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "991fea3b-c64d-4cbd-b59d-fe443179cfe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05282184-d7bc-4063-8a43-91f7df22f834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21bbf03a-0ac0-4c7e-b743-883c259281f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05282184-d7bc-4063-8a43-91f7df22f834",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "ad713074-60f0-41aa-8d4e-1942f637a8bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "70c39093-7866-43e1-b5b1-179e05dd307f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad713074-60f0-41aa-8d4e-1942f637a8bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3591162-05da-4add-8a58-eeadc21f811b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad713074-60f0-41aa-8d4e-1942f637a8bd",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "45468bbd-a990-47ae-912c-87e1c2fdb96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "ac30c539-77d3-4f7e-93eb-ad23064d82b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45468bbd-a990-47ae-912c-87e1c2fdb96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2435e9bd-210c-48d3-b9ca-00088132c9d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45468bbd-a990-47ae-912c-87e1c2fdb96e",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "8d9c25c3-3328-4d44-b1e8-84ef678098ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "b9fab666-2d26-4291-be66-4b3e3ddcc120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9c25c3-3328-4d44-b1e8-84ef678098ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83e45cb-5028-4215-8a4f-380e6016d74f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9c25c3-3328-4d44-b1e8-84ef678098ef",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "01fb5bca-893a-4b52-abfb-cf6770b3cc92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "e6f91624-f810-4dbe-9e4c-8a8e3f45dc90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fb5bca-893a-4b52-abfb-cf6770b3cc92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966607b2-e798-49f8-8c75-fde5f12df797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fb5bca-893a-4b52-abfb-cf6770b3cc92",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "ac441ace-80b1-4f33-83bb-d0c763cbe055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "607761a8-ba77-4b2b-836a-ad17dd16f5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac441ace-80b1-4f33-83bb-d0c763cbe055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af82a233-f70b-4edf-84da-402e7788d80f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac441ace-80b1-4f33-83bb-d0c763cbe055",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "8b099bf1-6d33-47e1-88d2-1d321c2df380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "4d245a6c-faa9-4ee1-a929-530d59b3a458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b099bf1-6d33-47e1-88d2-1d321c2df380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9025ab1f-e758-4019-803b-cfe6a3c3acb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b099bf1-6d33-47e1-88d2-1d321c2df380",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "34840284-0c96-4d0b-b968-485924553155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "39e43e0f-1e15-439f-b60a-8f91a5363c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34840284-0c96-4d0b-b968-485924553155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b899c735-b400-46c5-863e-7bd04aa2813e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34840284-0c96-4d0b-b968-485924553155",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "b73419c1-e98b-45f4-a44a-0e2f8747326a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "8b441f22-a6c6-4fb6-ae7b-d72a68ec5636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b73419c1-e98b-45f4-a44a-0e2f8747326a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc9bbcc-359d-4150-8b8a-6afa4e52615f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b73419c1-e98b-45f4-a44a-0e2f8747326a",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "17218a86-f4c7-4625-9880-70f4992355bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "c32af845-3a84-48e9-93b4-b98ca8b890c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17218a86-f4c7-4625-9880-70f4992355bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0048273-bcfe-4742-992b-900f83ab9f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17218a86-f4c7-4625-9880-70f4992355bf",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "a92d6178-5522-4581-8699-d835a4acde92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "f8ad0754-7ade-4123-9a1f-0e2801888443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92d6178-5522-4581-8699-d835a4acde92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d7ebf5-2ef4-42d9-9038-b11237edebb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92d6178-5522-4581-8699-d835a4acde92",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "43da1eb0-d190-4c33-9ad6-0cd865d1ddba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "cbf1ef56-511d-46a4-8e60-e43fb9e4219a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43da1eb0-d190-4c33-9ad6-0cd865d1ddba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43cd5272-5eea-470e-b5e1-7498a44fa790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43da1eb0-d190-4c33-9ad6-0cd865d1ddba",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "23297e32-601f-4986-9b98-719a9a86ef91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "fa627a8d-1175-4da5-9b4f-44678a928a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23297e32-601f-4986-9b98-719a9a86ef91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b111aae-b286-418f-98fe-cd3154769d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23297e32-601f-4986-9b98-719a9a86ef91",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "26005329-3cc4-49cd-99ad-f59128cf3196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "b23343de-75f0-460c-94f6-95318ac06dad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26005329-3cc4-49cd-99ad-f59128cf3196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd94929-2ff2-49d3-93a5-c99c24a1b1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26005329-3cc4-49cd-99ad-f59128cf3196",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "dcbe7099-b8be-4a16-ba28-088a0e0eb62c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "b2d508a5-b56d-4a29-a27e-0660e9af2f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcbe7099-b8be-4a16-ba28-088a0e0eb62c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18c0e761-0eb0-4fd1-87c9-30696b46c84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcbe7099-b8be-4a16-ba28-088a0e0eb62c",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "d4d827f7-12e1-4cf4-b133-40f2338f272f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "691c807a-6299-4345-bba8-8c4c869318ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d827f7-12e1-4cf4-b133-40f2338f272f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85da2f80-a6b6-4278-830e-4e7178d3bcea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d827f7-12e1-4cf4-b133-40f2338f272f",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "65a8ecfb-2257-40c2-aa20-c9105f7fad8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "11a35a67-e2a9-452e-89a9-edcd6e373f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a8ecfb-2257-40c2-aa20-c9105f7fad8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a365d6e3-b280-4414-9e35-f3c8e9a7994d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a8ecfb-2257-40c2-aa20-c9105f7fad8f",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "019fb4e7-93e3-4e10-b16f-0c092d758dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "b6c03d56-953c-43fa-9bfe-f41f956f660f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019fb4e7-93e3-4e10-b16f-0c092d758dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276c88ee-9710-4338-bb07-0031b25b4079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019fb4e7-93e3-4e10-b16f-0c092d758dcf",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "ce2457e5-bb5b-4499-a388-d41e985cc7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "4db5641a-ba25-4150-8dc4-a7d6b4895242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce2457e5-bb5b-4499-a388-d41e985cc7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3116966a-303e-4a72-850a-1270e8684641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce2457e5-bb5b-4499-a388-d41e985cc7ca",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "076fb5cd-cc2f-4e47-a2f2-4dea2e9e5ebc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "5c5c77d9-0b0f-40c8-be5f-0bfe522b544a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076fb5cd-cc2f-4e47-a2f2-4dea2e9e5ebc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b1f6edb-8fb7-4146-9d20-a299644a85e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076fb5cd-cc2f-4e47-a2f2-4dea2e9e5ebc",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "15aee1a1-3fe8-45c6-b5b5-dde05ccea988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "05b9188d-6cd7-4820-bb2d-bd885e88e18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15aee1a1-3fe8-45c6-b5b5-dde05ccea988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be6ee3e-e350-40e6-a202-07591734b8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15aee1a1-3fe8-45c6-b5b5-dde05ccea988",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "db1d7636-c7c4-418a-98f5-fcf06043c194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "2a1eb0ba-fb66-4f66-ba65-12800f1742a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1d7636-c7c4-418a-98f5-fcf06043c194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90ccbc04-8084-4127-bb49-fac9edd2bff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1d7636-c7c4-418a-98f5-fcf06043c194",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "cc4393f6-3515-41b1-b492-91dc9df45259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "2099d6e1-7c2b-4948-aad1-8aa6174e474c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4393f6-3515-41b1-b492-91dc9df45259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4de91a7d-4141-4130-8689-8335bd904209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4393f6-3515-41b1-b492-91dc9df45259",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "1f438672-1f74-47a4-8b18-92db8be867fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "fb39038f-3b9b-4e7c-825b-5213002ee8e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f438672-1f74-47a4-8b18-92db8be867fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "977b91bc-04f8-41f4-bd29-de80c5a2de17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f438672-1f74-47a4-8b18-92db8be867fe",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "0e7e8d8e-0612-4c3c-bf1c-1dc803ac4cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "026ba0e1-2618-4d43-8f6b-e0ffef46f80d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7e8d8e-0612-4c3c-bf1c-1dc803ac4cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e8a324-89b6-46d4-bf5c-07070cdef151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7e8d8e-0612-4c3c-bf1c-1dc803ac4cf3",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "a7ada3b7-1a04-4e57-a22e-6b7ec9ceb6f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "8d68927c-5e3f-4b0e-a946-ce3a9aaadbe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ada3b7-1a04-4e57-a22e-6b7ec9ceb6f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d570bc75-b006-4227-860f-f5d17fd225e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ada3b7-1a04-4e57-a22e-6b7ec9ceb6f3",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "2964678b-f9ba-44ea-9aaa-309c9b0b0605",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "94c002b4-0869-48ce-965c-477ead1295f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2964678b-f9ba-44ea-9aaa-309c9b0b0605",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8598b9ae-f70d-4318-b500-4a382937237b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2964678b-f9ba-44ea-9aaa-309c9b0b0605",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "59a1b9c0-9ccf-4b61-9449-d97e5360b027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "0807a32a-5977-4d77-9c08-d832fb876e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a1b9c0-9ccf-4b61-9449-d97e5360b027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0e76eef-9260-4012-8600-00ae345c79eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a1b9c0-9ccf-4b61-9449-d97e5360b027",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "72feb963-f600-4521-9fa5-c9d0bcb2150f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "a485d604-1eae-4004-9de7-ccf2a97d55b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72feb963-f600-4521-9fa5-c9d0bcb2150f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66a4674f-f19d-4001-ab1f-c985140636bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72feb963-f600-4521-9fa5-c9d0bcb2150f",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "4d6ff7f3-bc9a-46fb-9b1e-df0d65535500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "23bfbc29-20ba-453b-b411-3823076f3f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6ff7f3-bc9a-46fb-9b1e-df0d65535500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0e59ca-78a9-4e34-8b96-ac5a98a1869b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6ff7f3-bc9a-46fb-9b1e-df0d65535500",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "11774d18-5899-4936-9f73-816d5eb8fb0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "e5b028fd-a4d0-4c66-a24d-2f395112c1ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11774d18-5899-4936-9f73-816d5eb8fb0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508364d5-c647-4d71-97f3-369f4339e82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11774d18-5899-4936-9f73-816d5eb8fb0d",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "4bdd3a5d-7f25-4d92-be3b-73d922db4aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "a1cad261-0fdb-47f1-8b5c-8df91b0e432c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bdd3a5d-7f25-4d92-be3b-73d922db4aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5758625d-351b-4b15-b09b-2d8be5285c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bdd3a5d-7f25-4d92-be3b-73d922db4aec",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "df332f36-7f50-4730-8bc9-2c58f8eba34f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "29875c6b-c47b-4de6-b73f-de49179f86e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df332f36-7f50-4730-8bc9-2c58f8eba34f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "945a83b5-a1fc-4e43-a577-234b722168ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df332f36-7f50-4730-8bc9-2c58f8eba34f",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "0d4826cb-5420-43e6-b605-0c843554ef7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "af3edcd4-7a72-4c6e-a233-16f974c313d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d4826cb-5420-43e6-b605-0c843554ef7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db99a64f-e404-4433-97a0-1030b454e003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d4826cb-5420-43e6-b605-0c843554ef7d",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        },
        {
            "id": "f6cb909a-db01-4485-8e8d-bd7e78880f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "compositeImage": {
                "id": "e7f51a0d-a4d3-451e-89c3-cb289cfe358a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6cb909a-db01-4485-8e8d-bd7e78880f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff1295a-0174-445e-be58-f1262b1d7026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6cb909a-db01-4485-8e8d-bd7e78880f8b",
                    "LayerId": "801482cc-cd03-4973-a9c5-405879f3e551"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "801482cc-cd03-4973-a9c5-405879f3e551",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78f9e71b-20a5-4ef2-93da-9bff4595ea42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "2e11f373-00b4-4204-976b-3ba150910fec",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}