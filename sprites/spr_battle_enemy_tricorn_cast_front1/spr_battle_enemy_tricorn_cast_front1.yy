{
    "id": "70038516-2b5a-4278-8b83-6e443d7fb1e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_cast_front1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 38,
    "bbox_right": 210,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4032c856-7ad6-460f-90ec-7754c2da3f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70038516-2b5a-4278-8b83-6e443d7fb1e0",
            "compositeImage": {
                "id": "a763d2fb-7bf5-4753-8d0a-6979de7b9287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4032c856-7ad6-460f-90ec-7754c2da3f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a2c945e-0033-46c3-ab3a-6968ee1518d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4032c856-7ad6-460f-90ec-7754c2da3f31",
                    "LayerId": "9f3d46ca-8b98-4e85-a344-6562ec2f86db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9f3d46ca-8b98-4e85-a344-6562ec2f86db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70038516-2b5a-4278-8b83-6e443d7fb1e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}