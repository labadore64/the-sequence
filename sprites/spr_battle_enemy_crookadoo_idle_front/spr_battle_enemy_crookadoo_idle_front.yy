{
    "id": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_crookadoo_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 11,
    "bbox_right": 255,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "462258c0-095d-401e-b141-9d2b894fd7da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "45f97b05-9178-4c1c-8db0-4dc8af1eac24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462258c0-095d-401e-b141-9d2b894fd7da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "915a3c9a-34e9-485f-989f-1044c64b8a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462258c0-095d-401e-b141-9d2b894fd7da",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "13ff8d5e-acb3-4c9d-bbe6-dec2cce45a6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "54ca6501-465d-45e4-bbe7-274e878c8acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ff8d5e-acb3-4c9d-bbe6-dec2cce45a6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61969f2b-8f11-4577-bcf0-e2f8a300a58c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ff8d5e-acb3-4c9d-bbe6-dec2cce45a6e",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "86aec387-d3d2-47ef-b87b-94aa75d4fb8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "9b1a6849-3b55-4c06-9552-ec3c1ece7b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86aec387-d3d2-47ef-b87b-94aa75d4fb8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65f30f35-8e54-4141-90f5-09c29d55432a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86aec387-d3d2-47ef-b87b-94aa75d4fb8c",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "aaa3415e-7e07-4fe0-a223-53bc6f129c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "3e8c943f-8749-4b82-b1e8-dfd367945f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaa3415e-7e07-4fe0-a223-53bc6f129c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e30a4cf-c6be-450d-95a1-f3a1695ae9da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaa3415e-7e07-4fe0-a223-53bc6f129c6a",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "57506177-5b44-4854-ade9-8be75ef3f243",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "31853a05-82d4-4a42-86e0-24a7e4022013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57506177-5b44-4854-ade9-8be75ef3f243",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c49a4a1d-d798-495b-ab4b-56cd33f9d528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57506177-5b44-4854-ade9-8be75ef3f243",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "1c2743b0-1165-40e3-b160-2636ce53cedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "5248da75-979c-4a21-8a29-2fd00cb2de8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2743b0-1165-40e3-b160-2636ce53cedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dabce54e-6567-4505-8fe6-b4e34dc2ef5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2743b0-1165-40e3-b160-2636ce53cedf",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "e056b5bd-4846-4461-8088-c19b165afda6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "e226b410-3a95-45af-a031-aa9b2231012c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e056b5bd-4846-4461-8088-c19b165afda6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa86d6e-1630-4c4f-846d-8c91bd71d8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e056b5bd-4846-4461-8088-c19b165afda6",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "cbb74460-ecab-42b4-ac5a-4d1774a3d0be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "0906c3fa-5413-4456-84ed-2b9d58da1b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb74460-ecab-42b4-ac5a-4d1774a3d0be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f13094-6f5b-4c6c-b719-31ad84739767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb74460-ecab-42b4-ac5a-4d1774a3d0be",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "fdf13e0c-0775-4e2d-a3ee-866860f2b7f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "e3bfce84-9b52-459a-94b3-f5a1002c430f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf13e0c-0775-4e2d-a3ee-866860f2b7f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1fb9277-f6db-4000-9551-bbe9ed5cd684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf13e0c-0775-4e2d-a3ee-866860f2b7f9",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "695183a7-fdb8-4f73-8a56-78a880e8d68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "99ec5112-3c47-49be-a715-d42a0b64582f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "695183a7-fdb8-4f73-8a56-78a880e8d68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1085297-a10f-44be-bd81-e69c22c60e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "695183a7-fdb8-4f73-8a56-78a880e8d68f",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "be4bcacd-cbcc-48a1-bc6f-665e4680929c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "31202b37-db31-4c65-9c89-d7fd1e883b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4bcacd-cbcc-48a1-bc6f-665e4680929c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0f990c-513c-419b-b31a-ddaba63329c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4bcacd-cbcc-48a1-bc6f-665e4680929c",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "447d77e7-47c1-4ac2-b687-57c033338987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "27bb8804-b051-4876-ad04-50a30c5963d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "447d77e7-47c1-4ac2-b687-57c033338987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6d52572-0b84-4b0d-8a29-5c7a83cef547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "447d77e7-47c1-4ac2-b687-57c033338987",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "eb55a36b-0013-4ab9-8381-a163e8e57571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "7f897042-f845-45e1-b26d-2d259d532814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb55a36b-0013-4ab9-8381-a163e8e57571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be41424-3339-481a-9571-7aa789e3d3a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb55a36b-0013-4ab9-8381-a163e8e57571",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "444504dc-3309-4a05-8834-02f76a73c6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "c49fac0e-faed-4ff4-8a62-2ce56fc419c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444504dc-3309-4a05-8834-02f76a73c6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b04f5793-9a2d-49ff-b7d1-1a46c33fca89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444504dc-3309-4a05-8834-02f76a73c6b2",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "8592db04-d4b0-411b-808f-89fe8f005c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "d7f20735-11d3-41c3-a745-b46839d0b83b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8592db04-d4b0-411b-808f-89fe8f005c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df7e4359-e4f6-43c1-9378-46d8d55dcb5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8592db04-d4b0-411b-808f-89fe8f005c8a",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "6cd6bfb2-dc4d-4f63-944d-b06b1c069845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "5b00bdee-ca52-40cb-ac60-78987bfd38c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd6bfb2-dc4d-4f63-944d-b06b1c069845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e18e16e-dd3f-4a75-a785-763179535e46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd6bfb2-dc4d-4f63-944d-b06b1c069845",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "6f6ef9cb-e627-467b-b9e3-07f810d9e632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "4bd6dcb3-14e7-4e27-a4b2-4ec272247f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6ef9cb-e627-467b-b9e3-07f810d9e632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9435c76-0796-4719-bfb7-4a6b08ed487c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6ef9cb-e627-467b-b9e3-07f810d9e632",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "4b9935a9-daaf-409b-8b15-45fb77323d66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "d86a6b07-b13f-4473-aea6-1ead87519de1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9935a9-daaf-409b-8b15-45fb77323d66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6a6dd89-f1c2-4209-ba98-43f903c16aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9935a9-daaf-409b-8b15-45fb77323d66",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "181e8ec0-d6d3-434d-b3c0-5cd61ba84b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "29ab9304-80b5-46e4-a5a1-275625d5a08d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "181e8ec0-d6d3-434d-b3c0-5cd61ba84b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d31b3bd-ef93-4271-adab-67f21339c9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "181e8ec0-d6d3-434d-b3c0-5cd61ba84b38",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "59ff2500-6b15-4482-8528-5a616ac7b1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "8c8aeb84-2daa-45dc-af11-1f25cf87ae17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59ff2500-6b15-4482-8528-5a616ac7b1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18fbdcbf-d7e8-422e-ac3e-48d03f8b6356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59ff2500-6b15-4482-8528-5a616ac7b1d2",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "3eee4fbb-5c29-468c-8193-0ebbbe94dd10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "fac6e3b3-ce55-45e5-8550-dc176ca4305b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eee4fbb-5c29-468c-8193-0ebbbe94dd10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c7513fb-e78f-431a-9375-08ba0f20fdf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eee4fbb-5c29-468c-8193-0ebbbe94dd10",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "f0f2f915-4e82-4834-a151-225bc4c51a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "83192f33-ed19-4e26-96a0-60ef95bca493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0f2f915-4e82-4834-a151-225bc4c51a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea932bd-8a89-45c5-a8e0-63c0b765dcaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0f2f915-4e82-4834-a151-225bc4c51a70",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "d45ee458-05c8-4ee8-9c62-e063c4d5c926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "f9cc50c2-e308-4fdd-81a3-f5188350e150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d45ee458-05c8-4ee8-9c62-e063c4d5c926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0758af6f-6eca-469c-8a12-353ca0f599a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45ee458-05c8-4ee8-9c62-e063c4d5c926",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "a94fb710-18b1-4b58-bb90-71b24e47e2b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "bea7c0b8-b502-4571-8426-2f2a59e832d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94fb710-18b1-4b58-bb90-71b24e47e2b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4651eb95-4075-436a-94d6-334e0a064dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94fb710-18b1-4b58-bb90-71b24e47e2b0",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "0208360f-42dc-48e7-a20f-39d38985b7fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "ec160b4f-485a-4326-b0e6-f4895c48e10e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0208360f-42dc-48e7-a20f-39d38985b7fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "891a1fa7-7223-415c-8ee9-53ba015d6185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0208360f-42dc-48e7-a20f-39d38985b7fb",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        },
        {
            "id": "19246f47-473f-40c5-b3a3-9a981083a573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "compositeImage": {
                "id": "195be13c-b5f8-4c1e-8ed8-0fa92caf13ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19246f47-473f-40c5-b3a3-9a981083a573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a969a15b-f5ed-49c2-870e-6a2513cf5eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19246f47-473f-40c5-b3a3-9a981083a573",
                    "LayerId": "aaf26c50-0049-4374-9f8a-8cef22075d0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "aaf26c50-0049-4374-9f8a-8cef22075d0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc3eface-be3f-4f9c-a39e-7861d38856d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "580e916e-bc19-4b49-aabe-9fd387268b2e",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}