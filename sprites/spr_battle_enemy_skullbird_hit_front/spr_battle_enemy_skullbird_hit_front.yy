{
    "id": "d329a37b-81a7-433e-9b9c-158ec241c0f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 32,
    "bbox_right": 216,
    "bbox_top": 80,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "356d993e-1c1e-414f-8318-92da6bddbfe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d329a37b-81a7-433e-9b9c-158ec241c0f7",
            "compositeImage": {
                "id": "31d8f145-638b-40c4-ab71-cfaac30cf01e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "356d993e-1c1e-414f-8318-92da6bddbfe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e031bddd-4a1d-4b94-a55a-79e70af4ad6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "356d993e-1c1e-414f-8318-92da6bddbfe0",
                    "LayerId": "eb22605b-43cd-48fb-887a-65a404bd181e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "eb22605b-43cd-48fb-887a-65a404bd181e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d329a37b-81a7-433e-9b9c-158ec241c0f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}