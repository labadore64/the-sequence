{
    "id": "5096760c-abc4-49b8-a2dd-8a93ff631460",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_sheldon_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 65,
    "bbox_right": 223,
    "bbox_top": 67,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f388b139-8738-472b-9ba1-110c75e1840a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "f769bdac-1687-4ecf-af69-836bf3cdce2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f388b139-8738-472b-9ba1-110c75e1840a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2268d55-20f1-47da-98bf-aa05f9a7ec4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f388b139-8738-472b-9ba1-110c75e1840a",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "aefa24be-4002-4a19-89a1-566796abc497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "8b4bdcb1-2d56-4aa0-8b7a-014da45e895c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aefa24be-4002-4a19-89a1-566796abc497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98a78f8-7011-41e9-a5da-620caae13bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aefa24be-4002-4a19-89a1-566796abc497",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "bc59f691-52e5-4c90-8314-ceaa3b261348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "be89f1a6-d92f-4847-a82a-4b6808a2a508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc59f691-52e5-4c90-8314-ceaa3b261348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3a83a8-3e7e-42ae-a238-aa6e929e8704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc59f691-52e5-4c90-8314-ceaa3b261348",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "1344c6c1-23bc-4add-ae54-81d8d16774f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "bbe7d7c7-6f5f-4be2-b3e3-c6276c84f0bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1344c6c1-23bc-4add-ae54-81d8d16774f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "730e2d02-0dc2-4c07-9fc7-fbea86fe0c63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1344c6c1-23bc-4add-ae54-81d8d16774f1",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "5429d7be-1d7b-43a9-a0e4-b7a1014b5914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "cb099d94-c8d0-4754-8724-a99d74a40553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5429d7be-1d7b-43a9-a0e4-b7a1014b5914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b10b0a-f24b-46dd-8bd0-b997c70be614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5429d7be-1d7b-43a9-a0e4-b7a1014b5914",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "d09ec6bb-9175-44ab-98ef-6f98c84c4b36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "83c24911-70fa-49f0-81c0-b644e4644c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09ec6bb-9175-44ab-98ef-6f98c84c4b36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7619d270-f658-4340-92c2-ffb78194dede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09ec6bb-9175-44ab-98ef-6f98c84c4b36",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "456e4559-b59b-4ea4-bd8b-99fda3b2b856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "c0ffd777-0a8a-454d-857f-612aa1597153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456e4559-b59b-4ea4-bd8b-99fda3b2b856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37647df6-3585-46d9-9be2-206ae9318474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456e4559-b59b-4ea4-bd8b-99fda3b2b856",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "8aeb590a-ff5a-47c4-be79-27cebed0a29c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "bb773feb-bb05-4ef1-be96-32661c693fda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aeb590a-ff5a-47c4-be79-27cebed0a29c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fe4c8f5-9768-4d5b-a5a5-309e31197f1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aeb590a-ff5a-47c4-be79-27cebed0a29c",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "4051f4ed-0744-457d-889d-684b7054c8a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "e79268bb-3889-43d1-a833-f0f38075bba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4051f4ed-0744-457d-889d-684b7054c8a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3f7e73-09b0-4793-9833-61c496b2cc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4051f4ed-0744-457d-889d-684b7054c8a6",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "b3898911-5166-4f5d-bc98-1c542e373b9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "d0187ba1-3034-45b8-98d5-7dec78c3f321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3898911-5166-4f5d-bc98-1c542e373b9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b8f7d0-e458-49c3-8719-a123c3b23ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3898911-5166-4f5d-bc98-1c542e373b9d",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "798b957c-e09d-4864-8849-da8a54f8adc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "13fc8c1f-35f6-4c7a-a144-c9ef02e26a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798b957c-e09d-4864-8849-da8a54f8adc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4541fed-6cba-419c-a6a1-91a8889aa33a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798b957c-e09d-4864-8849-da8a54f8adc6",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "9763b1cf-94bb-450d-a3cc-0550e9684f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "61434c56-3b5d-4d04-bcd8-961dade7d9a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9763b1cf-94bb-450d-a3cc-0550e9684f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a737057b-3bd0-475f-b888-e9ae26b618bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9763b1cf-94bb-450d-a3cc-0550e9684f8a",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "9f52e533-b57e-469a-83ed-178106aa782b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "1a94e7b0-3860-48d9-a38f-b4a286cf226a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f52e533-b57e-469a-83ed-178106aa782b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70bed633-c502-437c-95db-26d888e86aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f52e533-b57e-469a-83ed-178106aa782b",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "8bcbfed1-cbe7-4c32-b0b5-f2ac032d7441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "7dd7e3f3-768c-448f-9965-617568668c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bcbfed1-cbe7-4c32-b0b5-f2ac032d7441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab5364d-dc43-4f57-ac15-b3be11cb5fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bcbfed1-cbe7-4c32-b0b5-f2ac032d7441",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "046b0fd8-36bd-42a5-b677-c0d98a0d5bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "cf6bded1-e121-4349-a8d4-63d62edfdd04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046b0fd8-36bd-42a5-b677-c0d98a0d5bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674c7008-4d97-4ac8-bc0e-417e35154179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046b0fd8-36bd-42a5-b677-c0d98a0d5bf5",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "9b8a1c3f-fbe8-42ca-9681-554d277bda7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "1b9622c2-fe5e-414f-8670-f974825c8768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b8a1c3f-fbe8-42ca-9681-554d277bda7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27fcd39e-eadd-466d-a4d2-5d7347100342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b8a1c3f-fbe8-42ca-9681-554d277bda7a",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "4cdd2488-8cc3-4bcf-9fb6-675707160aee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "d8d88828-4a0f-4a70-91d5-8a7df9e493f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cdd2488-8cc3-4bcf-9fb6-675707160aee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7412281f-1236-4ce9-a271-5074e804c122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdd2488-8cc3-4bcf-9fb6-675707160aee",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "dc517d15-5db0-46e6-be98-ff9caf1ad32b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "fc52acab-09c6-4f29-bf44-ea9f7356e4d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc517d15-5db0-46e6-be98-ff9caf1ad32b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5797bd0d-5d05-4385-9cfb-939cee156db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc517d15-5db0-46e6-be98-ff9caf1ad32b",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "300df345-199d-4e0d-9d93-93f3fc21e2fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "0c701dec-19d0-41c0-9a40-94b2a05510eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300df345-199d-4e0d-9d93-93f3fc21e2fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "529879c2-8534-400e-8ccf-948794124785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300df345-199d-4e0d-9d93-93f3fc21e2fd",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        },
        {
            "id": "d15d5400-bd93-4e08-ad37-ae2df6692f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "compositeImage": {
                "id": "c21ca481-fd09-4557-85b7-1a98b9fbc316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d15d5400-bd93-4e08-ad37-ae2df6692f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5766d2cd-15a2-410b-8ca4-960da224342d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d15d5400-bd93-4e08-ad37-ae2df6692f56",
                    "LayerId": "9e1d28b5-a49a-4aca-890c-450082a8eebe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9e1d28b5-a49a-4aca-890c-450082a8eebe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5096760c-abc4-49b8-a2dd-8a93ff631460",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "9e10e934-31a0-4a56-90f1-71dc8c1ec973",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}