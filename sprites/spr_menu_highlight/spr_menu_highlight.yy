{
    "id": "9c972b7c-e0c3-46ea-8233-737d22353711",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_highlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 16,
    "bbox_right": 236,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a0991eb-a2ac-432d-9e28-288a7b225c6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c972b7c-e0c3-46ea-8233-737d22353711",
            "compositeImage": {
                "id": "25fc76b5-0190-4580-af47-dc118de4a693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a0991eb-a2ac-432d-9e28-288a7b225c6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e5899b-2150-402f-99d5-275981759aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a0991eb-a2ac-432d-9e28-288a7b225c6f",
                    "LayerId": "94a463aa-d489-44b3-9a9a-cfb53e407fd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "94a463aa-d489-44b3-9a9a-cfb53e407fd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c972b7c-e0c3-46ea-8233-737d22353711",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.604,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}