{
    "id": "52eeaa41-8d84-4ca4-b83a-b1fe04aa1be9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite146",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8f86c1c-f3c8-458a-914b-1bab31cf465f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52eeaa41-8d84-4ca4-b83a-b1fe04aa1be9",
            "compositeImage": {
                "id": "ebaeed11-e527-489f-a6d9-09d9dc4fd262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8f86c1c-f3c8-458a-914b-1bab31cf465f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d31d8e46-728b-4231-aeda-b0190f75ca73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8f86c1c-f3c8-458a-914b-1bab31cf465f",
                    "LayerId": "f15fc629-ce5c-4886-bff0-71355865fa59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f15fc629-ce5c-4886-bff0-71355865fa59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52eeaa41-8d84-4ca4-b83a-b1fe04aa1be9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 31,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}