{
    "id": "18d2c8f0-052c-4e1e-b608-b89c8e92508d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faceSpriteMain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d919cbdc-a0fc-413c-ae39-8a2087766657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18d2c8f0-052c-4e1e-b608-b89c8e92508d",
            "compositeImage": {
                "id": "efe5cd01-0bad-4ecf-af25-dcbb411edc62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d919cbdc-a0fc-413c-ae39-8a2087766657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040aaabb-bef4-495d-b4e8-b1788c10e6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d919cbdc-a0fc-413c-ae39-8a2087766657",
                    "LayerId": "c9c05abe-f9e8-437d-9fe5-b16d88593562"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c9c05abe-f9e8-437d-9fe5-b16d88593562",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18d2c8f0-052c-4e1e-b608-b89c8e92508d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "19b41542-e9b6-42a9-94c7-05cbadc1d852",
    "type": 1,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}