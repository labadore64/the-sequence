{
    "id": "fce7ce0f-ae6c-4807-a916-80502c22678d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 45,
    "bbox_right": 183,
    "bbox_top": 100,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2e2d410-918c-43e8-96a7-8c4bf4bbb0bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fce7ce0f-ae6c-4807-a916-80502c22678d",
            "compositeImage": {
                "id": "5773199d-a0ba-406f-8791-85459e4c112b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e2d410-918c-43e8-96a7-8c4bf4bbb0bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf0bfb8b-039f-4323-9381-a31b9254ac10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e2d410-918c-43e8-96a7-8c4bf4bbb0bd",
                    "LayerId": "6b179153-3f4f-4c30-b3df-5d55af9c3fe9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6b179153-3f4f-4c30-b3df-5d55af9c3fe9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fce7ce0f-ae6c-4807-a916-80502c22678d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}