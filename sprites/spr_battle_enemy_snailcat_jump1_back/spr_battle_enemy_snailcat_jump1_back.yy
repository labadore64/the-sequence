{
    "id": "473a8b6c-47b7-4e6f-8ac4-3730b68d91c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 66,
    "bbox_right": 195,
    "bbox_top": 125,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3669c18f-e551-401e-904b-4c6211fba6fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "473a8b6c-47b7-4e6f-8ac4-3730b68d91c3",
            "compositeImage": {
                "id": "43e8d0f6-0b8c-4ccf-9768-7bfff410fba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3669c18f-e551-401e-904b-4c6211fba6fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc76965e-4445-4254-a300-f3b59dd96f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3669c18f-e551-401e-904b-4c6211fba6fb",
                    "LayerId": "1e0c0d01-bafe-48fc-a121-9d5a34c132eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1e0c0d01-bafe-48fc-a121-9d5a34c132eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "473a8b6c-47b7-4e6f-8ac4-3730b68d91c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}