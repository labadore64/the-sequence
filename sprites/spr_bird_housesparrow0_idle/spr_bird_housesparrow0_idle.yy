{
    "id": "a734ea05-0d31-4513-b536-cfd14e341c78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_housesparrow0_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 15,
    "bbox_right": 120,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a074bd34-df23-4083-a372-1607740db0e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a734ea05-0d31-4513-b536-cfd14e341c78",
            "compositeImage": {
                "id": "82f9d3ce-192a-4334-97eb-eeb0961ea154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a074bd34-df23-4083-a372-1607740db0e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961880d3-b3aa-496e-b032-0ad0898d348d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a074bd34-df23-4083-a372-1607740db0e1",
                    "LayerId": "a20968e4-7bfa-43af-931f-7566a318c263"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a20968e4-7bfa-43af-931f-7566a318c263",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a734ea05-0d31-4513-b536-cfd14e341c78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}