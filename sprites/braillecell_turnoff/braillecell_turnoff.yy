{
    "id": "2ac8468f-4157-421a-8216-3fe3310c1e9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "braillecell_turnoff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0dd716b-36fc-45d4-95f4-c22a9b5dfc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ac8468f-4157-421a-8216-3fe3310c1e9f",
            "compositeImage": {
                "id": "fce37f5d-ff8e-465d-b86c-6cad4c3c5951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dd716b-36fc-45d4-95f4-c22a9b5dfc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "defe1ef4-cc83-4544-b910-fa28f6219098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dd716b-36fc-45d4-95f4-c22a9b5dfc5b",
                    "LayerId": "d1c36520-ac31-48c9-998f-b4c443d43adc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1c36520-ac31-48c9-998f-b4c443d43adc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ac8468f-4157-421a-8216-3fe3310c1e9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}