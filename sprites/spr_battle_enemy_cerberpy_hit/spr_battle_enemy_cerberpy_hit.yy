{
    "id": "6d797b74-08e8-4544-94c1-0866d0e89528",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_cerberpy_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 76,
    "bbox_right": 199,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fff08cf-a5de-4887-9bcc-73a4e7cb6ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d797b74-08e8-4544-94c1-0866d0e89528",
            "compositeImage": {
                "id": "0a23063d-0cc5-4d36-9023-98979b79f2a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fff08cf-a5de-4887-9bcc-73a4e7cb6ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed226427-b389-4423-a0a7-c5d7cf4c2099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fff08cf-a5de-4887-9bcc-73a4e7cb6ad2",
                    "LayerId": "a4d3f910-033d-452e-bf84-4031dd8c410e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a4d3f910-033d-452e-bf84-4031dd8c410e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d797b74-08e8-4544-94c1-0866d0e89528",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}