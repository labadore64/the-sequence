{
    "id": "5d7e953e-eb94-4c2f-bf0a-774f50f258b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_cast_front1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 49,
    "bbox_right": 219,
    "bbox_top": 127,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efe470ef-a521-4cd2-82f3-90e005673748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d7e953e-eb94-4c2f-bf0a-774f50f258b6",
            "compositeImage": {
                "id": "d30e57c8-ab32-4fef-8e7f-84317ce5e407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe470ef-a521-4cd2-82f3-90e005673748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b0a95e-d721-4d34-90ab-039d138131e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe470ef-a521-4cd2-82f3-90e005673748",
                    "LayerId": "75c074e7-19a5-48f0-8ffb-2cd702d80a54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "75c074e7-19a5-48f0-8ffb-2cd702d80a54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d7e953e-eb94-4c2f-bf0a-774f50f258b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}