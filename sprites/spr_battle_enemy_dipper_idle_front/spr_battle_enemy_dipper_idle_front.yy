{
    "id": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_dipper_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 49,
    "bbox_right": 189,
    "bbox_top": 117,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eba1265-6703-4639-98a9-09e64a12c3b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "e5d8296f-0c0f-4244-bc1d-de5a87b3ed5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eba1265-6703-4639-98a9-09e64a12c3b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d75106a-bda2-4dfb-aa45-add57ca1c14a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eba1265-6703-4639-98a9-09e64a12c3b1",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "70989cd1-d3b8-4b7c-9f6c-729c23a87667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "d26272e4-8848-49ac-9b27-4312d3d6278f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70989cd1-d3b8-4b7c-9f6c-729c23a87667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08da1e5c-b6aa-4c73-a461-043ba15aedbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70989cd1-d3b8-4b7c-9f6c-729c23a87667",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "b3d77f9e-d651-481f-b1c1-cf4e036eb574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "489d5a13-e490-4bf3-8f7c-25a1e63cc28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d77f9e-d651-481f-b1c1-cf4e036eb574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c3c962-c8a2-45af-ad3c-2d7f80353e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d77f9e-d651-481f-b1c1-cf4e036eb574",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "76e360d8-0704-4efb-90e0-f640e0b60abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "33dc7f47-045c-45db-8b23-b75fca160d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e360d8-0704-4efb-90e0-f640e0b60abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "280d7491-d576-4ef1-baa1-5768e9cd65de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e360d8-0704-4efb-90e0-f640e0b60abf",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "6f3255ed-7792-4b51-94bf-b24523953e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "7626a123-1fe1-416e-a1ca-3c98d906ed5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3255ed-7792-4b51-94bf-b24523953e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1633274b-8e91-4a46-9c50-4bb93a348326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3255ed-7792-4b51-94bf-b24523953e65",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "35e25412-c580-40d0-9a5a-5c4fc6037665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "e1b79ac9-c26e-491d-ad48-c43c08663524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e25412-c580-40d0-9a5a-5c4fc6037665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d8371e2-b94b-4f73-9ffa-69145257f299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e25412-c580-40d0-9a5a-5c4fc6037665",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "0c43132a-54cf-4009-a9ed-68a2c8ebc051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "2a15aabe-56c4-4eea-937a-ddbff962726c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c43132a-54cf-4009-a9ed-68a2c8ebc051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f00037c9-6bb0-4082-80c2-9a2b1165493f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c43132a-54cf-4009-a9ed-68a2c8ebc051",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "a5ad1e77-b127-40c4-bb02-9419bb152774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "b645f1c6-b12f-4072-ad45-fdbae70037dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ad1e77-b127-40c4-bb02-9419bb152774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82af06f3-0a2f-466f-b4e6-10d4e5f429e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ad1e77-b127-40c4-bb02-9419bb152774",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "023211ba-1a86-4173-b0d4-ab7a541f2fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "5370a7b8-6346-4f9d-a590-5359f1dc0630",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "023211ba-1a86-4173-b0d4-ab7a541f2fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a87e1752-2134-4c3b-9301-17f76c6ec480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "023211ba-1a86-4173-b0d4-ab7a541f2fe3",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "afd66c9e-ac4d-4234-9b27-31bc76d1c067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "a7ee58f1-ba15-4e37-9509-38d11a94d093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd66c9e-ac4d-4234-9b27-31bc76d1c067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3695b5b0-52cb-4b12-aedb-db15f62f1a30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd66c9e-ac4d-4234-9b27-31bc76d1c067",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "776bb1ba-15d0-4e86-8761-4f8706abcdb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "a4cad6ff-8767-46de-abf7-07470dc5948f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "776bb1ba-15d0-4e86-8761-4f8706abcdb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c03ed62-e008-4b90-94b5-e9de73620d54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776bb1ba-15d0-4e86-8761-4f8706abcdb4",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "aec7718c-b929-4e11-a3b4-9037fa9e3ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "3806e6c5-5935-482d-ab43-ecfd7ee910e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec7718c-b929-4e11-a3b4-9037fa9e3ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20211f21-d426-4cde-8ac1-cfedb23bbd4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec7718c-b929-4e11-a3b4-9037fa9e3ab7",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "72c0b24d-eaaa-4d83-bb39-51c85b69a2d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "dd20e3a5-2bc6-45d0-9c44-6d483c174bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c0b24d-eaaa-4d83-bb39-51c85b69a2d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8b51c9-ad22-4528-ad0b-8904cf4b8949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c0b24d-eaaa-4d83-bb39-51c85b69a2d1",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        },
        {
            "id": "f08e11ac-1bfb-4e6f-a7b2-6b24810e453f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "compositeImage": {
                "id": "636b34e1-1d2f-4e3a-9396-2bab3512be45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f08e11ac-1bfb-4e6f-a7b2-6b24810e453f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f63465-7f1b-47a2-8bcf-8fd265b3e353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f08e11ac-1bfb-4e6f-a7b2-6b24810e453f",
                    "LayerId": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9e1c5f4e-4a50-418d-b9a9-4e5d01fab0e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ed6d05b-7478-4fca-8248-2e76200a86b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1e479664-c8d0-4e3e-9d43-b1d1b8e5d215",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}