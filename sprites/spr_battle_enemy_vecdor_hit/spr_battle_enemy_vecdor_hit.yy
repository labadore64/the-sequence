{
    "id": "d6759883-c5e7-4009-8f76-3c56123261fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_vecdor_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 44,
    "bbox_right": 217,
    "bbox_top": 59,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "451ee7c3-beb7-43fa-baf4-f5e2b4c9c452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6759883-c5e7-4009-8f76-3c56123261fe",
            "compositeImage": {
                "id": "bb75149b-ce56-47ae-af57-d29e177d5ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451ee7c3-beb7-43fa-baf4-f5e2b4c9c452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a5483f-2bd7-4cc4-8b22-372a2d97519f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451ee7c3-beb7-43fa-baf4-f5e2b4c9c452",
                    "LayerId": "fe4ddaa1-0eac-4336-ac7c-5201a2d52362"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fe4ddaa1-0eac-4336-ac7c-5201a2d52362",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6759883-c5e7-4009-8f76-3c56123261fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "2e11f373-00b4-4204-976b-3ba150910fec",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}