{
    "id": "3f25d14b-6911-43dc-94b8-2552459e1eab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_dipper_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 77,
    "bbox_right": 185,
    "bbox_top": 86,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92bd2ab2-a61e-49da-8863-00261b808a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f25d14b-6911-43dc-94b8-2552459e1eab",
            "compositeImage": {
                "id": "75f29fdd-6127-48d5-83f8-f01b98715f13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92bd2ab2-a61e-49da-8863-00261b808a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab2d79e-8c29-4569-a82f-38c1a650f679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92bd2ab2-a61e-49da-8863-00261b808a05",
                    "LayerId": "01cc0d01-74c2-4d00-a7ca-ca1fb75b3ac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "01cc0d01-74c2-4d00-a7ca-ca1fb75b3ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f25d14b-6911-43dc-94b8-2552459e1eab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1e479664-c8d0-4e3e-9d43-b1d1b8e5d215",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}