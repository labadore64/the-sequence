{
    "id": "c11845d0-cce9-4579-9920-fdbfc5ba0431",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_damp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 177,
    "bbox_left": 32,
    "bbox_right": 226,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5af58c72-f349-4033-b332-dc23fbb81069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11845d0-cce9-4579-9920-fdbfc5ba0431",
            "compositeImage": {
                "id": "bad216dc-01f6-4d3d-b2e0-f5da106934c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af58c72-f349-4033-b332-dc23fbb81069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc4a7e8-36bf-4842-aea1-fcd862660d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af58c72-f349-4033-b332-dc23fbb81069",
                    "LayerId": "c45781a3-58c2-4229-a6a8-544cdd8b1103"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c45781a3-58c2-4229-a6a8-544cdd8b1103",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c11845d0-cce9-4579-9920-fdbfc5ba0431",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 170
}