{
    "id": "7603b77b-a9b0-481f-89f0-a3c5878869f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign_chain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 9,
    "bbox_right": 120,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0212dfa3-df64-48d1-8e21-8f5bd62d5886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7603b77b-a9b0-481f-89f0-a3c5878869f0",
            "compositeImage": {
                "id": "ed33d14e-a6e9-4bae-80b8-448a8fa4cfaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0212dfa3-df64-48d1-8e21-8f5bd62d5886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c214161-f1d7-435c-906f-edace6456628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0212dfa3-df64-48d1-8e21-8f5bd62d5886",
                    "LayerId": "30e3e0ce-7f6e-4f96-a065-16de29ccb7a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "30e3e0ce-7f6e-4f96-a065-16de29ccb7a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7603b77b-a9b0-481f-89f0-a3c5878869f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}