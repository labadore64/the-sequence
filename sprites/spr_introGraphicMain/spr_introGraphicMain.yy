{
    "id": "476784e6-209a-43d1-9fec-5f8ac3a60cb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_introGraphicMain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 215,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f65c9dba-ba21-4edb-a345-5f8939640424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476784e6-209a-43d1-9fec-5f8ac3a60cb2",
            "compositeImage": {
                "id": "4a601e68-f777-4d01-a064-5865b6d4c25c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f65c9dba-ba21-4edb-a345-5f8939640424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a54a061-a049-4625-bf8b-d1b23b6bbcde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f65c9dba-ba21-4edb-a345-5f8939640424",
                    "LayerId": "16abe8d7-99b6-4339-9bc9-7c02c065639c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "16abe8d7-99b6-4339-9bc9-7c02c065639c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "476784e6-209a-43d1-9fec-5f8ac3a60cb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.4555,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}