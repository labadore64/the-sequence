{
    "id": "1f5698b6-1e19-43a7-b29a-a597a586eed1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_sprite_medusa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de28c533-e7f2-4db4-bb87-2f9ea704448d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f5698b6-1e19-43a7-b29a-a597a586eed1",
            "compositeImage": {
                "id": "b76df3fe-9498-4e7a-9dc6-66534302c99f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de28c533-e7f2-4db4-bb87-2f9ea704448d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca4f7630-72a9-40fe-afd3-fd728315644f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de28c533-e7f2-4db4-bb87-2f9ea704448d",
                    "LayerId": "b217f1f1-0e3e-4337-9da0-f79fdbcc1715"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b217f1f1-0e3e-4337-9da0-f79fdbcc1715",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f5698b6-1e19-43a7-b29a-a597a586eed1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}