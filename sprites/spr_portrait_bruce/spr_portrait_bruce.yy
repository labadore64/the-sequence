{
    "id": "31031f39-c545-4761-a782-10484180ad0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_bruce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 34,
    "bbox_right": 463,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8f6252b-343a-444b-bab1-14e17391cb78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31031f39-c545-4761-a782-10484180ad0d",
            "compositeImage": {
                "id": "1ffb1890-9e62-43e3-9ff6-f31f12c685e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f6252b-343a-444b-bab1-14e17391cb78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d8f7e4-5f09-449f-b35e-3c6487df70f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f6252b-343a-444b-bab1-14e17391cb78",
                    "LayerId": "d8dbd597-d001-4a0a-a06d-801c4c4eba96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "d8dbd597-d001-4a0a-a06d-801c4c4eba96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31031f39-c545-4761-a782-10484180ad0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}