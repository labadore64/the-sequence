{
    "id": "9a6c2949-f200-45b7-adda-f6fff952b000",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_flatscreenTV",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 15,
    "bbox_right": 111,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c48d9f7-f488-4025-b512-5bb4163edfae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a6c2949-f200-45b7-adda-f6fff952b000",
            "compositeImage": {
                "id": "70b83ec1-0c0a-4b20-9e8c-4d02a3e2fa85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c48d9f7-f488-4025-b512-5bb4163edfae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42f2934-c177-4a2c-8006-b6ba57c17fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c48d9f7-f488-4025-b512-5bb4163edfae",
                    "LayerId": "d4452f6c-4799-4c63-9092-2c01202b9e2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d4452f6c-4799-4c63-9092-2c01202b9e2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a6c2949-f200-45b7-adda-f6fff952b000",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}