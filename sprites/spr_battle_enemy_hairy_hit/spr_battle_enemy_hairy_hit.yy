{
    "id": "72ae928d-5da4-4b5f-ab5c-d9cbb09e4dd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 73,
    "bbox_right": 199,
    "bbox_top": 86,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd94bfd7-9137-4bdd-a3e9-5b59d0cd6bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72ae928d-5da4-4b5f-ab5c-d9cbb09e4dd5",
            "compositeImage": {
                "id": "89e194bd-6bb6-492a-a4d2-52ce510cfb78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd94bfd7-9137-4bdd-a3e9-5b59d0cd6bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02a3abd4-eee4-40fb-b8b4-97c8e791d8e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd94bfd7-9137-4bdd-a3e9-5b59d0cd6bbb",
                    "LayerId": "236dcb32-6429-4c13-88db-f42c3cfba05e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "236dcb32-6429-4c13-88db-f42c3cfba05e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72ae928d-5da4-4b5f-ab5c-d9cbb09e4dd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}