{
    "id": "b8b88ece-61b6-47d7-a510-8e51c38afa76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_baffle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 51,
    "bbox_right": 226,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe54467c-2f37-4f66-9981-580591bc0d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8b88ece-61b6-47d7-a510-8e51c38afa76",
            "compositeImage": {
                "id": "92ad54d2-4a46-45b9-952c-469827daf03e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe54467c-2f37-4f66-9981-580591bc0d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662a44f6-3167-4841-9ddc-1cc75aa45149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe54467c-2f37-4f66-9981-580591bc0d4c",
                    "LayerId": "16e5c402-b686-4ff6-ae2a-19c8899eb427"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "16e5c402-b686-4ff6-ae2a-19c8899eb427",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8b88ece-61b6-47d7-a510-8e51c38afa76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 150
}