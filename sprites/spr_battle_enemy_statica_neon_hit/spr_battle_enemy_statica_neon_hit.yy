{
    "id": "0702e565-789f-4f5b-8417-9b98896b97b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_statica_neon_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 22,
    "bbox_right": 210,
    "bbox_top": 154,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "008da745-3507-4ee2-a268-15a9f789bfe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0702e565-789f-4f5b-8417-9b98896b97b8",
            "compositeImage": {
                "id": "52485d3c-6150-4841-b4dd-96251e1b2730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "008da745-3507-4ee2-a268-15a9f789bfe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59cfdfcf-e894-4cb7-aa59-b6058981f150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "008da745-3507-4ee2-a268-15a9f789bfe5",
                    "LayerId": "ca3c332e-66a9-4819-87aa-69178915bb89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ca3c332e-66a9-4819-87aa-69178915bb89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0702e565-789f-4f5b-8417-9b98896b97b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "02376ac4-5c41-4d41-862b-c0ab58d85802",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}