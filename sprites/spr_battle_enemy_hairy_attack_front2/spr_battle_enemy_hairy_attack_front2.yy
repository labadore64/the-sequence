{
    "id": "fdf52115-67c5-42f2-84f8-30e002db18b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_attack_front2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 53,
    "bbox_right": 219,
    "bbox_top": 62,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32c321f2-a233-49d3-b3ae-c81a38de918a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdf52115-67c5-42f2-84f8-30e002db18b0",
            "compositeImage": {
                "id": "8e730ec9-507a-4fdd-9776-ab4820ae3a13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c321f2-a233-49d3-b3ae-c81a38de918a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d6365d6-913d-4a27-8224-639ba7b6c44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c321f2-a233-49d3-b3ae-c81a38de918a",
                    "LayerId": "0c7c1fde-f93e-42eb-98d6-b740eb131605"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0c7c1fde-f93e-42eb-98d6-b740eb131605",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdf52115-67c5-42f2-84f8-30e002db18b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}