{
    "id": "3a9474f5-58fe-47c9-9a80-5c903da9f420",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sparkleBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 55,
    "bbox_right": 73,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fe68cf6-dc84-41fc-a306-a4ffe69961de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a9474f5-58fe-47c9-9a80-5c903da9f420",
            "compositeImage": {
                "id": "2be70acb-d95f-45c9-9301-56ac8565f9a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe68cf6-dc84-41fc-a306-a4ffe69961de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8905615a-852d-4b88-9644-cb4cf9008d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe68cf6-dc84-41fc-a306-a4ffe69961de",
                    "LayerId": "6671395e-4342-43ae-9a26-00f1b298a92f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6671395e-4342-43ae-9a26-00f1b298a92f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a9474f5-58fe-47c9-9a80-5c903da9f420",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}