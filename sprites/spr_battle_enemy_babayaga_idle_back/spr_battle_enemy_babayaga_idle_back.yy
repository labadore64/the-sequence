{
    "id": "0789023f-bf70-471f-b9b7-4a21fbf80479",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_babayaga_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 0,
    "bbox_right": 221,
    "bbox_top": 87,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a83de711-388a-4a54-8b86-443961fb522c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "a005ebc8-0a03-43a0-83e1-af79ea2d241e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a83de711-388a-4a54-8b86-443961fb522c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ea4ac9-5ccc-4f89-a536-bce8512845e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a83de711-388a-4a54-8b86-443961fb522c",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "64243371-5740-4e77-b3c7-4ebbbbe7d3de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "6ffa521c-9df7-45b5-84d3-11814d15d243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64243371-5740-4e77-b3c7-4ebbbbe7d3de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4faad1-8894-4a4d-8842-33f91cc836ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64243371-5740-4e77-b3c7-4ebbbbe7d3de",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "add69808-7bb2-4064-9942-5fd9e09f996b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "a888b874-9a43-4c48-90cf-f8884a56d204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "add69808-7bb2-4064-9942-5fd9e09f996b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b8aebc-8847-4f89-8cf1-f41ec015c1c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "add69808-7bb2-4064-9942-5fd9e09f996b",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "e847d5a6-f21c-43c6-a71b-5d709588b543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "14d72ab3-6b97-493e-a08b-99028460f9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e847d5a6-f21c-43c6-a71b-5d709588b543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "513f95ca-becf-473f-8a92-380b60989aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e847d5a6-f21c-43c6-a71b-5d709588b543",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "936d88f4-e1c5-4a5d-92f2-c3b7bf159f9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "75280d60-2b22-4653-a01f-13e44b7fdc06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "936d88f4-e1c5-4a5d-92f2-c3b7bf159f9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ea4465-b73f-4542-9019-8f3353362e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "936d88f4-e1c5-4a5d-92f2-c3b7bf159f9e",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "6604133e-6393-4c6d-9dd4-e6fb36a62f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "427415ad-6e73-46a5-90f1-fb2b9dc2e170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6604133e-6393-4c6d-9dd4-e6fb36a62f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3664aee5-a7a7-44da-b087-e518aa5ad0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6604133e-6393-4c6d-9dd4-e6fb36a62f46",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "30095af2-9de5-4080-ad74-7dca9aef9c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "419fd715-e01f-4e87-9291-9abde848a86e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30095af2-9de5-4080-ad74-7dca9aef9c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba34450-d306-4410-b84d-d27e1eef904a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30095af2-9de5-4080-ad74-7dca9aef9c76",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "18e8b172-0840-4e4b-8fe4-cf46b52714c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "2fb7365b-4c2f-4f3b-b33d-64c0ef033dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18e8b172-0840-4e4b-8fe4-cf46b52714c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cba9c64-1e43-44e0-bd52-288ed55dd854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18e8b172-0840-4e4b-8fe4-cf46b52714c4",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "be1cb28b-f874-4f6c-b74e-59c5a6b0cba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "e2dfd475-fc2f-4a6f-b924-8e3a6f18c4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1cb28b-f874-4f6c-b74e-59c5a6b0cba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0dea91-da1d-4e6b-a63f-ce98616e9bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1cb28b-f874-4f6c-b74e-59c5a6b0cba5",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "b8ab069c-2fe7-4a75-8732-77c1788488c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "84c255d3-3a2e-47da-96fd-b1388c3f2a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ab069c-2fe7-4a75-8732-77c1788488c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474d9324-8a02-49fa-ad0c-80cf48ec8867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ab069c-2fe7-4a75-8732-77c1788488c5",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "9a8495dc-5fbd-4cab-b48b-5ae2c9195247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "a439b8aa-62f0-4511-952d-c2193e31dc89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a8495dc-5fbd-4cab-b48b-5ae2c9195247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7793a3e-3403-4e1e-9c47-7340145eeff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a8495dc-5fbd-4cab-b48b-5ae2c9195247",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "6e2da76a-a442-4a8b-9e69-3eab50758692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "6680afba-acf8-47d7-9c1e-9d06acd3e1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e2da76a-a442-4a8b-9e69-3eab50758692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d8563a8-e640-4410-98ed-423bd39ee6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e2da76a-a442-4a8b-9e69-3eab50758692",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "7c3c3877-a891-4b71-b084-4a899a985a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "7830c73f-3d22-43b7-913e-1ed27ecd8912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c3c3877-a891-4b71-b084-4a899a985a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3695b712-51e2-472e-b9d3-b084854960ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c3c3877-a891-4b71-b084-4a899a985a53",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "349c2d8b-09db-4205-895e-cae497bca9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "c175d06f-5df7-4053-a4ac-90204cd53417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349c2d8b-09db-4205-895e-cae497bca9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11a4aa97-0c44-4b3e-abe2-b6f939c923d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349c2d8b-09db-4205-895e-cae497bca9b8",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "4341fa73-8939-4ec2-acf7-65afe4766670",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "2a1a48d2-c360-4dc2-bc8e-15712aa787dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4341fa73-8939-4ec2-acf7-65afe4766670",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9794890f-806b-4fb9-965a-d3f0edd72faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4341fa73-8939-4ec2-acf7-65afe4766670",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "6c4e09be-060f-4ebf-92be-b93f016606c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "1ebdf0f3-0f0d-4258-bf43-27cdf4eecb5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c4e09be-060f-4ebf-92be-b93f016606c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49661245-ddbb-409e-ac80-3f2a3d150647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c4e09be-060f-4ebf-92be-b93f016606c7",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "03904d84-2a29-4e40-8ffa-c83dd030dad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "48e65331-cdc5-4378-b148-762d40fb32cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03904d84-2a29-4e40-8ffa-c83dd030dad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cba4abc-05b7-4446-944d-89e1f1cb74a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03904d84-2a29-4e40-8ffa-c83dd030dad1",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "360fb0e5-07f6-4e24-85ee-38b0793ec791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "c21eda72-b1da-438c-8258-08812aa3f390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360fb0e5-07f6-4e24-85ee-38b0793ec791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7f288b-bc17-4322-8c2c-2b4f44d45703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360fb0e5-07f6-4e24-85ee-38b0793ec791",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "1daed433-7b4e-49d8-87ed-83f5c0a19f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "c355af18-4f90-4b8b-a111-3b5364e586e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daed433-7b4e-49d8-87ed-83f5c0a19f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6d3289-71c4-404a-b509-25dd29747c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daed433-7b4e-49d8-87ed-83f5c0a19f8b",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        },
        {
            "id": "93f7c192-03e1-4c9b-bb25-691143faf0b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "compositeImage": {
                "id": "a6f0c79c-6ee1-469c-8e83-51acfaf4cc4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93f7c192-03e1-4c9b-bb25-691143faf0b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5b468c-f225-4dc1-aab1-03d90c41bd02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93f7c192-03e1-4c9b-bb25-691143faf0b6",
                    "LayerId": "9b53309b-1ef0-43e6-9b60-21026cb3da8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9b53309b-1ef0-43e6-9b60-21026cb3da8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0789023f-bf70-471f-b9b7-4a21fbf80479",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "660d4a34-eb5a-49a4-93ee-8d23a3172db4",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}