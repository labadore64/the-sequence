{
    "id": "8bc534f3-822b-4a52-a3a0-7922839b0946",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 234,
    "bbox_left": 22,
    "bbox_right": 215,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a543988c-9466-4169-a8be-1d79ed603a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bc534f3-822b-4a52-a3a0-7922839b0946",
            "compositeImage": {
                "id": "6cc23973-ac83-4581-b99d-8cd12b18f194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a543988c-9466-4169-a8be-1d79ed603a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a8324fa-1995-4e6e-95bf-c5bff6679d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a543988c-9466-4169-a8be-1d79ed603a99",
                    "LayerId": "28d14159-a183-441d-940b-77be27525163"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "28d14159-a183-441d-940b-77be27525163",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bc534f3-822b-4a52-a3a0-7922839b0946",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}