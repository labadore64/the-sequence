{
    "id": "f208ad1b-d1f4-4aac-8e81-7ca410225156",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aspyre_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 6,
    "bbox_right": 249,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eada08fd-de86-46a0-923b-698e3a550354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f208ad1b-d1f4-4aac-8e81-7ca410225156",
            "compositeImage": {
                "id": "438d4cb2-5e68-4db9-9c91-6ae93b333196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eada08fd-de86-46a0-923b-698e3a550354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dfd0979-2e11-48e9-b865-655897140722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eada08fd-de86-46a0-923b-698e3a550354",
                    "LayerId": "618e4c59-033f-4f05-be85-4935c21ffe15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "618e4c59-033f-4f05-be85-4935c21ffe15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f208ad1b-d1f4-4aac-8e81-7ca410225156",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "808f5ba0-e9e9-4dd6-a3e1-5fd671722977",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}