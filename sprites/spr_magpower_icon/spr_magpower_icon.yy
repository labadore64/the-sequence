{
    "id": "c1c5f75e-3ecd-4e30-a038-2ee61ed1f678",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_magpower_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbf8e108-6513-4d8b-a448-1a51cbe7e900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1c5f75e-3ecd-4e30-a038-2ee61ed1f678",
            "compositeImage": {
                "id": "ad1d3363-7330-4c6f-97bd-6aa709e35102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf8e108-6513-4d8b-a448-1a51cbe7e900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774b397f-ec4b-45e6-acdb-5237c07aaffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf8e108-6513-4d8b-a448-1a51cbe7e900",
                    "LayerId": "9613b825-a369-4cea-9cfd-da3bbec2b471"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9613b825-a369-4cea-9cfd-da3bbec2b471",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1c5f75e-3ecd-4e30-a038-2ee61ed1f678",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}