{
    "id": "9f402848-a8ec-4782-aefa-8f9db0276a71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cellphone_signal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cd97435-23ea-42e2-87a2-b457f822c657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f402848-a8ec-4782-aefa-8f9db0276a71",
            "compositeImage": {
                "id": "c22266a4-e2be-4994-a33a-32af7c8d7727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd97435-23ea-42e2-87a2-b457f822c657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "143c8a6c-e80a-4ac6-bcf2-12bd62c1bdf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd97435-23ea-42e2-87a2-b457f822c657",
                    "LayerId": "763abb0a-a6df-431b-aee9-eb8820026148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "763abb0a-a6df-431b-aee9-eb8820026148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f402848-a8ec-4782-aefa-8f9db0276a71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}