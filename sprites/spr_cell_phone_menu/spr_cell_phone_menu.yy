{
    "id": "90a0b373-0c2a-41be-a924-7fe924ea7d2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cell_phone_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 135,
    "bbox_right": 664,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e653e5da-0e34-403c-84c6-a132616fe67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a0b373-0c2a-41be-a924-7fe924ea7d2d",
            "compositeImage": {
                "id": "29ad4fca-f2bd-40e4-bc5c-856ee46a5c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e653e5da-0e34-403c-84c6-a132616fe67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ec6bf5-40d8-4cfd-898e-5537e9eaf32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e653e5da-0e34-403c-84c6-a132616fe67e",
                    "LayerId": "c93d2f94-c676-43e0-b8ee-9164470dbd9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "c93d2f94-c676-43e0-b8ee-9164470dbd9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90a0b373-0c2a-41be-a924-7fe924ea7d2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}