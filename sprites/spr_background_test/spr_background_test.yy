{
    "id": "588d4ece-8351-4c85-8733-26c3a1d51840",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29108f4d-96e6-45e4-90c4-ebf3ccc7bb66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "588d4ece-8351-4c85-8733-26c3a1d51840",
            "compositeImage": {
                "id": "8f43e0ee-b803-4a20-88b2-767aee40ac1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29108f4d-96e6-45e4-90c4-ebf3ccc7bb66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d7e46a8-cc75-4416-8fd8-d170411850ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29108f4d-96e6-45e4-90c4-ebf3ccc7bb66",
                    "LayerId": "14d0e69a-5d97-432f-be24-c3b81a2aceaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "14d0e69a-5d97-432f-be24-c3b81a2aceaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "588d4ece-8351-4c85-8733-26c3a1d51840",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}