{
    "id": "ca461fdb-6033-40ad-a18a-fb5e31f150f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_mute",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 80,
    "bbox_right": 178,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21264e4f-a09d-4716-b88c-ea6c0e98ade8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca461fdb-6033-40ad-a18a-fb5e31f150f3",
            "compositeImage": {
                "id": "d72b09a9-fa30-4fec-af78-bfaa6f02a3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21264e4f-a09d-4716-b88c-ea6c0e98ade8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b9eb9b-7851-4b01-8dc3-0e16440fb0ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21264e4f-a09d-4716-b88c-ea6c0e98ade8",
                    "LayerId": "5ba23052-a952-4105-a69f-68cd5762b593"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5ba23052-a952-4105-a69f-68cd5762b593",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca461fdb-6033-40ad-a18a-fb5e31f150f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 220
}