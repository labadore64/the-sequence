{
    "id": "25584a23-312c-4aab-b284-b7cea996d5ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_dad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 26,
    "bbox_right": 106,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1b3c2a2-e0fa-4c69-8f6d-a6b846d9cd54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25584a23-312c-4aab-b284-b7cea996d5ac",
            "compositeImage": {
                "id": "11504ba1-ad9e-47f8-a2f1-3b4fbccbf666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b3c2a2-e0fa-4c69-8f6d-a6b846d9cd54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd0b18a9-d775-4801-bf8b-9d54ff2a8c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b3c2a2-e0fa-4c69-8f6d-a6b846d9cd54",
                    "LayerId": "194ca35d-0b02-4cc0-90ec-aa095e8899dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "194ca35d-0b02-4cc0-90ec-aa095e8899dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25584a23-312c-4aab-b284-b7cea996d5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}