{
    "id": "53a78314-1e28-4958-ab2e-8d05647f1f00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_cast_front2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 38,
    "bbox_right": 243,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff6e9b8f-18c8-483f-9560-a26f092fba1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a78314-1e28-4958-ab2e-8d05647f1f00",
            "compositeImage": {
                "id": "8bdb1092-8c0d-4c6e-ba5c-3dc42f907d03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6e9b8f-18c8-483f-9560-a26f092fba1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2f560b-40b4-4c5f-aa5c-8eb1ad7f158e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6e9b8f-18c8-483f-9560-a26f092fba1a",
                    "LayerId": "beecb743-981d-43e2-8eb0-71ec22622dbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "beecb743-981d-43e2-8eb0-71ec22622dbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53a78314-1e28-4958-ab2e-8d05647f1f00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}