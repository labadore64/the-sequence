{
    "id": "ea3c797e-5df5-4002-b69f-90585198c9a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_binoculars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 232,
    "bbox_left": 9,
    "bbox_right": 237,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d0eda6e-61b2-446a-b641-9358e7b59122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea3c797e-5df5-4002-b69f-90585198c9a6",
            "compositeImage": {
                "id": "c8c04363-1da7-4612-87fc-e0f3c0f6fa11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0eda6e-61b2-446a-b641-9358e7b59122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a9fb72-6b91-4789-aba9-c8e7e69c2527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0eda6e-61b2-446a-b641-9358e7b59122",
                    "LayerId": "6b16a597-25d7-4d3e-b479-113bdf472aea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6b16a597-25d7-4d3e-b479-113bdf472aea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea3c797e-5df5-4002-b69f-90585198c9a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}