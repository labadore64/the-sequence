{
    "id": "dcaf7a35-2943-441f-b416-49b61afd76b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "braillecell_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a81124f7-b552-4c84-8e29-2d85f780c57c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcaf7a35-2943-441f-b416-49b61afd76b3",
            "compositeImage": {
                "id": "8cff88b6-6667-493b-828d-4c7328385b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a81124f7-b552-4c84-8e29-2d85f780c57c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a49b34b-ef7c-4dee-ae93-184af1c1e3a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a81124f7-b552-4c84-8e29-2d85f780c57c",
                    "LayerId": "d8c6f4aa-dceb-45a4-8143-79b62d527133"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d8c6f4aa-dceb-45a4-8143-79b62d527133",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcaf7a35-2943-441f-b416-49b61afd76b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}