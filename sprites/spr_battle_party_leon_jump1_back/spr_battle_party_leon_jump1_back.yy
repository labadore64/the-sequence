{
    "id": "b9e4dc2c-ed32-49f1-b7fa-def13ebb2885",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 33,
    "bbox_right": 219,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "510a1f86-0575-4514-9fe5-83aefb741b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9e4dc2c-ed32-49f1-b7fa-def13ebb2885",
            "compositeImage": {
                "id": "bb0bcda5-1131-416d-b10b-27d7453e1d90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510a1f86-0575-4514-9fe5-83aefb741b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30e6329-0258-4a4f-8496-c9bdc5ca17f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510a1f86-0575-4514-9fe5-83aefb741b80",
                    "LayerId": "6a1e0629-acf9-40f3-9199-f8667a27f0d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6a1e0629-acf9-40f3-9199-f8667a27f0d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9e4dc2c-ed32-49f1-b7fa-def13ebb2885",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}