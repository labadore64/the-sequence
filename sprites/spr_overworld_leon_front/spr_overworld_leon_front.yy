{
    "id": "f3ab0717-d5f5-4421-a9bf-245c6259bd48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld_leon_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 44,
    "bbox_right": 204,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ea149ab-1358-4c7e-bf99-b5f573f2c34f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3ab0717-d5f5-4421-a9bf-245c6259bd48",
            "compositeImage": {
                "id": "08789def-93c6-4df7-b7cf-621d6878a3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea149ab-1358-4c7e-bf99-b5f573f2c34f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e61ea28-0dd0-4f61-babf-f2997841bd04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea149ab-1358-4c7e-bf99-b5f573f2c34f",
                    "LayerId": "250280fb-6975-4882-abb3-d900dd89d908"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "250280fb-6975-4882-abb3-d900dd89d908",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3ab0717-d5f5-4421-a9bf-245c6259bd48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 145
}