{
    "id": "8cf50ff1-b867-4ab9-8ed7-79e27653381d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_rubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 17,
    "bbox_right": 114,
    "bbox_top": 67,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a753fee9-4272-40d8-abd6-74bd3fbe175f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cf50ff1-b867-4ab9-8ed7-79e27653381d",
            "compositeImage": {
                "id": "dc45fd4d-1c34-4db5-8920-5773ffd416a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a753fee9-4272-40d8-abd6-74bd3fbe175f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0bf3597-faf1-4ea5-b7a5-4dfabadad3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a753fee9-4272-40d8-abd6-74bd3fbe175f",
                    "LayerId": "096b1f7f-953d-48be-a263-3bd7bfafed5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "096b1f7f-953d-48be-a263-3bd7bfafed5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cf50ff1-b867-4ab9-8ed7-79e27653381d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}