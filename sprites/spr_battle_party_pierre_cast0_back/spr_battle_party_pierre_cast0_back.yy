{
    "id": "002b0f61-57fa-456c-af0d-54197fbca0e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_cast0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 36,
    "bbox_right": 205,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6504d322-7125-4a8e-ac00-61b16cd090d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "002b0f61-57fa-456c-af0d-54197fbca0e6",
            "compositeImage": {
                "id": "0fa42f04-9d2a-4590-beca-f8fcb69c5703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6504d322-7125-4a8e-ac00-61b16cd090d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e08e98b-6187-4cc5-b487-835077243be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6504d322-7125-4a8e-ac00-61b16cd090d5",
                    "LayerId": "f59353d4-7fb0-40ec-ad1c-702b9000bb66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f59353d4-7fb0-40ec-ad1c-702b9000bb66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "002b0f61-57fa-456c-af0d-54197fbca0e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}