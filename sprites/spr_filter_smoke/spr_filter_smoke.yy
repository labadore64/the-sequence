{
    "id": "000b2c9d-a325-4ea2-898a-25b0eefdfff3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 192,
    "bbox_left": 36,
    "bbox_right": 233,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b10da581-8fd9-4554-82d1-2b1b74b8b864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "000b2c9d-a325-4ea2-898a-25b0eefdfff3",
            "compositeImage": {
                "id": "9f1a4b14-0767-4504-b374-348932925dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10da581-8fd9-4554-82d1-2b1b74b8b864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "193230a9-3de2-4aca-85e5-72777efca634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10da581-8fd9-4554-82d1-2b1b74b8b864",
                    "LayerId": "ee715782-ce2f-4b24-89bb-81502e0c4913"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ee715782-ce2f-4b24-89bb-81502e0c4913",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "000b2c9d-a325-4ea2-898a-25b0eefdfff3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 3.29225016,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}