{
    "id": "0a949f8f-d1e1-42fa-8e92-1173faf12794",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_bluejay_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 8,
    "bbox_right": 127,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2cca2098-477f-465d-8dc0-d64fd5a2b9d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a949f8f-d1e1-42fa-8e92-1173faf12794",
            "compositeImage": {
                "id": "37409e62-77b7-4f83-9c94-17fb52beb9d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cca2098-477f-465d-8dc0-d64fd5a2b9d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0db57a6-74a6-44d5-83f5-be2b661beb99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cca2098-477f-465d-8dc0-d64fd5a2b9d7",
                    "LayerId": "dc8060b2-3e53-46ee-9e95-f372dd67fd9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "dc8060b2-3e53-46ee-9e95-f372dd67fd9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a949f8f-d1e1-42fa-8e92-1173faf12794",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}