{
    "id": "a1db9dbf-a89e-4f10-a92c-1a52ad4c4aa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_self_snack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6479864d-5ebc-4704-8ab5-a29687043cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1db9dbf-a89e-4f10-a92c-1a52ad4c4aa4",
            "compositeImage": {
                "id": "1286301d-b3cf-4263-9c28-2a445c60c3b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6479864d-5ebc-4704-8ab5-a29687043cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e34ab36-f85c-4f85-8861-0400f87fe9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6479864d-5ebc-4704-8ab5-a29687043cd7",
                    "LayerId": "479df5bf-7168-4585-8c4e-947784ec7b7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "479df5bf-7168-4585-8c4e-947784ec7b7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1db9dbf-a89e-4f10-a92c-1a52ad4c4aa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}