{
    "id": "db9d0286-6f5e-40c0-8e29-df9b1d1b3dd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mode_challenge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44a306c5-9738-47f1-bb96-63e77f09fdec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db9d0286-6f5e-40c0-8e29-df9b1d1b3dd8",
            "compositeImage": {
                "id": "67904b37-2a92-431a-873b-bb04bfaf14e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a306c5-9738-47f1-bb96-63e77f09fdec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72d5ce51-8805-44f5-9d0a-9318b754de69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a306c5-9738-47f1-bb96-63e77f09fdec",
                    "LayerId": "261e3286-c5ec-478a-a109-85936582e71c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "261e3286-c5ec-478a-a109-85936582e71c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db9d0286-6f5e-40c0-8e29-df9b1d1b3dd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}