{
    "id": "c2681f00-682a-4f09-ad8b-a04b3fab963c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_cooClock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 37,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d08396-bccd-4f4d-89fe-44c18a547d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2681f00-682a-4f09-ad8b-a04b3fab963c",
            "compositeImage": {
                "id": "395922f4-55b2-4555-8e81-0813a047643f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d08396-bccd-4f4d-89fe-44c18a547d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95163137-4fc0-482c-8cad-797ea4e2b419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d08396-bccd-4f4d-89fe-44c18a547d8b",
                    "LayerId": "aed62ff1-8ce9-47f8-a0fc-f0c98cb33ca8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aed62ff1-8ce9-47f8-a0fc-f0c98cb33ca8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2681f00-682a-4f09-ad8b-a04b3fab963c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}