{
    "id": "fc3421f1-4442-4e0d-a659-262c97a4db44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_cast1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 222,
    "bbox_left": 5,
    "bbox_right": 243,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27871a4e-901e-4cc5-9117-be40f9d1afc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc3421f1-4442-4e0d-a659-262c97a4db44",
            "compositeImage": {
                "id": "bd08e6cd-b978-4610-956d-f87ffcc10600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27871a4e-901e-4cc5-9117-be40f9d1afc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d60c64-05ab-42df-b471-0231e95d8d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27871a4e-901e-4cc5-9117-be40f9d1afc1",
                    "LayerId": "6e2207dc-945a-43b1-aa75-8912d9e59edc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6e2207dc-945a-43b1-aa75-8912d9e59edc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc3421f1-4442-4e0d-a659-262c97a4db44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}