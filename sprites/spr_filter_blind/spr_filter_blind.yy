{
    "id": "47e900de-77ca-4c80-9467-3b5aa90a12c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_blind",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 80,
    "bbox_right": 178,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a141634-8997-433a-a3b1-bb07706bc7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47e900de-77ca-4c80-9467-3b5aa90a12c5",
            "compositeImage": {
                "id": "87a78c08-262b-4bb3-b764-7c87b112dee6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a141634-8997-433a-a3b1-bb07706bc7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d4465a-487d-48df-9cdd-04af864a54bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a141634-8997-433a-a3b1-bb07706bc7e4",
                    "LayerId": "fc11b725-6c8f-4240-b927-c948feca4ca2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fc11b725-6c8f-4240-b927-c948feca4ca2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47e900de-77ca-4c80-9467-3b5aa90a12c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 220
}