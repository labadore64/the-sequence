{
    "id": "f637de93-58ac-413f-b212-ed702a6d8c0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e154fda5-edbe-4119-aa6d-4e77b23fd331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f637de93-58ac-413f-b212-ed702a6d8c0a",
            "compositeImage": {
                "id": "833a563f-5ee7-49f3-8dc0-21c5bcba5870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e154fda5-edbe-4119-aa6d-4e77b23fd331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "915635e7-9972-48d0-8a7c-f5d64572535d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e154fda5-edbe-4119-aa6d-4e77b23fd331",
                    "LayerId": "92327b97-2bd2-47ce-a482-3f75a005b2dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "92327b97-2bd2-47ce-a482-3f75a005b2dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f637de93-58ac-413f-b212-ed702a6d8c0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 33,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}