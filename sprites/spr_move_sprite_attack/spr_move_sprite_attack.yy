{
    "id": "c5da4b17-76c3-4bb1-ac85-87cd34999157",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_sprite_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3c4fc99-9f2f-4d7b-ad9b-51fc3da3fdd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5da4b17-76c3-4bb1-ac85-87cd34999157",
            "compositeImage": {
                "id": "7b8ffccd-2542-4ba9-9c83-d316f2f95c4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3c4fc99-9f2f-4d7b-ad9b-51fc3da3fdd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf38080-246c-4079-b3ee-c0d66b4c7651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3c4fc99-9f2f-4d7b-ad9b-51fc3da3fdd1",
                    "LayerId": "41310002-e75f-4a3a-bc7b-6c9006bbe8e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "41310002-e75f-4a3a-bc7b-6c9006bbe8e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5da4b17-76c3-4bb1-ac85-87cd34999157",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}