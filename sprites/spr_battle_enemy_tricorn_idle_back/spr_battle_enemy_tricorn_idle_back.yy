{
    "id": "d84153ae-d763-44b3-8604-d18289702df6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 36,
    "bbox_right": 213,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c70aac9d-3e64-4b22-9c2e-f3fe2bcee8cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "414f99e7-a110-4f25-8d38-2a1111a9c39a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70aac9d-3e64-4b22-9c2e-f3fe2bcee8cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda8b442-3deb-48b5-91b4-c9323dd4c419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70aac9d-3e64-4b22-9c2e-f3fe2bcee8cb",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "f615a53a-aa7f-4703-bc93-4b89f7bd0bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "a70ff3d2-5b83-40e9-bbca-c23e43463667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f615a53a-aa7f-4703-bc93-4b89f7bd0bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15cc4563-7f05-413b-b78a-f6816fbe98a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f615a53a-aa7f-4703-bc93-4b89f7bd0bcf",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "d6ebcaf8-dd31-40d6-bae7-53faa3aa4bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "c4983423-82d5-4099-9053-b0f8d8ad94ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ebcaf8-dd31-40d6-bae7-53faa3aa4bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a6d1cce-60f5-45f6-8414-98cd456c09ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ebcaf8-dd31-40d6-bae7-53faa3aa4bc7",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "11936ac5-5ec8-4f62-aed8-b7af927f95ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "ad402197-d909-434d-b4fc-fcff48a26555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11936ac5-5ec8-4f62-aed8-b7af927f95ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f4ceb11-c4c7-4646-812e-b78c0770e5ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11936ac5-5ec8-4f62-aed8-b7af927f95ff",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "0643bbf3-6154-4e2f-8834-5209d82eda6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "6c266b52-abc9-4d18-bb6c-f677a5dc2c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0643bbf3-6154-4e2f-8834-5209d82eda6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2892b251-e1de-44f1-930a-b899374dcb62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0643bbf3-6154-4e2f-8834-5209d82eda6d",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "b168124b-80a6-4308-9b62-ef431125c933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "08201211-6925-4cc1-a09d-f402b52df3b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b168124b-80a6-4308-9b62-ef431125c933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563eda3b-9035-4d04-8a44-1bbe893828ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b168124b-80a6-4308-9b62-ef431125c933",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "e8bd8b0c-1b24-4e40-a5b6-d4ff4ad0a8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "49eba296-b8e6-493a-afa2-993b60d210be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8bd8b0c-1b24-4e40-a5b6-d4ff4ad0a8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1925e80f-3e92-4ab2-b8b1-37494be182dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8bd8b0c-1b24-4e40-a5b6-d4ff4ad0a8ea",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "6e7bbff5-d3df-42d4-9cf4-50c9cc5f2b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "c8b57cb0-cebc-4946-bc65-f5843bdaaa26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7bbff5-d3df-42d4-9cf4-50c9cc5f2b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec95999f-84a0-4f0d-a6f0-b9d4a2738713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7bbff5-d3df-42d4-9cf4-50c9cc5f2b2c",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "6f5d5144-5458-4ea3-9801-4e74d41fc63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "f69b1a1d-be5a-4342-a8f0-b4634c43875d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f5d5144-5458-4ea3-9801-4e74d41fc63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d814aea7-4224-4963-a83e-cf5e9367470f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f5d5144-5458-4ea3-9801-4e74d41fc63c",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "9e005384-46a1-4e66-aeed-910f7de738c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "2182fb85-265b-4909-a19b-eb9d96c777ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e005384-46a1-4e66-aeed-910f7de738c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "746085d3-fad9-4e0b-8b7c-0707d87aa659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e005384-46a1-4e66-aeed-910f7de738c0",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "d5215b9c-7ee0-41ee-98c1-9ea21cac4697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "23a40f25-7f8d-46fd-b06e-19972a5939fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5215b9c-7ee0-41ee-98c1-9ea21cac4697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82259e9d-4c56-4349-890b-1ace4b3dfb48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5215b9c-7ee0-41ee-98c1-9ea21cac4697",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "6cd1e990-582e-4cec-97f1-dffc5795ef1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "8de58119-1d95-4f2e-94f9-9bf5d689478d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd1e990-582e-4cec-97f1-dffc5795ef1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e69e7915-e183-4435-ade9-0e23b39120de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd1e990-582e-4cec-97f1-dffc5795ef1b",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "ef4fb353-2d16-434e-bf41-8702254e9b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "3d880580-eb93-4ce7-bf79-44773c12738c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4fb353-2d16-434e-bf41-8702254e9b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e812241f-8f2a-44ba-818b-a7c7a02c56a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4fb353-2d16-434e-bf41-8702254e9b89",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "7a13a7dd-c924-4425-8eb5-300c975012cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "e1042d81-9eed-4e03-a971-237d3b57c747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a13a7dd-c924-4425-8eb5-300c975012cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3a1bba-41d5-4bf8-867f-181a51015753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a13a7dd-c924-4425-8eb5-300c975012cb",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "729efe68-fd1b-4ebb-8c27-fd8cdcd9137c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "2f602831-c6e5-4deb-a3f6-a752421ed9d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "729efe68-fd1b-4ebb-8c27-fd8cdcd9137c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88f0d9d0-be58-4e3c-8909-ea260a35ad26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "729efe68-fd1b-4ebb-8c27-fd8cdcd9137c",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "ef2f59b7-8f9a-41de-9c00-9432855eb6b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "91eee255-7337-4e30-ae33-36e1eec64218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2f59b7-8f9a-41de-9c00-9432855eb6b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4c7da6-ef11-4064-8282-7d1b08f4ea6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2f59b7-8f9a-41de-9c00-9432855eb6b0",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "60e1a402-7723-4c8c-89e6-4c888a73bf03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "8c7dbab4-1541-405c-a3b5-89804194998f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60e1a402-7723-4c8c-89e6-4c888a73bf03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d633505-20e7-4c2c-bad2-0f0bda7dd4da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60e1a402-7723-4c8c-89e6-4c888a73bf03",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        },
        {
            "id": "d979c7cd-aa06-467e-a4a8-c57812e3bf7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "compositeImage": {
                "id": "b3a04be0-c64a-43f9-bfee-1032201131cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d979c7cd-aa06-467e-a4a8-c57812e3bf7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32946ab7-cda8-4040-bdd8-55b4daa748c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d979c7cd-aa06-467e-a4a8-c57812e3bf7c",
                    "LayerId": "8398e02a-1928-4bef-8bde-fcafd1beb1f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8398e02a-1928-4bef-8bde-fcafd1beb1f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d84153ae-d763-44b3-8604-d18289702df6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}