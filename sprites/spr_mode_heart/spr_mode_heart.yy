{
    "id": "ffc3fd96-8ba5-4745-b630-5c96b1dda59b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mode_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c56b619-f94b-4c2d-9a3c-11c1f75ab9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffc3fd96-8ba5-4745-b630-5c96b1dda59b",
            "compositeImage": {
                "id": "f0ad15c5-1b87-4533-b07d-682148826da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c56b619-f94b-4c2d-9a3c-11c1f75ab9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc15920c-6cdf-47ef-a676-6fb611dc9eb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c56b619-f94b-4c2d-9a3c-11c1f75ab9e1",
                    "LayerId": "1b43820f-d725-499f-945d-c9c77511704c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b43820f-d725-499f-945d-c9c77511704c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffc3fd96-8ba5-4745-b630-5c96b1dda59b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}