{
    "id": "3547b613-caec-4c80-b64f-f6e3bbcdbbcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menubg_option_main",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 134,
    "bbox_right": 680,
    "bbox_top": 115,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37aab677-66f2-4edc-aa20-fdc062570c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3547b613-caec-4c80-b64f-f6e3bbcdbbcb",
            "compositeImage": {
                "id": "d411704c-d61e-4c07-9205-6dea2ce975fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37aab677-66f2-4edc-aa20-fdc062570c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f8b145-3684-471d-b324-832c61c81e12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37aab677-66f2-4edc-aa20-fdc062570c98",
                    "LayerId": "4345107c-484e-4872-b418-fab2a03db554"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "4345107c-484e-4872-b418-fab2a03db554",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3547b613-caec-4c80-b64f-f6e3bbcdbbcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 400,
    "yorig": 300
}