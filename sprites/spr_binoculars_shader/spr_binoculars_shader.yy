{
    "id": "446b5000-4416-415b-b7de-4e8dc6cc8985",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_binoculars_shader",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d28d67d-29fc-4bb1-965a-66f31f7f42e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "446b5000-4416-415b-b7de-4e8dc6cc8985",
            "compositeImage": {
                "id": "f841a1f0-5593-4abc-a72c-0ad035e65b05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d28d67d-29fc-4bb1-965a-66f31f7f42e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "592be1a7-61aa-4b9e-9c01-a6549928f888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d28d67d-29fc-4bb1-965a-66f31f7f42e7",
                    "LayerId": "baa0501f-6b31-455e-a49d-9c49ccf5c36b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "baa0501f-6b31-455e-a49d-9c49ccf5c36b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "446b5000-4416-415b-b7de-4e8dc6cc8985",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6ede7562-7c5c-4143-a366-d84b74c40ab8",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}