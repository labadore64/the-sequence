{
    "id": "aa5357d0-a600-44b3-a130-a506e059aa81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_leon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 122,
    "bbox_right": 436,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "455c6317-c32a-4948-9643-e2902877ef41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa5357d0-a600-44b3-a130-a506e059aa81",
            "compositeImage": {
                "id": "382909f9-dc8e-4b12-86dd-06728edaef36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455c6317-c32a-4948-9643-e2902877ef41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9600574e-bc3b-409c-911b-3d2d46cdd11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455c6317-c32a-4948-9643-e2902877ef41",
                    "LayerId": "ed62a2a5-8c0f-4382-bcfa-e5b610ffb92b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "ed62a2a5-8c0f-4382-bcfa-e5b610ffb92b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa5357d0-a600-44b3-a130-a506e059aa81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}