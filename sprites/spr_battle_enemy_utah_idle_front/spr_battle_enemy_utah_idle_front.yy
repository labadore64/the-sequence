{
    "id": "a1198247-959f-431b-9970-810e0b1c9f9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 36,
    "bbox_right": 248,
    "bbox_top": 78,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d0910ff-afee-454d-9241-2ec94e505b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "cc399c76-b589-4f13-b341-981fe6cd2ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0910ff-afee-454d-9241-2ec94e505b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70f38df-1321-476d-a7a1-bb83b6ca7b5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0910ff-afee-454d-9241-2ec94e505b59",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "73da5a1c-d8ae-429d-8ed8-9a120b0b4e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "206584ab-738a-4708-a0e4-2a4ae0f33385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73da5a1c-d8ae-429d-8ed8-9a120b0b4e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "976d7247-14fd-4685-8aac-5e7c8db06fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73da5a1c-d8ae-429d-8ed8-9a120b0b4e39",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "dabca4e9-b059-463a-a056-fac7466eaf7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "b3bb1ecc-1ff8-4d0d-b1de-5416ede7ee4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabca4e9-b059-463a-a056-fac7466eaf7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710b2890-d0af-4e2c-9415-7822dcd17bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabca4e9-b059-463a-a056-fac7466eaf7b",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "3b7bb5e5-062d-46c5-b001-6f434b0fafce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "7e709549-28ea-4bbe-8735-6f7d7ab56232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7bb5e5-062d-46c5-b001-6f434b0fafce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65db68ce-e17f-459a-9310-8c747a2d5473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7bb5e5-062d-46c5-b001-6f434b0fafce",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "a1615e44-aaf4-4c0d-b7f7-173321765562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "90d8d45a-a9c5-4f17-bf1c-21f0e4d9cfb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1615e44-aaf4-4c0d-b7f7-173321765562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "459873ec-82d4-40e9-9179-b03eb76bc223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1615e44-aaf4-4c0d-b7f7-173321765562",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "109de50c-3757-4b0b-8eae-3f9899d0a5bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "3a3ecac8-f89d-43f6-ac18-2b1b20a94143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109de50c-3757-4b0b-8eae-3f9899d0a5bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08e82332-27a6-468b-a711-bb71ef3699a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109de50c-3757-4b0b-8eae-3f9899d0a5bc",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "2fe8ed29-67fe-4c69-bfe2-682b293154e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "2bb41b2a-1abc-411d-a848-e1fe09df11eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe8ed29-67fe-4c69-bfe2-682b293154e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472c21ad-ef84-4671-a654-4e88566748dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe8ed29-67fe-4c69-bfe2-682b293154e4",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "b48c72e3-b625-4c29-bbf9-6c29e4df7f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "6fe6b183-093b-4683-a5a0-5b3b75abb882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48c72e3-b625-4c29-bbf9-6c29e4df7f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88272ed-c773-4d26-83c1-6dbca06d982a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48c72e3-b625-4c29-bbf9-6c29e4df7f01",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "2ce5969c-ab9b-4066-b53c-c758ebd5ed6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "7814d177-f90f-4931-8700-58090d2920d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce5969c-ab9b-4066-b53c-c758ebd5ed6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7212b94-0f90-452b-8460-1d18b4b1ce48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce5969c-ab9b-4066-b53c-c758ebd5ed6b",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "5c16d774-1224-4b1b-a0b0-bb945361e511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "8698b066-2dd9-4c30-8828-2befc3718aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c16d774-1224-4b1b-a0b0-bb945361e511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d18e0e2-fd85-4dcb-be4d-4925c491867b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c16d774-1224-4b1b-a0b0-bb945361e511",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "a1333a46-7c12-4402-a7d8-f21ff4c25c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "d7478021-bda4-4ced-ac5a-d02801f9a889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1333a46-7c12-4402-a7d8-f21ff4c25c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "886cc4da-4896-4c8d-880e-4000e1d6411e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1333a46-7c12-4402-a7d8-f21ff4c25c8a",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "a84a6333-3f98-4a9a-a9dc-decf02e22798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "2e5d3515-da6a-4f8f-ad84-c52a2902e12c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84a6333-3f98-4a9a-a9dc-decf02e22798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1629d173-717a-45d2-bc6a-44c0bead4a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84a6333-3f98-4a9a-a9dc-decf02e22798",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "993fe127-58f4-4e6d-8fa5-2f1d8f267982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "9bfd36e4-6c64-48ff-8df9-3f34ed5b496e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "993fe127-58f4-4e6d-8fa5-2f1d8f267982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c83973b-0041-49a4-96fd-4a40999045e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "993fe127-58f4-4e6d-8fa5-2f1d8f267982",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "77356469-a156-462d-9f76-2a51ed908cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "bace604a-4a00-48fa-b8b3-5bd91283b682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77356469-a156-462d-9f76-2a51ed908cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93eb3a51-50bb-4814-b60e-ffce6518ffb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77356469-a156-462d-9f76-2a51ed908cf6",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "063f99a3-6ef1-469e-95b7-2498e5906b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "c406f808-6be3-4220-ac80-aae6e7448089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063f99a3-6ef1-469e-95b7-2498e5906b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccaeeb3a-2172-4a5f-ba45-3d2c3a158c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063f99a3-6ef1-469e-95b7-2498e5906b94",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "737c2ce2-8ec9-4a02-b1a6-80987e7cb2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "3b922ab9-9c37-4d98-9d03-035e00596ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "737c2ce2-8ec9-4a02-b1a6-80987e7cb2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f499d635-1b32-4494-9e4b-d7d7769eefac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "737c2ce2-8ec9-4a02-b1a6-80987e7cb2e5",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "ca69a95e-6eda-4754-843b-d90d56a1fd6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "727d6166-1faf-42bb-b92e-89a9be0ea39e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca69a95e-6eda-4754-843b-d90d56a1fd6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae219d07-9ea3-493a-8e28-e391990111c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca69a95e-6eda-4754-843b-d90d56a1fd6c",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "3f832aaa-7cd2-425b-b816-3c9bee7ea5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "dbe803ac-4d8c-456d-9d2f-87094689fe74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f832aaa-7cd2-425b-b816-3c9bee7ea5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "390f2b42-ffe1-4aae-a2ac-cb6f9e31f776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f832aaa-7cd2-425b-b816-3c9bee7ea5c0",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "302a60f7-3bbf-4946-b8ba-64050c901449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "5302dc2b-e496-4629-ab7d-06ee77d5768f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302a60f7-3bbf-4946-b8ba-64050c901449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e24a11-d6c5-4291-a537-8e7d53e5d5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302a60f7-3bbf-4946-b8ba-64050c901449",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "cba41af2-1e1f-4143-93bb-9286291e290f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "7d68d4cb-13cb-4f1e-84e5-eb1de17906aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba41af2-1e1f-4143-93bb-9286291e290f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5621e776-0d50-40a0-a0ac-d890b3e5be4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba41af2-1e1f-4143-93bb-9286291e290f",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "d30242db-1750-4f22-a5da-a48d2ee70102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "d7e84746-6b30-425c-a1d4-f4a81ebdcfc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d30242db-1750-4f22-a5da-a48d2ee70102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0286c916-0a70-4b1e-9371-2cab3ced25b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d30242db-1750-4f22-a5da-a48d2ee70102",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "5129ea4b-c59d-4c8f-beb2-8db5b31edb1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "730828ce-3f23-42c3-bd7d-fa76c65c582a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5129ea4b-c59d-4c8f-beb2-8db5b31edb1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b5dd7b-005c-4229-a5f8-a0d886fb8412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5129ea4b-c59d-4c8f-beb2-8db5b31edb1a",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "7635744b-7b96-45cc-be82-4d40a9bccfbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "8ff824f1-833b-4144-927b-ff4ea66c7a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7635744b-7b96-45cc-be82-4d40a9bccfbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07690f9b-71ab-410f-a46e-40742ea24b95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7635744b-7b96-45cc-be82-4d40a9bccfbb",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "81d119bd-08bf-467f-ad0e-a8b4d2c2fef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "d6b9dd39-7e61-41fb-8cbf-c6b96bf57ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d119bd-08bf-467f-ad0e-a8b4d2c2fef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0557739-419d-4931-8f39-586236267a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d119bd-08bf-467f-ad0e-a8b4d2c2fef0",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "b4cc1f7f-b91a-4fb2-a09e-ebb353655e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "eb5a3986-343a-4d8d-8efc-c15a179fee1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4cc1f7f-b91a-4fb2-a09e-ebb353655e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f542c790-e752-4762-8f13-5cd3920361af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4cc1f7f-b91a-4fb2-a09e-ebb353655e6f",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "aa27e133-7b3c-4854-b8c1-1559e96b1bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "2b5cd157-8484-461a-be72-1c9a9dfe6623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa27e133-7b3c-4854-b8c1-1559e96b1bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bcf8ed-cf56-4dac-af43-4c9744fe9597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa27e133-7b3c-4854-b8c1-1559e96b1bb5",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "abf8330f-c1ea-4f52-af62-0b2d09d7e85d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "61cb6847-8a54-46eb-ad91-e18a66254db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf8330f-c1ea-4f52-af62-0b2d09d7e85d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f37f5cf2-fa14-42a0-b554-3e99b5b95fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf8330f-c1ea-4f52-af62-0b2d09d7e85d",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "cb733bee-188a-4a2c-9858-cbd4880d89c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "46df9d24-394f-4356-920f-c2172b48d5c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb733bee-188a-4a2c-9858-cbd4880d89c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1236f53f-fd8f-4760-b2c0-fbd87d2071ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb733bee-188a-4a2c-9858-cbd4880d89c5",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        },
        {
            "id": "6bbecb0d-474d-4573-bf63-4469f873a7a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "compositeImage": {
                "id": "c5926b8a-b95b-407d-8a63-361278c9a859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bbecb0d-474d-4573-bf63-4469f873a7a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9c14fe-d41b-498f-acc4-8a2e75937a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bbecb0d-474d-4573-bf63-4469f873a7a0",
                    "LayerId": "4a191d7e-3ab2-4313-9a43-581f366726ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "4a191d7e-3ab2-4313-9a43-581f366726ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1198247-959f-431b-9970-810e0b1c9f9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}