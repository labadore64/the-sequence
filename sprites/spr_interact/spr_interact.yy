{
    "id": "2c265d5f-787e-42eb-a640-da8f9fd6855f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbd2fde4-75e6-4051-bc3b-30900a0e906f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c265d5f-787e-42eb-a640-da8f9fd6855f",
            "compositeImage": {
                "id": "8860ec29-287d-44e0-86de-0e945eb07c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd2fde4-75e6-4051-bc3b-30900a0e906f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d317d0-9a73-4db9-9bcc-038633b28eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd2fde4-75e6-4051-bc3b-30900a0e906f",
                    "LayerId": "795ccc19-3062-4244-920e-1654cf0d6509"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "795ccc19-3062-4244-920e-1654cf0d6509",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c265d5f-787e-42eb-a640-da8f9fd6855f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}