{
    "id": "228a735b-689c-4d3c-84f6-432d99241d31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_current_position",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 3,
    "bbox_right": 122,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97e3929f-5306-411a-9b73-564065c57d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "228a735b-689c-4d3c-84f6-432d99241d31",
            "compositeImage": {
                "id": "072c22fa-248c-4940-9f63-3e71594ee4eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e3929f-5306-411a-9b73-564065c57d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10dc46c3-3b72-4852-b9fc-776f3fae4fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e3929f-5306-411a-9b73-564065c57d1b",
                    "LayerId": "17bce0d1-1aca-4c90-b6da-fd3e69fa403c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "17bce0d1-1aca-4c90-b6da-fd3e69fa403c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "228a735b-689c-4d3c-84f6-432d99241d31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}