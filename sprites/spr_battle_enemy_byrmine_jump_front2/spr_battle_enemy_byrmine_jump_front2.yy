{
    "id": "ea1c01d4-0211-41b1-9942-03e7c6f34381",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_jump_front2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 35,
    "bbox_right": 223,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5ffa8bf-4f02-4570-9f16-f10aa1549166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1c01d4-0211-41b1-9942-03e7c6f34381",
            "compositeImage": {
                "id": "9f1d1f88-d5ae-4cf1-9400-feb9d3a2e794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ffa8bf-4f02-4570-9f16-f10aa1549166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f057ba1-9483-4728-a80b-90dece5c3471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ffa8bf-4f02-4570-9f16-f10aa1549166",
                    "LayerId": "fc2dca11-76f9-4352-a507-61c11e46b61f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fc2dca11-76f9-4352-a507-61c11e46b61f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea1c01d4-0211-41b1-9942-03e7c6f34381",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}