{
    "id": "60926286-3742-414f-91b5-bfb527830f6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 36,
    "bbox_right": 198,
    "bbox_top": 70,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f167428-fe95-48fd-93e4-0e670cad7a31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60926286-3742-414f-91b5-bfb527830f6e",
            "compositeImage": {
                "id": "d5280eab-dbdc-48e8-91e0-03cd4d6cb2c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f167428-fe95-48fd-93e4-0e670cad7a31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40bf062c-95d2-49fd-b99a-cfed21ba5415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f167428-fe95-48fd-93e4-0e670cad7a31",
                    "LayerId": "a2da6553-2c95-4544-a1d4-03e0dd6dea82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a2da6553-2c95-4544-a1d4-03e0dd6dea82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60926286-3742-414f-91b5-bfb527830f6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}