{
    "id": "7cad9188-d18f-46fc-9ba3-19d0463ada5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 36,
    "bbox_right": 218,
    "bbox_top": 94,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3355e53e-da53-448e-a179-d43ae76d75b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cad9188-d18f-46fc-9ba3-19d0463ada5a",
            "compositeImage": {
                "id": "afd5d35b-9e7e-48a9-a45f-9673a8c578ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3355e53e-da53-448e-a179-d43ae76d75b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21aad6f4-2d10-4c74-9d80-0ee2ecfc058f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3355e53e-da53-448e-a179-d43ae76d75b4",
                    "LayerId": "5323801b-a9dc-4d57-b4f6-abb5fbb4796e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5323801b-a9dc-4d57-b4f6-abb5fbb4796e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cad9188-d18f-46fc-9ba3-19d0463ada5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}