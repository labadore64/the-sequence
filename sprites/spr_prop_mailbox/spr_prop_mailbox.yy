{
    "id": "aef92fe0-7960-4bad-882c-fa88094314db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_mailbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 33,
    "bbox_right": 92,
    "bbox_top": 61,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "425a7692-a20b-49cf-87ec-7593bebeadd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aef92fe0-7960-4bad-882c-fa88094314db",
            "compositeImage": {
                "id": "e4f5613f-235d-4c5f-98d5-3eb685c9eed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "425a7692-a20b-49cf-87ec-7593bebeadd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c111702c-76db-4e05-84b8-4a454c2777ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "425a7692-a20b-49cf-87ec-7593bebeadd9",
                    "LayerId": "835d39d3-b3e0-49e3-b16b-5fa31e3cafbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "835d39d3-b3e0-49e3-b16b-5fa31e3cafbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aef92fe0-7960-4bad-882c-fa88094314db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}