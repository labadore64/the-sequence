{
    "id": "4c76805e-ad52-4d7a-b0f1-8f16fabaf1c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_housePlant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 23,
    "bbox_right": 101,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44a35fa6-f417-4c50-a713-84ccb4981f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c76805e-ad52-4d7a-b0f1-8f16fabaf1c3",
            "compositeImage": {
                "id": "3a42365e-b3a1-465c-b4f7-29397730e8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a35fa6-f417-4c50-a713-84ccb4981f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "759481cc-8546-4326-9bfc-d3cd31bc4d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a35fa6-f417-4c50-a713-84ccb4981f72",
                    "LayerId": "d72ef2c5-a40f-48d4-9a02-48e87e3a7fb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d72ef2c5-a40f-48d4-9a02-48e87e3a7fb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c76805e-ad52-4d7a-b0f1-8f16fabaf1c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}