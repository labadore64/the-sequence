{
    "id": "2a0971b4-a901-4ad9-9153-e9e2c23641b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_vecdor_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 5,
    "bbox_right": 248,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abafef5b-b6d3-42eb-8a1e-b3f0e41fdf3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a0971b4-a901-4ad9-9153-e9e2c23641b0",
            "compositeImage": {
                "id": "6c7da3ac-0fcf-4e22-a34a-31ae1ddf9fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abafef5b-b6d3-42eb-8a1e-b3f0e41fdf3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ad27ba-538e-423a-a424-9138b9492ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abafef5b-b6d3-42eb-8a1e-b3f0e41fdf3a",
                    "LayerId": "9ab38347-a22d-46fc-83ec-303111c9bf13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9ab38347-a22d-46fc-83ec-303111c9bf13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a0971b4-a901-4ad9-9153-e9e2c23641b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "2e11f373-00b4-4204-976b-3ba150910fec",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}