{
    "id": "11b8da3a-a0de-4669-80d0-e4b4e9abf312",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 240,
    "bbox_left": 53,
    "bbox_right": 215,
    "bbox_top": 91,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24764589-40d4-4d40-aca8-c19ad22157e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b8da3a-a0de-4669-80d0-e4b4e9abf312",
            "compositeImage": {
                "id": "86af01e9-1856-44aa-b993-274bc393d2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24764589-40d4-4d40-aca8-c19ad22157e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aee4ed5-12d2-42b6-8fba-d8b1146c8a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24764589-40d4-4d40-aca8-c19ad22157e1",
                    "LayerId": "9eb63f62-663d-4144-8c4f-6c5ef8e23f45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9eb63f62-663d-4144-8c4f-6c5ef8e23f45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11b8da3a-a0de-4669-80d0-e4b4e9abf312",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}