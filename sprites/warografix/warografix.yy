{
    "id": "6179a617-253d-4158-9176-856156f2c744",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "warografix",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86e69e35-73e6-4239-92df-8909d0b83719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6179a617-253d-4158-9176-856156f2c744",
            "compositeImage": {
                "id": "563a32da-8828-4f07-9087-d6ca832ba048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e69e35-73e6-4239-92df-8909d0b83719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f6db32e-0072-4242-a1cb-564f3fa3b158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e69e35-73e6-4239-92df-8909d0b83719",
                    "LayerId": "d0e0093e-6193-4acc-add6-77f4ac579f7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d0e0093e-6193-4acc-add6-77f4ac579f7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6179a617-253d-4158-9176-856156f2c744",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 30,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}