{
    "id": "7d5f6a9d-c963-44d9-8bd4-8d177a9652e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_fuzzyglove",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 37,
    "bbox_right": 225,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a6b0a8d-ef55-43d1-977d-aa911a87d376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d5f6a9d-c963-44d9-8bd4-8d177a9652e1",
            "compositeImage": {
                "id": "97922441-518a-4dad-9818-09a413833ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6b0a8d-ef55-43d1-977d-aa911a87d376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe13f660-2944-4836-bf72-b8cfd0ab3645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6b0a8d-ef55-43d1-977d-aa911a87d376",
                    "LayerId": "c60dc000-7105-4a39-a9ff-9606c4a0f443"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c60dc000-7105-4a39-a9ff-9606c4a0f443",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d5f6a9d-c963-44d9-8bd4-8d177a9652e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}