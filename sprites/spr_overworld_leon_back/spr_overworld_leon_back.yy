{
    "id": "88f85428-5bf8-4565-a160-06101b8dfd2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld_leon_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 44,
    "bbox_right": 204,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f047f7ea-0272-4969-b5b3-643908e86d01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88f85428-5bf8-4565-a160-06101b8dfd2c",
            "compositeImage": {
                "id": "73f3e03e-ffe3-4c5e-befc-afe1ad6321e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f047f7ea-0272-4969-b5b3-643908e86d01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e481e0-176a-4a8f-87d5-de06f0a277dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f047f7ea-0272-4969-b5b3-643908e86d01",
                    "LayerId": "5fde7e44-6810-4831-b1d1-b9b7d6345f66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5fde7e44-6810-4831-b1d1-b9b7d6345f66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88f85428-5bf8-4565-a160-06101b8dfd2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 145
}