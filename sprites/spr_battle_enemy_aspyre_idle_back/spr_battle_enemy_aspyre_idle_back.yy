{
    "id": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aspyre_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 6,
    "bbox_right": 249,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc77dd32-c840-4e32-9b13-0f5b869a39f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "77b161b7-8ca5-45f9-b313-555dbf60398a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc77dd32-c840-4e32-9b13-0f5b869a39f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8709116c-4023-440b-b5cc-2d2d52a7acc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc77dd32-c840-4e32-9b13-0f5b869a39f7",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "a7e9d945-9c99-4942-b8ca-d58fd33cf995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "299a4d77-7fb1-4bd5-bf93-3ac2578fcf3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e9d945-9c99-4942-b8ca-d58fd33cf995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b42dd73-cf68-46c0-91d7-34471c7fe45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e9d945-9c99-4942-b8ca-d58fd33cf995",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "3a5bbdab-2930-4637-a6d5-706c96c1d628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "b42eee36-a876-41c6-a133-97c3fbc7ebf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a5bbdab-2930-4637-a6d5-706c96c1d628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c0ae1b6-fd61-49f4-9012-fcaf9348dc9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a5bbdab-2930-4637-a6d5-706c96c1d628",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "419c7085-a325-4e01-b270-9a7291c5341d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "e0cfd3d5-828d-4632-ae02-eeba5252d341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419c7085-a325-4e01-b270-9a7291c5341d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed229b8-a666-4f95-8cd6-40db306aa7a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419c7085-a325-4e01-b270-9a7291c5341d",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "e93f2116-116c-4f88-8da3-18046abb95d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "5d0c22e1-f739-4c89-98b2-5cb84567db19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e93f2116-116c-4f88-8da3-18046abb95d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8971f511-10b4-4c44-b2ba-7dd69e5a2db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e93f2116-116c-4f88-8da3-18046abb95d9",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "e84d9136-6700-4d98-8471-92ab8132e07e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "425eb8d2-b265-475a-a8af-bb1e0e686e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84d9136-6700-4d98-8471-92ab8132e07e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a0a6d3-d724-45da-a150-ef58e7636239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84d9136-6700-4d98-8471-92ab8132e07e",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "4ba857eb-fdea-41c4-920f-6c8b8604ef9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "8a72b1db-6bed-4672-8712-4aef335b37a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba857eb-fdea-41c4-920f-6c8b8604ef9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab486733-8c6c-4d6f-b1ca-accc8770c82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba857eb-fdea-41c4-920f-6c8b8604ef9f",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "18102ca4-7400-4e6d-b9b1-420f83f9808e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "244aa200-2ce1-4765-a80a-cc92ad53bde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18102ca4-7400-4e6d-b9b1-420f83f9808e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "230a57e9-56b5-490c-9456-3b87ee0252fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18102ca4-7400-4e6d-b9b1-420f83f9808e",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "1dfd4c7c-05b4-47c9-a313-e6f11f6b55a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "c0917b07-1ee3-424d-a189-5a3f8ee2745a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfd4c7c-05b4-47c9-a313-e6f11f6b55a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb701c3-fa8b-4fa5-a4f1-bf855f2b2356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfd4c7c-05b4-47c9-a313-e6f11f6b55a9",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "a0df8f46-d610-4c8c-bb8b-fe28279ec42c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "5e23530d-8e70-4d67-9a9e-ac340f24fdae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0df8f46-d610-4c8c-bb8b-fe28279ec42c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e132001-4a43-4cf0-9daa-2ece8bfeb44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0df8f46-d610-4c8c-bb8b-fe28279ec42c",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "25789226-baaf-4595-9e34-fbc5ac84583e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "4f57d268-f128-423d-9a87-87573b970d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25789226-baaf-4595-9e34-fbc5ac84583e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90226378-6558-4121-b327-265a1301073a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25789226-baaf-4595-9e34-fbc5ac84583e",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "6fc04ab9-4d6a-44cc-8c64-4712a4ec7d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "5d9d12e3-4161-4290-b480-4445fcb7d00f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc04ab9-4d6a-44cc-8c64-4712a4ec7d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac43bf1-7217-487c-80db-630598b24b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc04ab9-4d6a-44cc-8c64-4712a4ec7d95",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "ba89fefb-d470-4661-b97b-349c39c74302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "6cc044ef-52c6-40b7-a0f1-8f15a5828d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba89fefb-d470-4661-b97b-349c39c74302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece80643-2583-4f05-b631-8c478979a18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba89fefb-d470-4661-b97b-349c39c74302",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "73c63000-669a-41f4-8e23-ed7a62bb7cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "8f9e9cb9-2e68-47ab-85aa-709cd783b751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c63000-669a-41f4-8e23-ed7a62bb7cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c6d02e9-fa49-4bfc-b737-13a0955b2103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c63000-669a-41f4-8e23-ed7a62bb7cc1",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "810be84d-6a4a-46cf-b735-0334216dc2cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "d8425da9-b5dc-42ed-b997-b4b448221b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "810be84d-6a4a-46cf-b735-0334216dc2cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d618fb0f-6660-4189-9691-4876e2fd5c55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "810be84d-6a4a-46cf-b735-0334216dc2cd",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "fb6b109c-4461-43ef-a2b3-8cbdad1572c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "4c678a18-6bcb-4245-8d19-db814650a53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6b109c-4461-43ef-a2b3-8cbdad1572c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f409710-4417-4c9e-9264-56cffdbd084a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6b109c-4461-43ef-a2b3-8cbdad1572c3",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "b9ee0b0e-9d9b-49e3-bee7-c17416c8c367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "56fb476e-5431-4296-a595-b096ce5926c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ee0b0e-9d9b-49e3-bee7-c17416c8c367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e8bd85a-a269-4349-8242-78d04816b40b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ee0b0e-9d9b-49e3-bee7-c17416c8c367",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "c6eded1a-fcdc-46a4-a39b-e4f5c68eb7da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "cd499472-c4e1-4a3c-92fa-0a71429cdc15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6eded1a-fcdc-46a4-a39b-e4f5c68eb7da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a70d13-8bcb-478f-a5b3-45cca1673d26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6eded1a-fcdc-46a4-a39b-e4f5c68eb7da",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "a643e730-f990-4c56-8507-2c0ad67b2009",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "22c1f00a-6e10-431d-b05a-e0b0a733739b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a643e730-f990-4c56-8507-2c0ad67b2009",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c3116a6-d4ee-4724-bfd4-f7ebc97c9f37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a643e730-f990-4c56-8507-2c0ad67b2009",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "808b31f8-26e9-4ca2-b55f-f8a65ae68452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "c49f3bf3-5c4f-4c3b-8ff7-4467ef6b9015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "808b31f8-26e9-4ca2-b55f-f8a65ae68452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e3ac15-029c-401a-a704-d3c5e7a3d1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "808b31f8-26e9-4ca2-b55f-f8a65ae68452",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "aed81f81-9361-406a-abf9-9bafaaf520e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "c4e1e010-d378-4bdf-b531-5f854c0f9016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed81f81-9361-406a-abf9-9bafaaf520e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac215d46-b59b-45c1-ba51-047375081792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed81f81-9361-406a-abf9-9bafaaf520e5",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "cea2bc13-3f86-4ac0-b262-280ba5744d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "80695c60-d960-412d-a59e-2d8094795015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea2bc13-3f86-4ac0-b262-280ba5744d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "973380e3-252e-4bf6-84c9-6d16a182f8d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea2bc13-3f86-4ac0-b262-280ba5744d73",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "43e23d3f-2fe1-492c-916f-bda2f045eb9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "2b000a6e-4f04-4c17-9005-8d1a2eaeed5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e23d3f-2fe1-492c-916f-bda2f045eb9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3eb91b-6869-4e37-9fcc-2ec1a5811190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e23d3f-2fe1-492c-916f-bda2f045eb9c",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "88acbeda-1e08-40a0-b2fc-281b7b97394d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "bac8a3b7-7be6-41cf-adc6-7baadc768da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88acbeda-1e08-40a0-b2fc-281b7b97394d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f75148fa-1a81-416a-9352-349b9e7d195c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88acbeda-1e08-40a0-b2fc-281b7b97394d",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "e4be93bc-36cb-4ffe-9137-b14bc0a5f9b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "09312cc3-0e18-4ad3-8223-35fee185bdb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4be93bc-36cb-4ffe-9137-b14bc0a5f9b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f977ff0-4eed-49c5-8b1b-aff361db4efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4be93bc-36cb-4ffe-9137-b14bc0a5f9b1",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "f1658672-ba56-4e93-8ecb-e68a0eca6789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "05114b84-bd92-46b9-aa8a-06838a8b8bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1658672-ba56-4e93-8ecb-e68a0eca6789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8395193b-151b-43c1-a8db-187a19c33265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1658672-ba56-4e93-8ecb-e68a0eca6789",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "43f70e18-2449-4574-96b7-ee71a6a90101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "bf089182-bb57-42ca-bf42-fa827c41d0d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f70e18-2449-4574-96b7-ee71a6a90101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c0c4140-7afc-4a7f-b0b3-8bdeedf070cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f70e18-2449-4574-96b7-ee71a6a90101",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "bafb5de0-d5c7-4180-9f0e-973b97e95557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "23fafafa-6c14-486b-914a-5b3327de595d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bafb5de0-d5c7-4180-9f0e-973b97e95557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186e3980-4878-4e4e-8f45-9e369a6eff2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bafb5de0-d5c7-4180-9f0e-973b97e95557",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "a2d691d7-4616-423e-8b08-19075c629a0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "5e65167b-3465-436e-b3bf-b2229fa393b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2d691d7-4616-423e-8b08-19075c629a0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0350d9c-2850-4791-9646-1cd0ef2d655b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2d691d7-4616-423e-8b08-19075c629a0e",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "ab2215e9-bd1c-44a8-9b0a-3ea0ead48634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "1be1372f-0936-476b-b906-4a2b0b3f7907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab2215e9-bd1c-44a8-9b0a-3ea0ead48634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "439824e8-04c1-4b52-8486-09b6287e1d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab2215e9-bd1c-44a8-9b0a-3ea0ead48634",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "e6bcbef1-dbac-4cc7-8954-21cc73c7c5d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "bb847127-1c15-472b-8775-56d573db5f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bcbef1-dbac-4cc7-8954-21cc73c7c5d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "272d2629-bf26-45b7-9885-0d48eb76989f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bcbef1-dbac-4cc7-8954-21cc73c7c5d6",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "f6e530e0-c95d-4d9f-92d0-ce0bb3284887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "d1e3de50-adc2-40fa-8d85-397d48286940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e530e0-c95d-4d9f-92d0-ce0bb3284887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d74b29-dbc7-4480-b599-7d1f75edae74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e530e0-c95d-4d9f-92d0-ce0bb3284887",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "72ff41bf-bd26-4716-aae4-d4755c1517db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "70e7ad36-5713-4a30-89e1-da38ea8329af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ff41bf-bd26-4716-aae4-d4755c1517db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0c5773-00c4-40d2-bbba-ac9e98e05a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ff41bf-bd26-4716-aae4-d4755c1517db",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "7023817b-41b5-419c-85a7-873b60b0646b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "c932f725-59fe-49a2-ad19-5e7a9613c969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7023817b-41b5-419c-85a7-873b60b0646b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af2bfb8-e038-4e3b-aa6c-efb41d7d6a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7023817b-41b5-419c-85a7-873b60b0646b",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "182dad00-b0d6-4a99-be83-a12e7231ccd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "c5fb7e30-2628-4c96-aa44-0c0459fce645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "182dad00-b0d6-4a99-be83-a12e7231ccd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8d2a284-e2e0-485f-b064-5675bbb52a7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "182dad00-b0d6-4a99-be83-a12e7231ccd2",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        },
        {
            "id": "6118c2a3-fa17-4518-9fd6-2ec6a687ebf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "compositeImage": {
                "id": "d4382e38-67ae-4caf-a1a3-0d2a1faf9aa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6118c2a3-fa17-4518-9fd6-2ec6a687ebf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac56092a-f3aa-4d1a-bd23-7ba79ef44efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6118c2a3-fa17-4518-9fd6-2ec6a687ebf6",
                    "LayerId": "cb3d0a66-4aa7-4038-8655-eb7353b96e56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "cb3d0a66-4aa7-4038-8655-eb7353b96e56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c2e1d5b-b1de-4a23-bfbd-6e675daf4434",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "808f5ba0-e9e9-4dd6-a3e1-5fd671722977",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}