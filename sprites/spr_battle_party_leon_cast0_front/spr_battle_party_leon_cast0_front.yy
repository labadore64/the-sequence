{
    "id": "8e7f1d2e-e617-4bb1-9999-d107c13b4efc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 40,
    "bbox_right": 195,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24d42f93-9c17-47a8-9988-e4e97cade817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e7f1d2e-e617-4bb1-9999-d107c13b4efc",
            "compositeImage": {
                "id": "94e5f860-d73e-4fd5-9404-f578dcd123be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24d42f93-9c17-47a8-9988-e4e97cade817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "012f5d5c-d6e1-41a6-a526-a19daa4e5730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24d42f93-9c17-47a8-9988-e4e97cade817",
                    "LayerId": "a6b4116d-118a-4716-93cc-5da8fd3fcf0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a6b4116d-118a-4716-93cc-5da8fd3fcf0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e7f1d2e-e617-4bb1-9999-d107c13b4efc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}