{
    "id": "68cc3a71-5f10-4df3-b90a-75048f7ff35c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_debug_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 34,
    "bbox_right": 89,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5199a9e-0452-480e-9243-ce27aaf9a1dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68cc3a71-5f10-4df3-b90a-75048f7ff35c",
            "compositeImage": {
                "id": "8c592d1c-6e8f-4fc4-8f5b-30dd54092348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5199a9e-0452-480e-9243-ce27aaf9a1dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc776639-f3de-4d50-a7d1-9a52a594a30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5199a9e-0452-480e-9243-ce27aaf9a1dc",
                    "LayerId": "d9abcf9c-bde6-4ab6-841a-c56b28dac319"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d9abcf9c-bde6-4ab6-841a-c56b28dac319",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68cc3a71-5f10-4df3-b90a-75048f7ff35c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}