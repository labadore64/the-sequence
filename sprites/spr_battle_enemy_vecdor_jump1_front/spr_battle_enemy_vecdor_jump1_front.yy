{
    "id": "7519b038-6314-4e44-9838-58f0d81d0650",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_vecdor_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 248,
    "bbox_top": 99,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1593b0a-ba3c-4216-afc9-341c29421c9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7519b038-6314-4e44-9838-58f0d81d0650",
            "compositeImage": {
                "id": "adf8e437-f912-4364-9990-ef7d43859d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1593b0a-ba3c-4216-afc9-341c29421c9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e8d06b5-b0b5-48b9-8baf-21c9c95b4028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1593b0a-ba3c-4216-afc9-341c29421c9c",
                    "LayerId": "b75a3ad5-6c27-4168-b228-cbc4a213c2e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b75a3ad5-6c27-4168-b228-cbc4a213c2e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7519b038-6314-4e44-9838-58f0d81d0650",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "2e11f373-00b4-4204-976b-3ba150910fec",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}