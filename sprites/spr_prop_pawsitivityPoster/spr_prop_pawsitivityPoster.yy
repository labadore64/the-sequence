{
    "id": "cd82fead-7f2b-4e18-bcec-4c0e065080d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_pawsitivityPoster",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 4,
    "bbox_right": 122,
    "bbox_top": 28,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6388e3d-0c66-462a-b2e1-830195285901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd82fead-7f2b-4e18-bcec-4c0e065080d7",
            "compositeImage": {
                "id": "65409c24-96c7-45aa-a8b9-b65de8970821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6388e3d-0c66-462a-b2e1-830195285901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce340dc-8679-4eda-bdb3-dc8c2d42ffcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6388e3d-0c66-462a-b2e1-830195285901",
                    "LayerId": "fb5d70fc-8507-493c-a83c-04a2a51f55e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "fb5d70fc-8507-493c-a83c-04a2a51f55e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd82fead-7f2b-4e18-bcec-4c0e065080d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 1.21325016,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}