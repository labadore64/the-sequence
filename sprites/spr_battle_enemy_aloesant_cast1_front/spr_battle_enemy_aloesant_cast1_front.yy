{
    "id": "56d0e86b-6b2f-4a6f-9484-477dc350e077",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 4,
    "bbox_right": 215,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "474b1ccb-7f90-4ffb-95b4-cb578706a528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56d0e86b-6b2f-4a6f-9484-477dc350e077",
            "compositeImage": {
                "id": "22473371-c66d-4399-a904-a99a88c18c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "474b1ccb-7f90-4ffb-95b4-cb578706a528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38214c3-d7f6-44dc-b7bd-29f399acdbd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "474b1ccb-7f90-4ffb-95b4-cb578706a528",
                    "LayerId": "a8442401-3835-4a86-85f5-ce29936463f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a8442401-3835-4a86-85f5-ce29936463f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56d0e86b-6b2f-4a6f-9484-477dc350e077",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}