{
    "id": "583e7052-f143-4556-a85c-503d85708477",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_pierre",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 89,
    "bbox_right": 451,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f860a72-2b63-45c6-8c31-f32cc56a937c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "583e7052-f143-4556-a85c-503d85708477",
            "compositeImage": {
                "id": "e4b00279-5cee-42fc-9d6b-a2843a7242a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f860a72-2b63-45c6-8c31-f32cc56a937c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dabb5ece-6596-4d09-a2e8-f6b41cc1696c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f860a72-2b63-45c6-8c31-f32cc56a937c",
                    "LayerId": "60f07fc0-ae1d-4723-b524-dd34efbeaa5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "60f07fc0-ae1d-4723-b524-dd34efbeaa5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "583e7052-f143-4556-a85c-503d85708477",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}