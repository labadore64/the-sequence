{
    "id": "019164fd-1dc8-4365-ba6d-fdb6ef71b09a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_mikeDog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 20,
    "bbox_right": 95,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a69f4e6-ce33-4f29-844c-086f0ef4dd53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019164fd-1dc8-4365-ba6d-fdb6ef71b09a",
            "compositeImage": {
                "id": "7cb2973d-f1cc-4c46-aa3d-41f1d99cd221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a69f4e6-ce33-4f29-844c-086f0ef4dd53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e7afcf-07ef-4d86-9082-0aff8d767a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a69f4e6-ce33-4f29-844c-086f0ef4dd53",
                    "LayerId": "865bd49b-5334-41bb-b295-82d7c32a449a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "865bd49b-5334-41bb-b295-82d7c32a449a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "019164fd-1dc8-4365-ba6d-fdb6ef71b09a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}