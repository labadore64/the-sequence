{
    "id": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 56,
    "bbox_right": 184,
    "bbox_top": 111,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fde242b-140c-403b-8acc-929ce4a41dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "a73fbf09-2b2f-4e95-9609-36cbdcd15e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fde242b-140c-403b-8acc-929ce4a41dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3355c34-e3cd-4662-8ca2-da387638aeb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fde242b-140c-403b-8acc-929ce4a41dc0",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "551cc1f0-93b6-4006-b372-ac58fffafa6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "67785052-fe7b-4eed-9c9f-d03cd31f515b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "551cc1f0-93b6-4006-b372-ac58fffafa6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ade4e081-cf47-40db-912c-244d7f891208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "551cc1f0-93b6-4006-b372-ac58fffafa6b",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "68fa2678-a4f6-4c09-9144-236af68936c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "34878478-2df0-45d3-a5e0-1f6a2d8a536f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68fa2678-a4f6-4c09-9144-236af68936c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a1ea293-fb43-48ba-8638-a714fd21a499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68fa2678-a4f6-4c09-9144-236af68936c0",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "9ced58d6-8e4f-41e4-b2c2-a6a3161bb8f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "1a78fbc4-13fd-4dc0-9bb7-c934b96678b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ced58d6-8e4f-41e4-b2c2-a6a3161bb8f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e306b2e-636b-4fb7-a46d-f7d3ffdd2959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ced58d6-8e4f-41e4-b2c2-a6a3161bb8f6",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "2d772e73-929a-4a56-9a3f-f21667acc300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "2a75e54c-f273-4825-b880-47f8451ec5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d772e73-929a-4a56-9a3f-f21667acc300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb781f11-617c-4dc3-9bb5-a19f0f237689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d772e73-929a-4a56-9a3f-f21667acc300",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "d3a3dfb5-8571-417d-8b5f-3686c5913f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "59b5c3c7-20cd-4a71-9f38-2646bc6aa712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a3dfb5-8571-417d-8b5f-3686c5913f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e29154-6fd6-472c-b665-e78f0241cc02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a3dfb5-8571-417d-8b5f-3686c5913f74",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "e90203ee-ebe4-46fe-b5a1-e7dd519aafea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "960548a3-4ce9-40c3-9865-1f31495d4f34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90203ee-ebe4-46fe-b5a1-e7dd519aafea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53117958-3cb2-4ef3-ae12-c55f65cce6f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90203ee-ebe4-46fe-b5a1-e7dd519aafea",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "f14bb3ed-ce9e-463e-afb7-e78334b85351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "a44001fb-e183-41b5-a1ca-277ddd6d8ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f14bb3ed-ce9e-463e-afb7-e78334b85351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4974a1fa-0780-4a7a-ac2b-b4d05ef7694a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f14bb3ed-ce9e-463e-afb7-e78334b85351",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "7a8d1cfe-7fd0-4304-89e1-e8d8fc123d20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "34429866-7fc6-4143-bd98-82748cb3e040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a8d1cfe-7fd0-4304-89e1-e8d8fc123d20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a473a183-6b15-41c0-ba86-1b58f82d129d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a8d1cfe-7fd0-4304-89e1-e8d8fc123d20",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "2d6dbf8d-191d-45a7-85d0-239f1028ab3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "ecc75c87-daa7-4e16-bcab-e498c105dfaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6dbf8d-191d-45a7-85d0-239f1028ab3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7feaa1-7586-4841-9abb-358a375d98cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6dbf8d-191d-45a7-85d0-239f1028ab3c",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "c66c080a-5454-4e1e-9b25-c4144e90f798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "ab7eb676-2786-4a91-b27f-40caf661e598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66c080a-5454-4e1e-9b25-c4144e90f798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31acb8bb-faeb-4755-86ae-1c004df20c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66c080a-5454-4e1e-9b25-c4144e90f798",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "18832f03-b80d-48a5-9ca4-5024ceb8438f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "d7feee62-6e3e-48cf-a03a-b21f28b73213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18832f03-b80d-48a5-9ca4-5024ceb8438f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c37f27-2bc7-4754-9e63-ffa281223702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18832f03-b80d-48a5-9ca4-5024ceb8438f",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        },
        {
            "id": "c7c7ac20-2076-49e4-8b4a-db95bb8e9632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "compositeImage": {
                "id": "68e341d9-5a52-4737-bd5f-aa04d045710b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7c7ac20-2076-49e4-8b4a-db95bb8e9632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f8542b-7f2a-420e-b309-17b1741dc038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7c7ac20-2076-49e4-8b4a-db95bb8e9632",
                    "LayerId": "9fc72655-487f-42a4-84b6-9d76e352f3dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9fc72655-487f-42a4-84b6-9d76e352f3dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f806d2ac-7d25-4802-8795-a0b772dbf4c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}