{
    "id": "a8552313-59e1-4103-bc38-be9e0005c3b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 226,
    "bbox_left": 35,
    "bbox_right": 193,
    "bbox_top": 61,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e190e24-7a4a-4f75-961a-bf1aeedca7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8552313-59e1-4103-bc38-be9e0005c3b1",
            "compositeImage": {
                "id": "a26c4e5d-4936-438f-94d8-3b09ef87281a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e190e24-7a4a-4f75-961a-bf1aeedca7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3af9916-622e-40da-afb5-cdd6959b57cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e190e24-7a4a-4f75-961a-bf1aeedca7e0",
                    "LayerId": "fa77f591-6662-46b2-b925-f4f317e41c71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fa77f591-6662-46b2-b925-f4f317e41c71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8552313-59e1-4103-bc38-be9e0005c3b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}