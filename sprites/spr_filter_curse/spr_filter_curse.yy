{
    "id": "b3db6fe5-24f9-4b37-aaf0-50a3a49074e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_curse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 233,
    "bbox_left": 35,
    "bbox_right": 199,
    "bbox_top": 67,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87bb6547-b156-44f3-b996-6fb34c9e9132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3db6fe5-24f9-4b37-aaf0-50a3a49074e3",
            "compositeImage": {
                "id": "f8575f90-1d18-4314-90e2-f5b835d0d594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bb6547-b156-44f3-b996-6fb34c9e9132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3fec4e-c75f-43b6-bc92-c688a650b853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bb6547-b156-44f3-b996-6fb34c9e9132",
                    "LayerId": "e2082e3e-45e1-4405-8878-327a78cf8783"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e2082e3e-45e1-4405-8878-327a78cf8783",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3db6fe5-24f9-4b37-aaf0-50a3a49074e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 150
}