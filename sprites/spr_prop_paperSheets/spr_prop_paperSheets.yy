{
    "id": "67376508-1501-45bb-bb3c-f568b9664e79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_paperSheets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 30,
    "bbox_right": 86,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ee3ffce-f0cb-4d21-ab5f-8fae0976e6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67376508-1501-45bb-bb3c-f568b9664e79",
            "compositeImage": {
                "id": "1f9d04bc-0012-405e-8f4e-48bc9ad597c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee3ffce-f0cb-4d21-ab5f-8fae0976e6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "081b794d-747e-41cb-9f1d-6cad287268f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee3ffce-f0cb-4d21-ab5f-8fae0976e6e4",
                    "LayerId": "61f5e384-0f13-4a19-9d17-4ffdefd9b108"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "61f5e384-0f13-4a19-9d17-4ffdefd9b108",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67376508-1501-45bb-bb3c-f568b9664e79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}