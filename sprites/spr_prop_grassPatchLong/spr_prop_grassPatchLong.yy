{
    "id": "c7e4ec4d-f767-466f-a2d1-d5151c13d191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_grassPatchLong",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 1,
    "bbox_right": 123,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5314807d-9f85-4dd0-b591-ddd1d87b39f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e4ec4d-f767-466f-a2d1-d5151c13d191",
            "compositeImage": {
                "id": "50f38c62-b182-4fa2-8f51-649bff901d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5314807d-9f85-4dd0-b591-ddd1d87b39f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b662f13-767d-4d1c-9a84-9cef5699fc60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5314807d-9f85-4dd0-b591-ddd1d87b39f0",
                    "LayerId": "e0a954fa-ce06-4240-80d1-5d6b25deadd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e0a954fa-ce06-4240-80d1-5d6b25deadd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7e4ec4d-f767-466f-a2d1-d5151c13d191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}