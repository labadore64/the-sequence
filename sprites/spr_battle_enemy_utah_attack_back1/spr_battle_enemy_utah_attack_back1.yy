{
    "id": "48deff2e-7126-43d0-a091-5c7fa4a15775",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_attack_back1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 234,
    "bbox_left": 63,
    "bbox_right": 189,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8102abc5-c2b8-401d-90ad-539dfe49a63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48deff2e-7126-43d0-a091-5c7fa4a15775",
            "compositeImage": {
                "id": "0b0ea46e-d6e0-4370-a6fe-e09f09e903bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8102abc5-c2b8-401d-90ad-539dfe49a63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef1186f9-a49c-478c-b2e1-189c17c40682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8102abc5-c2b8-401d-90ad-539dfe49a63d",
                    "LayerId": "f88cd6f4-fdb6-467c-b808-76d5eb3710d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f88cd6f4-fdb6-467c-b808-76d5eb3710d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48deff2e-7126-43d0-a091-5c7fa4a15775",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}