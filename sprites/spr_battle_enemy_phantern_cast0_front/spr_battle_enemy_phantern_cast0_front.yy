{
    "id": "98a2f36e-c271-4616-86cd-4907a4e0b64e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 64,
    "bbox_right": 170,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c133d6f-e5d6-4c01-a74e-06135596de45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a2f36e-c271-4616-86cd-4907a4e0b64e",
            "compositeImage": {
                "id": "ca65685b-eeec-4867-94a0-5d21dd39acd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c133d6f-e5d6-4c01-a74e-06135596de45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aee57a84-3eec-4467-b973-874ac667a956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c133d6f-e5d6-4c01-a74e-06135596de45",
                    "LayerId": "de31ac42-e2d0-4158-a3c2-fa91d4cc21cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "de31ac42-e2d0-4158-a3c2-fa91d4cc21cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98a2f36e-c271-4616-86cd-4907a4e0b64e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}