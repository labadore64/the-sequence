{
    "id": "035b5f57-c463-44a7-a2c4-fbff1e249dce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 16,
    "bbox_right": 214,
    "bbox_top": 114,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51af3e68-0e8f-4bd1-a3bf-5564057f1bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035b5f57-c463-44a7-a2c4-fbff1e249dce",
            "compositeImage": {
                "id": "b722170b-cd36-436e-9cad-46bfcec937a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51af3e68-0e8f-4bd1-a3bf-5564057f1bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89fff19-6df3-412d-856b-4a937ab295c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51af3e68-0e8f-4bd1-a3bf-5564057f1bc2",
                    "LayerId": "8f221584-0bc1-4d6a-98cf-47349c81912b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8f221584-0bc1-4d6a-98cf-47349c81912b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "035b5f57-c463-44a7-a2c4-fbff1e249dce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}