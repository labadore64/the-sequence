{
    "id": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snowbird_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 55,
    "bbox_right": 170,
    "bbox_top": 96,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63da1433-7c80-4114-97f0-89c4079bd2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "1cd1ae24-1213-4f7b-aecf-ead5b50048ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63da1433-7c80-4114-97f0-89c4079bd2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae70d1cb-66ef-41a6-84bc-ecb5347f1ab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63da1433-7c80-4114-97f0-89c4079bd2ba",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "5f3c6cb7-e761-494b-9a56-7ed90dc1531e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "4649ce37-b413-492c-a1a2-f7a1d536262d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3c6cb7-e761-494b-9a56-7ed90dc1531e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9913826-ff10-47e3-b47a-78cfd8cddc07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3c6cb7-e761-494b-9a56-7ed90dc1531e",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "4eb613f3-db35-49fb-9740-ad269ebe6e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "931890c1-1827-4f11-a89b-68879488be9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb613f3-db35-49fb-9740-ad269ebe6e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e4f2ad-cb86-49d5-981c-b4d33c3a7ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb613f3-db35-49fb-9740-ad269ebe6e52",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "8aced53f-ad3f-4b6e-9581-0b744906a229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "daae6760-5433-4785-9a29-061d6a2924d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aced53f-ad3f-4b6e-9581-0b744906a229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a55cb0-2406-4f65-bdea-8ba3c32509bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aced53f-ad3f-4b6e-9581-0b744906a229",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "abf32858-4fc4-4f90-868d-1f1e2a162718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "9b2b4f98-af29-48eb-bad6-ea6e7df837ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf32858-4fc4-4f90-868d-1f1e2a162718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d2eb7b-10fc-4d69-9778-80da4dad8571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf32858-4fc4-4f90-868d-1f1e2a162718",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "bed491b2-0b68-486d-8b3f-b77f512fba67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "db6481b9-53ed-4b88-ad48-2ea6a9085e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed491b2-0b68-486d-8b3f-b77f512fba67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da55948-5692-495a-aceb-c157d1c0a8f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed491b2-0b68-486d-8b3f-b77f512fba67",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "3fa739a9-1f14-419e-b955-64cc1ccda5cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "2c8db90f-482e-4c81-848d-7ff6fd77789e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa739a9-1f14-419e-b955-64cc1ccda5cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52d6753-5927-4d24-afb2-c87035bf155e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa739a9-1f14-419e-b955-64cc1ccda5cc",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "4bc581f0-8b81-41cc-ab17-58076d751c50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "375cf80d-96d3-4c94-b670-e7aaf22fb022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc581f0-8b81-41cc-ab17-58076d751c50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f602987a-3a06-46d5-b69d-310238f519be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc581f0-8b81-41cc-ab17-58076d751c50",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "ae9e654a-3abc-4f03-8cf4-273f7912872f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "d55d5d3d-7561-4bd8-88fb-d7dc19431866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9e654a-3abc-4f03-8cf4-273f7912872f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "971f5ce3-cb0f-4820-8d59-beb602e929aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9e654a-3abc-4f03-8cf4-273f7912872f",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "ab90d4c0-8308-4dd0-9a9c-d1bec49affef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "36049ac5-7f80-44c0-8ee8-952086ada0ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab90d4c0-8308-4dd0-9a9c-d1bec49affef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68cd2326-7b6e-4cc7-92d3-1867a5f1f981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab90d4c0-8308-4dd0-9a9c-d1bec49affef",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "364f39b8-3d5c-4502-8fd7-d9cad5fa8b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "3a2397da-425e-41e9-a7a6-0386da911495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "364f39b8-3d5c-4502-8fd7-d9cad5fa8b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccfeec93-6ff9-48fb-a707-b6a9975bbd74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "364f39b8-3d5c-4502-8fd7-d9cad5fa8b08",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "28cd16fa-e53e-43bd-becf-bfd0087982e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "f5047986-57e7-45f0-bc65-f75c23a42f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28cd16fa-e53e-43bd-becf-bfd0087982e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff745bf2-cc38-49a2-add2-a80fa6def253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28cd16fa-e53e-43bd-becf-bfd0087982e8",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        },
        {
            "id": "b3c739fa-373d-4819-a797-ca8d0219080f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "compositeImage": {
                "id": "2dbe8632-f377-4dbe-85f2-7a12fbc947c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3c739fa-373d-4819-a797-ca8d0219080f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc5c6f6-dce6-4a05-8691-d85293bdf90d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3c739fa-373d-4819-a797-ca8d0219080f",
                    "LayerId": "c22a6a0f-1625-4cc7-a0b1-b673e258589c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c22a6a0f-1625-4cc7-a0b1-b673e258589c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95c5b778-875c-4c0e-b73e-b59debf7b9aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d41a043f-5316-4f92-8dc3-11dc8826a28b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}