{
    "id": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 21,
    "bbox_right": 193,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dbafc6e-f0ee-4a0a-ab95-d4f99643b05b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "b1552a9d-74b1-466b-a467-e1e17364db80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dbafc6e-f0ee-4a0a-ab95-d4f99643b05b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc110314-68fb-4fbd-a9e7-222795fee139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dbafc6e-f0ee-4a0a-ab95-d4f99643b05b",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "b3d6f7f2-ae16-4d99-9da1-b297be5c6886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "bd180fa4-fea9-413d-b600-ffa4fb5f7074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d6f7f2-ae16-4d99-9da1-b297be5c6886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89f4dd5b-6e14-4449-9f08-e2fd2c563718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d6f7f2-ae16-4d99-9da1-b297be5c6886",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "df426c16-112f-4be5-9726-61d3b4800bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "bf3fc643-c134-483a-bc5a-c793df94f2f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df426c16-112f-4be5-9726-61d3b4800bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116c76e8-9f6a-4833-a67c-6906ead67bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df426c16-112f-4be5-9726-61d3b4800bb7",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "0e6147e2-fdbe-4721-8711-ac5c50e289fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "cc0f9953-e877-4e6b-a86d-5c5b81ae8ec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6147e2-fdbe-4721-8711-ac5c50e289fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c33f04-7ed7-46ab-8e2a-b2edbbc43a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6147e2-fdbe-4721-8711-ac5c50e289fd",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "554d2dbc-aa31-4d1a-abfc-9839a05b278f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "a5ac85b6-7b46-4f6b-8a70-d94e320b1451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554d2dbc-aa31-4d1a-abfc-9839a05b278f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dbc2f14-be3c-4156-b279-89348bab6a9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554d2dbc-aa31-4d1a-abfc-9839a05b278f",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "fc4cf524-87ea-4533-b468-1ae273fbc4ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "f3c541d8-3494-49fd-bc3c-cbfcff5baee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc4cf524-87ea-4533-b468-1ae273fbc4ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7e8fa65-8499-4581-a45f-6fad1a4adfb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc4cf524-87ea-4533-b468-1ae273fbc4ec",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "0b731b3f-5296-4fc9-ba7c-fcbdb6108080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "788cc237-02a1-4f02-b572-55588db0c7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b731b3f-5296-4fc9-ba7c-fcbdb6108080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0754bf1e-cf50-4628-98a8-e0214b2c6f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b731b3f-5296-4fc9-ba7c-fcbdb6108080",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "d9802fbb-d03b-4c2a-a16b-c2b9a178969b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "cd73b5fa-4fbc-4571-96b4-c51841a68fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9802fbb-d03b-4c2a-a16b-c2b9a178969b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7777cde8-ee1a-471e-80d1-83bf8a48d3bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9802fbb-d03b-4c2a-a16b-c2b9a178969b",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "a28e24fb-8819-416d-be04-612dc053dde2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "7e91b2d4-9ca9-481d-9626-5d42b62f5ea5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28e24fb-8819-416d-be04-612dc053dde2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f6fc73d-88a8-452d-8dab-996882b23bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28e24fb-8819-416d-be04-612dc053dde2",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "270b4e5e-731a-4bad-8c58-0a6746d8577b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "c09034ed-2359-489d-b99c-be3255581ad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270b4e5e-731a-4bad-8c58-0a6746d8577b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57966e95-13ed-42b8-9125-18cc8cf98452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270b4e5e-731a-4bad-8c58-0a6746d8577b",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "7c5f1239-f1af-45f5-a418-d5904e362821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "ab7debe4-f256-42e9-a7df-d8563539baf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c5f1239-f1af-45f5-a418-d5904e362821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "828e9a17-c3c6-4e79-a657-1f1e168f86f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c5f1239-f1af-45f5-a418-d5904e362821",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "d1f14e64-9a90-4000-bedb-481155536e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "0595f925-2b2c-495e-9c62-6fcf7869cd0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f14e64-9a90-4000-bedb-481155536e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60288d62-2da5-4152-8b97-80caa49aa82d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f14e64-9a90-4000-bedb-481155536e81",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "f9119117-af5e-49fd-9579-cc22c5523b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "4e46186a-63e5-4b13-b806-0484c610a9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9119117-af5e-49fd-9579-cc22c5523b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d28dff1-e6ac-4555-8c76-05e9efeaddc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9119117-af5e-49fd-9579-cc22c5523b1c",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "c2f26016-cd70-4c64-a132-0120a3f4bbf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "c1a5581b-2333-4c77-b8e7-0342f8d0abcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f26016-cd70-4c64-a132-0120a3f4bbf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6010b963-7e50-4d5c-9ce8-4e55afbcd0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f26016-cd70-4c64-a132-0120a3f4bbf2",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "e34a2bbb-ce96-4588-beb9-13258c406a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "2c819b02-2552-43c6-a875-27599937f63e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e34a2bbb-ce96-4588-beb9-13258c406a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5808e3bd-fd77-4096-82be-1e249642c4d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e34a2bbb-ce96-4588-beb9-13258c406a07",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "d9239b70-79a0-40b1-b5e9-b19089bf0400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "ac4526da-4227-4c8e-9210-0faf33c6807e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9239b70-79a0-40b1-b5e9-b19089bf0400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0895c11c-aefc-40ec-9d13-1890da3db1a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9239b70-79a0-40b1-b5e9-b19089bf0400",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "1035bb4a-1664-4f02-a901-b6a08614c1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "46bb8cd6-4287-4d07-b860-c7297107753f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1035bb4a-1664-4f02-a901-b6a08614c1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86865c98-9e52-4b0d-a117-122b503086a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1035bb4a-1664-4f02-a901-b6a08614c1ea",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "c7d609f3-ba04-4cc0-8f2f-d5e5a46de00a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "0972400c-92aa-4ff7-a73d-fd0708083e65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d609f3-ba04-4cc0-8f2f-d5e5a46de00a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5cdc9b3-cd5f-4b4a-b6b6-5a8c1cacdd01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d609f3-ba04-4cc0-8f2f-d5e5a46de00a",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "cfd0a0ef-91f5-436f-bffc-047fe3daba2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "e3594723-d4a0-4d5b-8be0-6f14e45c77ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfd0a0ef-91f5-436f-bffc-047fe3daba2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b18e2e2-6cbd-4639-8112-91f0ffb588cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfd0a0ef-91f5-436f-bffc-047fe3daba2e",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "669839f8-c9e2-47c0-a25f-00a731568ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "fc3ae9dd-8e9c-4ced-8789-eaf255841343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "669839f8-c9e2-47c0-a25f-00a731568ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d1406e-5e73-435b-94ed-f6eb077f7b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "669839f8-c9e2-47c0-a25f-00a731568ce7",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "a7bd0fc4-5d29-4d13-91f5-31fd81258797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "f970bd43-e99e-41c1-a787-23cd0f0eb658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7bd0fc4-5d29-4d13-91f5-31fd81258797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de1d85e-9b7d-473c-a55e-cbf7f1802eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7bd0fc4-5d29-4d13-91f5-31fd81258797",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "5d1ede0e-71dc-490b-b218-d07cc54f95f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "3841c042-110e-4f80-bf26-0f3ee5de4fcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d1ede0e-71dc-490b-b218-d07cc54f95f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bdc0f75-7449-407d-b92e-ec4e26f75548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d1ede0e-71dc-490b-b218-d07cc54f95f6",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "e84ace01-0d4c-410a-b274-3c687f72ca23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "15e7bc5f-6598-4ee3-a08f-b3af4ebfeb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84ace01-0d4c-410a-b274-3c687f72ca23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33875b15-9d71-4ef0-a987-3138adb0c157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84ace01-0d4c-410a-b274-3c687f72ca23",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "e09a0446-1d34-46f9-95ac-a578f0a5dcb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "748444a4-831d-4c31-8648-959477b9c75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09a0446-1d34-46f9-95ac-a578f0a5dcb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3d2337f-da4c-4248-b818-e510a02140fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09a0446-1d34-46f9-95ac-a578f0a5dcb9",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "51133b63-75b3-47b1-b9a9-98291992131c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "b3b1213f-7957-433e-91e3-57f51b7634a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51133b63-75b3-47b1-b9a9-98291992131c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac04ec8-bfbf-473f-9150-3282d0c5685b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51133b63-75b3-47b1-b9a9-98291992131c",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "10560d05-163e-4233-a8bb-18c1e844f5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "394284e9-8050-4dd1-b002-e79407b2fa8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10560d05-163e-4233-a8bb-18c1e844f5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041a4099-332f-4800-9414-b624a191d17f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10560d05-163e-4233-a8bb-18c1e844f5d9",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "b1bf5247-1d95-433c-b2c1-bfc0c6c05422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "0a28f4e2-2e34-4347-8535-bb8655ac2642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1bf5247-1d95-433c-b2c1-bfc0c6c05422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bee39d10-8a05-4761-a9c3-3ae6b59a3003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1bf5247-1d95-433c-b2c1-bfc0c6c05422",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "4c672e79-1c0f-41d4-8b26-d88d517c1ed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "b3ec837d-d59c-4696-a454-ef88ade84b5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c672e79-1c0f-41d4-8b26-d88d517c1ed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1d3e640-f32e-480b-9c34-d4d43abb4911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c672e79-1c0f-41d4-8b26-d88d517c1ed5",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "7d124012-f463-4f8f-819c-b65520205e9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "d347cd66-440c-44ea-9992-7d2b0ceecae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d124012-f463-4f8f-819c-b65520205e9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db3c226-bccd-4f6f-b5dd-5fcb6b7327f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d124012-f463-4f8f-819c-b65520205e9c",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "580b0eaa-feef-4cb9-a6a2-cd832ce7f441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "553842db-9b7b-4754-a819-2726f486dc3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "580b0eaa-feef-4cb9-a6a2-cd832ce7f441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5a81dce-e00c-4ad8-8393-b88dde81dbd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "580b0eaa-feef-4cb9-a6a2-cd832ce7f441",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "2a931ba5-17e8-4b2a-a9a9-2222137c3446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "0821a047-bb50-46d5-923c-d877bf5ec842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a931ba5-17e8-4b2a-a9a9-2222137c3446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "957cd7ac-e236-40b0-a4aa-9bc17cb2638d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a931ba5-17e8-4b2a-a9a9-2222137c3446",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "ea9d1008-b432-4dee-9aff-d4f7de57cafc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "b7229561-40e0-46c8-acfb-0e312078f0fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea9d1008-b432-4dee-9aff-d4f7de57cafc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3475d9-c64a-4ae0-af29-43876079be2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea9d1008-b432-4dee-9aff-d4f7de57cafc",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "f13fa286-b40b-498e-87d8-78cb2d02c4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "9c4f9561-6882-4865-9da1-86f644388f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13fa286-b40b-498e-87d8-78cb2d02c4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b04553e-8a6d-447c-89f2-4ff9e8124603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13fa286-b40b-498e-87d8-78cb2d02c4ef",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "22413ca6-4742-4d63-b148-1d702c39334f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "446db47c-be54-4a55-8967-5af50bebaf8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22413ca6-4742-4d63-b148-1d702c39334f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1d930a-d2fb-4e25-8aaa-efc27c58885b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22413ca6-4742-4d63-b148-1d702c39334f",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "7ce44be0-b747-45e6-a9fc-fa0e544584f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "db7e0c49-a36d-4df3-975f-39a1156eafbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce44be0-b747-45e6-a9fc-fa0e544584f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533e1c5b-e007-4cb6-aeba-f9f5edce2432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce44be0-b747-45e6-a9fc-fa0e544584f5",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "6fc5abe9-f7a2-464a-b0f4-e17fcfa33d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "7d17e461-0729-423f-a991-5d0a4f87bc05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc5abe9-f7a2-464a-b0f4-e17fcfa33d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d37ec1-308b-479f-834c-8dad5f92b97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc5abe9-f7a2-464a-b0f4-e17fcfa33d9a",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "d1c11ecf-2438-47db-b0e0-de054cf0ed38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "4c91e64f-7fe8-4341-b356-aefc1f9accb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c11ecf-2438-47db-b0e0-de054cf0ed38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0620bf13-4bef-4ea9-8ecd-06cc603e877d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c11ecf-2438-47db-b0e0-de054cf0ed38",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "6866fd80-98d4-4668-80f9-877466bb920a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "1c137360-8ada-4868-9952-697fa3ae949e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6866fd80-98d4-4668-80f9-877466bb920a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a302999-be9b-41e4-8542-8cd2a376cadf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6866fd80-98d4-4668-80f9-877466bb920a",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "cafdd7e5-dfff-4069-9a56-7904c4615864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "72912b41-ae2c-4372-a5f8-29347708db33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafdd7e5-dfff-4069-9a56-7904c4615864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a355c78-cb33-4446-836f-c22a64cc639f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafdd7e5-dfff-4069-9a56-7904c4615864",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "0572c4b3-9a6a-4447-a58d-045ffad27a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "7a982cee-b2df-49de-b4ee-7941df7ebbc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0572c4b3-9a6a-4447-a58d-045ffad27a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff2bf68-a426-4c49-9c78-dcd35e75012c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0572c4b3-9a6a-4447-a58d-045ffad27a7f",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "560633bb-1885-470c-af68-f2deff33d74d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "9e17d0d3-7b6b-4f7d-8712-d4fa28d7b7d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560633bb-1885-470c-af68-f2deff33d74d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e60fdcb-778f-441f-bd31-188c01bac503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560633bb-1885-470c-af68-f2deff33d74d",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "6673a8ec-450a-4e18-9918-0948776e671e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "a1a1d5ef-fc65-4567-8720-ef423f5181d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6673a8ec-450a-4e18-9918-0948776e671e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53eee6b2-078d-47f9-9fe4-edb1437787e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6673a8ec-450a-4e18-9918-0948776e671e",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "34ae749d-4ce3-4d81-b57a-4ae9e9237166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "1ff8c177-44f4-45ac-b4f3-df8d95e9615f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ae749d-4ce3-4d81-b57a-4ae9e9237166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d53a39-7a64-44d5-8756-e854b50030fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ae749d-4ce3-4d81-b57a-4ae9e9237166",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "4afe5cde-99b1-4efb-b5af-6c47e0cb7969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "663b561e-7634-4d0b-8fdb-cf6c58d92f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4afe5cde-99b1-4efb-b5af-6c47e0cb7969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fc089f0-496a-4eb0-88a4-b9860adcc35b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4afe5cde-99b1-4efb-b5af-6c47e0cb7969",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "e9b5c8d0-a64e-4e77-92aa-238b7f4e91d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "c2465bc7-e0c1-48a6-9cb7-c1dcd0d30762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b5c8d0-a64e-4e77-92aa-238b7f4e91d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ed8fc3-a95d-4812-b6e2-669e9f7b6195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b5c8d0-a64e-4e77-92aa-238b7f4e91d3",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "1471cebf-4e3d-4189-8ce1-11ddfbb0c541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "d511c81a-4058-4918-ad02-18b9453a8e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1471cebf-4e3d-4189-8ce1-11ddfbb0c541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12887963-5b73-484d-b62f-8e53699fa8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1471cebf-4e3d-4189-8ce1-11ddfbb0c541",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "cddc1040-8b39-47ba-bfa8-8dbc77734091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "75564029-a40d-4d35-bc3e-0bb877eb9779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cddc1040-8b39-47ba-bfa8-8dbc77734091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c1340d-09c6-42c6-9f5f-39c550e1a265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cddc1040-8b39-47ba-bfa8-8dbc77734091",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "ea21b6eb-c268-44ee-abb4-4db345c2f8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "09bc7d44-6dfb-4f36-b3ef-39a924cba7c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea21b6eb-c268-44ee-abb4-4db345c2f8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eae9438-b70c-4040-a122-392b01d664c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea21b6eb-c268-44ee-abb4-4db345c2f8eb",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "285d5f09-9713-49f4-aae7-00a35ede7cbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "bd0e97da-452b-4f3d-a168-ab2de4c7ea4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "285d5f09-9713-49f4-aae7-00a35ede7cbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b7de47-c9d6-44f3-91a2-98069d031b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "285d5f09-9713-49f4-aae7-00a35ede7cbf",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        },
        {
            "id": "4a505c48-74db-4047-aa37-c7b423c911a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "compositeImage": {
                "id": "892194bc-3f9c-4eda-9dbd-98594d193603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a505c48-74db-4047-aa37-c7b423c911a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc95c5ef-6c31-4d38-9b8c-258a2c2dd435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a505c48-74db-4047-aa37-c7b423c911a0",
                    "LayerId": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "81529a66-ff10-4fdc-ab8f-0d4148c4d2fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "883fb630-f0b5-4b27-b6d0-c5e33ba3dcb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}