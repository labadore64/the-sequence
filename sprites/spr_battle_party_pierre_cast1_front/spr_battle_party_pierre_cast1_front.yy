{
    "id": "224a91c6-8d7d-42e1-97c8-34cac5e85092",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 40,
    "bbox_right": 223,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d2cab74-914a-4de5-b218-f6538aaddcef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224a91c6-8d7d-42e1-97c8-34cac5e85092",
            "compositeImage": {
                "id": "4376d5c3-89de-40c4-b8a7-1cb8cf01e8f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2cab74-914a-4de5-b218-f6538aaddcef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e9db75-53f5-483c-a3b1-371acd2773e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2cab74-914a-4de5-b218-f6538aaddcef",
                    "LayerId": "2c30d144-afaf-4c82-bdda-1f3f16192e02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2c30d144-afaf-4c82-bdda-1f3f16192e02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "224a91c6-8d7d-42e1-97c8-34cac5e85092",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}