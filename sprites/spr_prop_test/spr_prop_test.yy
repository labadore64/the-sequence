{
    "id": "2b931465-e4c9-4d90-9dc4-cbe039b23e67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 46,
    "bbox_right": 77,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "962faecd-7a49-484c-8009-a5c678996ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b931465-e4c9-4d90-9dc4-cbe039b23e67",
            "compositeImage": {
                "id": "b2b1999f-532b-47e7-ac8e-b716148df7fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "962faecd-7a49-484c-8009-a5c678996ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f120d2-80b0-4c32-9a1e-edf1a0a12bd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "962faecd-7a49-484c-8009-a5c678996ddc",
                    "LayerId": "2ae19010-de93-4654-817d-b53cfd7f5c2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2ae19010-de93-4654-817d-b53cfd7f5c2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b931465-e4c9-4d90-9dc4-cbe039b23e67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}