{
    "id": "70d2db04-0ff9-4394-82ea-4585d853bcab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_fg_braille_burn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a05bd271-6c20-4af1-acd7-929ee7281a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70d2db04-0ff9-4394-82ea-4585d853bcab",
            "compositeImage": {
                "id": "13e4e716-10b2-4600-80a6-7cdb47e0fd89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a05bd271-6c20-4af1-acd7-929ee7281a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5950585b-707e-48b0-9868-f23f8c74f751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a05bd271-6c20-4af1-acd7-929ee7281a18",
                    "LayerId": "e59d4267-29f4-4580-a0cc-f53489b02c00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "e59d4267-29f4-4580-a0cc-f53489b02c00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70d2db04-0ff9-4394-82ea-4585d853bcab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}