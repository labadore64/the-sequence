{
    "id": "b0f15968-b163-4bdb-9e0c-f4af70e9cbae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_sheldon_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 217,
    "bbox_left": 37,
    "bbox_right": 235,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "854b5884-b971-40f4-9a98-94be65948ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0f15968-b163-4bdb-9e0c-f4af70e9cbae",
            "compositeImage": {
                "id": "52c8558c-9c66-41e0-b795-6a0f9f570e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854b5884-b971-40f4-9a98-94be65948ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09c82419-c632-40bc-8490-6b963871ba7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854b5884-b971-40f4-9a98-94be65948ae9",
                    "LayerId": "dc688498-52d5-415f-863b-4aed2643eebc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "dc688498-52d5-415f-863b-4aed2643eebc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0f15968-b163-4bdb-9e0c-f4af70e9cbae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "9e10e934-31a0-4a56-90f1-71dc8c1ec973",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}