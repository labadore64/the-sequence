{
    "id": "c64828ee-83a8-43b3-9756-834f08be341c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_daimen_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 29,
    "bbox_right": 216,
    "bbox_top": 77,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b3deb06-a191-4472-b7f1-a9363e86dd5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c64828ee-83a8-43b3-9756-834f08be341c",
            "compositeImage": {
                "id": "a55e113c-4026-4cfc-98c0-431b8452d83d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3deb06-a191-4472-b7f1-a9363e86dd5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c6b2c04-685a-4566-8c13-15b34b664f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3deb06-a191-4472-b7f1-a9363e86dd5d",
                    "LayerId": "fcabc35b-f6fe-4415-9d8e-3125df14a55d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fcabc35b-f6fe-4415-9d8e-3125df14a55d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c64828ee-83a8-43b3-9756-834f08be341c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4038d625-6d07-42e2-825f-bcc4b31256a3",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}