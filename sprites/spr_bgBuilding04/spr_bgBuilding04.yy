{
    "id": "8c5f21b1-091d-4a13-b79e-e3bfd2cad53f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgBuilding04",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 561,
    "bbox_left": 111,
    "bbox_right": 533,
    "bbox_top": 193,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "668faee4-f635-4037-a29d-cc7ec67f24cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c5f21b1-091d-4a13-b79e-e3bfd2cad53f",
            "compositeImage": {
                "id": "6cf30e6b-0224-48c3-a7c4-86b927f297a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668faee4-f635-4037-a29d-cc7ec67f24cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06144739-b97d-4701-879d-951c4c42484a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668faee4-f635-4037-a29d-cc7ec67f24cb",
                    "LayerId": "8ca5c947-a064-407e-92e7-06bf840efd74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "8ca5c947-a064-407e-92e7-06bf840efd74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c5f21b1-091d-4a13-b79e-e3bfd2cad53f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d88bd537-21cf-4c13-90d9-8153410ad825",
    "type": 0,
    "width": 825,
    "xorig": 0,
    "yorig": 0
}