{
    "id": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fontLarge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "afa8e26d-2f5d-4e80-bacb-3cd4ccff50d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0388b0c5-1bf8-4e1c-9fbf-51d0349246e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac5da3f-5fb4-482e-a3c2-e0c8c5a243ec",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "a837ee12-96a0-475b-a28f-8785b012cb21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "98c60c96-71ae-4150-8b94-b2634a711687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a837ee12-96a0-475b-a28f-8785b012cb21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b1b6b3-1d79-4314-9c3d-bddcb6c65eb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a837ee12-96a0-475b-a28f-8785b012cb21",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b95e2175-b01c-4c45-bb8d-faae7c886d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08ea7f0-cbda-4ade-9b85-c435c03810d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8922f33d-94e8-48a7-b471-f4eff7e532c8",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "362e1614-b346-4b20-b895-4876faa2512b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "df81891b-a24c-470c-b685-7927b6581c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362e1614-b346-4b20-b895-4876faa2512b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e304e443-f352-4c19-987b-403b193937ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362e1614-b346-4b20-b895-4876faa2512b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "271c2af2-92bd-4e3f-a8e3-5d4616008a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7160c764-548f-4aca-8d49-3ec9f254247b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6b8dbf-249f-4965-9907-dc59f4c75e31",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9c09a75e-0056-4d8e-a8ea-8bd69cd13401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4dc9d9-7ad8-4f74-9721-8b41aa9592b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38e1c42-09c2-4824-9afe-1ea4e773e3cf",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9399f2a9-dad2-4185-8997-c4c902dfd7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f7374cc-da83-4739-974d-d20bebb1f40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5ec308-22c2-4409-9dd7-5aa4e30de878",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "6dfa9b5f-dfb2-4098-8aea-5e87da87ef0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed6bc43-0a00-4f6d-808b-327507e49a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d528712-dc4e-4c5a-a150-b6d0e3b1eca2",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "35bcd022-5ee0-455a-ab45-4035fdb69164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "996fc92f-ae98-4704-8bc6-da8a22c74363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35bcd022-5ee0-455a-ab45-4035fdb69164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40714127-5e81-437a-af38-389e353f3e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bcd022-5ee0-455a-ab45-4035fdb69164",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "f76b9b49-ace0-4e6d-a755-78efd391304f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "be05406b-0838-4bad-b5cd-ea8992510aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76b9b49-ace0-4e6d-a755-78efd391304f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4944a6f-a7f8-4eb2-bbec-074d6324fcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76b9b49-ace0-4e6d-a755-78efd391304f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b7396498-8c62-4342-8557-3c2c424f59ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f31118-b3d0-41d0-b00f-df4e06ed3df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf96e6f9-cf16-4f2d-a4cd-e82ad356c049",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e38b9e27-cc9d-4d04-bd18-c33bca18a99c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d2dab9f-3fc7-42a0-a29e-a4ecfda0267e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f321052e-e8f5-430d-80ce-63fd5ac2c0b7",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "c144a520-3267-42f8-97ba-182d0f198d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "440b9263-4f59-4e9c-a867-1fc0a690b1af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c144a520-3267-42f8-97ba-182d0f198d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ccada93-2e68-4267-80ae-f0f5ac6a8eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c144a520-3267-42f8-97ba-182d0f198d31",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "c91a5ed1-3bad-429a-908c-e78d646d5649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f22fae3c-c3aa-4e61-a7aa-a9c37e478713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91a5ed1-3bad-429a-908c-e78d646d5649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37310c2b-b2f0-4fed-bb12-10757cdd0f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91a5ed1-3bad-429a-908c-e78d646d5649",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "45c36b6a-831b-4618-975a-d348eabe8aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9390ad13-3e4e-4469-9a11-09bedf3fc9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c36b6a-831b-4618-975a-d348eabe8aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf98708-5cb9-45b2-9c73-8bb8ce04e8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c36b6a-831b-4618-975a-d348eabe8aa6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "8389e2b9-7759-43b6-8477-d89d8df11776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "82cb701e-e346-4ec3-b11c-b1ff6edd180a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8389e2b9-7759-43b6-8477-d89d8df11776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5473c01-046c-4685-8b93-1cfbe12fa5c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8389e2b9-7759-43b6-8477-d89d8df11776",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "87fd8ba8-2df0-464a-a923-1d30e53fc63d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c3d1d9-4393-49d0-ac3a-d849f4c5015a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f95413dd-9f1d-4c67-a24c-769381d33ffd",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ee49b1b7-f2c0-4173-be49-1ce1e50436aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b320249-b869-4cdf-8ff2-cad53c3478fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d6dbdaf-7a41-4f8c-8917-704998d3fa57",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1101be79-3e98-4a97-8c03-6b0d2ae748c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9945320c-aad8-43e8-8add-da2c48a6d496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac47495c-9696-4fb9-8c73-b148b2cc9afa",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "878b4584-0896-4893-8bbd-66398acb35af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1307e09-58ff-4351-bd8f-6a1369406755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f025a2c-6e3a-4a2b-8226-be3421986cbb",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "e916d7d8-3576-49cf-851c-9858bb46357c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "344ac833-4a5c-44f6-af3d-1c3d6eb19f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e916d7d8-3576-49cf-851c-9858bb46357c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b1b1b6-c981-4a0a-bec5-b0f75707805a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e916d7d8-3576-49cf-851c-9858bb46357c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "13193fab-4995-4ca8-a7df-5facb2986f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68769296-a310-4968-ac8b-2ee724c515a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d5f3497-21bc-4ca1-a7ee-fedfcdc5f1e0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "66fa7325-d991-4369-963e-24df01292e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d6d14281-abcd-43e6-817d-5930b1f5f04a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fa7325-d991-4369-963e-24df01292e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "938f0ae4-d1b3-464a-a6bc-80437dd308c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fa7325-d991-4369-963e-24df01292e08",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "63227d54-8b3f-4b00-979b-3542c8461d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ec176cd6-ed72-4e71-a55d-e73c54543d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63227d54-8b3f-4b00-979b-3542c8461d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07cbc1e3-141b-4478-a418-d9d360286807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63227d54-8b3f-4b00-979b-3542c8461d4b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "65558847-b4aa-4669-8f86-263bf07f3e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85495c8f-1ffb-4c6a-9f11-c0116a8210d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3268f50-a8d3-4f50-ab27-2d08dc28f680",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "adefa744-dadf-430f-b5e1-e72b547305d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70501c66-76f8-4c32-a2fe-c0b4a1cb5268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65633e34-718a-4ce2-8d49-4b2fe6b2782b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "bd3de32f-6cd2-4b90-8cef-5c5ba5477854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e050fc8-ea0d-43a9-a1cc-8804b5cd4381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "738a0d26-fa8a-48f8-b534-866f4c35d9e2",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f01ba076-6033-4335-953d-eff8e3f82058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded5b7fe-c4e6-47b2-80e1-23f61881b2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c4bc9e-d917-4d4b-aca9-e8f4ead09a19",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "72b97c5d-be8f-4a7f-bff7-82916665264b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7c1c2f15-bad3-4d04-9cf2-b93067d0d07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72b97c5d-be8f-4a7f-bff7-82916665264b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d93577a4-ccd0-471b-bc35-bc0fee30d27e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b97c5d-be8f-4a7f-bff7-82916665264b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "bc7dec1e-d9ce-4854-a813-32218e4c5427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb8b0e6-d26d-4614-b582-83a36e780d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5a4271-bf23-480c-aa7b-a4b924e02e27",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d2f4bb65-aaf1-43e7-a8b6-91a50050b9c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d3be4a-975c-4c91-a540-dbe28abc62c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c20af64-014b-41e2-ac52-ecbcabc6f83a",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e4f6a643-145a-4899-b5de-8d278a39b448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7eee978-d4f9-4efc-816c-77c3f5c76cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4212d0cf-a6f7-4900-a508-c165cfe9668e",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "9338ef49-bf61-4cce-bdb3-9ba67d2ba472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8364a69-9576-4b36-ac55-4ed43ed4cb8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d3a7405-b8ce-4c45-8e54-2679102429d1",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "874cf113-24e6-4725-b19c-70784b17ddcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b53f8911-ad31-4c6f-a790-f8b4a9d21218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874cf113-24e6-4725-b19c-70784b17ddcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea783973-140a-4703-9266-15d41955f8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874cf113-24e6-4725-b19c-70784b17ddcb",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "6504f926-689f-453d-abaf-2a5e9e9d6160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "bc798851-35a3-432c-97ba-057587599944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6504f926-689f-453d-abaf-2a5e9e9d6160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b583e6-301a-4373-8f5d-1eed3aabe5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6504f926-689f-453d-abaf-2a5e9e9d6160",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "949a2782-b44a-496b-9c4d-f7f1cf861379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "24a362ed-5332-469c-949f-961f33450979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949a2782-b44a-496b-9c4d-f7f1cf861379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c7372f-6fa5-4a9d-b014-8a39daf585e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949a2782-b44a-496b-9c4d-f7f1cf861379",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "148684f2-2fb5-4399-8567-1f03a7f84a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca0647a-d2b7-4315-94e6-bd612e705244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe15e4e-8334-4299-acd2-b2e1c33250e9",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "89248728-c99d-447d-b6f7-a5044eae490b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "99414337-0a16-444c-83a8-955e80e9467b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89248728-c99d-447d-b6f7-a5044eae490b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ae269e-48ae-411d-8882-eb6c453389e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89248728-c99d-447d-b6f7-a5044eae490b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "49acab48-77a7-406c-9379-eca923bed749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "161df8c1-79c8-44c9-9fec-d26f20d6c185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49acab48-77a7-406c-9379-eca923bed749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928be747-a593-4118-9614-dab470fba158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49acab48-77a7-406c-9379-eca923bed749",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "42822745-5135-4a9a-976b-33d61b0694f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e36a633b-ddbf-4bd6-a975-b6ef5cc1ca57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42822745-5135-4a9a-976b-33d61b0694f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0433a209-3975-4650-aee5-258cf97d5b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42822745-5135-4a9a-976b-33d61b0694f0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "f9f4789b-1461-44bd-a442-f2c49e86b125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c367ca-0426-4ddc-9f62-01ea5da19482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96beecd0-3bf9-4e33-954d-520bcf3bce06",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "8043ec9e-ec63-4203-8971-b001723aca33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5f7234-8420-468c-a81b-e7d95c1ea68a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb37fc7-45c9-484e-9558-5b18734a2fee",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "47aa7f55-695f-4dce-a79f-9cc37ff346d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b9ba85-5db6-42f5-b755-d484106f9cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0354646-7cb1-4cd8-9487-d991fe49ed51",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "97f1c75e-31a0-43c2-abdb-27deb56989d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3218689f-ffb4-4376-a402-8d4deb3fc370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb686a7-475f-4cea-80d0-682986fa2d8b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "dc54520f-0680-4902-9331-a037ba72a499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce1596e-f8b8-48dc-be69-6d66f42a7991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb31ed86-0a0b-4d6e-b068-3fe6335f3aaf",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "b9385cbb-b8e2-42f5-a834-1512a04ec88e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a678098-e725-4bf8-bea1-d4a72d87637e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f09cf6-d647-499a-a9dd-2653f37a7dce",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "657c6fdb-febd-4565-a6bd-ffbee3357417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a96deb-ac8b-46f4-900e-90a61babef06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bbbf7e-ff3b-438f-93ec-00457dfc2054",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "38ad37ca-df4f-4848-906a-496f76936235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "6a239f81-cb0f-4c87-9c6e-acb32f1b5e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ad37ca-df4f-4848-906a-496f76936235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee40c47-51a3-480e-8922-89a365596bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ad37ca-df4f-4848-906a-496f76936235",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "5d333f4f-3f40-4471-839f-0276a293ebda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "76771879-abb4-4ceb-88d8-df29e6d2e13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d333f4f-3f40-4471-839f-0276a293ebda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ef7e628-a542-4388-8603-a3a7ece9382d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d333f4f-3f40-4471-839f-0276a293ebda",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "4c6b0124-74d8-4e07-b8f7-803d726fd8d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061aeac7-9678-47dd-b523-21c382918406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd1d27b-4147-456b-aa7d-5dc3893f5f96",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "19863bed-ca24-444d-8878-2817cf1fe625",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2580aa14-a2cc-4ca8-b61b-7aa1e84b23a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1a1a6c-b1cf-4ad2-9426-c3649d12583b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "7669ac4a-41b0-4aea-907b-963c26f1f360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b6aeb7-f2d4-4dfd-ac06-77b154e2d445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e339e7c-b170-446f-bdc3-5b251a818bd0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "abe7bf06-1658-49de-8498-8646b4b63fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d20a381c-3014-4538-aa74-3dbef136e5dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe7bf06-1658-49de-8498-8646b4b63fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c28801f5-b8cc-4ff3-a2fd-7f7a0a7c332d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe7bf06-1658-49de-8498-8646b4b63fa6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "abbf6b96-59d5-4487-b3b2-de9bb9fcfe8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59624008-61f7-4d7b-aa53-2f23f6e94975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad70d77d-3c34-4907-8e18-ddbd261bcd90",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ac2884ca-d670-4532-a48b-76d19649cd5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7da04d-24ba-483d-9ce1-b467cff864c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6d24d0-42d0-4177-b928-7f84c7418a24",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "15fbbd04-e0e7-46a6-be88-03f90672e932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "653e0ca5-680a-4361-a272-64715361d122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2612aaf1-619e-4b83-b8a5-da7fd002f42c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1750bc38-1710-4b35-b726-f8b2dd257bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "912b7bf5-5434-4db7-af14-e4e995d5bf2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b4868e-230f-4cf3-88e4-e71f8b243341",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "ac848319-1a38-4234-b50b-600c1ec72730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "215f5aef-f98b-4943-b30f-60b53429235c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac848319-1a38-4234-b50b-600c1ec72730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a45c4e-e34c-455b-bca3-c6383a7bb975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac848319-1a38-4234-b50b-600c1ec72730",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "93bd620d-6696-4609-9b93-2a29f98aae91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "498a9dca-8315-4c89-b146-657ad88104fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93bd620d-6696-4609-9b93-2a29f98aae91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b86b0e-286f-4f94-9141-4710ccd5c0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bd620d-6696-4609-9b93-2a29f98aae91",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "604a67f7-2921-4cb2-8562-ab2fda1b30ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3f6f94-95e6-4ee6-8e99-2edbc688e474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1436ff47-aa6c-4cc0-a940-08bc8fc4012e",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "475870bb-0ce8-41f3-9376-2671e28bda6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "64a7d9b9-0a05-4c52-a813-63b53da4eff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475870bb-0ce8-41f3-9376-2671e28bda6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65b992c5-34ac-4117-9206-f6f332570ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475870bb-0ce8-41f3-9376-2671e28bda6c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "926e6485-80f5-43f3-80e9-ea90377e4a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf28688-3f85-441a-9dfd-29cf13b20fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a951e0d-b44f-4c3e-86bd-d6e86c337a2c",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "3e08624b-7f45-4a70-923a-9341fa3818d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ae72bb67-7335-48e7-885c-eb7a7232b89e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e08624b-7f45-4a70-923a-9341fa3818d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d35af7a-98fc-4b56-b542-623ebc982083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e08624b-7f45-4a70-923a-9341fa3818d3",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "fd3454bb-fce6-4497-bd6a-dd24d138d8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8ef369-d221-4b4a-8886-b6ef9733a875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b164cbe-ba22-41dc-959e-464b1aca23e0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ae773464-7e2a-415e-af7f-afa51a2645c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a023ef-3a77-488a-8a83-f0fd2981cb7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcc051a-5b1b-4c51-a846-79a2e3663a05",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "d1b4ce2e-f334-450d-b5d5-d736d4caebe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1124f66f-46c3-4a72-b415-ca7b193ab9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf9aadc-f5c4-4638-bbf5-128d20250044",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "a36bf504-f103-4cb1-89ac-322e81004804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "044edf02-a256-4d37-8611-e4dd4ad4be57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36bf504-f103-4cb1-89ac-322e81004804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9980189a-5087-4ebb-bd6d-51ecdc0ce37f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36bf504-f103-4cb1-89ac-322e81004804",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "6ca0d132-d655-4b22-995f-03f52c5768af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ee0f3ebd-67b2-45e6-8643-6889127bb851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca0d132-d655-4b22-995f-03f52c5768af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59c28a6-0cee-4232-bec2-c1718e914aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca0d132-d655-4b22-995f-03f52c5768af",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "d9fbf6d0-127c-44df-8a94-b062e795131f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ff339706-8e92-4d8f-ad98-6cd0e37bad91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9fbf6d0-127c-44df-8a94-b062e795131f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6741d6e6-ed53-4075-abe1-1a8dff723ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9fbf6d0-127c-44df-8a94-b062e795131f",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "12a3add5-daa9-4512-9b26-c6bb0629662b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "c9d10542-cf1f-4d4a-a68b-10d63da47a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12a3add5-daa9-4512-9b26-c6bb0629662b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e30d581b-7c54-48f3-9466-18909004f169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12a3add5-daa9-4512-9b26-c6bb0629662b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "1ab04ab8-ae4d-427f-bc27-be7a438aafa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a6707d4-aa56-4ab7-a1d1-aeb9c4c459eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03ee9fb-abc3-4c3d-a3c6-b3c666ef9b24",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e48a82d5-cfc6-4607-9f5f-b3bc4c798764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9dd7d6-13c6-4cee-8fc0-3d96ba360e74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cba07ca-376c-44a7-a5f8-ea05c15755fd",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "dc511ca5-12bb-4fb6-88a5-22cf0edbe58c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1543290-de68-4f1a-9b21-67cede6f5e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a398bb53-6e7d-49ea-b03c-9f06a04e8b13",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "217e16db-7d91-40a4-9df2-8d0043f9ce22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf1ff24-a71e-45b7-898d-6c25bca0c055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc7c40b-7111-48bb-aff5-fb64a8917eb6",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "26892c83-0bba-4e76-ba27-021694cd58fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "73c7ec39-8348-470a-8d0d-d8ff4e739cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26892c83-0bba-4e76-ba27-021694cd58fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8881cd-f14d-4949-a85a-433b31525be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26892c83-0bba-4e76-ba27-021694cd58fa",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "8e54e858-915c-4868-8832-ca3f8a796999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7371d56f-9293-463c-a797-e3e99b823c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3e623a-6744-4774-b7f5-02d8c78af4f0",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "04181ce5-7444-463f-91f1-47dd882475e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "06e875f8-c37f-4536-8a34-9257975b3abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04181ce5-7444-463f-91f1-47dd882475e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf26064-e7c0-4ead-99c9-194c0eb95d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04181ce5-7444-463f-91f1-47dd882475e1",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "75fcd19d-e680-4bb1-a16b-d441fc377634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "88faf19a-6692-424e-bd33-6b4be0594258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75fcd19d-e680-4bb1-a16b-d441fc377634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b84457-ec09-40fd-8e8a-f39c8e996692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75fcd19d-e680-4bb1-a16b-d441fc377634",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "ebe4e4a1-dd02-497f-87a2-597c62828f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa4e49c3-e431-436f-a120-d9452ec6e373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64942201-f8af-4e3d-8f93-a171e5fcc47b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "d8341155-f1e7-4511-9d16-403712a58a7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e79e40c1-7fc3-44f1-b3d6-c14cccfa3a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8341155-f1e7-4511-9d16-403712a58a7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a172a07-416a-40b0-8247-600371744df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8341155-f1e7-4511-9d16-403712a58a7b",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "dd72dfcc-f606-41b2-be11-f0d87270b895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "41834685-18b5-4e48-98df-cabfcf183301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd72dfcc-f606-41b2-be11-f0d87270b895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab710c41-f68f-4824-9b1e-9505ed6c8cf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd72dfcc-f606-41b2-be11-f0d87270b895",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "6ea6737f-5a1f-4a8f-b96e-b64a50e4a56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45a66db-1534-4a8a-baff-d6b4a6d2c23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d714a7a-b5f6-4cc2-b596-c3020f6f4a68",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "e879ad83-a521-459b-adfe-933f4a790e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f8f8ea-428a-4f76-ac46-59f24a73d816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a690945-8553-449d-b7ca-80b9e6ab4fb4",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "389817e1-3ff3-4e4f-8640-c204125862d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "5c5b2a15-c3f9-4e12-addc-772652f304dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389817e1-3ff3-4e4f-8640-c204125862d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3273bf1b-be0f-401e-aa0f-1af86f6cd86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389817e1-3ff3-4e4f-8640-c204125862d8",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        },
        {
            "id": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "compositeImage": {
                "id": "2a7ae38e-2176-4a38-9c05-07eca6b389a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d42feb77-2b7e-48fe-bb5e-0db421bd074d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd357e7-a0b5-4af0-8fbe-c6b7c82544a7",
                    "LayerId": "16b8bc86-31a6-42ac-aeab-86247216000d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "16b8bc86-31a6-42ac-aeab-86247216000d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66ca4f6d-0c94-4dfb-bf4c-3f1288ac87fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "65af0988-cb88-42df-b650-971d782eab72",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}