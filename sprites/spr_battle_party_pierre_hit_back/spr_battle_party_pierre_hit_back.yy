{
    "id": "d7babf55-2f75-4888-9bb4-9d3c2e030c55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 25,
    "bbox_right": 230,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46b22b11-a0c7-41d4-b067-b328e1897906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7babf55-2f75-4888-9bb4-9d3c2e030c55",
            "compositeImage": {
                "id": "49d858a7-9814-450d-b585-73238daecc6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b22b11-a0c7-41d4-b067-b328e1897906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3656f5f6-4800-4608-b43b-eae1a236f152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b22b11-a0c7-41d4-b067-b328e1897906",
                    "LayerId": "a5635055-549f-4572-9906-9d3d6d6afb40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a5635055-549f-4572-9906-9d3d6d6afb40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7babf55-2f75-4888-9bb4-9d3c2e030c55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}