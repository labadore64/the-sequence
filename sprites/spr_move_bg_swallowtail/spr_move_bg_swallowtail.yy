{
    "id": "8a85fc76-fd71-47b4-bb2f-f39bacb6073b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_swallowtail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 292,
    "bbox_left": 0,
    "bbox_right": 90,
    "bbox_top": 286,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94c9e409-3271-4441-8a0d-02448035dda0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a85fc76-fd71-47b4-bb2f-f39bacb6073b",
            "compositeImage": {
                "id": "046a7d94-9b39-4fd0-9a8a-8eb342593444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c9e409-3271-4441-8a0d-02448035dda0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0dd04a-a529-4481-b3e4-3c0261a25194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c9e409-3271-4441-8a0d-02448035dda0",
                    "LayerId": "e07318f9-b1cc-4885-9d78-b6492ee4e9c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "e07318f9-b1cc-4885-9d78-b6492ee4e9c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a85fc76-fd71-47b4-bb2f-f39bacb6073b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.50025,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}