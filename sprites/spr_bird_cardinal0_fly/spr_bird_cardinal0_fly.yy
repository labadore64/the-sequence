{
    "id": "48f5147a-b79c-44d6-b4bd-7bd6f4dc865d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_cardinal0_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 24,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2a7f72b-641d-4a75-ae49-a405a6398ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f5147a-b79c-44d6-b4bd-7bd6f4dc865d",
            "compositeImage": {
                "id": "22068711-5164-4d9d-ac77-4c5de35d0ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a7f72b-641d-4a75-ae49-a405a6398ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b97e10-1c23-455c-be81-5729504fa8a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a7f72b-641d-4a75-ae49-a405a6398ca2",
                    "LayerId": "31918de8-b9d2-407d-8ff1-591c8d84123e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "31918de8-b9d2-407d-8ff1-591c8d84123e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48f5147a-b79c-44d6-b4bd-7bd6f4dc865d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}