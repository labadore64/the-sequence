{
    "id": "1657b57c-9265-4aa5-93d8-d770fb516eba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 52,
    "bbox_right": 176,
    "bbox_top": 101,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b117d8b8-0ecb-4a1a-816d-44d68b678c88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1657b57c-9265-4aa5-93d8-d770fb516eba",
            "compositeImage": {
                "id": "58ae87b3-ec98-43be-adfa-fc080b287163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b117d8b8-0ecb-4a1a-816d-44d68b678c88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05efc69b-c2ce-4d29-85ed-7f870bd2c621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b117d8b8-0ecb-4a1a-816d-44d68b678c88",
                    "LayerId": "79a98bdf-f7b6-4124-9fbd-28655ff84c72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "79a98bdf-f7b6-4124-9fbd-28655ff84c72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1657b57c-9265-4aa5-93d8-d770fb516eba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}