{
    "id": "969b9a00-4b31-423b-a637-7e9b25ac6ada",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainCharacterStandBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 231,
    "bbox_left": 89,
    "bbox_right": 168,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbe4b3d2-8abd-4b65-b236-c09238b41462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "969b9a00-4b31-423b-a637-7e9b25ac6ada",
            "compositeImage": {
                "id": "245eae8e-9325-4e4c-a1f3-418cd2acc7b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbe4b3d2-8abd-4b65-b236-c09238b41462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59d1d80c-6ca7-4748-af0c-1769ae204890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbe4b3d2-8abd-4b65-b236-c09238b41462",
                    "LayerId": "d736c0ce-0ad7-47fd-9e54-ce96615bc51a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d736c0ce-0ad7-47fd-9e54-ce96615bc51a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "969b9a00-4b31-423b-a637-7e9b25ac6ada",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}