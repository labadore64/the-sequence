{
    "id": "583ec971-e328-48b4-9b3a-0b3bf65b0c5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_physpower_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af9f65c9-622b-4aaf-85aa-779473572a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "583ec971-e328-48b4-9b3a-0b3bf65b0c5b",
            "compositeImage": {
                "id": "99f5f369-d6c3-4c64-ac97-72c57974916a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9f65c9-622b-4aaf-85aa-779473572a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e188e7-6c84-4407-9475-ded728a54e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9f65c9-622b-4aaf-85aa-779473572a3c",
                    "LayerId": "ea83ca43-69ba-4130-9660-082bcedf42ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ea83ca43-69ba-4130-9660-082bcedf42ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "583ec971-e328-48b4-9b3a-0b3bf65b0c5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}