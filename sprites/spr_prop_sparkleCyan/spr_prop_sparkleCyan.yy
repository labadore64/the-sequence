{
    "id": "cedff800-996d-4ca2-84ac-7b15c6577698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sparkleCyan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 55,
    "bbox_right": 73,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a7a2a5e-cfa7-4969-9378-4a89264ed6cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cedff800-996d-4ca2-84ac-7b15c6577698",
            "compositeImage": {
                "id": "a279c0d5-130c-490c-b0e4-9451d13da4e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a7a2a5e-cfa7-4969-9378-4a89264ed6cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e3517b-71b3-43f2-8be9-a3807a480c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a7a2a5e-cfa7-4969-9378-4a89264ed6cc",
                    "LayerId": "6cac8165-f00d-4821-b839-a841623cfa44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6cac8165-f00d-4821-b839-a841623cfa44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cedff800-996d-4ca2-84ac-7b15c6577698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}