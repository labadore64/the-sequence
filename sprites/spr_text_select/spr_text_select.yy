{
    "id": "7f31e931-c1d0-4fdc-a0cc-d4610a28b376",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 13,
    "bbox_right": 52,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb68e6a0-746b-49dd-90a3-d1b8c48f6ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f31e931-c1d0-4fdc-a0cc-d4610a28b376",
            "compositeImage": {
                "id": "f049b8c9-1c91-4f0b-8f75-0074cccdd8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb68e6a0-746b-49dd-90a3-d1b8c48f6ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97df7063-3a79-4069-a99c-a6a75dbdbf04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb68e6a0-746b-49dd-90a3-d1b8c48f6ae6",
                    "LayerId": "f0109c77-7d0b-4397-83b6-e8ec231b89cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f0109c77-7d0b-4397-83b6-e8ec231b89cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f31e931-c1d0-4fdc-a0cc-d4610a28b376",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 31
}