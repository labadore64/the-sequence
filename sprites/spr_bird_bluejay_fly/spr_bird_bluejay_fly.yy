{
    "id": "0e9079c9-2c77-4c83-89e3-cc2257b4de54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_bluejay_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 15,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67666e5f-314f-4b5a-a092-d84a27d2166b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9079c9-2c77-4c83-89e3-cc2257b4de54",
            "compositeImage": {
                "id": "f9644c55-f25d-4a9f-be8d-d275f50fedd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67666e5f-314f-4b5a-a092-d84a27d2166b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2d63420-a2c1-4a06-ad89-2ebbb67cdb19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67666e5f-314f-4b5a-a092-d84a27d2166b",
                    "LayerId": "c615e951-d7ef-48c9-973e-e7dff8201199"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c615e951-d7ef-48c9-973e-e7dff8201199",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e9079c9-2c77-4c83-89e3-cc2257b4de54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}