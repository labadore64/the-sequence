{
    "id": "56cb6a69-3e5a-4120-be6b-2d78b6bc3c78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait_0000FF_neutral",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 60,
    "bbox_right": 446,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e86b9da7-9bb6-4147-bc38-8bb95fd311ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56cb6a69-3e5a-4120-be6b-2d78b6bc3c78",
            "compositeImage": {
                "id": "9d241952-ba2b-4476-b7cf-0bff285b0da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86b9da7-9bb6-4147-bc38-8bb95fd311ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e24b650-4b04-4b6f-8531-5111921d2bb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86b9da7-9bb6-4147-bc38-8bb95fd311ba",
                    "LayerId": "e9b76adf-0b05-4ada-bab6-85805e19716b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "e9b76adf-0b05-4ada-bab6-85805e19716b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56cb6a69-3e5a-4120-be6b-2d78b6bc3c78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}