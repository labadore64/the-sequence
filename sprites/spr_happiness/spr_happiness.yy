{
    "id": "0b2b773a-bec1-49bb-a52e-97959bd98788",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_happiness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 13,
    "bbox_right": 51,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "e3885462-852a-4e02-bba3-862f56dda932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b2b773a-bec1-49bb-a52e-97959bd98788",
            "compositeImage": {
                "id": "75afa3c7-4fb3-4754-9ce1-b1a0625b23f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3885462-852a-4e02-bba3-862f56dda932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9594307-c96b-4552-aa2a-aeef14570148",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3885462-852a-4e02-bba3-862f56dda932",
                    "LayerId": "d5070364-9b0a-4802-847a-aa76548067e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d5070364-9b0a-4802-847a-aa76548067e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b2b773a-bec1-49bb-a52e-97959bd98788",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}