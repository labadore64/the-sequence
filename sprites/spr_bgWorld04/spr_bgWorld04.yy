{
    "id": "ff3cf2af-79ac-481f-88cd-e110e2fbfedf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgWorld04",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1199,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac795d50-9ea9-4207-962c-ca0e05cc8aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cf2af-79ac-481f-88cd-e110e2fbfedf",
            "compositeImage": {
                "id": "853695fc-1c9b-4144-8bdd-c1c0eef2714b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac795d50-9ea9-4207-962c-ca0e05cc8aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c55eb58-5715-45c9-9a75-943f720f2e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac795d50-9ea9-4207-962c-ca0e05cc8aac",
                    "LayerId": "277b2ad8-2c7b-4561-bf28-44c393d3ae8d"
                },
                {
                    "id": "c62f6a79-1e4f-4736-9f12-a07320373e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac795d50-9ea9-4207-962c-ca0e05cc8aac",
                    "LayerId": "b965f71d-f08f-4fc9-8149-eb4b45483a30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1200,
    "layers": [
        {
            "id": "b965f71d-f08f-4fc9-8149-eb4b45483a30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff3cf2af-79ac-481f-88cd-e110e2fbfedf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "277b2ad8-2c7b-4561-bf28-44c393d3ae8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff3cf2af-79ac-481f-88cd-e110e2fbfedf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "c59d5afc-a3e4-4946-9095-840bb00c9dba",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}