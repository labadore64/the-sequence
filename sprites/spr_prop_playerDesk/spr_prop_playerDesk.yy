{
    "id": "b175fe95-e77e-4e0c-917f-28043d826586",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_playerDesk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 8,
    "bbox_right": 124,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf0b6003-f279-40a2-8857-d2deff306f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b175fe95-e77e-4e0c-917f-28043d826586",
            "compositeImage": {
                "id": "761f6333-9470-445d-861d-08bb38528a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0b6003-f279-40a2-8857-d2deff306f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e341a32d-0713-448e-bc85-e28b80075e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0b6003-f279-40a2-8857-d2deff306f2c",
                    "LayerId": "3a466b6d-6ab2-4841-85fc-d3d7f17a1c8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3a466b6d-6ab2-4841-85fc-d3d7f17a1c8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b175fe95-e77e-4e0c-917f-28043d826586",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.767750263,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}