{
    "id": "c63681d7-db50-42b8-bfe1-d03e55e69e6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 75,
    "bbox_right": 186,
    "bbox_top": 80,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "723b5714-d2fb-432d-b9e1-22bc18a8902b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c63681d7-db50-42b8-bfe1-d03e55e69e6c",
            "compositeImage": {
                "id": "977e9549-1345-4c94-b6eb-2829c0cb10de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "723b5714-d2fb-432d-b9e1-22bc18a8902b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0391597e-ca23-4040-9110-d49eefb8967d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "723b5714-d2fb-432d-b9e1-22bc18a8902b",
                    "LayerId": "b7dca273-9c2d-4990-a01d-05aaadaa0b67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b7dca273-9c2d-4990-a01d-05aaadaa0b67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c63681d7-db50-42b8-bfe1-d03e55e69e6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}