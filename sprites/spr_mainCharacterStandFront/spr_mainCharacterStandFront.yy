{
    "id": "a314b28c-1027-400d-ade0-93ba910f74fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainCharacterStandFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 88,
    "bbox_right": 164,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8915fa2f-dd69-467a-8085-9a5fb0aa9d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a314b28c-1027-400d-ade0-93ba910f74fa",
            "compositeImage": {
                "id": "229a460f-2983-4732-b471-31d4b5085f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8915fa2f-dd69-467a-8085-9a5fb0aa9d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "465a372c-3d03-4377-a5ef-27af7e379571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8915fa2f-dd69-467a-8085-9a5fb0aa9d38",
                    "LayerId": "0ff19b3a-7eb5-4a5a-95cd-11813845b170"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0ff19b3a-7eb5-4a5a-95cd-11813845b170",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a314b28c-1027-400d-ade0-93ba910f74fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}