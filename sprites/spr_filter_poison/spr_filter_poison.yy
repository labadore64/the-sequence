{
    "id": "c6143eaf-209f-4d54-a1f7-b78a03c4a450",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_poison",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 68,
    "bbox_right": 182,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c19881e8-35ee-4996-822d-2c9c7c823cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6143eaf-209f-4d54-a1f7-b78a03c4a450",
            "compositeImage": {
                "id": "dfa91901-385b-4351-a7c3-99f6daa3a613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19881e8-35ee-4996-822d-2c9c7c823cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce5ca77-6c05-4714-a913-ea2c4b23a613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19881e8-35ee-4996-822d-2c9c7c823cc1",
                    "LayerId": "497cd1fc-ef2a-4af5-984f-e742a4768aa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "497cd1fc-ef2a-4af5-984f-e742a4768aa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6143eaf-209f-4d54-a1f7-b78a03c4a450",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 115
}