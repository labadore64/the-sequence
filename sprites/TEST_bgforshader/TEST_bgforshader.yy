{
    "id": "2ced81e0-94ce-4047-a61b-0be5498a8b8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TEST_bgforshader",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 958,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8177895a-1392-426b-97d9-e3cd862540a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ced81e0-94ce-4047-a61b-0be5498a8b8c",
            "compositeImage": {
                "id": "e88b1211-4f6d-495b-9d86-41b63504c92a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8177895a-1392-426b-97d9-e3cd862540a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f729051-fecb-4bc1-9cf2-b77919aa04a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8177895a-1392-426b-97d9-e3cd862540a8",
                    "LayerId": "0a757c29-fc98-45e6-8db9-14de9dea8b33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 959,
    "layers": [
        {
            "id": "0a757c29-fc98-45e6-8db9-14de9dea8b33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ced81e0-94ce-4047-a61b-0be5498a8b8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}