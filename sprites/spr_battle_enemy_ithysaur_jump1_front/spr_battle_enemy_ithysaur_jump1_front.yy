{
    "id": "8ff2c155-3b8f-4769-b8a8-3cee2657618b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 208,
    "bbox_left": 5,
    "bbox_right": 241,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d1d7801-5b15-4a02-b6b6-f1ad64d5192e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff2c155-3b8f-4769-b8a8-3cee2657618b",
            "compositeImage": {
                "id": "1f5b051e-86c1-4b0c-a53f-5d20fc2e2d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1d7801-5b15-4a02-b6b6-f1ad64d5192e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee880276-c108-48a0-98da-6f4610dc6308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1d7801-5b15-4a02-b6b6-f1ad64d5192e",
                    "LayerId": "cc97dfe9-1b7b-421b-9e7f-fc57609260c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "cc97dfe9-1b7b-421b-9e7f-fc57609260c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ff2c155-3b8f-4769-b8a8-3cee2657618b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}