{
    "id": "f19b445b-527b-46e4-85fc-6f5302ecffb3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 45,
    "bbox_right": 185,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "215f66ec-beb4-4c97-aafc-8003c921b1ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f19b445b-527b-46e4-85fc-6f5302ecffb3",
            "compositeImage": {
                "id": "91935baf-6807-42de-b691-9d2ab136258e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "215f66ec-beb4-4c97-aafc-8003c921b1ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c04c76-7f51-4843-8c7a-2d4c4371aa82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "215f66ec-beb4-4c97-aafc-8003c921b1ac",
                    "LayerId": "8b855a00-ec20-4bfc-b9a4-37f2c734fd14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8b855a00-ec20-4bfc-b9a4-37f2c734fd14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f19b445b-527b-46e4-85fc-6f5302ecffb3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}