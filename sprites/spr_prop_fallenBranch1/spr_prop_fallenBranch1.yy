{
    "id": "e3de5eea-9412-4529-8ca3-ddbaa1d40639",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_fallenBranch1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 18,
    "bbox_right": 112,
    "bbox_top": 53,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94f379c9-4298-4377-b641-4efc43508d33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3de5eea-9412-4529-8ca3-ddbaa1d40639",
            "compositeImage": {
                "id": "bbf46aa9-85e9-498f-a88a-07d47ad8b03a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f379c9-4298-4377-b641-4efc43508d33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66654af-9828-41b8-822b-9ae8763520dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f379c9-4298-4377-b641-4efc43508d33",
                    "LayerId": "4b78384b-c8ee-4718-ba62-b3d09a2c90e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4b78384b-c8ee-4718-ba62-b3d09a2c90e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3de5eea-9412-4529-8ca3-ddbaa1d40639",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}