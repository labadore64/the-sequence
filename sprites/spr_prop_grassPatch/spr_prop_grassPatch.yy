{
    "id": "5b1402b7-048a-429e-936a-504937bb4890",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_grassPatch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 44,
    "bbox_right": 82,
    "bbox_top": 98,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d72ff46-206c-4356-af94-cd93cdd86279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b1402b7-048a-429e-936a-504937bb4890",
            "compositeImage": {
                "id": "444df44e-7498-4378-964f-d30eb2e24cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d72ff46-206c-4356-af94-cd93cdd86279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f069c5-733c-4648-823f-47519d0bf76b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d72ff46-206c-4356-af94-cd93cdd86279",
                    "LayerId": "1882421a-20ed-4f2b-b06f-c8224359d038"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1882421a-20ed-4f2b-b06f-c8224359d038",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b1402b7-048a-429e-936a-504937bb4890",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}