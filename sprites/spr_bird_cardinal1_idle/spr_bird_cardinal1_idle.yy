{
    "id": "a58c8e07-8486-4a07-a621-a98b08a33bfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_cardinal1_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 27,
    "bbox_right": 103,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76e1d5b6-b632-40e8-8ae3-7dc28dd2e93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a58c8e07-8486-4a07-a621-a98b08a33bfa",
            "compositeImage": {
                "id": "499a7308-5e38-4bde-96fb-bd836d6a4334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e1d5b6-b632-40e8-8ae3-7dc28dd2e93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9897d922-a406-4960-a3b4-291f0fd51842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e1d5b6-b632-40e8-8ae3-7dc28dd2e93f",
                    "LayerId": "f30d87e3-b6e8-4f3a-b94d-8c8a98d03585"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f30d87e3-b6e8-4f3a-b94d-8c8a98d03585",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a58c8e07-8486-4a07-a621-a98b08a33bfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}