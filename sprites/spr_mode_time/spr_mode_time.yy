{
    "id": "751683f1-0e66-4458-a2c7-8d533c374171",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mode_time",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 7,
    "bbox_right": 53,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e94c08d3-a27d-4fe9-89d1-f6acf3031fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751683f1-0e66-4458-a2c7-8d533c374171",
            "compositeImage": {
                "id": "f3e3a1df-cfc7-47ae-8b83-6bd5577c5e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e94c08d3-a27d-4fe9-89d1-f6acf3031fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2502dd9f-b569-4adb-ad9b-8da80ba0e97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e94c08d3-a27d-4fe9-89d1-f6acf3031fde",
                    "LayerId": "426d3b61-45c8-4b5c-b825-8b6b8c1e85ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "426d3b61-45c8-4b5c-b825-8b6b8c1e85ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "751683f1-0e66-4458-a2c7-8d533c374171",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 1.33700013,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}