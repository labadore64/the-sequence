{
    "id": "551e0f39-8992-4163-bd57-8f897e2ed99e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_dippertail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 18,
    "bbox_right": 219,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c1f9efa-3532-497a-8260-fc3a80638b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551e0f39-8992-4163-bd57-8f897e2ed99e",
            "compositeImage": {
                "id": "09267352-2ffa-4886-a0ff-43a6c28fd30a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1f9efa-3532-497a-8260-fc3a80638b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d293fa11-d00e-46ff-816f-ccc2c17719f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1f9efa-3532-497a-8260-fc3a80638b5b",
                    "LayerId": "2588b791-599a-4b86-8575-7f463f8fe070"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2588b791-599a-4b86-8575-7f463f8fe070",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "551e0f39-8992-4163-bd57-8f897e2ed99e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}