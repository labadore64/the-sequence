{
    "id": "429fce12-0e6e-4617-a13e-99c231cf5302",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_fg_medusa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "912154ce-76df-4a43-b097-e634eaf63375",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "429fce12-0e6e-4617-a13e-99c231cf5302",
            "compositeImage": {
                "id": "f20c06fc-5062-4c2a-9cc2-5c1cf66e2ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "912154ce-76df-4a43-b097-e634eaf63375",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f55319b-f38e-4332-bc1b-4efbd374497a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "912154ce-76df-4a43-b097-e634eaf63375",
                    "LayerId": "5e721a35-fdea-421f-9e85-e1a5f354911f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "5e721a35-fdea-421f-9e85-e1a5f354911f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "429fce12-0e6e-4617-a13e-99c231cf5302",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}