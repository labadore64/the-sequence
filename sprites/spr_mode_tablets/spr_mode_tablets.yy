{
    "id": "8e5da87f-05d6-45f4-85c4-0ff259b37989",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mode_tablets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dc1885b-5e52-4642-a7bf-61d750783a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e5da87f-05d6-45f4-85c4-0ff259b37989",
            "compositeImage": {
                "id": "f8e93524-1380-4ca6-9c24-5144b26e83bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dc1885b-5e52-4642-a7bf-61d750783a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c43c545-9cf3-4266-94c7-11f8d2aa7c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dc1885b-5e52-4642-a7bf-61d750783a5a",
                    "LayerId": "25ff6f2b-aee3-414b-9971-95363827cf90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "25ff6f2b-aee3-414b-9971-95363827cf90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e5da87f-05d6-45f4-85c4-0ff259b37989",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}