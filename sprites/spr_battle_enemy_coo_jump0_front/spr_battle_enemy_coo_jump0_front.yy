{
    "id": "c577c37e-9a20-410f-b56d-3bbf83838cb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 225,
    "bbox_left": 44,
    "bbox_right": 192,
    "bbox_top": 130,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd51795b-d8cf-443c-b554-45942d1f3cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c577c37e-9a20-410f-b56d-3bbf83838cb9",
            "compositeImage": {
                "id": "bda3f4b4-e5d0-4faa-8010-62e9cd9a58eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd51795b-d8cf-443c-b554-45942d1f3cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df4d5d8-1895-4534-8951-7e5675d1fe31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd51795b-d8cf-443c-b554-45942d1f3cb3",
                    "LayerId": "41f3fb15-0f1a-4d4f-b0e6-4964c31c6975"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "41f3fb15-0f1a-4d4f-b0e6-4964c31c6975",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c577c37e-9a20-410f-b56d-3bbf83838cb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}