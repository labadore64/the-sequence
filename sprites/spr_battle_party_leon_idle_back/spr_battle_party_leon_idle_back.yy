{
    "id": "98f92b12-c9db-4650-9b21-1faa73bf0698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 27,
    "bbox_right": 192,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "205a52bf-2a55-418d-92e9-197f0e95ea7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "0a4d6035-bec8-4455-be27-515020feb70b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "205a52bf-2a55-418d-92e9-197f0e95ea7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c7e334-3756-4d99-84bc-0c84b14cfc8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "205a52bf-2a55-418d-92e9-197f0e95ea7d",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "f7a670f7-ecbf-4576-9a7f-81e1b85e4e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "d08a40b5-af1a-4694-bfcb-56a68b0ddb08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7a670f7-ecbf-4576-9a7f-81e1b85e4e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6162da14-e2e2-412a-a7d6-053b46ba5083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7a670f7-ecbf-4576-9a7f-81e1b85e4e5d",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "fdb5391d-dee7-4307-8ef5-d87f8f6ab179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "e4ae5ecc-46e9-4916-abd9-72c9e2a0cf23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb5391d-dee7-4307-8ef5-d87f8f6ab179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be35152-f57e-4fd0-a4f1-e4aaab948c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb5391d-dee7-4307-8ef5-d87f8f6ab179",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "d42a3499-e8ad-473c-9b8e-0f5ac0247458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "9bb0c6ef-85c2-415a-ae65-efb9abc416da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d42a3499-e8ad-473c-9b8e-0f5ac0247458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75aeea5f-1a35-4f91-8bc8-6403745aa965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d42a3499-e8ad-473c-9b8e-0f5ac0247458",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "bca710d9-7842-4e0e-8597-bcaff7693b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "7d34156b-af3b-44ee-8449-6f318003f3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca710d9-7842-4e0e-8597-bcaff7693b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f09beb-0dfa-477e-b612-79229534cfc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca710d9-7842-4e0e-8597-bcaff7693b7e",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "15fd701e-46b1-4c58-8646-10537131d076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "939556f0-d12c-4914-8908-c252d43050b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15fd701e-46b1-4c58-8646-10537131d076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9017f6a7-71b5-4f4a-8d95-d1f081916c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15fd701e-46b1-4c58-8646-10537131d076",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "b08b7c2c-084b-4d75-b984-3ff9dd31b258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "185a55c4-16cf-4053-b838-ae69581c3da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08b7c2c-084b-4d75-b984-3ff9dd31b258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c51e05d-3b11-4264-8dff-0959e340cbe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08b7c2c-084b-4d75-b984-3ff9dd31b258",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "8066d0c0-3f66-449c-af3c-941f8bac7bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "1fb32c15-aee5-4dff-9f8c-7f6675c1b4ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8066d0c0-3f66-449c-af3c-941f8bac7bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0840744d-8800-4456-9dc3-2aeb97deb62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8066d0c0-3f66-449c-af3c-941f8bac7bc5",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "2f4a52ae-f6c3-40a3-b901-9f7a0262e01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "a1d84e04-93ab-46b1-abec-c41439cac72a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f4a52ae-f6c3-40a3-b901-9f7a0262e01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdab87f3-2ebb-43a6-a022-552bebfc4f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f4a52ae-f6c3-40a3-b901-9f7a0262e01c",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "9fe7946a-35b2-4481-9a66-fba4ef154ecb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "91828ef6-8e3b-476a-8be7-e49521269d39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe7946a-35b2-4481-9a66-fba4ef154ecb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ee10cf-fd88-4cd2-b805-3e0674ff0948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe7946a-35b2-4481-9a66-fba4ef154ecb",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "2230e40a-1fb5-43a5-9529-0cfa921399d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "b42cfa93-b82b-43e9-ba27-bd6afb42481b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2230e40a-1fb5-43a5-9529-0cfa921399d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846d31d3-1a2a-4fe4-b758-578cb51ae102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2230e40a-1fb5-43a5-9529-0cfa921399d0",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "9fa1ab57-b881-49c2-b44a-27fe9c488b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "4e8c39e1-d295-4803-96c8-3dc631eb8f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa1ab57-b881-49c2-b44a-27fe9c488b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dafd6ee-1bdc-417a-ade8-ad856d49467b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa1ab57-b881-49c2-b44a-27fe9c488b3e",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "ef8ed033-e5a7-4e4f-8357-d55cf8e230a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "b0dff545-4b71-4b51-8c5b-6091a19f7ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8ed033-e5a7-4e4f-8357-d55cf8e230a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6e67ab-fe22-4e09-b9a9-934038e103c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8ed033-e5a7-4e4f-8357-d55cf8e230a5",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "d335fbd1-c77e-47b4-b4bf-c0f3f47dd06c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "81f4f4fd-51d1-4881-b465-a9cda5e4fde6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d335fbd1-c77e-47b4-b4bf-c0f3f47dd06c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "242f264b-4422-4059-bcbf-acfa1ac92702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d335fbd1-c77e-47b4-b4bf-c0f3f47dd06c",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "0dea68be-514b-4bd9-8798-3459b27668bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "6797972c-0a37-46ba-8124-dc3e1c74f7b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dea68be-514b-4bd9-8798-3459b27668bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23befce6-3d59-495e-8f89-eb9abbaadce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dea68be-514b-4bd9-8798-3459b27668bb",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "fb31b72d-6f07-4cd1-b44c-3c2586ec116e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "3fbcf87c-3a4a-4a9a-9aa3-64f7fbe25f4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb31b72d-6f07-4cd1-b44c-3c2586ec116e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e98a5f-470b-4420-9b07-3018a058a6bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb31b72d-6f07-4cd1-b44c-3c2586ec116e",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "eb0b434f-aa02-42b7-89aa-f40792710763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "ec163279-d3b4-4b27-829d-cfee6eed2ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0b434f-aa02-42b7-89aa-f40792710763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd39a3b-d349-4578-af3e-f4c608214719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0b434f-aa02-42b7-89aa-f40792710763",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "f0df3a38-2e08-421e-9a1b-670551c6c053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "3cf32aea-1443-43d9-87a1-827cb98a7b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0df3a38-2e08-421e-9a1b-670551c6c053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7273ee3-a0ea-4f7d-994d-f77afcc8e9e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0df3a38-2e08-421e-9a1b-670551c6c053",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "4f10f173-6439-4786-a459-67a642b60be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "fb0d2dc9-ea8e-4dcb-9be3-309fb73404c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f10f173-6439-4786-a459-67a642b60be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca8ee55-8262-40eb-8ebe-60673f2fab0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f10f173-6439-4786-a459-67a642b60be6",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "06d93ffb-9422-431d-adea-66cb388ce523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "5de9dd67-136d-447f-80b8-3d9fd0a33106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d93ffb-9422-431d-adea-66cb388ce523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443e96e6-37ef-499d-b251-574bd112c577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d93ffb-9422-431d-adea-66cb388ce523",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "fe99bcc2-8f59-4993-a632-23b6d7fe6f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "f83b93f4-80fa-4b5d-ae69-29b6e7b2a4a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe99bcc2-8f59-4993-a632-23b6d7fe6f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abf06b58-963b-46ba-a8a5-6bc464651a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe99bcc2-8f59-4993-a632-23b6d7fe6f97",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "b96586ca-c712-487b-b3bd-0194a52e5f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "216d7b10-b529-4738-9a9e-590be8616270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96586ca-c712-487b-b3bd-0194a52e5f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3568efbf-3a46-4616-b877-32ca5d5f14da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96586ca-c712-487b-b3bd-0194a52e5f77",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "af808bef-f897-4d45-ad2a-f5f3c346f8a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "6c04e8a0-3ffc-47c6-a8d6-2ee1bf750801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af808bef-f897-4d45-ad2a-f5f3c346f8a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "717a041b-9299-4cd3-9d18-e74a6980a23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af808bef-f897-4d45-ad2a-f5f3c346f8a6",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "0aac8dd3-455f-499d-9acb-c26ac8789262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "00c5c2c4-5b6d-41ab-9577-aa88129c21cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aac8dd3-455f-499d-9acb-c26ac8789262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea6c9da-08c1-426e-8347-734382939260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aac8dd3-455f-499d-9acb-c26ac8789262",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "bbb4d488-10f6-44e9-9ab5-f0ad0c0584d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "6295b027-1a9b-4979-8fc6-031a54b918f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbb4d488-10f6-44e9-9ab5-f0ad0c0584d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5365b2bb-5c69-4216-b90c-24270af332da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbb4d488-10f6-44e9-9ab5-f0ad0c0584d0",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "050cac8f-1439-45c4-b8c2-12d9d90660ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "4d7495c7-6eb7-4ff7-beef-bf6f544f1b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "050cac8f-1439-45c4-b8c2-12d9d90660ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a071ef7a-0b45-44e6-8f55-9837293debd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "050cac8f-1439-45c4-b8c2-12d9d90660ff",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "8656e7b4-b296-4531-8814-56dfb8ca0b91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "095cbfa1-bdff-4059-a15c-1745d39251f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8656e7b4-b296-4531-8814-56dfb8ca0b91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf0fd8e-1cee-4b97-a3ee-279c2b932543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8656e7b4-b296-4531-8814-56dfb8ca0b91",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "60b1efcb-145a-4dd1-aa10-c273da58567f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "b63f549b-ff9d-4d37-81b5-242b98c472fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b1efcb-145a-4dd1-aa10-c273da58567f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad186b2e-89b0-454d-8f01-84ce11355a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b1efcb-145a-4dd1-aa10-c273da58567f",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "7941e442-1162-43a6-b935-786e586b2775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "cbe86b28-a0d0-49f7-8f7b-a267b0bbde24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7941e442-1162-43a6-b935-786e586b2775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2db618c-be50-41a4-a202-df461cf9d0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7941e442-1162-43a6-b935-786e586b2775",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        },
        {
            "id": "a097e1db-4b96-4d91-b3ba-3bff18683314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "compositeImage": {
                "id": "391c8502-8856-44c9-bbe4-85f93870cab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a097e1db-4b96-4d91-b3ba-3bff18683314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b0b38da-ab87-412d-8f6e-18d898e3b725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a097e1db-4b96-4d91-b3ba-3bff18683314",
                    "LayerId": "45a5b7be-4f05-419b-8b04-28dad6d7d14b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "45a5b7be-4f05-419b-8b04-28dad6d7d14b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98f92b12-c9db-4650-9b21-1faa73bf0698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}