{
    "id": "1ed2758f-f84a-490d-a849-ecb1b268d939",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fontBraille",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2f08e9a-dadd-45ca-8abd-9d3f01e901df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "84751971-c327-4020-aac5-29d4df5207d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2f08e9a-dadd-45ca-8abd-9d3f01e901df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bf0695b-20b4-41ff-b7ad-17a56864076d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2f08e9a-dadd-45ca-8abd-9d3f01e901df",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "f44a605e-e781-46b5-88bc-06cc7984d390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "24372545-af9e-4c3e-8dcb-fb442f4f191a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44a605e-e781-46b5-88bc-06cc7984d390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6992b909-6f93-42b5-b739-74d9f3a33815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44a605e-e781-46b5-88bc-06cc7984d390",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "e4337b1d-d982-4e6e-adaa-ced17ecf0e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "5809115e-8894-4b85-9b1e-369e82192e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4337b1d-d982-4e6e-adaa-ced17ecf0e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7624a92f-c2a3-43f6-b835-e704bc1a91c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4337b1d-d982-4e6e-adaa-ced17ecf0e7b",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "96bf27df-db6b-4501-ab5a-202aab6ec937",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "62d29474-5ea6-4bd3-b98d-5e205d2e9a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96bf27df-db6b-4501-ab5a-202aab6ec937",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be59883-0ec5-4cbb-a35b-1a66d972664b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96bf27df-db6b-4501-ab5a-202aab6ec937",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "2b2764cb-5fc8-4e70-ae6a-99b933142536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "17d009ab-7289-4c3d-9824-b3cee9d07140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b2764cb-5fc8-4e70-ae6a-99b933142536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7774cf6-0b8b-4043-b639-da8cba41a754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b2764cb-5fc8-4e70-ae6a-99b933142536",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "cb396d66-ea3d-4b3e-8ff2-9f9fa8dfe1a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "80234dc5-2bf5-4fe3-ade0-df81d3b37d69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb396d66-ea3d-4b3e-8ff2-9f9fa8dfe1a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d7635f-000e-4a37-8508-3b6f0c456bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb396d66-ea3d-4b3e-8ff2-9f9fa8dfe1a7",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "c68914b7-05a1-451a-b16d-9b35db343668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "4cf1dc44-a60d-4856-8555-432e86f06f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68914b7-05a1-451a-b16d-9b35db343668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a5e6a1-bcf7-4efb-a8e4-bc6f11ebbd02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68914b7-05a1-451a-b16d-9b35db343668",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "6a5b5345-945f-4dd5-a3b0-983068603475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "654e8dbb-d07b-42d6-a5a8-def16eaced4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5b5345-945f-4dd5-a3b0-983068603475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f00a38-b0e1-45b6-85ee-fb8b3ac1d4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5b5345-945f-4dd5-a3b0-983068603475",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "3e1c2ca2-59ae-454d-b4bd-e0b52b074f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "301a31ce-b06b-4821-9039-c6f15d3eb410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e1c2ca2-59ae-454d-b4bd-e0b52b074f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2f42bb4-3ee8-4369-b354-0e109a8ba134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e1c2ca2-59ae-454d-b4bd-e0b52b074f24",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "f914fb19-ca59-4607-b66a-ad676017729b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "bf259273-0fdd-476a-9ca9-627f275d00cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f914fb19-ca59-4607-b66a-ad676017729b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5c51ab-9943-474a-8e58-7bc8fc8c305c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f914fb19-ca59-4607-b66a-ad676017729b",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "59177fad-3c3d-4942-999d-9c78bcc156ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "c903fabd-6726-4c72-84da-54aee77fd842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59177fad-3c3d-4942-999d-9c78bcc156ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f63ff500-e4ae-492f-ad05-27cbdfc8f062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59177fad-3c3d-4942-999d-9c78bcc156ea",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "175d45f8-942b-4172-8525-81bd832ef037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "05914887-785c-44fa-b386-5ca5703dc01a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "175d45f8-942b-4172-8525-81bd832ef037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8db50f7-e702-46cb-a080-aba4c5e74c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "175d45f8-942b-4172-8525-81bd832ef037",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "62b6bc4f-7fa0-48a4-9fb4-279a6cbacb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "bbb9ea5f-6015-4c2d-aea3-3c17904e9fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b6bc4f-7fa0-48a4-9fb4-279a6cbacb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66468772-7511-49ac-a0aa-11bb44990a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b6bc4f-7fa0-48a4-9fb4-279a6cbacb3a",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "748b6009-362c-495f-ba0b-bdcf40c68828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "674e984e-0ece-474a-a313-4f2fc84ead93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748b6009-362c-495f-ba0b-bdcf40c68828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a092e513-94e9-44e9-b623-8af7209cd9cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748b6009-362c-495f-ba0b-bdcf40c68828",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "15336cbd-881f-431d-b1f7-87bc4d36eff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "da3b9cb2-eee8-487a-a6c5-0776878170c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15336cbd-881f-431d-b1f7-87bc4d36eff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b95b504c-228f-4203-959e-f12420e5bc6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15336cbd-881f-431d-b1f7-87bc4d36eff6",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "05265555-61bf-4fde-a7b2-747de7edef6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "b7041438-f560-4139-a880-74f939e94818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05265555-61bf-4fde-a7b2-747de7edef6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5b2b4e-137a-42d4-bcb4-8310ab334623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05265555-61bf-4fde-a7b2-747de7edef6a",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "139cf35f-60ad-40c6-966f-8eab85dc96e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "34f16488-c6bf-4261-8c9e-90f3e809db8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "139cf35f-60ad-40c6-966f-8eab85dc96e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ff9dc4-34c4-4f3b-bcc8-154c281a6505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "139cf35f-60ad-40c6-966f-8eab85dc96e6",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "b94cd22c-2e9d-4f31-88fc-20e75720fa97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "f2798eeb-dd90-4c7a-82ad-189bca690c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94cd22c-2e9d-4f31-88fc-20e75720fa97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a089ebd-dd2b-4cf6-95a1-cb59a0d02bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94cd22c-2e9d-4f31-88fc-20e75720fa97",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "0cbd7c9f-a14c-4b7b-8c73-fe09a96a88d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "55619b6c-9bd9-4a42-8df2-1fd078c1c84d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbd7c9f-a14c-4b7b-8c73-fe09a96a88d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88e18c1-289d-471d-bc0e-cd6a149c9a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbd7c9f-a14c-4b7b-8c73-fe09a96a88d7",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "9062877c-ddf8-47ab-ad91-f926cec8366c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "4d49a4fb-39ce-4b13-afa4-b69e02b11e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9062877c-ddf8-47ab-ad91-f926cec8366c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da951417-c2c6-4b7f-a64f-6584a2bd7943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9062877c-ddf8-47ab-ad91-f926cec8366c",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "d24aeaf4-43bc-4eec-b16e-498ddb3656ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "07173659-6eff-4b70-bbb0-a11d6754a53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d24aeaf4-43bc-4eec-b16e-498ddb3656ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "301c50b6-0b65-4571-a40f-ed03631f542a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d24aeaf4-43bc-4eec-b16e-498ddb3656ad",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "3bee93f4-b3cf-4676-a3cb-df72067584f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "d83ea18c-f7bf-4f6a-9e6d-6fea37b9dcd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bee93f4-b3cf-4676-a3cb-df72067584f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8c606a-b34b-4ad3-ac2d-28ca84795aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bee93f4-b3cf-4676-a3cb-df72067584f6",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "65f28c8a-a1f7-4a40-b75a-c0d587386cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "c97edcc7-3851-4f60-82c8-166f109e028a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f28c8a-a1f7-4a40-b75a-c0d587386cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeff00ac-32ca-4380-bc45-dad6f336bd9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f28c8a-a1f7-4a40-b75a-c0d587386cc5",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "26a61450-1315-45a4-a02a-7fac5d0de1fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "c2d06b88-4efe-4237-bbbf-d85a73089b6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a61450-1315-45a4-a02a-7fac5d0de1fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a567d5-ed77-467a-974d-1642e68b9dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a61450-1315-45a4-a02a-7fac5d0de1fc",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "b8199a61-b83c-4187-9282-6df60cbf319e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "a1366c31-f64f-42b9-ab47-775e25a90824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8199a61-b83c-4187-9282-6df60cbf319e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e8f5f4d-bf9a-464e-875b-131a66d6e458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8199a61-b83c-4187-9282-6df60cbf319e",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "12e093aa-9f79-4026-900c-05871ebd909d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "0c7588fb-9aa4-410b-bdf7-409e87195908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e093aa-9f79-4026-900c-05871ebd909d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40799298-5283-4926-98d8-4d73644deaf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e093aa-9f79-4026-900c-05871ebd909d",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        },
        {
            "id": "3d2b9e06-2d08-4b5c-a0ec-212de28b0ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "compositeImage": {
                "id": "be2651c1-5ca7-41c9-aaf7-471d6384d678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2b9e06-2d08-4b5c-a0ec-212de28b0ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e566b14-859f-409e-8ee8-c298e3e11fab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2b9e06-2d08-4b5c-a0ec-212de28b0ac0",
                    "LayerId": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "cf87e6fa-d79f-438f-8ceb-d4df41e53c5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ed2758f-f84a-490d-a849-ecb1b268d939",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "65af0988-cb88-42df-b650-971d782eab72",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}