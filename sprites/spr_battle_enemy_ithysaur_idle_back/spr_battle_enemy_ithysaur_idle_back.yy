{
    "id": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 2,
    "bbox_right": 242,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d25763f5-e624-499e-a350-d7fd9173d55d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "eb3a3a96-c5c8-4641-878a-a7a0109ac117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d25763f5-e624-499e-a350-d7fd9173d55d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd332d44-2172-4eaa-b47d-118c2fc2a474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d25763f5-e624-499e-a350-d7fd9173d55d",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "a6af1746-c17b-40f7-a96a-8be62e638710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "75605ae5-0946-4cc8-bf44-35f0d646d7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6af1746-c17b-40f7-a96a-8be62e638710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a666766-a7a7-461c-be9b-1e9d13cd3a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6af1746-c17b-40f7-a96a-8be62e638710",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "50e25a72-7170-4822-bc20-c041e0071894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "702001bf-5def-450c-a244-aa0d0ce8416b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50e25a72-7170-4822-bc20-c041e0071894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac90ceea-cff8-446b-809a-2f0034be4b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50e25a72-7170-4822-bc20-c041e0071894",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "d3ee2b49-56c0-439e-be43-c162577b36bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "02460574-b488-4a45-8a93-950a4aa94c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ee2b49-56c0-439e-be43-c162577b36bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5345a4-79be-47c9-a9b4-3ed8b9ce454b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ee2b49-56c0-439e-be43-c162577b36bc",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "f4a8ac09-69bd-4adb-a753-425ffff7e84d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "1f16fa9e-35f5-4ac1-928e-91818647fc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a8ac09-69bd-4adb-a753-425ffff7e84d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e69bca3a-2713-45f0-85af-b4ed1f7a7dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a8ac09-69bd-4adb-a753-425ffff7e84d",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "6fe3d619-7973-4af6-9d81-963ad7c24feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "db2f5cb8-3b6f-40eb-a1a4-e665a2e33d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe3d619-7973-4af6-9d81-963ad7c24feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfcccec-9f8e-448f-93df-b088daaa71ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe3d619-7973-4af6-9d81-963ad7c24feb",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "87336eb4-8de2-4e0e-8782-d106a4694af9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "17f08578-18ce-4b6a-93cd-e80a8163a97a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87336eb4-8de2-4e0e-8782-d106a4694af9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa12c3a-c514-4d19-80ae-bb9a54a64908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87336eb4-8de2-4e0e-8782-d106a4694af9",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "387ca5da-4160-4e57-991c-f404d20f1168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "eea3d268-4176-4083-b576-62748f15aa6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387ca5da-4160-4e57-991c-f404d20f1168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d473503-054f-4c5a-a79b-be4c0d5ece4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387ca5da-4160-4e57-991c-f404d20f1168",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "0b1ae634-5d6c-4420-b8da-e938d48eb469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "636cf8a2-418a-4259-bdf4-1a5b7de6e41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b1ae634-5d6c-4420-b8da-e938d48eb469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e30513-6f2b-460c-a2b7-27edc6ad9b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b1ae634-5d6c-4420-b8da-e938d48eb469",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "5744ad98-2083-4360-ace9-abf74a8579da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "abef0e85-b342-4545-a906-54b5929601fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5744ad98-2083-4360-ace9-abf74a8579da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702bbfd5-0a85-49cd-a497-3664fb8e732a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5744ad98-2083-4360-ace9-abf74a8579da",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "427e1930-bc19-4afc-b1aa-b4f734be0be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "07094316-0130-445b-8512-28f4ccff7f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427e1930-bc19-4afc-b1aa-b4f734be0be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bbf693b-5e0b-49f2-9755-93eb93d8e909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427e1930-bc19-4afc-b1aa-b4f734be0be3",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "fc74fa70-2801-4e35-bf58-b261ffa806d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "b9ef090c-ae4d-4ca4-ba2f-89997e4bd020",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc74fa70-2801-4e35-bf58-b261ffa806d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5276ada-0fa2-44e7-b2a8-bee0c6a72e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc74fa70-2801-4e35-bf58-b261ffa806d9",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "b1f1b710-b9dd-4bef-aa4b-45e19ab96b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "38f9d283-3783-4b24-a1b4-7ea2ff029669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f1b710-b9dd-4bef-aa4b-45e19ab96b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7d2ce1-6e36-4069-9308-0b78160280fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f1b710-b9dd-4bef-aa4b-45e19ab96b12",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "042fcf99-1088-49a0-b3ce-367809b21a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "80880bf1-8d68-41c2-9062-f79f2ccb88f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042fcf99-1088-49a0-b3ce-367809b21a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba8049d-1cbf-4034-b413-83a2c1109624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042fcf99-1088-49a0-b3ce-367809b21a1e",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "99e4fee8-9cc1-4a58-a20d-fe5eb5a5ec6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "4b59742c-c1ca-4e21-8f7f-8f1559f7018c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e4fee8-9cc1-4a58-a20d-fe5eb5a5ec6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70bfbaec-ed1b-4827-bc40-f51e001fb0e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e4fee8-9cc1-4a58-a20d-fe5eb5a5ec6e",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "a3cf8dc4-610c-429d-8cd7-18109b4ec775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "416918b0-5d02-451b-8760-e76fdc1ed538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3cf8dc4-610c-429d-8cd7-18109b4ec775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5398d0f-a797-4fe4-a43c-179f16c440da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3cf8dc4-610c-429d-8cd7-18109b4ec775",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "083fcfa7-60a3-4c03-88f3-70c3f86bba0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "20dcfab5-b068-4aab-8cde-92c9e59aa23e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083fcfa7-60a3-4c03-88f3-70c3f86bba0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a21b550-0f67-4611-bb4a-1c4c9151c13a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083fcfa7-60a3-4c03-88f3-70c3f86bba0a",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "d747d36e-a142-4bd9-a6f3-1b42910df200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "e777f3a7-7e7e-4c74-8265-5a381d1249a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d747d36e-a142-4bd9-a6f3-1b42910df200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d79f798-a30f-4e82-8a06-9309f1a07d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d747d36e-a142-4bd9-a6f3-1b42910df200",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "8894a85e-659f-41ad-b0dc-390d51a387f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "24e1d4c7-4d71-4f80-9bb9-d77b38699c13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8894a85e-659f-41ad-b0dc-390d51a387f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe8390c-6528-4b05-875c-793afa7bca95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8894a85e-659f-41ad-b0dc-390d51a387f0",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "d1b120ae-18d8-41af-b0d6-94e06771770e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "65a64400-b8d9-4b7b-8870-78004be608ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1b120ae-18d8-41af-b0d6-94e06771770e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc18225-0565-4110-8eca-2730535ebfae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1b120ae-18d8-41af-b0d6-94e06771770e",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "01a41df1-2c03-4832-a634-994174f4654b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "8e71805b-49b6-4ced-98ac-074ea51c7e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a41df1-2c03-4832-a634-994174f4654b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84668be5-b22f-4b5f-9a0c-50a8c00a95a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a41df1-2c03-4832-a634-994174f4654b",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "09796f7b-ca9c-4149-9f3b-85267c147f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "878157a7-cf29-4f05-99d5-5bab5ddb56a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09796f7b-ca9c-4149-9f3b-85267c147f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b55b35a-58ae-4e5a-944a-caec769143ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09796f7b-ca9c-4149-9f3b-85267c147f9b",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "b6c05bff-ed12-485c-8b09-d5aa575f96a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "cc08650e-d79b-4d82-ab4a-bfdfde08bf8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c05bff-ed12-485c-8b09-d5aa575f96a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1b6a8b9-bdb3-46b3-9158-b8080ba30258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c05bff-ed12-485c-8b09-d5aa575f96a4",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "264795ed-26e1-4717-98be-13485c83d391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "31e2a9fd-bbac-443e-97b2-f031e709689f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "264795ed-26e1-4717-98be-13485c83d391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8147a381-b2b9-44a3-92d5-2a91866735f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264795ed-26e1-4717-98be-13485c83d391",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "09d3cba1-dab5-426b-b1df-26873cc3b47f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "025271e6-816b-4f70-878a-4d658985e85c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09d3cba1-dab5-426b-b1df-26873cc3b47f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94623f8a-5e3e-4e17-b324-11e17751dcf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09d3cba1-dab5-426b-b1df-26873cc3b47f",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "3f8798c2-c943-40e7-babe-72bad3694072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "caea1a08-30e5-4a3e-9888-0a516c3a6a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f8798c2-c943-40e7-babe-72bad3694072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6ad2257-69d0-4538-93cd-c2ce7cb560aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f8798c2-c943-40e7-babe-72bad3694072",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "9fbed46f-b781-45c9-af25-86e8b18c12b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "55c712cd-3702-4ffa-9589-1bb657775146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbed46f-b781-45c9-af25-86e8b18c12b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9a3309-9290-4e1a-8b36-0182a73e3aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbed46f-b781-45c9-af25-86e8b18c12b6",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "108ef1c1-1de6-4d44-a950-b5742985ddb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "e9840225-f977-4f51-9ac0-7b8a909e7454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108ef1c1-1de6-4d44-a950-b5742985ddb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee9b237-fc91-48c7-9df8-2609fb090ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108ef1c1-1de6-4d44-a950-b5742985ddb5",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "462a9231-0c74-4d79-842e-2afe785075b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "e551f240-c938-4e64-b5ec-3c90d21edc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462a9231-0c74-4d79-842e-2afe785075b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6d9f428-d8fd-49d1-b653-8d73dd56bdc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462a9231-0c74-4d79-842e-2afe785075b2",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "69d67ac1-3388-4c3f-bb6f-98f200d79574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "b1cdc6f3-4e4e-4a31-ac22-7ef0ce5433a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d67ac1-3388-4c3f-bb6f-98f200d79574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a938b752-21f8-4831-aa2c-9cbc98d17aa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d67ac1-3388-4c3f-bb6f-98f200d79574",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "907d16d5-5ee8-4e68-a531-46ed6c0cd9da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "a49d504e-65ac-46c1-82af-e0979625e3c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907d16d5-5ee8-4e68-a531-46ed6c0cd9da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "696cf042-28f4-42c7-81bc-c5ba06929ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907d16d5-5ee8-4e68-a531-46ed6c0cd9da",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "208333eb-0171-410f-aa84-8ee018780082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "615f624f-697a-4b28-a32a-df785a79744c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208333eb-0171-410f-aa84-8ee018780082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a5df93-7886-4399-9cbe-16d03ceda587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208333eb-0171-410f-aa84-8ee018780082",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "e19cad38-3a63-447e-a258-b64d5b8fc86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "9f3e4a43-9ae2-4c72-a810-e6e4686468da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19cad38-3a63-447e-a258-b64d5b8fc86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0441a5a4-926d-4dbf-a311-bef43ff7824f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19cad38-3a63-447e-a258-b64d5b8fc86e",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "290e40b8-bf24-46b6-bd29-26ff44867497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "c429ec9b-28cf-4cda-af90-5a70b652d0f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290e40b8-bf24-46b6-bd29-26ff44867497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9b4ec2-79b1-429c-b843-f7c1dc54f48c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290e40b8-bf24-46b6-bd29-26ff44867497",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "b25c1545-2d3c-4920-ae7d-9aa1f721dce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "0ea91e9f-4a3e-4739-9a99-dc584202829c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25c1545-2d3c-4920-ae7d-9aa1f721dce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6844066c-3001-43a6-8f0a-78a03fbac1be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25c1545-2d3c-4920-ae7d-9aa1f721dce6",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "077bb7f7-f45a-4b9a-9cf4-c6ebf87ba000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "ce238f65-f781-4ea5-a2f9-4ddc735a6b42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "077bb7f7-f45a-4b9a-9cf4-c6ebf87ba000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f699563c-47b0-44f0-9714-5dfb42620284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "077bb7f7-f45a-4b9a-9cf4-c6ebf87ba000",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "5c26c64b-0016-4a06-8945-ebd2868ffb2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "450aeef1-6e5d-4835-8a1c-2b3b362991b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c26c64b-0016-4a06-8945-ebd2868ffb2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a1916e-cb41-46ce-a6b1-938eed9065d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c26c64b-0016-4a06-8945-ebd2868ffb2f",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "25af202b-bb45-48dc-9dfc-f966d7717db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "92da0a73-3eba-4fef-b130-c31320347e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25af202b-bb45-48dc-9dfc-f966d7717db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d61fb32-713e-4c4d-ad63-aeafb1507202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25af202b-bb45-48dc-9dfc-f966d7717db8",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "b174e23e-381f-4eb1-87d4-ed59806780b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "86b621e6-4b98-4752-81e6-a995bfd9db15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b174e23e-381f-4eb1-87d4-ed59806780b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135edd27-b66b-452d-8e4d-f43e7c712610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b174e23e-381f-4eb1-87d4-ed59806780b8",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "6d6e2b1f-d2bd-44cf-88d2-6037d4482120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "90220e19-3fdc-4016-a36b-98cd9b92a0a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6e2b1f-d2bd-44cf-88d2-6037d4482120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6d1e5d2-07c0-484e-bb88-00a847425601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6e2b1f-d2bd-44cf-88d2-6037d4482120",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "7d6ab28a-4cd0-482f-96ae-ad87e859aa82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "e0df2cc6-caa6-4a3d-a4e4-b903c267ae2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d6ab28a-4cd0-482f-96ae-ad87e859aa82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b07c351-0ff6-451d-b179-324a85876d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d6ab28a-4cd0-482f-96ae-ad87e859aa82",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "f64f9b83-6563-41e7-8afd-74e137d08ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "15ab62b1-5712-4d80-ba50-5918372cd8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64f9b83-6563-41e7-8afd-74e137d08ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d5cbc9-2b60-4c43-a9de-b5436f43515c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64f9b83-6563-41e7-8afd-74e137d08ed2",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "790b5f67-1bc3-4917-95b8-83611650cc63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "573dd024-0237-4da9-8df4-39ced0ab432d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "790b5f67-1bc3-4917-95b8-83611650cc63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d54c8d8-c2e9-4d2b-8ceb-05454f19b6bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790b5f67-1bc3-4917-95b8-83611650cc63",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "91949efd-d25c-42ba-bf26-d84e1d41a6d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "87d31e21-0d91-4959-80cb-b1954bdaede1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91949efd-d25c-42ba-bf26-d84e1d41a6d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79db6a5f-d185-4d31-9b23-3357199ae803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91949efd-d25c-42ba-bf26-d84e1d41a6d2",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        },
        {
            "id": "c13b2a46-5240-4d12-9e67-1c9f71986ea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "compositeImage": {
                "id": "51f24f2d-3a6f-4ebd-bc46-abc5b6355847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13b2a46-5240-4d12-9e67-1c9f71986ea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d23f3e-a71e-4852-b186-e615b97816d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13b2a46-5240-4d12-9e67-1c9f71986ea5",
                    "LayerId": "77c8556f-ed8e-4e19-a47d-585a452a0182"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "77c8556f-ed8e-4e19-a47d-585a452a0182",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6e0a7e9-2523-49ee-bdf1-0a7cf83dd953",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}