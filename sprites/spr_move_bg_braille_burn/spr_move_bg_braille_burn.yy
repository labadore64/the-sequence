{
    "id": "099e8766-bc47-48ce-9644-68e35bbcf0df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_braille_burn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "458ec4eb-03ea-432a-bf83-6243b8cdd469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "099e8766-bc47-48ce-9644-68e35bbcf0df",
            "compositeImage": {
                "id": "63bb1e6c-6bea-4a43-bd81-8d0558ec2e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "458ec4eb-03ea-432a-bf83-6243b8cdd469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e29bdf-9e6c-47e8-af03-c47b42b058ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "458ec4eb-03ea-432a-bf83-6243b8cdd469",
                    "LayerId": "2fdb99d3-2b1e-4127-bdb6-35a592ca9afa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "2fdb99d3-2b1e-4127-bdb6-35a592ca9afa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "099e8766-bc47-48ce-9644-68e35bbcf0df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}