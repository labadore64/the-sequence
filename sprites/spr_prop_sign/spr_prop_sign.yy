{
    "id": "3a58bce6-8e41-44e9-8bc0-89a783465505",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 16,
    "bbox_right": 105,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b5d5df4-be97-4858-b984-f088e79af190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a58bce6-8e41-44e9-8bc0-89a783465505",
            "compositeImage": {
                "id": "d70f1266-2975-4f50-8cd0-60fcb2d1be19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b5d5df4-be97-4858-b984-f088e79af190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b3b8ac-ccd9-476d-bf8f-178cd1ecec9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b5d5df4-be97-4858-b984-f088e79af190",
                    "LayerId": "9b9368fe-ee25-4062-8049-3bac73e578ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9b9368fe-ee25-4062-8049-3bac73e578ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a58bce6-8e41-44e9-8bc0-89a783465505",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}