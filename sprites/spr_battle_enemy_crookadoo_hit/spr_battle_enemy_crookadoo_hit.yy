{
    "id": "4c902c0b-83a5-46ed-84cf-8edc21925efc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_crookadoo_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 45,
    "bbox_right": 220,
    "bbox_top": 61,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efabdd92-dfc3-4c21-a99a-41a392f2da00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c902c0b-83a5-46ed-84cf-8edc21925efc",
            "compositeImage": {
                "id": "742ca09f-047f-4be7-bbfa-c5b43f6c89ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efabdd92-dfc3-4c21-a99a-41a392f2da00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "398987f0-d24b-4ede-b863-1de26781dc0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efabdd92-dfc3-4c21-a99a-41a392f2da00",
                    "LayerId": "deb39e02-3d22-413c-804b-f9d6a07dc9ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "deb39e02-3d22-413c-804b-f9d6a07dc9ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c902c0b-83a5-46ed-84cf-8edc21925efc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "580e916e-bc19-4b49-aabe-9fd387268b2e",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}