{
    "id": "af39b281-9f0d-4f0b-b0f9-a23866b3fd96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_dipper_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 66,
    "bbox_right": 185,
    "bbox_top": 82,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a3888b9-7fce-431f-8c93-2768613ff65b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af39b281-9f0d-4f0b-b0f9-a23866b3fd96",
            "compositeImage": {
                "id": "e29ed49c-f03b-4f4c-82ea-8feffb1381ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3888b9-7fce-431f-8c93-2768613ff65b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fefcf07e-0e44-4b7d-ac8e-0fd0ea347c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3888b9-7fce-431f-8c93-2768613ff65b",
                    "LayerId": "278b17cd-2d4c-49be-ba5c-55ee5c7bbc07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "278b17cd-2d4c-49be-ba5c-55ee5c7bbc07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af39b281-9f0d-4f0b-b0f9-a23866b3fd96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1e479664-c8d0-4e3e-9d43-b1d1b8e5d215",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}