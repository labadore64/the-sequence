{
    "id": "36b49516-b9f5-49d7-a9b0-57794a52cfaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_attack_back2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 61,
    "bbox_right": 221,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d7064be-f5a0-4eca-8e91-82c295fe4a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b49516-b9f5-49d7-a9b0-57794a52cfaa",
            "compositeImage": {
                "id": "cbab964d-12b1-4499-ad32-8cfeb0a02889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7064be-f5a0-4eca-8e91-82c295fe4a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6bff68-be83-4323-ba09-25f30c73a5c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7064be-f5a0-4eca-8e91-82c295fe4a14",
                    "LayerId": "65fa46ed-b2bf-47e4-866d-8e68cbd82b63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "65fa46ed-b2bf-47e4-866d-8e68cbd82b63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36b49516-b9f5-49d7-a9b0-57794a52cfaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}