{
    "id": "42a3b1fe-db93-4ce3-89cc-af099c0494bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cellphone_comment",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "660a8ebc-557a-4d04-bc9f-fc8f413142e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42a3b1fe-db93-4ce3-89cc-af099c0494bc",
            "compositeImage": {
                "id": "112136c5-9b2a-4124-95ab-60aa06bb7c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660a8ebc-557a-4d04-bc9f-fc8f413142e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0d4c08-ba7d-47e6-9081-597973620bad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660a8ebc-557a-4d04-bc9f-fc8f413142e2",
                    "LayerId": "de8e724f-5433-45b4-b8ef-fa3fbfb6147c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "de8e724f-5433-45b4-b8ef-fa3fbfb6147c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42a3b1fe-db93-4ce3-89cc-af099c0494bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}