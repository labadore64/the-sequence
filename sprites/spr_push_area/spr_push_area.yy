{
    "id": "a7262d81-862f-402b-9050-48faa6e998cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_push_area",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1df50249-8569-45c0-8a6b-d149e7242550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7262d81-862f-402b-9050-48faa6e998cc",
            "compositeImage": {
                "id": "53f72e7f-9ac7-4447-9447-ff01547dab15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df50249-8569-45c0-8a6b-d149e7242550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69cd56b-3755-4947-b9a2-a1dce8103459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df50249-8569-45c0-8a6b-d149e7242550",
                    "LayerId": "955b37d4-93ff-4000-9692-d6de5f8fe0c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "955b37d4-93ff-4000-9692-d6de5f8fe0c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7262d81-862f-402b-9050-48faa6e998cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 28,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}