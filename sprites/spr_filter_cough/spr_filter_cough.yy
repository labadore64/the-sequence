{
    "id": "3319d3cd-0b91-4e99-b537-7ccfb511cef9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_cough",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 145,
    "bbox_right": 199,
    "bbox_top": 83,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5ad1dce-0af4-4bc2-abc4-056c4cab71c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3319d3cd-0b91-4e99-b537-7ccfb511cef9",
            "compositeImage": {
                "id": "908dfa31-8b13-491b-9fa6-4a15e6e2c08a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5ad1dce-0af4-4bc2-abc4-056c4cab71c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f01b8f21-848f-4ddc-b0a1-518b349fee6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5ad1dce-0af4-4bc2-abc4-056c4cab71c4",
                    "LayerId": "e9b29308-3ff4-4eb3-ac54-2835b18755bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e9b29308-3ff4-4eb3-ac54-2835b18755bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3319d3cd-0b91-4e99-b537-7ccfb511cef9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}