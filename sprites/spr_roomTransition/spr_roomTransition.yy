{
    "id": "65b2907f-c371-4ea3-9bdd-d3887c7f45f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_roomTransition",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59360fa7-62df-40aa-a07c-a275da0f03e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65b2907f-c371-4ea3-9bdd-d3887c7f45f2",
            "compositeImage": {
                "id": "301352fe-a539-429e-83b7-bee73d73baec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59360fa7-62df-40aa-a07c-a275da0f03e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a17181cc-45e7-418b-b526-cde76ce15258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59360fa7-62df-40aa-a07c-a275da0f03e6",
                    "LayerId": "a9987680-c5f7-4d7d-b48d-bfafca36a9a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "a9987680-c5f7-4d7d-b48d-bfafca36a9a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65b2907f-c371-4ea3-9bdd-d3887c7f45f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}