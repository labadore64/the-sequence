{
    "id": "e9dc4b18-618d-495a-ba7b-9912eee26a86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_attack_front2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 22,
    "bbox_right": 224,
    "bbox_top": 92,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2dd1001-5507-4cb6-a5f5-c0db4672af8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9dc4b18-618d-495a-ba7b-9912eee26a86",
            "compositeImage": {
                "id": "18987b85-4c7d-4fa5-a9a8-7ef03daf99ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2dd1001-5507-4cb6-a5f5-c0db4672af8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9cd363-11b1-4a71-a798-883fa6a68d3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2dd1001-5507-4cb6-a5f5-c0db4672af8e",
                    "LayerId": "0e41b5fd-ab85-4f4f-9a8f-df8d6aa5500c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0e41b5fd-ab85-4f4f-9a8f-df8d6aa5500c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9dc4b18-618d-495a-ba7b-9912eee26a86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}