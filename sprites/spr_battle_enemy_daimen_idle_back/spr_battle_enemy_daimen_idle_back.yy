{
    "id": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_daimen_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 14,
    "bbox_right": 243,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed99abe6-41ed-4c75-8366-06f94abc5f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "9e3872c5-6e47-44eb-9d06-6619864b8d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed99abe6-41ed-4c75-8366-06f94abc5f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b3424d-e3dd-4c83-8c31-719ea1f85e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed99abe6-41ed-4c75-8366-06f94abc5f6b",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "729132a3-c206-4785-aac7-5a7c3223ee1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "2f21faf0-a67b-4a42-9a8c-a14fc0119baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "729132a3-c206-4785-aac7-5a7c3223ee1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32fc81af-422b-4609-9671-91f7ba466ab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "729132a3-c206-4785-aac7-5a7c3223ee1e",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "d7f92f63-e442-4360-80a8-197614719167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "df1e6938-924e-4653-8f78-db9ada623cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f92f63-e442-4360-80a8-197614719167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4709e4b8-38a4-4c46-bf4d-d2c9ac9231a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f92f63-e442-4360-80a8-197614719167",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "56b9e12e-e2d3-4544-b6ec-3665a6e0a94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "1bb48dab-e3f4-4a24-9b20-89f866c99aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56b9e12e-e2d3-4544-b6ec-3665a6e0a94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fadfe44d-ab27-4635-8394-8fa82cd900e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56b9e12e-e2d3-4544-b6ec-3665a6e0a94f",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "48100608-67dc-4ee4-8364-858bf4567779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "5cb73644-ed47-41e1-82ac-3c7c4147ae70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48100608-67dc-4ee4-8364-858bf4567779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e64d5a9-d55f-4924-ab85-f167a3c8cc20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48100608-67dc-4ee4-8364-858bf4567779",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "f403ff83-0a60-4b48-be7d-95ac7dba2718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "67782514-8444-4dbe-bf82-57b4f2183226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f403ff83-0a60-4b48-be7d-95ac7dba2718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257bbe49-3d32-4d81-9fc5-17f60ce841ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f403ff83-0a60-4b48-be7d-95ac7dba2718",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "d583f777-41ca-4409-972b-faf87ae47d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "88a1b4c6-b2c2-4b65-be21-6d41898d54c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d583f777-41ca-4409-972b-faf87ae47d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fecb797-6a2a-40f2-a2bd-0023479a702e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d583f777-41ca-4409-972b-faf87ae47d96",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "ece51223-ed1b-4dcb-914b-1d66d4d91ac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "cd1e7844-b401-440e-8a47-e93ee5c08770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece51223-ed1b-4dcb-914b-1d66d4d91ac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849b4571-9750-4c0c-a61c-96b28aef2464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece51223-ed1b-4dcb-914b-1d66d4d91ac7",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "2a37b553-8166-447d-9f55-d5f05df812fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "060a522c-5116-4c79-b959-3ff590093c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a37b553-8166-447d-9f55-d5f05df812fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "654577db-eff5-41b4-baee-451024798a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a37b553-8166-447d-9f55-d5f05df812fc",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "ea027b87-b64a-44d1-9dd1-ebd7933524e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "29351449-99d4-4f95-8812-4fa0dad0d4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea027b87-b64a-44d1-9dd1-ebd7933524e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c686a7d-9211-4c48-8a0d-4c71fdbe80ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea027b87-b64a-44d1-9dd1-ebd7933524e0",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "fb93e1e4-8acf-4bb9-88e0-8684906a9775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "20a6a3ff-afa1-4cf2-ba7b-1be7100e0686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb93e1e4-8acf-4bb9-88e0-8684906a9775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24194b8-3704-4580-8e20-fd4b6641ad7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb93e1e4-8acf-4bb9-88e0-8684906a9775",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "f743a9e8-803a-4e25-9d93-3d912249b9fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "dcc8cddf-0407-413e-8130-1918d86a92f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f743a9e8-803a-4e25-9d93-3d912249b9fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df79f80c-dfe5-4f78-9a55-2d0d2c5cfb29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f743a9e8-803a-4e25-9d93-3d912249b9fc",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "052829ec-5b06-4b1b-a80a-5cde090d96df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "61c219ce-3330-4a58-a54e-f2e7ca1c8e78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "052829ec-5b06-4b1b-a80a-5cde090d96df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f670f447-a4a3-408c-871f-14ac27b4ea7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052829ec-5b06-4b1b-a80a-5cde090d96df",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "95ff13ab-1950-407f-98f5-df3d60f5a1bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "11ed592f-e2b9-43ad-9179-d174c5e58605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ff13ab-1950-407f-98f5-df3d60f5a1bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a908c9e3-4a67-4ae0-91d7-dc61e39463ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ff13ab-1950-407f-98f5-df3d60f5a1bf",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "15ef938e-78a2-49f8-83ac-1e0baa84754f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "265f64af-73df-4f02-8237-b98fd6b9548e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ef938e-78a2-49f8-83ac-1e0baa84754f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2a8586-7305-41f7-92ed-14ff61d99d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ef938e-78a2-49f8-83ac-1e0baa84754f",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "945b0b4d-3d2a-44b6-941e-11edcd571bb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "8293e5ef-fff4-41bc-8f45-ddf3a9f76d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945b0b4d-3d2a-44b6-941e-11edcd571bb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412df281-3e8f-4583-bb9d-5f5d97cd8f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945b0b4d-3d2a-44b6-941e-11edcd571bb9",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "9893f269-d7e2-4d91-a536-ae64ac9d47c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "05b8a10d-23d1-4612-86cf-3021e33b1b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9893f269-d7e2-4d91-a536-ae64ac9d47c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdac117e-199f-41a2-aae5-48e11c41c31d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9893f269-d7e2-4d91-a536-ae64ac9d47c4",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "96b21165-4dcd-4df4-8ebd-6aa15e71ae57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "9c026abf-4290-464a-b91a-5ecfdcdbc593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b21165-4dcd-4df4-8ebd-6aa15e71ae57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65012b7b-1c2d-4e50-8aa5-a83944d83334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b21165-4dcd-4df4-8ebd-6aa15e71ae57",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "a649119e-f22d-43a0-84c1-d50701bbc76e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "f6b28f9a-32b6-4f67-8a97-038a1b449bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a649119e-f22d-43a0-84c1-d50701bbc76e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d2b041-23c1-4267-a1ad-06931ee662d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a649119e-f22d-43a0-84c1-d50701bbc76e",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        },
        {
            "id": "c216b6a9-9586-41c3-8a39-c97e1b81fe89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "compositeImage": {
                "id": "a6dd92c7-c220-43f7-9527-b85fd659520f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c216b6a9-9586-41c3-8a39-c97e1b81fe89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dda8b40d-42a4-4ad6-babb-f9118036a3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c216b6a9-9586-41c3-8a39-c97e1b81fe89",
                    "LayerId": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "23ce34ae-50d4-4f6a-ad0f-90d056e0d28c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89bbc69c-1844-4ecd-891c-21f61f6e5698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4038d625-6d07-42e2-825f-bcc4b31256a3",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}