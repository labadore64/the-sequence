{
    "id": "c76e9616-3e50-4345-8e0b-30903fe07d4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_babayaga_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 14,
    "bbox_right": 226,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ef167ce-c407-4da1-8a38-c53b8787334e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c76e9616-3e50-4345-8e0b-30903fe07d4c",
            "compositeImage": {
                "id": "ca140b6b-706d-48cc-8481-624d8b7ece41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef167ce-c407-4da1-8a38-c53b8787334e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "139b52ca-074f-482f-97e5-31ac2e6d32d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef167ce-c407-4da1-8a38-c53b8787334e",
                    "LayerId": "3d8e1ba5-3808-44db-9f3e-8eef80fc8b4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "3d8e1ba5-3808-44db-9f3e-8eef80fc8b4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c76e9616-3e50-4345-8e0b-30903fe07d4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "660d4a34-eb5a-49a4-93ee-8d23a3172db4",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}