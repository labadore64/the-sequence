{
    "id": "c6b38051-6b3d-407c-8116-93e4bd5dfe99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "146a2bed-1f27-4fa6-bc21-6ed265bc6b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b38051-6b3d-407c-8116-93e4bd5dfe99",
            "compositeImage": {
                "id": "40d2ce55-55b4-453d-b213-b086b8408f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "146a2bed-1f27-4fa6-bc21-6ed265bc6b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee997d9-b338-4d92-866c-13d292bb48a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "146a2bed-1f27-4fa6-bc21-6ed265bc6b64",
                    "LayerId": "f46e35e1-fc7d-4bf8-a5ce-a53fa48e11e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "f46e35e1-fc7d-4bf8-a5ce-a53fa48e11e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6b38051-6b3d-407c-8116-93e4bd5dfe99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}