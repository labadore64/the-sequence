{
    "id": "8657e3d7-5b95-417e-9857-00a4fcf9bf7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_tree1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52d6b7f8-588b-4cbe-9346-f2638db6a003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8657e3d7-5b95-417e-9857-00a4fcf9bf7c",
            "compositeImage": {
                "id": "8dce7fcd-3de8-45e6-87e8-52687027eda5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d6b7f8-588b-4cbe-9346-f2638db6a003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef70b9e4-225a-47d1-bf69-4193835bbfa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d6b7f8-588b-4cbe-9346-f2638db6a003",
                    "LayerId": "942a773b-e5bf-4aab-a461-ac12933a1a14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "942a773b-e5bf-4aab-a461-ac12933a1a14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8657e3d7-5b95-417e-9857-00a4fcf9bf7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}