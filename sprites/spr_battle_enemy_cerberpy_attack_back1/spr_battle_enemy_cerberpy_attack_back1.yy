{
    "id": "1c43ebe0-4ab2-4bc8-8eee-c209df3856b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_cerberpy_attack_back1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 53,
    "bbox_right": 179,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "812bb922-9554-4f4d-9aa5-1511f1f00f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c43ebe0-4ab2-4bc8-8eee-c209df3856b4",
            "compositeImage": {
                "id": "9c2a4167-5888-4794-8bcf-47b904a0aa0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "812bb922-9554-4f4d-9aa5-1511f1f00f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88fac5e4-6c78-4ecf-820d-bfa4f3c34077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "812bb922-9554-4f4d-9aa5-1511f1f00f28",
                    "LayerId": "37efecb4-21be-435e-8355-8bc09306383f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "37efecb4-21be-435e-8355-8bc09306383f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c43ebe0-4ab2-4bc8-8eee-c209df3856b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}