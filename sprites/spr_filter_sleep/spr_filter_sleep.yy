{
    "id": "6075683c-be29-4e47-aa6c-76240205b033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_sleep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 129,
    "bbox_left": 92,
    "bbox_right": 243,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4f70271-c7e3-43e9-b4a7-ff85b14a1841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6075683c-be29-4e47-aa6c-76240205b033",
            "compositeImage": {
                "id": "c57fd1a7-56d9-49d8-bb82-a74b51d717e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f70271-c7e3-43e9-b4a7-ff85b14a1841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb13f409-85cc-4bd2-8bb6-5646e760f8a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f70271-c7e3-43e9-b4a7-ff85b14a1841",
                    "LayerId": "d3930676-b90c-4936-b5c8-46408efccafa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d3930676-b90c-4936-b5c8-46408efccafa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6075683c-be29-4e47-aa6c-76240205b033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 160
}