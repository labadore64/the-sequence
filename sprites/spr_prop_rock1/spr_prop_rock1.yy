{
    "id": "43ae108e-0cba-499f-a002-de5b194a36e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_rock1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 23,
    "bbox_right": 109,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "417f2249-e40f-4949-ad8e-77603f2bba61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43ae108e-0cba-499f-a002-de5b194a36e1",
            "compositeImage": {
                "id": "933b39ae-c52c-4c60-9bb5-fe8200b5adbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417f2249-e40f-4949-ad8e-77603f2bba61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e104d691-946c-4a95-a7cb-570e4a8fa939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417f2249-e40f-4949-ad8e-77603f2bba61",
                    "LayerId": "8f9a493f-76a1-4d08-93b2-3bbe69aafeed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8f9a493f-76a1-4d08-93b2-3bbe69aafeed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43ae108e-0cba-499f-a002-de5b194a36e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}