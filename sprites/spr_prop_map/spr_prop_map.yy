{
    "id": "166aa0da-1ac0-4a15-8a2e-03ba9fcc036d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 6,
    "bbox_right": 117,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfb5d3aa-8387-43e7-8157-000989921aa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "166aa0da-1ac0-4a15-8a2e-03ba9fcc036d",
            "compositeImage": {
                "id": "28a4321e-8124-4498-9523-f91cd63ae1f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfb5d3aa-8387-43e7-8157-000989921aa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c463a4b8-3570-4cdf-b9b1-5c746d91d373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfb5d3aa-8387-43e7-8157-000989921aa0",
                    "LayerId": "24a476b4-bba7-483a-8737-799882a87cef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "24a476b4-bba7-483a-8737-799882a87cef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "166aa0da-1ac0-4a15-8a2e-03ba9fcc036d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}