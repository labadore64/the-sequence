{
    "id": "01cd4087-628b-4d23-8189-c19deacff588",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_cast1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 2,
    "bbox_right": 216,
    "bbox_top": 74,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d795627-4c47-4871-803c-5496264805d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01cd4087-628b-4d23-8189-c19deacff588",
            "compositeImage": {
                "id": "1333a6e6-dde1-4997-82b5-0a871641dd09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d795627-4c47-4871-803c-5496264805d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf7dd63-8289-4eec-adf1-ae9e0adc3e53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d795627-4c47-4871-803c-5496264805d3",
                    "LayerId": "00f2a9bb-0c5e-4884-8522-0b40fc62d642"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "00f2a9bb-0c5e-4884-8522-0b40fc62d642",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01cd4087-628b-4d23-8189-c19deacff588",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}