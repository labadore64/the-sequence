{
    "id": "2d44acc7-a31e-42e0-986a-545db73c5616",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_tree3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27d89aa9-82b1-40a9-8263-851dda092aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d44acc7-a31e-42e0-986a-545db73c5616",
            "compositeImage": {
                "id": "85916137-41dd-4c0d-8cc8-5bf9a9b38844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d89aa9-82b1-40a9-8263-851dda092aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e27a94-a572-4066-92d5-6426948a7f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d89aa9-82b1-40a9-8263-851dda092aeb",
                    "LayerId": "29f9ac8a-2446-48c9-b2a2-0278be9f9cfe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "29f9ac8a-2446-48c9-b2a2-0278be9f9cfe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d44acc7-a31e-42e0-986a-545db73c5616",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}