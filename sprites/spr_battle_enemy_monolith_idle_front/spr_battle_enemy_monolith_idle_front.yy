{
    "id": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_monolith_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 51,
    "bbox_right": 204,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d40f03c-8d5d-4fd1-ab82-b31e19cf8990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "bdacf6b0-0923-48b3-bdff-27df0217dd7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d40f03c-8d5d-4fd1-ab82-b31e19cf8990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c054a4a-1d21-48ad-b537-52bf4cd4e84f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d40f03c-8d5d-4fd1-ab82-b31e19cf8990",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "bff9c78f-d775-426e-a089-807cadbc242c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "b304297c-4e4a-4732-ac7a-41a6444df838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bff9c78f-d775-426e-a089-807cadbc242c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a167e5-e413-4c83-bbf1-f58f4d2a1054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bff9c78f-d775-426e-a089-807cadbc242c",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "c6043ccc-7ee5-4ba5-af03-521d96c5f044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "d0318e14-63bc-4a8c-b953-31d2df3d180a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6043ccc-7ee5-4ba5-af03-521d96c5f044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbce7b76-9c04-4b5f-aef7-f846471891ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6043ccc-7ee5-4ba5-af03-521d96c5f044",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "fc2209fe-820c-4e47-ab4a-cf828e66869e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "db3dcbff-a966-42d5-9beb-8c594826b67c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2209fe-820c-4e47-ab4a-cf828e66869e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "755a513f-c401-426a-9ce7-90c95a4fad09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2209fe-820c-4e47-ab4a-cf828e66869e",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "26225be3-7096-4189-8e2b-88539c61c50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "084245d4-a2ac-4bd5-8d19-03812ae02c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26225be3-7096-4189-8e2b-88539c61c50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35492b1-aaa8-4bcb-97ae-062108d8b22e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26225be3-7096-4189-8e2b-88539c61c50f",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "4db583a6-17df-4419-86f3-be0956d790c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "ded21790-57e5-4412-a859-7333845923e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db583a6-17df-4419-86f3-be0956d790c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83cf1f3-702e-49c4-93eb-18fd7cee07bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db583a6-17df-4419-86f3-be0956d790c5",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "8925b854-9f3c-448b-9cf7-0c76b10367a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "17ac5d17-99df-4e37-86c5-67bdb59e32f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8925b854-9f3c-448b-9cf7-0c76b10367a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b7bd9e-9e42-476a-a2fa-eb3014520c00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8925b854-9f3c-448b-9cf7-0c76b10367a1",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "6de24afa-7879-4293-8df1-740ad694f6aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "ee1066fa-7412-4ce5-a0d9-0eb530565f66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de24afa-7879-4293-8df1-740ad694f6aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b4ef87-f3fa-45a9-add0-6604ad0d61af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de24afa-7879-4293-8df1-740ad694f6aa",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "0585876c-d49d-4720-99ea-382ee512312e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "48dbda0a-bc64-41c5-9c30-53880caa4f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0585876c-d49d-4720-99ea-382ee512312e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0423d00-d589-4d96-be90-f253de873f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0585876c-d49d-4720-99ea-382ee512312e",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "8aa96b74-c3fd-43bb-ac11-f6f3cf98c882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "f3a0bd63-69f3-4f40-9d02-564e74e8af02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa96b74-c3fd-43bb-ac11-f6f3cf98c882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb0c3c82-5470-4b1f-b529-e1962eda3bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa96b74-c3fd-43bb-ac11-f6f3cf98c882",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "be960e42-f23e-4f22-b49b-9e6025232c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "6e502cae-5859-43ca-989c-44bbad285c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be960e42-f23e-4f22-b49b-9e6025232c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a2fc4f-7987-4b3e-b383-bf5a4fb5d8b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be960e42-f23e-4f22-b49b-9e6025232c35",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "b886a5d1-4b5f-4bbf-99e3-56420fd67e3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "0fbb7e51-9701-482b-96e8-bd3873231aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b886a5d1-4b5f-4bbf-99e3-56420fd67e3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f02811-5b22-4025-9b62-392f0b2e1c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b886a5d1-4b5f-4bbf-99e3-56420fd67e3b",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "2bf79479-7032-4d4e-b42d-c9ef547d83cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "7f23bb37-703c-4901-8033-a407c99a198a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf79479-7032-4d4e-b42d-c9ef547d83cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e5a205-eb9d-49dd-a698-905c7830cea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf79479-7032-4d4e-b42d-c9ef547d83cd",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "4cab25ba-6fb8-4339-ae7a-c229ed84c732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "db9d3fec-7a7d-49aa-acec-c92bb78cffd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cab25ba-6fb8-4339-ae7a-c229ed84c732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31452671-57b0-4a1d-ac38-1b4f5b8d05b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cab25ba-6fb8-4339-ae7a-c229ed84c732",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "e22b5f96-6189-4ea6-84d2-40be1922e935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "1fed069f-ea7b-40e0-be63-aca6c5f5c5b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22b5f96-6189-4ea6-84d2-40be1922e935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "981a217e-cb64-479d-aee3-9a1006f41b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22b5f96-6189-4ea6-84d2-40be1922e935",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "c447c703-bf23-4f6f-9a05-0edf52176851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "ff60b56f-199e-4027-9de0-b8710a62ea11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c447c703-bf23-4f6f-9a05-0edf52176851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f57a9ad-7126-46fc-a887-b433d2af3bcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c447c703-bf23-4f6f-9a05-0edf52176851",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "508172c9-198f-4922-9744-93b05232a4de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "335a9f55-378b-4fa1-8ba4-f16f1f947e16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "508172c9-198f-4922-9744-93b05232a4de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b468845-a24c-4d36-bbac-4956f1c9837f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "508172c9-198f-4922-9744-93b05232a4de",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "0b4d4794-0c38-40a5-9af0-cd76f948eccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "2cb18f7a-ff35-43bc-a4d0-fea287d069a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b4d4794-0c38-40a5-9af0-cd76f948eccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "211ab4e3-4f30-461b-aede-d8cf2e7af99a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b4d4794-0c38-40a5-9af0-cd76f948eccd",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "4949fbc1-f876-4bbb-ae4b-affec2302bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "2ae2480f-d12d-41a8-915a-e936ee068dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4949fbc1-f876-4bbb-ae4b-affec2302bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb554bcd-4fec-46b7-8683-9831cd12ee97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4949fbc1-f876-4bbb-ae4b-affec2302bfa",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "32c826d0-5f2a-4c9e-8ad7-7f3f9d287a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "23c0d58f-3e2e-43e7-8de6-43fd2efbeede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c826d0-5f2a-4c9e-8ad7-7f3f9d287a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "642af544-1bb8-43da-920f-f707da22e260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c826d0-5f2a-4c9e-8ad7-7f3f9d287a80",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "34284915-f090-45cc-ab7f-9d1e6aa72b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "8ef768a3-2e9b-45b7-9d3b-dae8546dc391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34284915-f090-45cc-ab7f-9d1e6aa72b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce933d20-0ab6-42d2-8eb3-3fa2a83cf8e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34284915-f090-45cc-ab7f-9d1e6aa72b81",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "a51266f8-6b76-4b51-bdc2-ac6080101664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "803e8c09-797f-4371-bc3f-c645c6f926ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51266f8-6b76-4b51-bdc2-ac6080101664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "260e95ed-712e-4a41-91c3-977e6f21fc5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51266f8-6b76-4b51-bdc2-ac6080101664",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "1f270ab8-a94e-41e2-8f36-b4c68ae40dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "6b372cb6-11b6-4fe0-b72d-98231cedcd8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f270ab8-a94e-41e2-8f36-b4c68ae40dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc83296d-5f86-46d4-be3f-873181ecb848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f270ab8-a94e-41e2-8f36-b4c68ae40dde",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "571dcaaa-ae26-4d27-9bec-f4168cf52c75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "b0fddf41-88f8-4f09-9e12-b45141bae558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "571dcaaa-ae26-4d27-9bec-f4168cf52c75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c99932-5fed-481f-b75f-4d282a62f0c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "571dcaaa-ae26-4d27-9bec-f4168cf52c75",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        },
        {
            "id": "34ccfbe4-f2b6-4397-9159-6664975c4893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "compositeImage": {
                "id": "dfd4e008-4f52-4b77-89c8-cea7f282be88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ccfbe4-f2b6-4397-9159-6664975c4893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b75e017-3e88-4729-933e-bbcf393b72d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ccfbe4-f2b6-4397-9159-6664975c4893",
                    "LayerId": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "bbdc5c68-fb37-4dc1-8b7c-5b2d38cafef8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5debe25-eed3-4ce6-b6d2-ed708960a7ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "e3c3b26b-ae20-44a3-ba2c-2cd239127348",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}