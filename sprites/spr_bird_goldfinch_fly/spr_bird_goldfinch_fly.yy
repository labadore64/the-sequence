{
    "id": "29f3ad92-f54f-4f86-9cf2-6322c8bf53c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_goldfinch_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 32,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "305b664e-1102-4123-bc13-d79601780479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29f3ad92-f54f-4f86-9cf2-6322c8bf53c2",
            "compositeImage": {
                "id": "de4c044d-c61c-4063-b552-29f56d6fd71a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305b664e-1102-4123-bc13-d79601780479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "700db7ba-dc58-470e-ad17-087aba0d6e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305b664e-1102-4123-bc13-d79601780479",
                    "LayerId": "252aaa38-cb75-406f-92b2-1ee9a865d449"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "252aaa38-cb75-406f-92b2-1ee9a865d449",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29f3ad92-f54f-4f86-9cf2-6322c8bf53c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}