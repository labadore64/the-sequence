{
    "id": "57433f84-07e4-4c6f-a4db-1d95ec4858bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainCharacterWalkSide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 88,
    "bbox_right": 155,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90f7c16a-be6e-46e1-8cb6-2f1b069b7444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57433f84-07e4-4c6f-a4db-1d95ec4858bc",
            "compositeImage": {
                "id": "fd987116-5524-43b9-9023-b7ff3ff50f05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f7c16a-be6e-46e1-8cb6-2f1b069b7444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dc7edf6-7224-49c2-8cba-28889b68d7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f7c16a-be6e-46e1-8cb6-2f1b069b7444",
                    "LayerId": "bb429435-8577-4b60-9316-3f3263d79a51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "bb429435-8577-4b60-9316-3f3263d79a51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57433f84-07e4-4c6f-a4db-1d95ec4858bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}