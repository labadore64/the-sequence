{
    "id": "9d819d85-18b4-4a10-989a-22365adf6279",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_bookcase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 35,
    "bbox_right": 93,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b9ec3e1-17ae-4a96-a2e7-e64cc0159bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d819d85-18b4-4a10-989a-22365adf6279",
            "compositeImage": {
                "id": "f03b5b04-dda5-4ad7-8a13-91f824525478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b9ec3e1-17ae-4a96-a2e7-e64cc0159bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f170ddd-6c17-49a5-b0d3-11bf8c0207ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b9ec3e1-17ae-4a96-a2e7-e64cc0159bdb",
                    "LayerId": "a763911e-b05c-4891-9db9-f52b8fc75a43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a763911e-b05c-4891-9db9-f52b8fc75a43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d819d85-18b4-4a10-989a-22365adf6279",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}