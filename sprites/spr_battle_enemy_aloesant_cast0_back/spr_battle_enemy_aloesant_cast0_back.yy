{
    "id": "468717ed-2869-4d48-a377-783cdbed9966",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_cast0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 16,
    "bbox_right": 214,
    "bbox_top": 101,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0216e21-8ff7-41e3-a57a-4700ec900c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "468717ed-2869-4d48-a377-783cdbed9966",
            "compositeImage": {
                "id": "d1321aa8-cc9d-47cd-8466-ac81616ed2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0216e21-8ff7-41e3-a57a-4700ec900c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff88002-9cd8-4340-86fb-87dc020f5d01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0216e21-8ff7-41e3-a57a-4700ec900c65",
                    "LayerId": "c8e04839-0c72-47f7-b7d4-075093defda7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c8e04839-0c72-47f7-b7d4-075093defda7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "468717ed-2869-4d48-a377-783cdbed9966",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}