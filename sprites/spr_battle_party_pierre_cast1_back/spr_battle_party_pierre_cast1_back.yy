{
    "id": "ba909209-baea-48a0-9762-3bbf19bd58a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_cast1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 46,
    "bbox_right": 188,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "306b9af5-846f-4fe3-b617-20555b005707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba909209-baea-48a0-9762-3bbf19bd58a6",
            "compositeImage": {
                "id": "3b019120-f697-49c1-9e7b-9ca310d52bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306b9af5-846f-4fe3-b617-20555b005707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29336d6f-eb6f-4421-b9ed-a3638dfb75ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306b9af5-846f-4fe3-b617-20555b005707",
                    "LayerId": "1a2184ef-9d5f-4d6f-9e55-3363d4c95044"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1a2184ef-9d5f-4d6f-9e55-3363d4c95044",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba909209-baea-48a0-9762-3bbf19bd58a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}