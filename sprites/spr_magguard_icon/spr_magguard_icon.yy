{
    "id": "b3945cc4-82e3-42fd-87a6-108078747344",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_magguard_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd12824c-c70e-4c60-8a1e-46bfbfa0a17a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3945cc4-82e3-42fd-87a6-108078747344",
            "compositeImage": {
                "id": "a81fbd7a-7c8b-4704-b8a3-5d1ede524b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd12824c-c70e-4c60-8a1e-46bfbfa0a17a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a765bfe1-cb23-4866-8ba9-17b734fee531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd12824c-c70e-4c60-8a1e-46bfbfa0a17a",
                    "LayerId": "9b5afadb-b7c4-40a9-bec4-3d0408b0e7c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9b5afadb-b7c4-40a9-bec4-3d0408b0e7c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3945cc4-82e3-42fd-87a6-108078747344",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}