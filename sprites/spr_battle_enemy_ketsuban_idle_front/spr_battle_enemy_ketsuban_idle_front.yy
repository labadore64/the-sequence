{
    "id": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ketsuban_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 32,
    "bbox_right": 234,
    "bbox_top": 71,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c55eb0f-c0b4-4263-9169-5cb103f349f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "df03895e-0f6d-469e-95fa-38fa128973b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c55eb0f-c0b4-4263-9169-5cb103f349f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8383a084-c89f-4ecb-aa2f-5352149cc943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c55eb0f-c0b4-4263-9169-5cb103f349f4",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "c891989d-d5d5-4f8e-8913-4814879abc83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "5b5f094b-9e58-4198-85eb-4822d383c9f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c891989d-d5d5-4f8e-8913-4814879abc83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c800eb2-89c4-41eb-945e-f184fc05c99b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c891989d-d5d5-4f8e-8913-4814879abc83",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "2460853f-a080-4f0b-b546-11af33562706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "4a0ed996-d783-4172-a24f-21746c41c5ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2460853f-a080-4f0b-b546-11af33562706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fda78c1-4458-4ccf-8251-bbecf9b496b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2460853f-a080-4f0b-b546-11af33562706",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "17d8c79b-2b8a-47dc-a175-1dd4d69023c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "a7b8ae39-ea2c-4f9f-b423-8c6729ccef11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17d8c79b-2b8a-47dc-a175-1dd4d69023c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab3ef8f2-a207-44a5-bbb8-b737f7c3c71e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17d8c79b-2b8a-47dc-a175-1dd4d69023c3",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "ea7144db-fc96-4bcc-a79a-7440867f5807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "52de620d-aba6-4054-a25e-cf329fcf0f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7144db-fc96-4bcc-a79a-7440867f5807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a91bf51-7d08-455a-ad45-2c50bdf0a851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7144db-fc96-4bcc-a79a-7440867f5807",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "8d9c7eee-26a6-42b1-b292-8b187de33bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "fdca61ee-c6ce-45dd-ab8b-e8d3d4aefd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9c7eee-26a6-42b1-b292-8b187de33bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b2fe9f5-801b-4436-bff0-0c02a06c52a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9c7eee-26a6-42b1-b292-8b187de33bde",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "5fa7bada-4e4f-4655-898c-b8fdb6ad2414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "e47f3b9f-50fe-4aee-97eb-29d14a126187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa7bada-4e4f-4655-898c-b8fdb6ad2414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c22ac3c-c1d1-41ab-973a-8262c250840d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa7bada-4e4f-4655-898c-b8fdb6ad2414",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "1b2eee3d-4203-49bb-a645-ccc5b5827997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "a0c9fd54-d727-4c8f-8c49-bab37f55edc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b2eee3d-4203-49bb-a645-ccc5b5827997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2fe3b62-93b0-4539-9324-59058da001cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2eee3d-4203-49bb-a645-ccc5b5827997",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "3fa95b63-856f-4342-a374-a864b2efac54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "3f8074b1-1bea-4df3-8154-337598c57191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa95b63-856f-4342-a374-a864b2efac54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17142d1c-f2ce-4dfa-baaa-871c437eba81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa95b63-856f-4342-a374-a864b2efac54",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "f83cef05-1e5e-415a-bc82-bc9d9a047d1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "aa2dbbcd-e80d-4f1d-8dc1-d5080b739648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83cef05-1e5e-415a-bc82-bc9d9a047d1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f40610f-1817-4ca1-935f-8501bdb6428d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83cef05-1e5e-415a-bc82-bc9d9a047d1e",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "a0bd2d9f-be13-4d8f-8bd4-11206becc46b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "c5c6cfa8-6316-404f-999f-9dcd0efd316c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bd2d9f-be13-4d8f-8bd4-11206becc46b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d4b0f8e-3dc7-4fca-a191-f9affc39b3d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bd2d9f-be13-4d8f-8bd4-11206becc46b",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "3eba29c8-bd82-4ae7-87f4-fe048b956434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "e27af8a5-45a9-4ff7-a54a-082118a1bf5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eba29c8-bd82-4ae7-87f4-fe048b956434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1506c557-43c8-4a26-9e90-72bcbb5019d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eba29c8-bd82-4ae7-87f4-fe048b956434",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "5fe26786-7e0c-4955-938a-68a9d8132457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "b72ea6b0-ab94-49b0-b928-c7a125a023f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe26786-7e0c-4955-938a-68a9d8132457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548b7a25-9d1d-4dba-a8f6-3467eaaa54a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe26786-7e0c-4955-938a-68a9d8132457",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "33697798-dce4-46f9-b5ec-e4bf8228d5f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "d8e6d589-0111-4886-89b9-083df199fbbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33697798-dce4-46f9-b5ec-e4bf8228d5f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6926b11-6bc4-4f02-adcb-118c615f9c6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33697798-dce4-46f9-b5ec-e4bf8228d5f5",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "0457c750-334d-4199-b7be-db3b58adcfe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "e371076f-cfb2-4bc8-9df8-6c02fef63eaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0457c750-334d-4199-b7be-db3b58adcfe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e59aded0-34c9-4a1d-9805-061b7ab96cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0457c750-334d-4199-b7be-db3b58adcfe1",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "d84b26ae-f5ea-44e8-83f0-12b7aa5bfd5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "c7bd756a-825b-42c2-ad55-c0d7a03efe66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84b26ae-f5ea-44e8-83f0-12b7aa5bfd5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "233a090d-18f3-4fa0-91cc-fe040c79ec65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84b26ae-f5ea-44e8-83f0-12b7aa5bfd5c",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "f468ebb9-f6e5-444c-a660-0805f0740ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "bf3b52e3-117a-453f-85b6-4769a9a46447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f468ebb9-f6e5-444c-a660-0805f0740ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d6736ee-5e1b-4bf3-ab0d-f97e1a0f1116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f468ebb9-f6e5-444c-a660-0805f0740ad9",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "12aca870-7cc1-4b40-9a7d-213e2971dc9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "9864026d-ccac-40c0-a5eb-830c96671ab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12aca870-7cc1-4b40-9a7d-213e2971dc9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a79436-84a4-4677-83f0-8dd8f3e7a2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12aca870-7cc1-4b40-9a7d-213e2971dc9c",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "747c8568-48ec-46ae-b191-fb096ce8a344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "d98cae66-561a-4dde-a0db-b536beebf788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747c8568-48ec-46ae-b191-fb096ce8a344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4324cfc-7c6a-40f2-b490-71de2b8137b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747c8568-48ec-46ae-b191-fb096ce8a344",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "ba410aa9-9547-4e44-b9e2-7e506723107f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "b8337680-cc9f-4f84-892b-e9719bc81965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba410aa9-9547-4e44-b9e2-7e506723107f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b92532df-abed-49f8-bbce-4c9d3103b56f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba410aa9-9547-4e44-b9e2-7e506723107f",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "774aa151-464d-4adb-b60c-68dabf31b6c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "b33774ac-a5f9-4636-9c9d-2a3d0fccb83b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774aa151-464d-4adb-b60c-68dabf31b6c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce374d5-4400-400e-ae5c-00e4979cc5e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774aa151-464d-4adb-b60c-68dabf31b6c9",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "1746034d-9ea4-4083-b55c-65e0228b1e3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "126bda4b-99ae-4a87-a24f-b4b11cd78002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1746034d-9ea4-4083-b55c-65e0228b1e3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "075c6b32-59e5-4e18-af5f-8d4b2db3ac8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1746034d-9ea4-4083-b55c-65e0228b1e3b",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "1137c39f-f47b-403c-9d94-a4c01b564ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "dc26a8a0-eacf-4162-b270-e818c20cb5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1137c39f-f47b-403c-9d94-a4c01b564ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e04e42e-e078-4205-91e2-a31764cdfbe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1137c39f-f47b-403c-9d94-a4c01b564ebf",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "3de41969-836e-48a8-996c-2aa26cb3e5b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "da1dc2ae-62cb-405a-a086-d372cb5b35e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de41969-836e-48a8-996c-2aa26cb3e5b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0873abd-4fa6-4bc6-957f-e0c13176987c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de41969-836e-48a8-996c-2aa26cb3e5b6",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "ca0073ea-886d-4a43-b5f7-7a90569053d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "75b63267-3f17-4dc7-9241-1e4bd6705409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0073ea-886d-4a43-b5f7-7a90569053d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e814932-9e03-4c70-913b-2e6eabc39667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0073ea-886d-4a43-b5f7-7a90569053d8",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "ff868092-66f6-48dd-970a-baeead781d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "8a8d2ea4-2493-4c89-ae2e-1068dfcf1a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff868092-66f6-48dd-970a-baeead781d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3636b79c-9a9c-43df-a037-307b7abc35bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff868092-66f6-48dd-970a-baeead781d1f",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "7f9c8fdd-a812-468f-9773-8f229883c3fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "2055cb42-67cb-4037-a17a-f657458e4fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f9c8fdd-a812-468f-9773-8f229883c3fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2855f249-1c1b-49e3-b9f8-ac31304197d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f9c8fdd-a812-468f-9773-8f229883c3fe",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "42a6f5ee-c3ae-4fe1-a6d6-d3586c8d4b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "e1acbd4e-9543-49b3-8445-0b212f9980bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a6f5ee-c3ae-4fe1-a6d6-d3586c8d4b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e963745-547f-4d73-8dbe-b20e6fe257e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a6f5ee-c3ae-4fe1-a6d6-d3586c8d4b8e",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "8a1f5f94-70b6-4bc3-b9c7-65bc4aff6da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "9841e497-229d-4d54-bfb1-14cc94616ce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a1f5f94-70b6-4bc3-b9c7-65bc4aff6da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec4b6ee2-1c18-40e2-83d0-95b14aac1fbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a1f5f94-70b6-4bc3-b9c7-65bc4aff6da5",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "e8d448b5-c408-47df-a739-e9ba4c3d93f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "c14433da-0b92-4187-a61a-8b3e63baf9ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d448b5-c408-47df-a739-e9ba4c3d93f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9518e27-b364-49ee-b5c0-b6ecd0207eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d448b5-c408-47df-a739-e9ba4c3d93f6",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "c0ca481d-c6fe-4413-8a0a-c81e664edf01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "7f3e7eda-11e1-4c30-8de5-5d76eebf7d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ca481d-c6fe-4413-8a0a-c81e664edf01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a3d031-81de-44a0-a53e-37038f8107d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ca481d-c6fe-4413-8a0a-c81e664edf01",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "e8ada6db-24a8-499e-97e5-59806e652644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "fb5f9447-5cd3-42a1-9805-1c035d990dbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ada6db-24a8-499e-97e5-59806e652644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ddd8eba-ef2d-44ab-8221-bdf7c779d8a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ada6db-24a8-499e-97e5-59806e652644",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "17c0c2f6-dc32-4eeb-aa89-fe1157be6d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "ea55647f-325f-4a04-9001-b93f9be889d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c0c2f6-dc32-4eeb-aa89-fe1157be6d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c9b956-c6fd-4da6-92b3-1ebe8ec24c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c0c2f6-dc32-4eeb-aa89-fe1157be6d58",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "b4681f85-4e42-47c6-becc-e10ece85a836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "be142e99-2010-45d4-aaf2-56afdb40ea26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4681f85-4e42-47c6-becc-e10ece85a836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b897501a-9adf-4d8b-b138-30fe48678d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4681f85-4e42-47c6-becc-e10ece85a836",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "45269086-a5fc-411f-a306-1a6dbf23ebdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "c3f2f83e-26a3-4b3b-9909-e67edee1e1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45269086-a5fc-411f-a306-1a6dbf23ebdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "448a985e-1eba-4131-89f0-a0384dbdd4c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45269086-a5fc-411f-a306-1a6dbf23ebdb",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "91863f1e-8141-4673-8d32-6f7a0550cc06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "1554cf6b-0c22-4984-96ae-cbfbf436d169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91863f1e-8141-4673-8d32-6f7a0550cc06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3377da17-0a16-429f-820c-cb243802d2fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91863f1e-8141-4673-8d32-6f7a0550cc06",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "8922af75-2ec4-472b-9b32-8cc35e4544b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "96047a05-3e19-4b40-9955-bb5d53f7fb14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8922af75-2ec4-472b-9b32-8cc35e4544b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5955d41e-cda3-4725-bdde-8b5b18fd8105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8922af75-2ec4-472b-9b32-8cc35e4544b3",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "0ec58f33-d0ee-4dab-8b83-8d1e4d36f635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "51882bf4-e4f1-4a43-aa68-7a31ff8a2257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec58f33-d0ee-4dab-8b83-8d1e4d36f635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "988852e3-9351-425d-b21a-b2d16b496420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec58f33-d0ee-4dab-8b83-8d1e4d36f635",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "5d98ccfe-5670-4d3b-9705-c3512d648b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "6dd02203-2428-4bf0-979c-57e7dbc8f5e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d98ccfe-5670-4d3b-9705-c3512d648b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38c9adb-2787-4950-8cd7-253253467538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d98ccfe-5670-4d3b-9705-c3512d648b9c",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "bb14f222-71a3-41a5-8d44-2e5e5e4d31c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "68118328-7fc3-464d-9457-5d7dbf17836e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb14f222-71a3-41a5-8d44-2e5e5e4d31c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f283b3-8398-4a7e-9a81-9ee57ff3e232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb14f222-71a3-41a5-8d44-2e5e5e4d31c3",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "928cfb90-2f6c-49f2-b025-008e12b7161e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "8ee75374-e373-4456-a756-9bacb9bbff0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928cfb90-2f6c-49f2-b025-008e12b7161e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04adfd6a-ccd7-4b6d-95d7-3bbfca11792f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928cfb90-2f6c-49f2-b025-008e12b7161e",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "b7b22da0-ec0e-44ea-890c-ba529bd4d73a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "5ae86e3d-a6fc-4a7d-83b0-f77bf803ae39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7b22da0-ec0e-44ea-890c-ba529bd4d73a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0465aa68-8366-4e16-b591-af3ec8638ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7b22da0-ec0e-44ea-890c-ba529bd4d73a",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "0f2251ef-4c77-4cb9-ad12-78d635392a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "3ef9824a-d279-4ab2-9d78-73584cacbce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f2251ef-4c77-4cb9-ad12-78d635392a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8157222-239a-4d73-bb83-14f0dd28ff3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f2251ef-4c77-4cb9-ad12-78d635392a80",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "88c9d8be-d305-4450-ad8f-38bd72ee90c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "ac9d3fe3-0465-47d3-bc11-e05980def36b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88c9d8be-d305-4450-ad8f-38bd72ee90c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e5969d-7ef1-4ed8-918a-8ebb49ff2a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88c9d8be-d305-4450-ad8f-38bd72ee90c6",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "6360bc2e-b226-4337-b2af-afce95860d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "0aa50cde-f813-44bd-929f-45496b6b3225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6360bc2e-b226-4337-b2af-afce95860d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2568905-5e0f-434b-9e77-b7f0c990f7fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6360bc2e-b226-4337-b2af-afce95860d18",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "7fdc831c-bf21-4c52-a463-894e8b7c9798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "486640c4-1ec6-487a-ac20-8333e311b2af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fdc831c-bf21-4c52-a463-894e8b7c9798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c2ff51-1b7c-4bbd-9cbd-b5d088eba38d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fdc831c-bf21-4c52-a463-894e8b7c9798",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "d44aa6c0-dc51-4401-849b-2a127a28ca63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "03241d92-c2ce-45f9-8d3c-5ee68e4e0ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d44aa6c0-dc51-4401-849b-2a127a28ca63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0be6219-150f-40dc-aef2-ce12ee41d0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d44aa6c0-dc51-4401-849b-2a127a28ca63",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "0c274472-d0bc-4b5a-8bfd-b92e8d911835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "c541802c-7003-48f9-b43d-0cb286176b8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c274472-d0bc-4b5a-8bfd-b92e8d911835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be23f44f-ef3d-4b0b-b410-210367eafaad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c274472-d0bc-4b5a-8bfd-b92e8d911835",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "3356d9a7-333a-4b62-97e6-8f068764f260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "3748ba03-3762-4146-bfee-aa9562c04960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3356d9a7-333a-4b62-97e6-8f068764f260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ba15083-584d-4556-a568-aed2a06829b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3356d9a7-333a-4b62-97e6-8f068764f260",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        },
        {
            "id": "772ce7f8-6cbd-4ea4-b5f1-4992423602ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "compositeImage": {
                "id": "bc3e2419-513f-4c67-a158-98ab66c7b000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772ce7f8-6cbd-4ea4-b5f1-4992423602ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041170e7-3d44-4b18-848a-909193b20922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772ce7f8-6cbd-4ea4-b5f1-4992423602ef",
                    "LayerId": "9fd98b04-df77-4c3a-b78e-c01545a89a6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9fd98b04-df77-4c3a-b78e-c01545a89a6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bcbba14-40b6-4d67-864f-9ac8c26db177",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7b778d75-44fd-4ecb-aef7-f51162934a3d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}