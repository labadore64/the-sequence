{
    "id": "9a61dc75-4998-4a81-a7df-a443a142edb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_magzie_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 234,
    "bbox_left": 32,
    "bbox_right": 219,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "416d2a79-ae43-4488-8e6d-7c4046a287e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a61dc75-4998-4a81-a7df-a443a142edb0",
            "compositeImage": {
                "id": "712949ee-2350-4b48-82ff-b27cc561428a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416d2a79-ae43-4488-8e6d-7c4046a287e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a46d12ee-4f07-42d3-b912-959540ec18d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416d2a79-ae43-4488-8e6d-7c4046a287e9",
                    "LayerId": "1d61963c-fe53-4de1-957d-b10843e23a11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1d61963c-fe53-4de1-957d-b10843e23a11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a61dc75-4998-4a81-a7df-a443a142edb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "c521fe91-d359-41ad-a304-750ae173f15c",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}