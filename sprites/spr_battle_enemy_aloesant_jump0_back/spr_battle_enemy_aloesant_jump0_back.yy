{
    "id": "f2536c2f-b689-44c2-ad5e-00372fa1a7c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 3,
    "bbox_right": 213,
    "bbox_top": 101,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45815b0a-e27f-4f5d-92f5-23983c798d9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2536c2f-b689-44c2-ad5e-00372fa1a7c2",
            "compositeImage": {
                "id": "7aabd63e-81e9-4412-a5e9-738c373e0003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45815b0a-e27f-4f5d-92f5-23983c798d9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49f1d4cc-a53c-4bb1-8093-b58b532d388a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45815b0a-e27f-4f5d-92f5-23983c798d9c",
                    "LayerId": "803bbebc-aa7b-43eb-a315-6f066590953d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "803bbebc-aa7b-43eb-a315-6f066590953d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2536c2f-b689-44c2-ad5e-00372fa1a7c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}