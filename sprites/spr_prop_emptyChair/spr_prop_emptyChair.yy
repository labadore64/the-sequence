{
    "id": "fbc3d504-98fa-4992-adbe-c9faea2afb3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_emptyChair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 54,
    "bbox_right": 85,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdc284d1-927d-430b-8462-051aadbef99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc3d504-98fa-4992-adbe-c9faea2afb3e",
            "compositeImage": {
                "id": "1e261005-b31c-4017-ab0f-51af3f645314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc284d1-927d-430b-8462-051aadbef99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "353f9ea4-27d3-424f-a3ff-06e24c3db3c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc284d1-927d-430b-8462-051aadbef99a",
                    "LayerId": "d2ae94a4-8208-47b2-a1da-4390f4fa9077"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d2ae94a4-8208-47b2-a1da-4390f4fa9077",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbc3d504-98fa-4992-adbe-c9faea2afb3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}