{
    "id": "f9ecac98-080c-476e-aeed-7d450e561163",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_photos",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 19,
    "bbox_right": 106,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c5e3927-8288-4bd6-a323-4af77f9ae100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9ecac98-080c-476e-aeed-7d450e561163",
            "compositeImage": {
                "id": "412b356b-376b-479e-84a0-01ccf9e3f09c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c5e3927-8288-4bd6-a323-4af77f9ae100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a7c7fb-f71a-4cb4-8435-ba5bcde9f23f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c5e3927-8288-4bd6-a323-4af77f9ae100",
                    "LayerId": "a81fca2a-171d-4eb6-8653-d444baae455c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a81fca2a-171d-4eb6-8653-d444baae455c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9ecac98-080c-476e-aeed-7d450e561163",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}