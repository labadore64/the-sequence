{
    "id": "3d8838a0-917d-41f2-8f6a-b1fb4a99f993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_self_yap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c73570d0-7bcc-4d88-9d89-b140a9644b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d8838a0-917d-41f2-8f6a-b1fb4a99f993",
            "compositeImage": {
                "id": "88232267-0aa7-435e-8637-3258a56f236a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73570d0-7bcc-4d88-9d89-b140a9644b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44093b2e-2b6e-4a00-84de-d882eb101400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73570d0-7bcc-4d88-9d89-b140a9644b5f",
                    "LayerId": "91428367-9aa3-41da-9dec-5616434d7a7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "91428367-9aa3-41da-9dec-5616434d7a7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d8838a0-917d-41f2-8f6a-b1fb4a99f993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}