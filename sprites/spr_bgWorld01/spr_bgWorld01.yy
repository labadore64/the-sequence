{
    "id": "af954ec8-30ee-41c2-9e0f-c3d8af49ffaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgWorld01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1199,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34233bbd-4cee-4ed1-85c3-f97bcf77fb86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af954ec8-30ee-41c2-9e0f-c3d8af49ffaa",
            "compositeImage": {
                "id": "da0cec92-6c36-4ddd-9ea6-6eb93f5f6b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34233bbd-4cee-4ed1-85c3-f97bcf77fb86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b6815b4-407e-42f3-8101-deca12664be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34233bbd-4cee-4ed1-85c3-f97bcf77fb86",
                    "LayerId": "4e95df3b-e36f-4f5e-8e93-99d3506f07aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1200,
    "layers": [
        {
            "id": "4e95df3b-e36f-4f5e-8e93-99d3506f07aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af954ec8-30ee-41c2-9e0f-c3d8af49ffaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a8e27ac-4d24-4949-b359-46355e167777",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}