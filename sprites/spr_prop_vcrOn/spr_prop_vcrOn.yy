{
    "id": "a8391da6-b468-40dc-98eb-dcb432a5fdfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_vcrOn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 9,
    "bbox_right": 117,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13e7e871-43ce-4e2f-9e51-6d5724047a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8391da6-b468-40dc-98eb-dcb432a5fdfe",
            "compositeImage": {
                "id": "7625364a-41d3-4b82-969e-169079661cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e7e871-43ce-4e2f-9e51-6d5724047a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d2c9b7-5f66-489e-8895-d9cf1beb69bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e7e871-43ce-4e2f-9e51-6d5724047a22",
                    "LayerId": "51830c4a-832a-4b21-8e1a-dcbafd4c3dc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "51830c4a-832a-4b21-8e1a-dcbafd4c3dc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8391da6-b468-40dc-98eb-dcb432a5fdfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}