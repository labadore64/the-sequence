{
    "id": "13a3fb50-8350-4951-835d-ff47bb8b8701",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign_cat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a19aaa0d-e70b-43e3-be27-ec9331fc8db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13a3fb50-8350-4951-835d-ff47bb8b8701",
            "compositeImage": {
                "id": "64df2e97-f560-4064-8804-e89274694c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a19aaa0d-e70b-43e3-be27-ec9331fc8db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b1c0dd-a4c0-4c61-8848-652cecfe5703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a19aaa0d-e70b-43e3-be27-ec9331fc8db7",
                    "LayerId": "025160d7-88fd-49e2-ba88-f6803b98d4d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "025160d7-88fd-49e2-ba88-f6803b98d4d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13a3fb50-8350-4951-835d-ff47bb8b8701",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}