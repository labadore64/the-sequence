{
    "id": "f446ff5a-7d39-4b80-a6b5-0b7e3d5fd17c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_bone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 7,
    "bbox_right": 117,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1daeecfb-d2d4-4ac2-a09c-7a58f7a91d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f446ff5a-7d39-4b80-a6b5-0b7e3d5fd17c",
            "compositeImage": {
                "id": "d59fb1ec-77bd-48cc-834c-7c55c38ae149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daeecfb-d2d4-4ac2-a09c-7a58f7a91d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bf0a88c-422a-4a89-89a5-8a7f5b262fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daeecfb-d2d4-4ac2-a09c-7a58f7a91d72",
                    "LayerId": "89ded9fe-863c-4329-8096-91a3a0f30b7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "89ded9fe-863c-4329-8096-91a3a0f30b7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f446ff5a-7d39-4b80-a6b5-0b7e3d5fd17c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}