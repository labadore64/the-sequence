{
    "id": "918f3286-7e2a-48ee-9d67-a7865ec05d3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 64,
    "bbox_right": 197,
    "bbox_top": 128,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47a7493e-cd1a-4215-9149-82c67e991c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "918f3286-7e2a-48ee-9d67-a7865ec05d3f",
            "compositeImage": {
                "id": "2e69ec82-33bd-4ce6-b0ff-d12974bb45f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47a7493e-cd1a-4215-9149-82c67e991c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6578f96-df46-4e28-9ddf-8c1518128643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47a7493e-cd1a-4215-9149-82c67e991c76",
                    "LayerId": "8f482c95-f442-4c53-9339-bbdb4fec9214"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8f482c95-f442-4c53-9339-bbdb4fec9214",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "918f3286-7e2a-48ee-9d67-a7865ec05d3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}