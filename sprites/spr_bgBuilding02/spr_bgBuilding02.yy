{
    "id": "4e5aba7d-f9d4-42b7-8d9f-cac11b88e8c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgBuilding02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 780,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc21da55-efe1-4750-b7e3-353e9caf27d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e5aba7d-f9d4-42b7-8d9f-cac11b88e8c8",
            "compositeImage": {
                "id": "f49b2fc1-a2ce-40a3-854a-77b88ddb1710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc21da55-efe1-4750-b7e3-353e9caf27d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "899c75f0-538b-47d7-8b1e-0f96490b8912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc21da55-efe1-4750-b7e3-353e9caf27d5",
                    "LayerId": "b8549c9b-beab-4aae-ac59-43ea112644de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "b8549c9b-beab-4aae-ac59-43ea112644de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e5aba7d-f9d4-42b7-8d9f-cac11b88e8c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "e77601e8-e824-40e3-adc3-938104e85bec",
    "type": 0,
    "width": 781,
    "xorig": 0,
    "yorig": 0
}