{
    "id": "d3df28d0-8aa7-4524-94f4-b784bc4039dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_hit_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 38,
    "bbox_right": 204,
    "bbox_top": 97,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c205c72e-2119-4ca2-8aef-9c22b825dc2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3df28d0-8aa7-4524-94f4-b784bc4039dd",
            "compositeImage": {
                "id": "eb437e1b-2ab4-4a88-a476-d5a845f8155a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c205c72e-2119-4ca2-8aef-9c22b825dc2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c83a374-cd4e-4b7a-b6bf-d6536d26956d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c205c72e-2119-4ca2-8aef-9c22b825dc2e",
                    "LayerId": "6aace709-d90f-4ce3-92e0-9ee28cfc52f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6aace709-d90f-4ce3-92e0-9ee28cfc52f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3df28d0-8aa7-4524-94f4-b784bc4039dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}