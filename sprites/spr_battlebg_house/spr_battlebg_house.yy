{
    "id": "77a3321f-120c-48f1-aa09-89800685e7aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battlebg_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d214ac3-c1b3-45e2-aee5-6c9a514d9f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a3321f-120c-48f1-aa09-89800685e7aa",
            "compositeImage": {
                "id": "1d2e2c69-2874-43dd-b042-139ce0ddee89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d214ac3-c1b3-45e2-aee5-6c9a514d9f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "840a77e4-d1fd-4390-afde-114548e4597a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d214ac3-c1b3-45e2-aee5-6c9a514d9f5f",
                    "LayerId": "72481655-03bc-4b69-9fc5-da315f71899b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "72481655-03bc-4b69-9fc5-da315f71899b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77a3321f-120c-48f1-aa09-89800685e7aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "68830fb3-9087-468c-b1b4-fcc022a8879c",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}