{
    "id": "a6edee61-f7eb-410c-a8ae-eea4e553ab87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 72,
    "bbox_right": 142,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "609f3245-8af3-449d-9b50-b6cfee8ad453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6edee61-f7eb-410c-a8ae-eea4e553ab87",
            "compositeImage": {
                "id": "4bd71748-f516-498e-8c09-077ecb809849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609f3245-8af3-449d-9b50-b6cfee8ad453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0de56d-8406-414b-8404-c32328eb9de1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609f3245-8af3-449d-9b50-b6cfee8ad453",
                    "LayerId": "c02166dd-482f-4544-8617-ecab61dda085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c02166dd-482f-4544-8617-ecab61dda085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6edee61-f7eb-410c-a8ae-eea4e553ab87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 180
}