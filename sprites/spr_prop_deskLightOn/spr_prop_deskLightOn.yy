{
    "id": "725bed81-202c-4b43-90d0-6404eb31599c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_deskLightOn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 13,
    "bbox_right": 109,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05b03dce-238e-4c4f-b47c-3b6849b79936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "725bed81-202c-4b43-90d0-6404eb31599c",
            "compositeImage": {
                "id": "d7dc8b71-b920-4753-9eb0-79953d95a7e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b03dce-238e-4c4f-b47c-3b6849b79936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73f77ecc-5dd6-40ab-ac55-0dda7662cb7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b03dce-238e-4c4f-b47c-3b6849b79936",
                    "LayerId": "50a2632c-52c3-47dc-ba95-9b89d266926d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "50a2632c-52c3-47dc-ba95-9b89d266926d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "725bed81-202c-4b43-90d0-6404eb31599c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}