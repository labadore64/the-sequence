{
    "id": "02987c91-65d1-4df7-aa79-5429b8900874",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "debugMenuTransformation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccd60243-b792-4bd5-87f1-b810e5e94959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02987c91-65d1-4df7-aa79-5429b8900874",
            "compositeImage": {
                "id": "8a082703-f906-42e5-8b4c-e8dc6a410268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd60243-b792-4bd5-87f1-b810e5e94959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97ddb87f-6ef3-470b-a7b3-3e5223910efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd60243-b792-4bd5-87f1-b810e5e94959",
                    "LayerId": "c422b286-afb9-414f-afc6-b4f669f7c69e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c422b286-afb9-414f-afc6-b4f669f7c69e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02987c91-65d1-4df7-aa79-5429b8900874",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}