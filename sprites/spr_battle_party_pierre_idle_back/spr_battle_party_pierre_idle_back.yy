{
    "id": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 246,
    "bbox_left": 29,
    "bbox_right": 209,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88af198a-40f7-4969-8277-beaa7172053d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "75073546-1341-4d17-89bb-d2eb15581d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88af198a-40f7-4969-8277-beaa7172053d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b4eaf0-1926-4e21-a017-39810293dc51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88af198a-40f7-4969-8277-beaa7172053d",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "271af34b-a243-4fb0-8672-97ce69a4e689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "92718a96-499c-435a-851c-5f49cdbf440f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "271af34b-a243-4fb0-8672-97ce69a4e689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d30d1125-b968-458f-a154-6a8e90a91d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "271af34b-a243-4fb0-8672-97ce69a4e689",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "92ab9d5c-af83-4976-b9d9-417b25ed3111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "d68e3e50-6fe4-4e19-85a5-85cd3a753c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92ab9d5c-af83-4976-b9d9-417b25ed3111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c713f4-eb45-49ad-8afa-d25846ac1b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92ab9d5c-af83-4976-b9d9-417b25ed3111",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "f988dd80-74e6-414e-b742-d9f1f9cdaef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "d7d31298-7cfd-4b8d-9ab9-b05641e6ccad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f988dd80-74e6-414e-b742-d9f1f9cdaef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b2f11f-3cde-4eae-895a-fcf7830689e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f988dd80-74e6-414e-b742-d9f1f9cdaef9",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "4453da9d-5cf5-455e-bf98-0c1aeda56caf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "aa2ad9a1-5468-420b-8f18-32909ab4a05c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4453da9d-5cf5-455e-bf98-0c1aeda56caf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a3bd5a-ad3b-459a-bb50-93c7a6fda467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4453da9d-5cf5-455e-bf98-0c1aeda56caf",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "19bbb85f-1f9a-4ef8-8fe3-a394b3451d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "28fa14a9-88f7-47eb-8159-975e0c76d0f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19bbb85f-1f9a-4ef8-8fe3-a394b3451d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e455c4-4205-4ea2-b3cd-e1d8661d722a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19bbb85f-1f9a-4ef8-8fe3-a394b3451d12",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "b4d551c0-7802-4f88-9c7b-44dbf09c13cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "361b183a-9545-44e7-a3f4-a2d39ec84522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d551c0-7802-4f88-9c7b-44dbf09c13cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1653a74d-6667-4db8-b202-df80476b69c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d551c0-7802-4f88-9c7b-44dbf09c13cc",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "594eea99-ed2b-4c4e-90b5-a7e8158947e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "c25e4c3b-66f3-4c8a-a44c-2d0c2a136e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594eea99-ed2b-4c4e-90b5-a7e8158947e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "026768e9-ffba-44eb-9154-4a414fed5c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594eea99-ed2b-4c4e-90b5-a7e8158947e8",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "ff0f82d3-25c6-47f7-a4dd-cf22ad5357d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "248aa4dd-e564-4df9-b9a4-9399919c3838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0f82d3-25c6-47f7-a4dd-cf22ad5357d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ce3a1b-260c-4a81-8b5e-57ff444e9ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0f82d3-25c6-47f7-a4dd-cf22ad5357d0",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "bfa160d1-a94f-4416-a75b-e31aebbaa6f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "60bba74b-6f60-4617-8e58-b7d519e00a05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa160d1-a94f-4416-a75b-e31aebbaa6f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900cd038-ca31-41d0-9220-e68078e6ba8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa160d1-a94f-4416-a75b-e31aebbaa6f8",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "1f5d767b-3f7e-4260-abb9-bc1000449a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "4f29a445-bcab-4f3e-b661-369fa3ee1cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5d767b-3f7e-4260-abb9-bc1000449a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1628d0c-ce58-40af-9dd4-d2698cf8fbb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5d767b-3f7e-4260-abb9-bc1000449a5c",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "26a17a8c-51e7-48f4-b10e-d0a8084c87d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "8182578b-a02f-4e38-897f-e3c71fa1ddc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a17a8c-51e7-48f4-b10e-d0a8084c87d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef78ff84-aad1-4d04-a226-2959cbd40095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a17a8c-51e7-48f4-b10e-d0a8084c87d2",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "b122bd2e-528c-4690-9fc7-682fd573a5a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "5709817a-1ff1-4fa0-99c6-9f5b16c72244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b122bd2e-528c-4690-9fc7-682fd573a5a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4298f38d-7c07-401a-b909-bb37dcf6ae18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b122bd2e-528c-4690-9fc7-682fd573a5a3",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "933cd9dd-242e-4c0e-8eff-1388764145b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "f429a149-60e8-4bfd-9297-b667645c306c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933cd9dd-242e-4c0e-8eff-1388764145b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dbde186-65f9-4f14-a9b5-b48a5cad7358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933cd9dd-242e-4c0e-8eff-1388764145b2",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "ebc62664-f82d-409b-b593-3ad1de0df632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "2be7c05b-bd48-46f4-a409-aeed864b9841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc62664-f82d-409b-b593-3ad1de0df632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b3ae99-744a-42eb-95a8-2205258440f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc62664-f82d-409b-b593-3ad1de0df632",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "52b8dea5-8100-4443-a012-45aaf32d0b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "164e5340-6c88-4382-a896-e4f1a68c7a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b8dea5-8100-4443-a012-45aaf32d0b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71be4000-24f6-4bf0-9733-b76b2aa30a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b8dea5-8100-4443-a012-45aaf32d0b80",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "714ce0a7-417b-4d67-86aa-b512bd749298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "43a6d58c-2ac3-40c9-a3d8-59c8e18e1556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "714ce0a7-417b-4d67-86aa-b512bd749298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce36b543-4c3d-4283-92b4-ea7a40b91a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "714ce0a7-417b-4d67-86aa-b512bd749298",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "a34e4d53-e55d-4f40-a268-5ba30aa622a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "7ddb5211-37ed-4831-aa6e-5cd3f3510093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a34e4d53-e55d-4f40-a268-5ba30aa622a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd2d838b-275a-48b2-8aa2-a6a2d16a8aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a34e4d53-e55d-4f40-a268-5ba30aa622a9",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "f8d31b4d-46b7-495b-bb66-36f403a1a0e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "282a14bf-a8fe-4ac2-ad00-bd191a767da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d31b4d-46b7-495b-bb66-36f403a1a0e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece9df6a-dd7f-4562-8644-d99df3bf65d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d31b4d-46b7-495b-bb66-36f403a1a0e9",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "fb38aa69-40fe-4a00-be32-003bb37c57fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "db11f096-350c-4527-91aa-173cdaae246e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb38aa69-40fe-4a00-be32-003bb37c57fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c1fb467-6b25-4291-b598-a658f010ac06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb38aa69-40fe-4a00-be32-003bb37c57fd",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "118c2c8a-f1a5-4ac4-aa73-1572eb1570bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "ed0f3381-003b-4f8a-b6fe-5a51e2e6ebdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "118c2c8a-f1a5-4ac4-aa73-1572eb1570bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd3e371-2979-4518-9e11-1c32cbf5a531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "118c2c8a-f1a5-4ac4-aa73-1572eb1570bd",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "54477b29-6beb-480f-9f88-f9db1a4a5824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "8f604a0c-ca04-469d-b0be-854d499e9ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54477b29-6beb-480f-9f88-f9db1a4a5824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9a9351-478b-4449-868d-e454b699c0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54477b29-6beb-480f-9f88-f9db1a4a5824",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "1952d3f3-d58a-4bcf-8c5b-161a59b12ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "71c553b3-4c2e-4102-8818-4d8247840510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1952d3f3-d58a-4bcf-8c5b-161a59b12ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a56473-7f44-4801-9042-e3fbf5ce7d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1952d3f3-d58a-4bcf-8c5b-161a59b12ad8",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "82379cff-bf5f-4cf2-abd5-6517499392d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "ca5f3b68-1b72-4ea3-8700-56155c099ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82379cff-bf5f-4cf2-abd5-6517499392d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c38269-3050-44f1-af7d-ae3b789f14b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82379cff-bf5f-4cf2-abd5-6517499392d2",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "9662e80d-790a-42b0-a5a2-d1a011ca1a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "c0fd0b1d-4477-4c66-a82c-0d5c92b89cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9662e80d-790a-42b0-a5a2-d1a011ca1a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b80420a8-0bd7-456d-b8b2-edab9aadf172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9662e80d-790a-42b0-a5a2-d1a011ca1a51",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "f61a4246-b8c7-46c9-8252-01f4862f3c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "7a74d39e-9bd6-45ca-a35d-f101fe45b88b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f61a4246-b8c7-46c9-8252-01f4862f3c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "325850c8-7076-4a65-a86b-abb5eeddde1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f61a4246-b8c7-46c9-8252-01f4862f3c2e",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "b1484f25-c777-4d51-8a28-391cf69862d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "4fec9866-c1bd-460b-9ed8-cfdc010b978c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1484f25-c777-4d51-8a28-391cf69862d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f287fd-5754-47ed-9ac5-b7813a7793fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1484f25-c777-4d51-8a28-391cf69862d1",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "8df38205-3c27-4693-b730-e4b36b2334a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "d83b37bd-728f-4110-94d3-fdaae2211466",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8df38205-3c27-4693-b730-e4b36b2334a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "814c0045-7c12-4f5a-9993-e850eabfca5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8df38205-3c27-4693-b730-e4b36b2334a9",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "fac96e87-8911-437b-aadc-7579171c62aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "6df47f40-8fc6-417e-8063-8168d2606ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac96e87-8911-437b-aadc-7579171c62aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "807988f8-2fa8-441c-80dc-c9c8554e8abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac96e87-8911-437b-aadc-7579171c62aa",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "66f402c1-aefe-48a3-8d7e-4a357aed9325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "9de3d2a2-5bca-42dd-a7f0-fe13ad0c6481",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f402c1-aefe-48a3-8d7e-4a357aed9325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83c8d46-0882-4d6e-aa42-f282b1777b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f402c1-aefe-48a3-8d7e-4a357aed9325",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "777039dc-d9ca-42ab-91a5-e305934b7986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "c326f86c-8642-4b23-bebd-650509e24e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777039dc-d9ca-42ab-91a5-e305934b7986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caa10cb6-85cd-4757-9e63-e146c37d14d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777039dc-d9ca-42ab-91a5-e305934b7986",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "5b15c90e-bce1-4d41-8e7d-c32662719bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "cf3085b9-b5a7-42b4-95c9-647272c35afd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b15c90e-bce1-4d41-8e7d-c32662719bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e3c3de-e3f1-4b5f-a436-47db537cc73a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b15c90e-bce1-4d41-8e7d-c32662719bee",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "a18f3336-8c9b-4149-98cf-ceab09faf44f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "6490058c-b1ef-4f54-827b-7b9d0f14c6d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18f3336-8c9b-4149-98cf-ceab09faf44f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b9c1d2-b3fc-423e-b9a5-d59ec6b77100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18f3336-8c9b-4149-98cf-ceab09faf44f",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "df203990-c2de-4bd6-ab52-8fedb0e08840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "25c27522-bf02-4265-8407-b61151190251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df203990-c2de-4bd6-ab52-8fedb0e08840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87adee67-4059-4cd1-8ad5-12e73d61d147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df203990-c2de-4bd6-ab52-8fedb0e08840",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "928ebd83-78ba-4112-a36c-7aaaad9a0b50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "a91e24e8-97ad-40f9-92c3-98d993e507d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928ebd83-78ba-4112-a36c-7aaaad9a0b50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78222b45-54c4-46dd-a939-b01df4d8e04c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928ebd83-78ba-4112-a36c-7aaaad9a0b50",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "d93b6d31-14dd-4ae2-9f86-6e3dfb1395f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "53b787c0-4bfb-4d5d-adca-61b8e762aca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93b6d31-14dd-4ae2-9f86-6e3dfb1395f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48940877-8e1d-494b-ba07-01f2a9bdb7df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93b6d31-14dd-4ae2-9f86-6e3dfb1395f4",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "433dc99e-ddd2-499a-aa88-08bbfa3b6c3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "40b1e6ba-0db0-40cd-9a6d-23f34f7e7abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "433dc99e-ddd2-499a-aa88-08bbfa3b6c3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eecad511-cfb0-4c56-9386-1ec9312fb08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "433dc99e-ddd2-499a-aa88-08bbfa3b6c3b",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "19409027-2582-4148-948b-ceb8a026f39c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "483535ed-e24c-4360-9d8e-55b046755036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19409027-2582-4148-948b-ceb8a026f39c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2aa569-3175-4486-9ec0-1bf1379e98ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19409027-2582-4148-948b-ceb8a026f39c",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "22b1ce07-176e-493d-a323-65fb44bd939d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "f6ad13f0-d4dc-4e81-82f8-bbbf45e79ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22b1ce07-176e-493d-a323-65fb44bd939d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61fd3ed6-455e-4eae-a4e8-1c94c1394bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22b1ce07-176e-493d-a323-65fb44bd939d",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "4db90c95-4ab4-4284-a1d3-ff265b504751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "e1675ab5-a2fb-4b0b-83cb-c561884418df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db90c95-4ab4-4284-a1d3-ff265b504751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6015ce8c-871b-4345-abb4-840d38da0d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db90c95-4ab4-4284-a1d3-ff265b504751",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "4aec6ce4-4dff-4ced-a9da-b34f8f3c2f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "5d67a655-548d-468f-8307-41bf625dc31d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aec6ce4-4dff-4ced-a9da-b34f8f3c2f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c9fac78-7413-4f77-9429-1f7cf4db305a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aec6ce4-4dff-4ced-a9da-b34f8f3c2f28",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "4603fa2c-fc57-4164-a870-eaf5aee36624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "f3e33e7c-6bba-4586-b68b-8eb961d626d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4603fa2c-fc57-4164-a870-eaf5aee36624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46fa4a6b-fd61-47ac-9a95-4b24d88d66ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4603fa2c-fc57-4164-a870-eaf5aee36624",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "f571b318-cab7-415f-b37d-1a7c9969734d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "3bd16905-2a96-4085-9ce6-8c2782a75b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f571b318-cab7-415f-b37d-1a7c9969734d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75933a42-5e90-46ee-8167-81b21b4262c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f571b318-cab7-415f-b37d-1a7c9969734d",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "e3f32994-3dbd-42a4-9404-0153e2451b9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "11cef2fc-7b2c-4429-886c-c23bdc379d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f32994-3dbd-42a4-9404-0153e2451b9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2acf8ec-eb59-4d26-825a-4b74435fc81d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f32994-3dbd-42a4-9404-0153e2451b9e",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "036ae880-5363-41ba-8bac-6bb05f47bb64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "38bbb86d-134f-45b8-91f1-8f8e68e74274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "036ae880-5363-41ba-8bac-6bb05f47bb64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef81d011-9e57-4012-9d99-93a8cebcb0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "036ae880-5363-41ba-8bac-6bb05f47bb64",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "f85d7540-e668-43cc-9e2e-7ea05568b5f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "dd86f611-9d47-40eb-849f-f396a52eb079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f85d7540-e668-43cc-9e2e-7ea05568b5f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "435e115a-7d02-4e6e-8615-ccc193dd5c48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f85d7540-e668-43cc-9e2e-7ea05568b5f6",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "1a7bd388-6303-4681-b972-c8c832461286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "b3706a8b-a76f-4166-9ee3-292aa9689cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a7bd388-6303-4681-b972-c8c832461286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb1d5842-15f8-4846-a719-a92ff8c34057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a7bd388-6303-4681-b972-c8c832461286",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "b04cdc95-d7ce-4bbc-9cb5-e5e8ea3ef87b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "cb211791-ce07-4f74-a0fb-0b73d83c23ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04cdc95-d7ce-4bbc-9cb5-e5e8ea3ef87b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ab2896-4504-4286-abaf-2da436ad579b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04cdc95-d7ce-4bbc-9cb5-e5e8ea3ef87b",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "2b5ad32e-92be-4720-92e9-8c4a8fc12cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "f09313da-14f7-46c2-ac08-45682723735b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5ad32e-92be-4720-92e9-8c4a8fc12cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5465b18-4951-49cf-a1c6-83f978faac32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5ad32e-92be-4720-92e9-8c4a8fc12cf6",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        },
        {
            "id": "e3830529-8291-449b-b53d-0d23031fd08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "compositeImage": {
                "id": "d4312909-4299-4bcc-92a9-03ce62c3a61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3830529-8291-449b-b53d-0d23031fd08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf59d18-33c5-403c-b11f-b76cf42bd348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3830529-8291-449b-b53d-0d23031fd08a",
                    "LayerId": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1c22bd34-33ae-4ec6-b142-6d81aa37cfdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f81bd5d-beaa-4035-ad4c-b48d89462bf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}