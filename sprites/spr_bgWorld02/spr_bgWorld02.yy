{
    "id": "1e5a462b-8cfb-44b0-875d-268d470f70a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgWorld02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1199,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a062165d-b4e5-49e4-a87e-878e7fdc3e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e5a462b-8cfb-44b0-875d-268d470f70a4",
            "compositeImage": {
                "id": "20a41bbb-73fc-4a21-b90a-4a9d68d2ebc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a062165d-b4e5-49e4-a87e-878e7fdc3e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f428a31-4d9b-4ffc-acd5-b8636183d125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a062165d-b4e5-49e4-a87e-878e7fdc3e2c",
                    "LayerId": "0ae44fa5-8e0d-47be-8740-a9f9a85d7be7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1200,
    "layers": [
        {
            "id": "0ae44fa5-8e0d-47be-8740-a9f9a85d7be7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e5a462b-8cfb-44b0-875d-268d470f70a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "bed839bd-b155-4540-a621-964448d42377",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}