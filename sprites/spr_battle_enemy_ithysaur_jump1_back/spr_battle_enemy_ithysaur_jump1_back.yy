{
    "id": "657dbf56-27b7-4586-a09f-c7a4a885ebd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 204,
    "bbox_left": 4,
    "bbox_right": 241,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2e762f6-d4dc-415f-aeb0-18fe38ae803f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "657dbf56-27b7-4586-a09f-c7a4a885ebd3",
            "compositeImage": {
                "id": "22eae44f-2606-4deb-90a8-518d72b96095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e762f6-d4dc-415f-aeb0-18fe38ae803f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8480295-edf3-4bd7-b472-92a8b1728e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e762f6-d4dc-415f-aeb0-18fe38ae803f",
                    "LayerId": "8d9c7332-25dc-416a-abef-0782d138deac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8d9c7332-25dc-416a-abef-0782d138deac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "657dbf56-27b7-4586-a09f-c7a4a885ebd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}