{
    "id": "773615fd-b0ba-455b-82d8-70ba4ce1487f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainCharacterStandSide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 88,
    "bbox_right": 155,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb8c540d-84a7-45da-af97-ca39d6fb2e57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773615fd-b0ba-455b-82d8-70ba4ce1487f",
            "compositeImage": {
                "id": "c3e07a01-a509-4973-8883-20cb09fa4979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb8c540d-84a7-45da-af97-ca39d6fb2e57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdd9dcc5-7eb1-4493-8b13-fbe57c8aea21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb8c540d-84a7-45da-af97-ca39d6fb2e57",
                    "LayerId": "94d154d7-a43c-4c5e-a22f-bc37182952f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "94d154d7-a43c-4c5e-a22f-bc37182952f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "773615fd-b0ba-455b-82d8-70ba4ce1487f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}