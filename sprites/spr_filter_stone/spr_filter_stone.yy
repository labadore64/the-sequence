{
    "id": "f0dc05ee-1034-4a59-a8d3-2c60e215df7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_stone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 220,
    "bbox_left": 42,
    "bbox_right": 217,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f518a757-4667-4f1f-ada7-86cb339a79b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0dc05ee-1034-4a59-a8d3-2c60e215df7e",
            "compositeImage": {
                "id": "308c05f0-b99d-4ff0-91d5-3f5a8674b55a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f518a757-4667-4f1f-ada7-86cb339a79b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f79eb5b-379a-49b8-b524-f2483a5cdb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f518a757-4667-4f1f-ada7-86cb339a79b1",
                    "LayerId": "27a74368-2704-41a2-a32c-ab75428f64ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "27a74368-2704-41a2-a32c-ab75428f64ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0dc05ee-1034-4a59-a8d3-2c60e215df7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}