{
    "id": "11101010-d234-4638-a72d-46219dd74c7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 38,
    "bbox_right": 222,
    "bbox_top": 100,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7916b49-9464-4b34-bbc0-b5e99fdfd866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11101010-d234-4638-a72d-46219dd74c7f",
            "compositeImage": {
                "id": "9e82df8b-15dc-4b7a-88cd-e72dd959df13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7916b49-9464-4b34-bbc0-b5e99fdfd866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9513fdd-1bec-405e-9ccb-caabd788523b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7916b49-9464-4b34-bbc0-b5e99fdfd866",
                    "LayerId": "ff976696-03d5-4a48-8b99-65ae589ccaed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ff976696-03d5-4a48-8b99-65ae589ccaed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11101010-d234-4638-a72d-46219dd74c7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}