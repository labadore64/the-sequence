{
    "id": "dbfc81ee-9f6b-4e56-8e76-a523708dd26f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_rock2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 24,
    "bbox_right": 122,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a0dacc0-2961-49e5-8ff3-b95ff13a08bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbfc81ee-9f6b-4e56-8e76-a523708dd26f",
            "compositeImage": {
                "id": "283c8085-187a-487d-b7f1-ca221ffbe0df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0dacc0-2961-49e5-8ff3-b95ff13a08bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f54a61-3496-43be-bde3-28e8f78eedc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0dacc0-2961-49e5-8ff3-b95ff13a08bb",
                    "LayerId": "5b1972f4-9b9f-4f49-97d9-f357832b3e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5b1972f4-9b9f-4f49-97d9-f357832b3e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbfc81ee-9f6b-4e56-8e76-a523708dd26f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}