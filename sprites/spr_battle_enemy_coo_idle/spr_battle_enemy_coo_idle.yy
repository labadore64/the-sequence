{
    "id": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 64,
    "bbox_right": 205,
    "bbox_top": 151,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5e43e2e-765b-496c-9615-4719bd48a9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "9c5eeaa9-9373-4e9e-a783-ebeb8d747e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e43e2e-765b-496c-9615-4719bd48a9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1776ed93-85ba-4262-8141-ef8f276a4fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e43e2e-765b-496c-9615-4719bd48a9b0",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "9b7f138b-2093-4ae0-8713-90e6a0870336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "03cecf11-4e3b-4ba9-b141-518d7a51c959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b7f138b-2093-4ae0-8713-90e6a0870336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84416ac3-e255-4d4a-8f8e-b49969fc28d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b7f138b-2093-4ae0-8713-90e6a0870336",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "abca6cd5-d141-4ebd-9d5a-e868c31df9a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "9c2f7324-33c3-4e8c-95d9-34ccf30ff477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abca6cd5-d141-4ebd-9d5a-e868c31df9a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761dfe3b-0308-47c9-ad20-c3706dfec9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abca6cd5-d141-4ebd-9d5a-e868c31df9a0",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "6376167b-a149-4676-a6a0-5caf5a8381d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "516d6eb2-6fb1-4248-a7b5-5fb43f90c341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6376167b-a149-4676-a6a0-5caf5a8381d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fdbea7f-93ce-465f-9513-b4ea47e1044d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6376167b-a149-4676-a6a0-5caf5a8381d4",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "04dcaf39-c72f-4660-b2d0-7e88ef83b4ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "aaf4fa0f-88ae-40be-b35c-851f9b21321b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04dcaf39-c72f-4660-b2d0-7e88ef83b4ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5063ca0d-5cc8-41e0-9dd6-5afd81be0479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04dcaf39-c72f-4660-b2d0-7e88ef83b4ee",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "a075201b-8164-4e45-81df-6377fa4e6d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "7d6d6ab8-3d4f-4efa-8813-03488674005a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a075201b-8164-4e45-81df-6377fa4e6d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0949f65a-98c6-4c80-998c-74914cf45b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a075201b-8164-4e45-81df-6377fa4e6d10",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "842a028d-fa6b-4b24-b2b5-55f84bd6d2f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "0081ab54-5ee9-4bc3-b194-50949b62ba40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "842a028d-fa6b-4b24-b2b5-55f84bd6d2f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e023f62c-4428-4077-89e6-ee99e31ec54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "842a028d-fa6b-4b24-b2b5-55f84bd6d2f1",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "1d3b3af5-6a92-4e60-b826-0e64ccd9f45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "8ba2ee89-2a1b-493a-a720-bd314a23cf7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3b3af5-6a92-4e60-b826-0e64ccd9f45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc4f16ad-688b-4950-a7db-98298c5de0ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3b3af5-6a92-4e60-b826-0e64ccd9f45e",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "39f5eea0-5296-4365-bee1-6b76ee958bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "b762d8f9-4b09-47fb-beac-f9d833934773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f5eea0-5296-4365-bee1-6b76ee958bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a2896e-ec5a-4eed-9277-87a0f8caa9ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f5eea0-5296-4365-bee1-6b76ee958bc4",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "22799ef4-a0cf-40e0-87dd-5656ee48294f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "5e2dacc2-4fa4-489e-ae15-31064e5a0524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22799ef4-a0cf-40e0-87dd-5656ee48294f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0b5809-d812-42c9-8d1d-c80d43609060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22799ef4-a0cf-40e0-87dd-5656ee48294f",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "dc4ae5fc-2741-4c10-aef3-672a70b18a2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "bebe561e-11a2-48d9-b852-e9421c80ac16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4ae5fc-2741-4c10-aef3-672a70b18a2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c55707-3b3f-46fe-8620-738c6b6ec8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4ae5fc-2741-4c10-aef3-672a70b18a2f",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "2b80e8e7-36e4-4ef0-8894-abc9d8cc91a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "53181f19-013a-4d42-8a17-327916f43fec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b80e8e7-36e4-4ef0-8894-abc9d8cc91a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ed2261-6438-4180-b365-76cd33ab3641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b80e8e7-36e4-4ef0-8894-abc9d8cc91a7",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "567757f4-893c-491f-82b5-2e0cee207aa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "af06a1f6-e444-4d98-b50b-732b895d5e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567757f4-893c-491f-82b5-2e0cee207aa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cfd5d4d-9630-4a9a-8a9a-0b213169364d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567757f4-893c-491f-82b5-2e0cee207aa0",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "927a47e9-c56d-418e-a3a3-a8ab6dea7264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "7739548d-6024-4b45-9e2a-db3c7574f008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927a47e9-c56d-418e-a3a3-a8ab6dea7264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8f9772-28c9-4ae0-bad9-f0c3d86b5b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927a47e9-c56d-418e-a3a3-a8ab6dea7264",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "a7e9ea8c-4dd8-4de2-97d1-3efc312ceb3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "5ffec28c-d695-429e-8a28-ebe480c2a689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e9ea8c-4dd8-4de2-97d1-3efc312ceb3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db4fcb6d-1f11-4d4d-80e9-4f1cfc283c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e9ea8c-4dd8-4de2-97d1-3efc312ceb3e",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "8cf50c83-b8c4-469d-bf91-4d294de6c989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "b4b550a8-2b18-4125-8d90-fc0fa4e2df2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf50c83-b8c4-469d-bf91-4d294de6c989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6da3fd1-0f4f-4dee-80af-7ebd1f33b7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf50c83-b8c4-469d-bf91-4d294de6c989",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "82066853-5cb1-4d50-a131-77a1fb7ae3b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "6c5b4de8-8431-4936-9f4a-de8e3a1e12ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82066853-5cb1-4d50-a131-77a1fb7ae3b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89b8d5c-d0de-4267-bec7-3fd6537f2661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82066853-5cb1-4d50-a131-77a1fb7ae3b4",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "9bf3e303-934c-48ab-968e-de91418f61af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "f7f1895c-c6cb-4b3f-85db-f79aecdae442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf3e303-934c-48ab-968e-de91418f61af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbd1362-bf9b-4db6-bdbf-c58af50e41e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf3e303-934c-48ab-968e-de91418f61af",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "a28bbdf1-6a41-48f5-9c8c-10ba3dc19c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "0bd134a8-7ad1-48da-8ab9-213ff4653ae2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28bbdf1-6a41-48f5-9c8c-10ba3dc19c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f8337a-ce6d-4f0a-a0ab-dc56eadfcf9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28bbdf1-6a41-48f5-9c8c-10ba3dc19c6a",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "13118bc9-ea18-4849-a7c2-367903b8e60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "7b805a93-fcab-4e7e-8407-f0c12087c5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13118bc9-ea18-4849-a7c2-367903b8e60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "181e3b34-07af-47b4-9e78-12474bdae7e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13118bc9-ea18-4849-a7c2-367903b8e60d",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "d8b8cf30-54e1-4eab-9744-e07360a91ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "e68a6875-5678-474d-b679-65e0c287079f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b8cf30-54e1-4eab-9744-e07360a91ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e672d8-36a4-406c-a5c8-fc5441cf620f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b8cf30-54e1-4eab-9744-e07360a91ad9",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "12383847-5252-4906-9e2f-69b68b008508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "dd229b2a-2268-49b7-9abf-74d89137ecec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12383847-5252-4906-9e2f-69b68b008508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a67871c-bb75-49de-97be-f700c8513f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12383847-5252-4906-9e2f-69b68b008508",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "61e0b7ba-982f-4ed6-8525-b96df12c6e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "04799995-7568-4de4-84db-b6f5938da273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61e0b7ba-982f-4ed6-8525-b96df12c6e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b8b269a-2d64-4736-879c-f26503793b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61e0b7ba-982f-4ed6-8525-b96df12c6e7c",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "cf4d3840-425e-4bd7-98a6-69586282bd71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "8273db07-57aa-452f-a3ea-b2e61bed7dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4d3840-425e-4bd7-98a6-69586282bd71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe13f2e2-7104-49e1-b327-51efea05fee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4d3840-425e-4bd7-98a6-69586282bd71",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "a276032a-1cc1-4abd-a8a0-b11ebec53991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "43a3bcdf-26c3-44ab-996e-f9848911661e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a276032a-1cc1-4abd-a8a0-b11ebec53991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f737ea-d611-4e59-b132-a42db306c1f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a276032a-1cc1-4abd-a8a0-b11ebec53991",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "53666a98-5aa8-4d80-89bd-f87615094877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "7a412fea-0fe4-4543-b3da-f364dad3295a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53666a98-5aa8-4d80-89bd-f87615094877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40b3da7-5aec-4503-89ae-e10ea813d310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53666a98-5aa8-4d80-89bd-f87615094877",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "37d78b82-8836-4880-819c-832041d29f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "cf2d4eeb-9a94-42d0-b6e7-3f61bdd1d964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d78b82-8836-4880-819c-832041d29f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c0e1bd-7545-4e54-91af-1df3123c6e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d78b82-8836-4880-819c-832041d29f3f",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "e766215f-54eb-4f39-8e27-328bb7ef2fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "bb81f829-2385-47fd-967b-edd8a1eb8600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e766215f-54eb-4f39-8e27-328bb7ef2fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444296c5-71a5-44d4-a0da-ede0d0a6d2f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e766215f-54eb-4f39-8e27-328bb7ef2fe1",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "118e2d4d-acfb-41cf-a769-c4e7464d1336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "e4a485e2-482a-4f31-8b81-a5c40e4acc9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "118e2d4d-acfb-41cf-a769-c4e7464d1336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "554fa515-c8ef-42a8-b966-8f30a91d380c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "118e2d4d-acfb-41cf-a769-c4e7464d1336",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "c5679a96-9a69-43c7-9df4-36e32e72689f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "01c80572-27f8-466b-9d3f-992cd9e0b01b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5679a96-9a69-43c7-9df4-36e32e72689f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b146e7d-b4fe-4586-9e26-feb350787796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5679a96-9a69-43c7-9df4-36e32e72689f",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "756e9282-22d8-46a8-9ddc-ea0f1576aa2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "a767c427-e87c-46d6-b19d-b4e8bbd15ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "756e9282-22d8-46a8-9ddc-ea0f1576aa2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd6ae69d-4ad0-4ef0-a1f8-11f5c30cd90d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "756e9282-22d8-46a8-9ddc-ea0f1576aa2e",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "a9588982-e5e2-47a2-a04e-a476d689394b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "d00af133-f38c-472b-be2c-45d78449f8e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9588982-e5e2-47a2-a04e-a476d689394b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef946ce3-73e9-4c74-a668-d7a9a31ee037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9588982-e5e2-47a2-a04e-a476d689394b",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "fd9764a4-900f-4389-9282-77ab25dde5a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "4ee95677-beb8-40ad-9f04-7c5c4f7fefd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9764a4-900f-4389-9282-77ab25dde5a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29421f5b-2b11-47f6-a561-c69620d1b488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9764a4-900f-4389-9282-77ab25dde5a0",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "47b6f011-8050-47bc-bf80-6e537389e83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "548078f7-d8e6-4239-bd40-fe788eab5cfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b6f011-8050-47bc-bf80-6e537389e83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7628fd79-36b1-49de-9888-e7d7f94355bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b6f011-8050-47bc-bf80-6e537389e83a",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "f83c4657-9bf3-471b-9037-8e515213a6a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "d622c340-fb6f-4643-9a9f-b0ad9efa2496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83c4657-9bf3-471b-9037-8e515213a6a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3871ca-622b-42f2-ab5c-57f7532f8438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83c4657-9bf3-471b-9037-8e515213a6a2",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "b4a76ddf-a08b-4e78-ac81-c50af2448fa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "c017b4f2-a054-4ac0-9aaa-3cabff5996d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a76ddf-a08b-4e78-ac81-c50af2448fa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "792d1c29-a465-4dcf-8501-66b031ad7972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a76ddf-a08b-4e78-ac81-c50af2448fa7",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "c82aa515-6812-4af0-b507-ae819091ed5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "2c26d6bd-66ca-4e01-bf4d-24d0d173f34f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82aa515-6812-4af0-b507-ae819091ed5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76836b50-d422-4dff-8f0b-32bd20f395cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82aa515-6812-4af0-b507-ae819091ed5e",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "4bca8bd3-85b3-4d96-a656-512ebffdf99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "9b5e8281-c72a-4734-a7ac-ef9dfb8d7bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bca8bd3-85b3-4d96-a656-512ebffdf99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960dcb1b-fed6-4dc9-ba6c-2fa6dd6802a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bca8bd3-85b3-4d96-a656-512ebffdf99a",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "476b6a6b-f2d9-4767-a676-f8f98fe5bf52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "9cb019a4-34f6-405b-b889-7f97347b5547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "476b6a6b-f2d9-4767-a676-f8f98fe5bf52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f924e2-3f95-4d6d-b5d9-c9db89545784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "476b6a6b-f2d9-4767-a676-f8f98fe5bf52",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "78ed8715-a645-4e8a-8500-4bceedd2b590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "9de96d5a-039d-41ad-906e-68923953fcc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78ed8715-a645-4e8a-8500-4bceedd2b590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692803a0-4942-4185-ac16-d36f01e785b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78ed8715-a645-4e8a-8500-4bceedd2b590",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "d0785250-a983-46a0-869a-5ea6a34226e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "0a6bf867-72e1-4b30-86d3-5538270d0c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0785250-a983-46a0-869a-5ea6a34226e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fcba92f-28c1-4925-813b-cae60fd982d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0785250-a983-46a0-869a-5ea6a34226e2",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "01390c4b-26ef-45e3-96e1-094a024eb71b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "250a76ba-b4ec-413a-94bc-fcad70def005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01390c4b-26ef-45e3-96e1-094a024eb71b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822883a6-5ddc-49f5-ae4e-bd5e40f00fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01390c4b-26ef-45e3-96e1-094a024eb71b",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "698632c1-d3f3-464a-8377-2235617af7e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "6fd3ab67-62a3-49ef-9cec-da83f1ebbb06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698632c1-d3f3-464a-8377-2235617af7e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2cdf54-9efd-4f90-88ef-a1f39c8eb1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698632c1-d3f3-464a-8377-2235617af7e7",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "a39fb35f-ff96-41a0-83c5-a80ef94e9c3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "42cb9d5b-f1b4-4b84-a715-a3a3d037fb18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a39fb35f-ff96-41a0-83c5-a80ef94e9c3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93721ad1-9017-4e89-bd10-d3471cd7725c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a39fb35f-ff96-41a0-83c5-a80ef94e9c3e",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "573fd56a-6d01-4556-80f5-6873860f6ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "e2661c2c-4e48-42c3-981e-0fe23164cc03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573fd56a-6d01-4556-80f5-6873860f6ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bbaef17-e9d8-420a-9668-abdacfb25d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573fd56a-6d01-4556-80f5-6873860f6ef4",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "fbe50023-cb52-45de-af9a-492aa210b0be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "30e81a40-8969-46f6-8f46-a4ee06da41e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe50023-cb52-45de-af9a-492aa210b0be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95bd49b-9d25-4f01-961f-683badafde6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe50023-cb52-45de-af9a-492aa210b0be",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "f8ef9d68-df8b-492f-8ea0-a11d3b6f110b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "15813fa9-c501-4147-875d-3c972aef2901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ef9d68-df8b-492f-8ea0-a11d3b6f110b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5fa7e72-78dd-4274-98b8-e04f5fa13c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ef9d68-df8b-492f-8ea0-a11d3b6f110b",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "c8ec4ef9-41ab-4487-a01b-b2d8ef17fef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "c0fc089e-7ba2-4fba-a0df-649a43fb0118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ec4ef9-41ab-4487-a01b-b2d8ef17fef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c4785b-7162-4f84-8765-5b15eeb04135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ec4ef9-41ab-4487-a01b-b2d8ef17fef3",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "1e1e75f8-705b-4d88-8e47-fbf5d1807fb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "071d973b-c777-4bf4-acfd-bfcdc32f5330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e1e75f8-705b-4d88-8e47-fbf5d1807fb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4e286c-054e-4c12-ada8-9dedbabd2f76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e1e75f8-705b-4d88-8e47-fbf5d1807fb8",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        },
        {
            "id": "19736249-7658-40c5-b573-683bfbf2e158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "compositeImage": {
                "id": "6550bbab-0072-4a03-b9c2-92d8c23ab9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19736249-7658-40c5-b573-683bfbf2e158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0964e9-a1fd-479b-87c8-54f56a710f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19736249-7658-40c5-b573-683bfbf2e158",
                    "LayerId": "da53da05-8496-48e4-88b9-d21a2fcf636e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "da53da05-8496-48e4-88b9-d21a2fcf636e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b40e27e6-4f07-4a6d-a8d2-9a7859e746f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}