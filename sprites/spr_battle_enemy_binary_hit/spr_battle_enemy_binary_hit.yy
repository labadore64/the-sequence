{
    "id": "47d480bb-ba59-47f2-9dcf-b0c01647139f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_binary_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 5,
    "bbox_right": 251,
    "bbox_top": 117,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a8e6235-5905-4fa6-a6ec-6b4998d41f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47d480bb-ba59-47f2-9dcf-b0c01647139f",
            "compositeImage": {
                "id": "388eda4a-1332-4a91-b1cf-5e276aea3752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8e6235-5905-4fa6-a6ec-6b4998d41f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ad4926-5949-4bee-97e8-f9f32371098c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8e6235-5905-4fa6-a6ec-6b4998d41f44",
                    "LayerId": "790a2a86-9b2a-48d7-aacd-bdf6e1b4d979"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "790a2a86-9b2a-48d7-aacd-bdf6e1b4d979",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47d480bb-ba59-47f2-9dcf-b0c01647139f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6efab5bc-3ed9-4097-8e3a-b423e88d6ca0",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}