{
    "id": "6141d57f-5779-4fa0-9aa9-ba987fb537fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_retroskates",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 31,
    "bbox_right": 228,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0255e7db-2cda-4af0-b609-87ef116437b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6141d57f-5779-4fa0-9aa9-ba987fb537fc",
            "compositeImage": {
                "id": "5c9c95b1-73f8-40f0-ae0b-3a56d3b8bcbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0255e7db-2cda-4af0-b609-87ef116437b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ef8f95-4465-4115-859b-810ddadbf058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0255e7db-2cda-4af0-b609-87ef116437b7",
                    "LayerId": "f5ca20e5-d7dd-4854-8415-321778ac4655"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f5ca20e5-d7dd-4854-8415-321778ac4655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6141d57f-5779-4fa0-9aa9-ba987fb537fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}