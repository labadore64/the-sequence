{
    "id": "1c05bcfe-d5b1-4b8c-95c1-44ca3ba535bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_physguard_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "112740f5-fc0d-44ee-8dad-5841b92edf26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c05bcfe-d5b1-4b8c-95c1-44ca3ba535bf",
            "compositeImage": {
                "id": "0c6ec16a-c98f-44c1-9835-e9e026e72d36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112740f5-fc0d-44ee-8dad-5841b92edf26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83a7dc0-f3a8-43fc-b1d3-7fb3e868200d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112740f5-fc0d-44ee-8dad-5841b92edf26",
                    "LayerId": "26fb963f-bf88-4833-949b-2399d70e4acb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "26fb963f-bf88-4833-949b-2399d70e4acb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c05bcfe-d5b1-4b8c-95c1-44ca3ba535bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}