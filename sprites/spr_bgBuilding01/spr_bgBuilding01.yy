{
    "id": "c28bd073-8b58-4a8e-ab35-10a4cfded528",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bgBuilding01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 321,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25a5ec3b-81a4-4126-acb0-a269e4f5e873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c28bd073-8b58-4a8e-ab35-10a4cfded528",
            "compositeImage": {
                "id": "bc5a94d0-0c8c-4aad-aecc-b61501544e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a5ec3b-81a4-4126-acb0-a269e4f5e873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c90aa3-8c9c-4efb-be01-dad9e2af3a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a5ec3b-81a4-4126-acb0-a269e4f5e873",
                    "LayerId": "1c43526f-04d0-479d-ad21-941f77dd1f8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "1c43526f-04d0-479d-ad21-941f77dd1f8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c28bd073-8b58-4a8e-ab35-10a4cfded528",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "c2964b5e-7657-43f8-90d7-9771fa335c9c",
    "type": 0,
    "width": 322,
    "xorig": 0,
    "yorig": 0
}