{
    "id": "5f4af999-cea3-4800-8fed-7beff41c12ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_fridgeSink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 7,
    "bbox_right": 117,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "851fd447-1ac1-479f-9e3d-b4052213c818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f4af999-cea3-4800-8fed-7beff41c12ae",
            "compositeImage": {
                "id": "546f8a54-6a97-4b88-b6fe-669ca6a8d653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "851fd447-1ac1-479f-9e3d-b4052213c818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e85e345-9616-4d7b-8301-a2214d15dbc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "851fd447-1ac1-479f-9e3d-b4052213c818",
                    "LayerId": "02f58774-1147-4505-ac03-c0ef7b43e1d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "02f58774-1147-4505-ac03-c0ef7b43e1d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f4af999-cea3-4800-8fed-7beff41c12ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}