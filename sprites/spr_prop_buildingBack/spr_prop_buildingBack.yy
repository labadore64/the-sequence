{
    "id": "ce7f3388-8a05-43b1-b586-0f5912056a15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_buildingBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 8,
    "bbox_right": 120,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8fe13c6-1db6-44bc-9e53-f21559d5a0db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce7f3388-8a05-43b1-b586-0f5912056a15",
            "compositeImage": {
                "id": "d6a94c53-7fe9-4de4-b442-2a77436b3fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8fe13c6-1db6-44bc-9e53-f21559d5a0db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5a9457-d44c-4722-a97b-3c083203c79b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8fe13c6-1db6-44bc-9e53-f21559d5a0db",
                    "LayerId": "f5786de2-bb28-44d0-b6f7-74f3d7c0c559"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f5786de2-bb28-44d0-b6f7-74f3d7c0c559",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce7f3388-8a05-43b1-b586-0f5912056a15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}