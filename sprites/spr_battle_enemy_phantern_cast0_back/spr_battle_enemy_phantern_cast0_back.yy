{
    "id": "c2179df0-16e3-4955-8db7-8c524e03f351",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_cast0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 64,
    "bbox_right": 170,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7881482-b8e7-43b8-a841-9eda9541dddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2179df0-16e3-4955-8db7-8c524e03f351",
            "compositeImage": {
                "id": "0d80332e-7fed-4291-a159-5653def4db3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7881482-b8e7-43b8-a841-9eda9541dddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d56a1911-5ee9-424a-ba5b-8250e4d843e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7881482-b8e7-43b8-a841-9eda9541dddd",
                    "LayerId": "278cf879-7b23-4643-ab5a-4e6c9f3a052b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "278cf879-7b23-4643-ab5a-4e6c9f3a052b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2179df0-16e3-4955-8db7-8c524e03f351",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}