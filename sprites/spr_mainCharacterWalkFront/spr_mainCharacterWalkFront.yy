{
    "id": "8a90a3a2-222f-433c-8047-834fd41410a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainCharacterWalkFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 88,
    "bbox_right": 166,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "378009b6-4eac-4599-8493-ddf2cd5e75c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a90a3a2-222f-433c-8047-834fd41410a4",
            "compositeImage": {
                "id": "1f084420-b880-4ec4-9c94-97e77cfbc265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "378009b6-4eac-4599-8493-ddf2cd5e75c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f24deb3-4580-47ca-a451-35e8361af71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "378009b6-4eac-4599-8493-ddf2cd5e75c5",
                    "LayerId": "a64a1814-cf48-4341-a928-a9b239557c43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a64a1814-cf48-4341-a928-a9b239557c43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a90a3a2-222f-433c-8047-834fd41410a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}