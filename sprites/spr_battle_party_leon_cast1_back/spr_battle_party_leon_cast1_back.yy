{
    "id": "9d43c389-d1e1-4572-8310-1ec37ddb74bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_cast1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 33,
    "bbox_right": 227,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f74276b-011b-438f-a876-6750608173dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d43c389-d1e1-4572-8310-1ec37ddb74bf",
            "compositeImage": {
                "id": "4a20f78c-f06c-4d81-a430-568a8446a0a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f74276b-011b-438f-a876-6750608173dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623ff368-475f-4774-aae3-ba6f55807b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f74276b-011b-438f-a876-6750608173dc",
                    "LayerId": "1b0dedd9-5342-46cd-a78e-0d521fa5b2ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1b0dedd9-5342-46cd-a78e-0d521fa5b2ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d43c389-d1e1-4572-8310-1ec37ddb74bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}