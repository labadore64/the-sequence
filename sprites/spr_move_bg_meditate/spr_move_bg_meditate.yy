{
    "id": "ba0f3be2-28cf-445e-846e-3c5f398e9075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_meditate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea84e5ae-81bf-40fc-b606-82832af1b630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba0f3be2-28cf-445e-846e-3c5f398e9075",
            "compositeImage": {
                "id": "b3280fed-3aa7-410b-8074-adfceeabc7a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea84e5ae-81bf-40fc-b606-82832af1b630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5361c19-39a8-4659-ada5-c4e52976844e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea84e5ae-81bf-40fc-b606-82832af1b630",
                    "LayerId": "30a9d2a1-827e-4751-acad-501b0b0f9a35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "30a9d2a1-827e-4751-acad-501b0b0f9a35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba0f3be2-28cf-445e-846e-3c5f398e9075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.50025,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}