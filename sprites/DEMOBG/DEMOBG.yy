{
    "id": "ef916f15-9928-44db-948a-ea1330eabe62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "DEMOBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 800,
    "bbox_left": 0,
    "bbox_right": 473,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a43d141-abc8-4ed4-ad87-a440dd8af4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef916f15-9928-44db-948a-ea1330eabe62",
            "compositeImage": {
                "id": "cc39d8ec-b9bf-464f-b498-295c81af32f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a43d141-abc8-4ed4-ad87-a440dd8af4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc5105e-b5f3-41fe-bc47-e59770fe06b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a43d141-abc8-4ed4-ad87-a440dd8af4b0",
                    "LayerId": "a73c7531-7ef5-418d-a897-f131251ad2d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 801,
    "layers": [
        {
            "id": "a73c7531-7ef5-418d-a897-f131251ad2d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef916f15-9928-44db-948a-ea1330eabe62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 474,
    "xorig": 0,
    "yorig": 0
}