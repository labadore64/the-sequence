{
    "id": "34c69bcb-8547-45be-b1a7-aa445e0f9c28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_utah_attack_front1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 240,
    "bbox_left": 63,
    "bbox_right": 205,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ddd6530-692e-478a-a961-9576441de425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c69bcb-8547-45be-b1a7-aa445e0f9c28",
            "compositeImage": {
                "id": "1fb5955b-c80f-4e2b-973e-61c93b993d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ddd6530-692e-478a-a961-9576441de425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6d0a4d-9fcd-41e3-994e-057178ff7fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ddd6530-692e-478a-a961-9576441de425",
                    "LayerId": "99a17895-cdcd-44df-a37e-9df7dc7ecd17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "99a17895-cdcd-44df-a37e-9df7dc7ecd17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34c69bcb-8547-45be-b1a7-aa445e0f9c28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}