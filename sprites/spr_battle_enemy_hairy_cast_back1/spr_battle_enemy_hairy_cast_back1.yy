{
    "id": "3f9ec573-0f6f-4a99-80c9-7120c13b90f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_cast_back1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 41,
    "bbox_right": 210,
    "bbox_top": 130,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a47a6bf-3841-4026-86b0-97bf112790ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f9ec573-0f6f-4a99-80c9-7120c13b90f2",
            "compositeImage": {
                "id": "8ec24e13-cb80-4353-a74b-598bd192d829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a47a6bf-3841-4026-86b0-97bf112790ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50f0789-a7c7-489e-a375-6adaa04d7799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a47a6bf-3841-4026-86b0-97bf112790ca",
                    "LayerId": "f33c8fed-4a05-4098-ac01-b2b082bc3512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f33c8fed-4a05-4098-ac01-b2b082bc3512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f9ec573-0f6f-4a99-80c9-7120c13b90f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}