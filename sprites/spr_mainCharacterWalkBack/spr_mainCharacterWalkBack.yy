{
    "id": "3e1c4786-473c-4e62-9361-51758a26afe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainCharacterWalkBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 244,
    "bbox_left": 88,
    "bbox_right": 167,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0db4163-089e-4a19-acf0-44092a510f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e1c4786-473c-4e62-9361-51758a26afe0",
            "compositeImage": {
                "id": "d40c3aac-06a3-404f-870f-45de421e4954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0db4163-089e-4a19-acf0-44092a510f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "910b7195-fb64-4722-9f5e-f37a2517f1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0db4163-089e-4a19-acf0-44092a510f96",
                    "LayerId": "c4b48d54-9fbe-4071-9556-9244736f35b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c4b48d54-9fbe-4071-9556-9244736f35b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e1c4786-473c-4e62-9361-51758a26afe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}