{
    "id": "5a808f22-d78b-432b-a31c-81cfde4e71b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dd02ffe-fafd-4a9c-8368-acf371329f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a808f22-d78b-432b-a31c-81cfde4e71b9",
            "compositeImage": {
                "id": "ca1704ac-b40e-4b8f-b1f0-d88f64c2abdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd02ffe-fafd-4a9c-8368-acf371329f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86728856-88df-415b-aff6-757c7fc6d4ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd02ffe-fafd-4a9c-8368-acf371329f23",
                    "LayerId": "6744d7cc-31c6-4951-aa8c-b327f4935e72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "6744d7cc-31c6-4951-aa8c-b327f4935e72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a808f22-d78b-432b-a31c-81cfde4e71b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}