{
    "id": "b5004512-92b8-4b05-9ff3-7684a9083f04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_vcrOff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 9,
    "bbox_right": 117,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bc37cb3-b4a1-4cc2-82b9-c8a3f5516fb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5004512-92b8-4b05-9ff3-7684a9083f04",
            "compositeImage": {
                "id": "ddc4d3e0-7094-4cdc-bcce-60c41033387f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bc37cb3-b4a1-4cc2-82b9-c8a3f5516fb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1726a754-ee5e-4cfe-b982-6ef05d771e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bc37cb3-b4a1-4cc2-82b9-c8a3f5516fb5",
                    "LayerId": "efab9f54-728c-47c7-85c4-a1d856c3caeb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "efab9f54-728c-47c7-85c4-a1d856c3caeb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5004512-92b8-4b05-9ff3-7684a9083f04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}