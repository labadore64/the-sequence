{
    "id": "21a97f15-1cb9-4521-9467-9dba55fa3c9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 14,
    "bbox_right": 207,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d4e397e-2a15-47ed-afbc-6f43324c31ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21a97f15-1cb9-4521-9467-9dba55fa3c9b",
            "compositeImage": {
                "id": "58ed8a90-3c55-4c4a-a63d-413d3789255b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4e397e-2a15-47ed-afbc-6f43324c31ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8739190-cef9-478d-a6a0-1aa80f5b7e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4e397e-2a15-47ed-afbc-6f43324c31ec",
                    "LayerId": "9cb3d0c6-7481-4420-a6b5-a345d1e14af2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9cb3d0c6-7481-4420-a6b5-a345d1e14af2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21a97f15-1cb9-4521-9467-9dba55fa3c9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}