{
    "id": "64e10657-1aa1-41eb-883e-b6f25fd931ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_fallenBranch2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 11,
    "bbox_right": 119,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "078cbfa9-7df2-4687-bed8-85b7d352021b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e10657-1aa1-41eb-883e-b6f25fd931ad",
            "compositeImage": {
                "id": "8252a7b1-10e1-47c8-88b7-2a378d302980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "078cbfa9-7df2-4687-bed8-85b7d352021b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed5c9f8-364f-4d89-8496-fcc29b272134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "078cbfa9-7df2-4687-bed8-85b7d352021b",
                    "LayerId": "7c557a98-e745-4263-8bdc-a28af428d835"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7c557a98-e745-4263-8bdc-a28af428d835",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64e10657-1aa1-41eb-883e-b6f25fd931ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}