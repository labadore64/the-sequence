{
    "id": "924650bf-3cf7-41ea-94c7-352e1f820c37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 37,
    "bbox_right": 201,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c39c6bfc-b413-4b1f-92f9-cae8934ef148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "924650bf-3cf7-41ea-94c7-352e1f820c37",
            "compositeImage": {
                "id": "534d7639-2b2c-437f-b76f-3d65fb86c7a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c39c6bfc-b413-4b1f-92f9-cae8934ef148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d23b7c2-e9aa-4595-b29c-cd9cc99a1578",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c39c6bfc-b413-4b1f-92f9-cae8934ef148",
                    "LayerId": "d6fa9c18-da83-4f69-9ae6-92ad7febd2c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d6fa9c18-da83-4f69-9ae6-92ad7febd2c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "924650bf-3cf7-41ea-94c7-352e1f820c37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}