{
    "id": "d0f7d6a6-c0a6-4297-a887-546ee08b7135",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_mikeDeskItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 8,
    "bbox_right": 115,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "093da645-4da8-40a5-8ef7-30c267649577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0f7d6a6-c0a6-4297-a887-546ee08b7135",
            "compositeImage": {
                "id": "9817eb94-7a04-4fd7-bd4f-9ab2bdd368e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093da645-4da8-40a5-8ef7-30c267649577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69479392-9e42-4580-b4a9-f2e42d834fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093da645-4da8-40a5-8ef7-30c267649577",
                    "LayerId": "a1c6c51f-68f1-4da7-94bf-3b1f5fba6753"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a1c6c51f-68f1-4da7-94bf-3b1f5fba6753",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0f7d6a6-c0a6-4297-a887-546ee08b7135",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}