{
    "id": "5135bd1b-f4b9-4dfc-ba44-24b5946cd8a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_cerberpy_attack_front1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 53,
    "bbox_right": 175,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d25032bc-4817-408a-8dc5-16b0be758713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5135bd1b-f4b9-4dfc-ba44-24b5946cd8a8",
            "compositeImage": {
                "id": "b213c1fa-0506-43dc-a653-e2da1e91e192",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d25032bc-4817-408a-8dc5-16b0be758713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4c332a5-25a9-470f-9e9c-7026365279e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d25032bc-4817-408a-8dc5-16b0be758713",
                    "LayerId": "2da9ad7c-6963-4591-ade1-8c94616cc4ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2da9ad7c-6963-4591-ade1-8c94616cc4ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5135bd1b-f4b9-4dfc-ba44-24b5946cd8a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}