{
    "id": "76d5108b-b96b-4c8b-8e41-f6fa86fd329a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 42,
    "bbox_right": 245,
    "bbox_top": 109,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fe6bcaa-a946-4be2-9371-9ff8a7481250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76d5108b-b96b-4c8b-8e41-f6fa86fd329a",
            "compositeImage": {
                "id": "b8f4af6f-175f-486c-beb8-264e14d153d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe6bcaa-a946-4be2-9371-9ff8a7481250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c66e6c-45d3-47a2-bde7-5bebca94a449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe6bcaa-a946-4be2-9371-9ff8a7481250",
                    "LayerId": "16ebda60-3bf7-400b-9425-0d16c9e3806f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "16ebda60-3bf7-400b-9425-0d16c9e3806f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76d5108b-b96b-4c8b-8e41-f6fa86fd329a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}