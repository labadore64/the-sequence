{
    "id": "d5258ea1-15a2-46ef-8479-768796869347",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sparkleGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 55,
    "bbox_right": 73,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2b6de77-c888-415b-bf93-4bf6ecbd87d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5258ea1-15a2-46ef-8479-768796869347",
            "compositeImage": {
                "id": "e709afce-c32e-499e-80b5-6e48e9b62496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b6de77-c888-415b-bf93-4bf6ecbd87d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f622990a-b7d9-417d-8bff-3006845c815d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b6de77-c888-415b-bf93-4bf6ecbd87d4",
                    "LayerId": "cfb8bb28-17e1-4e9c-baa4-dcc4c68a8054"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cfb8bb28-17e1-4e9c-baa4-dcc4c68a8054",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5258ea1-15a2-46ef-8479-768796869347",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}