{
    "id": "bc8c052f-ce90-42b6-8c43-5159bd0fb7e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_attack_back1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 61,
    "bbox_right": 216,
    "bbox_top": 103,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec3bb19c-c58d-4c66-8fcd-96912a5c36cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc8c052f-ce90-42b6-8c43-5159bd0fb7e5",
            "compositeImage": {
                "id": "0e39a2f2-dee7-431f-99b6-494be703b628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3bb19c-c58d-4c66-8fcd-96912a5c36cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c58eeb8c-078e-4767-a6a0-010c7afff640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3bb19c-c58d-4c66-8fcd-96912a5c36cc",
                    "LayerId": "fa67d545-b7c1-4964-910a-69b66c6114a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fa67d545-b7c1-4964-910a-69b66c6114a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc8c052f-ce90-42b6-8c43-5159bd0fb7e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}