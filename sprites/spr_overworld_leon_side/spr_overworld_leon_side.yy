{
    "id": "9430c7de-dd62-4a77-97b5-f20fa11dd2cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_overworld_leon_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 32,
    "bbox_right": 229,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5b5f99a-d784-4b34-98a8-3d0ef6177f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9430c7de-dd62-4a77-97b5-f20fa11dd2cc",
            "compositeImage": {
                "id": "8ed32fa7-29ee-45f1-bbf9-756738408c8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b5f99a-d784-4b34-98a8-3d0ef6177f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88161c22-f03e-44d6-80eb-f749c5b26f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b5f99a-d784-4b34-98a8-3d0ef6177f46",
                    "LayerId": "6ce3f2ad-d61f-4cc8-8552-6e0ce4447864"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6ce3f2ad-d61f-4cc8-8552-6e0ce4447864",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9430c7de-dd62-4a77-97b5-f20fa11dd2cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 145
}