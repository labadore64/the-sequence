{
    "id": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 15,
    "bbox_right": 251,
    "bbox_top": 114,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13203212-6ed9-43b0-add9-005a392bf1a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "3dc04006-7768-4f91-90f9-7db0ad0aafe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13203212-6ed9-43b0-add9-005a392bf1a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "647e4b31-8f25-4b26-937b-5dd3fd974140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13203212-6ed9-43b0-add9-005a392bf1a4",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "fc858940-ad7e-433a-95ea-d9ea36bcf900",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "816ecffb-8e94-4a02-9207-b496e3e50392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc858940-ad7e-433a-95ea-d9ea36bcf900",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9cb86e-76fc-4c67-be40-ab53cc814bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc858940-ad7e-433a-95ea-d9ea36bcf900",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "f3bb2080-2cc5-40d3-b616-b0cf78ef42e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "cac6c84b-1b2f-4a14-a049-3bbe564c8946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3bb2080-2cc5-40d3-b616-b0cf78ef42e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d6100cf-682b-4593-a10b-9273514510f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3bb2080-2cc5-40d3-b616-b0cf78ef42e5",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "fb3dc6d4-ecac-4960-8f16-cedbc6b30c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "dc86630d-6b93-4bf0-879a-632b4558640f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb3dc6d4-ecac-4960-8f16-cedbc6b30c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d74f99bc-daef-4ff2-9cb6-c8c8b368cffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb3dc6d4-ecac-4960-8f16-cedbc6b30c53",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "ef4b27ef-f5ac-4d60-bac4-105599de75b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "2a859a03-9eda-43a9-a893-826eee39cb63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4b27ef-f5ac-4d60-bac4-105599de75b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff539dd1-815f-430b-9fc1-815e8788be4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4b27ef-f5ac-4d60-bac4-105599de75b0",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "437024e5-af84-4498-9b9a-dec89f38c5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "7404169b-1109-458b-9e11-3a0e77c64190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "437024e5-af84-4498-9b9a-dec89f38c5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc4d6cd-52d1-4a58-9b98-2c2be1e4b019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "437024e5-af84-4498-9b9a-dec89f38c5b0",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "69fc52c2-0ea2-4f2d-8a8b-a0dfcc67c483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "13f5b299-1550-4296-a711-0ca4a4e49936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69fc52c2-0ea2-4f2d-8a8b-a0dfcc67c483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1d4b7a-2f0d-4635-a87a-66ff93900d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69fc52c2-0ea2-4f2d-8a8b-a0dfcc67c483",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "2dc39716-f3ab-4a34-ba5b-c9c030b346f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "30c23eed-5208-4664-9c88-975026c4176b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dc39716-f3ab-4a34-ba5b-c9c030b346f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0812d37-4c7a-4105-a318-0edfb30bcf4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dc39716-f3ab-4a34-ba5b-c9c030b346f6",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "dce8ff89-abb5-49c2-95e6-bde23b470d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "1dba95b3-3aad-4b70-87e6-0fec527527b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce8ff89-abb5-49c2-95e6-bde23b470d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfede51d-41e1-4829-ab90-8fe364e2007c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce8ff89-abb5-49c2-95e6-bde23b470d57",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "85d3d073-3192-41c3-94d3-a49357d393d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "949005b2-1112-4de5-a0f6-20b50fb194c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d3d073-3192-41c3-94d3-a49357d393d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b0560a-236a-43c8-ba22-2f1b92118a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d3d073-3192-41c3-94d3-a49357d393d6",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "c19934f4-4c3d-42da-ae6e-69ff65d83d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "f71b57a7-b941-4280-b605-4984bd2dae72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19934f4-4c3d-42da-ae6e-69ff65d83d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe2eea6-2e87-4be1-b297-7e9c0925791a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19934f4-4c3d-42da-ae6e-69ff65d83d2f",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "63362a23-c762-4653-8506-487a7db41c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "ba2c234b-d167-48f0-987d-a2ad3485b259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63362a23-c762-4653-8506-487a7db41c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a4c12b4-35f8-4224-abb2-f3898d7a932f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63362a23-c762-4653-8506-487a7db41c5a",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "65a75f5d-10de-4cc3-80af-2d874bdf9680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "73328f92-3484-411a-b304-b451b0a3d10c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a75f5d-10de-4cc3-80af-2d874bdf9680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ee3dd6-b80e-4d48-b2d7-863980b0d856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a75f5d-10de-4cc3-80af-2d874bdf9680",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "c33232bb-fa04-41c9-bb8e-5e14202e3440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "24b43b1d-51d4-4e59-b097-29f05aa28443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33232bb-fa04-41c9-bb8e-5e14202e3440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b93e22a5-6769-4c35-9c7b-af2fa3471958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33232bb-fa04-41c9-bb8e-5e14202e3440",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "02695d3e-bb8f-4e5a-aa5b-c3f6e1d5bde0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "881d9744-0aa5-49c2-9459-7823e52c0b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02695d3e-bb8f-4e5a-aa5b-c3f6e1d5bde0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4729c8aa-55c4-4110-b5bb-3480834c4f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02695d3e-bb8f-4e5a-aa5b-c3f6e1d5bde0",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "cabe060c-f16c-4609-b842-6d442b0c3ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "658aa5c1-4f11-4399-8499-ec79d302fc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cabe060c-f16c-4609-b842-6d442b0c3ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86dd4e0d-ebe3-42a8-bcee-132d64671255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cabe060c-f16c-4609-b842-6d442b0c3ae9",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "d3a84783-e320-45ee-826c-405f3d01868b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "b6df98ef-ca8c-4b36-b79f-f097be22ea37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a84783-e320-45ee-826c-405f3d01868b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4d07859-83b0-47bc-988b-8a7aa6f1c316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a84783-e320-45ee-826c-405f3d01868b",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "d31b6fa8-2218-4eb7-bf72-edacec33083f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "e070c48a-fee4-4fb9-940b-fae5c31c0f74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d31b6fa8-2218-4eb7-bf72-edacec33083f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "166788a4-0574-4ed4-918f-1e132ec0980e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d31b6fa8-2218-4eb7-bf72-edacec33083f",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "e0024426-295c-492b-b13f-45ed1d6f0e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "82bec3e0-dab3-4ff8-90d9-20e0db95b6f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0024426-295c-492b-b13f-45ed1d6f0e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33660036-e720-4ffb-b076-7587e05e14e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0024426-295c-492b-b13f-45ed1d6f0e20",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "3a660a73-8927-4e9d-95ef-2d2d50a7c928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "c7b3c9e1-896c-4d38-b990-05644b833e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a660a73-8927-4e9d-95ef-2d2d50a7c928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c874c75-cc01-4911-a610-5801623a1e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a660a73-8927-4e9d-95ef-2d2d50a7c928",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "06d4b3a4-1528-4e2f-8cd2-77078bfe7db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "c1d8d1d3-d498-46bc-81de-ca3f9d69ad53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d4b3a4-1528-4e2f-8cd2-77078bfe7db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d96e93a-43be-4caa-b564-a52a45eb44a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d4b3a4-1528-4e2f-8cd2-77078bfe7db3",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "6d09e26e-900a-471e-af12-193da3840ec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "12544309-9a0c-43a7-90cc-7c59f3ab4dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d09e26e-900a-471e-af12-193da3840ec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "818a1e87-8267-48b8-ae5f-d040bc07d0fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d09e26e-900a-471e-af12-193da3840ec0",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "6f1fcec1-a139-409b-bf34-b47b697fc8af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "ed68830c-19c2-4dac-b671-4db3fed1aafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f1fcec1-a139-409b-bf34-b47b697fc8af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673fbbd1-f2aa-4dc9-8211-d6f92fed4b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f1fcec1-a139-409b-bf34-b47b697fc8af",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "48d983bb-b2df-413b-a759-a82e06f5dcf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "c98f9482-b2f7-44ce-a67a-605678f33bd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d983bb-b2df-413b-a759-a82e06f5dcf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af320676-f0e1-4f1e-9bee-b1b7274dd40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d983bb-b2df-413b-a759-a82e06f5dcf5",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "6ca292e8-ac36-417d-b9f5-e4aee9ad34b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "2a0d29c2-73f6-4b4e-9f6e-f4c962ddd671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca292e8-ac36-417d-b9f5-e4aee9ad34b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c99e0ea-2815-4ed5-b597-7859931ee717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca292e8-ac36-417d-b9f5-e4aee9ad34b2",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "03dda85d-eeb7-4337-9b76-c1c86e6b3785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "eb12bdc2-2b2f-4b05-837d-913d9611050a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03dda85d-eeb7-4337-9b76-c1c86e6b3785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37a57ff5-902d-44e9-afc5-d9a2ca1679df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03dda85d-eeb7-4337-9b76-c1c86e6b3785",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "93bd7a66-1dbd-4ce3-9a9e-38c178314638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "dde0692b-ad4d-4b8c-92cb-95b3f9d7a604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93bd7a66-1dbd-4ce3-9a9e-38c178314638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c0c2a7-c179-4286-9a49-20e6c9eeb2e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bd7a66-1dbd-4ce3-9a9e-38c178314638",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "31954131-c7d4-40b7-b9ac-2ad7c1f05bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "4590093e-777e-4b66-b8e8-5f33c1151cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31954131-c7d4-40b7-b9ac-2ad7c1f05bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "272e3add-ba40-4626-9483-c94dee9c511f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31954131-c7d4-40b7-b9ac-2ad7c1f05bc3",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "65d75b6d-f0bc-4ad7-a787-b35e86bb16d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "4143082e-96d8-4382-9917-d67fe37c5db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d75b6d-f0bc-4ad7-a787-b35e86bb16d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96a50cb-526c-46b3-b149-a41a8d43b0a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d75b6d-f0bc-4ad7-a787-b35e86bb16d9",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "56fdd418-524a-442c-beb1-f699abd761cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "7d0d6d64-4c95-43d8-97b2-21f0c9a95d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56fdd418-524a-442c-beb1-f699abd761cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc81c46d-acdf-482d-8833-d556e3f31378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56fdd418-524a-442c-beb1-f699abd761cc",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "ba3ceedf-b41b-4c59-9bc7-b196626a3fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "d9056579-f91e-4c0f-9518-ab8a6d101bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3ceedf-b41b-4c59-9bc7-b196626a3fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4504e108-6aba-4583-8b81-55b3f0e3fd00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3ceedf-b41b-4c59-9bc7-b196626a3fc9",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "69063402-ea8f-4866-911d-695afdf89d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "92fb3038-f0f2-4c63-8ba6-dd430153714f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69063402-ea8f-4866-911d-695afdf89d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f46462-ef10-47d8-aaa3-1119c74e0494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69063402-ea8f-4866-911d-695afdf89d27",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "d985a3f6-265e-443d-894c-9b0bca06611b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "75c6c2bd-c45c-40a7-bf97-d5e25085b6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d985a3f6-265e-443d-894c-9b0bca06611b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "809a261a-dc20-4404-9324-026b0c44a2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d985a3f6-265e-443d-894c-9b0bca06611b",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "880559e1-63b9-42ff-8c8c-f9204f856de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "5a38091b-f661-4053-8207-a659af7bbbfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880559e1-63b9-42ff-8c8c-f9204f856de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e8d0ab-b2d1-42fc-974d-ad3e98e977a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880559e1-63b9-42ff-8c8c-f9204f856de1",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "d479701b-bfc4-4dd2-93d6-8da537e82652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "f1b6d01c-8ccd-40eb-87fc-7abd5151851d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d479701b-bfc4-4dd2-93d6-8da537e82652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0495219c-ce23-40f8-a65d-a734827d76a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d479701b-bfc4-4dd2-93d6-8da537e82652",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "a71de825-7b01-4732-9113-490f2c539dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "cb4ca4da-e05f-4d47-ba41-df05883191c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a71de825-7b01-4732-9113-490f2c539dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3147ea-0818-4ce2-a77b-5d69c14beca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a71de825-7b01-4732-9113-490f2c539dae",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "9a76d823-ef58-4481-99b4-46b5fc11dbe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "4014d0b4-09d9-40f3-be4e-baa268a9598a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a76d823-ef58-4481-99b4-46b5fc11dbe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2d5f3dc-6899-475d-8a97-fa5054318cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a76d823-ef58-4481-99b4-46b5fc11dbe1",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "9cd20a13-beda-4c8e-b909-e0cff340da51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "f1947b93-4b91-4770-95a1-97d2a1559270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd20a13-beda-4c8e-b909-e0cff340da51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc319fa2-8311-43ca-9a24-ce0ad0ee3d9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd20a13-beda-4c8e-b909-e0cff340da51",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "d86a8ae3-ca86-41a9-b468-9784a965f124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "1baf640c-2f7f-4b81-b812-5f3d991e65a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d86a8ae3-ca86-41a9-b468-9784a965f124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5450358-c5c5-42f4-9fca-8dcd187ce881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d86a8ae3-ca86-41a9-b468-9784a965f124",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        },
        {
            "id": "3f4f7cc6-57fa-45b7-80b5-b4c3d44e8342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "compositeImage": {
                "id": "b62ebd43-9573-40e2-ad04-f4af8e724c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f4f7cc6-57fa-45b7-80b5-b4c3d44e8342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "663615ff-96c2-4892-9053-e4ab5c875979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f4f7cc6-57fa-45b7-80b5-b4c3d44e8342",
                    "LayerId": "69f119f9-4e7a-4208-b07a-284e19010a07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "69f119f9-4e7a-4208-b07a-284e19010a07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "497ee3fe-349b-4384-a2ec-435bb4fd70f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}