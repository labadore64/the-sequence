{
    "id": "1bd7c343-b081-48d7-af10-cc17b816482f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_fg_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28b526b6-4027-432e-a880-539da8a80bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bd7c343-b081-48d7-af10-cc17b816482f",
            "compositeImage": {
                "id": "ff9df6d3-3bd5-4921-9653-8ba1e67aaf84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b526b6-4027-432e-a880-539da8a80bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60b79baf-6f21-439f-bb84-b0660befb3fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b526b6-4027-432e-a880-539da8a80bee",
                    "LayerId": "e86f83fd-129d-436b-8ae1-9d52cdd5c334"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "e86f83fd-129d-436b-8ae1-9d52cdd5c334",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bd7c343-b081-48d7-af10-cc17b816482f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}