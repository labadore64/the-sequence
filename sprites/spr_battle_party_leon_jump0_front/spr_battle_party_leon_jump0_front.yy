{
    "id": "8863d3a1-d73f-4f41-a7e8-2ae2d9922965",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 22,
    "bbox_right": 211,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "791ca5fc-ca52-4482-ad77-05525929633e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8863d3a1-d73f-4f41-a7e8-2ae2d9922965",
            "compositeImage": {
                "id": "5f91fd7c-4abd-444c-bb7f-efef68e3c109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "791ca5fc-ca52-4482-ad77-05525929633e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f832c6d1-c316-412a-90e4-43f539a5be59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "791ca5fc-ca52-4482-ad77-05525929633e",
                    "LayerId": "acc28406-a6da-4e74-af8c-89436f0507ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "acc28406-a6da-4e74-af8c-89436f0507ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8863d3a1-d73f-4f41-a7e8-2ae2d9922965",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}