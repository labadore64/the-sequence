{
    "id": "f791b599-0793-407d-a584-5783653aeb53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "030bc36f-dd8d-418c-b684-b69e78ceb3ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f791b599-0793-407d-a584-5783653aeb53",
            "compositeImage": {
                "id": "40ffaadb-9f7a-4024-b365-ba07c6ce7e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "030bc36f-dd8d-418c-b684-b69e78ceb3ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "258a9eb7-5796-4da8-bab9-b40ae8c60985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "030bc36f-dd8d-418c-b684-b69e78ceb3ce",
                    "LayerId": "ba811ba1-499a-418b-8767-f033f20946fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ba811ba1-499a-418b-8767-f033f20946fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f791b599-0793-407d-a584-5783653aeb53",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}