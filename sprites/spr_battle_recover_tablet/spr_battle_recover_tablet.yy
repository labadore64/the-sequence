{
    "id": "0a19ae8d-5590-492d-82fc-2ac7d07823e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_recover_tablet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3baa1eca-261f-4ffc-9fa6-4419a05bbd5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a19ae8d-5590-492d-82fc-2ac7d07823e4",
            "compositeImage": {
                "id": "1aa23a1c-eda7-4c19-b8ce-29bbcd41d2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3baa1eca-261f-4ffc-9fa6-4419a05bbd5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0061c2-6194-494b-a39b-c27ca4e06ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3baa1eca-261f-4ffc-9fa6-4419a05bbd5d",
                    "LayerId": "39a705c3-e9c8-442a-a529-28ae6acce9df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "39a705c3-e9c8-442a-a529-28ae6acce9df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a19ae8d-5590-492d-82fc-2ac7d07823e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}