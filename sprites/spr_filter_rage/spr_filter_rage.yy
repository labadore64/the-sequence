{
    "id": "670d6906-3b86-482c-9a7e-11d783f17418",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_rage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 122,
    "bbox_right": 199,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d547e46-596a-4c19-a46b-70994a14a7d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "670d6906-3b86-482c-9a7e-11d783f17418",
            "compositeImage": {
                "id": "57bc8967-9fea-4d80-8515-d1ebb583f272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d547e46-596a-4c19-a46b-70994a14a7d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de022f3-ab7d-48f9-a547-c0fd0921707e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d547e46-596a-4c19-a46b-70994a14a7d5",
                    "LayerId": "365db7cc-d1f3-4f43-9191-029df428b20e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "365db7cc-d1f3-4f43-9191-029df428b20e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "670d6906-3b86-482c-9a7e-11d783f17418",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 160
}