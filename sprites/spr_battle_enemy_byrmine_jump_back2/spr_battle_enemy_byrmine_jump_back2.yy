{
    "id": "050fba99-fc6f-415f-b03e-1f381813e0e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_byrmine_jump_back2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 244,
    "bbox_left": 32,
    "bbox_right": 223,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3554c5c-7aa2-4fe9-8669-832491430388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050fba99-fc6f-415f-b03e-1f381813e0e0",
            "compositeImage": {
                "id": "d5b33887-2803-4717-b7d3-0f931a82b188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3554c5c-7aa2-4fe9-8669-832491430388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ec85be-fc61-427e-a14c-930c8796ae25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3554c5c-7aa2-4fe9-8669-832491430388",
                    "LayerId": "7e23130c-43cd-4c33-8b4f-2f3cb7900b61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7e23130c-43cd-4c33-8b4f-2f3cb7900b61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "050fba99-fc6f-415f-b03e-1f381813e0e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}