{
    "id": "2805a570-31c5-466b-be92-23bb1f6b0533",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sparkleYellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 55,
    "bbox_right": 73,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d4ba954-ecff-4351-96c3-99345a9fde3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2805a570-31c5-466b-be92-23bb1f6b0533",
            "compositeImage": {
                "id": "c5d1123b-c57d-4be7-b6cc-d636c122fda5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4ba954-ecff-4351-96c3-99345a9fde3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619a202e-f0de-4426-85d3-5c7665f1a246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4ba954-ecff-4351-96c3-99345a9fde3e",
                    "LayerId": "bba41d5d-6e91-42a7-8989-c71dc9dfb151"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bba41d5d-6e91-42a7-8989-c71dc9dfb151",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2805a570-31c5-466b-be92-23bb1f6b0533",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}