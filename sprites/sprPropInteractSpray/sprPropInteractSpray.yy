{
    "id": "6480ebb6-837a-4179-9c6e-93f68c821409",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPropInteractSpray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfdb3a0e-51a4-4293-97ec-d2a9476d5b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6480ebb6-837a-4179-9c6e-93f68c821409",
            "compositeImage": {
                "id": "dafb48e6-02a1-480c-baa7-6858d21b5b59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfdb3a0e-51a4-4293-97ec-d2a9476d5b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe313ef6-9afb-4b1d-80ed-9b662e0af2f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfdb3a0e-51a4-4293-97ec-d2a9476d5b42",
                    "LayerId": "53b58e1b-a3a4-4820-8237-330454026a1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "53b58e1b-a3a4-4820-8237-330454026a1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6480ebb6-837a-4179-9c6e-93f68c821409",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 32,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}