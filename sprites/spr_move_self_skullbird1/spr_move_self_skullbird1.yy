{
    "id": "8f343f57-c7bd-450b-8ebe-2af5071e19ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_self_skullbird1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0213f57-f09b-42fb-9b2c-bbce4ff69eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f343f57-c7bd-450b-8ebe-2af5071e19ed",
            "compositeImage": {
                "id": "1902f447-73c3-47c3-9656-ea58131a68fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0213f57-f09b-42fb-9b2c-bbce4ff69eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b081652-2fd5-427f-b759-549bb2819513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0213f57-f09b-42fb-9b2c-bbce4ff69eb3",
                    "LayerId": "b66730b6-9e5d-4870-8113-c7161cda9d7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b66730b6-9e5d-4870-8113-c7161cda9d7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f343f57-c7bd-450b-8ebe-2af5071e19ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}