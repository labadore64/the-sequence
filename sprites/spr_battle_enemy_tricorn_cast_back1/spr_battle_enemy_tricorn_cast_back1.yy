{
    "id": "047f2701-1210-4a92-bbdf-3ef08a4b017e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_cast_back1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 248,
    "bbox_left": 33,
    "bbox_right": 239,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d91d9447-1bbc-412f-91f4-179f3cae28dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "047f2701-1210-4a92-bbdf-3ef08a4b017e",
            "compositeImage": {
                "id": "06de4524-931a-4dcf-a380-c97ae0d7f7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91d9447-1bbc-412f-91f4-179f3cae28dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5403e360-e4dd-47d4-9994-f818d71141fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91d9447-1bbc-412f-91f4-179f3cae28dd",
                    "LayerId": "bf053dbf-8026-4cb8-83d2-abe989520ec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "bf053dbf-8026-4cb8-83d2-abe989520ec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "047f2701-1210-4a92-bbdf-3ef08a4b017e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}