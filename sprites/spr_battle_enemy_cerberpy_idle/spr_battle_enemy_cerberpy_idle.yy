{
    "id": "1534a026-77b4-49e7-aa37-d512567d5c37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_cerberpy_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 66,
    "bbox_right": 221,
    "bbox_top": 104,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b879438-dc84-4f96-b67a-ca53efca1144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "9b98a76c-f1ad-462d-8fc1-2d5f75c78c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b879438-dc84-4f96-b67a-ca53efca1144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c444cf-99e8-4f03-81c0-fdbcb6d85e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b879438-dc84-4f96-b67a-ca53efca1144",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "1a39fe6a-afd1-4a83-9d11-78228e411237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "83e023a5-95de-410c-b37a-4aff8ff9422c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a39fe6a-afd1-4a83-9d11-78228e411237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59a3cc4-520e-4a32-aacc-9aae70191067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a39fe6a-afd1-4a83-9d11-78228e411237",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "811c6265-a3b6-4158-9e53-1a7450d18a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "57ba09b8-fe90-4be3-b639-bba1b4002e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811c6265-a3b6-4158-9e53-1a7450d18a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd2a34a7-585f-42a1-9e95-7adf4dfdd49f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811c6265-a3b6-4158-9e53-1a7450d18a7f",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "d862f635-7b32-4675-895f-b5bb39c3b23c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "aea688b4-1f43-4c3a-9a8f-614a94b68b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d862f635-7b32-4675-895f-b5bb39c3b23c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c3ded6-e4f4-403d-8e13-bd7e93e7b4c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d862f635-7b32-4675-895f-b5bb39c3b23c",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "1f1d07a1-69a8-403a-95e1-396ed6592709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "183fd4de-3daa-46d6-8de2-7b3e24b6d22a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f1d07a1-69a8-403a-95e1-396ed6592709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3999b38-c1f8-48aa-a836-5a345138af44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f1d07a1-69a8-403a-95e1-396ed6592709",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "1b083840-2f70-4361-a171-63766f41c2e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "4fd133bc-eb04-483c-9031-b34a89047b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b083840-2f70-4361-a171-63766f41c2e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9fb00da-b477-4977-97fd-9e27f04ff5aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b083840-2f70-4361-a171-63766f41c2e1",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "ed909ab2-c1a8-4e47-a3b3-17d8c0ea150e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "29c75399-a9f6-44ee-9a5f-373073120cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed909ab2-c1a8-4e47-a3b3-17d8c0ea150e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e87b5e-9941-4fa0-89a9-3e934092139b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed909ab2-c1a8-4e47-a3b3-17d8c0ea150e",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "e59d9ece-12e4-4ee9-b0af-c80a9f5e186c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "6c7a5781-6245-4dc2-962a-feaad6075cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e59d9ece-12e4-4ee9-b0af-c80a9f5e186c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06daaf4c-f279-47ac-b756-27f3ffc230c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e59d9ece-12e4-4ee9-b0af-c80a9f5e186c",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "e1d58f91-f902-427f-93ad-726da51888dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "de8ecbee-36f2-4753-a395-a3715553fe66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d58f91-f902-427f-93ad-726da51888dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68aea56f-cc92-486e-a3c6-0e0387360c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d58f91-f902-427f-93ad-726da51888dd",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "fedf579b-0aa3-4d36-9648-6a645cb6523a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "91102627-35ce-4cf5-8b2e-9ec02c9e58b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedf579b-0aa3-4d36-9648-6a645cb6523a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f2a36c5-3629-4a48-a4ea-7f149c5ec74c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedf579b-0aa3-4d36-9648-6a645cb6523a",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "095be3d4-d94b-49ae-9af4-ed6eed8f19fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "89e8e541-7893-42dd-b4be-838468f2515c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095be3d4-d94b-49ae-9af4-ed6eed8f19fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3569bf91-7459-4761-bbfb-44f67f498a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095be3d4-d94b-49ae-9af4-ed6eed8f19fe",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "7c8452f7-21fc-4ad1-b85b-1870e716d60a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "19971527-7c45-423f-a980-b376c0da7ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8452f7-21fc-4ad1-b85b-1870e716d60a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c9465e-47f9-4b4e-9090-f094dcdc02cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8452f7-21fc-4ad1-b85b-1870e716d60a",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "4066c84e-205f-47c7-a357-932429b6c9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "02eabfaa-27f3-4294-8cac-1a9b785f1ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4066c84e-205f-47c7-a357-932429b6c9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c574da3f-4218-47f8-8ede-180fe56b979b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4066c84e-205f-47c7-a357-932429b6c9dd",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "2d4382dd-80dd-4dea-aeae-3f5fb33ba26d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "a80ad3ea-edb7-4a44-8e2e-773d42040850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4382dd-80dd-4dea-aeae-3f5fb33ba26d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186578ec-1d1e-4f93-a1d0-3f84641d8442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4382dd-80dd-4dea-aeae-3f5fb33ba26d",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "9cb5be4e-8f5c-4585-980d-eea781d86c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "6f1013f5-04bf-4353-bee4-2b3b0be0f48b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cb5be4e-8f5c-4585-980d-eea781d86c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7952c94-6d69-49f0-834e-fdb2456750de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cb5be4e-8f5c-4585-980d-eea781d86c37",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "9555f09e-c1ec-4090-acd2-940b334bd484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "8dd6be9a-ce96-436f-9aed-d91e9b87c037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9555f09e-c1ec-4090-acd2-940b334bd484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca1b22fd-9693-49fa-aed4-8dfdaaf2ec27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9555f09e-c1ec-4090-acd2-940b334bd484",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "a82badf9-51d1-4431-86cc-b2302753bb6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "38958754-ff92-48a0-b1e8-ba2943145442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a82badf9-51d1-4431-86cc-b2302753bb6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f405a8-f56b-4c12-97bd-afaf1479445a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a82badf9-51d1-4431-86cc-b2302753bb6b",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "962f6947-0070-4afd-a3f4-8c4fe0a8696d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "d3af1b3c-6493-40ba-a18c-d2d1eb9db10f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "962f6947-0070-4afd-a3f4-8c4fe0a8696d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c33540-4d8e-4438-a020-8fd6e33e9607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "962f6947-0070-4afd-a3f4-8c4fe0a8696d",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "5b77fca2-a066-45a7-8b7c-47df54acd84a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "685647a2-51e0-4e58-9d61-c266b1ff182f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b77fca2-a066-45a7-8b7c-47df54acd84a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83a3744-bb78-43dc-a87f-00d416ae4d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b77fca2-a066-45a7-8b7c-47df54acd84a",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "8509c408-c8a4-4336-81bf-8fb3df568bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "7eaaac34-1e0e-4c59-a0e8-88b66a809baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8509c408-c8a4-4336-81bf-8fb3df568bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2bd7099-b07a-4d37-b313-5e71aea44143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8509c408-c8a4-4336-81bf-8fb3df568bb1",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "6df5490d-3d77-486a-97da-35e347a1855d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "4c4962ff-d48e-4317-97be-da9e6cd24d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df5490d-3d77-486a-97da-35e347a1855d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "300cf6f1-9507-40e4-8b66-4982bf97570f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df5490d-3d77-486a-97da-35e347a1855d",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        },
        {
            "id": "f3a13d77-cbce-478d-9a6d-60c241d1dc74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "compositeImage": {
                "id": "31ac39fb-6a5c-4527-9c53-8225ec7e6026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a13d77-cbce-478d-9a6d-60c241d1dc74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "636282f6-4062-4a1a-9a0d-63d6822439ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a13d77-cbce-478d-9a6d-60c241d1dc74",
                    "LayerId": "715bb308-e14a-49d1-a3da-91b6eb642afd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "715bb308-e14a-49d1-a3da-91b6eb642afd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1534a026-77b4-49e7-aa37-d512567d5c37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}