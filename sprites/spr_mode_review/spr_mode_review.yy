{
    "id": "d0f91a95-94b2-491a-92d7-6de60ca7e1f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mode_review",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11a75575-c055-4621-b6ea-5ac75c0ac981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0f91a95-94b2-491a-92d7-6de60ca7e1f2",
            "compositeImage": {
                "id": "0584fea0-909c-4870-a2c5-84559be4497b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a75575-c055-4621-b6ea-5ac75c0ac981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c5e2fa-477c-4e07-8587-fe8d739cb9d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a75575-c055-4621-b6ea-5ac75c0ac981",
                    "LayerId": "4dc84bef-c35a-446f-8fc0-4a03d40b3918"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4dc84bef-c35a-446f-8fc0-4a03d40b3918",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0f91a95-94b2-491a-92d7-6de60ca7e1f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}