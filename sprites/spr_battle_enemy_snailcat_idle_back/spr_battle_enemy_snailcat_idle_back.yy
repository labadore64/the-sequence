{
    "id": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snailcat_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 5,
    "bbox_right": 232,
    "bbox_top": 93,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cf2bbec-53dd-4aaf-afcb-d2bfc570e19d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "4994dcb5-c406-419c-a58e-4583cf974ed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf2bbec-53dd-4aaf-afcb-d2bfc570e19d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea788f58-8c93-4e18-b70d-6d9f6f9f379b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf2bbec-53dd-4aaf-afcb-d2bfc570e19d",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "2d6d83e0-a46d-4bc3-b76b-6d4f08500a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "0fbe4359-e4bd-4cea-ac2e-7543a2a36265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6d83e0-a46d-4bc3-b76b-6d4f08500a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73681001-1545-4920-ab5f-06d06c80e006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6d83e0-a46d-4bc3-b76b-6d4f08500a90",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "7089f6b3-cfb4-4027-aa3f-f9133aae2a4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "c127aa0e-b11f-4faf-a7f0-a8b21d5ae9fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7089f6b3-cfb4-4027-aa3f-f9133aae2a4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba5ad8b0-cb54-4c29-a3b6-ae7c26e2923a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7089f6b3-cfb4-4027-aa3f-f9133aae2a4e",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "71fb9660-2602-4e71-9540-9b0d0fc87617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "b99c7f55-3208-45e5-87d0-f62d97c256c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71fb9660-2602-4e71-9540-9b0d0fc87617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5a0893-d553-4998-80b6-1b7d1b68420d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71fb9660-2602-4e71-9540-9b0d0fc87617",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "14c3be2c-22aa-4aa8-8594-db68268355c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "f1c4d129-cb43-433a-9557-093922d57ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c3be2c-22aa-4aa8-8594-db68268355c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3197d47a-bc5a-4254-9f0c-a0106533ae2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c3be2c-22aa-4aa8-8594-db68268355c4",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "98f12a1d-bf6a-4b80-9ba4-b287cd0cf430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "40807808-7a96-4618-bf8c-293646db355d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f12a1d-bf6a-4b80-9ba4-b287cd0cf430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3faf69-b359-4f5f-b5dd-34a575437475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f12a1d-bf6a-4b80-9ba4-b287cd0cf430",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "5a756ef8-e984-4a48-a855-a9dff1ed4f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "be8e401b-ea95-4ae2-b5a2-a95ddeab6a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a756ef8-e984-4a48-a855-a9dff1ed4f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4508c9-10e8-4aab-a729-79126e1cf5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a756ef8-e984-4a48-a855-a9dff1ed4f92",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "72bd27e3-666d-4917-afc0-855f4a0ee8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "4c7893a1-93cd-4a70-b9bf-a7fcf32c5eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72bd27e3-666d-4917-afc0-855f4a0ee8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab0883eb-ec41-47bf-8874-848de00d7cd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72bd27e3-666d-4917-afc0-855f4a0ee8c0",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "82457379-16f1-4d1f-a50d-8f2b2e0f5c7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "4ccc4d68-9e78-4851-a129-fbb14e6e0385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82457379-16f1-4d1f-a50d-8f2b2e0f5c7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8698a4f-ccbc-49e1-bc1d-96ff1d2f9b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82457379-16f1-4d1f-a50d-8f2b2e0f5c7e",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "4d4ef9d2-3f6a-400a-b5c7-5206eae543c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "ee79aa71-bb90-4c07-a4ee-d4a4f0270103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d4ef9d2-3f6a-400a-b5c7-5206eae543c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4262122-c54d-4858-b186-ef49105bdc5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d4ef9d2-3f6a-400a-b5c7-5206eae543c1",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "e880e244-5d6c-4f20-8d4c-5a5a06086f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "036a0e41-1d9c-4b9d-aece-a049a6eaac38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e880e244-5d6c-4f20-8d4c-5a5a06086f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe9d13ed-54c4-4515-9718-d32d1e5fc2c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e880e244-5d6c-4f20-8d4c-5a5a06086f96",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "a651e3f5-fb95-41e3-bacf-370f3b558c42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "43e60501-4324-4a94-b307-0462abb40fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a651e3f5-fb95-41e3-bacf-370f3b558c42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785ab8cd-aae2-4b2e-9cc1-aa7aada0fe16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a651e3f5-fb95-41e3-bacf-370f3b558c42",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "c4e32fad-a54b-4bb2-b708-1c020d972601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "01018653-3b2b-43db-b1c8-1ce2b297a3ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e32fad-a54b-4bb2-b708-1c020d972601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdb8a096-6035-47c7-a2af-b4b5f4eb430c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e32fad-a54b-4bb2-b708-1c020d972601",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "67e5e0df-dcaa-4684-8906-f0fe0ca8ed63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "1dbb5c9e-346c-4dea-933c-8a82c846d061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e5e0df-dcaa-4684-8906-f0fe0ca8ed63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b061980-d8e3-4cdd-b9a3-4fc2292b5799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e5e0df-dcaa-4684-8906-f0fe0ca8ed63",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "1f2c449b-999b-4475-8886-6e98b1a83dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "f99412f2-6a9b-4cd4-b9cc-9d2fc7c75fda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2c449b-999b-4475-8886-6e98b1a83dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b3ede23-d6ec-4365-bb15-7b3c427130d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2c449b-999b-4475-8886-6e98b1a83dfd",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "aa447adb-2072-43ea-88b7-a90bf8f788af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "d3d81030-cdad-4a0b-9c93-8d46ce91cb98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa447adb-2072-43ea-88b7-a90bf8f788af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03316757-1b91-40cf-8732-6b5991ac8dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa447adb-2072-43ea-88b7-a90bf8f788af",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "5ceb2aa9-4fa5-49ba-a090-c9decb5ab56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "6d8e7bd6-5331-4224-8f46-4f8a9add1038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ceb2aa9-4fa5-49ba-a090-c9decb5ab56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7c816e-2965-4f6c-ae6c-829f707b814c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ceb2aa9-4fa5-49ba-a090-c9decb5ab56f",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "2e4a5977-abd7-4e4a-b5d1-5d37320e0801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "cb785820-5856-4322-bd1a-3fdfe19208f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4a5977-abd7-4e4a-b5d1-5d37320e0801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f756b522-c022-4efa-ad51-750b0ce04238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4a5977-abd7-4e4a-b5d1-5d37320e0801",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "94db1db9-0c94-4429-9314-cf4508d7410c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "8c496f5b-ed65-4fdb-8c8f-9353138d1efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94db1db9-0c94-4429-9314-cf4508d7410c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f15128-b483-4e61-867e-c33b1e0a7de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94db1db9-0c94-4429-9314-cf4508d7410c",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "0cd87659-ba3a-4077-8d00-7ca8b086e8ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "8d178946-afca-4476-89f4-0404ff5b1d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd87659-ba3a-4077-8d00-7ca8b086e8ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38f5029-a297-496b-807b-e035c5affb42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd87659-ba3a-4077-8d00-7ca8b086e8ce",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "ccd8483e-2772-48ef-b750-652df1c9288e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "9661cef9-19f5-452a-8467-e4ce1954aa72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd8483e-2772-48ef-b750-652df1c9288e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b8188a-710f-4866-80e9-d22b80857626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd8483e-2772-48ef-b750-652df1c9288e",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "f7a6a273-1879-4e88-bcb6-cbda8d89b4ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "a3a1e22f-9429-46bc-b21d-2821d1e5d775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7a6a273-1879-4e88-bcb6-cbda8d89b4ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e3d4fe2-8b13-4543-9888-eeb0c6554b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7a6a273-1879-4e88-bcb6-cbda8d89b4ac",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "7c8f18f5-862e-4e49-ac1f-26874558df58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "c06a0a96-8f7e-4ccb-86ac-a14a87198b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8f18f5-862e-4e49-ac1f-26874558df58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b28934f5-11ca-4f81-9c2b-911d472ba5c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8f18f5-862e-4e49-ac1f-26874558df58",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "c288b7ac-dfff-4020-a796-cbbce65a1f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "c16e0c51-7b82-4b08-801d-36daff9970c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c288b7ac-dfff-4020-a796-cbbce65a1f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def592ec-75af-4184-8c1e-6c220acd8ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c288b7ac-dfff-4020-a796-cbbce65a1f7c",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "cd694ffd-acc8-41c7-ba4a-e058c50b7eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "463654b4-b115-44ef-9d1e-4f4e01a960f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd694ffd-acc8-41c7-ba4a-e058c50b7eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c52a0b02-56f9-4d34-90e1-0c39901d1590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd694ffd-acc8-41c7-ba4a-e058c50b7eac",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "b0ec2017-7327-4993-8410-92482fc1c6ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "0fe74ff9-8a2e-4043-a9de-f12e764b16a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ec2017-7327-4993-8410-92482fc1c6ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc4d826-7894-46ff-82d3-fe1ceb6506ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ec2017-7327-4993-8410-92482fc1c6ef",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "d73c08cb-4e63-44ac-9db8-0dbaac8692a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "c6eae957-9f87-4265-b205-984dd3d00735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73c08cb-4e63-44ac-9db8-0dbaac8692a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8334e2d0-0366-4de6-a1c5-f215f21889c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73c08cb-4e63-44ac-9db8-0dbaac8692a2",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "310eb95d-31f1-4763-a253-5ec5e7e65240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "6b9dd468-64bd-4751-8577-aa303ec7a3d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310eb95d-31f1-4763-a253-5ec5e7e65240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afea0209-44bc-4ac1-af15-736a58354ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310eb95d-31f1-4763-a253-5ec5e7e65240",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "ab5b9d71-5967-412a-9403-951b3fd96480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "f32b19d0-ff99-4bc2-a59d-0d2aac66afe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab5b9d71-5967-412a-9403-951b3fd96480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6300ace9-da68-443f-a11a-a33bbbd8d853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab5b9d71-5967-412a-9403-951b3fd96480",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "ade169f9-1386-49f8-b164-30048497e5b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "e001e2bc-24e1-4a47-81e0-4cce9f6901e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ade169f9-1386-49f8-b164-30048497e5b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12232048-4499-4600-b670-56b0f233aff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ade169f9-1386-49f8-b164-30048497e5b6",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "1ff7511a-9178-41aa-b2fd-8a7c8ec3a354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "94cc0527-f823-455b-8cb3-17082f8a01bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ff7511a-9178-41aa-b2fd-8a7c8ec3a354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf08bf8-c643-437b-a9e8-7a8c8341ad4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ff7511a-9178-41aa-b2fd-8a7c8ec3a354",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "83e304bf-f14a-4a97-b504-03ac1c98362c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "a310369c-0b0a-4bae-a915-9d787f3357f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e304bf-f14a-4a97-b504-03ac1c98362c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b14482d0-315a-4055-a581-74a4fc042969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e304bf-f14a-4a97-b504-03ac1c98362c",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "23d682eb-db13-4131-8fb6-cf70c498eb30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "95ad4058-d11c-4730-8bab-a21adf40b7d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d682eb-db13-4131-8fb6-cf70c498eb30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52591ee-3c09-4712-babf-fd7ca355ccb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d682eb-db13-4131-8fb6-cf70c498eb30",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "fdb21722-7040-4131-b4d5-5099785a0b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "394f34d1-2472-4780-ac50-ac3ba97821a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb21722-7040-4131-b4d5-5099785a0b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9699d54-58bc-43e9-bbaf-9c66eb32ebaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb21722-7040-4131-b4d5-5099785a0b48",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "1f98cd27-3453-4b58-a9a1-575ea756d112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "87f42a0c-1815-41e7-b0e4-289bc6ff2c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f98cd27-3453-4b58-a9a1-575ea756d112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcad55fa-21c3-4602-a896-baf13794acca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f98cd27-3453-4b58-a9a1-575ea756d112",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "8c8510a1-145e-4d0e-97b7-365329fe55cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "6767c9cd-5154-4f61-b681-45c552b51867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8510a1-145e-4d0e-97b7-365329fe55cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "842b5635-9fdb-4aef-b1d2-f14e8f6e1ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8510a1-145e-4d0e-97b7-365329fe55cb",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "380dc885-cced-4688-973c-ca36f4d58030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "7ef9b158-fc63-48f1-a5f1-1dd2d921c2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380dc885-cced-4688-973c-ca36f4d58030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b06ad08c-f79b-4580-a4e7-d9758427369b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380dc885-cced-4688-973c-ca36f4d58030",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "3de9c7a9-9d81-4a52-b448-748f5f387786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "df1059f1-87f0-447a-b205-12b4c29a6799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de9c7a9-9d81-4a52-b448-748f5f387786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd211e34-d134-4759-94c4-2abcc609d49a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de9c7a9-9d81-4a52-b448-748f5f387786",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "abae2c4e-334e-427b-96f6-cbec2f3f998d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "5fddadc1-447e-4929-8665-c9651882b08f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abae2c4e-334e-427b-96f6-cbec2f3f998d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f90d8d7c-f3c8-4348-aa9d-0419e77225bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abae2c4e-334e-427b-96f6-cbec2f3f998d",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "3682e070-59c7-43ca-a608-4e66f84e66c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "7ced0add-a5f9-4e0d-8862-e99bced04c86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3682e070-59c7-43ca-a608-4e66f84e66c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda29fff-357d-4df4-8de1-306f7e5552db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3682e070-59c7-43ca-a608-4e66f84e66c4",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "4c3928f4-001b-4904-affa-7f7d4acdc3ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "b042a6c1-93a3-4268-bf5a-b310e5eef3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c3928f4-001b-4904-affa-7f7d4acdc3ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93e3db9-1a07-4cdb-88a2-dbbd43a293b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c3928f4-001b-4904-affa-7f7d4acdc3ab",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "09681c99-a34b-4a3c-be75-427af32ff528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "3949ed3b-18af-47a4-872a-7da763f8679a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09681c99-a34b-4a3c-be75-427af32ff528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239f4e18-5f5d-43cc-bf1f-b590d27d21c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09681c99-a34b-4a3c-be75-427af32ff528",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "237cd026-e722-481c-91e6-0c1729cd390b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "4c24c314-0107-464f-bb9f-be45e8967282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237cd026-e722-481c-91e6-0c1729cd390b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99bbc05b-0b7e-4aab-af7e-956f8a6f36a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237cd026-e722-481c-91e6-0c1729cd390b",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "90db39e6-8d31-454f-a70a-940fcb494eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "ca2c04f4-ad08-481a-8871-4d236c261552",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90db39e6-8d31-454f-a70a-940fcb494eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe03077c-b486-4937-adfa-3e4df9b19ec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90db39e6-8d31-454f-a70a-940fcb494eba",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        },
        {
            "id": "28a9b5f6-b046-4023-bd6c-eee47f515b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "compositeImage": {
                "id": "683b9c61-9a9c-4c68-aa07-04cfcfb54374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a9b5f6-b046-4023-bd6c-eee47f515b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c4bdfd-78f7-48aa-8a9f-f54425028da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a9b5f6-b046-4023-bd6c-eee47f515b7d",
                    "LayerId": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9e0240c0-f5ef-44ef-8fd0-3b17e61c7e39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9b4fe35-7645-4b2c-9129-ac3075c82973",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}