{
    "id": "aae4cf08-74e8-45b5-91ae-42cf0f8f0c52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_fg_dipper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e20c5c4-8090-43eb-a2d6-b79c439e068f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aae4cf08-74e8-45b5-91ae-42cf0f8f0c52",
            "compositeImage": {
                "id": "5b135779-c38a-44c1-ae5e-7b2cbace5a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e20c5c4-8090-43eb-a2d6-b79c439e068f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e53462-e946-4448-8727-6eaf18493249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e20c5c4-8090-43eb-a2d6-b79c439e068f",
                    "LayerId": "311e7095-a93f-4240-9829-7f1e16927f64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "311e7095-a93f-4240-9829-7f1e16927f64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aae4cf08-74e8-45b5-91ae-42cf0f8f0c52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}