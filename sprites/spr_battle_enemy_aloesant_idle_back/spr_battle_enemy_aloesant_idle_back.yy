{
    "id": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 0,
    "bbox_right": 201,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c986d6e-6382-4548-b934-e7c6e58b93f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "a14a25e3-243c-4c9f-91ac-42743ee36f73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c986d6e-6382-4548-b934-e7c6e58b93f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dcc12a9-58a8-4d6c-9eda-f8b2d7fdc72b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c986d6e-6382-4548-b934-e7c6e58b93f7",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "50ec9bdb-23e8-4ee7-875d-610a42b890bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "c59bf6df-0414-47b3-b8b5-a1223015f254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ec9bdb-23e8-4ee7-875d-610a42b890bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51429123-12a5-4a51-a391-227f6fe37cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ec9bdb-23e8-4ee7-875d-610a42b890bb",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "c4ca9b45-680e-4658-9c68-902aa3b9c468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "4802e2a2-526a-47ce-932f-fcea6429f8d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ca9b45-680e-4658-9c68-902aa3b9c468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40018e8f-7293-465e-8963-ea075aa6f96d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ca9b45-680e-4658-9c68-902aa3b9c468",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "95b204d7-919b-41a0-9f16-6c1492bac7b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "de73dedf-ec15-45b7-a99f-157150e402bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b204d7-919b-41a0-9f16-6c1492bac7b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dffe90d5-699d-4fc7-abc1-a3425e77611b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b204d7-919b-41a0-9f16-6c1492bac7b9",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "af0a72f9-3331-409f-8c72-b8dd95db2947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "be8aa462-4f4a-4ac7-bada-193ab767c539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af0a72f9-3331-409f-8c72-b8dd95db2947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f64e90-42d0-4d70-966e-2ac6cd212613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af0a72f9-3331-409f-8c72-b8dd95db2947",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "8d999821-736c-472b-b34f-c5364793c804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "9dd97496-0b5f-4576-8d6d-f4a7967f1790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d999821-736c-472b-b34f-c5364793c804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e260df-25bc-47a4-affb-40b4fece5343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d999821-736c-472b-b34f-c5364793c804",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "59072eb7-1334-4b3d-bd0b-8a9cb8842ce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "a22fcc3e-6cb4-4cc0-9c03-4d0e16c451db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59072eb7-1334-4b3d-bd0b-8a9cb8842ce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cabda9ed-b186-48aa-b7ed-f82668b3c51d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59072eb7-1334-4b3d-bd0b-8a9cb8842ce1",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "86439bdc-f6c6-4354-9653-2461f5c80496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "f4e91c5a-7bdb-4be0-a0f1-6320c2c42a0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86439bdc-f6c6-4354-9653-2461f5c80496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e35024b-2c3f-40c5-952b-9befdbf44520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86439bdc-f6c6-4354-9653-2461f5c80496",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "e01a427a-b230-42e6-b253-7ca5184c2f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "24e461a5-4bda-490f-8e2b-076d2161ebf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e01a427a-b230-42e6-b253-7ca5184c2f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "278b4bf2-e111-4a0f-a7dc-1ed4921c29da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e01a427a-b230-42e6-b253-7ca5184c2f0c",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "a8d530f2-b835-4fc3-92a6-c05bd7a1c4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "1b512108-c668-489e-85c2-aa42ee8326f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d530f2-b835-4fc3-92a6-c05bd7a1c4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7869f915-70a9-4be4-8fea-b633fc84b363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d530f2-b835-4fc3-92a6-c05bd7a1c4a3",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "4a99e6aa-9e53-44b1-9d7e-a039bebb96b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "7c09ab42-9b36-405c-81d2-9f5cd6bad62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a99e6aa-9e53-44b1-9d7e-a039bebb96b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b26e649-da0c-4e63-8a2d-7ea32e721626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a99e6aa-9e53-44b1-9d7e-a039bebb96b8",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "96621844-8780-431a-9e64-0b0eaa3c3349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "25f964cf-5b66-48de-a993-86a6c121540a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96621844-8780-431a-9e64-0b0eaa3c3349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2d0deaf-fb0b-4ec5-b1df-77fbe926bec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96621844-8780-431a-9e64-0b0eaa3c3349",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "599a4a86-0f1e-49b4-a75e-b80404157b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "407e5db0-d7b4-4bfd-a557-6cef34c0e1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599a4a86-0f1e-49b4-a75e-b80404157b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b05d87a-bac2-4ed2-a4d9-39791fad2bb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599a4a86-0f1e-49b4-a75e-b80404157b9f",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "5cabf47e-9c2e-4cd1-ae74-8c50f365fa1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "b15826a3-b24b-4a2c-9191-2fce11c437ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cabf47e-9c2e-4cd1-ae74-8c50f365fa1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad67abd8-bbc7-45ec-b717-2835c5f6b05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cabf47e-9c2e-4cd1-ae74-8c50f365fa1e",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "afd31fff-2b9b-46ff-aeef-12b5b386b8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "e1625284-4499-4364-a1ba-da165e9aefa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd31fff-2b9b-46ff-aeef-12b5b386b8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b57da604-8340-470f-9950-02a1e370901a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd31fff-2b9b-46ff-aeef-12b5b386b8f0",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "35e852a1-728f-4ad8-94cc-d8a9cd825770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "822085f4-374b-457d-9c88-60a40daeb996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e852a1-728f-4ad8-94cc-d8a9cd825770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a25bb9f-ba69-4ade-8be1-bc7404f58d54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e852a1-728f-4ad8-94cc-d8a9cd825770",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "62f1461b-2f82-470e-80e0-48af8c86ae8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "3397b5d6-8003-4ddb-94e1-1b5c3cdc9ef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f1461b-2f82-470e-80e0-48af8c86ae8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f2e5d4-cb3d-44d0-9731-f2b14f9c250c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f1461b-2f82-470e-80e0-48af8c86ae8d",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "6b157d9b-8bbc-4d18-a86c-1f6971a6473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "c42130db-c23c-44d2-b11a-1fd2af51aff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b157d9b-8bbc-4d18-a86c-1f6971a6473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958c357d-2c8e-4a3a-869f-413031542f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b157d9b-8bbc-4d18-a86c-1f6971a6473a",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "d9962ccf-ad4e-4700-ae34-05659761a523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "956ca8eb-196b-4ba7-a1a3-3abdd911ba6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9962ccf-ad4e-4700-ae34-05659761a523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "307364e9-19a5-4b1b-be28-5e8d09fcdc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9962ccf-ad4e-4700-ae34-05659761a523",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "1c114a5f-9905-4316-844e-0310ae3150ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "cae00436-b7e4-4415-b50c-4df6e1987271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c114a5f-9905-4316-844e-0310ae3150ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0caea9f4-4a62-41f9-85fa-d2e36ef0c5f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c114a5f-9905-4316-844e-0310ae3150ac",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "8b3c966b-9065-4d03-987a-1f9b4383ab80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "c17af1dd-b704-4468-8d05-401b754a0712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3c966b-9065-4d03-987a-1f9b4383ab80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce264afc-cd6c-44ba-8dc6-377284082b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3c966b-9065-4d03-987a-1f9b4383ab80",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "8273592c-8bc4-438e-abb3-27ab9a360c69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "712f41e5-a92e-4374-b441-8186d1ab66d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8273592c-8bc4-438e-abb3-27ab9a360c69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "447dfc83-b919-41e7-ae2c-a01204d296fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8273592c-8bc4-438e-abb3-27ab9a360c69",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "3193aaa3-aefd-4d50-835b-630c6d000bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "4c794731-beeb-47b7-ad4c-7204c5282653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3193aaa3-aefd-4d50-835b-630c6d000bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909e7b79-3a2f-499d-85f2-0938b642cf28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3193aaa3-aefd-4d50-835b-630c6d000bbb",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "fcdb31ee-f6fd-4716-b7f5-8c769a434ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "2ab89287-2ca4-4bf3-a198-d90d501cb4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcdb31ee-f6fd-4716-b7f5-8c769a434ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cff0d35-ca2f-45f1-a65f-8e99ebc254b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcdb31ee-f6fd-4716-b7f5-8c769a434ce4",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "e05976c9-220c-4526-9999-32ed02d4368c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "41a1274f-d4dc-4acb-bfee-6699c8ad95e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e05976c9-220c-4526-9999-32ed02d4368c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34349bc2-1a21-4eb4-b46c-320468a6adbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e05976c9-220c-4526-9999-32ed02d4368c",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "7f049508-47b9-4b56-96b2-2b95da72b319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "acdaa883-6504-425d-9414-2ddc7d2079bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f049508-47b9-4b56-96b2-2b95da72b319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5568b8c8-2ff7-41c7-a6a8-8309c9521ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f049508-47b9-4b56-96b2-2b95da72b319",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "477da39c-1af5-432c-98ad-f446205a2884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "8084add7-5311-4521-acf7-0654e12827dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "477da39c-1af5-432c-98ad-f446205a2884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e966ae6a-41af-40f0-acb1-b3490f3c0e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "477da39c-1af5-432c-98ad-f446205a2884",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "c4c35bb8-3638-401a-abc8-93133ee6fd5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "d97aded3-7044-4c42-820b-1af32ae076d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4c35bb8-3638-401a-abc8-93133ee6fd5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f31e3f49-b7e4-417c-a71f-d359a1c419a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4c35bb8-3638-401a-abc8-93133ee6fd5c",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "a7a91eac-96a0-4eb9-beae-78e07fa67fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "02df7e0b-a34d-4a42-a49f-d844b3176069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a91eac-96a0-4eb9-beae-78e07fa67fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81f330d0-aa61-4521-830e-9cc440cff943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a91eac-96a0-4eb9-beae-78e07fa67fe1",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "4cdc4b0d-810a-485e-abbc-dc08f18d0784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "287c8eaa-64f8-4169-b909-22a2d93eb733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cdc4b0d-810a-485e-abbc-dc08f18d0784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3064a719-43e6-4120-b998-6d4408498a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdc4b0d-810a-485e-abbc-dc08f18d0784",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "4a0544f6-ce41-4ad0-ade4-12ac48aa6a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "ca23394e-e98e-41d4-916d-0dc55aa1aa43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a0544f6-ce41-4ad0-ade4-12ac48aa6a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6061414f-18d8-428a-9ad7-7b29c15b0adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a0544f6-ce41-4ad0-ade4-12ac48aa6a5f",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "fe9646f4-ffd6-49c0-bcef-ae244c41c54d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "4e77ae7a-c550-451e-ac23-fb2c6e59a740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe9646f4-ffd6-49c0-bcef-ae244c41c54d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7666e096-fdb9-4f07-a3e9-3aa600a54b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe9646f4-ffd6-49c0-bcef-ae244c41c54d",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "815370fa-603d-4e6d-9d71-1e0c9ee27d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "6f9202b6-f2f2-4627-80f6-a4567f0340e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815370fa-603d-4e6d-9d71-1e0c9ee27d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8326a2-5619-4831-b04d-055a8ea5b31e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815370fa-603d-4e6d-9d71-1e0c9ee27d87",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "5d4c4225-1eae-4239-b294-b5aa4b53f605",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "2f2acd00-6537-4f09-bdf2-3a98cddf99f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4c4225-1eae-4239-b294-b5aa4b53f605",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d787b0b-84fd-4d4a-a3cb-02a69c2cfb60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4c4225-1eae-4239-b294-b5aa4b53f605",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "fceeeb2d-0861-40b2-afa5-3ccc0d554446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "3dd1e198-819f-4a14-b379-308c90d949d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fceeeb2d-0861-40b2-afa5-3ccc0d554446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1f6fec-2cfc-4323-8645-2c3a9e1eee16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceeeb2d-0861-40b2-afa5-3ccc0d554446",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "cff24180-f277-4fe1-b493-e771d2450c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "9d3a05a5-2617-4030-a003-90fc26a85b83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cff24180-f277-4fe1-b493-e771d2450c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f9c887a-9234-4df0-a9c0-46afb5b87525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cff24180-f277-4fe1-b493-e771d2450c87",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "ca5d85a3-b68c-4528-a777-b0bf762f6a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "32398901-c367-4a10-a7ff-2c6196a62f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca5d85a3-b68c-4528-a777-b0bf762f6a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db2ad3be-1312-4c4b-b460-f840427f086f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca5d85a3-b68c-4528-a777-b0bf762f6a63",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "bc745c74-41e5-4552-b2ab-e73d4267b291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "b656b9ec-873b-481a-9014-08ff89833503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc745c74-41e5-4552-b2ab-e73d4267b291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0218a39-b3d3-4743-8583-34cd2c00d5ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc745c74-41e5-4552-b2ab-e73d4267b291",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "85c8cbb2-92f3-47d3-925f-9036b871db03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "0873d3dc-a9d3-45d3-a7da-e8d41da8d648",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85c8cbb2-92f3-47d3-925f-9036b871db03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656c0df1-6fd4-4651-b16d-06ae661e6ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85c8cbb2-92f3-47d3-925f-9036b871db03",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        },
        {
            "id": "2e4b9ad3-8512-45ef-b817-d6efad822dd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "compositeImage": {
                "id": "2b51ef8b-7515-4896-8a16-63e3d05660ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4b9ad3-8512-45ef-b817-d6efad822dd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b001c5-9ae2-48ea-bcb6-46b96f7e8168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4b9ad3-8512-45ef-b817-d6efad822dd8",
                    "LayerId": "adb96c91-a002-4fc1-8f01-492fc2aff5a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "adb96c91-a002-4fc1-8f01-492fc2aff5a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01f4749f-efa1-445f-afeb-bc09a8448a7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}