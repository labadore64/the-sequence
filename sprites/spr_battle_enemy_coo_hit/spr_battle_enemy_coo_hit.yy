{
    "id": "db1ea283-cc73-4017-bf3f-94a5381a4bbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 235,
    "bbox_left": 33,
    "bbox_right": 201,
    "bbox_top": 107,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88ab14e1-f2be-4c0a-8f67-fe9577da2a91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db1ea283-cc73-4017-bf3f-94a5381a4bbe",
            "compositeImage": {
                "id": "81baf565-62f4-4a91-a3ce-d8267396f68e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ab14e1-f2be-4c0a-8f67-fe9577da2a91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2cffbbd-f888-4a74-ba96-135e4b7a6e07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ab14e1-f2be-4c0a-8f67-fe9577da2a91",
                    "LayerId": "8b4d65b5-b88b-41ee-9d76-d77c39a5568b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8b4d65b5-b88b-41ee-9d76-d77c39a5568b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db1ea283-cc73-4017-bf3f-94a5381a4bbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}