{
    "id": "0bf40f04-0038-4124-a921-2bc0b8077da7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_deskLight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 26,
    "bbox_right": 109,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fa7265f-5383-4f09-98b8-1ba1f96554c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bf40f04-0038-4124-a921-2bc0b8077da7",
            "compositeImage": {
                "id": "d4ffa7a2-5e06-4595-8624-5e654ce92012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa7265f-5383-4f09-98b8-1ba1f96554c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb47ed7-0884-43ca-a597-06564c50a65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa7265f-5383-4f09-98b8-1ba1f96554c2",
                    "LayerId": "a526d9fc-c601-4df8-a350-af449f6ec900"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a526d9fc-c601-4df8-a350-af449f6ec900",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bf40f04-0038-4124-a921-2bc0b8077da7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}