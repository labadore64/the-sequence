{
    "id": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_binary_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 4,
    "bbox_right": 251,
    "bbox_top": 141,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1787d87e-1115-48ca-afbc-124375e39cd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "895d9963-1aee-4428-bdbb-c0ba1cb3e211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1787d87e-1115-48ca-afbc-124375e39cd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a8846d-ce82-466f-a21a-e38c4fbbaa84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1787d87e-1115-48ca-afbc-124375e39cd5",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "6a8fad9f-7387-4081-8d4f-f8c893c8eab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "88376c43-9232-46bc-b221-95feebc5366a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8fad9f-7387-4081-8d4f-f8c893c8eab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c029f3bb-c120-4036-83b5-55e2807d6a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8fad9f-7387-4081-8d4f-f8c893c8eab3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "41e0bfd2-ef5b-4443-a6e8-2e4c287d0168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "b71d8928-935c-4e59-8718-46fd86ec190c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41e0bfd2-ef5b-4443-a6e8-2e4c287d0168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a49080ce-5ea7-42c8-8aa4-8449811b7f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41e0bfd2-ef5b-4443-a6e8-2e4c287d0168",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "3174844f-adb5-4007-ae41-d268af8b015e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "e9f76937-19c0-430f-8eb3-36e44c324954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3174844f-adb5-4007-ae41-d268af8b015e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c59816d-f8ff-4394-8e7b-68bb44e66742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3174844f-adb5-4007-ae41-d268af8b015e",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "2434b2a0-1067-49cb-aacc-50917615b750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "4ad75ff8-00ff-4592-95eb-2c6893a74d44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2434b2a0-1067-49cb-aacc-50917615b750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6f0a6d-6c18-4808-99a7-a671b2741595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2434b2a0-1067-49cb-aacc-50917615b750",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "277e16f4-1ba1-4dae-8e46-c74ac0c0d7bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "de5427f9-214e-438a-b871-a45bcb781355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "277e16f4-1ba1-4dae-8e46-c74ac0c0d7bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "292d207d-8263-4b76-ab59-4cfdacce7291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "277e16f4-1ba1-4dae-8e46-c74ac0c0d7bf",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "11a03eb6-936a-486e-883c-01ec378b825b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "f2d03e0d-b670-4e15-a247-77e4a7024bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a03eb6-936a-486e-883c-01ec378b825b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9a7cc11-5eaa-4b18-addd-868f50eff47e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a03eb6-936a-486e-883c-01ec378b825b",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "47668ee4-4b23-4c39-abf1-ab55e3d8282d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "a460a309-4ee3-4b0d-9e20-5da43d8d9eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47668ee4-4b23-4c39-abf1-ab55e3d8282d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b40514-15e3-4b5d-be6f-38c6e5c4ae20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47668ee4-4b23-4c39-abf1-ab55e3d8282d",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "739d5423-33cc-4bda-a329-04f7639975e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "2a2ce846-1f73-4844-84aa-5a1d6a1fbf49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "739d5423-33cc-4bda-a329-04f7639975e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a31e63f-e56f-493a-a4b6-b1a9439fa618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "739d5423-33cc-4bda-a329-04f7639975e2",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "436945db-ae24-4468-aca0-e502441323b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "d339d8fd-2571-4bee-a86a-4ad44ff6b4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "436945db-ae24-4468-aca0-e502441323b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9b4f99f-00d7-40bf-8642-e27c43b38a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "436945db-ae24-4468-aca0-e502441323b4",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "126d8317-d914-4a22-aba9-3ce92c76a11e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "37506585-1f4e-4fcb-a179-2d0410d007b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "126d8317-d914-4a22-aba9-3ce92c76a11e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd12f23-ea20-4dd8-a23c-22f4b64178a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "126d8317-d914-4a22-aba9-3ce92c76a11e",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "f1d40239-b99f-4693-a01d-7bb90a66023d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "974a4f96-7cef-4d2c-9909-47f38da5e4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d40239-b99f-4693-a01d-7bb90a66023d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f5fa09-24a1-458a-8c17-e3781364e04b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d40239-b99f-4693-a01d-7bb90a66023d",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "7583d820-f8b0-482f-b0e8-090b5e7a36bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "58398f1b-0563-4f1f-ab5a-bb4b476c5160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7583d820-f8b0-482f-b0e8-090b5e7a36bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3882c11-ba30-4522-b9d0-9d27e61c74e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7583d820-f8b0-482f-b0e8-090b5e7a36bf",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "f0ccee24-8a34-4019-9c42-1baac69d079a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "5a99413d-0e5d-4624-bb3d-81bab71212cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ccee24-8a34-4019-9c42-1baac69d079a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0fdd9d-cd1f-41ce-be43-6025e00d4934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ccee24-8a34-4019-9c42-1baac69d079a",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "380bc42b-7ee7-45ff-814d-ceb770535921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "10a4b953-97b7-4777-95e3-b27f95f44a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380bc42b-7ee7-45ff-814d-ceb770535921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3b6b84-8249-4d69-8515-24c27064f1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380bc42b-7ee7-45ff-814d-ceb770535921",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "b61b2ad0-94f5-428e-b956-9e02f5f2da9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "8e011516-6184-400b-b95d-5ea7f680a2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61b2ad0-94f5-428e-b956-9e02f5f2da9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f9d4503-5b93-4b24-b8e6-691d7c96ec5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61b2ad0-94f5-428e-b956-9e02f5f2da9c",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "4cf92bae-5bdc-4c18-96dc-fcacf3d980c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "b4a18c8f-6480-4970-b5e5-8e7d8778896b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf92bae-5bdc-4c18-96dc-fcacf3d980c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c74e58-fc3f-4918-9348-5548389e22cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf92bae-5bdc-4c18-96dc-fcacf3d980c1",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "caa20456-ed1a-4aad-92c2-53be4cae95ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "db32022a-456b-4f42-a5b3-44c6f6abdd93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa20456-ed1a-4aad-92c2-53be4cae95ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d343e85-22e0-4406-81a7-74358c7650f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa20456-ed1a-4aad-92c2-53be4cae95ac",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "cb1b4801-eccb-4157-a3b6-97cc2120c69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "0a3909ff-da70-4fd7-bc1d-17443e56acd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1b4801-eccb-4157-a3b6-97cc2120c69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50c67e75-7d02-45f1-b897-a34058360e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1b4801-eccb-4157-a3b6-97cc2120c69c",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "05217e13-0bc6-4265-8b0a-b39a77c598ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "1e1e3e3b-2a6c-4a43-a351-3bdd3925824a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05217e13-0bc6-4265-8b0a-b39a77c598ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a45e66da-097c-475c-918d-e2e7c8c723b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05217e13-0bc6-4265-8b0a-b39a77c598ed",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "3d212892-7ff4-47e1-8b76-1ce9cd837414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "dd8955e4-3902-4700-aba8-d61e7410a48d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d212892-7ff4-47e1-8b76-1ce9cd837414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb912442-03ea-43e0-8d5e-a64bcef9f437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d212892-7ff4-47e1-8b76-1ce9cd837414",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "f5911136-5d67-4be0-8353-3bdb792c7cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "cdf327d4-3836-4a36-a9ef-992941fc0f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5911136-5d67-4be0-8353-3bdb792c7cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c932155-8a3f-48f8-ba2f-14a47abf400b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5911136-5d67-4be0-8353-3bdb792c7cb3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "e1f64788-c91e-4b7d-a592-5a6f15f0c983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "be89e752-9c77-4f65-9793-4502898c7c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f64788-c91e-4b7d-a592-5a6f15f0c983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cdc0630-5407-4e9e-b336-2969c7835796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f64788-c91e-4b7d-a592-5a6f15f0c983",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "933c6dbb-33a5-4a4b-a1b9-1720d1af484f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "b7ff8de7-a5d5-45fe-bff2-1d08fac25030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933c6dbb-33a5-4a4b-a1b9-1720d1af484f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a883f9-efce-430b-b899-747f2a1599a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933c6dbb-33a5-4a4b-a1b9-1720d1af484f",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "88264d33-908e-4a6f-957b-0a77817c66e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "6aa74c45-d1af-42db-a3cc-5af158f74cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88264d33-908e-4a6f-957b-0a77817c66e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f027421a-06a8-4b4c-8f88-e2742e317b78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88264d33-908e-4a6f-957b-0a77817c66e3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "c7155efb-bca7-4321-ab72-d8e8829b84ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "a24a4f13-9f0b-4ab0-9e09-f88b890f1b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7155efb-bca7-4321-ab72-d8e8829b84ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b73aff-e2d7-4cea-822f-f59947134534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7155efb-bca7-4321-ab72-d8e8829b84ca",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "1a3e652f-d724-4457-93d1-699b6fc994c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "2c7945d1-60fd-4c13-a26c-b3c5e1a055bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3e652f-d724-4457-93d1-699b6fc994c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6445966e-d1bf-4fab-bc17-cd7e9b2ebe83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3e652f-d724-4457-93d1-699b6fc994c2",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "243f508d-e83e-44ca-872f-7f58628a28cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "2ba506f4-585d-4800-a0e2-006fd3fe8f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "243f508d-e83e-44ca-872f-7f58628a28cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8cd2c81-befd-425b-a6d0-86ba9a0b76e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "243f508d-e83e-44ca-872f-7f58628a28cf",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "cd609941-1e5f-41c0-98c5-4eb7fb646cfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "54cb16c5-059f-4685-b9e7-33e4f0a5e2d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd609941-1e5f-41c0-98c5-4eb7fb646cfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4cb38d7-6419-4c12-9a6c-aea89b0b021c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd609941-1e5f-41c0-98c5-4eb7fb646cfe",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "97e2f471-a1da-46b3-810b-0f3c0c736e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "c9b1309f-46dc-4de7-80ed-ed143383a5e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e2f471-a1da-46b3-810b-0f3c0c736e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7297253b-251d-4ae3-95d5-953beb6ffd0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e2f471-a1da-46b3-810b-0f3c0c736e5f",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "ecf95059-4d28-44b1-8df3-0737590af233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "bd7f2303-bdd7-485a-ad79-4801c7199607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf95059-4d28-44b1-8df3-0737590af233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a1945c-a17f-4804-9d54-fded86f36a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf95059-4d28-44b1-8df3-0737590af233",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "214af014-ef2e-42df-8bb7-7f9d3f54ace6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "67817a5b-c9f8-4f6a-af06-b27bd8becb72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "214af014-ef2e-42df-8bb7-7f9d3f54ace6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a7c6982-9d69-4282-a651-7123808e5e39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "214af014-ef2e-42df-8bb7-7f9d3f54ace6",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "b140555f-ccbc-4183-b877-5551463da077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "9d979b38-6aea-413f-9862-01f9cf261b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b140555f-ccbc-4183-b877-5551463da077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f07eee8d-cac2-4b83-9b6b-fdd010b4a8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b140555f-ccbc-4183-b877-5551463da077",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "b02bcb79-e675-43b3-a046-93f6325be7c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "232f7bce-bbf8-45b4-8345-afd75a7a5823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b02bcb79-e675-43b3-a046-93f6325be7c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a725ebf-d4ae-401b-9afc-fe73c96813a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b02bcb79-e675-43b3-a046-93f6325be7c3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "e27248bf-470c-4b5c-b8f7-6215613c749d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "03315a83-2bd0-4ef6-ae80-4369158a1134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e27248bf-470c-4b5c-b8f7-6215613c749d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8be3365a-40cb-4745-b182-bdba7d936f97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e27248bf-470c-4b5c-b8f7-6215613c749d",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "908f2a3d-847e-4d49-9053-71dbcea9354f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "7d53e842-65ae-417d-9147-a73f9f220fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "908f2a3d-847e-4d49-9053-71dbcea9354f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89f23542-8eb1-4d7d-b4f6-62ad06479f9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908f2a3d-847e-4d49-9053-71dbcea9354f",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "e1c9acc8-2279-4fba-9d30-3aa7df33add2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "5024e072-3b1b-47e5-9712-38a664619ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c9acc8-2279-4fba-9d30-3aa7df33add2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ce7812-f785-4a5c-8fba-04f1dcbd0ccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c9acc8-2279-4fba-9d30-3aa7df33add2",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "630a45da-7582-4a96-baf0-1f7129e4b997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "1d15eb02-1d56-49ba-9dc6-c40d5354045f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630a45da-7582-4a96-baf0-1f7129e4b997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e4ef22-cd2a-4d92-9d47-a0b8a8d9f70e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630a45da-7582-4a96-baf0-1f7129e4b997",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "25788a97-094a-4c1a-92a9-6c56afe8d80e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "828ad3f9-586e-47a2-910f-474007d89852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25788a97-094a-4c1a-92a9-6c56afe8d80e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa628f5-d005-4f54-8696-95eb3ba231ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25788a97-094a-4c1a-92a9-6c56afe8d80e",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "5d663e40-741c-4156-b35a-e316d8785c46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "b5839528-3d02-4699-b88d-8143d0b5f096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d663e40-741c-4156-b35a-e316d8785c46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c32d43e-2f8a-4fb6-8558-a664bfac3c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d663e40-741c-4156-b35a-e316d8785c46",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "46d4b4a1-ae3e-4382-a908-0e45939b82cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "e4ab1e79-74b3-4a03-8a11-6508ede6ebfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d4b4a1-ae3e-4382-a908-0e45939b82cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c45fddc-fd89-450a-8f3d-9988c1c6fc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d4b4a1-ae3e-4382-a908-0e45939b82cf",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "5198f18f-9af5-40da-ad75-4186d0ad13fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "afd9b6a2-c4dd-4ecf-b04c-237dbb0f0edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5198f18f-9af5-40da-ad75-4186d0ad13fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ff701f-d293-4d5d-aea3-365995973a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5198f18f-9af5-40da-ad75-4186d0ad13fa",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "0426b8d8-5d51-453f-8553-a827ec377e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "f5e38f50-e17e-4276-b306-275167d74f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0426b8d8-5d51-453f-8553-a827ec377e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cdb0ecb-e283-4ed9-80ca-258875078963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0426b8d8-5d51-453f-8553-a827ec377e5c",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "4e4c7c58-6433-4c38-8735-a3d4328e2e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "110691f9-730a-4aa6-8457-e220146c3454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e4c7c58-6433-4c38-8735-a3d4328e2e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee320e3b-9cfb-477b-9fc8-45ff252b1775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e4c7c58-6433-4c38-8735-a3d4328e2e18",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "9485a0b8-6278-4cfe-aebf-d92df307facd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "027fdd9d-d56e-4358-8163-1e6971097f8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9485a0b8-6278-4cfe-aebf-d92df307facd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d124d5b8-8e8e-46bb-87d6-05a64d660c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9485a0b8-6278-4cfe-aebf-d92df307facd",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "1c4e30ef-a10f-40e1-9368-4d4c4e995b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "111e709c-365a-4844-ab51-9a5a4a82ac3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c4e30ef-a10f-40e1-9368-4d4c4e995b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686689d9-eadf-4e67-b5c3-3876fb7b2531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c4e30ef-a10f-40e1-9368-4d4c4e995b98",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "da8275ad-8e91-485c-82c6-655437184b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "bd753532-19d3-41e5-8dc5-1cd627474b9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da8275ad-8e91-485c-82c6-655437184b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c430ea0-1eeb-4bce-bb87-5f0141de3ada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da8275ad-8e91-485c-82c6-655437184b1c",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "9a4298cb-8c3e-4ee5-b1c1-760975f903b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "1c10fa87-cd54-4704-a22b-7b7fe94c5848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a4298cb-8c3e-4ee5-b1c1-760975f903b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0cdbfd-317c-496d-a31b-ec52dc50051d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a4298cb-8c3e-4ee5-b1c1-760975f903b3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "6c84d5a5-ef7b-49dd-83fd-9d7a795212a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "ebf454b3-71da-4f93-83da-a4fe38c0c83a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c84d5a5-ef7b-49dd-83fd-9d7a795212a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b403c74-f2fe-4b4c-8781-0fe6c8f3f20c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c84d5a5-ef7b-49dd-83fd-9d7a795212a8",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "2ec6014c-ff40-434c-9db3-2080dff24623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "8854486f-17af-4420-897a-228148e4dfab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec6014c-ff40-434c-9db3-2080dff24623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc5eb7e0-096e-4a0c-ab8a-16fd352710a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec6014c-ff40-434c-9db3-2080dff24623",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "3f1564f2-0e14-4ada-bd50-261688f51981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "2c03b78f-94f3-4f79-a5f8-67728f57db5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1564f2-0e14-4ada-bd50-261688f51981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990a0ca7-576f-4539-9d3a-f33bb4e737a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1564f2-0e14-4ada-bd50-261688f51981",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "8f1cc9d0-f6a2-4603-93e6-0e8005896536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "14326a1b-48ed-497c-ae27-a3572a715812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1cc9d0-f6a2-4603-93e6-0e8005896536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eee3793-332c-4fcc-9838-74c406f3531d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1cc9d0-f6a2-4603-93e6-0e8005896536",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "2762ad2a-f16a-4713-b4a3-b8e8db2b4df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "38a45dfa-4dcf-4a60-9213-78abdc56be0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2762ad2a-f16a-4713-b4a3-b8e8db2b4df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a8b53d-0304-497e-b0af-4390ff8c5fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2762ad2a-f16a-4713-b4a3-b8e8db2b4df3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "3f4539b4-798d-4421-8d9d-524df6db3449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "49fd2bb6-785a-4eb5-8d06-7058484910e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f4539b4-798d-4421-8d9d-524df6db3449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6fe5e26-ee64-4a12-8789-152592e51e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f4539b4-798d-4421-8d9d-524df6db3449",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "f8a6f8c5-1a3c-407f-b542-a915b4ab333d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "64880e08-c8c0-4da7-bacc-adeb4be77c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a6f8c5-1a3c-407f-b542-a915b4ab333d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0117e7d2-99cf-47ab-9a64-f91e21561e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a6f8c5-1a3c-407f-b542-a915b4ab333d",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "89ec2d6f-1c6d-431d-a42c-e88e82da8fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "bf134b51-8e96-4a15-b3ec-c6e1884aa542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ec2d6f-1c6d-431d-a42c-e88e82da8fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d6f4d01-fe3a-45f4-8de8-6b93271ad701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ec2d6f-1c6d-431d-a42c-e88e82da8fce",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "4d449f9d-cba0-4311-8da2-02d10ecdc2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "db84643f-e48d-47cf-b9f8-e9c1d937faa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d449f9d-cba0-4311-8da2-02d10ecdc2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689436a8-7f23-4aad-a2b4-5b2e6b7d4d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d449f9d-cba0-4311-8da2-02d10ecdc2b1",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "7cde4943-355e-488a-af70-1964ff1839e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "8b7a45b3-eed5-44b4-9972-0af7ba51b0ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cde4943-355e-488a-af70-1964ff1839e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be12f5a-56d4-479b-8a6d-47538b8f5e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cde4943-355e-488a-af70-1964ff1839e0",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "141c40b2-71b8-4085-8a93-b5bd0de583e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "cc8252ea-5074-48c0-81d1-c09026812b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "141c40b2-71b8-4085-8a93-b5bd0de583e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e64bb3ec-77ab-4610-8165-3cbdff3ec7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141c40b2-71b8-4085-8a93-b5bd0de583e8",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        },
        {
            "id": "e389135f-491e-4dfe-8782-0cdfcff1c3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "compositeImage": {
                "id": "4c0ccb95-685c-4674-ab39-c43ebb27c0fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e389135f-491e-4dfe-8782-0cdfcff1c3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4d05b5-04aa-4d8b-a190-aa952d23d659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e389135f-491e-4dfe-8782-0cdfcff1c3e3",
                    "LayerId": "79507e64-27d0-4afc-81a0-dfef8e28c981"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "79507e64-27d0-4afc-81a0-dfef8e28c981",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3849a0ad-a945-4e4b-86e2-6bbd841b3f0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6efab5bc-3ed9-4097-8e3a-b423e88d6ca0",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}