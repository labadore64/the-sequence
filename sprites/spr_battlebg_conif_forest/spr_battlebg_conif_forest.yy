{
    "id": "4206f97d-4537-4300-b0dc-4f67f2bc768b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battlebg_conif_forest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96c356d2-ce1d-497c-a9e9-f60594031b04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4206f97d-4537-4300-b0dc-4f67f2bc768b",
            "compositeImage": {
                "id": "6418a51a-62db-4370-963e-0922f19aa4a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c356d2-ce1d-497c-a9e9-f60594031b04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc447551-e6a5-4fbf-bcb9-f3945c64f6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c356d2-ce1d-497c-a9e9-f60594031b04",
                    "LayerId": "f8d574c0-0273-4b4c-955d-42a9cdf5e359"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "f8d574c0-0273-4b4c-955d-42a9cdf5e359",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4206f97d-4537-4300-b0dc-4f67f2bc768b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "8fe852b8-9b54-467e-8a10-605b21170b0f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}