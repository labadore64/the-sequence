{
    "id": "374cac43-5702-404e-8c26-84a805c73774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_backgroundTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 8,
    "bbox_right": 120,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cfa23eb-9836-4dd7-beef-f33b64c51b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374cac43-5702-404e-8c26-84a805c73774",
            "compositeImage": {
                "id": "5ad18270-e7b2-4fd4-bb92-fb3b04308eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfa23eb-9836-4dd7-beef-f33b64c51b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04e68f07-ec7a-48e6-a610-66f237ea36a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfa23eb-9836-4dd7-beef-f33b64c51b31",
                    "LayerId": "73d6a74f-373b-4154-b41b-8d92dc251a55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "73d6a74f-373b-4154-b41b-8d92dc251a55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "374cac43-5702-404e-8c26-84a805c73774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}