{
    "id": "ceb25b50-1569-400a-821f-d38f2146bcd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_monolith_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 51,
    "bbox_right": 204,
    "bbox_top": 89,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "863e1e83-59b1-465b-9bb5-3a8deb021719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ceb25b50-1569-400a-821f-d38f2146bcd0",
            "compositeImage": {
                "id": "b978a11d-3c8e-4deb-9759-59ee1ad59c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "863e1e83-59b1-465b-9bb5-3a8deb021719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4adc5344-7149-46b3-9e2d-92da782d4e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "863e1e83-59b1-465b-9bb5-3a8deb021719",
                    "LayerId": "0011af62-e052-447f-9db2-22a5243c480f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0011af62-e052-447f-9db2-22a5243c480f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ceb25b50-1569-400a-821f-d38f2146bcd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "e3c3b26b-ae20-44a3-ba2c-2cd239127348",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}