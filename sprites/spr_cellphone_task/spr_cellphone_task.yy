{
    "id": "1a9c376a-a0cb-4212-b140-616aee9d1b9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cellphone_task",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "962d7bf7-f79d-4b7a-9893-6559cac8b26c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a9c376a-a0cb-4212-b140-616aee9d1b9f",
            "compositeImage": {
                "id": "485048e4-0ccc-49f1-a01d-b6b3d13a3e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "962d7bf7-f79d-4b7a-9893-6559cac8b26c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c6bd9c-5ce1-4183-bd34-92e2d708f862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "962d7bf7-f79d-4b7a-9893-6559cac8b26c",
                    "LayerId": "d06fa237-7fc8-4775-9237-137f0fcbfcf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d06fa237-7fc8-4775-9237-137f0fcbfcf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a9c376a-a0cb-4212-b140-616aee9d1b9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.38125,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}