{
    "id": "f064ab1e-ba9c-41a3-a2ba-518ecc3a7b47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_sprite_tricorn1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e478c35-25aa-444e-acde-51d2bcebbc1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f064ab1e-ba9c-41a3-a2ba-518ecc3a7b47",
            "compositeImage": {
                "id": "a909d7e6-d44b-4a79-8798-fbc01687a8c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e478c35-25aa-444e-acde-51d2bcebbc1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b7e95c9-4994-47ca-a6c0-fd5fcd25d312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e478c35-25aa-444e-acde-51d2bcebbc1f",
                    "LayerId": "adeb469a-d484-44a7-9740-08f87fd77d89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "adeb469a-d484-44a7-9740-08f87fd77d89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f064ab1e-ba9c-41a3-a2ba-518ecc3a7b47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}