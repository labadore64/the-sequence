{
    "id": "bc786245-992d-4de6-ace1-58de271526a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_typePhys",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7317c23-3b99-4ce9-8cd2-7b3dbb810961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc786245-992d-4de6-ace1-58de271526a9",
            "compositeImage": {
                "id": "6b3b79b4-f75e-48c3-9d63-7104138a4ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7317c23-3b99-4ce9-8cd2-7b3dbb810961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "248ef75c-4247-41d0-b506-b585856f4ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7317c23-3b99-4ce9-8cd2-7b3dbb810961",
                    "LayerId": "a68b3a80-bd8e-47d4-b996-41c1fea10569"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a68b3a80-bd8e-47d4-b996-41c1fea10569",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc786245-992d-4de6-ace1-58de271526a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}