{
    "id": "b504860b-7807-468c-ac3d-fd2e2b18d67d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_speguard_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e45bf017-8da4-4c8d-8be8-0e19cf7181e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b504860b-7807-468c-ac3d-fd2e2b18d67d",
            "compositeImage": {
                "id": "13e02508-730e-48fd-9477-5c092e46537e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e45bf017-8da4-4c8d-8be8-0e19cf7181e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75043a7f-b27f-4d85-bc06-ee7500f924fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45bf017-8da4-4c8d-8be8-0e19cf7181e9",
                    "LayerId": "60bbcb85-b861-4a20-a5dd-dd13e25309e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "60bbcb85-b861-4a20-a5dd-dd13e25309e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b504860b-7807-468c-ac3d-fd2e2b18d67d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ed8436d5-3937-421f-9a50-d3320ef06c20",
    "type": 1,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}