{
    "id": "91b30d59-2ec4-40b3-9b65-0501cc9669a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_table",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 15,
    "bbox_right": 112,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6d81d25-6744-468c-96a3-0e40a8633ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91b30d59-2ec4-40b3-9b65-0501cc9669a9",
            "compositeImage": {
                "id": "cc14072b-c5f6-49a1-8e51-252d860652b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d81d25-6744-468c-96a3-0e40a8633ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20a9a54-5aae-4476-986a-cb1d7caaaba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d81d25-6744-468c-96a3-0e40a8633ae0",
                    "LayerId": "8057d11b-9f97-4e32-9e37-a9a57e42e5dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8057d11b-9f97-4e32-9e37-a9a57e42e5dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91b30d59-2ec4-40b3-9b65-0501cc9669a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}