{
    "id": "dea790a2-2270-46c9-ba5a-399a63e85ac9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_mailboxItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 33,
    "bbox_right": 92,
    "bbox_top": 61,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b40246d-983c-404f-bb5e-07042b2c0e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea790a2-2270-46c9-ba5a-399a63e85ac9",
            "compositeImage": {
                "id": "c282f72b-b394-4d87-81ae-9ecdac5b1f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b40246d-983c-404f-bb5e-07042b2c0e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c153842-caab-4644-8a6b-dd6756b56ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b40246d-983c-404f-bb5e-07042b2c0e83",
                    "LayerId": "72e6275a-2a4a-430f-8cc4-a979d9c11441"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "72e6275a-2a4a-430f-8cc4-a979d9c11441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dea790a2-2270-46c9-ba5a-399a63e85ac9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}