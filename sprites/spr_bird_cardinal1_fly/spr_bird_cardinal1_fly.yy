{
    "id": "1070b6c4-e47e-4092-af8a-364825dff39d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_cardinal1_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 25,
    "bbox_right": 103,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d16b2620-2b44-4a91-8153-d112a24a6bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1070b6c4-e47e-4092-af8a-364825dff39d",
            "compositeImage": {
                "id": "c8c9ff3f-ace6-49bd-aa8a-13b5406461dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d16b2620-2b44-4a91-8153-d112a24a6bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10923428-145c-4c64-800b-2405d591fb0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d16b2620-2b44-4a91-8153-d112a24a6bb7",
                    "LayerId": "fe3ccc3b-bbba-4461-9aae-663a2a9b3695"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "fe3ccc3b-bbba-4461-9aae-663a2a9b3695",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1070b6c4-e47e-4092-af8a-364825dff39d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}