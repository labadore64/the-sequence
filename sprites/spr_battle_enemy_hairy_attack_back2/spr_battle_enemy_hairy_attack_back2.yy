{
    "id": "792554dd-2ee9-405c-ba1a-bcd652e29338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_attack_back2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 36,
    "bbox_right": 210,
    "bbox_top": 76,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82437315-e107-4775-a32f-1f8b0e93f0e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "792554dd-2ee9-405c-ba1a-bcd652e29338",
            "compositeImage": {
                "id": "2e75adec-c6d0-45da-b5c5-8ad8fd481d7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82437315-e107-4775-a32f-1f8b0e93f0e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "504adb48-6f44-4bd5-89b2-9ee08c8dc61f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82437315-e107-4775-a32f-1f8b0e93f0e9",
                    "LayerId": "fbe3bcd2-725a-47bc-917e-030f0f47f52c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fbe3bcd2-725a-47bc-917e-030f0f47f52c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "792554dd-2ee9-405c-ba1a-bcd652e29338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}