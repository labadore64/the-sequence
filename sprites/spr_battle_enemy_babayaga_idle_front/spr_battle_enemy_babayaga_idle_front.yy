{
    "id": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_babayaga_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 231,
    "bbox_top": 77,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "852474d1-293c-479d-afc5-50b5b3fe7dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "e155ff17-4b65-4166-a01d-1702e1d3a30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "852474d1-293c-479d-afc5-50b5b3fe7dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3698bf-5bd8-4bf6-9a64-3b89eaa089d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "852474d1-293c-479d-afc5-50b5b3fe7dac",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "e1f82f6a-d86c-4109-b9fb-45598cb6637d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "fc10f36d-2d3f-499c-ac0e-7e569af59218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f82f6a-d86c-4109-b9fb-45598cb6637d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "923cd656-fb15-4dff-ab37-c3f6f8477cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f82f6a-d86c-4109-b9fb-45598cb6637d",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "0fd19ba5-cc08-4801-9c73-536c3a724477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "1598bfc6-7fc3-44c7-a63f-9dca681bdf5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd19ba5-cc08-4801-9c73-536c3a724477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4caec1d-8b7e-46f6-9095-d98476a0cef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd19ba5-cc08-4801-9c73-536c3a724477",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "698a221a-a736-45fd-bed6-bf0e74b04d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "b478f81d-ad52-48a1-bcc3-e82ca7d79d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698a221a-a736-45fd-bed6-bf0e74b04d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d20104c-b2f1-4d30-a510-1bcaacfc7a83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698a221a-a736-45fd-bed6-bf0e74b04d0c",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "a0ddf609-db83-47c2-aa0a-0f583b2753ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "5ada844a-a59c-4182-b1db-e84ef749b825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0ddf609-db83-47c2-aa0a-0f583b2753ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "014a12bd-8aac-4d61-9ce8-6b5e75dbdf2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0ddf609-db83-47c2-aa0a-0f583b2753ad",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "ffc7b427-af3e-4521-972f-63f7d654311a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "4d6894d6-2ec0-40ea-a6d1-e6347d6e0840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc7b427-af3e-4521-972f-63f7d654311a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1124a329-581a-4264-86f4-8c8685325988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc7b427-af3e-4521-972f-63f7d654311a",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "f19f53b3-03f4-4098-a395-4476adbeb33b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "283b88cf-c0d8-48de-be02-e0fa6b08b5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f19f53b3-03f4-4098-a395-4476adbeb33b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493e5fd2-2b2b-4662-b86e-2afa80a1a840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f19f53b3-03f4-4098-a395-4476adbeb33b",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "0415b09a-4c46-4d05-be18-7da79fced11c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "d29f7e12-093b-468a-a960-7f67bd2923bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0415b09a-4c46-4d05-be18-7da79fced11c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3efa030-5487-40e7-a3a5-e2d300fed7d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0415b09a-4c46-4d05-be18-7da79fced11c",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "1c6ff38a-6843-4321-a16f-e131f063f99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "a98d0cf8-6c58-428e-a0f8-e7085471d330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6ff38a-6843-4321-a16f-e131f063f99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de24ebd-f0d5-4be4-9338-5eb7607747ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6ff38a-6843-4321-a16f-e131f063f99a",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "d3065074-415f-4943-93c9-91e1e1fcf06c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "cc82b2fc-0a52-4eef-acc6-4db320fba8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3065074-415f-4943-93c9-91e1e1fcf06c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff6f263-7a7b-4e30-9f41-cd858e2199fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3065074-415f-4943-93c9-91e1e1fcf06c",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "5628778c-05da-4edb-bd7a-9336a2b4bc12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "f9198281-105f-4639-b781-073d8476b3b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5628778c-05da-4edb-bd7a-9336a2b4bc12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8d69cbd-ffaf-4c64-85ef-33fa72d919cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5628778c-05da-4edb-bd7a-9336a2b4bc12",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "db2f8882-9425-471a-a001-297814150bd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "2a9b9127-4b58-437a-9db9-a760e35a170e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db2f8882-9425-471a-a001-297814150bd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342d4e07-71e8-4f44-a655-c554944f956d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db2f8882-9425-471a-a001-297814150bd3",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "fa87da6b-370e-4466-8e81-d358b113b7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "b142203c-a689-4bb4-a324-66d62b151c89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa87da6b-370e-4466-8e81-d358b113b7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52b41679-8327-4688-bbf3-99a19045e815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa87da6b-370e-4466-8e81-d358b113b7b4",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "9ec1541d-41ea-42a7-9024-4e6b346075fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "e55d22e6-fbc5-4de8-8db7-105b6f48ba38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec1541d-41ea-42a7-9024-4e6b346075fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d5fc3ee-4152-4ffb-bb03-8f86249fe80b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec1541d-41ea-42a7-9024-4e6b346075fd",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "66881ae9-0df4-4eba-b372-19decdc9d3da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "3be941f7-b07e-47ef-adac-4ba742ea96ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66881ae9-0df4-4eba-b372-19decdc9d3da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3eb2ec-ba40-49ba-be27-c948d2a76c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66881ae9-0df4-4eba-b372-19decdc9d3da",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "3def168a-9c22-4b77-aca2-6f7ce03c14fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "77783885-3c4e-4869-867a-d717f3a3d74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3def168a-9c22-4b77-aca2-6f7ce03c14fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb50383-8e16-4c12-bb62-fede669bb43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3def168a-9c22-4b77-aca2-6f7ce03c14fd",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "5b19d39f-bcce-4db0-bdcc-8f20bbbd0828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "63e8e9bf-6cef-4cc4-8967-48f81b8f9c1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b19d39f-bcce-4db0-bdcc-8f20bbbd0828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "291cd451-d16f-49b6-8313-ff75752411ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b19d39f-bcce-4db0-bdcc-8f20bbbd0828",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "8e8f7b1a-08b4-441e-b776-99255dc025c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "74fed4b3-fd01-4d0c-9713-8df53641b60a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8f7b1a-08b4-441e-b776-99255dc025c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0be06a7-217a-4975-bf70-716476f5b3ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8f7b1a-08b4-441e-b776-99255dc025c8",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "c001d075-6c96-43fa-af0e-fba40b4f696b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "b501c3af-84b2-4d61-9d6e-cb144604278b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c001d075-6c96-43fa-af0e-fba40b4f696b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eca5b11-3ea9-4d44-9de8-0b1e3fcf9cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c001d075-6c96-43fa-af0e-fba40b4f696b",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        },
        {
            "id": "b751b5f0-f662-4db4-b0a9-2da71fb2b623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "compositeImage": {
                "id": "919ea563-8ac0-4f9d-8089-a4251256b9d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b751b5f0-f662-4db4-b0a9-2da71fb2b623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f4d3ea-ba82-442c-88a9-1d4c8b5a527d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b751b5f0-f662-4db4-b0a9-2da71fb2b623",
                    "LayerId": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "93de9578-1bc6-4eb3-b40c-f2d4d66b007b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfaee87c-372c-48ca-8971-3fefeeef9a06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "660d4a34-eb5a-49a4-93ee-8d23a3172db4",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}