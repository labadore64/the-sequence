{
    "id": "58a48361-9b3c-48e7-995c-04530151d1ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_snowbird_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 65,
    "bbox_right": 170,
    "bbox_top": 96,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c57c0ba-bdb0-4382-88d8-f1224c937cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "e1295cf2-d6f2-4f32-a578-62ddce676221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c57c0ba-bdb0-4382-88d8-f1224c937cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10b502c-734d-462f-90a6-265188c0643d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c57c0ba-bdb0-4382-88d8-f1224c937cb0",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "41f90ef7-2697-4613-847f-22ee5f479b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "5a1daed4-eb91-44ee-8eec-9e3070fce37f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41f90ef7-2697-4613-847f-22ee5f479b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4f32e5-1df9-40cf-b81c-842ab8d685bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41f90ef7-2697-4613-847f-22ee5f479b37",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "01be2f06-6596-4ef0-93f7-d878b78ad3fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "e5596ea8-0ac6-41dd-a7dd-b19d411a1776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01be2f06-6596-4ef0-93f7-d878b78ad3fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b2b4f1-7551-4f70-b2e0-d95c5753c361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01be2f06-6596-4ef0-93f7-d878b78ad3fa",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "25f82e7c-0fd1-4efd-b221-aee811a5b6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "688f4906-b914-4cfa-829a-bd69a2a70834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f82e7c-0fd1-4efd-b221-aee811a5b6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47318ce8-eb07-4926-870b-0c90f13584ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f82e7c-0fd1-4efd-b221-aee811a5b6ec",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "79622fdd-fa61-4da6-a400-ea86d69230ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "0d09301f-260c-480a-b22d-8d02aab6e839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79622fdd-fa61-4da6-a400-ea86d69230ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01e60b2-73f0-43b1-9eb8-7cd95b79798d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79622fdd-fa61-4da6-a400-ea86d69230ad",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "563fe3b0-27c9-4cce-8c81-be3a0271252c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "2c2dfaad-6348-4961-9c1f-eb1140bdb9ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563fe3b0-27c9-4cce-8c81-be3a0271252c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "111dfe56-1597-4463-bc3a-95bb239892a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563fe3b0-27c9-4cce-8c81-be3a0271252c",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "7f5c47ed-a746-45c1-b6cc-1234a93fb9e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "33b113d7-96ab-4793-a42e-bf11f8ab3030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f5c47ed-a746-45c1-b6cc-1234a93fb9e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d34574-d5d6-4f17-92c1-c39f8c4a5d16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f5c47ed-a746-45c1-b6cc-1234a93fb9e4",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "853bb5ed-0a5a-41da-ad58-174d284fd63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "b40ff18e-9734-4c24-ae34-796f8448e3c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853bb5ed-0a5a-41da-ad58-174d284fd63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86cdc253-92d8-4b60-94ab-79fd315cfd24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853bb5ed-0a5a-41da-ad58-174d284fd63c",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "1f81a215-aa8f-4121-a6a3-cd67f7534cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "22e0a943-a170-4c0b-a509-aa5417f74c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f81a215-aa8f-4121-a6a3-cd67f7534cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5244c328-571e-4c32-bee5-462c832f9a84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f81a215-aa8f-4121-a6a3-cd67f7534cc8",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "fca777a3-5a33-4cda-a95c-564537e4ef17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "191ee1ef-14c3-4220-a80e-f177ec443ddd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fca777a3-5a33-4cda-a95c-564537e4ef17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0fb1763-d81a-418a-9e85-36bc395777c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fca777a3-5a33-4cda-a95c-564537e4ef17",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "813e4b83-602e-4328-8b3e-973eebe24c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "f9e50520-67b9-4bdd-a139-798743722ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "813e4b83-602e-4328-8b3e-973eebe24c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d5cb4c9-636d-4de2-aaf4-40da3a5755c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "813e4b83-602e-4328-8b3e-973eebe24c8e",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "4331ad00-b194-4edb-8e05-3c2d2c01ae2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "c3bf8ecd-a892-40c0-9a61-c8e5e1644f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4331ad00-b194-4edb-8e05-3c2d2c01ae2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d119ea60-4a3a-4915-8e1d-34e8dafea683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4331ad00-b194-4edb-8e05-3c2d2c01ae2a",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        },
        {
            "id": "c2e8b27f-2f71-4c20-98a6-ccae6775a3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "compositeImage": {
                "id": "b1999598-5ca3-4973-99ab-db11ce07c333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e8b27f-2f71-4c20-98a6-ccae6775a3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dae537e-926d-4187-a7d4-d7a9c60728c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e8b27f-2f71-4c20-98a6-ccae6775a3e1",
                    "LayerId": "41a792f7-c6cd-4322-aa77-c45b692a4d85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "41a792f7-c6cd-4322-aa77-c45b692a4d85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58a48361-9b3c-48e7-995c-04530151d1ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "d41a043f-5316-4f92-8dc3-11dc8826a28b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}