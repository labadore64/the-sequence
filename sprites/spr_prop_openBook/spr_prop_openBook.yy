{
    "id": "59de4bce-f60a-41df-a5de-d86d897a7491",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_openBook",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 31,
    "bbox_right": 87,
    "bbox_top": 97,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e48f458-1ca1-4a33-ab7d-03838e2c246d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59de4bce-f60a-41df-a5de-d86d897a7491",
            "compositeImage": {
                "id": "cf0b3395-cd52-4c0b-803a-9aa5e02a8a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e48f458-1ca1-4a33-ab7d-03838e2c246d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee20102b-8378-4495-80a2-79662f3a8a31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e48f458-1ca1-4a33-ab7d-03838e2c246d",
                    "LayerId": "9652aaab-d251-4e76-bf2a-a90869507ee7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9652aaab-d251-4e76-bf2a-a90869507ee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59de4bce-f60a-41df-a5de-d86d897a7491",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}