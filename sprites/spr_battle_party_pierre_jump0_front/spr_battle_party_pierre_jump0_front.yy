{
    "id": "3891e0b1-4d85-4168-9a3c-59e2124de291",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 243,
    "bbox_left": 28,
    "bbox_right": 207,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4130d84-ebe2-4326-88f7-ecdf8d4c995b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3891e0b1-4d85-4168-9a3c-59e2124de291",
            "compositeImage": {
                "id": "d0f56ba2-e686-4c0f-9ad5-ba66c3be91e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4130d84-ebe2-4326-88f7-ecdf8d4c995b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "697bbc0e-4f81-4ef6-b368-60985b5a7e53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4130d84-ebe2-4326-88f7-ecdf8d4c995b",
                    "LayerId": "afebbd6f-c1ce-42da-9e09-826deb50b7e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "afebbd6f-c1ce-42da-9e09-826deb50b7e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3891e0b1-4d85-4168-9a3c-59e2124de291",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}