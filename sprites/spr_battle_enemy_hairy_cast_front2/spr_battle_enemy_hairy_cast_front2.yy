{
    "id": "52a2ebfb-bffa-4374-a70d-35f27135a42e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_hairy_cast_front2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 59,
    "bbox_right": 223,
    "bbox_top": 109,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c05ff64a-c9c9-49fd-ae52-1058d6f28102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52a2ebfb-bffa-4374-a70d-35f27135a42e",
            "compositeImage": {
                "id": "c7990fc8-0863-4506-8cf3-88655286ef07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05ff64a-c9c9-49fd-ae52-1058d6f28102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3dc057c-8b97-432e-8228-85ba650565be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05ff64a-c9c9-49fd-ae52-1058d6f28102",
                    "LayerId": "a34ad790-992e-44b7-8456-da5c335ac51a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a34ad790-992e-44b7-8456-da5c335ac51a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52a2ebfb-bffa-4374-a70d-35f27135a42e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}