{
    "id": "6c0461ee-eef9-4b4b-b34c-18d2f8378128",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_jump0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 192,
    "bbox_left": 10,
    "bbox_right": 238,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb6aafb3-7f2e-4982-a9f0-92d53cd7a5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0461ee-eef9-4b4b-b34c-18d2f8378128",
            "compositeImage": {
                "id": "7d88afd8-64fc-4fb3-b62e-872ee3614d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6aafb3-7f2e-4982-a9f0-92d53cd7a5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48151e0b-6cc4-49ac-a8ce-c16f10a4c6ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6aafb3-7f2e-4982-a9f0-92d53cd7a5ce",
                    "LayerId": "26b72df2-b06d-4611-b63c-df09eec89871"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "26b72df2-b06d-4611-b63c-df09eec89871",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c0461ee-eef9-4b4b-b34c-18d2f8378128",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}