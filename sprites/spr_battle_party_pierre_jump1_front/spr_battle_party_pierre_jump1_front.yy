{
    "id": "38ca8ac1-3502-4046-9837-1f2773981a76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_jump1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 22,
    "bbox_right": 212,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebb141e2-30b7-49a6-a7b5-98efe3782a0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38ca8ac1-3502-4046-9837-1f2773981a76",
            "compositeImage": {
                "id": "78489a8c-f51f-4b8b-9425-fc90fcd34c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb141e2-30b7-49a6-a7b5-98efe3782a0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c44fe031-d728-410d-8489-165a0aace5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb141e2-30b7-49a6-a7b5-98efe3782a0e",
                    "LayerId": "47a03180-6e8e-4a7f-84d4-44eb2503194d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "47a03180-6e8e-4a7f-84d4-44eb2503194d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38ca8ac1-3502-4046-9837-1f2773981a76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}