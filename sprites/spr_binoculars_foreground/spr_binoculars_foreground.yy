{
    "id": "d283e6b0-c6ba-4fa8-b20b-26f825274286",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_binoculars_foreground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a5cb566-33fa-4ed4-a33c-8da75e10b54e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d283e6b0-c6ba-4fa8-b20b-26f825274286",
            "compositeImage": {
                "id": "9d48e2d4-70e1-4183-89ec-3ddfa4ce3cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a5cb566-33fa-4ed4-a33c-8da75e10b54e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7918a321-276d-4823-b970-75d5ab72f6f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a5cb566-33fa-4ed4-a33c-8da75e10b54e",
                    "LayerId": "060d0620-3cd7-4dcc-aae9-e949d645e36b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "060d0620-3cd7-4dcc-aae9-e949d645e36b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d283e6b0-c6ba-4fa8-b20b-26f825274286",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ac1c074f-77c8-4cff-8cc3-e8f1a62d4829",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}