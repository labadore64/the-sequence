{
    "id": "074429db-31dc-40ba-a1db-dd78d3325bf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_tricorn_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 51,
    "bbox_right": 228,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98fa7cc1-2732-4496-b18f-c8888bc558fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074429db-31dc-40ba-a1db-dd78d3325bf5",
            "compositeImage": {
                "id": "48b5684a-91f8-490f-af64-bc5d5cbfe8db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fa7cc1-2732-4496-b18f-c8888bc558fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "009414ab-39c8-4b8f-875f-abfd1fb1916f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fa7cc1-2732-4496-b18f-c8888bc558fa",
                    "LayerId": "5a138407-4ce5-43e1-901a-54fb38528cd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "5a138407-4ce5-43e1-901a-54fb38528cd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "074429db-31dc-40ba-a1db-dd78d3325bf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1da56509-3009-4dd8-9c99-99f6352cf90d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}