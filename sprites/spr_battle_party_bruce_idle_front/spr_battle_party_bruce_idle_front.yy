{
    "id": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_bruce_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 3,
    "bbox_right": 206,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "389ff109-f4fc-4b59-a4c9-d529e3647b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "16f15cd0-bc26-48b8-92c1-1c24ffbfe0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389ff109-f4fc-4b59-a4c9-d529e3647b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8447d40a-9d69-44b6-84d1-886a4515c727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389ff109-f4fc-4b59-a4c9-d529e3647b1f",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "c8c11f4a-e3a8-44ef-9349-e7e9f7668cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "3416a268-b1ae-4404-80a5-1a8658ea10d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8c11f4a-e3a8-44ef-9349-e7e9f7668cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e102fb58-4b35-4751-920c-159bd24f5bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8c11f4a-e3a8-44ef-9349-e7e9f7668cee",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "955d8d81-4dc1-437b-9f04-309247790357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "846fa845-6ed2-4e11-8a44-6af45c47831e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955d8d81-4dc1-437b-9f04-309247790357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e07823-5f26-4eeb-bd22-83d37cd512f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955d8d81-4dc1-437b-9f04-309247790357",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "df359796-320e-450e-b101-2898d49cda8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "529c1462-1342-4ba6-8b34-8d0a68cec31b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df359796-320e-450e-b101-2898d49cda8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8195a238-1999-4fc9-95b3-23cbf68d9063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df359796-320e-450e-b101-2898d49cda8b",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "10dda78e-ef69-452b-b7ea-81f9bd8e3d13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "13235193-107a-4c62-afd7-fe8f171ecdbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10dda78e-ef69-452b-b7ea-81f9bd8e3d13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c555ab-b685-4b16-b310-0bb7dd6ba38c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10dda78e-ef69-452b-b7ea-81f9bd8e3d13",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "4ccf2ce6-4eef-4cf2-bd13-15bc624710d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "dd53ff28-69e4-497c-b61a-b202e6c851c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ccf2ce6-4eef-4cf2-bd13-15bc624710d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5195aeef-8d89-4574-8ad3-2ad9dd40150f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ccf2ce6-4eef-4cf2-bd13-15bc624710d7",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "834a02a6-680f-4ba4-9e0b-f09f133cca74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "3430970e-d555-47ac-aec2-2c0e86f2b1fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "834a02a6-680f-4ba4-9e0b-f09f133cca74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5dff2a-70d7-4ed5-9393-ee3461c926ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "834a02a6-680f-4ba4-9e0b-f09f133cca74",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "23c2d1a1-17a9-4cf1-afc5-13c85f80497f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "5b7ba806-4772-4428-9466-44ea1cdbea55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c2d1a1-17a9-4cf1-afc5-13c85f80497f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "775f4e2c-6cff-45eb-ad25-0c36968a9378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c2d1a1-17a9-4cf1-afc5-13c85f80497f",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "dc4dd2b3-6a1f-4a55-ad5a-1a30edd7aaea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "2e463685-76ee-40de-8e83-18906456ea07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4dd2b3-6a1f-4a55-ad5a-1a30edd7aaea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5195e2-04c2-47f6-8c6e-f25a0e9d40ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4dd2b3-6a1f-4a55-ad5a-1a30edd7aaea",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "8d033ecb-c1c9-45bf-a38e-b67937ef2dd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "c7bcb95f-9b20-4113-abcc-941aeb1b23ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d033ecb-c1c9-45bf-a38e-b67937ef2dd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b09c8cf4-edfc-478b-aa4c-c4abf794601b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d033ecb-c1c9-45bf-a38e-b67937ef2dd7",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "653aa98c-4aac-42c8-b910-e7bb19a109ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "d67221c4-4526-47b4-ab4b-2ee808f4de37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "653aa98c-4aac-42c8-b910-e7bb19a109ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff98f130-1085-46c3-88c3-ed1ed6bc4ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "653aa98c-4aac-42c8-b910-e7bb19a109ff",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "0f381d67-49e8-4844-95e0-7a0fbef7900b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "a9184f96-7115-4b8a-bacc-4cb64bdf408d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f381d67-49e8-4844-95e0-7a0fbef7900b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2295c59c-31ef-4816-83e5-b91318921bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f381d67-49e8-4844-95e0-7a0fbef7900b",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "34b412ab-5ba7-4576-a134-c85ede6f11b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "323bf002-2bf0-4bef-9a36-f635adf8f92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b412ab-5ba7-4576-a134-c85ede6f11b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93017092-4716-4899-8a27-b34c6cc42136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b412ab-5ba7-4576-a134-c85ede6f11b7",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "7aa82727-bab0-48d3-a945-af9d9cbf1b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "06e94204-b859-414e-ba4d-27dd4fe72de2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa82727-bab0-48d3-a945-af9d9cbf1b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1504c1-7463-4a2a-8349-e3cd47c5c43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa82727-bab0-48d3-a945-af9d9cbf1b13",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "647788fa-19e2-4b7d-8ebf-4f65cfe5f1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "bdcda40d-c1ad-44b9-9575-c66560de3658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647788fa-19e2-4b7d-8ebf-4f65cfe5f1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "931e4aff-637d-4228-af9a-e8d348e56ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647788fa-19e2-4b7d-8ebf-4f65cfe5f1cf",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "3685610a-a8a7-4def-8945-980d3181db47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "f226ed8b-7049-405f-ad55-3c42ea8fa094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3685610a-a8a7-4def-8945-980d3181db47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6923f76f-0341-44d9-9f18-f6e4e9d6588c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3685610a-a8a7-4def-8945-980d3181db47",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "4bf41ecd-1137-40f8-9636-cbaab81a477f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "dd11f9d3-381f-4eb3-bec4-f26b8e912747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf41ecd-1137-40f8-9636-cbaab81a477f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0df39e-8228-4120-b81b-402318c132aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf41ecd-1137-40f8-9636-cbaab81a477f",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "ffb50aa5-7dfc-4dfb-8863-24c85f970b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "52270227-0cbe-44a5-b205-3c65f9cbcaad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb50aa5-7dfc-4dfb-8863-24c85f970b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d5c6dbb-1b30-4da5-928a-4ed6c8e6998e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb50aa5-7dfc-4dfb-8863-24c85f970b08",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "7bb68fbb-b6a2-489f-935b-e5a0d2b3d1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "ca4fd36a-3274-46dc-aeaa-8a8d784d1848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bb68fbb-b6a2-489f-935b-e5a0d2b3d1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "922e74c4-f0ce-417e-8509-c1e735698baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bb68fbb-b6a2-489f-935b-e5a0d2b3d1eb",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "aea577e4-58e1-49cb-af60-92b6ddf4b6a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "84e6f695-247f-47c1-8bcf-c8a79ba5acca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea577e4-58e1-49cb-af60-92b6ddf4b6a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbe49abe-b13c-4c3e-a1c9-f24ac18de79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea577e4-58e1-49cb-af60-92b6ddf4b6a3",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "a9de3a76-6ef4-48b2-a104-aa61326132eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "a1cc03e5-3e9a-4f00-9e66-a7dc337e3a41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9de3a76-6ef4-48b2-a104-aa61326132eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a28ff318-84eb-4859-b033-5a851fbec3bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9de3a76-6ef4-48b2-a104-aa61326132eb",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "6658c3e7-0d9a-42c9-ab38-66e3e1fe96dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "d42e8dcd-3357-46ea-bfbf-3c5b890c5cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6658c3e7-0d9a-42c9-ab38-66e3e1fe96dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba894ca8-f484-4407-857a-36ab77ab5020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6658c3e7-0d9a-42c9-ab38-66e3e1fe96dc",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "46d30657-2ca7-4bf9-89b8-0137754dc651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "d5d4bcfe-63e1-45d9-855d-576ecb4e8299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d30657-2ca7-4bf9-89b8-0137754dc651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89093699-56b2-49cb-aeea-04ffa1643f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d30657-2ca7-4bf9-89b8-0137754dc651",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "8eae5a49-90b0-49e4-a4f7-08097b98b5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "d2e569a2-e89d-44de-8b92-9282665d3439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eae5a49-90b0-49e4-a4f7-08097b98b5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5720495-3cb4-4004-b112-328be5002386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eae5a49-90b0-49e4-a4f7-08097b98b5fa",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "b73962a4-1361-4d4c-946e-98c9802bb3b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "c4f6635a-89ef-4cf7-8896-077db11e3921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b73962a4-1361-4d4c-946e-98c9802bb3b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904ccb70-c021-4561-b2db-39477711b79b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b73962a4-1361-4d4c-946e-98c9802bb3b7",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "9cac7e39-a939-4180-8f0c-a4d2c5695b76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "c2ad11ac-0642-4434-bf7d-3f76d0fb6ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cac7e39-a939-4180-8f0c-a4d2c5695b76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f21a7141-928c-4cce-a4fd-d6c204d05e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cac7e39-a939-4180-8f0c-a4d2c5695b76",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "f8fc1f4c-9ca2-44a9-bea5-987553e7b827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "636fef05-395b-4d4e-b4c1-6c3a124395d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8fc1f4c-9ca2-44a9-bea5-987553e7b827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ab89327-0504-4d27-8769-9fcf88d9a40b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8fc1f4c-9ca2-44a9-bea5-987553e7b827",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "9595f812-717d-45d2-bae2-38d2d4e504e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "3d157e0d-19aa-4ae6-bf1f-5fbf533d4007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9595f812-717d-45d2-bae2-38d2d4e504e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e16f1265-23a2-4044-98d6-485a0d198257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9595f812-717d-45d2-bae2-38d2d4e504e6",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "906a0a99-fe85-47ea-ac17-383e57b4294d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "bc6da415-1f9a-4509-9160-62d999aa6624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "906a0a99-fe85-47ea-ac17-383e57b4294d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f9135c-415e-4b07-8657-e118fcaa92d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "906a0a99-fe85-47ea-ac17-383e57b4294d",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        },
        {
            "id": "8d55ef90-c9fb-4b7f-9230-a1ef357e2223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "compositeImage": {
                "id": "360f7ba6-a762-4325-bf9f-65cfa523abc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d55ef90-c9fb-4b7f-9230-a1ef357e2223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8d0080-d50a-4f0f-bf9b-ef4a3560eb36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d55ef90-c9fb-4b7f-9230-a1ef357e2223",
                    "LayerId": "e51d1446-3f9b-4063-9fb5-64fffd747a8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e51d1446-3f9b-4063-9fb5-64fffd747a8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95e1e4b3-c753-4e8e-8da9-aa994749c5c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "885781c6-d6ce-4320-b0de-d7a48d43760d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}