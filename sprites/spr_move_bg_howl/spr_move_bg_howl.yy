{
    "id": "ece92b33-0eef-4693-b426-0c5f832e437a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_move_bg_howl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dfe6e1c-3913-4f72-802f-b5577cffc2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece92b33-0eef-4693-b426-0c5f832e437a",
            "compositeImage": {
                "id": "659527d1-0a8d-466f-a9ff-7ecaee63f1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dfe6e1c-3913-4f72-802f-b5577cffc2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "794e23d2-9ee3-42e3-9a6e-b4ac5c254585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dfe6e1c-3913-4f72-802f-b5577cffc2ac",
                    "LayerId": "dbd610c3-3468-449c-8933-5db00377fb02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "dbd610c3-3468-449c-8933-5db00377fb02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ece92b33-0eef-4693-b426-0c5f832e437a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.50025,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}