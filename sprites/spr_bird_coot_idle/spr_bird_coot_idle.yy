{
    "id": "f668bad7-213f-4b60-8d95-626e6b699bdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_coot_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 53,
    "bbox_right": 108,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b64159c-8b0f-432e-be68-c605f4015db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f668bad7-213f-4b60-8d95-626e6b699bdc",
            "compositeImage": {
                "id": "848b6d67-8764-4b69-904b-5c7223a3055d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b64159c-8b0f-432e-be68-c605f4015db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d8b4aa-1afd-4ab5-89f9-94d80689b2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b64159c-8b0f-432e-be68-c605f4015db5",
                    "LayerId": "2d4c86db-f086-41e9-9375-662339055d4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2d4c86db-f086-41e9-9375-662339055d4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f668bad7-213f-4b60-8d95-626e6b699bdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}