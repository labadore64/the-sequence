{
    "id": "55a9340c-7356-4010-b3e5-003ff1a97a49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_Black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "464d940b-cd52-4be8-8ea5-5ec9b1634a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55a9340c-7356-4010-b3e5-003ff1a97a49",
            "compositeImage": {
                "id": "d0eb0f55-eda5-4cf6-b017-e565885f5958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464d940b-cd52-4be8-8ea5-5ec9b1634a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47754c86-de81-47f6-a224-5f1732b0577e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464d940b-cd52-4be8-8ea5-5ec9b1634a12",
                    "LayerId": "55d2819a-3123-445a-b20e-12d1afac8ee7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "55d2819a-3123-445a-b20e-12d1afac8ee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55a9340c-7356-4010-b3e5-003ff1a97a49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}