{
    "id": "0478e38b-6ef6-4b5a-bbcd-7c2643ff6328",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_mirror",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 216,
    "bbox_left": 50,
    "bbox_right": 212,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bb58df9-6e82-43f9-b281-a198b14d2aff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0478e38b-6ef6-4b5a-bbcd-7c2643ff6328",
            "compositeImage": {
                "id": "7a8e68af-65ce-43e6-bde5-50aa602f1210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bb58df9-6e82-43f9-b281-a198b14d2aff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dfb2e4e-48c3-4512-80ac-df2ffa3a4691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bb58df9-6e82-43f9-b281-a198b14d2aff",
                    "LayerId": "f934231e-d114-405b-9dc7-815ebc06c3d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f934231e-d114-405b-9dc7-815ebc06c3d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0478e38b-6ef6-4b5a-bbcd-7c2643ff6328",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}