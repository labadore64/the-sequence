{
    "id": "6df9a92f-b158-4801-9eb0-4ccadf8ccf48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_yourHouse_closed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 10,
    "bbox_right": 121,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f536044e-30e9-4b8c-a56c-c0be5584166e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6df9a92f-b158-4801-9eb0-4ccadf8ccf48",
            "compositeImage": {
                "id": "ce5f0e10-bb71-4d39-9e46-aadce6ce39a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f536044e-30e9-4b8c-a56c-c0be5584166e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc45809e-ad1d-4d34-8340-af13938f6c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f536044e-30e9-4b8c-a56c-c0be5584166e",
                    "LayerId": "ac2ade94-08b0-49fd-8051-57f88adca1fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ac2ade94-08b0-49fd-8051-57f88adca1fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6df9a92f-b158-4801-9eb0-4ccadf8ccf48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}