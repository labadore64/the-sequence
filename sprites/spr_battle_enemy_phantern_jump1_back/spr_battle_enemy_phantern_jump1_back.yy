{
    "id": "9d9f9fdd-b297-4999-862f-a96ceb0a79c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 77,
    "bbox_right": 183,
    "bbox_top": 107,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a71bd50-dacb-404c-bd83-330c16c3796f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9f9fdd-b297-4999-862f-a96ceb0a79c6",
            "compositeImage": {
                "id": "91c6be8c-cb88-4ddd-9090-1d998eee7605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a71bd50-dacb-404c-bd83-330c16c3796f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a071a53-09ff-4090-b3ee-f9db28b370e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a71bd50-dacb-404c-bd83-330c16c3796f",
                    "LayerId": "e6a49be7-acef-4534-bb1d-8d49a5da7d49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e6a49be7-acef-4534-bb1d-8d49a5da7d49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d9f9fdd-b297-4999-862f-a96ceb0a79c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}