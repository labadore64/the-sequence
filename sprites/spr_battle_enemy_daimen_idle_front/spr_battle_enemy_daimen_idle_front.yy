{
    "id": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_daimen_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 3,
    "bbox_right": 243,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e297f1a3-ed85-41c2-8745-d665c2f8c820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "13a0e64e-a7d7-4fe0-8362-a4db12dfe86a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e297f1a3-ed85-41c2-8745-d665c2f8c820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff95e792-34dc-435d-8feb-4e7a3cd7e483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e297f1a3-ed85-41c2-8745-d665c2f8c820",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "d88e603b-6b36-48bd-883e-4a91ae571f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "c78f7d69-02e3-4d3a-a1c4-ec39359f9542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88e603b-6b36-48bd-883e-4a91ae571f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef15a950-c3c7-45a1-b0e4-965bb03e378e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88e603b-6b36-48bd-883e-4a91ae571f68",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "c7978472-fe62-4ede-8346-9b1896732395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "4ddeb8fb-460e-42ce-8222-3d9651823a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7978472-fe62-4ede-8346-9b1896732395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f21cd3-8629-4d68-b1dd-8369ef341851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7978472-fe62-4ede-8346-9b1896732395",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "d883d26a-8dcb-4435-a8cf-ec37c166c5b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "9c20b59a-02ec-472f-a981-525002107e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d883d26a-8dcb-4435-a8cf-ec37c166c5b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e35141-233c-45af-aa5f-8aca4151f629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d883d26a-8dcb-4435-a8cf-ec37c166c5b6",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "9713ce81-ddea-483b-956a-6c4f4ae45681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "1bb02fef-eded-4093-8cbd-f65e68e12f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9713ce81-ddea-483b-956a-6c4f4ae45681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90a11cc9-30d2-4465-b40f-9ee776bb984a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9713ce81-ddea-483b-956a-6c4f4ae45681",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "49e3d452-1ecf-440c-8bf5-271f340781a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "191841a9-836b-4bec-a650-65cfafdb9940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49e3d452-1ecf-440c-8bf5-271f340781a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c0ac10-47f2-4a2e-a8ac-6e681ff36e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49e3d452-1ecf-440c-8bf5-271f340781a5",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "3987f216-304e-405b-9cca-4f3efc6fc54c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "999d0ed2-5c0e-4c44-ae44-bc99caf56a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3987f216-304e-405b-9cca-4f3efc6fc54c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "867a2a78-fad2-4e32-8a2f-a002af02972e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3987f216-304e-405b-9cca-4f3efc6fc54c",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "c01d483d-134c-4f4d-8b8f-b30235ade142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "7fe76d6e-bbd4-401f-ad03-48273306cb67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01d483d-134c-4f4d-8b8f-b30235ade142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a6ce3b-713a-4b0f-b470-e22ba8f4c2c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01d483d-134c-4f4d-8b8f-b30235ade142",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "a234b0fb-9211-42f2-98a9-0bf6c0a77aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "4fe9fab2-0d36-48bd-9816-9215715d6053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a234b0fb-9211-42f2-98a9-0bf6c0a77aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3c30c6-05b0-46d8-a5da-95bf8ee378d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a234b0fb-9211-42f2-98a9-0bf6c0a77aa2",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "0856bcbf-46f8-4e6f-a2ed-6c4677017522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "cb51aa8b-c17f-4d9c-b471-31e0aab99eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0856bcbf-46f8-4e6f-a2ed-6c4677017522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148b4b26-fd84-4275-8063-750bfb886986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0856bcbf-46f8-4e6f-a2ed-6c4677017522",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "ba39c1c6-6714-4a23-abc2-1b07bf25ac26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "64635bb2-8e3c-4a17-84b6-c4fcf558d4be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba39c1c6-6714-4a23-abc2-1b07bf25ac26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff9491bb-b9e6-481d-8b07-3bdb7402041e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba39c1c6-6714-4a23-abc2-1b07bf25ac26",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "762e45fe-53d7-4391-8353-5089edf298cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "c55bf88c-4452-4fef-b913-8212790444f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "762e45fe-53d7-4391-8353-5089edf298cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae44dab-6ec4-4f3f-aca4-77b7ab35c875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "762e45fe-53d7-4391-8353-5089edf298cc",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "3db72659-abfc-442d-a036-65d9cff7f49c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "a703986e-b537-4862-ac0d-caa6cb5e0746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db72659-abfc-442d-a036-65d9cff7f49c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c419e690-afee-4860-8a52-56b1646270e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db72659-abfc-442d-a036-65d9cff7f49c",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "79cfe18d-7987-4d5f-95a2-9f5c8a0b4a98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "b7551e5c-17c4-4c97-b591-db2967b6259c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79cfe18d-7987-4d5f-95a2-9f5c8a0b4a98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda4d6fd-b9bc-448e-b74e-d3628b9e1d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79cfe18d-7987-4d5f-95a2-9f5c8a0b4a98",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "5acf4e1b-87e4-4d73-8f6c-48465243eedd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "02187ff2-ee11-4527-87c0-dd52d09db682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5acf4e1b-87e4-4d73-8f6c-48465243eedd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68f518a2-e905-41ba-97be-0af46f24276a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5acf4e1b-87e4-4d73-8f6c-48465243eedd",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "ec94a753-1564-4391-86b9-f396ccb8194f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "5b310120-3388-459a-bf0e-6b21632a58af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec94a753-1564-4391-86b9-f396ccb8194f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220c8cde-b0b6-422e-9f3b-86aa2002b0bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec94a753-1564-4391-86b9-f396ccb8194f",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "8fec0b13-c6f5-4876-8904-94479a7716ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "8a3792ea-e5f3-4399-808e-7b65ba212c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fec0b13-c6f5-4876-8904-94479a7716ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f3d4481-9486-4443-9d16-f5f3df614f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fec0b13-c6f5-4876-8904-94479a7716ae",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "bd2e53c6-c4ff-4e94-a1d2-7ff655323264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "cd1c11d4-38fe-4889-bf4d-3fb878b5d025",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd2e53c6-c4ff-4e94-a1d2-7ff655323264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd4ae31-71df-43c0-baeb-34e49643d074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd2e53c6-c4ff-4e94-a1d2-7ff655323264",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "56b843b2-7aba-47eb-97b3-846109458245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "0737d05d-5efc-429b-87f5-e17245c817a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56b843b2-7aba-47eb-97b3-846109458245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "469c3b09-bb98-458d-b4f4-39d5d382bc30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56b843b2-7aba-47eb-97b3-846109458245",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        },
        {
            "id": "28399c85-0583-45cc-9255-58834588428f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "compositeImage": {
                "id": "8a9c8572-45a2-4ef6-bee8-78e2d0940287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28399c85-0583-45cc-9255-58834588428f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f193249-0482-4827-9e18-0eaf01016251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28399c85-0583-45cc-9255-58834588428f",
                    "LayerId": "63acfc82-5c68-4aea-81e2-64828e3bfd08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "63acfc82-5c68-4aea-81e2-64828e3bfd08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe8b158-1b72-4b5b-a9ef-b5ba4682be43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "4038d625-6d07-42e2-825f-bcc4b31256a3",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}