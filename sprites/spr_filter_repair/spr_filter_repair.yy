{
    "id": "36451369-120d-4380-8c66-ec2d308bf25c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_repair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 82,
    "bbox_right": 181,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e91e4cf-562a-4583-85a0-a892f3e86d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36451369-120d-4380-8c66-ec2d308bf25c",
            "compositeImage": {
                "id": "021135c8-7488-4b67-9b3a-b9c6b5164ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e91e4cf-562a-4583-85a0-a892f3e86d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b7cc7e-1ba4-4c64-88d0-dc9257e8344e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e91e4cf-562a-4583-85a0-a892f3e86d67",
                    "LayerId": "98b58042-6a31-4fa4-aa3c-e5bffb7a8594"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "98b58042-6a31-4fa4-aa3c-e5bffb7a8594",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36451369-120d-4380-8c66-ec2d308bf25c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 180
}