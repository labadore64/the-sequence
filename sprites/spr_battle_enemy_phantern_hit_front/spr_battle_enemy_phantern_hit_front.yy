{
    "id": "7390bf5e-289b-4610-98d9-8b7cbde43236",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_hit_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 78,
    "bbox_right": 183,
    "bbox_top": 121,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3d2b3ca-28bd-4389-a58d-042654fec672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7390bf5e-289b-4610-98d9-8b7cbde43236",
            "compositeImage": {
                "id": "9f2e0ed8-a326-41c6-9a44-98fa46f88067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d2b3ca-28bd-4389-a58d-042654fec672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a31b1e0c-eb70-4780-9365-95764cbb21bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d2b3ca-28bd-4389-a58d-042654fec672",
                    "LayerId": "948d2190-2bb3-4a95-9c2f-26fe15bca4ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "948d2190-2bb3-4a95-9c2f-26fe15bca4ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7390bf5e-289b-4610-98d9-8b7cbde43236",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}