{
    "id": "3f0c4e70-abb8-4028-ba2a-5b4d9bcb2591",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_bless",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 91,
    "bbox_right": 168,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4205bd11-9b16-4e34-9c60-1cda979e65ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f0c4e70-abb8-4028-ba2a-5b4d9bcb2591",
            "compositeImage": {
                "id": "bc28ca59-4551-4085-ac1d-2d876500b7a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4205bd11-9b16-4e34-9c60-1cda979e65ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5aaa687-95f4-45c3-a36b-cbca38d355fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4205bd11-9b16-4e34-9c60-1cda979e65ae",
                    "LayerId": "baaf2eed-5c13-4794-a121-945d3f355f41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "baaf2eed-5c13-4794-a121-945d3f355f41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f0c4e70-abb8-4028-ba2a-5b4d9bcb2591",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 140
}