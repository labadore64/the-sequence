{
    "id": "42ae5bf6-0af9-4e6d-b073-3e1d547d07e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_boombox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 9,
    "bbox_right": 117,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a236e13f-6ae4-468e-8a64-31e2def427d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ae5bf6-0af9-4e6d-b073-3e1d547d07e4",
            "compositeImage": {
                "id": "ddc0cf27-b9e1-4d01-9bce-7819bd36486f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a236e13f-6ae4-468e-8a64-31e2def427d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9916b1b7-1fec-471b-86e7-07d349509768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a236e13f-6ae4-468e-8a64-31e2def427d3",
                    "LayerId": "52bb6990-c816-4358-b26f-8e2e4f09e3d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "52bb6990-c816-4358-b26f-8e2e4f09e3d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42ae5bf6-0af9-4e6d-b073-3e1d547d07e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}