{
    "id": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_dipper_idle_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 53,
    "bbox_right": 193,
    "bbox_top": 114,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dafca1fe-b351-4c82-b7b3-6889a6eada3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "74a44818-775a-4772-9781-36b6024a31ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dafca1fe-b351-4c82-b7b3-6889a6eada3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53347d32-5620-4c71-9471-8ea8b28bfaa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dafca1fe-b351-4c82-b7b3-6889a6eada3c",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "6b5f3475-11fb-4b0e-a6c4-cf7cd3e9e925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "015701f0-2f36-47dc-ac2a-de781b218202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b5f3475-11fb-4b0e-a6c4-cf7cd3e9e925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475f7cde-70c4-4d01-900b-6110579c7c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b5f3475-11fb-4b0e-a6c4-cf7cd3e9e925",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "b0296e62-5a74-4840-91ae-2aa17226ef7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "36e05046-ae3c-418e-8494-a083d7270aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0296e62-5a74-4840-91ae-2aa17226ef7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "995c7ad4-6313-43c5-a636-f46f99b6ac6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0296e62-5a74-4840-91ae-2aa17226ef7d",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "0527614e-a462-44ab-88c3-5fade985bdf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "b66e7d10-cc8c-4959-be2e-e0858a0ed905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0527614e-a462-44ab-88c3-5fade985bdf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8139e658-b3ce-4c3a-badc-25c2bc85fa33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0527614e-a462-44ab-88c3-5fade985bdf3",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "fd3242a3-fdc3-41bb-8722-8604c2fc557e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "6d20e759-9dae-4d98-9309-87a148c42a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3242a3-fdc3-41bb-8722-8604c2fc557e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39094fec-4694-4429-9ffe-17987342b45d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3242a3-fdc3-41bb-8722-8604c2fc557e",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "34add7d4-51f9-4805-ad87-a8de8cdc8548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "d817753f-852f-47b7-9778-ba22ebf1ab5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34add7d4-51f9-4805-ad87-a8de8cdc8548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80c7eaa3-6e1d-4736-8c76-867cca6a3269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34add7d4-51f9-4805-ad87-a8de8cdc8548",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "84cb4e0e-2654-4013-8ede-4365c3ed4fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "6e33404b-b651-41bd-8113-ef76e0ca41d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84cb4e0e-2654-4013-8ede-4365c3ed4fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b30c70a-c169-480f-b264-ca9f2d4433a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84cb4e0e-2654-4013-8ede-4365c3ed4fc2",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "753d89e1-feab-4e08-a8ee-fb85964fb882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "f696ebdb-eb22-49fc-817e-a4737cfa4fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "753d89e1-feab-4e08-a8ee-fb85964fb882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc9e321-cabd-435d-a203-522434e42174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "753d89e1-feab-4e08-a8ee-fb85964fb882",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "514c576b-c26a-4fbe-92ad-b4c958743ae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "b2603367-ef95-459f-915f-efbd8d334283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514c576b-c26a-4fbe-92ad-b4c958743ae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a971d23a-06e9-44ef-8a8f-16de57be8d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514c576b-c26a-4fbe-92ad-b4c958743ae5",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "a7ccbbdf-f8aa-41c8-9686-cef31854a7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "e7c48b25-7977-40ef-879b-5467a90009d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ccbbdf-f8aa-41c8-9686-cef31854a7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c40e249-3ab1-4e96-a33f-cf55c6756bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ccbbdf-f8aa-41c8-9686-cef31854a7b3",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "084ddc6c-2112-408d-994a-10409aea3361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "b147cd70-bf35-4b02-a543-ebfedb47f522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084ddc6c-2112-408d-994a-10409aea3361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40175afa-a993-4449-8e66-15b8bc064e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084ddc6c-2112-408d-994a-10409aea3361",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "5148a7d1-722d-4985-96cd-ca2d6ec0f11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "300d8475-14bc-4134-bb73-f5b01d9d560a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5148a7d1-722d-4985-96cd-ca2d6ec0f11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01bba1b-a6e1-43b7-8d16-8f1d128d8712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5148a7d1-722d-4985-96cd-ca2d6ec0f11d",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "e326fa9f-3ffd-4715-8f21-d9238385d172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "8942b2d1-aa8c-439b-9342-27e685a42853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e326fa9f-3ffd-4715-8f21-d9238385d172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc6a85f-8fc3-4e87-ac80-16fcdd3682bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e326fa9f-3ffd-4715-8f21-d9238385d172",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        },
        {
            "id": "41eb1649-2cfe-4c5a-9739-9c04dd3b8eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "compositeImage": {
                "id": "e290122e-9409-4100-a164-6e77c3fdd1f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41eb1649-2cfe-4c5a-9739-9c04dd3b8eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c6d305-34ed-4a79-b708-c60f415b967e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41eb1649-2cfe-4c5a-9739-9c04dd3b8eb8",
                    "LayerId": "45c211a9-a231-421f-bfae-8120b6bc1898"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "45c211a9-a231-421f-bfae-8120b6bc1898",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "244e8b73-3d27-4a9a-88d4-afee13e292d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1e479664-c8d0-4e3e-9d43-b1d1b8e5d215",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}