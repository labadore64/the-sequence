{
    "id": "39c8d55f-4156-4700-bf68-010dab116f72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_cast0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 58,
    "bbox_right": 163,
    "bbox_top": 138,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b0abd65-d41b-4df3-92ab-929fcb406950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39c8d55f-4156-4700-bf68-010dab116f72",
            "compositeImage": {
                "id": "9cc3cd60-1054-4bf6-9eb9-d84061ebaca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0abd65-d41b-4df3-92ab-929fcb406950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476ac7c0-b25f-46b7-be6e-953dfe871a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0abd65-d41b-4df3-92ab-929fcb406950",
                    "LayerId": "c1043b73-66fe-45a1-a7fb-f4ea12590058"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c1043b73-66fe-45a1-a7fb-f4ea12590058",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39c8d55f-4156-4700-bf68-010dab116f72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}