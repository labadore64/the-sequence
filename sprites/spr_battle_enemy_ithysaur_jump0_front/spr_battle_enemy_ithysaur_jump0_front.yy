{
    "id": "0cf55d5d-b891-4209-9b5a-9ae91894d79d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_ithysaur_jump0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 192,
    "bbox_left": 10,
    "bbox_right": 238,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "988c1ce0-326f-4b69-8fdd-e026b6bffca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cf55d5d-b891-4209-9b5a-9ae91894d79d",
            "compositeImage": {
                "id": "6e343b23-d7c1-48b8-bdda-80f4c8535804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "988c1ce0-326f-4b69-8fdd-e026b6bffca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162d765d-1d66-414d-a334-1405a6fff022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "988c1ce0-326f-4b69-8fdd-e026b6bffca1",
                    "LayerId": "6892df4f-f9f9-4684-ae5c-65101427de9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6892df4f-f9f9-4684-ae5c-65101427de9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cf55d5d-b891-4209-9b5a-9ae91894d79d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "f06561cc-765a-44de-946d-9f8dc9dbab86",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}