{
    "id": "09f9e2ff-0025-46f8-85a7-eccd20b7a83f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_test_map_point",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bd6dc0c-39e0-41b8-ae77-d47cf3e95d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09f9e2ff-0025-46f8-85a7-eccd20b7a83f",
            "compositeImage": {
                "id": "2246a97e-e810-44e7-a441-e518df5841e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd6dc0c-39e0-41b8-ae77-d47cf3e95d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b7599f-ae50-455d-87af-f53988513227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd6dc0c-39e0-41b8-ae77-d47cf3e95d57",
                    "LayerId": "0309b700-9f87-41d6-bdf6-b913bd17ffed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0309b700-9f87-41d6-bdf6-b913bd17ffed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09f9e2ff-0025-46f8-85a7-eccd20b7a83f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 73,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}