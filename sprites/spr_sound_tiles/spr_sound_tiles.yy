{
    "id": "b6eb68e8-5161-46dd-85e2-7db56c4dda6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sound_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "417244b9-a8da-45c8-9154-45ec6926f778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6eb68e8-5161-46dd-85e2-7db56c4dda6e",
            "compositeImage": {
                "id": "e74ac446-ac20-4509-b131-767d8fc2f099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417244b9-a8da-45c8-9154-45ec6926f778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea5afce-ec14-4604-bd7a-e75fe6a7bf9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417244b9-a8da-45c8-9154-45ec6926f778",
                    "LayerId": "5e215dc8-a44c-4511-9177-2def2eb09fa5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5e215dc8-a44c-4511-9177-2def2eb09fa5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6eb68e8-5161-46dd-85e2-7db56c4dda6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 31,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}