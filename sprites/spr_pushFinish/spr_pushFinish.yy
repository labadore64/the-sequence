{
    "id": "2d2670df-ab69-4d01-a6ac-88df40dfef49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pushFinish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfbff804-0323-4d09-9808-81b6f18c9685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2670df-ab69-4d01-a6ac-88df40dfef49",
            "compositeImage": {
                "id": "dc24d15f-ae91-4a0a-ab71-467610e676c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfbff804-0323-4d09-9808-81b6f18c9685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32fe111-d48b-419b-9a81-09b6cbb74a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfbff804-0323-4d09-9808-81b6f18c9685",
                    "LayerId": "633b5944-f368-4296-9a77-9f9c8fd5da8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "633b5944-f368-4296-9a77-9f9c8fd5da8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d2670df-ab69-4d01-a6ac-88df40dfef49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 29,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}