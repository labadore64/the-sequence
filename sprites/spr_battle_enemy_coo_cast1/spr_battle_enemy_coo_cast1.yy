{
    "id": "6364b07c-a21d-4efa-9a73-89b69a6cfccb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_coo_cast1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 58,
    "bbox_right": 163,
    "bbox_top": 138,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54d30038-52df-4ef3-b88b-cc28dd8c2674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6364b07c-a21d-4efa-9a73-89b69a6cfccb",
            "compositeImage": {
                "id": "bb094481-cd00-4333-924f-e875bfe05f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d30038-52df-4ef3-b88b-cc28dd8c2674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14da8a02-e5b1-470e-b2dd-c6cdaa163b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d30038-52df-4ef3-b88b-cc28dd8c2674",
                    "LayerId": "aa361946-c269-49ef-9d47-0b727aa0de1d"
                }
            ]
        },
        {
            "id": "5561e700-16ae-4dcc-8627-fdbafee3e505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6364b07c-a21d-4efa-9a73-89b69a6cfccb",
            "compositeImage": {
                "id": "78217103-689b-427b-83b8-6f8a0a7b1e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5561e700-16ae-4dcc-8627-fdbafee3e505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e25a10-8131-4697-86d6-6257858eff8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5561e700-16ae-4dcc-8627-fdbafee3e505",
                    "LayerId": "aa361946-c269-49ef-9d47-0b727aa0de1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "aa361946-c269-49ef-9d47-0b727aa0de1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6364b07c-a21d-4efa-9a73-89b69a6cfccb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}