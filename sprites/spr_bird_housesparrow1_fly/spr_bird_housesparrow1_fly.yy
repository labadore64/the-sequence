{
    "id": "4bfddbe3-5d84-4aab-ab28-fe8f3a4d685e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_housesparrow1_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 20,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a7eb600-337a-4d0f-bc09-0748b04bff07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bfddbe3-5d84-4aab-ab28-fe8f3a4d685e",
            "compositeImage": {
                "id": "fe2a6f84-ea08-4cc3-aef2-f41ea28fc689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a7eb600-337a-4d0f-bc09-0748b04bff07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "721a3d1c-7fed-4b69-99c6-3301462e2aa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a7eb600-337a-4d0f-bc09-0748b04bff07",
                    "LayerId": "0666d813-4d37-4133-8446-c7be752ab4f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0666d813-4d37-4133-8446-c7be752ab4f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bfddbe3-5d84-4aab-ab28-fe8f3a4d685e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}