{
    "id": "1573f92f-b0f7-4aa3-8298-e9e65d713463",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_mikeChair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 41,
    "bbox_right": 86,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03a630a3-6f13-4e7d-8dcf-0b9fb80ab915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1573f92f-b0f7-4aa3-8298-e9e65d713463",
            "compositeImage": {
                "id": "ddd87b43-fac9-49d4-9adf-d95f60817a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a630a3-6f13-4e7d-8dcf-0b9fb80ab915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38aaf9f6-8c2c-426d-830d-6ac0bef71848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a630a3-6f13-4e7d-8dcf-0b9fb80ab915",
                    "LayerId": "f84d2801-651d-4261-a46f-cb14c863e8c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f84d2801-651d-4261-a46f-cb14c863e8c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1573f92f-b0f7-4aa3-8298-e9e65d713463",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}