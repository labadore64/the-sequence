{
    "id": "8d4d8f7c-6b80-4db6-a0cb-df2f78cffd17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_repeat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 92,
    "bbox_right": 163,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "676899c0-2fa5-4b7a-8d51-e23c098baa65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d4d8f7c-6b80-4db6-a0cb-df2f78cffd17",
            "compositeImage": {
                "id": "f43808cf-d244-4e20-a3ea-bddc30bbe980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676899c0-2fa5-4b7a-8d51-e23c098baa65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b23f6df0-23dd-49b6-88c0-07bb3b1391e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676899c0-2fa5-4b7a-8d51-e23c098baa65",
                    "LayerId": "154c4849-e4b1-42b1-9440-3aee2e5c5466"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "154c4849-e4b1-42b1-9440-3aee2e5c5466",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d4d8f7c-6b80-4db6-a0cb-df2f78cffd17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 180
}