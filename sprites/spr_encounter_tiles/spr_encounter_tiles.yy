{
    "id": "dbe9caf7-8915-4f92-8a06-9ea3f882d37e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_encounter_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a1a9ad2-9dcd-4cdf-a2c4-4e28742a0641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbe9caf7-8915-4f92-8a06-9ea3f882d37e",
            "compositeImage": {
                "id": "7cf5ee8a-2530-4e0d-b91e-a9f3f283de4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1a9ad2-9dcd-4cdf-a2c4-4e28742a0641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c107630a-b353-4056-aa64-2f2b55a3957f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1a9ad2-9dcd-4cdf-a2c4-4e28742a0641",
                    "LayerId": "d7e6e31c-1879-4cde-a43b-2096ae73e59e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d7e6e31c-1879-4cde-a43b-2096ae73e59e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbe9caf7-8915-4f92-8a06-9ea3f882d37e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 31,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}