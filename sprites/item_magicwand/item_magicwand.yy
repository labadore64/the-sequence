{
    "id": "7f72c05e-2cc5-49e2-8895-48b53ba01513",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_magicwand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 235,
    "bbox_left": 18,
    "bbox_right": 237,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b508ce6-2854-4c5d-8b33-11b7482ee5cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f72c05e-2cc5-49e2-8895-48b53ba01513",
            "compositeImage": {
                "id": "6215835a-23ad-494e-bfa5-fb4ff5ad75ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b508ce6-2854-4c5d-8b33-11b7482ee5cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba15838e-b747-4e18-915a-5bc572eec42d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b508ce6-2854-4c5d-8b33-11b7482ee5cf",
                    "LayerId": "2115b120-ca54-4504-8ebb-f869e1f45cb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2115b120-ca54-4504-8ebb-f869e1f45cb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f72c05e-2cc5-49e2-8895-48b53ba01513",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}