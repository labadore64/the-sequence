{
    "id": "84a43ac7-fddf-48d5-843f-bb3ed44b57b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_housesparrow0_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 20,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac40c2a6-8c23-4910-8f72-ef91b0358fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84a43ac7-fddf-48d5-843f-bb3ed44b57b1",
            "compositeImage": {
                "id": "78407e9e-7b78-4fd4-aa4b-fd6bc1597078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac40c2a6-8c23-4910-8f72-ef91b0358fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbaabba1-593d-4798-b9a3-37f0118e1471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac40c2a6-8c23-4910-8f72-ef91b0358fbb",
                    "LayerId": "c56ce53a-a924-4138-b36a-3256cd20c264"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c56ce53a-a924-4138-b36a-3256cd20c264",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84a43ac7-fddf-48d5-843f-bb3ed44b57b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}