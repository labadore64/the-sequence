{
    "id": "0b634513-41e1-4f91-9907-6a20210791ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_dry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 228,
    "bbox_left": 27,
    "bbox_right": 235,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "124ede57-f759-49bf-a891-71dfed25462c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b634513-41e1-4f91-9907-6a20210791ad",
            "compositeImage": {
                "id": "59aa9dd2-45ef-4415-a557-a26728b24b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124ede57-f759-49bf-a891-71dfed25462c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570fb205-1ce4-4f25-af33-7860c7f970fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124ede57-f759-49bf-a891-71dfed25462c",
                    "LayerId": "7ce85582-abab-4a9f-95c4-ca9f1cd4239e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "7ce85582-abab-4a9f-95c4-ca9f1cd4239e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b634513-41e1-4f91-9907-6a20210791ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}