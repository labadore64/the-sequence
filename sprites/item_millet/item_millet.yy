{
    "id": "7d3ed075-1df1-488e-bbfc-c2ea893d8981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_millet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 16,
    "bbox_right": 234,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb654d76-8d6d-4418-87b0-055b9937f8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3ed075-1df1-488e-bbfc-c2ea893d8981",
            "compositeImage": {
                "id": "95342998-5019-48cc-8e73-74101c47b280",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb654d76-8d6d-4418-87b0-055b9937f8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "076028ce-1f9b-4bf0-a1ed-eb2ab4c16bd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb654d76-8d6d-4418-87b0-055b9937f8ec",
                    "LayerId": "2bbcd89a-9941-4a1a-95ea-2ca19e86e374"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2bbcd89a-9941-4a1a-95ea-2ca19e86e374",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d3ed075-1df1-488e-bbfc-c2ea893d8981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}