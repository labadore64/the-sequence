{
    "id": "74392f67-46bb-4a7b-bcff-caf70411e70c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aspyre_cast0_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 6,
    "bbox_right": 249,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43fcd2ae-e23e-4a33-84d7-9d9ba00a4edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74392f67-46bb-4a7b-bcff-caf70411e70c",
            "compositeImage": {
                "id": "3cd842b3-d944-443c-ba63-307ac7503742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43fcd2ae-e23e-4a33-84d7-9d9ba00a4edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673b16e2-a382-4e9a-8dba-65c97e7d076f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43fcd2ae-e23e-4a33-84d7-9d9ba00a4edf",
                    "LayerId": "aa6c995d-83e2-4745-a85d-228bc87d6717"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "aa6c995d-83e2-4745-a85d-228bc87d6717",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74392f67-46bb-4a7b-bcff-caf70411e70c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "808f5ba0-e9e9-4dd6-a3e1-5fd671722977",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}