{
    "id": "f8b41fb1-1288-4cb8-b6a5-9b65b831bc8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactStep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282c8ee2-d215-452c-8852-d7f6d52d4597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8b41fb1-1288-4cb8-b6a5-9b65b831bc8f",
            "compositeImage": {
                "id": "001a39b1-ea17-496e-9100-0f544e044643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282c8ee2-d215-452c-8852-d7f6d52d4597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfd2cdaa-ca0c-4da0-8fb4-086d49cb74c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282c8ee2-d215-452c-8852-d7f6d52d4597",
                    "LayerId": "374ed545-d32b-47e1-b5b6-92fd216df17f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "374ed545-d32b-47e1-b5b6-92fd216df17f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8b41fb1-1288-4cb8-b6a5-9b65b831bc8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 25,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}