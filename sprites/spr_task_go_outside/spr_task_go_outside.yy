{
    "id": "11d2fbcb-f661-4815-ace0-5852deb73c88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_task_go_outside",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 6,
    "bbox_right": 114,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b6a8ad5-5422-44be-a5b3-9b305c417259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11d2fbcb-f661-4815-ace0-5852deb73c88",
            "compositeImage": {
                "id": "39f0a84a-6438-4364-ad08-3b023fa7d774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6a8ad5-5422-44be-a5b3-9b305c417259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33adc75e-e5ca-44de-8466-b4237e5cac01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6a8ad5-5422-44be-a5b3-9b305c417259",
                    "LayerId": "8f600fb3-b0bc-4299-9d2b-9e2635a2a9b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8f600fb3-b0bc-4299-9d2b-9e2635a2a9b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11d2fbcb-f661-4815-ace0-5852deb73c88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}