{
    "id": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_skullbird_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 54,
    "bbox_right": 184,
    "bbox_top": 115,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bfa8c31-d7dc-4b86-b175-9c6a33ecbbb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "a0a35376-2f43-46af-bfe3-a5acb35bb69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bfa8c31-d7dc-4b86-b175-9c6a33ecbbb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1619efb5-7bb2-4b3f-be35-879c6ef399d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bfa8c31-d7dc-4b86-b175-9c6a33ecbbb6",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "ea3d48cc-6c7e-4577-8429-6d0a44d43057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "f74d564e-274e-42ed-ac49-81091d4e142a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea3d48cc-6c7e-4577-8429-6d0a44d43057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0df6663-2dea-4671-9fd6-329477723907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea3d48cc-6c7e-4577-8429-6d0a44d43057",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "d40400e6-807a-4480-9ffc-ff0306b694e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "49a73ae8-15be-498f-8c33-58a083ff70e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d40400e6-807a-4480-9ffc-ff0306b694e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1482d65-b1ef-47d2-86e7-ff844c7e2329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d40400e6-807a-4480-9ffc-ff0306b694e6",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "09a9ed3a-5d68-46c5-8145-119ed5f82c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "1a4faa52-2200-4cf8-9300-1995b1b70036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a9ed3a-5d68-46c5-8145-119ed5f82c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3731346e-f786-462c-acf7-be962a2b779b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a9ed3a-5d68-46c5-8145-119ed5f82c66",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "ec91ba43-4f9c-4b6e-817d-88156d818cdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "78d56b09-05c1-42c3-aaac-2738b218b795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec91ba43-4f9c-4b6e-817d-88156d818cdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a716a77-dd84-4c86-bcb2-56a77b04138c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec91ba43-4f9c-4b6e-817d-88156d818cdc",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "e63f8e75-4490-48d2-9522-344d5742195b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "a6e4206e-73bf-41d4-b3f1-c1a533d19c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63f8e75-4490-48d2-9522-344d5742195b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13a0b19-08d6-4f7c-bc41-190752105244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63f8e75-4490-48d2-9522-344d5742195b",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "582c3cb5-f83e-4b72-b569-ec582d46191e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "d8722707-e0b0-4a1b-baf5-d2b01e2a29d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582c3cb5-f83e-4b72-b569-ec582d46191e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4494497c-5003-4aa9-8f3d-0fced9523b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582c3cb5-f83e-4b72-b569-ec582d46191e",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "f88aeafd-74fa-4c8d-a9db-9be704deabb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "b4fa3f70-c41b-4a18-8b26-2b84919cfb75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f88aeafd-74fa-4c8d-a9db-9be704deabb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e5d97d-bee4-4274-9570-c7609ced910c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f88aeafd-74fa-4c8d-a9db-9be704deabb7",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "083442c5-6033-494d-842d-65ac0c920d7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "58d1b9e2-ebc7-4239-ab8e-55eade8ab968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083442c5-6033-494d-842d-65ac0c920d7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5f95a6-da82-4bd1-80c0-34ee1ea0efec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083442c5-6033-494d-842d-65ac0c920d7e",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "9fb90550-1b83-4e0c-9b81-f22ef031c4a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "d90823a6-96b3-4a31-8f5a-2fc9d5307848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb90550-1b83-4e0c-9b81-f22ef031c4a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928a8ff9-0fab-4c32-96ba-9748713f9440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb90550-1b83-4e0c-9b81-f22ef031c4a6",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "e65e5cd1-d517-4b75-bcb9-799552a7ac61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "630cf9c4-83c0-48e1-981b-67d7ec35d9ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e65e5cd1-d517-4b75-bcb9-799552a7ac61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce98217b-576e-4c1b-8e19-19fdb3b74205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e65e5cd1-d517-4b75-bcb9-799552a7ac61",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "9fe5d068-cbee-4a8a-b94a-e26c84bbfa13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "c0f42c4c-c512-4e34-94b3-817cc559930e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe5d068-cbee-4a8a-b94a-e26c84bbfa13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2321e9d-150c-4d92-baea-374e3cd67f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe5d068-cbee-4a8a-b94a-e26c84bbfa13",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        },
        {
            "id": "d93483df-08e8-4464-9aea-7dbff4e4583c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "compositeImage": {
                "id": "8b22047f-e582-4752-bb28-ec7a11ccc7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93483df-08e8-4464-9aea-7dbff4e4583c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4324442-2e53-4fe6-a35b-80b076b513f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93483df-08e8-4464-9aea-7dbff4e4583c",
                    "LayerId": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ebe93ca1-bc5d-4460-bd53-5559db8a3c73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5baae418-8b6a-4d19-b41c-f02db8d2d59f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e557950-8e6b-4bcc-8a34-2600b78af370",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}