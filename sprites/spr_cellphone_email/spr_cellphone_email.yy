{
    "id": "77046fc2-26c8-414e-ac1f-f60599b000d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cellphone_email",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 6,
    "bbox_right": 58,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f87d318b-89f8-4802-8192-9e5a15ad236d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77046fc2-26c8-414e-ac1f-f60599b000d9",
            "compositeImage": {
                "id": "eec8ea5d-034b-4a5a-ad8c-d5e16984c488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87d318b-89f8-4802-8192-9e5a15ad236d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecabac65-cbca-4adb-a824-c6da07a811d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87d318b-89f8-4802-8192-9e5a15ad236d",
                    "LayerId": "cefebbbe-387a-49f7-ac1d-10ea2fe7c6b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cefebbbe-387a-49f7-ac1d-10ea2fe7c6b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77046fc2-26c8-414e-ac1f-f60599b000d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 4.307,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}