{
    "id": "a1a6a1d3-ec37-423f-a804-97cdebf75005",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 24,
    "bbox_right": 219,
    "bbox_top": 61,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "465861f0-0d74-4dc3-9a46-b172ae2e4630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1a6a1d3-ec37-423f-a804-97cdebf75005",
            "compositeImage": {
                "id": "12656fa2-9872-42b1-97dd-6ee466985e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "465861f0-0d74-4dc3-9a46-b172ae2e4630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e95b67-e8c6-49f1-8e40-03e44384f748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "465861f0-0d74-4dc3-9a46-b172ae2e4630",
                    "LayerId": "6e4c44e6-d775-4d84-b433-eb06315f0049"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6e4c44e6-d775-4d84-b433-eb06315f0049",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1a6a1d3-ec37-423f-a804-97cdebf75005",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}