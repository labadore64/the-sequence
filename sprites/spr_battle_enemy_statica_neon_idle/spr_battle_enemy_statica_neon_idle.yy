{
    "id": "61a2c910-7db0-4e25-b55a-d98a45483183",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_statica_neon_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 19,
    "bbox_right": 243,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86bcac24-7d0e-4b52-903a-ad3261bbc3e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "bb33e597-1bf6-4b4d-8fd5-45fd122a61b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86bcac24-7d0e-4b52-903a-ad3261bbc3e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda91395-598b-4fce-82af-5a8665fe8db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86bcac24-7d0e-4b52-903a-ad3261bbc3e4",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "d92866a4-2e46-4eb9-b5c4-763fcd4a9f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "d61a3a31-8f49-4555-bd3d-bb61a844e619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d92866a4-2e46-4eb9-b5c4-763fcd4a9f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad013d3-1906-4720-ae44-67542cbd60c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d92866a4-2e46-4eb9-b5c4-763fcd4a9f51",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "e405d60f-6f0a-4c68-8f52-f76abf471d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "3a2fa154-0f27-42df-99da-db7c06fac847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e405d60f-6f0a-4c68-8f52-f76abf471d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a75001-1804-4af1-becf-ff279da05617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e405d60f-6f0a-4c68-8f52-f76abf471d0d",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "426c1ed8-8346-4cef-94e8-36a3a260ed36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "cc0e9ad0-0232-4650-bf3c-76bf38d109d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426c1ed8-8346-4cef-94e8-36a3a260ed36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890ed02f-ce3a-43a8-89eb-3e7082ac9b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426c1ed8-8346-4cef-94e8-36a3a260ed36",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "0cbf9ce3-cdd8-4c65-9c5a-e82a72d3898f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "24d20d3e-0eaa-42ca-ad04-11d9437949a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbf9ce3-cdd8-4c65-9c5a-e82a72d3898f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16935fee-2737-41c7-8b7e-d8329167ef29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbf9ce3-cdd8-4c65-9c5a-e82a72d3898f",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "b402a569-a065-47cc-b194-f2c6f25809f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "ab586773-13d5-44e3-b75c-3da32a1aba76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b402a569-a065-47cc-b194-f2c6f25809f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f9a665-5bd6-4c50-99f0-d47cd2b74ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b402a569-a065-47cc-b194-f2c6f25809f4",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "c6d67ba7-1823-4d3e-84e5-3b2c3eac72cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "58037768-a068-491d-90ac-2aa51b5f12a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d67ba7-1823-4d3e-84e5-3b2c3eac72cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be978b1b-72d8-4a6e-9518-9c55047e7270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d67ba7-1823-4d3e-84e5-3b2c3eac72cf",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "15016a17-1805-4efd-963e-7f82d5d80bf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "571f7aa1-c0af-477a-ba47-20b2fe140e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15016a17-1805-4efd-963e-7f82d5d80bf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee33cd2-d824-4bbf-b443-452e562121aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15016a17-1805-4efd-963e-7f82d5d80bf7",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "904f6ec7-5ba8-4630-9338-5a6903bded4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "0f5bfaaf-84c7-4eed-8d65-f7250c61696f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "904f6ec7-5ba8-4630-9338-5a6903bded4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24215a0a-e44e-43d1-990b-4c54f8ec46ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "904f6ec7-5ba8-4630-9338-5a6903bded4c",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "f96762bf-e7c3-4f9c-b665-ff1509585cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "bfae624c-910b-4224-bea4-6f11f82f26ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96762bf-e7c3-4f9c-b665-ff1509585cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3880419a-a9d5-40c2-9bc5-dd39d4d97ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96762bf-e7c3-4f9c-b665-ff1509585cfd",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "dc2c53a7-0607-42af-a308-e84a3296c957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "80a7a266-c5b2-4256-b3fc-07c165a2e540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc2c53a7-0607-42af-a308-e84a3296c957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9787b1b8-919b-4a4e-a4b1-562f4eacd846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2c53a7-0607-42af-a308-e84a3296c957",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "2cbdc013-e6ee-4ae0-afc7-a6140ebd8d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "1b9781e7-5347-4b09-aad6-7e4a6562f248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbdc013-e6ee-4ae0-afc7-a6140ebd8d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e08d86f-453f-4897-8a57-160acc3554bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbdc013-e6ee-4ae0-afc7-a6140ebd8d12",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "df572916-84aa-4a59-8b83-a88e48cff4d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "c9391633-a477-4b4c-a606-e5d7fba9d942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df572916-84aa-4a59-8b83-a88e48cff4d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde09d75-edad-4866-8761-f6eae035cdab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df572916-84aa-4a59-8b83-a88e48cff4d1",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "06da4547-0b56-4f85-841a-675e951b3054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "c13d1239-d230-44e8-bb20-e7bee86544fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06da4547-0b56-4f85-841a-675e951b3054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97628b2f-b142-46ba-a554-67e1dfd03766",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06da4547-0b56-4f85-841a-675e951b3054",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "df588eaa-d263-4c03-a26f-456822c569e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "b7ec4faa-0290-4fa8-b0d6-30731edd5f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df588eaa-d263-4c03-a26f-456822c569e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22071c6-6474-4735-8b86-1e6ce6a820fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df588eaa-d263-4c03-a26f-456822c569e2",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "cda5d155-067f-4ceb-9d57-b075c5dde8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "aae4adbe-7917-4589-9649-ee7284401432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda5d155-067f-4ceb-9d57-b075c5dde8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a833cd7-d2e4-44b1-b077-657b5ebb6663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda5d155-067f-4ceb-9d57-b075c5dde8f4",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "d6084746-3dc1-4e2e-bfb1-2153d41bb066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "7788a931-0309-435f-801e-8a55dc84cdeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6084746-3dc1-4e2e-bfb1-2153d41bb066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "affad8bc-038e-448e-b201-fe1c8c828f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6084746-3dc1-4e2e-bfb1-2153d41bb066",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "89b0cab2-b062-427a-9050-c6c46b7bf56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "f5d92791-ccce-48c5-823d-e8e008474d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b0cab2-b062-427a-9050-c6c46b7bf56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fcbf6cc-423a-4ecd-8689-b02e3643b813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b0cab2-b062-427a-9050-c6c46b7bf56e",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "56bb03d4-33e4-42f5-8f7f-0f828a745030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "3443d5c3-f079-4299-b6ac-5fc683b42c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bb03d4-33e4-42f5-8f7f-0f828a745030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad97fc0a-0c44-481a-b591-6567ae6b3dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bb03d4-33e4-42f5-8f7f-0f828a745030",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "652f8016-6b7a-41c7-973c-d6bef24f2c75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "2d7ec046-0699-4eb6-b8aa-dea86c715287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652f8016-6b7a-41c7-973c-d6bef24f2c75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5dc9f9-6d9b-43f2-8b7c-845040614b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652f8016-6b7a-41c7-973c-d6bef24f2c75",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        },
        {
            "id": "1faa0ed5-cf3b-4d6c-9182-72c92bf217ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "compositeImage": {
                "id": "9bfc9a70-9d9c-4b05-b6b8-45373103765c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1faa0ed5-cf3b-4d6c-9182-72c92bf217ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5635f645-a85e-4e23-a964-e91b99c04ad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1faa0ed5-cf3b-4d6c-9182-72c92bf217ea",
                    "LayerId": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "3395b58b-3b10-4eaf-b7e4-c1dab61d4f19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61a2c910-7db0-4e25-b55a-d98a45483183",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "02376ac4-5c41-4d41-862b-c0ab58d85802",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}