{
    "id": "3796649f-183b-45ef-b7af-8f384a2328a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aloesant_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 194,
    "bbox_top": 97,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "220f093d-7b23-4818-96b3-fff12ce2edc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "d330f65d-4922-47ae-a3e5-5faec9a31601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220f093d-7b23-4818-96b3-fff12ce2edc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f05a49-fcb4-41b7-af19-49fe24ea3969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220f093d-7b23-4818-96b3-fff12ce2edc5",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "30afde0f-d9f1-4bed-ad9e-0a3c8b8a8fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "e8829cfc-c340-430a-98a3-f6070681dd90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30afde0f-d9f1-4bed-ad9e-0a3c8b8a8fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bfe30a9-8f32-44b5-8952-80c8a3dc916b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30afde0f-d9f1-4bed-ad9e-0a3c8b8a8fa5",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "3a413c07-00fd-444f-8d64-85a15fb057b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "89dee3b6-91ea-460f-9ca6-2e174a6d8903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a413c07-00fd-444f-8d64-85a15fb057b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a792706f-ce46-48b8-a4d4-88f831012506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a413c07-00fd-444f-8d64-85a15fb057b8",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "4e87169a-4e76-4aa7-8895-1b8d33f70569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "e39f2d99-1046-42d4-a69e-d32ef328f060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e87169a-4e76-4aa7-8895-1b8d33f70569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd4cc389-20c4-4593-9ee7-9f78186c1f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e87169a-4e76-4aa7-8895-1b8d33f70569",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "9a2dabc0-ff26-44a7-aee8-ae4e932c8efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "f480ba51-2b35-4d9f-a98d-6df99b98d5a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a2dabc0-ff26-44a7-aee8-ae4e932c8efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afde4293-5b0f-4dc4-a1ef-d7f58dfbe5ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a2dabc0-ff26-44a7-aee8-ae4e932c8efe",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "ca9fc0d4-b69d-4d0c-9e8d-d36dbeda67db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "86ec1d3e-d35e-437a-93b8-5ebfc57c5d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca9fc0d4-b69d-4d0c-9e8d-d36dbeda67db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d74f128f-0955-457a-99f1-25883d73dfbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca9fc0d4-b69d-4d0c-9e8d-d36dbeda67db",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "b1d78686-8a74-4e41-9dd4-466df9d3f9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "d1638367-0641-4839-8c18-2fcb84cfecf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1d78686-8a74-4e41-9dd4-466df9d3f9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0eda9d1-3ea1-4efb-878d-38a6227d7be4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d78686-8a74-4e41-9dd4-466df9d3f9cc",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "50f2b359-0117-49e4-bca9-57d0f6a18479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "4c47fda1-c5dd-435b-bfc2-7c5b1d7639fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f2b359-0117-49e4-bca9-57d0f6a18479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeea4722-1486-46bd-9943-86fe05d3531f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f2b359-0117-49e4-bca9-57d0f6a18479",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "669008a6-93fd-4775-8768-c867c38e47e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "c596ac70-b382-4222-8858-245b4c34d96f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "669008a6-93fd-4775-8768-c867c38e47e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75a9064-a879-42f6-8260-a5d1119386af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "669008a6-93fd-4775-8768-c867c38e47e9",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "87043683-7934-46b0-a212-ce6f381b7d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "e94996de-d0e1-4757-8aa9-67ebde2c2b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87043683-7934-46b0-a212-ce6f381b7d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c14821-0799-419c-b0a2-2d14ee719ee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87043683-7934-46b0-a212-ce6f381b7d35",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "9bfb5b45-9caf-4287-91b7-c8c83ec83ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "55a8c853-5e06-42f3-921c-41518dab84dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bfb5b45-9caf-4287-91b7-c8c83ec83ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea050c5-f802-4495-8ec1-da3ee02ee8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bfb5b45-9caf-4287-91b7-c8c83ec83ae1",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "79d40b13-409d-4844-b93a-10742b96f6d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "c0caec29-0a0b-487a-8efc-104fd966fadd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d40b13-409d-4844-b93a-10742b96f6d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c05547-6e90-4174-9f36-691ed7894285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d40b13-409d-4844-b93a-10742b96f6d1",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "0b2da33a-9987-412c-9c82-07d6edf988dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "f6235692-a794-4f20-9ea3-689749f645e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b2da33a-9987-412c-9c82-07d6edf988dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c90906-609a-4e9b-a941-fd76c36beb39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b2da33a-9987-412c-9c82-07d6edf988dd",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "f0cb04ce-a065-475a-9c10-77a249186fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "783f59a2-63d8-4eef-a05b-b496b604bd0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0cb04ce-a065-475a-9c10-77a249186fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c25f4e6-0819-4ddc-b881-8e121df5ad26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0cb04ce-a065-475a-9c10-77a249186fcc",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "9158c780-43e9-4a83-b54b-d2a91add7cb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "b81ae86e-071c-46aa-a0b8-5f9071424df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9158c780-43e9-4a83-b54b-d2a91add7cb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d32c91a6-258a-4371-8a63-fc67996e129a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9158c780-43e9-4a83-b54b-d2a91add7cb5",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "a498d2e4-1ff2-4f44-aa4b-236cac20d22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "105e80c6-191f-4653-8af4-6d31e27df662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a498d2e4-1ff2-4f44-aa4b-236cac20d22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62750caa-e55e-4110-b8fb-019c1afcd2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a498d2e4-1ff2-4f44-aa4b-236cac20d22d",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "e7c73fc0-e686-41b8-9bcc-573f56199859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "484e7957-d2df-4b52-9c4c-a92e94f751d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c73fc0-e686-41b8-9bcc-573f56199859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d42bf654-01bb-4a0f-8360-687f2ff0a36f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c73fc0-e686-41b8-9bcc-573f56199859",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "1dc0370b-4756-49c2-abed-4bdfa97fbe42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "4656dc3e-9565-4de0-93fa-7c0dc36ae375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc0370b-4756-49c2-abed-4bdfa97fbe42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14610583-c767-4582-90c6-ca2f01cf66b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc0370b-4756-49c2-abed-4bdfa97fbe42",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "b48090ed-6451-4196-bf98-472069810c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "d8cd92a7-434a-4e6f-ae42-10c63b2170cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48090ed-6451-4196-bf98-472069810c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6fcabfc-f83a-4b57-9e04-8daf7136de0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48090ed-6451-4196-bf98-472069810c10",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "33b4b650-c4f7-4f83-8e13-3cced7388b56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "964d97bd-2ead-4a55-b2eb-689649424904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33b4b650-c4f7-4f83-8e13-3cced7388b56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c702ac81-3392-4de7-8411-6d5e78cbae9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33b4b650-c4f7-4f83-8e13-3cced7388b56",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "f6e7108c-278a-4957-84b0-c1390cf8fc68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "6469ef4c-bad5-4c88-b82e-52e79cf6ada8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e7108c-278a-4957-84b0-c1390cf8fc68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac9637d-2211-4154-9cb6-e59dbd8817ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e7108c-278a-4957-84b0-c1390cf8fc68",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "b41371e6-3452-447f-912f-c92e99a92eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "5d951227-21ea-4581-b298-66b49b91bf23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41371e6-3452-447f-912f-c92e99a92eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1268a03-5a6c-40a0-a201-d8b263f4a5d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41371e6-3452-447f-912f-c92e99a92eef",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "2d2e35a8-823b-46cc-9009-bf3b79411b65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "28a173f9-81b5-40c4-9d66-df04de64b24c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d2e35a8-823b-46cc-9009-bf3b79411b65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40f7bbf8-5889-4456-a49c-07a3ecbe21ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d2e35a8-823b-46cc-9009-bf3b79411b65",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "647b66c1-2f85-4c45-b26c-20bcc7c8f984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "e8850765-f24a-4b7e-bfac-fe2cb95d5de3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647b66c1-2f85-4c45-b26c-20bcc7c8f984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e449d03-9fd6-4439-a9f5-1ed0edc62c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647b66c1-2f85-4c45-b26c-20bcc7c8f984",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "1a44d04e-4a5a-4912-b816-735a0d28018a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "16a3ebb5-3c1e-48e5-b2a2-32d11ac04427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a44d04e-4a5a-4912-b816-735a0d28018a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64b8d8db-b9b0-49fd-baa3-44bd7b9f63cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a44d04e-4a5a-4912-b816-735a0d28018a",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "8b979f88-db40-480d-adcf-5b804787ae0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "efa14111-ae66-4e81-8a64-8c3deb7db142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b979f88-db40-480d-adcf-5b804787ae0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8e89a7-1a75-4712-a719-1e6a3d88260a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b979f88-db40-480d-adcf-5b804787ae0b",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "bc950d9c-af7f-464c-8eb9-38c10f2dda26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "3ef6e5aa-7c56-47e3-aa43-3f6c3fcf3488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc950d9c-af7f-464c-8eb9-38c10f2dda26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7357497c-e7b3-4abd-a8b0-c6f48a31cc5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc950d9c-af7f-464c-8eb9-38c10f2dda26",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "70d03de4-3e4a-46f9-ad5a-02d6632ed652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "f284ef03-a2f4-4603-a303-616a3a9df70d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70d03de4-3e4a-46f9-ad5a-02d6632ed652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c22222db-2145-4ffe-9089-433cd67b198e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70d03de4-3e4a-46f9-ad5a-02d6632ed652",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "bdaa8a47-5dbd-45d4-a291-aed860fe6e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "9733d44c-8a21-4931-a5c9-84b1519055c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdaa8a47-5dbd-45d4-a291-aed860fe6e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812f1909-3197-4069-8c90-f4ffb5a9deea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdaa8a47-5dbd-45d4-a291-aed860fe6e5c",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "e8b453c2-10b1-427d-aa7e-4a62ab457c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "9d79b669-0c9d-4943-954e-9bc0d754cb59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b453c2-10b1-427d-aa7e-4a62ab457c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2d4c87-63f0-4a44-b2b2-c19660b0b869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b453c2-10b1-427d-aa7e-4a62ab457c65",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "9f73e9f7-012b-44a2-bea3-8d84ab9bc5fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "9d665ed5-a4a4-412c-a046-6800d62647eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f73e9f7-012b-44a2-bea3-8d84ab9bc5fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e7e1fb-9bee-4e75-8906-625d5bf02832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f73e9f7-012b-44a2-bea3-8d84ab9bc5fd",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "2ec06057-382c-4c23-8079-26a2910f0ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "9422ebc2-1df4-45d9-bc97-ed2323ead31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ec06057-382c-4c23-8079-26a2910f0ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135188f9-d66c-4b4b-af4a-0128c7e46edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ec06057-382c-4c23-8079-26a2910f0ca6",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "c5b6d0ed-dd69-4e50-a459-dae6e6d52bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "2a8feac2-efc6-491a-8e12-e461be74939f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b6d0ed-dd69-4e50-a459-dae6e6d52bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0464afde-b8ad-4056-8854-003246c0ee80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b6d0ed-dd69-4e50-a459-dae6e6d52bb1",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "b2efe4ab-00d9-4455-85ce-54b63bcd3f4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "63ea4b77-07a3-4729-93c4-b29b9cfaff48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2efe4ab-00d9-4455-85ce-54b63bcd3f4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f97ca6-d60d-4d44-ac7f-5edf68b961a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2efe4ab-00d9-4455-85ce-54b63bcd3f4a",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "2e31f427-4a98-4885-bda1-7dae5cbe1201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "35c6cbee-259c-4b48-a177-f4c478eae30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e31f427-4a98-4885-bda1-7dae5cbe1201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ae98a1-3793-4d72-9773-058ad36421a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e31f427-4a98-4885-bda1-7dae5cbe1201",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "97fbba18-5191-4517-a382-7327fb834b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "7407a488-1407-455c-8756-fbaeec150dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fbba18-5191-4517-a382-7327fb834b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f2c62e-ee6d-4c0a-8409-bf388cc4365d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fbba18-5191-4517-a382-7327fb834b5f",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "4601f25b-3edd-4a50-a7ba-cde790c99c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "d83517f1-c7ef-4325-b073-fe49d3280071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4601f25b-3edd-4a50-a7ba-cde790c99c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709f12a8-d5bf-40ae-8939-801ff54ee9d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4601f25b-3edd-4a50-a7ba-cde790c99c5e",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "8f1197dd-1bab-4417-b01a-2f83e3797cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "81f80375-86eb-42b3-8553-dfdc47042731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1197dd-1bab-4417-b01a-2f83e3797cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad4d18b1-327b-4c59-8b7c-caf09711e66e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1197dd-1bab-4417-b01a-2f83e3797cd8",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "41a77c03-6e01-4e4b-8c0e-ce06201e6931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "61f5575d-2ad6-49c9-85f1-bc40e20cecb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a77c03-6e01-4e4b-8c0e-ce06201e6931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6864de7-305a-4fc9-a9e5-de531733d782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a77c03-6e01-4e4b-8c0e-ce06201e6931",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        },
        {
            "id": "cf41201f-69a7-4162-8717-d6f5da1ba366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "compositeImage": {
                "id": "efce915e-6906-4c1d-b3d8-c223b15359a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf41201f-69a7-4162-8717-d6f5da1ba366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b85aef-0b0b-4545-a9bc-c928ae57b3d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf41201f-69a7-4162-8717-d6f5da1ba366",
                    "LayerId": "9bf5c241-a147-4a44-bd0b-7a546eef4917"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9bf5c241-a147-4a44-bd0b-7a546eef4917",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3796649f-183b-45ef-b7af-8f384a2328a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "a007c71a-638e-4605-b559-ceb379856bdd",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}