{
    "id": "849ed778-c4e6-498a-b7cf-6c5597a8d587",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_pierre_jump1_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 31,
    "bbox_right": 212,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "070f23db-8ce4-4ec7-9c20-86870a3f4c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849ed778-c4e6-498a-b7cf-6c5597a8d587",
            "compositeImage": {
                "id": "cfee5371-3c09-4db1-b618-eb20917cb177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070f23db-8ce4-4ec7-9c20-86870a3f4c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664f7fe2-38b0-444c-98da-dc5af63489d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070f23db-8ce4-4ec7-9c20-86870a3f4c05",
                    "LayerId": "2255f9b8-9ec1-436b-b911-93f141f82fad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "2255f9b8-9ec1-436b-b911-93f141f82fad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "849ed778-c4e6-498a-b7cf-6c5597a8d587",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}