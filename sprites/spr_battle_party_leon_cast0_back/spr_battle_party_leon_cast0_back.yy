{
    "id": "7b2513d6-ac6e-4b8d-810c-5dc64e106ffe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_party_leon_cast0_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 55,
    "bbox_right": 205,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ffbecd2-0d96-4a76-818c-ea320542ac93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b2513d6-ac6e-4b8d-810c-5dc64e106ffe",
            "compositeImage": {
                "id": "5d5147ad-f577-4dae-a475-ee71027c6058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ffbecd2-0d96-4a76-818c-ea320542ac93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66d5c104-1f98-48ab-b8cb-8dd82e4e341b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ffbecd2-0d96-4a76-818c-ea320542ac93",
                    "LayerId": "9a1db349-57fe-4375-b518-d1affdb696ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "9a1db349-57fe-4375-b518-d1affdb696ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b2513d6-ac6e-4b8d-810c-5dc64e106ffe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}