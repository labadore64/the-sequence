{
    "id": "16f8b3d6-800d-4aa1-96e8-ccf670bce006",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battlebg_bird_statue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d6d714b-28e5-40de-98dc-3c1de97bbaee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16f8b3d6-800d-4aa1-96e8-ccf670bce006",
            "compositeImage": {
                "id": "01e5cf6b-3c70-4b00-a765-e3e0e31f7a9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6d714b-28e5-40de-98dc-3c1de97bbaee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd1f87b-bd81-4f08-ace9-31574bf7a095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6d714b-28e5-40de-98dc-3c1de97bbaee",
                    "LayerId": "569111d0-b32d-4c5e-b3ff-a0c1493d67ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "569111d0-b32d-4c5e-b3ff-a0c1493d67ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16f8b3d6-800d-4aa1-96e8-ccf670bce006",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "7e7782c2-acba-4f4c-b48b-41e8d3d0063a",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}