{
    "id": "d695fc8c-6e7b-43ce-9ad9-04cd831ea3e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_map_position",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 10,
    "bbox_right": 86,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e48ca239-5794-4dab-8b8d-404594c95e47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d695fc8c-6e7b-43ce-9ad9-04cd831ea3e2",
            "compositeImage": {
                "id": "77579c0f-4ad2-4ab9-a9ab-dbf7f99d6016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e48ca239-5794-4dab-8b8d-404594c95e47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c60c32-589f-466b-80a1-a34e0ae999a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e48ca239-5794-4dab-8b8d-404594c95e47",
                    "LayerId": "cdd61f9c-9562-4311-bfec-52e5645e1284"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cdd61f9c-9562-4311-bfec-52e5645e1284",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d695fc8c-6e7b-43ce-9ad9-04cd831ea3e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 1.85675025,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 99,
    "yorig": 24
}