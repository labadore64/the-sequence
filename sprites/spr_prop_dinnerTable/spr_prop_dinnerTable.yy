{
    "id": "bca02a93-2d8b-43d0-bc8d-62bde56c56cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_dinnerTable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 10,
    "bbox_right": 114,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5165233-5943-4c0d-804d-bf51976e020f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bca02a93-2d8b-43d0-bc8d-62bde56c56cc",
            "compositeImage": {
                "id": "b03f1e10-b272-4bb4-82de-f4533a1cf377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5165233-5943-4c0d-804d-bf51976e020f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00225639-941d-4399-ba1d-81fd9c133dbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5165233-5943-4c0d-804d-bf51976e020f",
                    "LayerId": "1a91329d-4167-4a87-9168-a97b2595d069"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1a91329d-4167-4a87-9168-a97b2595d069",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bca02a93-2d8b-43d0-bc8d-62bde56c56cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}