{
    "id": "ca56b283-2970-4785-81ed-22b72d372248",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_housesparrow1_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 15,
    "bbox_right": 120,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06ba77e2-45f8-45e0-988b-0d5b2a3fe9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca56b283-2970-4785-81ed-22b72d372248",
            "compositeImage": {
                "id": "3e7bba1d-a5a4-4c96-9aa0-404f44d7c275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ba77e2-45f8-45e0-988b-0d5b2a3fe9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb0b1aa8-13ee-4610-86d7-569e1c42b7e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ba77e2-45f8-45e0-988b-0d5b2a3fe9d2",
                    "LayerId": "910c4157-b2bf-44f3-9af5-6ea972dd0595"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "910c4157-b2bf-44f3-9af5-6ea972dd0595",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca56b283-2970-4785-81ed-22b72d372248",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}