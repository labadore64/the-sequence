{
    "id": "0d3089f3-7b4b-4acb-bfad-e2c04303b459",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_phantern_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 254,
    "bbox_left": 85,
    "bbox_right": 202,
    "bbox_top": 134,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e163c8b3-fd17-4aa9-8b0b-931e4ff27207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d3089f3-7b4b-4acb-bfad-e2c04303b459",
            "compositeImage": {
                "id": "3f6f7735-3832-4826-91a4-c75fb4b35794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e163c8b3-fd17-4aa9-8b0b-931e4ff27207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535b46b1-1fa1-4d58-9ea9-a54795f7795b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e163c8b3-fd17-4aa9-8b0b-931e4ff27207",
                    "LayerId": "f5b68484-60d7-4bcd-8053-6eabf172fefc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f5b68484-60d7-4bcd-8053-6eabf172fefc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d3089f3-7b4b-4acb-bfad-e2c04303b459",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 45,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}