{
    "id": "645517e6-fdc0-44fc-a272-0ea3756fe744",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_faceSpriteBruce",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "224fc663-0e6d-411c-8dc5-cc1a2cdcdba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645517e6-fdc0-44fc-a272-0ea3756fe744",
            "compositeImage": {
                "id": "56599621-3c6f-4af8-b2f7-86b43053297f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224fc663-0e6d-411c-8dc5-cc1a2cdcdba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0adf19aa-8182-4082-a3f4-51bca922309c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224fc663-0e6d-411c-8dc5-cc1a2cdcdba6",
                    "LayerId": "43624f11-7cf0-4d3a-945b-fe32c16a29a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "43624f11-7cf0-4d3a-945b-fe32c16a29a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "645517e6-fdc0-44fc-a272-0ea3756fe744",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "19b41542-e9b6-42a9-94c7-05cbadc1d852",
    "type": 1,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}