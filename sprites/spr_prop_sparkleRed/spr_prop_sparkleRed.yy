{
    "id": "1a079c92-e9de-4bba-9949-6bc209bd5117",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_sparkleRed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 55,
    "bbox_right": 73,
    "bbox_top": 60,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd79bcba-b57c-4b86-a4cd-d425f3332f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a079c92-e9de-4bba-9949-6bc209bd5117",
            "compositeImage": {
                "id": "7dadd2a4-3615-479c-ad2b-aded773989ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd79bcba-b57c-4b86-a4cd-d425f3332f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173f3ee8-05c4-4124-bd6b-86ff0def0d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd79bcba-b57c-4b86-a4cd-d425f3332f9b",
                    "LayerId": "aa6e66b6-b426-46d6-b63e-2fd056086a51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aa6e66b6-b426-46d6-b63e-2fd056086a51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a079c92-e9de-4bba-9949-6bc209bd5117",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}