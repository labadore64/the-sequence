{
    "id": "0e520d88-dd4b-43e2-9b15-c64f73556226",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_aspyre_cast1_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 6,
    "bbox_right": 249,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d94def82-4d43-4999-8ed7-d388d101c149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e520d88-dd4b-43e2-9b15-c64f73556226",
            "compositeImage": {
                "id": "745e1e6e-a082-4cbf-bbbc-f5b1d849b9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d94def82-4d43-4999-8ed7-d388d101c149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d533cfc-d9c7-4ad5-aafd-2b090c02bee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d94def82-4d43-4999-8ed7-d388d101c149",
                    "LayerId": "fe5088af-a627-4cac-89d7-52302ec0e00d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fe5088af-a627-4cac-89d7-52302ec0e00d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e520d88-dd4b-43e2-9b15-c64f73556226",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "808f5ba0-e9e9-4dd6-a3e1-5fd671722977",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}