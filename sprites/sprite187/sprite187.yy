{
    "id": "18265d66-17c8-4b0a-8d7f-bfbd843e46ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite187",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72f9e0f7-d42a-4ab9-8bb1-9760d05ab5a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18265d66-17c8-4b0a-8d7f-bfbd843e46ef",
            "compositeImage": {
                "id": "2438c5d8-3744-4ab1-9cab-f4b6ffc66556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f9e0f7-d42a-4ab9-8bb1-9760d05ab5a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bdb3850-be29-48c4-ad36-b249f13c3f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f9e0f7-d42a-4ab9-8bb1-9760d05ab5a0",
                    "LayerId": "b3135dd7-cdc0-41e3-ab43-8d15954e0f2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b3135dd7-cdc0-41e3-ab43-8d15954e0f2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18265d66-17c8-4b0a-8d7f-bfbd843e46ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 28,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}