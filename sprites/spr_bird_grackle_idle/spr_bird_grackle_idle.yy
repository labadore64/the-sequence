{
    "id": "4a228641-49d4-4221-97cc-517ac6d5d205",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bird_grackle_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 25,
    "bbox_right": 116,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92b8b52b-332d-49b9-99e9-21789c6768eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a228641-49d4-4221-97cc-517ac6d5d205",
            "compositeImage": {
                "id": "08a8a347-e300-4f2d-82e7-6198078fb410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b8b52b-332d-49b9-99e9-21789c6768eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc496999-846d-40ef-bdba-321a309f99fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b8b52b-332d-49b9-99e9-21789c6768eb",
                    "LayerId": "21eba6ae-51a2-4e81-b12f-f2189a378552"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "21eba6ae-51a2-4e81-b12f-f2189a378552",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a228641-49d4-4221-97cc-517ac6d5d205",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}