{
    "id": "363f8c1d-9dfd-4936-8114-679e304272da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_battle_enemy_spike_idle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 11,
    "bbox_right": 245,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e53c7712-66f5-4955-864b-b06c6bf77856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "976010ce-832c-4e50-8f57-63b655117f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e53c7712-66f5-4955-864b-b06c6bf77856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44ca4bf7-163c-4641-8d63-020ffdc57e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e53c7712-66f5-4955-864b-b06c6bf77856",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "826beb67-8dd3-4bea-91dc-67919a2699c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "23edda7f-cf49-47bb-bbc7-16af5928b9f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "826beb67-8dd3-4bea-91dc-67919a2699c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1030e33-79ca-49b5-9488-c49379a927e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "826beb67-8dd3-4bea-91dc-67919a2699c4",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "b3ed2dd8-487c-4408-bd3e-5b370c551cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "4045e2db-3f6b-49dd-a99b-d8ddb5931d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ed2dd8-487c-4408-bd3e-5b370c551cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9265f59b-feef-4276-ab60-58277e4967a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ed2dd8-487c-4408-bd3e-5b370c551cf4",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "c5b11a54-bc62-4660-9318-9a7f627bb99c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "f6294926-0b03-4633-9c8b-cd37d032e538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b11a54-bc62-4660-9318-9a7f627bb99c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1ed2e4-91e8-47aa-ad1b-094e4903c3a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b11a54-bc62-4660-9318-9a7f627bb99c",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "33fbcb02-7fe2-4699-9523-6f1f5623d1b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "a3de9ace-c6a9-4be9-885c-57608edf4a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33fbcb02-7fe2-4699-9523-6f1f5623d1b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1d9b40-4087-420e-94b8-a32e72bbe3c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33fbcb02-7fe2-4699-9523-6f1f5623d1b0",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "4bef41d0-536d-437a-8c7f-a50bde7f5f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "c9b46c72-3ea8-4403-ab4d-e2f42ba153d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bef41d0-536d-437a-8c7f-a50bde7f5f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adcb7c80-8f94-4748-87eb-119bdbbf51e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bef41d0-536d-437a-8c7f-a50bde7f5f86",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "de20b6e5-deea-43d1-b485-086cc00285ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "7cec6342-9fbd-4041-9828-2ad687548975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de20b6e5-deea-43d1-b485-086cc00285ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64666558-49fb-46cb-bb8d-afdb3009366b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de20b6e5-deea-43d1-b485-086cc00285ee",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "c147b370-8b8b-46c1-9f51-8311069788f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "dff81dd8-289e-4d0a-92d3-a7e44e81b5a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c147b370-8b8b-46c1-9f51-8311069788f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7840d873-10c0-4346-b30e-fc02a225e9d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c147b370-8b8b-46c1-9f51-8311069788f5",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "409c3dca-1a2d-4c1d-8665-43e72e235e07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "e1fff498-4631-4d05-afa8-a8034f586d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "409c3dca-1a2d-4c1d-8665-43e72e235e07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddfdc1b9-7bda-498d-9f5a-672e19f71026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "409c3dca-1a2d-4c1d-8665-43e72e235e07",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "2573476d-5f48-47f0-8710-804eb8dd4b79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "42b33fb7-f43e-465f-bcb8-a00140a3e86f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2573476d-5f48-47f0-8710-804eb8dd4b79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7112bb63-3ebe-4139-8c07-59664e939060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2573476d-5f48-47f0-8710-804eb8dd4b79",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "e5c6dbba-d60c-42b6-8aa7-17edad1b2474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "7f2d285a-007e-485f-86e3-1c223afd1402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c6dbba-d60c-42b6-8aa7-17edad1b2474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb2194f-544e-4aba-8dee-66ddf054fbd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c6dbba-d60c-42b6-8aa7-17edad1b2474",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "b4bc826c-ab25-47bd-8ec3-5421f9a5da9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "152118ab-95a6-4a4e-8106-d112a19470ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4bc826c-ab25-47bd-8ec3-5421f9a5da9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e33c9a6-bece-4040-bb81-4b15188da194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4bc826c-ab25-47bd-8ec3-5421f9a5da9a",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "4a5733e1-3127-46a7-a14c-5832509f4f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "172756a5-a1fd-469f-be67-23856a295cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a5733e1-3127-46a7-a14c-5832509f4f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2000a287-48e9-4023-a179-d2831603cef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a5733e1-3127-46a7-a14c-5832509f4f45",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "50874590-8809-4b8d-91a9-bb8234744a43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "fe77122a-697d-4406-848f-961adff0ba86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50874590-8809-4b8d-91a9-bb8234744a43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93af99b1-b802-4073-b759-0fa913fa10b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50874590-8809-4b8d-91a9-bb8234744a43",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "633056f3-f2a6-4f1b-a35c-0e76aa9c1c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "96a5cf1c-c884-40f4-823a-d1112b284068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "633056f3-f2a6-4f1b-a35c-0e76aa9c1c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e12030-db53-480f-ac7d-65dc3f8f1e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "633056f3-f2a6-4f1b-a35c-0e76aa9c1c58",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "52d6e47e-f7b5-4bc7-99fd-b8c6b63494c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "907a799a-010b-4257-a3c0-44681c42da30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d6e47e-f7b5-4bc7-99fd-b8c6b63494c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd4ae395-f412-461d-a1da-0a29ad1cf1ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d6e47e-f7b5-4bc7-99fd-b8c6b63494c8",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "ae2c9efa-7d37-448e-9942-775acbe832bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "f52bd40d-c24c-47e5-8e74-36c0029195b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae2c9efa-7d37-448e-9942-775acbe832bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6e06184-a27b-4843-a6d1-236fe1366f9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae2c9efa-7d37-448e-9942-775acbe832bd",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "bfdbe9f0-20c7-43a5-a4dc-05d17a305f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "5769efc7-1796-4d46-be1a-e082b0bad031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfdbe9f0-20c7-43a5-a4dc-05d17a305f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357e7ad9-995f-42d7-9a4a-ff57e7ddddcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfdbe9f0-20c7-43a5-a4dc-05d17a305f92",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        },
        {
            "id": "6bbd04ea-2aba-410f-b2fc-daf6c3567adc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "compositeImage": {
                "id": "7a2a5bc1-4ca1-4a53-b6b8-e27ce6a36853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bbd04ea-2aba-410f-b2fc-daf6c3567adc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f720ab1-adf0-4b06-8172-c5a374cfd6ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bbd04ea-2aba-410f-b2fc-daf6c3567adc",
                    "LayerId": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "0a5b48a0-1cd7-448f-8616-2d9ca2112c65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "363f8c1d-9dfd-4936-8114-679e304272da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "6535ae2f-d8ac-44c4-b49f-b65f6f1f572d",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 255
}