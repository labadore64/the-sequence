{
    "id": "b86c44bd-030e-4d73-86c6-79fa37eba24e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_filter_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 81,
    "bbox_right": 169,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bccd796-ba8d-4159-a06a-db40c28572fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b86c44bd-030e-4d73-86c6-79fa37eba24e",
            "compositeImage": {
                "id": "ab549ae4-b274-40b1-9d45-bbee2aff99ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bccd796-ba8d-4159-a06a-db40c28572fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ae6f311-1bfc-4882-9faa-9641880af129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bccd796-ba8d-4159-a06a-db40c28572fc",
                    "LayerId": "15ef9e59-9bc0-4adf-9d72-b37f628c78cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "15ef9e59-9bc0-4adf-9d72-b37f628c78cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b86c44bd-030e-4d73-86c6-79fa37eba24e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 180
}