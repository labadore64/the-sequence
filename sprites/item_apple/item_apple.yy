{
    "id": "ed305065-7195-44bc-b639-b84cb257c3f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "item_apple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 228,
    "bbox_left": 23,
    "bbox_right": 220,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69d91f41-4dac-4574-80e9-bd93156d804a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed305065-7195-44bc-b639-b84cb257c3f0",
            "compositeImage": {
                "id": "1662e7c6-0b82-48f3-94a0-f331e6741281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d91f41-4dac-4574-80e9-bd93156d804a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84adf5e6-bee7-46ca-a1d9-ba39c16208e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d91f41-4dac-4574-80e9-bd93156d804a",
                    "LayerId": "8ab10306-4b09-4ba6-ba9c-80a7c2d172f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8ab10306-4b09-4ba6-ba9c-80a7c2d172f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed305065-7195-44bc-b639-b84cb257c3f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}