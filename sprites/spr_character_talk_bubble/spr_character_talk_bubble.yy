{
    "id": "cbe81332-02ee-4a31-85f8-b80725d92c6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_talk_bubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3cc3f6d-9840-44d3-a788-1a36dc43b5a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe81332-02ee-4a31-85f8-b80725d92c6c",
            "compositeImage": {
                "id": "be8f36bd-71dc-4662-811d-10e7c2702dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3cc3f6d-9840-44d3-a788-1a36dc43b5a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ee065bc-2a91-4b0d-a9ef-319fc185939b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3cc3f6d-9840-44d3-a788-1a36dc43b5a8",
                    "LayerId": "08ce7fa1-9c24-486c-bfd4-20b27ac2cc69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "08ce7fa1-9c24-486c-bfd4-20b27ac2cc69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe81332-02ee-4a31-85f8-b80725d92c6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0.05000019,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}