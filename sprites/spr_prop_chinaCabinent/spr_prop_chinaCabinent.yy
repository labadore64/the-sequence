{
    "id": "512a0977-0a42-4513-b0b3-15ee4b57a0eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_prop_chinaCabinent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 22,
    "bbox_right": 106,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36b11e19-6071-4aca-ba8f-8a1c96b56854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512a0977-0a42-4513-b0b3-15ee4b57a0eb",
            "compositeImage": {
                "id": "c094f723-5cbd-4f80-9d10-efcf6a8fabeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b11e19-6071-4aca-ba8f-8a1c96b56854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8994bf8-c3f2-4002-9744-31a08461042a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b11e19-6071-4aca-ba8f-8a1c96b56854",
                    "LayerId": "12bcea0b-cbd4-4710-ab5a-2a46ec64c2c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "12bcea0b-cbd4-4710-ab5a-2a46ec64c2c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "512a0977-0a42-4513-b0b3-15ee4b57a0eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 1,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}