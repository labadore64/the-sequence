{
    "id": "b5b5d9ba-6d4c-4236-9f50-ee1638b49296",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "encounterTiles",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 2,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "dbe9caf7-8915-4f92-8a06-9ea3f882d37e",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 4,
    "tileheight": 32,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}