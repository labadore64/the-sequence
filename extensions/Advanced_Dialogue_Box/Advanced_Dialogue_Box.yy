{
    "id": "efd260f3-4430-40cb-889c-2b75098b5140",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "Advanced_Dialogue_Box",
    "IncludedResources": [
        "Sprites\\spr_portrait",
        "Sprites\\spr_cursor",
        "Sprites\\spr_chatbox",
        "Sprites\\spr_portrait2",
        "Sounds\\so_chat",
        "Scripts\\newChat",
        "Fonts\\f_chat",
        "Objects\\README",
        "Objects\\obj_dialogue",
        "Rooms\\room0"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-00-04 11:02:02",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "com.diestware.advanceddialoguebox",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "supportedTargets": -1,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosdelegatename": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "1.0.3"
}