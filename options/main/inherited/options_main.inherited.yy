1.0.0←ed6a955d-5826-4f98-a450-10b414266c27←ed6a955d-5826-4f98-a450-10b414266c27|{
    "option_gameguid": "bb2adb2d-6ecc-45be-943f-31ae4bd9d797",
    "option_lastchanged": "18 September 2020 19:11:41",
    "option_author": "PunishedFelix",
    "option_game_speed": 60
}←be5f1418-b31b-48af-a023-f04cdf6e5121|{
    "textureGroups": {
        "Additions": [
            {
                "Key": 1,
                "Value": {
                    "id": "65af0988-cb88-42df-b650-971d782eab72",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Font",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 2,
                "Value": {
                    "id": "4a8e27ac-4d24-4949-b359-46355e167777",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundOverworld01",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 3,
                "Value": {
                    "id": "bed839bd-b155-4540-a621-964448d42377",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundOverworld02",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 4,
                "Value": {
                    "id": "ff175901-ec9d-483a-9180-48c4f474844c",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_element",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 5,
                "Value": {
                    "id": "19b41542-e9b6-42a9-94c7-05cbadc1d852",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_face_sprite",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 6,
                "Value": {
                    "id": "ed8436d5-3937-421f-9a50-d3320ef06c20",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_stat_icon",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 7,
                "Value": {
                    "id": "1e479664-c8d0-4e3e-9d43-b1d1b8e5d215",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_dipper0",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 8,
                "Value": {
                    "id": "1da56509-3009-4dd8-9c99-99f6352cf90d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_tricorn",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 9,
                "Value": {
                    "id": "75bbdcfd-7fbe-43e4-b837-e411c82ea814",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_party_main",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 10,
                "Value": {
                    "id": "6c20423d-5ff7-4858-aa76-02182ffe8d6b",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_party_pierre",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 11,
                "Value": {
                    "id": "60f63707-79d2-420e-b054-bf244fe3419d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_party_7F7F7F",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 12,
                "Value": {
                    "id": "aca45c45-6cf3-4f13-8018-e7bdaed5cbd1",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundOverworld03",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 13,
                "Value": {
                    "id": "c59d5afc-a3e4-4946-9095-840bb00c9dba",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundOverworld04",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 14,
                "Value": {
                    "id": "c2964b5e-7657-43f8-90d7-9771fa335c9c",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBuilding01",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 15,
                "Value": {
                    "id": "e77601e8-e824-40e3-adc3-938104e85bec",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBuilding02",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 16,
                "Value": {
                    "id": "57570f21-0695-4f13-a303-519260b98bb3",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBuilding03",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 17,
                "Value": {
                    "id": "d88bd537-21cf-4c13-90d9-8153410ad825",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBuilding04",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 18,
                "Value": {
                    "id": "68830fb3-9087-468c-b1b4-fcc022a8879c",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBattle001",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 19,
                "Value": {
                    "id": "8fe852b8-9b54-467e-8a10-605b21170b0f",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBattle002",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 20,
                "Value": {
                    "id": "7e7782c2-acba-4f4c-b48b-41e8d3d0063a",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BackgroundBattle003",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 21,
                "Value": {
                    "id": "7976e58a-40f8-45f7-916f-55559250f195",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BattleSceneGraphics",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 22,
                "Value": {
                    "id": "6ede7562-7c5c-4143-a366-d84b74c40ab8",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Binoculars_shdr",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 23,
                "Value": {
                    "id": "ac1c074f-77c8-4cff-8cc3-e8f1a62d4829",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Binoculars_foreground",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 24,
                "Value": {
                    "id": "ffa25ff3-7f2d-4bc7-b842-72e9a33bd971",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "collision_sprites",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 25,
                "Value": {
                    "id": "e4ce4744-f5e7-4f64-9369-db7cae44bf83",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_dipper1",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 26,
                "Value": {
                    "id": "580e916e-bc19-4b49-aabe-9fd387268b2e",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_crookadoo0",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 27,
                "Value": {
                    "id": "808f5ba0-e9e9-4dd6-a3e1-5fd671722977",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_aspyre0",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 28,
                "Value": {
                    "id": "4a6112d1-aa22-4e25-a6cc-3bbd1df4ee7b",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_cerberpy0",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 29,
                "Value": {
                    "id": "6efab5bc-3ed9-4097-8e3a-b423e88d6ca0",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_binary0",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 30,
                "Value": {
                    "id": "af0e5729-66d4-4847-a163-9e7c02c2a2f3",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_logick",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 31,
                "Value": {
                    "id": "f87c8e4d-9fa6-4595-97e1-c9388e680f0b",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_hairy",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 32,
                "Value": {
                    "id": "6535ae2f-d8ac-44c4-b49f-b65f6f1f572d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_spike",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 33,
                "Value": {
                    "id": "4038d625-6d07-42e2-825f-bcc4b31256a3",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_daimen",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 34,
                "Value": {
                    "id": "15a0eafd-2aa4-4815-ba8f-57efdc41f764",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_coo",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 35,
                "Value": {
                    "id": "ea19956a-33ca-4e1d-bc47-a06f6113bab1",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_byrmine",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 36,
                "Value": {
                    "id": "02376ac4-5c41-4d41-862b-c0ab58d85802",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_statica1",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 37,
                "Value": {
                    "id": "660d4a34-eb5a-49a4-93ee-8d23a3172db4",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_babayoga",
                    "targets": 29263750006690030,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 38,
                "Value": {
                    "id": "0c1b29f3-5336-4ec1-b084-08d03bc391fd",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_utah",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 39,
                "Value": {
                    "id": "e5dfed5a-58ce-491f-b8a6-68360bea8ae2",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_brendafly",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 40,
                "Value": {
                    "id": "e3c3b26b-ae20-44a3-ba2c-2cd239127348",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_monolith",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 41,
                "Value": {
                    "id": "a007c71a-638e-4605-b559-ceb379856bdd",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_aloesant",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 42,
                "Value": {
                    "id": "5681c8e4-33b2-4144-800f-9b369c5fce5d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_phantern",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 43,
                "Value": {
                    "id": "d4bfa0fe-6e4c-4df7-abc4-4633abdabe22",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_snailcat",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 44,
                "Value": {
                    "id": "9e10e934-31a0-4a56-90f1-71dc8c1ec973",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_sheldon",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 45,
                "Value": {
                    "id": "7e557950-8e6b-4bcc-8a34-2600b78af370",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_skullbird",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 46,
                "Value": {
                    "id": "d41a043f-5316-4f92-8dc3-11dc8826a28b",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_snowbird",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 47,
                "Value": {
                    "id": "f06561cc-765a-44de-946d-9f8dc9dbab86",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_ithysaur",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 48,
                "Value": {
                    "id": "2e11f373-00b4-4204-976b-3ba150910fec",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_battle_vector",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 49,
                "Value": {
                    "id": "885781c6-d6ce-4320-b0de-d7a48d43760d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_party_bruce",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 50,
                "Value": {
                    "id": "7b778d75-44fd-4ecb-aef7-f51162934a3d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_enemy_ketsuban",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 51,
                "Value": {
                    "id": "c521fe91-d359-41ad-a304-750ae173f15c",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "spr_enemy_magzie",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            }
        ],
        "Checksum": "ࢺ懜璙ᓆ锯翋꤯맫",
        "Deletions": [
            
        ],
        "Ordering": [
            
        ]
    }
}←1225f6b0-ac20-43bd-a82e-be73fa0b6f4f|{
    "targets": 461609314234257646
}←7fa50043-cea6-4cd0-9521-a8ba8c6ea9f0|{
    "audioGroups": {
        "Additions": [
            {
                "Key": 1,
                "Value": {
                    "id": "3faaa6ec-481b-424b-a6f7-712595aea95a",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_monster_voice",
                    "targets": 29263750006690030
                }
            },
            {
                "Key": 2,
                "Value": {
                    "id": "8106a157-c2d0-4949-af75-b5e682c66e92",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_character_voice",
                    "targets": 29263750006690030
                }
            },
            {
                "Key": 3,
                "Value": {
                    "id": "bac14b1c-e330-4f85-b39d-d5cbbfd14017",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_braille",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 4,
                "Value": {
                    "id": "d88fbba7-9eb3-48fc-a463-2660a982b370",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_ax",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 5,
                "Value": {
                    "id": "2ce00712-685e-4c34-93fb-873e0e5ed2cc",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_battle",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 6,
                "Value": {
                    "id": "4c55b7ca-85e5-4260-8052-5ae5110cf6f5",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_monstercry",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 7,
                "Value": {
                    "id": "7696c522-5cda-48ac-8265-16345566adb2",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_birds",
                    "targets": 461609314234257646
                }
            },
            {
                "Key": 8,
                "Value": {
                    "id": "7393570d-f45c-48b7-987d-3876551d02e5",
                    "modelName": "GMAudioGroup",
                    "mvc": "1.0",
                    "groupName": "audiogroup_dialer",
                    "targets": 461609314234257646
                }
            }
        ],
        "Checksum": "濑Ⱥ朦씁ᬄ縈",
        "Deletions": [
            
        ],
        "Ordering": [
            
        ]
    }
}←7b2c4976-1e09-44e5-8256-c527145e03bb|{
    "targets": 461609314234257646
}