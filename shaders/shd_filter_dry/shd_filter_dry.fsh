varying vec2 v_texcoord;
uniform float alpha;

void main()
{ 
    vec4 colour = texture2D(gm_BaseTexture, v_texcoord);
    vec3 sepia = vec3(0.0);
    sepia.r = dot(colour.rgb, vec3(0.793,0.469,0.489));
    sepia.g = dot(colour.rgb, vec3(0.249,0.486,0.268));
    sepia.b = dot(colour.rgb, vec3(0.272,0.034,0.131));
    gl_FragColor.rgb = mix(colour.rgb,sepia.rgb, .95);
    gl_FragColor.a = colour.a*alpha;
}
