varying vec2 v_texcoord;
uniform float alpha;

void main()
{ 
    vec4 colour = texture2D(gm_BaseTexture,v_texcoord);
    gl_FragColor = vec4( (colour.rgb-100.00), colour.a*alpha);
}
