varying vec2 v_texcoord;
uniform float alpha;

void main()
{ 
    vec4 colour = texture2D(gm_BaseTexture, v_texcoord);
    vec3 sepia = vec3(0.0);
    sepia.r = dot(colour.rgb, vec3(0.193,0.169,0.189));
    sepia.g = dot(colour.rgb, vec3(0.149,0.186,0.168));
    sepia.b = dot(colour.rgb, vec3(0.472,0.634,0.831));
    gl_FragColor.rgb = mix(colour.rgb,sepia.rgb, .95);
    gl_FragColor.a = colour.a*alpha;
}
