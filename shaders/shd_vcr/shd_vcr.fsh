//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

//uniforms
uniform vec2 u_uv;
uniform float blur_amount;

uniform vec2 glitch_properties;
uniform vec3 glitch_wave_properties;
uniform vec2 jitter_properties;
uniform vec2 vignette_properties;
uniform vec2 scan_properties;

uniform float counter;

uniform float noise_strength;

float randomnoise(vec3 _scale, float _seed)
{
    return fract(sin(dot(vec3( v_vTexcoord, 1.0)+_seed, _scale))*43758.5453+_seed);
}

//random value
float random(float _scale, float _seed)
{
    return fract(sin(1.0+_seed)*43758.5453+_seed);
}

void main()
{
	vec4 base = texture2D( gm_BaseTexture, v_vTexcoord);
	vec4 px = v_vColour * base;
	
	///glitch///
	
	float val = mod(floor((counter*glitch_properties.y)+v_vTexcoord.y * glitch_properties.x )/glitch_properties.x,1.0);
	float randomlineval = (random(floor(val*1979.0),floor(counter*.25))-.5);
	
	if(floor(mod((glitch_properties.x*val),4.0)) == 0.0){
		px = mix(px,texture2D( gm_BaseTexture, vec2(v_vTexcoord.x-sin(counter*glitch_wave_properties.x)*glitch_wave_properties.y,v_vTexcoord.y)),.5);
	} else if(floor(mod((glitch_properties.x*val),2.0)) == 0.0){
		px = mix(px,texture2D( gm_BaseTexture, vec2(v_vTexcoord.x-sin(1748.8754*val+counter*glitch_wave_properties.x+v_vTexcoord.y)*glitch_wave_properties.y+randomlineval*glitch_wave_properties.z,v_vTexcoord.y)),.8);	
	}
	
	float modder = glitch_properties.x-8.0;
	if(modder < 1.0){
		modder = 1.0;	
	}
	
	float vall = mod(floor((counter*glitch_properties.y)+v_vTexcoord.y * modder )/modder,1.0);
	
	if(floor(mod((glitch_properties.x*vall),5.784578724)) <= 0.5){
		float brightness = (v_vTexcoord.y + .5)*.05;
		px = px + vec4(vec3(px.x,px.y,px.z)*vec3(brightness,brightness,brightness),1.0);
	}
	
	px = px;
	///glitch
	///jitter///
	float inc = floor(counter / jitter_properties.y);
	vec2 pos_r; 
	vec2 pos_g;
	vec2 pos_b;
	
	vec4 red;
	vec4 green;
	vec4 blue;
	//gets the random offset for each layer's colour
	float holder = random(1.0,inc);
	
	if(holder < .33){
		pos_r.x = (holder-0.5)*jitter_properties.x;
	}
	inc+=1.0;
	holder = random(1.0,inc);
	if(holder < .33){
	pos_r.y =(holder-0.5)*jitter_properties.x;
	}
	inc+=1.0;
	holder = random(1.0,inc);
	if(holder < .33){
	pos_g.x = (holder-0.5)*jitter_properties.x;
	}
	inc+=1.0;
	holder = random(1.0,inc);
	if(holder < .33){
	pos_g.y = (holder-0.5)*jitter_properties.x;
	}
	inc+=1.0;

	holder = random(1.0,inc);
	if(holder < .33){
	pos_b.x = (holder-0.5)*jitter_properties.x;
	}
	inc+=1.0;
	holder = random(1.0,inc);
	if(holder < .33){
	pos_b.y = (holder-0.5)*jitter_properties.x;
	}
	
	red = vec4(1.0,0.0,0.0,1.0) * texture2D(gm_BaseTexture, vec2(v_vTexcoord.x+pos_r.x,v_vTexcoord.y+pos_r.y));  
	green = vec4(0.0,1.0,0.0,1.0) *  texture2D(gm_BaseTexture, vec2(v_vTexcoord.x+pos_g.x,v_vTexcoord.y+pos_g.y));  
	blue = vec4(0.0,0.0,1.0,1.0) *  texture2D(gm_BaseTexture, vec2(v_vTexcoord.x+pos_b.x,v_vTexcoord.y+pos_b.y));  

	px = mix((red+green+blue),px,.8);
	///jitter///
	///scanlines///
	float pos = mod(v_vTexcoord.y,scan_properties.x)/scan_properties.x;

	vec3 lines = vec3(1.0);

	if(pos < 0.1){
		lines = mix(vec3(0.0),vec3(0.0,1.0,0.0),pos/0.1);
	} else if(pos > 0.9){
		lines = mix(vec3(0.0),vec3(1.0,0.0,1.0),(1.0-pos)/0.1);
	} else if (pos < .3){
		lines = vec3(0.0,1.0,0.0);
	} else if (pos > .7){
		lines = vec3(1.0,0.0,1.0);
	} else if (pos < .4){
		lines = mix(vec3(1.0),vec3(0.0,1.0,0.0),(0.4-pos)/0.1);
	} else if (pos > .6){
		lines = mix(vec3(1.0),vec3(1.0,0.0,1.0),(pos-0.6)/0.1);
	}

	px= mix(vec4(lines,1.0),px,1.0-scan_properties.y);
	///scanlines///
	///noise///
    float noise = noise_strength * ( 0.5 - randomnoise( vec3( 1.0 ), length( vec2(v_vTexcoord.x/v_vTexcoord.y, v_vTexcoord.y)*counter) ) );	
	px = vec4(vec3(px.x,px.y,px.z) + noise, px.a);
	///noise///
	///vignette///
	float vignette = distance( vec2(0.5, 0.5), v_vTexcoord);

	vignette = float(vignette_properties.y - vignette * vignette_properties.x);
	vec3 vignette_color = vec3(vec4(.25,.25,.25,1.0)/255.0) * vignette;
	px = px * vec4(mix(base.rgb,vignette_color,.5)+vignette_color, base.a);
	//vignette
	
    gl_FragColor = px;
}