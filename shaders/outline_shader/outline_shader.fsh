//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float alpha_amount;
uniform float border_size;

uniform vec4 u_uv;

void main()
{
	vec4 texColor = texture2D(gm_BaseTexture, v_vTexcoord);
	
	float alphaz = texColor.a;
	
	vec4 pointTest = vec4(0.0,0.0,0.0,0.0);
	
	float spacer_value = 0.0;
	float changeVal = .004;
	
	if(texColor.a < .99){
		for(float i = 0.0; i < 5.0; i++){
				spacer_value = i*changeVal;
				//pointTest = texture2D(gm_BaseTexture, vec2( v_vTexcoord.x + spacer_value, v_vTexcoord.y));
				//alphaz = (pointTest.a+alphaz)*0.75;
				//pointTest = texture2D(gm_BaseTexture, vec2( v_vTexcoord.x - spacer_value, v_vTexcoord.y));
				//alphaz = (pointTest.a+alphaz)*0.75;
				pointTest = texture2D(gm_BaseTexture, vec2( v_vTexcoord.x, v_vTexcoord.y + spacer_value));
				alphaz = (pointTest.a+alphaz)*0.5;
				//pointTest = texture2D(gm_BaseTexture, vec2( v_vTexcoord.x, v_vTexcoord.y - spacer_value));
				//alphaz = (pointTest.a+alphaz)*0.75;
			
		}
	} else {
		alphaz = 0.0;	
	}
	
	gl_FragColor = vec4(0.0,0.0,0.0, alphaz*alpha_amount) +(v_vColour * texture2D( gm_BaseTexture, v_vTexcoord ));
}
