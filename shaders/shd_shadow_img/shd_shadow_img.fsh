varying vec2 v_texcoord;
uniform float alpha;

uniform vec2 u_uv;

void main()
{ 
	float pos = (v_texcoord.y - u_uv[0]) / (u_uv[1] - u_uv[0]);
    vec4 colour = texture2D(gm_BaseTexture,v_texcoord);
    gl_FragColor = vec4( (colour.rgb-100.00), colour.a*alpha*pos);
}
