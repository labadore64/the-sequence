varying vec2 v_texcoord;

vec4 pixelize(vec2 uv, float sca) {
    float dx = 1.0/sca;
    float dy = 1.0/sca;
    vec2 coord = vec2(dx*ceil(uv.x/dx),dy*ceil(uv.y/dy));
    return texture2D(gm_BaseTexture, coord);
}

void main()
{
		float led_size = 2000.0;
		float brightness = 1.0;
		vec2 resolution = vec2(1024.0,768.0);
	
        vec2 uv = v_texcoord;
       
        vec2 coor = uv*led_size;
        coor.x *= (resolution.x/resolution.y);
        
        vec4 resColor = pixelize(uv, led_size) * brightness;

        float mvx = abs(sin(coor.x*3.1415))*1.5;
        float mvy = abs(sin(coor.y*3.1415))*1.5;
        resColor = resColor*(mvx*mvy);

 gl_FragColor = resColor;
 
}
