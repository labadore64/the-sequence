varying vec2 v_uv0;
varying vec2 v_uv0Ratio;

uniform float u_threshold;
uniform sampler2D sampler0;

void main()
{
    vec2 MK = texture2D( sampler0, v_uv0Ratio ).rg * 2.0 - 1.0;
    
    vec2 uv_dist = v_uv0 + (MK * u_threshold);
    
    vec4 Complete = texture2D( gm_BaseTexture, uv_dist );
    
    gl_FragColor = Complete;
}

