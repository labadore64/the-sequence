varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec4 u_settings; //vignette inner circle size, vignette outter circle size, noise strength
//uniform vec3 u_vignette_colour; //R,G,B

uniform float counter;

float random(vec3 _scale, float _seed)
{
    return fract(sin(dot(vec3(v_vTexcoord, 1.0)+_seed, _scale))*43758.5453+_seed);
}

void main()
{
	vec3 u_vignette_colour = vec3(0.75,0.75,0.75);
    vec4 base = texture2D( gm_BaseTexture, v_vTexcoord );

    float vignette = distance( vec2(0.5, 0.5), v_vTexcoord );
    vignette = u_settings.x - vignette * u_settings.y;
    vec3 vignette_colour = (u_vignette_colour/255.0) * vignette;

    float noise_strength = u_settings.z;
    float noise = noise_strength * ( 0.5 - random( vec3(1.0), counter*distance(v_vTexcoord,vec2(0.5))));
    if (u_settings.w == 1.0) {vignette_colour += noise;}

    gl_FragColor =  v_vColour * vec4(base.rgb * mix(vignette_colour,base.rgb,1.0-distance(v_vTexcoord,vec2(0.5))), base.a);
}

